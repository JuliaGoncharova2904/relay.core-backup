﻿using MomentumPlus.Relay.CompositionRoot;
using MomentumPlus.Relay.CompositionRoot.IOC;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Startup), "Configuration")]
namespace MomentumPlus.Relay.CompositionRoot
{
    public static class Startup
    {
        public static void Configuration()
        {
            RelayLightInject.RegisterContainer();
        }
    }
}
