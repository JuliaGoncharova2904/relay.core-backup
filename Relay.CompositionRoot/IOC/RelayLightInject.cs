﻿using System;
using LightInject;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Relay.Interfaces;
using System.Reflection;
using MomentumPlus.Relay.Mailer;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.Services;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Auth;
using MomentumPlus.Relay.Interfaces.Logic;

namespace MomentumPlus.Relay.CompositionRoot.IOC
{
    public static class RelayLightInject
    {
        private enum LifeTimeType { PerRequest = 1, PerThread = 2 }

        private static IServiceContainer _jobContainer;
        private const string WebAssemblyName = "MomentumPlus.Relay.Web";

        public static IServiceContainer JobContainer => _jobContainer;

        public static void RegisterContainer()
        {
            //-----------------------------------------------------------------------------
            IServiceContainer webContainer = CreateContainer(LifeTimeType.PerRequest);

            webContainer.RegisterControllers(Assembly.Load(WebAssemblyName));
            webContainer.EnableMvc();

            //-----------------------------------------------------------------------------
            IServiceContainer jobsContainer = CreateContainer(LifeTimeType.PerThread);

            jobsContainer.ScopeManagerProvider = new PerThreadScopeManagerProvider();

            _jobContainer = jobsContainer;
            //-----------------------------------------------------------------------------
        }

        private static IServiceContainer CreateContainer(LifeTimeType lifeTimeType)
        {
            IServiceContainer container = new ServiceContainer();


            using (container.BeginScope())
            {
                container.Register(typeof(IAuthBridge), typeof(AuthBridge), LifeTimeProvider(lifeTimeType));

                container.Register(typeof(IServicesUnitOfWork), typeof(ServicesUnitOfWork), LifeTimeProvider(lifeTimeType));
                container.Register(typeof(IRepositoriesUnitOfWork), typeof(RepositoriesUnitOfWork), LifeTimeProvider(lifeTimeType));
                container.Register(typeof(ILogicCoreUnitOfWork), typeof(LogicCoreUnitOfWork), LifeTimeProvider(lifeTimeType));
                container.Register(typeof(IMailerService), typeof(MailerService), LifeTimeProvider(lifeTimeType));
                container.Register(factory => new MomentumContext(factory.GetInstance<IAuthBridge>().GetActiveRelayDbConnectionString()), LifeTimeProvider(lifeTimeType));

                container.RegisterConstructorDependency<IMailerService>((factory, parameterInfo) => new MailerService(new RepositoriesUnitOfWork(new MomentumContext(container.GetInstance<IAuthBridge>().GetActiveRelayDbConnectionString()))));


                container.Register(factory =>
                {
                    Type adapterType = Assembly.Load(WebAssemblyName).GetType("MomentumPlus.Relay.Web.SignalRHubs.Adapters.NotificationAdapter");
                    ConstructorInfo providerConstructor = adapterType.GetConstructor(new Type[] { });
                    object adapterObject = providerConstructor.Invoke(new object[] { });

                    return (adapterObject as INotificationAdapter);
                });

            }

            return container;
        }

        private static ILifetime LifeTimeProvider(LifeTimeType lifeTimeType)
        {
            switch(lifeTimeType)
            {
                case LifeTimeType.PerRequest:
                    return new PerRequestLifeTime();
                case LifeTimeType.PerThread:
                    return new PerScopeLifetime();
                default:
                    throw new Exception();
            }
        }

    }
}