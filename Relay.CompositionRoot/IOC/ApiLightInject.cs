﻿using System;
using System.Reflection;
using System.Web.Http;
using LightInject;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Services;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Logic;
using MomentumPlus.Relay.Mailer;

namespace MomentumPlus.Relay.CompositionRoot
{
    public static class ApiLightInject
    {
        private const string ApiAssemblyName = "MomentumPlus.Relay.Api";

        public static void RegisterContainer(HttpConfiguration config)
        {
            IServiceContainer apiWebContainer = new ServiceContainer();

            apiWebContainer.RegisterApiControllers(Assembly.Load(ApiAssemblyName));


            apiWebContainer.Register(typeof(ApiServicesUnitOfWork), new PerRequestLifeTime());
            apiWebContainer.Register(typeof(LogicCoreUnitOfWork), new PerRequestLifeTime());

            apiWebContainer.Register<IApiServicesUnitOfWork>(factory => factory.GetInstance<ApiServicesUnitOfWork>(), new PerRequestLifeTime());
            apiWebContainer.Register<ILogicCoreUnitOfWork>(factory => factory.GetInstance<LogicCoreUnitOfWork>(), new PerRequestLifeTime());

            //apiWebContainer.Register<IMailerService>(factory => new MailerService());

            apiWebContainer.Register<INotificationAdapter>(factory =>
            {
                Type adapterType = Assembly.Load("MomentumPlus.Relay.Web").GetType("MomentumPlus.Relay.Web.SignalRHubs.Adapters.NotificationAdapter");
                ConstructorInfo providerConstructor = adapterType.GetConstructor(new Type[] { });
                object adapterObject = providerConstructor.Invoke(new object[] { });

                return (adapterObject as INotificationAdapter);
            });



            apiWebContainer.EnablePerWebRequestScope();
            apiWebContainer.EnableWebApi(config);



        }

    }
}
