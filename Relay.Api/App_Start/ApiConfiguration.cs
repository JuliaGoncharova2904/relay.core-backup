﻿using System.Web.Http;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using MomentumPlus.Relay.Api.Handlers;
using MomentumPlus.Relay.CompositionRoot;
using Newtonsoft.Json.Serialization;
using Owin;

namespace MomentumPlus.Relay.Api
{
    public static class ApiConfiguration
    {
        public static void Configure(IAppBuilder app)
        {
            // Web API configuration
            HttpConfiguration config = new HttpConfiguration();

            config.Services.Replace(typeof(IExceptionHandler), new ApiExceptionHandler());

            ConfigureApi(config);

            // Light Inject configuration
            ApiLightInject.RegisterContainer(config);

            app.UseWebApi(config);
        }

        // Web API configuration and services
        private static void ConfigureApi(HttpConfiguration config)
        {
            //Enable CORS 
            var cors = new EnableCorsAttribute(
                        origins: "*",
                        headers: "*",
                        methods: "*");
            config.EnableCors(cors);

            config.MessageHandlers.Add(new PreflightRequestsHandler());

            // Add Filters
            config.Filters.Add(new AuthorizeAttribute());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var jsonFormatter = config.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }

    }
}