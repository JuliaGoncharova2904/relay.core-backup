﻿using System;
using System.Threading;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Api.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        private IApiServicesUnitOfWork _services;

        protected IApiServicesUnitOfWork Services
        {
            get
            {
                return _services;
            }
        }


        protected BaseApiController(IApiServicesUnitOfWork servicesUnitOfWork)
        {
            _services = servicesUnitOfWork;
        }

        //private ApplicationUser _member;

        //public ApplicationUserManager UserManager
        //{
        //    get { return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        //}

        //public Guid CurrentUserId
        //{
        //    get
        //    {
        //        var user = UserManager.FindByName(User.Identity.Name);
        //        return user.Id;
        //    }
        //}

        //public ApplicationUser ApplicationUser
        //{
        //    get
        //    {
        //        if (_member != null)
        //        {
        //            return _member;
        //        }
        //        _member = UserManager.FindByEmail(Thread.CurrentPrincipal.Identity.Name);
        //        return _member;
        //    }
        //    set { _member = value; }
        //}

    }
}