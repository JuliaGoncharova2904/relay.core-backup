﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.Api;
using MomentumPlus.Relay.BLL.LogicCore.Cores;
using MomentumPlus.Relay.Models.Api;

namespace MomentumPlus.Relay.BLL.Mapper.ApiMappings
{
    public static class NotificationApiMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Notification, NotificationViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.IconPath.FullImagePath()))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.HtmlMessage))
                .ForMember(dest => dest.DateTime, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(dest => dest.IsOpened, opt => opt.MapFrom(src => src.IsOpened))
                .ForMember(dest => dest.TypeClass, opt => opt.ResolveUsing(src => src.Type.GetTypeClass()))
                .ForMember(dest => dest.RelationId, opt => opt.MapFrom(src => src.RelationId));

        }
    }
}
