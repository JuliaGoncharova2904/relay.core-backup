﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class SharingReportRelationMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RotationReportSharingRelation, ShareReportRelationViewModel>()
            .ForMember(dest => dest.RelationId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.RecipientId, opt => opt.MapFrom(src => src.RecipientId))
            .ForMember(dest => dest.RecipientTitle, opt => opt.MapFrom(src => src.Recipient.FullName))
            .ForMember(dest => dest.RecipientPosition, opt => opt.MapFrom(src => src.Recipient.Position.Name));
        }
    }
}
