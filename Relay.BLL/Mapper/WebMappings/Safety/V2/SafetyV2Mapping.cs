﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System.Linq;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class SafetyV2Mapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            #region MajorHazard

            cfg.CreateMap<MajorHazardV2, MajorHazardV2ViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.OwnerName, opt => opt.MapFrom(src => src.OwnerId.HasValue ? src.Owner.FullName : "Not Selected"))
                .ForMember(dest => dest.IconUrl, opt => opt.MapFrom(src => src.IconPath));

            cfg.CreateMap<MajorHazardV2, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            #endregion

            #region CriticalControl

            cfg.CreateMap<CriticalControlV2, CriticalControlV2ViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.OwnerName, opt => opt.MapFrom(src => src.Owner.FullName))
                .ForMember(dest => dest.Champions, opt => opt.MapFrom(src => src.Champions.Select(c => c.FullName).ToList()));

            cfg.CreateMap<CriticalControlV2, SelectListItem>()
                    .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Id));

            #endregion

            #region Archive

            cfg.CreateMap<SafetyMessageV2, ArchivedSafetyMessageV2ViewModel>()
                .ForMember(dest => dest.MessageId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.MajorHazardName, opt => opt.MapFrom(src => src.CriticalControl.MajorHazard.Name))
                .ForMember(dest => dest.MessageText, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.ScheduledDate, opt => opt.MapFrom(src => src.Date.FormatWithDayMonthYear()))
                .ForMember(dest => dest.LateOpenUsers, opt => opt.MapFrom(src => src.SafetyStats.Where(stat => stat.State == SafetyRecipientStateV2.ViewedLate).Select(stat => stat.Recipient)))
                .ForMember(dest => dest.OpenUsers, opt => opt.MapFrom(src => src.SafetyStats.Where(stat => stat.State == SafetyRecipientStateV2.Viewed).Select(stat => stat.Recipient)))
                .ForMember(dest => dest.UnopenedUsers, opt => opt.MapFrom(src => src.SafetyStats.Where(stat => stat.State == SafetyRecipientStateV2.NoViewed).Select(stat => stat.Recipient)));

            #endregion

            #region Safety Message
                cfg.CreateMap<SafetyMessageV2, SafetyMessageV2ViewModel>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.Message))
                    .ForMember(dest => dest.MajorHazardName, opt => opt.MapFrom(src => src.CriticalControl.MajorHazard.Name))
                    .ForMember(dest => dest.CriticalControlName, opt => opt.MapFrom(src => src.CriticalControl.Name))
                    .ForMember(dest => dest.IconUrl, opt => opt.MapFrom(src => src.CriticalControl.MajorHazard.IconPath));
            #endregion
        }
    }
}
