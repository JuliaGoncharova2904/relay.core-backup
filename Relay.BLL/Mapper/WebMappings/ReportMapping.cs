﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class ReportMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RotationModule, ReportModuleViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Type.ToString()))
                .ForMember(dest => dest.Topics, opt => opt.MapFrom(src => src.RotationTopicGroups.SelectMany(tg => tg.RotationTopics).Where(tg=> tg.Enabled)));

            cfg.CreateMap<RotationModule, ReportShiftModuleViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Type.ToString()))
                .ForMember(dest => dest.Topics, opt => opt.MapFrom(src => src.RotationTopicGroups.SelectMany(tg => tg.RotationTopics).Where(tg => tg.Enabled)));

            cfg.CreateMap<RotationTopic, ReportTopicViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.IsPinned, opt => opt.MapFrom(src => src.IsPinned))
                .ForMember(dest => dest.IsNullReport, opt => opt.MapFrom(src => src.IsNullReport))
                .ForMember(dest => dest.IsFinalized, opt => opt.MapFrom(src => src.FinalizeStatus == StatusOfFinalize.Finalized || src.FinalizeStatus == StatusOfFinalize.AutoFinalized))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.TopicGroupName, opt => opt.MapFrom(src => src.RotationTopicGroup.Name))
                .ForMember(dest => dest.FromRotationId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.RotationId))
                .ForMember(dest => dest.FromTeammateId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId))
                .ForMember(dest => dest.RelationId, opt => opt.MapFrom(src => src.RelationId))
                .ForMember(dest => dest.Tasks, opt => opt.MapFrom(src => src.RotationTasks.Where(tg => tg.Enabled)))
                .ForMember(dest => dest.HasComments, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.ManagerComments)))
                .ForMember(dest => dest.HasTasks, opt => opt.MapFrom(src => src.RotationTasks != null && src.RotationTasks.Any()))
                .ForMember(dest => dest.HasAttachments, opt => opt.MapFrom(src => src.Attachments != null && src.Attachments.Any()))
                .ForMember(dest => dest.HasVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages != null && src.VoiceMessages.Any()))
                .ForMember(dest => dest.HasLocation, opt => opt.MapFrom(src => src.Location != null))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName));

            cfg.CreateMap<RotationTopic, ReportShiftTopicViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.IsPinned, opt => opt.MapFrom(src => src.IsPinned))
                .ForMember(dest => dest.IsNullReport, opt => opt.MapFrom(src => src.IsNullReport))
                .ForMember(dest => dest.IsFinalized, opt => opt.MapFrom(src => src.FinalizeStatus == StatusOfFinalize.Finalized || src.FinalizeStatus == StatusOfFinalize.AutoFinalized))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.TopicGroupName, opt => opt.MapFrom(src => src.RotationTopicGroup.Name))
                .ForMember(dest => dest.FromShiftId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId))
                .ForMember(dest => dest.FromTeammateId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId))
                .ForMember(dest => dest.RelationId, opt => opt.MapFrom(src => src.RelationId))
                .ForMember(dest => dest.Tasks, opt => opt.MapFrom(src => src.RotationTasks.Where(tg => tg.Enabled)))
                .ForMember(dest => dest.HasComments, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.ManagerComments)))
                .ForMember(dest => dest.HasTasks, opt => opt.MapFrom(src => src.RotationTasks != null && src.RotationTasks.Any()))
                .ForMember(dest => dest.HasAttachments, opt => opt.MapFrom(src => src.Attachments != null && src.Attachments.Any()))
                .ForMember(dest => dest.HasVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages != null && src.VoiceMessages.Any()))
                .ForMember(dest => dest.HasLocation, opt => opt.MapFrom(src => src.Location != null))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName));

            cfg.CreateMap<RotationTask, ReportTaskViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.IsPinned, opt => opt.MapFrom(src => src.IsPinned))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.IsComplete, opt => opt.MapFrom(src => src.IsComplete))
                .ForMember(dest => dest.IsNullReport, opt => opt.MapFrom(src => src.IsNullReport))
                .ForMember(dest => dest.IsFinalized, opt => opt.MapFrom(src => src.FinalizeStatus == StatusOfFinalize.Finalized || src.FinalizeStatus == StatusOfFinalize.AutoFinalized))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.HasAttachments, opt => opt.MapFrom(src => src.Attachments != null && src.Attachments.Any()))
                .ForMember(dest => dest.HasVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages != null && src.VoiceMessages.Any()))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority.ToString()))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName));             
        }
    }
}
