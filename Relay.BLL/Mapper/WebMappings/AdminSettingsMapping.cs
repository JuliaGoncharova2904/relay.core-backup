﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class AdminSettingsMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<AdminSettings, PreferencesSettingsViewModel>()
                     .ForMember(dest => dest.HandoverPreviewTime, opt => opt.MapFrom(src =>  string.IsNullOrEmpty(src.HandoverPreviewTime) ? null : src.HandoverPreviewTime))
                     .ForMember(dest => dest.HandoverTriggerType, opt => opt.MapFrom(src => src.HandoverTriggerType))
                     .ForMember(dest => dest.LogoImageId, opt => opt.MapFrom(src => src.LogoId))
                     .ForMember(dest => dest.AllowedFileTypes, opt => opt.MapFrom(src => src.AllowedFileExtensions.Split(',')))
                     .ForMember(dest => dest.HandoverPreviewType, opt => opt.MapFrom(src => src.HandoverPreviewType))
                     .ForMember(dest => dest.Timezone, opt => opt.MapFrom(src => src.TimeZoneId))
                     .ForMember(dest => dest.HandoverTriggerTime, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.HandoverTriggerTime) ? "11:00pm" : src.HandoverTriggerTime));


        }
    }
}
