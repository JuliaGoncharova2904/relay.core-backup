﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class DashboardMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            #region Team member mapping

            cfg.CreateMap<UserProfile, TeamForWidgetViewModel>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.UserEmail, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.RotarionPatternName, opt => opt.MapFrom(src => src.RotationPattern.Name))
                .ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.Position.Name))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
                .ForMember(dest => dest.TeamName, opt => opt.MapFrom(src => src.Team.Name));

            #endregion

            #region Timeline mapping

            #endregion
        }
    }
}
