﻿using System;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class RotationMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            //----------------------------------------------------------
            cfg.CreateMap<CreateRotationViewModel, Rotation>()
                 .ForMember(dest => dest.Id, opt => opt.Ignore());

            cfg.CreateMap<Rotation, CreateRotationViewModel>()
                .ForMember(dest => dest.Contributors, opt => opt.Ignore())
                .ForMember(dest => dest.Projects, opt => opt.Ignore())
                .ForMember(dest => dest.ContributorsIds, opt => opt.MapFrom(src => src.RotationOwner.Contributors.Select(c => c.Id).ToList()));
            //----------------------------------------------------------
            cfg.CreateMap<EditRotationViewModel, Rotation>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.RotationOwner, opt => opt.Ignore())
                .ForMember(dest => dest.RotationOwnerId, opt => opt.Ignore())
                .ForMember(dest => dest.RotationType, opt => opt.Ignore());

            cfg.CreateMap<Rotation, EditRotationViewModel>()
                .ForMember(dest => dest.Contributors, opt => opt.Ignore())
                .ForMember(dest => dest.Projects, opt => opt.Ignore())
                .ForMember(dest => dest.RotationOwner, opt => opt.MapFrom(src => src.RotationOwner.FullName))
                .ForMember(dest => dest.RotationOwnerId, opt => opt.MapFrom(src => src.RotationOwnerId))
                .ForMember(dest => dest.PositionName, opt => opt.MapFrom(src => src.RotationOwner.Position.Name))
                .ForMember(dest => dest.PositionId, opt => opt.MapFrom(src => src.RotationOwner.PositionId))
                .ForMember(dest => dest.SiteName, opt => opt.MapFrom(src => src.RotationOwner.Position.Workplace.Name))
                .ForMember(dest => dest.SiteId, opt => opt.MapFrom(src => src.RotationOwner.Position.WorkplaceId))
                .ForMember(dest => dest.ContributorsIds, opt => opt.MapFrom(src => src.RotationOwner.Contributors.Select(c => c.Id).ToList()))
                .ForMember(dest => dest.RotationType, opt => opt.ResolveUsing(src =>
                {
                    Rotation nextRotation = src.NextRotation;
                    return (nextRotation != null) ? nextRotation.RotationType : src.RotationType;
                }));
            //----------------------------------------------------------
            cfg.CreateMap<Rotation, ReceivedRotationSlideViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.HandoverFrom, opt => opt.MapFrom(src => src.RotationOwner.FullName))
                .ForMember(dest => dest.Contributors, opt => opt.MapFrom(src => String.Join(" / ", src.RotationOwner.Contributors.Select(u => u.FullName))))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.StartDate.Value.FormatWithDayMonthYear() + " - " + src.StartDate.Value.AddDays(src.DayOn - 1).FormatWithDayMonthYear()));
            //----------------------------------------------------------
        }
    }
}
