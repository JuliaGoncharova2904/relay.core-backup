﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class SiteMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<Workplace, SiteViewModel>();

            cfg.CreateMap<SiteViewModel, Workplace>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());

        }
    }
}
