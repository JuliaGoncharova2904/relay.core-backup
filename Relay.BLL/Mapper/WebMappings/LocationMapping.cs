﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class LocationMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<Location, LocationViewModel>()
                    .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description));

            cfg.CreateMap<LocationViewModel, Location>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes));

        }
    }
}
