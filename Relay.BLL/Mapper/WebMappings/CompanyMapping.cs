﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class CompanyMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<Company, CompanyViewModel>()
                    .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Notes))
                    .ForMember(dest => dest.IsDefaultCompany, opt => opt.MapFrom(src => src.IsDefault));

            cfg.CreateMap<CompanyViewModel, Company>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.IsDefault, opt => opt.MapFrom(src => src.IsDefaultCompany));

        }
    }
}
