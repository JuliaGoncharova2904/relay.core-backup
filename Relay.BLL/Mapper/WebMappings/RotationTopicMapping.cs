﻿using System;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Extensions;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class RotationTopicMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            //====================================================================================================
            cfg.CreateMap<RotationTopic, HSETopicDialogViewModel>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.SearchTags.Split(',')))
                .ForMember(dest => dest.IsFromTemplate, opt => opt.MapFrom(src => src.TempateTopicId.HasValue))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.AttacmentsCounter, opt => opt.MapFrom(src => src.Attachments.Count))
                .ForMember(dest => dest.VoiceMessagesCounter, opt => opt.MapFrom(src => src.VoiceMessages.Count))
                .ForMember(dest => dest.TasksCounter, opt => opt.MapFrom(src => src.RotationTasks.Count))
                .ForMember(dest => dest.HasNewAttacments, opt => opt.MapFrom(src => src.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewTasks, opt => opt.MapFrom(src => src.RotationTasks.Any(t => t.CreatedUtc > DateTime.UtcNow.AddDays(-2))));

            cfg.CreateMap<HSETopicDialogViewModel, RotationTopic>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.ManagerComments, opt => opt.MapFrom(src => src.ManagerComments))
                .ForMember(dest => dest.SearchTags, opt => opt.MapFrom(src => string.Join(",", src.Tags)));

            //====================================================================================================
            cfg.CreateMap<RotationTopic, CoreTopicDialogViewModel>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.SearchTags.Split(',')))
                .ForMember(dest => dest.IsFromTemplate, opt => opt.MapFrom(src => src.TempateTopicId.HasValue))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.AttacmentsCounter, opt => opt.MapFrom(src => src.Attachments.Count))
                .ForMember(dest => dest.VoiceMessagesCounter, opt => opt.MapFrom(src => src.VoiceMessages.Count))
                .ForMember(dest => dest.TasksCounter, opt => opt.MapFrom(src => src.RotationTasks.Count))
                .ForMember(dest => dest.HasNewAttacments, opt => opt.MapFrom(src => src.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewTasks, opt => opt.MapFrom(src => src.RotationTasks.Any(t => t.CreatedUtc > DateTime.UtcNow.AddDays(-2))));

            cfg.CreateMap<CoreTopicDialogViewModel, RotationTopic>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.ManagerComments, opt => opt.MapFrom(src => src.ManagerComments))
                .ForMember(dest => dest.SearchTags, opt => opt.MapFrom(src => string.Join(",", src.Tags)));

            //====================================================================================================
            cfg.CreateMap<RotationTopic, ProjectTopicDialogViewModel>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.SearchTags.Split(',')))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.IsFromTemplate, opt => opt.MapFrom(src => src.TempateTopicId.HasValue))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.AttacmentsCounter, opt => opt.MapFrom(src => src.Attachments.Count))
                .ForMember(dest => dest.VoiceMessagesCounter, opt => opt.MapFrom(src => src.VoiceMessages.Count))
                .ForMember(dest => dest.TasksCounter, opt => opt.MapFrom(src => src.RotationTasks.Count))
                .ForMember(dest => dest.HasNewAttacments, opt => opt.MapFrom(src => src.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewTasks, opt => opt.MapFrom(src => src.RotationTasks.Any(t => t.CreatedUtc > DateTime.UtcNow.AddDays(-2))));

            cfg.CreateMap<ProjectTopicDialogViewModel, RotationTopic>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.ManagerComments, opt => opt.MapFrom(src => src.ManagerComments))
                .ForMember(dest => dest.SearchTags, opt => opt.MapFrom(src => string.Join(",", src.Tags)));

            //====================================================================================================
            cfg.CreateMap<RotationTopic, TeamTopicDialogViewModel>()
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.SearchTags.Split(',')))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired))
                .ForMember(dest => dest.IsFromTemplate, opt => opt.MapFrom(src => src.TempateTopicId.HasValue))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.AttacmentsCounter, opt => opt.MapFrom(src => src.Attachments.Count))
                .ForMember(dest => dest.VoiceMessagesCounter, opt => opt.MapFrom(src => src.VoiceMessages.Count))
                .ForMember(dest => dest.TasksCounter, opt => opt.MapFrom(src => src.RotationTasks.Count))
                .ForMember(dest => dest.HasNewAttacments, opt => opt.MapFrom(src => src.Attachments.Any(a => a.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages.Any(v => v.CreatedUtc > DateTime.UtcNow.AddDays(-2))))
                .ForMember(dest => dest.HasNewTasks, opt => opt.MapFrom(src => src.RotationTasks.Any(t => t.CreatedUtc > DateTime.UtcNow.AddDays(-2))));

            cfg.CreateMap<TeamTopicDialogViewModel, RotationTopic>()
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes))
                .ForMember(dest => dest.ManagerComments, opt => opt.MapFrom(src => src.ManagerComments))
                .ForMember(dest => dest.SearchTags, opt => opt.MapFrom(src => string.Join(",", src.Tags)));

            //====================================================================================================
            cfg.CreateMap<RotationTopic, TopicDetailsViewModel>()
                .ForMember(dest => dest.Section, opt => opt.MapFrom(src => src.RotationTopicGroup.RotationModule.Type.GetEnumDescription()))
                .ForMember(dest => dest.ItemName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.ManagerComments, opt => opt.MapFrom(src => src.ManagerComments))
                .ForMember(dest => dest.Tags, opt => opt.MapFrom(src => src.SearchTags.Split(',')))
                .ForMember(dest => dest.AttacmentsCounter, opt => opt.MapFrom(src => src.Attachments.Count))
                .ForMember(dest => dest.VoiceMessagesCounter, opt => opt.MapFrom(src => src.VoiceMessages.Count))
                .ForMember(dest => dest.TasksCounter, opt => opt.MapFrom(src => src.RotationTasks.Count))
                .ForMember(dest => dest.IsFeedbackRequired, opt => opt.MapFrom(src => src.IsFeedbackRequired));
            //====================================================================================================
        }
    }
}
