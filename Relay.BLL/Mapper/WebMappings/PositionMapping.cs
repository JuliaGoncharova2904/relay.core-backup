﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class PositionMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<Position, PositionViewModel>()
                    .ForMember(dest => dest.SiteId, opt => opt.MapFrom(src => src.WorkplaceId))
                    .ForMember(dest => dest.TemplateId, opt => opt.MapFrom(src => src.TemplateId));

            cfg.CreateMap<PositionViewModel, Position>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.TemplateId, opt => opt.Ignore())
                .ForMember(dest => dest.TemplateId, opt => opt.MapFrom(src => src.TemplateId))
                .ForMember(dest => dest.WorkplaceId, opt => opt.MapFrom(src => src.SiteId));

        }
    }
}
