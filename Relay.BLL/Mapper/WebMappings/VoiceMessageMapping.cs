﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class VoiceMessageMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<VoiceMessage, VoiceMessageViewModel>()
                .ForMember(dest => dest.AddedTime, opt => opt.MapFrom(src => src.CreatedUtc.FormatWithDayAndMonth()))
                .ForMember(dest => dest.FileName, opt => opt.MapFrom(src => src.Name + ".mp3"));

            cfg.CreateMap<VoiceMessageViewModel, VoiceMessage>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.FileId, opt => opt.Ignore());

        }
    }
}
