﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class EmployeeMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            //----------------------------------------------------------------------------
            //cfg.CreateMap<UserProfile, EmployeeViewModel>()
            //    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.UserName))
            //    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FirstName))
            //    .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.LastName))
            //    .ForMember(dest => dest.PositionId, opt => opt.MapFrom(src => src.PositionId))
            //    .ForMember(dest => dest.UserTypeId, opt => opt.MapFrom(src => src.RoleId))
            //    .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.PositionName));

            //cfg.CreateMap<EmployeeViewModel, UserProfile>()
            //    .ForMember(dest => dest.Id, opt => opt.Ignore())
            //    .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
            //    .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Name))
            //    .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.Surname))
            //    .ForMember(dest => dest.PositionId, opt => opt.MapFrom(src => src.PositionId))
            //    .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.UserTypeId))
            //    .ForMember(model => model.AvatarId, model => model.Ignore())
            //    .AfterMap((src, dst) =>
            //    {
            //        if (src.ImageId != null)
            //            dst.AvatarId = src.ImageId;
            //    });

            //----------------------------------------------------------------------------
            cfg.CreateMap<UserProfile, TeammateViewModel>()
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id));
            //----------------------------------------------------------------------------
            cfg.CreateMap<PersonalDetailsViewModel, UserProfile>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.PositionId, opt => opt.Ignore())
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.Surname))
                .ForMember(dest => dest.Position, opt => opt.Ignore());

            cfg.CreateMap<UserProfile, PersonalDetailsViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.Surname, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.PositionName));
            //----------------------------------------------------------------------------
            cfg.CreateMap<SecurityViewModel, UserProfile>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.RelayEmail))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.RelayEmail))
                .ForMember(dest => dest.SecondaryEmail, opt => opt.MapFrom(src => src.SecondaryEmail));

            cfg.CreateMap<UserProfile, SecurityViewModel>()
                .ForMember(dest => dest.RelayEmail, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.SecondaryEmail, opt => opt.MapFrom(src => src.SecondaryEmail));
            //----------------------------------------------------------------------------
            //cfg.CreateMap<UserProfile, UserDetailsViewModel>()
            //    .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role))
            //    .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FullName))
            //    .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
            //    .ForMember(dest => dest.LastLogin, opt => opt.MapFrom(src =>
            //        src.LastLoginDate.FormatWithMonth()
            //    ));            //----------------------------------------------------------------------------
        }
    }
}
