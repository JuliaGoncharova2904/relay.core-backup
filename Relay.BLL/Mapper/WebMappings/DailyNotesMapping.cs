﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class DailyNotesMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RotationTopic, DailyNoteViewModel>()
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => Convert.ToDateTime(src.Name).FormatWithDayAndMonthYear()))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.RotationId, opt => opt.MapFrom(src => src.RotationTopicGroup.RotationModule.RotationId))
                .ForMember(dest => dest.ShowInReport, opt => opt.MapFrom(src => src.Enabled))
                .ForMember(dest => dest.HasContent, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.Description) ||
                                                                                             src.RotationTasks.Any() ||
                                                                                             src.Attachments.Any() ||
                                                                                             src.VoiceMessages.Any()))
                .ForMember(dest => dest.TasksNumber, opt => opt.MapFrom(src => src.RotationTasks.Count))
                .ForMember(dest => dest.AttachmentsNummer, opt => opt.MapFrom(src => src.Attachments.Count))
                .ForMember(dest => dest.NormalDate, opt => opt.MapFrom(src => DateTime.Parse(src.Name)))
                .AfterMap((src, dest) =>
                {
                    DateTime srcDate;
                    Guid srcRotationId = src.RotationTopicGroup.RotationModule.RotationId.Value;
                    Guid currentRotationId = src.RotationTopicGroup.RotationModule.Rotation.RotationOwner.CurrentRotationId.Value;

                    if (srcRotationId == currentRotationId && DateTime.TryParse(src.Name, out srcDate))
                    {
                        dest.CanBeEdited = srcDate.Date <= DateTime.Today;
                        dest.IsActiveInPanel = srcDate.Date == DateTime.Today;
                        dest.IsFuture = srcDate.Date > DateTime.Today;
                    }
                });

            cfg.CreateMap<DailyNoteViewModel, RotationTopic>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.Ignore())
                .ForMember(dest => dest.Enabled, opt => opt.MapFrom(src => src.ShowInReport))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes));


            cfg.CreateMap<RotationTopic, DailyNotesTopicViewModel>()
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => Convert.ToDateTime(src.Name).FormatWithDayAndMonthYear()))
                .ForMember(dest => dest.OriginalDate, opt => opt.MapFrom(src => Convert.ToDateTime(src.Name)))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.IsFinalized, opt => opt.MapFrom(src => src.FinalizeStatus == StatusOfFinalize.Finalized || src.FinalizeStatus == StatusOfFinalize.AutoFinalized))
                .ForMember(dest => dest.HasComments, opt => opt.MapFrom(src => !string.IsNullOrEmpty(src.ManagerComments)))
                .ForMember(dest => dest.HasTasks, opt => opt.MapFrom(src => src.RotationTasks != null && src.RotationTasks.Any(t => t.Enabled)))
                .ForMember(dest => dest.HasAttachments, opt => opt.MapFrom(src => src.Attachments != null && src.Attachments.Any()))
                .ForMember(dest => dest.HasVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages != null && src.VoiceMessages.Any()))
                .ForMember(dest => dest.TeammateId, opt => opt.MapFrom(src => src.AssignedToId))
                .ForMember(dest => dest.FromTeammateId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId))
                .ForMember(dest => dest.FromRotationId, opt => opt.MapFrom(src => src.AncestorTopic.RotationTopicGroup.RotationModule.RotationId))
                .ForMember(dest => dest.HasLocation, opt => opt.MapFrom(src => src.Location != null));

            cfg.CreateMap<DailyNotesTopicViewModel, RotationTopic>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.FinalizeStatus, opt => opt.MapFrom(src => src.IsFinalized ? StatusOfFinalize.Finalized : StatusOfFinalize.NotFinalized))
                .ForMember(dest => dest.Name, opt => opt.Ignore())
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes));
        }

    }
}