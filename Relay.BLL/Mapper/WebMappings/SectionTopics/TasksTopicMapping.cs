﻿using AutoMapper;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class TasksTopicMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<RotationTask, TasksTopicViewModel>()
                .ForMember(dest => dest.Location, opt => opt.ResolveUsing(src =>
                {
                    DateTime date;

                    if (DateTime.TryParse(src.RotationTopic.Name, out date))
                    {
                        return date.FormatWithDayAndMonthYear();
                    }

                    return src.RotationTopic.Name;
                }))
                .ForMember(dest => dest.ChildCompleteStatus, opt => opt.ResolveUsing(src =>
                {
                    RotationTask childTask = src?.SuccessorTask;

                    if (childTask != null)
                    {
                        return childTask.IsComplete ? "Complete" : "Incomplete";
                    }

                    return "Incomplete";
                }))
                .ForMember(dest => dest.OwnerId, opt => opt.MapFrom(src => src.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                                                                            ? src.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                                                                            : src.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.ToString()))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
                .ForMember(dest => dest.Notes, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.Deadline, opt => opt.MapFrom(src => src.Deadline.FormatWithDayAndMonth()))
                .ForMember(dest => dest.TeammateId, opt => opt.MapFrom(src => src.AssignedToId))
                .ForMember(dest => dest.FromTeammateId, opt => opt.MapFrom(src => (src.AncestorTask != null)
                                                        ? (src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                                                            ? src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                                                            : src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)
                                                        : (src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                                                            ? src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                                                            : src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)))
                 .ForMember(dest => dest.FromTeammateFullName, opt => opt.MapFrom(src => (src.AncestorTask != null)
                                                        ? (src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                                                            ? src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                                                            : src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName)
                                                        : (src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                                                            ? src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                                                            : src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName)))
                .ForMember(dest => dest.FromRotationId, opt => opt.MapFrom(src => src.AncestorTask != null
                                                        ? src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.RotationId
                                                        : src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.RotationId))
                .ForMember(dest => dest.FromShiftId, opt => opt.MapFrom(src => src.AncestorTask != null
                                                        ? src.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.ShiftId
                                                        : src.RotationTopic.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId))
                .ForMember(dest => dest.HasComments, opt => opt.MapFrom(src => !string.IsNullOrWhiteSpace(src.ManagerComments)))
                .ForMember(dest => dest.IsFinalized, opt => opt.MapFrom(src => src.FinalizeStatus == StatusOfFinalize.Finalized || src.FinalizeStatus == StatusOfFinalize.AutoFinalized))
                .ForMember(dest => dest.HasAttachments, opt => opt.MapFrom(src => src.Attachments != null && src.Attachments.Any()))
                .ForMember(dest => dest.CompleteStatus, opt => opt.MapFrom(src => src.IsComplete ? "Complete" : "Incomplete"))
                .ForMember(dest => dest.SectionType, opt => opt.MapFrom(src => src.RotationTopic.RotationTopicGroup.RotationModule.Type.ToString()))
                .ForMember(dest => dest.HasVoiceMessages, opt => opt.MapFrom(src => src.VoiceMessages != null && src.VoiceMessages.Any()))
                .ForMember(dest => dest.IsCustom, opt => opt.MapFrom(src => !src.TemplateTaskId.HasValue))
                .ForMember(dest => dest.CarryforwardCounter, opt => opt.MapFrom(src => src.ForkParentTaskId.HasValue ? src.ForkParentTask.ForkCounter : src.ForkCounter))
                .ForMember(dest => dest.IsComplete, opt => opt.MapFrom(src => src.IsComplete))
                .ForMember(dest => dest.IsRotationMode, opt => opt.MapFrom(src => false));



            cfg.CreateMap<TasksTopicViewModel, RotationTask>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Name, opt => opt.Ignore())
                .ForMember(dest => dest.Deadline, opt => opt.Ignore())
                .ForMember(dest => dest.AssignedToId, opt => opt.MapFrom(src => src.TeammateId))
                .ForMember(dest => dest.FinalizeStatus, opt => opt.MapFrom(src => src.IsFinalized ? StatusOfFinalize.Finalized : StatusOfFinalize.NotFinalized))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Notes));
        }
    }
}
