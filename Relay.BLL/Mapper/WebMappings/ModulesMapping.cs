﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class ModulesMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<TemplateModule, rModuleViewModel>();
            cfg.CreateMap<rModuleViewModel, TemplateModule>();

            cfg.CreateMap<TemplateTopicGroup, rTopicGroupViewModel>();
            cfg.CreateMap<rTopicGroupViewModel, TemplateTopicGroup>();

            cfg.CreateMap<TemplateTopic, rTopicViewModel>();
            cfg.CreateMap<rTopicViewModel, TemplateTopic>();

            cfg.CreateMap<TemplateTask, rTaskViewModel>();
            cfg.CreateMap<rTaskViewModel, TemplateTask>();

            cfg.CreateMap<Template, TemplateViewModel>();
            cfg.CreateMap<TemplateViewModel, Template>();
        }
    }
}
