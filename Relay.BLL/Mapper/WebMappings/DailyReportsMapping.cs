﻿using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class DailyReportsMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Shift, DailyReportViewModel>()
                .ForMember(dest => dest.ShiftId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.RotationId, opt => opt.MapFrom(src => src.RotationId))
                .ForMember(dest => dest.IsCurrentShift, opt => opt.MapFrom(src => src.State == ShiftState.Confirmed || src.State == ShiftState.Break))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.StartDateTime.HasValue ? src.StartDateTime.FormatWithDayAndMonthAndYear() : "Shift Not Confirm"))
                .ForMember(dest => dest.ShiftRecipientFullName, opt => opt.MapFrom(src => src.ShiftRecipientId.HasValue ? src.ShiftRecipient.FullName : "No Recipient"))
                .ForMember(dest => dest.HasItems, opt => opt.MapFrom(src => 
                    src.RotationModules.Where(m => m.Enabled)
                                    .Any(module => module.RotationTopicGroups
                                                        .Where(tg => tg.Enabled)
                                                        .SelectMany(tg=> tg.RotationTopics)
                                                        .Where(t => t.Enabled)
                                                        .Any())
                 ))
                .ForMember(dest => dest.StartDateTime, opt => opt.MapFrom(src => src.StartDateTime));
        }

    }
}