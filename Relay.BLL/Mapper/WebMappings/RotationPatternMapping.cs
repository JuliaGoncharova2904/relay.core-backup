﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class RotationPatternMapping
    {
        public static void Configure(IMapperConfigurationExpression cfg)
        {

            cfg.CreateMap<RotationPattern, RotationPatternViewModel>();

            cfg.CreateMap<RotationPatternViewModel, RotationPattern>()
                .ForMember(dest => dest.Name, opt => opt.Ignore());

        }
    }
}
