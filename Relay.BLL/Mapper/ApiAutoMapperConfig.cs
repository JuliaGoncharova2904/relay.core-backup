﻿using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper.ApiMappings;

namespace MomentumPlus.Relay.BLL.Mapper
{
    public static class ApiAutoMapperConfig
    {
        private static MapperConfiguration _config;

        static ApiAutoMapperConfig()
        {
            ConfigureAutomapper();
        }

        private static void ConfigureAutomapper()
        {
            _config = new MapperConfiguration(cfg =>
            {
                NotificationApiMapping.Configure(cfg);
            });
        }
        public static IMapper GetMapper()
        {
            return _config.CreateMapper();
        }
    }
}