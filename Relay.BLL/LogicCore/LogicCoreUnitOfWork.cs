﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore.Cores;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces.Logic;
using MomentumPlus.Relay.Mailer;

namespace MomentumPlus.Relay.BLL.LogicCore
{
    public class LogicCoreUnitOfWork : ILogicCoreUnitOfWork
    {
        private readonly IAuthBridge _authBridge;
        public IRepositoriesUnitOfWork Repositories { get; }
        public INotificationAdapter NotificationAdapter { get; }
        public IMailerService MailerService { get; }

        private ModuleCore _moduleCore;
        private TopicGroupCore _topicGroupCore;
        private TopicCore _topicCore;
        private TaskCore _taskCore;

        private TemplateCore _templateCore;
        private RotationCore _rotationCore;
        private PositionCore _positionCore;
        private UserProfileCore _userProfileCore;

        private RotationModuleCore _rotationModuleCore;
        private RotationTopicGroupCore _rotationTopicGroupCore;
        private RotationTopicCore _rotationTopicCore;
        private RotationTaskCore _rotationTaskCore;
        private DailyNotesModuleCore _dailyNotesModuleCore;

        private TeamCore _teamCore;

        private MediaCore _mediaCore;


        private AdministrationCore _administrationCore;

        private SafetyMessageCore _safetyMessageCore;
        // Safety V2
        private MajorHazardV2Core _majorHazardV2Core;
        private CriticalControlV2Core _criticalControlV2Core;
        private SafetyStatV2Core _safetyMessageStatV2Core;
        private SafetyMessageV2Core _safetyMessageV2Core;
        //

        private ProjectCore _projectCore;
        private NotificationCore _notificationCore;
        private RotationTopicSharingRelationCore _rotationTopicSharingRelationCore;

        private ShiftCore _shiftCore;
        private SharingReportCore _sharingReportCore;

        private TaskBoardCore _taskBoardCore;
        private AttachmentCore _attachmentCore;
        private TeamRotationsCore _teamRotationsCore;

        private RotationTaskLogCore _taskLogCore;

        private RotationTopicLogCore _topicLogCore;

        private ContributorsCore _contributorsCore;


        private TaskBoardTasksCore _taskBoardTasksCore;

        public LogicCoreUnitOfWork(IRepositoriesUnitOfWork repositories, INotificationAdapter notificationAdapter, IAuthBridge authBridge)
        {
            this.Repositories = repositories;
            this.NotificationAdapter = notificationAdapter;
            this.MailerService = new MailerService(repositories);
            this._authBridge = authBridge;

        }

        public void SyncWithDatabase()
        {
            Repositories.Save();
        }

        #region Cores

        public TaskBoardTasksCore TaskBoardTasksCore => _taskBoardTasksCore ?? (_taskBoardTasksCore = new TaskBoardTasksCore(this.Repositories, this));


        public RotationTopicLogCore RotationTopicLogCore => _topicLogCore ?? (_topicLogCore = new RotationTopicLogCore(this.Repositories, this));


        public RotationTaskLogCore RotationTaskLogCore => _taskLogCore ?? (_taskLogCore = new RotationTaskLogCore(this.Repositories, this));

        public TeamRotationsCore TeamRotationsCore => _teamRotationsCore ?? (_teamRotationsCore = new TeamRotationsCore(this.Repositories, this));

        public AttachmentCore AttachmentCore => _attachmentCore ?? (_attachmentCore = new AttachmentCore(this.Repositories, this));

        public TaskBoardCore TaskBoardCore => _taskBoardCore ?? (_taskBoardCore = new TaskBoardCore(this.Repositories, this));


        public SharingReportCore SharingReportCore => _sharingReportCore ?? (_sharingReportCore = new SharingReportCore(this.Repositories, this));

        public ShiftCore ShiftCore => _shiftCore ?? (_shiftCore = new ShiftCore(this.Repositories, this));

        public RotationTopicSharingRelationCore RotationTopicSharingRelationCore => _rotationTopicSharingRelationCore ?? (_rotationTopicSharingRelationCore = new RotationTopicSharingRelationCore(this.Repositories, this));

        public NotificationCore NotificationCore => _notificationCore ?? (_notificationCore = new NotificationCore(this.Repositories, this, this.NotificationAdapter, this._authBridge));

        public SafetyStatV2Core SafetyMessageStatV2Core => _safetyMessageStatV2Core ?? (_safetyMessageStatV2Core = new SafetyStatV2Core(this.Repositories, this));

        public MajorHazardV2Core MajorHazardV2Core => _majorHazardV2Core ?? (_majorHazardV2Core = new MajorHazardV2Core(this.Repositories, this));

        public CriticalControlV2Core CriticalControlV2Core => _criticalControlV2Core ?? (_criticalControlV2Core = new CriticalControlV2Core(this.Repositories, this));

        public SafetyMessageV2Core SafetyMessageV2Core => _safetyMessageV2Core ?? (_safetyMessageV2Core = new SafetyMessageV2Core(this.Repositories, this));

        public SafetyMessageCore SafetyMessageCore => _safetyMessageCore ?? (_safetyMessageCore = new SafetyMessageCore(this.Repositories, this));

        public AdministrationCore AdministrationCore => _administrationCore ?? (_administrationCore = new AdministrationCore(this.Repositories, this));

        public TeamCore TeamCore => _teamCore ?? (_teamCore = new TeamCore(this.Repositories, this));

        public MediaCore MediaCore => _mediaCore ?? (_mediaCore = new MediaCore(this.Repositories, this));

        public RotationTopicGroupCore RotationTopicGroupCore => _rotationTopicGroupCore ?? (_rotationTopicGroupCore = new RotationTopicGroupCore(this.Repositories, this));

        public RotationTaskCore RotationTaskCore => _rotationTaskCore ?? (_rotationTaskCore = new RotationTaskCore(this.Repositories, this));

        public RotationTopicCore RotationTopicCore => _rotationTopicCore ?? (_rotationTopicCore = new RotationTopicCore(this.Repositories, this));

        public RotationModuleCore RotationModuleCore => _rotationModuleCore ?? (_rotationModuleCore = new RotationModuleCore(this.Repositories, this));

        public RotationCore RotationCore => _rotationCore ?? (_rotationCore = new RotationCore(this.Repositories, this, MailerService, _authBridge.AuthService));

        public TemplateCore TemplateCore => _templateCore ?? (_templateCore = new TemplateCore(this.Repositories, this));

        public TaskCore TaskCore => _taskCore ?? (_taskCore = new TaskCore(this.Repositories, this));

        public ModuleCore ModuleCore => _moduleCore ?? (_moduleCore = new ModuleCore(this.Repositories, this));

        public TopicGroupCore TopicGroupCore => _topicGroupCore ?? (_topicGroupCore = new TopicGroupCore(this.Repositories, this));

        public TopicCore TopicCore => _topicCore ?? (_topicCore = new TopicCore(this.Repositories, this));

        public PositionCore PositionCore => _positionCore ?? (_positionCore = new PositionCore(this.Repositories, this));

        public UserProfileCore UserProfileCore => _userProfileCore ?? (_userProfileCore = new UserProfileCore(this.Repositories, this));

        public DailyNotesModuleCore DailyNotesModuleCore => _dailyNotesModuleCore ?? (_dailyNotesModuleCore = new DailyNotesModuleCore(this.Repositories, this));

        public ProjectCore ProjectCore => _projectCore ?? (_projectCore = new ProjectCore(this.Repositories, this));

        public ContributorsCore ContributorsCore => _contributorsCore ?? (_contributorsCore = new ContributorsCore(this.Repositories, this));

        #endregion

    }
}
