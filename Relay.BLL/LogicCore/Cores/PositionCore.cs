﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class PositionCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public PositionCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<Position> GetRelayPositions()
        {
            return _repositories.PositionRepository.Find(p => p.Template != null).OrderBy(p => p.Name);
        }
    }
}
