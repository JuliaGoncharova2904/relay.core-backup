﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;
//using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TopicGroupCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TopicGroupCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<TemplateTopicGroup> GetModuleTopicGroups(Guid moduleId)
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ModuleId == moduleId).OrderBy(tg => tg.Name);

            return model;
        }

        public IQueryable<TemplateTopicGroup> GetBaseTopicGroups()
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => !tg.ParentTopicGroupId.HasValue && tg.Module.Type == TypeOfModule.Core 
                                                                           || !tg.ParentTopicGroupId.HasValue && tg.Module.Type == TypeOfModule.HSE).OrderBy(tg => tg.Name);
             
            return model;
        }

        public TemplateTopicGroup GetTopicGroup(Guid topicGroupId)
        {
            var topicGroup = _repositories.TemplateTopicGroupRepository.Get(topicGroupId);

            return topicGroup;
        }

        public TemplateTopicGroup GetChildTopicGroup(Guid parentTopicGroupId, Guid moduleId)
        {
            var topicGroup = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ParentTopicGroupId == parentTopicGroupId && tg.ModuleId == moduleId).FirstOrDefault();

            return topicGroup;
        }

        public TemplateTopicGroup AddChildTopicGroup(TemplateTopicGroup baseTopicGroup, Guid moduleId, bool status = false)
        {
            var topicGroup = new TemplateTopicGroup
            {
                Id = Guid.NewGuid(),
                Enabled = status,
                ParentTopicGroupId = baseTopicGroup.Id,
                ModuleId = moduleId,
                RelationId = baseTopicGroup.RelationId,
                Description = baseTopicGroup.Description,
                Name = baseTopicGroup.Name
            };

            _repositories.TemplateTopicGroupRepository.Add(topicGroup);
            _repositories.Save();

            return topicGroup;
        }

        public IQueryable<TemplateTopicGroup> GetChildTopicGroups(Guid topicGroupId)
        {
            var topicGroups = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ParentTopicGroupId == topicGroupId);

            return topicGroups;
        }

        public void UpdateChildTopicGroups(TemplateTopicGroup topicGroup)
        {
            var childTopicGroups = GetChildTopicGroups(topicGroup.Id).ToList();

            foreach (var childTopicGroup in childTopicGroups)
            {
                UpdateChildTopicGroup(topicGroup, childTopicGroup);
            }
        }

        public void UpdateChildTopicGroup(TemplateTopicGroup parentTopicGroup, TemplateTopicGroup childTopicGroup)
        {
            childTopicGroup.Description = parentTopicGroup.Description;
            childTopicGroup.Name = parentTopicGroup.Name;
            childTopicGroup.RelationId = parentTopicGroup.RelationId;
            childTopicGroup.ParentTopicGroupId = parentTopicGroup.Id;
            childTopicGroup.Id = childTopicGroup.Id;

            _repositories.TemplateTopicGroupRepository.Update(childTopicGroup);
            _repositories.Save();
        }


        /// <summary>
        /// Get Or Create Template Topic Group From Base Topic Group
        /// </summary>
        /// <param name="templateModuleId"></param>
        /// <param name="baseTopicGroup"></param>
        /// <returns></returns>

        public TemplateTopicGroup GetOrCreateTemplateTopicGroup(Guid templateModuleId, TemplateTopicGroup baseTopicGroup)
        {
            var templateTopicGroup = GetChildTopicGroup(baseTopicGroup.Id, templateModuleId) ??
                                     AddChildTopicGroup(baseTopicGroup, templateModuleId);

            return templateTopicGroup;
        }

        public IQueryable<RotationTopic> FindTopicGroupsTopics(Guid?[] topicGroupsIds, Guid[] userFilterIds = null)
        {
            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                            && tg.RotationModule.SourceType == TypeOfModuleSource.Draft
                                                                            && topicGroupsIds.Contains(tg.TempateTopicGroup.ParentTopicGroupId))
                                                                              .SelectMany(tg => tg.RotationTopics)
                .Where(t => t.Enabled  && t.RotationTopicGroup.RotationModule.RotationId.HasValue && t.RotationTopicGroup.RotationModule.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Swing
                || t.Enabled && t.RotationTopicGroup.RotationModule.ShiftId.HasValue && t.RotationTopicGroup.RotationModule.Shift.State != ShiftState.Created && t.RotationTopicGroup.RotationModule.Shift.Rotation.State != RotationState.Created);

            //var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
            //                                                                  && tg.RotationModule.SourceType == TypeOfModuleSource.Draft
            //                                                                  && topicGroupsIds.Contains(tg.TempateTopicGroup.ParentTopicGroupId))
            //                                                                    .SelectMany(tg => tg.RotationTopics)
            //      .Where(t => t.Enabled && t.RotationTopicGroup.RotationModule.RotationId.HasValue && t.RotationTopicGroup.RotationModule.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Swing
            //      || t.Enabled && t.RotationTopicGroup.RotationModule.ShiftId.HasValue && t.RotationTopicGroup.RotationModule.Shift.State != ShiftState.Created && t.RotationTopicGroup.RotationModule.Shift.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Shift);

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId));

            }

            return topics;
        }

        public List<RotationTopic> FindTopicGroupsByName(string searchText, Guid? userId = null)
        {
            var topics = _repositories.RotationTopicGroupRepository.Find(t => t.Name == searchText).SelectMany(tg => tg.RotationTopics);

            if (userId != null)
            {
                topics = topics.Where(t => t.CreatorId == userId);
            }

            return topics.ToList();
        }

        public List<RotationTopic> FindReferenceByNameInTopicGroup(string searchText, Guid? userId = null)
        {
            var topics = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(t => t.RotationTopics).Where(r => r.Name == searchText);

            if (userId != null)
            {
                topics = topics.Where(t => t.CreatorId == userId);
            }

            return topics.ToList();
        }


        public List<RotationTopic> FindNotesByNameInTopicGroup(string searchText, Guid? userId = null)
        {
            IQueryable<RotationTopic> topics = null;

            topics = _repositories.RotationTopicGroupRepository.Find(t => t.Description == searchText).SelectMany(tg => tg.RotationTopics);

            if (topics.Count() == 0)
            {
                topics = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(t => t.RotationTopics).Where(t => t.Description == searchText);
            }

            if (userId != null)
            {
                topics = topics.Where(t => t.CreatorId == userId);
            }

            return topics.ToList();
        }

        public RotationTopic FindTopicGroupByName(string searchText)
        {
            var topic = _repositories.RotationTopicGroupRepository.Find(t => t.Name == searchText).SelectMany(tg => tg.RotationTopics).FirstOrDefault();

            return topic;
        }


        public RotationTopic FindTagsByName(string searchText)
        {
            List<RotationTopic> topics = null;

            if (searchText != null)
            {
                var topicHastags = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(tg => tg.RotationTopics).Where(t => t.SearchTags != null).ToList();

                topics = topicHastags.Where(t=> IsContainsSearchTag(t.SearchTags, searchText)).ToList();

                return topics.FirstOrDefault();
            }
            return null;
        }


        public RotationTask FindTaskByName(string searchText)
        {
            List<RotationTask> tasks = null;

            if (searchText != null)
            {
                tasks = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(t => t.RotationTopics.SelectMany(ts => ts.RotationTasks.Where(tn => tn.Name == searchText))).ToList();

                return tasks.FirstOrDefault();
            }
            return null;
        }

        public List<RotationTask> FindTasksByName(string searchText, Guid[] userFilterIds = null)
        {
            List<RotationTask> tasks = null;

            if (searchText != null)
            {
                tasks = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(t => t.RotationTopics.SelectMany(ts => ts.RotationTasks.Where(tn => tn.Name == searchText))).ToList();

                if (userFilterIds != null)
                {
                   
                }
            }

            return tasks;
        }

        public List<RotationTopic> FindTopicsByTagName(string searchText, Guid[] userFilterIds = null)
        {
            List<RotationTopic> topics = null;

            if (searchText != null)
            {
                var topicHastags = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(tg => tg.RotationTopics).Where(t => t.SearchTags != null).ToList();

                topics = topicHastags.Where(t => IsContainsSearchTag(t.SearchTags, searchText)).ToList();

                if (userFilterIds != null)
                {
                    topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                    && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                      || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                    && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();

                }
            }

            return topics;
        }

        private bool IsContainsSearchTag(string searchTags, string search)
        {
            var res = searchTags.Split(',').ToList();

            foreach (var tag in res)
            {
                if (tag == search)
                {
                    return true;
                }
            }
            return false;
        }

    }
}