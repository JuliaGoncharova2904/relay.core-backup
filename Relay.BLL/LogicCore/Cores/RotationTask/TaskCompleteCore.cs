﻿using System;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskCompleteCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskCompleteCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Complite task
        /// </summary>
        /// <param name="task"></param>
        /// <param name="feedBack"></param>
        public void CompleteTask(RotationTask task, string feedBack)
        {
            for(RotationTask complitTask = task; complitTask != null; complitTask = complitTask.AncestorTask)
            {
                complitTask.IsComplete = true;
                complitTask.Feedback = feedBack;
                complitTask.CompleteDateUtc = DateTime.UtcNow;

                _repositories.RotationTaskRepository.Update(complitTask);
            }

            _repositories.Save();

            this.NitifyAllTaskOwners(task);

            _logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{task.AssignedTo.FullName}</strong> completed the task.");
        }

        /// <summary>
        /// Notify all task owners
        /// </summary>
        /// <param name="task">RotationTask entity</param>
        private void NitifyAllTaskOwners(RotationTask task)
        {
            if (!task.AncestorTaskId.HasValue) return;

            string personName = task.TaskBoard?.User.FullName
                                 ?? task.RotationTopic.RotationTopicGroup.RotationModule.Rotation?.RotationOwner.FullName
                                 ?? task.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName;

            for (RotationTask parentTask = task.AncestorTask; parentTask != null; parentTask = parentTask.AncestorTask)
            {
                Guid notifyRecipientId = parentTask.TaskBoardId
                                        ?? parentTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation?.RotationOwner.Id
                                        ?? parentTask.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.Id;

                _logicCore.NotificationCore.NotificationTrigger.Send_UserHasCompletedNowTask(notifyRecipientId, parentTask.Id, personName);
            }
        }

    }
}
