﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskShareCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskShareCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public RotationTask AddShareTask(RotationTopic topic)
        {
            DateTime deadline = DateTime.Today;
            if (topic.RotationTopicGroup.RotationModule.RotationId.HasValue)
            {
                deadline = _logicCore.RotationCore.RotationExpiredDate(topic.RotationTopicGroup.RotationModule.Rotation).AddDays(-1);
            }
            else if (topic.RotationTopicGroup.RotationModule.ShiftId.HasValue)
            {
                Shift shift = topic.RotationTopicGroup.RotationModule.Shift;
                deadline = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes);
            }

            RotationTask task = new RotationTask
            {
                Id = Guid.NewGuid(),
                Enabled = topic.Enabled,
                RotationTopicId = topic.Id,
                Description = "Please read the topic details that have been shared with you.",
                Name = "For information",
                AssignedToId = topic.AssignedToId,
                IsNullReport = topic.IsNullReport,
                Status = StatusOfTask.NewNow,
                TemplateTaskId = null,
                FinalizeStatus = topic.FinalizeStatus,
                IsFeedbackRequired = false,
                IsPinned = topic.IsPinned,
                ManagerComments = null,
                SearchTags = "Shared",
                Deadline = deadline,
                IsComplete = false,
                Priority = PriorityOfTask.Normal
            };

            _repositories.RotationTaskRepository.Add(task);
            _repositories.Save();

            return task;

        }

    }
}
