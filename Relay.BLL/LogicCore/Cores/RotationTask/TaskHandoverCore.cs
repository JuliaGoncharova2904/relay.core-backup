﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskHandoverCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;


        public TaskHandoverCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public RotationTask GetOrCreateChildTask(Guid topicId, RotationTask parenTask, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            var childTask = GetChildTask(topicId, parenTask) ??
                                     AddChildRotationTask(topicId, parenTask, finalizeStatus);

            return childTask;
        }

        private RotationTask GetChildTask(Guid rotationTopicId, RotationTask parenTask)
        {
            var childTask = _repositories.RotationTaskRepository.Find(t => t.RotationTopicId == rotationTopicId
                                                                        && t.AncestorTaskId == parenTask.Id).FirstOrDefault();

            return childTask;
        }

        private RotationTask AddChildRotationTask(Guid topicId, RotationTask parenTask, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            Guid? assignedToId = null;

            if (parenTask.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue)
            {
                assignedToId = parenTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.NextRotationId.HasValue ?
                                                        parenTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.NextRotation.DefaultBackToBackId :
                                                        parenTask.AssignedToId;
            }
            else if (parenTask.RotationTopic.RotationTopicGroup.RotationModule.ShiftId.HasValue)
            {
                assignedToId = parenTask.RotationTopic.RotationTopicGroup.RotationModule.Shift.ShiftRecipientId;
            }

            var newTask = parenTask.Clone();
            newTask.RotationTopicId = topicId;
            newTask.AssignedToId = assignedToId;
            newTask.IsComplete = false;
            newTask.TemplateTaskId = null;
            //RotationTask task = new RotationTask
            //{
            //    Id = Guid.NewGuid(),
            //    Enabled = parenTask.Enabled,
            //    RotationTopicId = topicId,
            //    Description = parenTask.Description,
            //    CopyTimesCounter = parenTask.CopyTimesCounter + 1,
            //    Name = parenTask.Name,
            //    ParentRotationTaskId = parenTask.ParentRotationTaskId.HasValue ? parenTask.ParentRotationTaskId : parenTask.Id,
            //    AssignedToId = assignedToId,
            //    IsNullReport = parenTask.IsNullReport,
            //    TemplateTaskId = null,
            //    //FinalizeStatus = parenTask.FinalizeStatus,
            //    FinalizeStatus = finalizeStatus,
            //    IsFeedbackRequired = parenTask.IsFeedbackRequired,
            //    IsPinned = parenTask.IsPinned,
            //    RelationId = parenTask.RelationId,
            //    ManagerComments = parenTask.ManagerComments,
            //    SearchTags = parenTask.SearchTags,
            //    Notes = parenTask.Notes,
            //    Attachments = new HashSet<Attachment>(parenTask.Attachments ?? new Attachment[] { }),
            //    VoiceMessages = new HashSet<VoiceMessage>(parenTask.VoiceMessages ?? new VoiceMessage[] { }),
            //    Deadline = parenTask.Deadline,
            //    IsComplete = false,
            //    Priority = parenTask.Priority
            //};

            _repositories.RotationTaskRepository.Add(newTask);

            parenTask.SuccessorTaskId = newTask.Id;


            _repositories.RotationTaskRepository.Update(parenTask);

            _repositories.Save();

            return newTask;
        }
    }
}
