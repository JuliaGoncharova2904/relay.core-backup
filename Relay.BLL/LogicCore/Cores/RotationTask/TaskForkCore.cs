﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskForkCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskForkCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }
        public RotationTask GetOrCreatePinTask(Guid topicId, RotationTask parenTask, Guid? assignedToId = null)
        {
            var childForkTask = GetForkChildTask(topicId, parenTask);

            if (childForkTask == null)
            {
                RotationTask childTask = parenTask.Clone();
                childTask.RotationTopicId = topicId;
                childTask.AssignedToId = assignedToId;
                childTask.AssignedTo = null;
                childTask.Description = null;
                childTask.IsComplete = false;
                childTask.IsFeedbackRequired = false;
                childTask.FinalizeStatus = StatusOfFinalize.NotFinalized;

                childTask.Attachments = null;

                childTask.ManagerComments = null;


                childTask.ForkParentTaskId = parenTask.ForkParentTaskId.HasValue ? parenTask.ForkParentTaskId : parenTask.Id;

                if (parenTask.ForkParentTaskId.HasValue)
                {
                    parenTask.ForkParentTask.ForkCounter = parenTask.ForkParentTask.ForkCounter + 1;

                    _repositories.RotationTaskRepository.Update(parenTask.ForkParentTask);
                }
                else
                {
                    parenTask.ForkCounter = parenTask.ForkCounter + 1;
                    _repositories.RotationTaskRepository.Update(parenTask);
                }


                _repositories.RotationTaskRepository.Add(childTask);
                _repositories.Save();

                return childTask;
            }

            return childForkTask;
        }

        private RotationTask GetOrCreateForkChildTask(Guid topicId, RotationTask parentTask)
        {
            var childTask = GetForkChildTask(topicId, parentTask) ?? AddChildForkTask(topicId, parentTask);

            return childTask;
        }



        private RotationTask GetForkChildTask(Guid rotationTopicId, RotationTask parenTask)
        {
            var childForkTask = _repositories.RotationTaskRepository.Find(task => task.RotationTopicId == rotationTopicId
                                                            && task.ForkParentTaskId.HasValue
                                                            && task.ForkParentTaskId == parenTask.ForkParentTaskId).FirstOrDefault();
            return childForkTask;
        }

        private RotationTask AddChildForkTask(Guid topicId, RotationTask parenTask)
        {
            Guid? assignedToId = null;

            if (parenTask.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue)
            {
                assignedToId = parenTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.NextRotationId.HasValue ?
                               parenTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.NextRotation.DefaultBackToBackId
                               : parenTask.AssignedToId;
            }
            else if (parenTask.RotationTopic.RotationTopicGroup.RotationModule.ShiftId.HasValue)
            {
                assignedToId = parenTask.RotationTopic.RotationTopicGroup.RotationModule.Shift.ShiftRecipientId;
            }

            RotationTask childTask = parenTask.Clone();
            childTask.AssignedToId = assignedToId;
            childTask.RotationTopicId = topicId;
            childTask.IsPinned = false;


            childTask.IsComplete = false;
            childTask.IsFeedbackRequired = false;
            childTask.FinalizeStatus = StatusOfFinalize.NotFinalized;

            childTask.ManagerComments = null;


            childTask.ForkParentTaskId = parenTask.ForkParentTaskId.HasValue ? parenTask.ForkParentTaskId : parenTask.Id;

            if (parenTask.ForkParentTaskId.HasValue)
            {
                parenTask.ForkParentTask.ForkCounter = parenTask.ForkParentTask.ForkCounter + 1;

                _repositories.RotationTaskRepository.Update(parenTask.ForkParentTask);
            }
            else
            {
                parenTask.ForkCounter = parenTask.ForkCounter + 1;
                _repositories.RotationTaskRepository.Update(parenTask);
            }

            _repositories.RotationTaskRepository.Add(childTask);
            _repositories.Save();

            return childTask;
        }


        public void CarryforwardTask(RotationTask task, bool save = true)
        {
            var taskOwner = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner;

            var currentRotation = taskOwner?.CurrentRotation;

            if (currentRotation != null)
            {
                if (!_logicCore.RotationTaskCore.GetAllRotationTasks(currentRotation.Id, TypeOfModuleSource.Draft).Any(t => t.ForkParentTaskId.HasValue
                                                                                                && t.ForkParentTaskId == task.Id
                                                                                                || t.ForkParentTaskId.HasValue
                                                                                                && t.ForkParentTaskId == task.ForkParentTaskId))
                {
                    var carryforwardTopic = _logicCore.RotationTopicCore.CarryforwardTopic(task.RotationTopic);

                    if (carryforwardTopic != null)
                    {
                        var newTask = GetOrCreateForkChildTask(carryforwardTopic.Id, task);

                        if (newTask.IsFeedbackRequired && newTask.RotationTopic.IsFeedbackRequired == false)
                        {
                            newTask.RotationTopic.IsFeedbackRequired = true;
                            _repositories.RotationTopicRepository.Update(newTask.RotationTopic);
                        }

                        if (currentRotation.StartDate.HasValue)
                        {
                            newTask.Deadline = currentRotation.StartDate.Value.AddDays(currentRotation.DayOn + currentRotation.DayOff - 1).Date;
                            _repositories.RotationTaskRepository.Update(newTask);
                        }

                        if (save)
                        {
                            _repositories.Save();
                        }
                    }
                }
            }
        }
    }
}
