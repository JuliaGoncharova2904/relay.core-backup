﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskDeleteCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskDeleteCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }


        public void RemoveRotationTaskChild(RotationTask task, bool notify = false, bool save = true)
        {
            if (!task.SuccessorTaskId.HasValue)
                return;

            RotationTask destTask = task.SuccessorTask;

            if (destTask != null)
            {
                RotationTopic destTopic = destTask.RotationTopic;

                destTopic.RotationTasks.Remove(destTask);
                _logicCore.NotificationCore.RemoveNotificationRelation(destTask.Id);
                _repositories.RotationTaskRepository.Delete(destTask);
                task.SuccessorTask = null;
                task.SuccessorTaskId = null;

                _repositories.RotationTaskRepository.Update(task);

                if (!destTopic.RotationTasks.Any() && !destTopic.ShareSourceTopicId.HasValue)
                {
                    RotationTopicGroup destTopicGroup = destTopic.RotationTopicGroup;

                    _logicCore.RotationTopicSharingRelationCore.DeleteDestinationRotationTopicSharingRelations(destTopic);


                    destTopicGroup.RotationTopics.Remove(destTopic);
                    _repositories.RotationTopicRepository.Delete(destTopic);

                    if (!destTopicGroup.RotationTopics.Any())
                    {
                        RotationModule destModule = destTopicGroup.RotationModule;
                        destModule.RotationTopicGroups.Remove(destTopicGroup);
                        _repositories.RotationTopicGroupRepository.Delete(destTopicGroup);

                        if (!destModule.RotationTopicGroups.Any())
                        {
                            _repositories.RotationModuleRepository.Delete(destModule);
                        }
                    }
                }

                //---- notification 15 ----
                if (notify)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_CreatorDeletedNowTask1(task.AssignedToId.Value, task);
                }
                //-------------------------
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Remove task
        /// </summary>
        /// <param name="task">Rotation task</param>
        /// <param name="save">Sync with database</param>
        /// <returns></returns>
        public bool RemoveRotationTask(RotationTask task, bool save = true)
        {
            if (task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.State == RotationState.Expired)
                return false;

            this.RemoveRotationTaskChild(task, true);

            this.RemoveTaskAndRelations(task, save);

            return true;
        }

        /// <summary>
        /// Remove Shift task
        /// </summary>
        /// <param name="task">Shift task</param>
        /// <param name="save">Sync with database</param>
        /// <returns></returns>
        public bool RemoveShiftTask(RotationTask task, bool save = true)
        {
            if (task.RotationTopic.RotationTopicGroup.RotationModule.Shift.Rotation.State == RotationState.Expired)
                return false;

            this.RemoveRotationTaskChild(task, true);

            this.RemoveTaskAndRelations(task);

            if (save)
                _repositories.Save();

            return true;
        }

        /// <summary>
        /// Remove task board task.
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="save">Sync with database</param>
        /// <returns></returns>
        public bool RemoveTaskBoardTask(RotationTask task, bool save = true)
        {
            if (!task.IsTaskBoardTask())
                return false;

            this.RemoveTaskAndRelations(task, save);

            return true;
        }


        public void RemoveTaskAndRelations(RotationTask task, bool save = true)
        {
            RotationTask childTask = task.SuccessorTask;

            RotationTask parentTask = task.AncestorTask;

            if (parentTask != null)
            {
                parentTask.SuccessorTask = null;
                parentTask.SuccessorTaskId = null;

                _repositories.RotationTaskRepository.Update(parentTask);

                //_logicCore.TaskBoardCore.HandoverTask(parentTask);
            }

            RotationTask forkParentTask = task.ForkParentTask;

            if (forkParentTask?.ForkCounter > 0)
            {
                --forkParentTask.ForkCounter;
                forkParentTask.ForkChildTasks.Remove(task);

                _repositories.RotationTaskRepository.Update(forkParentTask);
            }

            while (childTask != null)
            {
                _repositories.VoiceMessageRepository.Delete(childTask.VoiceMessages);
                _repositories.AttachmentRepository.Delete(childTask.Attachments);

                _logicCore.NotificationCore.RemoveNotificationRelation(childTask.Id);

                var taskToDelete = childTask;

                childTask = childTask.SuccessorTask;

                _repositories.RotationTaskRepository.Delete(taskToDelete);
            }

            _repositories.VoiceMessageRepository.Delete(task.VoiceMessages);
            _repositories.AttachmentRepository.Delete(task.Attachments);

            _logicCore.NotificationCore.RemoveNotificationRelation(task.Id);
            _repositories.RotationTaskRepository.Delete(task);

            if (save)
                _repositories.Save();
        }
    }
}
