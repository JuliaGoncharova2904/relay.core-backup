﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationTaskLogCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public RotationTaskLogCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }


        public void AddMessageToTaskLog(RotationTask task, string message , bool save = true)
        {
            RotationTaskLog taskLog = new RotationTaskLog
            {
                Id = Guid.NewGuid(),
                LogDate = DateTime.UtcNow,
                LogMessage = message,
                TaskId = task.Id,
                RootTaskId = task.ParentTaskId ?? task.Id
            };

            _repositories.TaskLogRepository.Add(taskLog);

            if (save)
            {
                _repositories.Save();
            }

        }


        public void UnsubscribeFromLog(RotationTask rotationTask)
        {
            var rotationTaskLogs = _repositories.TaskLogRepository.Find(log => log.TaskId == rotationTask.Id);

            rotationTaskLogs.ForEach(log =>
            {
                log.TaskId = null;

                _repositories.TaskLogRepository.Update(log);
            });

            _repositories.Save();
        }


        public IEnumerable<string> GetTaskLogs(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            var rootTaskId = task.ParentTaskId ?? task.Id;

            var taskLogs = _repositories.TaskLogRepository.Find(log => log.RootTaskId == rootTaskId).ToList().OrderBy(log => log.LogDate);

            return taskLogs.Select(log => log.LogMessage + " " + log.LogDate.FormatWithMonth()).ToList();
        }

    }
}
