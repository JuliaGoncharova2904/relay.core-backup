﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;
using MomentumPlus.Relay.Roles;
//using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TopicGroupCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TopicGroupCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<TemplateTopicGroup> GetModuleTopicGroups(Guid moduleId)
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ModuleId == moduleId).OrderBy(tg => tg.Name);

            return model;
        }

        public IQueryable<TemplateTopicGroup> GetBaseTopicGroups()
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => !tg.ParentTopicGroupId.HasValue && tg.Module.Type == TypeOfModule.Core 
                                                                           || !tg.ParentTopicGroupId.HasValue && tg.Module.Type == TypeOfModule.HSE).OrderBy(tg => tg.Name);
             
            return model;
        }

        public TemplateTopicGroup GetTopicGroup(Guid topicGroupId)
        {
            var topicGroup = _repositories.TemplateTopicGroupRepository.Get(topicGroupId);

            return topicGroup;
        }

        public TemplateTopicGroup GetChildTopicGroup(Guid parentTopicGroupId, Guid moduleId)
        {
            var topicGroup = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ParentTopicGroupId == parentTopicGroupId && tg.ModuleId == moduleId).FirstOrDefault();

            return topicGroup;
        }

        public TemplateTopicGroup AddChildTopicGroup(TemplateTopicGroup baseTopicGroup, Guid moduleId, bool status = false)
        {
            var topicGroup = new TemplateTopicGroup
            {
                Id = Guid.NewGuid(),
                Enabled = status,
                ParentTopicGroupId = baseTopicGroup.Id,
                ModuleId = moduleId,
                RelationId = baseTopicGroup.RelationId,
                Description = baseTopicGroup.Description,
                Name = baseTopicGroup.Name
            };

            _repositories.TemplateTopicGroupRepository.Add(topicGroup);
            _repositories.Save();

            return topicGroup;
        }

        public IQueryable<TemplateTopicGroup> GetChildTopicGroups(Guid topicGroupId)
        {
            var topicGroups = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ParentTopicGroupId == topicGroupId);

            return topicGroups;
        }

        public void UpdateChildTopicGroups(TemplateTopicGroup topicGroup)
        {
            var childTopicGroups = GetChildTopicGroups(topicGroup.Id).ToList();

            foreach (var childTopicGroup in childTopicGroups)
            {
                UpdateChildTopicGroup(topicGroup, childTopicGroup);
            }
        }

        public void UpdateChildTopicGroup(TemplateTopicGroup parentTopicGroup, TemplateTopicGroup childTopicGroup)
        {
            childTopicGroup.Description = parentTopicGroup.Description;
            childTopicGroup.Name = parentTopicGroup.Name;
            childTopicGroup.RelationId = parentTopicGroup.RelationId;
            childTopicGroup.ParentTopicGroupId = parentTopicGroup.Id;
            childTopicGroup.Id = childTopicGroup.Id;

            _repositories.TemplateTopicGroupRepository.Update(childTopicGroup);
            _repositories.Save();
        }


        /// <summary>
        /// Get Or Create Template Topic Group From Base Topic Group
        /// </summary>
        /// <param name="templateModuleId"></param>
        /// <param name="baseTopicGroup"></param>
        /// <returns></returns>

        public TemplateTopicGroup GetOrCreateTemplateTopicGroup(Guid templateModuleId, TemplateTopicGroup baseTopicGroup)
        {
            var templateTopicGroup = GetChildTopicGroup(baseTopicGroup.Id, templateModuleId) ??
                                     AddChildTopicGroup(baseTopicGroup, templateModuleId);

            return templateTopicGroup;
        }

        public IQueryable<RotationTopic> FindTopicGroupsTopics(Guid?[] topicGroupsIds, Guid[] userFilterIds = null)
        {
            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                            && tg.RotationModule.SourceType == TypeOfModuleSource.Draft
                                                                            && topicGroupsIds.Contains(tg.TempateTopicGroup.ParentTopicGroupId))
                                                                              .SelectMany(tg => tg.RotationTopics)
                .Where(t => t.Enabled  && t.RotationTopicGroup.RotationModule.RotationId.HasValue && t.RotationTopicGroup.RotationModule.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Swing
                || t.Enabled && t.RotationTopicGroup.RotationModule.ShiftId.HasValue && t.RotationTopicGroup.RotationModule.Shift.State != ShiftState.Created && t.RotationTopicGroup.RotationModule.Shift.Rotation.State != RotationState.Created);

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId));

            }

            return topics;
        }


       //--------------------------------------------------------------------------------------------------------------
        public List<RotationTopic> FindTopicGroupsTopics(string text, Guid[] userFilterIds = null)
        {
            List<RotationTopic> result = new List<RotationTopic>();

            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                            && tg.RotationModule.SourceType == TypeOfModuleSource.Draft
                                                                            && tg.Name.Contains(text)).AsNoTracking().SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).ToList();

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();

            }

            result.AddRange(FindTopics(text, userFilterIds));
            result.AddRange(FindTopicsNotesTopics(text, userFilterIds));
            result.AddRange(topics);

            return result;
        }

        public List<RotationTopic> FindRelationTopicGroupsTopics(string text, Guid[] userFilterIds = null)
        {
            List<RotationTopic> result = new List<RotationTopic>();

            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                           && tg.RotationModule.SourceType == TypeOfModuleSource.Draft).AsNoTracking()
                                                                             .SelectMany(tg => tg.RotationTopics).ToList().Where(t=>t.RelationId.HasValue).ToList().Where(t => _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName.Contains(text))
               .Where(t => t.Enabled && t.RotationTopicGroup.RotationModule.RotationId.HasValue && t.RotationTopicGroup.RotationModule.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Swing
               || t.Enabled && t.RotationTopicGroup.RotationModule.ShiftId.HasValue && t.RotationTopicGroup.RotationModule.Shift.State != ShiftState.Created && t.RotationTopicGroup.RotationModule.Shift.Rotation.State != RotationState.Created).ToList();

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();

            }

            result.AddRange(topics);

            return result;
        }

        public List<RotationTopic> FindTopics(string text, Guid[] userFilterIds = null)
        {

            var topics = _repositories.RotationTopicRepository.Find(tg => tg.Enabled && tg.RotationTopicGroup.RotationModule.SourceType == TypeOfModuleSource.Draft).AsNoTracking()
                                                                  .Where(tg => tg.Name.Contains(text)).ToList();


            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();
            }

            return topics;
        }

        public List<RotationTopic> FindTopicsNotesTopics(string text, Guid[] userFilterIds = null)
        {
            var topics = _repositories.RotationTopicRepository.Find(tg => tg.Enabled && tg.RotationTopicGroup.RotationModule.SourceType == TypeOfModuleSource.Draft).AsNoTracking()
                                                                 .Where(tg => tg.Description.Contains(text)).ToList();

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();
            }

            return topics;
        }

        public List<RotationTopic> FindMetricsTopicGroupsTopics(string text, Guid[] userFilterIds = null)
        {
            List<RotationTopic> result = new List<RotationTopic>();

            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                            && tg.RotationModule.SourceType == TypeOfModuleSource.Draft
                                                                            && tg.Name.Contains(text)).AsNoTracking().SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled).ToList().Where(t => IsPlannedActualHas(t)).ToList();

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();

            }

            result.AddRange(FindMetricsTopics(text, userFilterIds));
            result.AddRange(topics);

            return result;
        }

        public List<RotationTopic> FindRelationMetricsTopicGroupsTopics(string text, Guid[] userFilterIds = null)
        {
            List<RotationTopic> result = new List<RotationTopic>();

            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                            && tg.RotationModule.SourceType == TypeOfModuleSource.Draft).AsNoTracking()
                                                                            .SelectMany(tg => tg.RotationTopics).ToList().Where(t => t.RelationId.HasValue).ToList()
                                                                           .Where(t => _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName.Contains(text))
                .Where(t => t.Enabled && t.RotationTopicGroup.RotationModule.RotationId.HasValue && t.RotationTopicGroup.RotationModule.Rotation.State != RotationState.Created && t.RotationTopicGroup.RotationModule.Rotation.RotationType == RotationType.Swing
                || t.Enabled && t.RotationTopicGroup.RotationModule.ShiftId.HasValue && t.RotationTopicGroup.RotationModule.Shift.State != ShiftState.Created && t.RotationTopicGroup.RotationModule.Shift.Rotation.State != RotationState.Created).ToList().Where(t => IsPlannedActualHas(t)).ToList();


            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList().Where(t => IsPlannedActualHas(t)).ToList();

            }

            return result;
        }

        public List<RotationTopic> FindMetricsTopics(string text, Guid[] userFilterIds = null)
        {
            var topics = _repositories.RotationTopicRepository.Find(tg => tg.Enabled && tg.RotationTopicGroup.RotationModule.SourceType == TypeOfModuleSource.Draft
                                                                                      && tg.Name.Contains(text)).AsNoTracking().ToList()
                                                                                                .Where(t => IsPlannedActualHas(t)).ToList();
            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();

            }

            return topics;
        }

        //--------------------------------------------------------------------------------------------------------------

        public List<RotationTopic> FindSharedTopicGroupsByName(string searchText, UserProfile user, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var userRotations = user.WorkRotations;

            var topicsWhichSharedForUser = userRotations.SelectMany(r => r.RotationModules.SelectMany(m => m.RotationTopicGroups.SelectMany(tg => tg.RotationTopics))).Where(t => t.ShareSourceTopicId.HasValue).OrderByDescending(t => t.CreatedUtc).Select(t => t.ShareSourceTopic).ToList();

            if (user.WorkRotations.Count == 0 && user.WorkRotations != null)
            {
                var rotations = _logicCore.TeamRotationsCore.GetTeamRotationsForAdmin(user).ToList();

                rotations.ForEach(rotation =>
                {
                    var resultTopics = rotation.RotationType == Core.Models.RotationType.Shift ?
                      rotation.RotationShifts.SelectMany(r => r.RotationModules.SelectMany(t => t.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic)))).ToList()
                      : rotation.RotationModules.SelectMany(r => r.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic))).ToList();

                    if (resultTopics != null && resultTopics.Any())
                    {
                        topicsWhichSharedForUser.AddRange(resultTopics);
                    }
                });
                topicsWhichSharedForUser = topicsWhichSharedForUser.Distinct().ToList();
            }

            return topicsWhichSharedForUser.Where(t => t.RotationTopicGroup.Name.Contains(searchText)).ToList();
        }

        public List<RotationTopic> FindMetricsTopicGroupsByName(List<RotationTopicGroup> rotationTopics, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var topics = rotationTopics.ToList().Where(t => IsContainsSearchText(searchText, t.Name))
                                                                   .SelectMany(tg => tg.RotationTopics).ToList().Where(t => IsPlannedActualHas(t)).ToList();

            if (userId != null && isLineManager == false)
            {
               topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
               && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
                 || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
               && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList();

            }

            if (userId != null && isLineManager)
            {
                teamUsers.ForEach(teamUser =>
                {
                    topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                          teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups).ToList()
                          .Where(t => IsContainsSearchText(searchText, t.Name)).SelectMany(t => t.RotationTopics).ToList()
                          .Where(t => IsPlannedActualHas(t)).ToList()
                          : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups).ToList()
                          .Where(t => IsContainsSearchText(searchText, t.Name)).SelectMany(t => t.RotationTopics).ToList()
                           .Where(t => IsPlannedActualHas(t)).ToList());

                });
            }

            return topics;
        }

        public List<RotationTopic> FindMetricsNotesByName(string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null)
        {
            List<RotationTopic> topics = null;

            topics = _repositories.RotationTopicGroupRepository.GetAll().ToList().Where(t => t.Description != null && IsContainsSearchText(searchText, t.Description)).SelectMany(tg => tg.RotationTopics).ToList().Where(t => IsPlannedActualHas(t)).ToList();

            if (topics.Count() == 0)
            {
                topics = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(t => t.RotationTopics).ToList().Where(t => IsContainsSearchText(searchText, t.Description)).ToList().Where(t => IsPlannedActualHas(t)).ToList();
            }

            if (userId != null && !isLineManager)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList().Where(t => IsContainsSearchText(searchText, t.Description)).ToList().Where(t => IsPlannedActualHas(t)).ToList();
            }

            if (userId != null && isLineManager)
            {
                var teamUsers = _logicCore.TeamCore.GetAllTeams().FirstOrDefault().Users.Where(u => u.CurrentRotationId.HasValue).ToList();

                teamUsers.ForEach(teamUser =>
                {
                    topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                    teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups)
                    .SelectMany(t => t.RotationTopics).ToList().Where(t => IsContainsSearchText(searchText, t.Description)).ToList().Where(t => IsPlannedActualHas(t)).ToList()
                    : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups).SelectMany(t => t.RotationTopics).ToList()
                    .Where(t => IsContainsSearchText(searchText, t.Description)).ToList().Where(t => IsPlannedActualHas(t)).ToList());
                });
            }

            return topics.ToList();
        }

        public List<RotationTopic> FindReferenceByNameInMetricsTopicGroup(List<RotationTopicGroup> rotationTopics, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var topics = rotationTopics.SelectMany(t => t.RotationTopics).ToList()
                                                                   .Where(r => IsContainsSearchText(searchText, r.Name)).ToList().Where(t => IsPlannedActualHas(t)).ToList();

            if (userId != null && isLineManager == false)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList();
            }

            if (userId != null && isLineManager)
            {

                teamUsers.ForEach(teamUser =>
                {
                    topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                       teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups).ToList()
                       .SelectMany(t => t.RotationTopics).Where(r => IsContainsSearchText(searchText, r.Name)).ToList()
                        .Where(t => IsPlannedActualHas(t)).ToList()
                       : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups)
                       .SelectMany(t => t.RotationTopics).ToList().Where(r => IsContainsSearchText(searchText, r.Name)).ToList().Where(t => IsPlannedActualHas(t)).ToList());
                });
            }

            return topics;
        }

        private bool IsPlannedActualHas(RotationTopic topic)
        {
            if (topic.Actual != null && topic.Planned != null && topic.UnitsSelectedType != null && topic.Actual != "" && topic.Planned != "" && topic.UnitsSelectedType != "")
            {
                return true;
            }

            if (topic.Actual != null && topic.UnitsSelectedType != null && topic.Actual != "" && topic.UnitsSelectedType != "")
            {
                return true;
            }

            if (topic.Planned != null && topic.UnitsSelectedType != null && topic.Planned != "" && topic.UnitsSelectedType != "")
            {
                return true;
            }

            if (topic.Planned != null && topic.Actual != null && topic.Planned != "" && topic.Actual != "")
            {
                return true;
            }

            if(topic.Planned != null && topic.Planned != "")
            {
                return true;
            }

            if (topic.Actual != null && topic.Actual != "")
            {
                return true;
            }

            return false;
        }

        public List<RotationTopic> FindTopicGroupsByName(List<RotationTopicGroup> rotationTopics, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var topics = rotationTopics.Where(t => IsContainsSearchText(searchText, t.Name)).SelectMany(tg => tg.RotationTopics).ToList();

            var res = rotationTopics.Where(t => IsContainsSearchText(searchText, t.Name)).ToList();

            if (userId != null && isLineManager == false)
            {
               topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
               && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
               || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
               && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList();
            }

            if (userId != null && isLineManager)
            {
                teamUsers.ForEach(teamUser =>
                {
                    topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                        teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups).ToList()
                        .Where(t => IsContainsSearchText(searchText, t.Name)).SelectMany(t => t.RotationTopics).ToList()
                        : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups).ToList()
                        .Where(t => IsContainsSearchText(searchText, t.Name)).SelectMany(t => t.RotationTopics).ToList());
                });
            }

            return topics;
        }

        public List<RotationTopic> FindTopicGroupsMetricsByNameOfRelation(List<RotationTopicGroup> rotationTopics, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var topics = rotationTopics.SelectMany(tg => tg.RotationTopics)
                                                .Where(t => t.RelationId.HasValue).ToList()
                                                .Where(t => IsContainsSearchText(searchText, _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName))
                                                .ToList().Where(t => IsPlannedActualHas(t)).ToList();

            if (userId != null && isLineManager == false)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
                || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList();
            }

            if (userId != null && isLineManager)
            {
              teamUsers.ForEach(teamUser =>
                {
                    topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                      teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups).ToList()
                      .SelectMany(t => t.RotationTopics).Where(t => t.RelationId.HasValue).ToList()
                      .Where(t => IsContainsSearchText(searchText, _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName)).ToList().Where(t => IsPlannedActualHas(t)).ToList()
                      : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups).ToList()
                      .SelectMany(t => t.RotationTopics).Where(t => t.RelationId.HasValue).ToList()
                      .Where(t => IsContainsSearchText(searchText, _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName)).ToList().Where(t => IsPlannedActualHas(t)).ToList());
                });
            }

            return topics;
        }

        public List<RotationTopic> FindTopicGroupsByNameOfRelation(List<RotationTopicGroup> rotationTopics, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var topics = rotationTopics.SelectMany(tg => tg.RotationTopics)
                                                .Where(t => t.RelationId.HasValue).ToList()
                                                .Where(t => _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName.Contains(searchText)).ToList();

            if (userId != null && isLineManager == false)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
                || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList();
            }

            if (userId != null && isLineManager)
            {
                teamUsers.ForEach(teamUser =>
                 {
                     topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                       teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups).ToList()
                       .SelectMany(t => t.RotationTopics).Where(t => t.RelationId.HasValue).ToList()
                       .Where(t => IsContainsSearchText(searchText, _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName)).ToList()
                       : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups).ToList()
                       .SelectMany(t => t.RotationTopics).Where(t => t.RelationId.HasValue).ToList()
                       .Where(t => IsContainsSearchText(searchText, _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName)).ToList());
                 });
            }

            return topics;
        }

        public List<RotationTopic> FindSharedTopicGroupsByNameOfRelation(string searchText, UserProfile user, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var topicsWhichSharedForUser = user.WorkRotations.SelectMany(r => r.RotationModules.SelectMany(m => m.RotationTopicGroups.SelectMany(tg => tg.RotationTopics)
            .Where(t => t.RelationId.HasValue).ToList().Where(t => _logicCore.UserProfileCore.GetUserProfile(t.RelationId.Value).FullName.Contains(searchText))))
            .Where(t => t.ShareSourceTopicId.HasValue).OrderByDescending(t => t.CreatedUtc).Select(t => t.ShareSourceTopic).ToList();

            if (user.WorkRotations != null && user.WorkRotations.Count == 0 || topicsWhichSharedForUser.Any() == false)
            {
                if (teamUsers.Any())
                {
                     teamUsers.Where(u => u.Id != user.Id).ToList().ForEach(teamUser =>
                    {
                        topicsWhichSharedForUser.AddRange(teamUser.CurrentRotation.RotationType == Core.Models.RotationType.Shift ?
                           teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules.SelectMany(t => t.RotationTopicGroups.SelectMany(rt => rt.RotationTopics
                           .Where(tr => tr.RelationId.HasValue).ToList().Where(ts => _logicCore.UserProfileCore.GetUserProfile(ts.RelationId.Value).FullName.Contains(searchText))
                           .Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic)))).ToList()
                           : teamUser.CurrentRotation.RotationModules.SelectMany(r => r.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(tr => tr.RelationId.HasValue).ToList()
                           .Where(ts => _logicCore.UserProfileCore.GetUserProfile(ts.RelationId.Value).FullName.Contains(searchText)).Where(sh => sh.SharedRecipientId == user.Id)
                           .Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic))).ToList());
                    });
                }
            }

            return topicsWhichSharedForUser.Distinct().OrderByDescending(t => t.CreatedUtc).ToList();
        }

        private List<UserProfile> GetAllUsersTeam(Guid? userId, IEnumerable<string> userRoles, List<UserProfile> result = null)
        {
            result = new List<UserProfile>();

            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId.Value);
            result.Add(user);

            List<UserProfile> teamUsers = _logicCore.TeamRotationsCore.GetActiveTeamRotationsForSearch(user, userRoles, true).Select(r => r.RotationOwner).ToList();

            teamUsers.ForEach(teamUser =>
            {
                if (!result.Contains(teamUser))
                {
                    result.AddRange(GetAllUsersTeam(teamUser.Id, userRoles, result));
                }
            });

            return result;
        }

        private List<UserProfile> GetAllUsersManager(Guid? userId, IEnumerable<string> userRoles, List<UserProfile> rusult = null)
        {
            rusult = new List<UserProfile>();

            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId.Value);
            List<Rotation> rotations = _logicCore.TeamRotationsCore.GetActiveTeamRotations(user, userRoles, true).ToList();
            var teamUsers = rotations.Select(r => r.RotationOwner).ToList();

            rusult.AddRange(teamUsers);

            return rusult;
        }

        public bool IsContainsSearchText(string searchText, string nameTopic)
        {
            if (searchText != null && nameTopic != null)
            {
                if (nameTopic.Replace(" ", string.Empty).ToLower().Contains(searchText.Replace(" ", string.Empty).ToLower()))
                {
                    return true;
                }
            }
            return false;
        }

        public List<RotationTopic> FindNotesByNameInSharedTopicGroup(string searchText, UserProfile user, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {

            var userRotations = user.WorkRotations;

            var topicsWhichSharedForUser = userRotations.SelectMany(r => r.RotationModules.SelectMany(m => m.RotationTopicGroups
                                                            .SelectMany(tg => tg.RotationTopics))).ToList().Where(t => t.Description != null)
                                                             .Where(t => t.ShareSourceTopicId.HasValue).Select(t => t.ShareSourceTopic).ToList();

            if (user.WorkRotations.Count == 0 && user.WorkRotations != null)
            {
                var rotations = _logicCore.TeamRotationsCore.GetTeamRotationsForAdmin(user).ToList();

                rotations.ForEach(rotation =>
                {
                    var resultTopics = rotation.RotationType == Core.Models.RotationType.Shift ?
                      rotation.RotationShifts.SelectMany(r => r.RotationModules.SelectMany(t => t.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic)))).ToList()
                      : rotation.RotationModules.SelectMany(r => r.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic))).ToList();

                    if (resultTopics != null && resultTopics.Any())
                    {
                        topicsWhichSharedForUser.AddRange(resultTopics);
                    }
                });

                topicsWhichSharedForUser = topicsWhichSharedForUser.Distinct().ToList();
            }

            return topicsWhichSharedForUser.Where(t => t.ParentTopicId.HasValue ? t.ParentTopic.Description.Contains(searchText) : t.Description.Contains(searchText)).ToList();
        }

        public List<RotationTopic> FindReferenceByNameInSharedTopicGroup(string searchText, UserProfile user, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var userRotations = user.WorkRotations;

            var topicsWhichSharedForUser = userRotations.SelectMany(r => r.RotationModules.SelectMany(m => m.RotationTopicGroups.SelectMany(tg => tg.RotationTopics)))
                .Where(t => t.ShareSourceTopicId.HasValue).OrderByDescending(t => t.CreatedUtc).Select(t => t.ShareSourceTopic).ToList();

            if (user.WorkRotations.Count == 0 && user.WorkRotations != null)
            {
                var rotations = _logicCore.TeamRotationsCore.GetTeamRotationsForAdmin(user).ToList();

                rotations.ForEach(rotation =>
                {
                    var resultTopics = rotation.RotationType == Core.Models.RotationType.Shift ?
                      rotation.RotationShifts.SelectMany(r => r.RotationModules.SelectMany(t => t.RotationTopicGroups
                      .SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue)
                      .Select(sht => sht.ShareSourceTopic)))).ToList()
                      : rotation.RotationModules.SelectMany(r => r.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id)
                      .Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic))).ToList();

                    if (resultTopics != null && resultTopics.Any())
                    {
                        topicsWhichSharedForUser.AddRange(resultTopics);
                    }
                });
                topicsWhichSharedForUser = topicsWhichSharedForUser.Distinct().ToList();
            }

            return topicsWhichSharedForUser.Where(t => t.Name.Contains(searchText)).ToList();
        }

        public List<RotationTopic> FindReferenceByNameInTopicGroup(List<RotationTopicGroup> rotationTopics, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            var topics = rotationTopics.SelectMany(t => t.RotationTopics).ToList().Where(r => IsContainsSearchText(searchText, r.Name)).ToList();

            if (userId != null && isLineManager == false)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList().Where(t => IsContainsSearchText(searchText, t.Name)).ToList();
            }

            if (userId != null && isLineManager)
            {
                teamUsers.ForEach(teamUser =>
                {
                    topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                       teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups).ToList()
                       .SelectMany(t => t.RotationTopics).Where(r => IsContainsSearchText(searchText, r.Name)).ToList()
                       : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups)
                       .SelectMany(t => t.RotationTopics).ToList().Where(r => IsContainsSearchText(searchText, r.Name)).ToList());
                });
            }

            return topics;
        }

        public List<RotationTopic> FindNotesByNameInTopicGroup(List<RotationTopicGroup> rotationTopics, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            List<RotationTopic> topics = new List<RotationTopic>();

            topics = rotationTopics.ToList().Where(t => t.Description != null && IsContainsSearchText(searchText, t.Description)).SelectMany(tg => tg.RotationTopics).ToList();

            if (topics.Any() == false)
            {
                topics = rotationTopics.SelectMany(t => t.RotationTopics).ToList().Where(t => IsContainsSearchText(searchText, t.Description)).ToList();
            }

            if (userId != null && isLineManager == false)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId.Value
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId.Value).ToList().Where(t => IsContainsSearchText(searchText, t.Description)).ToList();
            }

            if (userId != null && isLineManager)
            {
                teamUsers.ForEach(teamUser =>
                {
                    topics.AddRange(teamUser.CurrentRotation.RotationType == RotationType.Shift ?
                      teamUser.CurrentRotation.RotationShifts.SelectMany(r => r.RotationModules).SelectMany(r => r.RotationTopicGroups).SelectMany(t => t.RotationTopics).ToList()
                       .Where(t => IsContainsSearchText(searchText, t.Description)).ToList()
                      : teamUser.CurrentRotation.RotationModules.SelectMany(t => t.RotationTopicGroups).SelectMany(t => t.RotationTopics).ToList()
                      .Where(t => IsContainsSearchText(searchText, t.Description)).ToList());
                });
            }

            return topics;
        }

        public RotationTopic FindTopicGroupByName(string searchText)
        {
            var topic = _repositories.RotationTopicGroupRepository.Find(t => t.Name == searchText).SelectMany(tg => tg.RotationTopics).FirstOrDefault();

            return topic;
        }

        public RotationTopic FindTagsByName(string searchText)
        {
            List<RotationTopic> topics = null;

            if (searchText != null)
            {
                var topicHastags = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(tg => tg.RotationTopics).Where(t => t.SearchTags != null).ToList();

                topics = topicHastags.Where(t=> IsContainsSearchTag(t.SearchTags, searchText)).ToList();

                return topics.FirstOrDefault();
            }
            return null;
        }

        public RotationTask FindTaskByName(string searchText)
        {
            List<RotationTask> tasks = null;

            if (searchText != null)
            {
                tasks = _repositories.RotationTopicGroupRepository.GetAll().SelectMany(t => t.RotationTopics.SelectMany(ts => ts.RotationTasks.Where(tn => tn.Name == searchText))).ToList();

                return tasks.FirstOrDefault();
            }
            return null;
        }

        public List<RotationTask> FindTasksByName(List<RotationTask> tasks, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            if (searchText != null)
            {
                tasks = tasks.Where(t => t.Name.Contains(searchText)).ToList();

                if (tasks != null && !tasks.Any())
                {
                    tasks = tasks.Where(t => t.Name.Contains(searchText)).SelectMany(t => t.ChildTasks).ToList();
                }
                else if (tasks != null && !tasks.Any())
                {
                    tasks = tasks.Where(t => t.Name.Contains(searchText)).ToList();
                }

                if (userId != null && isLineManager == false)
                {
                    tasks = tasks.Where(t => t.TaskBoard.User.Id == userId).SelectMany(t => t.TaskBoard.Tasks).ToList().Where(t => t.Name.Contains(searchText)).Distinct().ToList();
                }

                if (userId != null && isLineManager)
                {
                   teamUsers.ForEach(teamUser =>
                    {
                        tasks.AddRange(_repositories.RotationTaskRepository.Find(t => t.TaskBoardId.HasValue && t.TaskBoardId == teamUser.Id).AsNoTracking().ToList()
                            .Where(t => t.Name.Contains(searchText)).ToList());
                    });
                }
            }

            return tasks;
        }

        public List<RotationTask> FindTasksByDescription(List<RotationTask> tasks, string searchText, Guid? userId = null, bool isLineManager = false, IEnumerable<string> userRoles = null, List<UserProfile> teamUsers = null)
        {
            if (searchText != null)
            {
                tasks = tasks.Where(t => t.Description != null).Where(t => t.Description.ToLower().Contains(searchText.ToLower())).ToList();

                if (userId != null && isLineManager == false)
                {
                    tasks = tasks.Where(t => t.TaskBoardId.HasValue && t.TaskBoardId == userId.Value).Where(t => t.Description != null).Where(t => t.Description.ToLower().Contains(searchText.ToLower())).ToList();
                }

                if (userId != null && isLineManager)
                {
                    teamUsers.ForEach(teamUser =>
                    {
                        tasks.AddRange(tasks.Where(t => t.TaskBoardId.HasValue && t.TaskBoardId == teamUser.Id).ToList()
                                                    .Where(t => t.Description != null).Where(t => t.Description.ToLower().Contains(searchText.ToLower())).ToList());
                    });
                }
            }

            return tasks;
        }

        public List<RotationTopic> FindTopicsByTagName(string searchText, Guid[] userFilterIds = null)
        {
            var topics = _repositories.RotationTopicGroupRepository.Find(tg => tg.Enabled
                                                                           && tg.RotationModule.SourceType == TypeOfModuleSource.Draft).AsNoTracking().SelectMany(tg => tg.RotationTopics)
                                                                           .Where(t => t.Enabled).ToList().Where(t => IsContainsSearchTag(t.SearchTags, searchText)).ToList();

            if (userFilterIds != null)
            {
                topics = topics.Where(t => t.RotationTopicGroup.RotationModule.RotationId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId)
                  || t.RotationTopicGroup.RotationModule.ShiftId.HasValue
                && userFilterIds.Contains(t.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)).ToList();
            }

            return topics;
        }

        private bool IsContainsSearchTag(string searchTags, string search)
        {
            bool result = false;

            if (search != null && searchTags != null)
            {
                searchTags.Split(',').ToList().ForEach(tag =>
                {
                    if (tag.Replace(" ", string.Empty).ToLower().Contains(search.ToLower()))
                    {
                        result = true;
                    }
                });
            }
            return result;
        }

    }
}