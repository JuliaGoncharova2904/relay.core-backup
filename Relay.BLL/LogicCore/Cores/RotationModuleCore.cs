﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Data.Entity.Core;
using System.Data.Entity;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationModuleCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public RotationModuleCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Return Rotation module by Id
        /// </summary>
        /// <param name="moduleId">Rotation module Id</param>
        /// <returns></returns>
        public RotationModule GetModule(Guid moduleId)
        {
            var module = _repositories.RotationModuleRepository.Get(moduleId);

            if (module == null)
                throw new ObjectNotFoundException(string.Format("RotationModule with ID: {0} was not found.", moduleId));

            return module;
        }

        /// <summary>
        /// Return Rotation modules
        /// </summary>
        /// <param name="rotationId">Source Rotation Id</param>
        /// <param name="sourceType">Type of modules source</param>
        /// <returns></returns>
        public IQueryable<RotationModule> GetRotationModules(Guid rotationId, TypeOfModuleSource sourceType)
        {
            IQueryable<RotationModule> modules = _repositories.RotationModuleRepository
                                                                .Find(m => m.RotationId == rotationId &&
                                                                            m.SourceType == sourceType);

            return modules;
        }

        /// <summary>
        /// Return modules
        /// </summary>
        /// <param name="shiftId">Source Shift Id<</param>
        /// <param name="sourceType">Type of modules source</param>
        /// <returns></returns>
        public IQueryable<RotationModule> GetShiftModules(Guid shiftId, TypeOfModuleSource sourceType)
        {
            IQueryable<RotationModule> modules = _repositories.RotationModuleRepository
                                                                .Find(m => m.ShiftId == shiftId &&
                                                                            m.SourceType == sourceType);

            return modules;
        }

        /// <summary>
        /// Get active rotations modules by template module Id
        /// </summary>
        /// <param name="templateModuleId">Parent Template module Id</param>
        /// <returns></returns>
        public IQueryable<RotationModule> GetActiveRotationsModulesByTemplate(Guid templateModuleId)
        {
            IQueryable<Rotation> activeRotations = _logicCore.RotationCore.GetAllActiveRotations();

            IQueryable<RotationModule> activeModules = activeRotations.SelectMany(r => r.RotationModules)
                                                                    .Where(rm => rm.TempateModuleId == templateModuleId);

            return activeModules;
        }


        /// <summary>
        /// Get active shifts modules by template module Id
        /// </summary>
        /// <param name="templateModuleId">Parent Template module Id</param>
        /// <returns></returns>
        public IQueryable<RotationModule> GetActiveShiftsModulesByTemplate(Guid templateModuleId)
        {
            IQueryable<Shift> activeShifts = _logicCore.ShiftCore.GetAllActiveShifts();

            IQueryable<RotationModule> activeModules = activeShifts.SelectMany(r => r.RotationModules)
                                                                    .Where(rm => rm.TempateModuleId == templateModuleId);

            return activeModules;
        }

        /// <summary>
        /// Get rotation module by type and rotation Id.
        /// </summary>
        /// <param name="type">Module type</param>
        /// <param name="rotationId">Rotation Id</param>
        /// <param name="sourceType">Module source type</param>
        /// <returns>RotationModule</returns>
        public RotationModule GetRotationModule(TypeOfModule type, Guid rotationId, TypeOfModuleSource sourceType)
        {
            RotationModule module = _repositories.RotationModuleRepository.Find(m => m.RotationId == rotationId &&
                                                                                    m.Type == type && !m.DeletedUtc.HasValue &&
                                                                                    m.SourceType == sourceType).AsNoTracking().FirstOrDefault();

            return module;
        }

        /// <summary>
        /// Get rotation module by type and shift Id.
        /// </summary>
        /// <param name="type">Module type</param>
        /// <param name="shiftId">Shift Id</param>
        /// <param name="sourceType">Module source type</param>
        /// <returns>RotationModule</returns>
        public RotationModule GetShiftModule(TypeOfModule type, Guid shiftId, TypeOfModuleSource sourceType)
        {
            RotationModule module = _repositories.RotationModuleRepository.Find(m => m.ShiftId == shiftId && m.Enabled
                                                                                  && m.Type == type && !m.DeletedUtc.HasValue
                                                                                  && m.SourceType == sourceType).AsQueryable().AsNoTracking().FirstOrDefault();

            return module;
        }

        public List<RotationModule> GetRotationModule(Guid rotationId, TypeOfModuleSource sourceType)
        {
            var modules = _repositories.RotationModuleRepository.Find(m => m.RotationId == rotationId && !m.DeletedUtc.HasValue &&
                                                                                    m.SourceType == sourceType).ToList();

            return modules;
        }

        public List<RotationTopic> GetTopicsByRelationId(Guid relationId, Guid rotationTopicGroupId)
        {
            var topics = _repositories.RotationTopicRepository.Find(tg => tg.RelationId.HasValue && tg.RelationId.Value == relationId && tg.RotationTopicGroupId == rotationTopicGroupId).ToList();

            return topics;
        }

        public List<RotationModule> GetShiftModule(Guid shiftId, TypeOfModuleSource sourceType)
        {
            var modules = _repositories.RotationModuleRepository.Find(m => m.ShiftId == shiftId && m.Enabled && !m.DeletedUtc.HasValue
                                                                                  && m.SourceType == sourceType).ToList();

            return modules;
        }

        /// <summary>
        /// Add rotation module
        /// </summary>
        /// <param name="rotationId">Target rotation Id</param>
        /// <param name="templateModule">Source template</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule AddRotationModule(Guid rotationId, TemplateModule templateModule, bool save = true)
        {
            var module = new RotationModule
            {
                Id = Guid.NewGuid(),
                SourceType = TypeOfModuleSource.Draft,
                Enabled = templateModule.Enabled,
                Type = templateModule.Type,
                RotationId = rotationId,
                TempateModuleId = templateModule.Id,
                RotationTopicGroups = null
            };


            _repositories.RotationModuleRepository.Add(module);
            _logicCore.RotationTopicGroupCore.AddOtherRotationTopicGroup(module.Id);

            if (save)
                _repositories.Save();

            return module;
        }

        /// <summary>
        /// Add shift module
        /// </summary>
        /// <param name="shiftId">Target shift Id</param>
        /// <param name="type">Type of module</param>
        /// <param name="sourceType">Type of module source</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule AddShiftModule(Guid shiftId, TemplateModule templateModule, bool save = true)
        {
            var module = new RotationModule
            {
                Id = Guid.NewGuid(),
                SourceType = TypeOfModuleSource.Draft,
                Enabled = templateModule.Enabled,
                Type = templateModule.Type,
                ShiftId = shiftId,
                TempateModuleId = templateModule.Id,
                RotationTopicGroups = null
            };


            _repositories.RotationModuleRepository.Add(module);
            _logicCore.RotationTopicGroupCore.AddOtherRotationTopicGroup(module.Id);

            if (save)
                _repositories.Save();

            return module;
        }


        /// <summary>
        /// Add rotation module
        /// </summary>
        /// <param name="rotationId">Target rotation Id</param>
        /// <param name="type">Type of module</param>
        /// <param name="sourceType">Type of module source</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule AddRotationModule(Guid rotationId, TypeOfModule type, TypeOfModuleSource sourceType, bool save = true)
        {
            var module = new RotationModule
            {
                Id = Guid.NewGuid(),
                SourceType = sourceType,
                Enabled = true,
                Type = type,
                RotationId = rotationId,
                TempateModuleId = null,
                RotationTopicGroups = null
            };

            _repositories.RotationModuleRepository.Add(module);

            if (sourceType == TypeOfModuleSource.Draft)
                _logicCore.RotationTopicGroupCore.AddOtherRotationTopicGroup(module.Id);

            if (save)
                _repositories.Save();

            return module;
        }


        /// <summary>
        /// Add shift module
        /// </summary>
        /// <param name="shiftId">Target shift Id</param>
        /// <param name="type">Type of module</param>
        /// <param name="sourceType">Type of module source</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule AddShiftModule(Guid shiftId, TypeOfModule type, TypeOfModuleSource sourceType, bool save = true)
        {
            var module = new RotationModule
            {
                Id = Guid.NewGuid(),
                SourceType = sourceType,
                Enabled = true,
                Type = type,
                ShiftId = shiftId,
                TempateModuleId = null,
                RotationTopicGroups = null
            };

            _repositories.RotationModuleRepository.Add(module);

            if (sourceType == TypeOfModuleSource.Draft)
                _logicCore.RotationTopicGroupCore.AddOtherRotationTopicGroup(module.Id);

            if (save)
                _repositories.Save();

            return module;
        }

        /// <summary>
        /// Get or create rotation module
        /// </summary>
        /// <param name="rotationId">Target rotation Id</param>
        /// <param name="templateModule">Source template</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule GetOrCreateRotationModule(Guid rotationId, TemplateModule templateModule, bool save = true)
        {
            var rotationModule = this.GetRotationModule(templateModule.Type, rotationId, TypeOfModuleSource.Draft) ??
                                 this.AddRotationModule(rotationId, templateModule, save);

            return rotationModule;
        }

        /// <summary>
        /// Get or create shift module
        /// </summary>
        /// <param name="shiftId">Target shift Id</param>
        /// <param name="templateModule">Source template</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule GetOrCreateShiftModule(Guid shiftId, TemplateModule templateModule, bool save = true)
        {
            var shiftModule = this.GetShiftModule(templateModule.Type, shiftId, TypeOfModuleSource.Draft) ??
                                 this.AddShiftModule(shiftId, templateModule, save);

            return shiftModule;
        }

        /// <summary>
        /// Get or create rotation module
        /// </summary>
        /// <param name="rotationId">Target rotation Id</param>
        /// <param name="type">Type of module</param>
        /// <param name="sourceType">Type of module source</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule GetOrCreateRotationModule(Guid rotationId, TypeOfModule type, TypeOfModuleSource sourceType, bool save = true)
        {
            var rotationModule = this.GetRotationModule(type, rotationId, sourceType) ??
                                 this.AddRotationModule(rotationId, type, sourceType, save);

            return rotationModule;
        }

        /// <summary>
        /// Get or create shift module
        /// </summary>
        /// <param name="shiftId">Target shift Id</param>
        /// <param name="type">Type of module</param>
        /// <param name="sourceType">Type of module source</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public RotationModule GetOrCreateShiftModule(Guid shiftId, TypeOfModule type, TypeOfModuleSource sourceType, bool save = true)
        {
            var rotationModule = this.GetShiftModule(type, shiftId, sourceType) ??
                                 this.AddShiftModule(shiftId, type, sourceType, save);

            return rotationModule;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="templateModule"></param>
        public void PopulateRotationsModules(TemplateModule templateModule)
        {
            if (templateModule.TemplateId != null)
            {
                var rotations = _logicCore.RotationCore.GetTemplateRotations((Guid)templateModule.TemplateId).ToList();

                foreach (var rotation in rotations)
                {
                    this.GetOrCreateRotationModule(rotation.Id, templateModule, false);
                }

                _logicCore.SyncWithDatabase();
            }
        }

    }
}
