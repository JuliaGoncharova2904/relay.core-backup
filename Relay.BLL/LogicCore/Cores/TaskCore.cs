﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;


namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public TemplateTask GetTask(Guid taskId)
        {
            var task = _repositories.TemplateTaskRepository.Get(taskId);

            return task;
        }

        public IQueryable<TemplateTask> GetTopicTasks(Guid topicId)
        {
            var tasks = _repositories.TemplateTaskRepository.Find(t => t.TopicId == topicId).OrderBy(t => t.Name);

            return tasks;
        }

        public TemplateTask GetChildTaskFromModule(Guid parentTaskId, Guid templateModuleId)
        {
            var task = _repositories.TemplateTaskRepository.Find(t => t.ParentTaskId == parentTaskId && t.Topic.TopicGroup.Module.Id == templateModuleId && !t.DeletedUtc.HasValue).FirstOrDefault();

            return task;
        }

        public TemplateTask GetChildTaskFromTopicGroup(Guid parentTaskId, Guid templateTopicGroupId)
        {
            var task = _repositories.TemplateTaskRepository.Find(t => t.ParentTaskId == parentTaskId && t.Topic.TopicGroup.Id == templateTopicGroupId && !t.DeletedUtc.HasValue).FirstOrDefault();

            return task;
        }

        public TemplateTask GetChildTaskFromTopic(Guid parentTaskId, Guid templateTopicId)
        {
            var task = _repositories.TemplateTaskRepository.Find(t => t.ParentTaskId == parentTaskId && t.Topic.Id == templateTopicId && !t.DeletedUtc.HasValue).FirstOrDefault();

            return task;
        }

        public TemplateTask AddChildTask(TemplateTask baseTask, Guid templateTopicId, bool status = false)
        {
            var task = new TemplateTask
            {
                Id = Guid.NewGuid(),
                Enabled = status,
                Description = baseTask.Description,
                Name = baseTask.Name,
                ParentTaskId = baseTask.Id,
                TopicId = templateTopicId,
                Priority = baseTask.Priority
            };

            _repositories.TemplateTaskRepository.Add(task);
            _repositories.Save();

            return task;
        }


        /// <summary>
        /// Get Or Create Template Task From Base Task
        /// </summary>
        /// <param name="templateTopicId"></param>
        /// <param name="baseTask"></param>
        /// <returns></returns>

        public TemplateTask GetOrCreateTemplateTopicTask(Guid templateTopicId, TemplateTask baseTask)
        {
            var templateTask = GetChildTaskFromTopic(baseTask.Id, templateTopicId) ??
                                      AddChildTask(baseTask, templateTopicId);

            return templateTask;
        }

        public IQueryable<TemplateTask> GetChildTasks(Guid taskId)
        {
            var tasks = _repositories.TemplateTaskRepository.Find(tg => tg.ParentTaskId == taskId);

            return tasks;
        }

        public void UpdateChildTasks(TemplateTask task)
        {
            var childTasks = GetChildTasks(task.Id).ToList();

            foreach (var childTask in childTasks)
            {
                UpdateChildTask(task, childTask);
            }
        }

        public void UpdateChildTask(TemplateTask parentTask, TemplateTask childTask)
        {
            childTask.Description = parentTask.Description;
            childTask.Name = parentTask.Name;
            childTask.ParentTaskId = parentTask.Id;
            childTask.Priority = parentTask.Priority;
            childTask.Id = childTask.Id;
            childTask.TopicId = childTask.TopicId;

            _repositories.TemplateTaskRepository.Update(childTask);
            _repositories.Save();
        }

    }
}