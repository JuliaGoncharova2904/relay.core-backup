﻿using MomentumPlus.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using NLog;
using System.Data.Entity;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class ShiftCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;
        private static readonly Logger HandoverLogger = LogManager.GetLogger("handover-logger");

        public ShiftCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Return shift by Id
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public Shift GetShift(Guid shiftId)
        {
            Shift shift = _repositories.ShiftRepository.Get(shiftId);

            if (shift == null)
                throw new ObjectNotFoundException(string.Format("Shift with Id: {0} was not found.", shiftId));

            return shift;
        }

        /// <summary>
        /// Return shifts by IDs
        /// </summary>
        /// <param name="shiftsIds">Shift IDs</param>
        /// <returns></returns>
        public IQueryable<Shift> GetShiftsByIds(IEnumerable<Guid> shiftsIds)
        {
            return _repositories.ShiftRepository.Find(s => shiftsIds.Contains(s.Id));
        }

        /// <summary>
        /// Return shifts with specific state
        /// </summary>
        /// <param name="state">Shift state</param>
        /// <returns></returns>
        public IQueryable<Shift> GetShiftsByState(ShiftState state)
        {
            return _repositories.ShiftRepository.GetAll().Where(s => s.State == state);
        }

        /// <summary>
        /// Auto finalize sll topics and tasks
        /// </summary>
        /// <param name="shift">Shift entity</param>
        /// <param name="save">Sinc with database</param>
        public void AutoFinalizeAll(Shift shift, bool save = true)
        {
            IQueryable<RotationModule> draftModules = _logicCore.RotationModuleCore.GetShiftModules(shift.Id, TypeOfModuleSource.Draft)
                                                                                    .Where(rm => rm.Enabled);

            IEnumerable<RotationTopic> draftTopics = draftModules.SelectMany(rm => rm.RotationTopicGroups)
                                                                    .Where(rtg => rtg.Enabled)
                                                                    .SelectMany(rtg => rtg.RotationTopics)
                                                                    .Where(rt => rt.Enabled)
                                                                    .ToList();

            IEnumerable<RotationTask> draftTasks = draftModules.SelectMany(rm => rm.RotationTopicGroups)
                                                                    .Where(rtg => rtg.Enabled)
                                                                    .SelectMany(rtg => rtg.RotationTopics)
                                                                    .Where(rt => rt.Enabled)
                                                                    .SelectMany(rt => rt.RotationTasks)
                                                                    .Where(t => t.Enabled)
                                                                    .ToList();

            foreach (RotationTopic draftTopic in draftTopics)
            {
                if (draftTopic.FinalizeStatus == StatusOfFinalize.NotFinalized)
                {
                    draftTopic.FinalizeStatus = StatusOfFinalize.AutoFinalized;
                    _repositories.RotationTopicRepository.Update(draftTopic);
                }
            }

            foreach (RotationTask draftTask in draftTasks)
            {
                if (draftTask.FinalizeStatus == StatusOfFinalize.NotFinalized)
                {
                    draftTask.FinalizeStatus = StatusOfFinalize.AutoFinalized;
                    _repositories.RotationTaskRepository.Update(draftTask);
                }
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Set shift state
        /// </summary>
        /// <param name="shift">Shift entity</param>
        /// <param name="state">Shift state</param>
        /// <param name="save">Sinc with database</param>
        public void SetShiftState(Shift shift, ShiftState state, bool save = true)
        {
            shift.State = state;
            _repositories.ShiftRepository.Update(shift);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Confirmation of next shift
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="backDateTime"></param>
        /// <param name="repeatTimes"></param>
        /// <param name="save">Sinc with database</param>
        public void ConfirmShift(Guid userId, DateTime startDateTime, DateTime endDateTime, DateTime backDateTime, int repeatTimes, bool save = true)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (!user.CurrentRotationId.HasValue)
                throw new NullReferenceException(string.Format("User with Id: {0} don't have rotation.", userId));

            if (user.CurrentRotation.RotationType != RotationType.Shift)
                throw new Exception(string.Format("Rotation with Id:{0} is not 'Shift' type.", user.CurrentRotationId.Value));

            if (user.CurrentRotation.RotationShifts.Any(s => s.State == ShiftState.Confirmed))
                throw new Exception(string.Format("Rotation with Id:{0} already has active shift.", user.CurrentRotationId.Value));

            //-------- finish previous shift ---------
            Shift prevNotFinishedShift = this.GetCurrentRotationShift(user.CurrentRotation);

            this.FinishShift(prevNotFinishedShift);
            //----------------------------------------

            Shift shift = this.GetOrCreateNextShift(user.CurrentRotation, false);

            shift.StartDateTime = startDateTime;
            shift.WorkMinutes = (int)(endDateTime - startDateTime).TotalMinutes;
            shift.BreakMinutes = (int)(backDateTime - endDateTime).TotalMinutes;
            shift.RepeatTimes = repeatTimes;
            shift.State = ShiftState.Confirmed;

            if (save)
                _repositories.Save();

            _logicCore.ProjectCore.InitProjectModuleTopicGroups(user);

            if (shift.ShiftRecipientId.HasValue)
            {
                this.InitShiftItemsFromTemplate(shift);
            }
        }


        /// <summary>
        /// Confirmation of next shift
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startDateTime"></param>
        /// <param name="endDateTime"></param>
        /// <param name="save">Sinc with database</param>
        public void ConfirmShiftV2(Guid userId, DateTime startDateTime, DateTime endDateTime, Guid identityUserId, Guid? adminSubscriptionId, bool save = true)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (!user.CurrentRotationId.HasValue)
                throw new NullReferenceException(string.Format("User with Id: {0} don't have rotation.", userId));

            if (user.CurrentRotation.RotationType != RotationType.Shift)
                throw new Exception(string.Format("Rotation with Id:{0} is not 'Shift' type.", user.CurrentRotationId.Value));

            if (user.CurrentRotation.RotationShifts.Any(s => s.State == ShiftState.Confirmed))
                throw new Exception(string.Format("Rotation with Id:{0} already has active shift.", user.CurrentRotationId.Value));

            //-------- finish previous shift ---------
            Shift prevNotFinishedShift = this.GetCurrentRotationShift(user.CurrentRotation);

            this.FinishShift(prevNotFinishedShift);
            //----------------------------------------

            Shift shift = this.GetOrCreateNextShift(user.CurrentRotation, false);


            DateTime floorStartDateTime = startDateTime.Floor(new TimeSpan(0,5,0));

            shift.StartDateTime = floorStartDateTime;
            shift.WorkMinutes = (int)(endDateTime - floorStartDateTime).TotalMinutes;
            shift.BreakMinutes = 1;
            shift.RepeatTimes = 0;
            shift.State = ShiftState.Confirmed;

            if (save)
                _repositories.Save();

            if (adminSubscriptionId.HasValue && identityUserId == shift.Rotation.RotationOwnerId)
            {
                _logicCore.NotificationCore.NotificationTrigger.Send_CreateOwnHandover(adminSubscriptionId.Value, shift.Rotation.RotationOwner.FullName, shift.Rotation.RotationOwnerId);
            }

            _logicCore.ProjectCore.InitProjectModuleTopicGroups(user);

            if (shift.ShiftRecipientId.HasValue)
            {
                this.InitShiftItemsFromTemplate(shift);
            }
        }

        public void InitPinItems(Shift newShift)
        {
            Shift previousShift = newShift.Rotation.RotationShifts.OrderBy(s => s.CreatedUtc).LastOrDefault(s => s.State == ShiftState.Finished);

            if (previousShift != null)
            {
                var pinnedTopicGroupsInPrevShift = _logicCore.RotationTopicGroupCore.GetShiftTopicGroupsWhereIsPinned(previousShift.Id).Where(tg => tg.Enabled).ToList();

                var currentShiftTopicGroups = _logicCore.RotationTopicGroupCore.GetAllShiftTopicGroups(newShift.Id, TypeOfModuleSource.Draft).Where(tg => tg.Enabled).ToList();

                pinnedTopicGroupsInPrevShift.ForEach(pinnedTopicGroupInPrevShift =>
                {
                    //For Template Topic Group

                    if (pinnedTopicGroupInPrevShift.TempateTopicGroupId.HasValue)
                    {
                        var topicGroupFromTemplateInCurrentShift = currentShiftTopicGroups.FirstOrDefault(tg => tg.TempateTopicGroupId.HasValue
                                                && tg.TempateTopicGroupId == pinnedTopicGroupInPrevShift.TempateTopicGroupId);

                        if (topicGroupFromTemplateInCurrentShift != null)
                        {
                            pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.TempateTopicGroupId != null 
                            && t.RotationTopicGroup.TempateTopicGroupId == topicGroupFromTemplateInCurrentShift.TempateTopicGroupId).ToList()
                            .ForEach(rotationTopic => 
                            {
                                var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupFromTemplateInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                                {
                                    var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                                }

                            });

                            //foreach (var rotationTopic in pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.TempateTopicGroupId != null && t.RotationTopicGroup.TempateTopicGroupId == topicGroupFromTemplateInCurrentShift.TempateTopicGroupId).ToList())
                            //{
                            //    var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupFromTemplateInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                            //    foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                            //    {
                            //        var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                            //    }
                            //}
                        }
                    }


                    // For Relation

                    else if (pinnedTopicGroupInPrevShift.RelationId.HasValue)
                    {
                        var topicGroupRelationInCurrentShift = currentShiftTopicGroups.FirstOrDefault(tg => tg.RelationId == pinnedTopicGroupInPrevShift.RelationId && tg.RelationId.HasValue);

                        if (topicGroupRelationInCurrentShift != null)
                        {

                            pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.Enabled).ToList().ForEach(rotationTopic => 
                            {
                                var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupRelationInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                                {
                                    var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                                }
                            });

                            //foreach (var rotationTopic in pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.Enabled).ToList())
                            //{
                            //    var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupRelationInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                            //    foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                            //    {
                            //        var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                            //    }
                            //}
                        }
                    }

                    //For Other 

                    else if (pinnedTopicGroupInPrevShift.Name.Equals("Other"))
                    {
                        var topicGroupOtherInCurrentShift = currentShiftTopicGroups.FirstOrDefault(tg => tg.RotationModule.Type == pinnedTopicGroupInPrevShift.RotationModule.Type &&
                        tg.Name.Equals("Other"));

                        if (topicGroupOtherInCurrentShift != null)
                        {

                            pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList().ForEach(rotationTopic => 
                            {
                                var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupOtherInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                                {
                                    var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                                }
                            });

                            //foreach (var rotationTopic in pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                            //{
                            //    var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupOtherInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                            //    foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                            //    {
                            //        var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                            //    }
                            //}
                        }
                    }
                });



                //foreach (var pinnedTopicGroupInPrevShift in pinnedTopicGroupsInPrevShift)
                //{
                //    //For Template Topic Group

                //    if (pinnedTopicGroupInPrevShift.TempateTopicGroupId.HasValue)
                //    {
                //        var topicGroupFromTemplateInCurrentShift = currentShiftTopicGroups.FirstOrDefault(tg => tg.TempateTopicGroupId.HasValue
                //                                && tg.TempateTopicGroupId == pinnedTopicGroupInPrevShift.TempateTopicGroupId);

                //        if (topicGroupFromTemplateInCurrentShift != null)
                //        {
                //            foreach (var rotationTopic in pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.TempateTopicGroupId != null && t.RotationTopicGroup.TempateTopicGroupId == topicGroupFromTemplateInCurrentShift.TempateTopicGroupId).ToList())
                //            {
                //                var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupFromTemplateInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                //                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                //                {
                //                    var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                //                }
                //            }
                //        }
                //    }


                //    // For Relation

                //    else if (pinnedTopicGroupInPrevShift.RelationId.HasValue)
                //    {
                //        var topicGroupRelationInCurrentShift = currentShiftTopicGroups.FirstOrDefault(tg => tg.RelationId == pinnedTopicGroupInPrevShift.RelationId && tg.RelationId.HasValue);

                //        if (topicGroupRelationInCurrentShift != null)
                //        {

                //            foreach (var rotationTopic in pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.Enabled).ToList())
                //            {
                //                var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupRelationInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                //                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                //                {
                //                    var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                //                }
                //            }
                //        }
                //    }

                //    //For Other 

                //    else if (pinnedTopicGroupInPrevShift.Name.Equals("Other"))
                //    {
                //        var topicGroupOtherInCurrentShift = currentShiftTopicGroups.FirstOrDefault(tg => tg.RotationModule.Type == pinnedTopicGroupInPrevShift.RotationModule.Type &&
                //        tg.Name.Equals("Other"));

                //        if (topicGroupOtherInCurrentShift != null)
                //        {

                //            foreach (var rotationTopic in pinnedTopicGroupInPrevShift.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                //            {
                //                var shiftPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupOtherInCurrentShift.Id, rotationTopic, newShift.ShiftRecipientId);

                //                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                //                {
                //                    var shiftPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(shiftPinTopic.Id, rotationTask, newShift.ShiftRecipientId);
                //                }
                //            }
                //        }
                //    }

                //}

                _repositories.Save();

            }
        }



        public void UpdateShiftRecipient(Guid shiftId, Guid shiftRecipientId, bool save = true)
        {
            Shift shift = this.GetShift(shiftId);

            bool isFirstRecipientSetUp = !shift.ShiftRecipientId.HasValue;

            if (shift.ShiftRecipientId != shiftRecipientId)
            {
                var oldRecipientId = shift.ShiftRecipientId;

                shift.ShiftRecipientId = shiftRecipientId;

                if (oldRecipientId != null)
                    this.UpdateShiftItemsAssignee(shift, oldRecipientId.Value, shiftRecipientId, false);

                _repositories.ShiftRepository.Update(shift);

                if (save)
                    _repositories.Save();

                if (isFirstRecipientSetUp)
                {
                    this.InitShiftItemsFromTemplate(shift);
                    InitPinItems(shift);
                }
            }
        }

        /// <summary>
        /// Update shift items Assignee
        /// </summary>

        private void UpdateShiftItemsAssignee(Shift shift, Guid oldRecipientId, Guid newRecipientId, bool save = true)
        {
            var shiftTopics = shift.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft)
                                   .SelectMany(m => m.RotationTopicGroups)
                                   .SelectMany(tg => tg.RotationTopics)
                                   .Where(topic => topic.AssignedToId.HasValue
                                                && topic.AssignedToId == oldRecipientId);


            var shiftTasks = shift.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft)
                                  .SelectMany(m => m.RotationTopicGroups)
                                  .SelectMany(tg => tg.RotationTopics)
                                  .SelectMany(t => t.RotationTasks)
                                  .Where(task => task.AssignedToId.HasValue
                                              && task.AssignedToId == oldRecipientId);

            foreach (var shiftTopic in shiftTopics)
            {
                shiftTopic.AssignedToId = newRecipientId;
                _repositories.RotationTopicRepository.Update(shiftTopic);
            }

            foreach (var shiftTask in shiftTasks)
            {
                shiftTask.AssignedToId = newRecipientId;
                _repositories.RotationTaskRepository.Update(shiftTask);
            }

            if (save)
            {
                _repositories.Save();
            }

        }

        /// <summary>
        /// Update shift dates
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <param name="startDateTime">Start shift date and time</param>
        /// <param name="endDateTime">End shift date and time</param>
        /// <param name="nextDateTime">Next shift date and time</param>
        /// <param name="repeatTimes">Repeat times for shift</param>
        /// <param name="save">Sinc with database</param>
        public void UpdateShiftDates(Guid shiftId, DateTime startDateTime, DateTime endDateTime, DateTime nextDateTime, int repeatTimes, bool save = true)
        {
            Shift shift = this.GetShift(shiftId);

            if (shift.State != ShiftState.Created)
            {
                shift.StartDateTime = startDateTime;
                shift.WorkMinutes = (int)(endDateTime - startDateTime).TotalMinutes;
                shift.BreakMinutes = (int)(nextDateTime - endDateTime).TotalMinutes;
                shift.RepeatTimes = repeatTimes;

                if (save)
                    _repositories.Save();

                return;
            }

            throw new Exception(string.Format("Shift with Id: {0} is not confirmed.", shiftId));
        }




        public void UpdateShiftEndDate(Guid shiftId, DateTime endShiftTime, bool save = true)
        {
            Shift shift = this.GetShift(shiftId);

            if (shift.State != ShiftState.Created)
            {
               shift.WorkMinutes = (int)(endShiftTime - shift.StartDateTime.Value.Floor(new TimeSpan(0, 5, 0))).TotalMinutes;

                if (save)
                    _repositories.Save();

                return;
            }

            throw new Exception(string.Format("Shift with Id: {0} is not confirmed.", shiftId));
        }

        /// <summary>
        /// Init Shift Items From Template
        /// </summary>
        /// <param name="shift">Shift where need init items</param>
        private void InitShiftItemsFromTemplate(Shift shift)
        {
            var template = _logicCore.TemplateCore.GetShiftTemplate(shift.Id);

            if (template.Modules == null || template.Modules.Count == 0 || template.Modules.Count < 4)
            {
                _logicCore.ModuleCore.InitTemplateModules(template.Id);
            }


            Shift previousShift = shift.Rotation.RotationShifts.OrderBy(s => s.CreatedUtc).LastOrDefault(s => s.State == ShiftState.Finished);

            template.Modules.ToList().ForEach(templateModule =>
            {
                var shiftModule = _logicCore.RotationModuleCore.GetOrCreateShiftModule(shift.Id, templateModule);

                if (templateModule.TopicGroups != null && templateModule.TopicGroups.Any())
                {
                    templateModule.TopicGroups.ToList().ForEach(templateTopicGroup =>
                    {
                        var shiftTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateRotationTopicGroup(shiftModule.Id, templateTopicGroup);

                        templateTopicGroup.Topics.Where(t => !t.DeletedUtc.HasValue).ToList().ForEach(templateTopic =>
                        {
                            IEnumerable<RotationTopicSharingRelation> topicShareRelations = null;

                            if (previousShift != null)
                            {
                                topicShareRelations = previousShift.RotationModules.SelectMany(t => t.RotationTopicGroups).SelectMany(t => t.RotationTopics).Where(t => t.TempateTopicId.HasValue).Where(t => t.TempateTopicId == templateTopic.Id).SelectMany(t => t.TopicSharingRelations);
                            }

                            var shiftTopic = _logicCore.RotationTopicCore.GetOrCreateRotationTopic(shiftTopicGroup.Id, templateTopic, topicShareRelations);

                              foreach (var templateTask in templateTopic.Tasks)
                              {
                                  _logicCore.RotationTaskCore.GetOrCreateRotationTask(shiftTopic.Id, templateTask);
                              }
                        });

                    });
                }
            });


            /*  foreach (var templateModule in template.Modules)
              {
                  var shiftModule = _logicCore.RotationModuleCore.GetOrCreateShiftModule(shift.Id, templateModule);

                  if (templateModule.TopicGroups != null && templateModule.TopicGroups.Any())
                  {
                      foreach (var templateTopicGroup in templateModule.TopicGroups)
                      {
                          var shiftTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateRotationTopicGroup(shiftModule.Id, templateTopicGroup);

                          foreach (var templateTopic in templateTopicGroup.Topics)
                          {
                              var shiftTopic = _logicCore.RotationTopicCore.GetOrCreateRotationTopic(shiftTopicGroup.Id, templateTopic, shift.Id);

                              foreach (var templateTask in templateTopic.Tasks)
                              {
                                  _logicCore.RotationTaskCore.GetOrCreateRotationTask(shiftTopic.Id, templateTask);
                              }
                          }
                      }
                  }
              }*/
        }

        /// <summary>
        /// Finish shift
        /// </summary>
        /// <param name="shift">Shift entity</param>
        /// <param name="save">Sinc with database</param>
        public void FinishShift(Shift shift, bool save = true)
        {
            if (shift != null && shift.State == ShiftState.Break)
            {
                DateTime currentDateTime = _logicCore.AdministrationCore.GetCurrentServerTime(shift.Rotation.RotationOwnerId); ;

                shift.BreakMinutes = (int)(currentDateTime - shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes)).TotalMinutes;
                shift.RepeatTimes = 0;
                shift.State = ShiftState.Finished;

                _repositories.ShiftRepository.Update(shift);

                if (save)
                    _repositories.Save();
            }
        }

        /// <summary>
        /// Return active shift for rotation
        /// </summary>
        /// <param name="rotation">Rotation entity</param>
        /// <returns></returns>
        public Shift GetActiveRotationShift(Rotation rotation)
        {
            if (rotation.RotationType != RotationType.Shift)
                throw new Exception(string.Format("Working Pattern with Id:{0} is not Shift type.", rotation.Id));

            if (rotation.RotationShifts.Count(s => s.State == ShiftState.Confirmed) > 1)
                throw new Exception(string.Format("Working Pattern with Id:{0} has more then one active shifts.", rotation.Id));

            Shift currentShift = rotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Confirmed);

            return currentShift;
        }


        /// <summary>
        /// Return all active shifts
        /// </summary>
        public IQueryable<Shift> GetAllActiveShifts()
        {
            IQueryable<Shift> shifts = _repositories.ShiftRepository.Find(shift => shift.State == ShiftState.Confirmed);

            return shifts;
        }


        /// <summary>
        /// Return current shift for rotation
        /// </summary>
        /// <param name="rotation">Rotation entity</param>
        /// <returns></returns>
        public Shift GetCurrentRotationShift(Rotation rotation)
        {
            if (rotation.RotationType != RotationType.Shift)
                throw new Exception(string.Format("Working Pattern with Id:{0} is not Shift type.", rotation.Id));

            if (rotation.RotationShifts.Count(s => s.State == ShiftState.Confirmed || s.State == ShiftState.Break) > 1)
                throw new Exception(string.Format("Working Pattern with Id:{0} has more then one current shifts.", rotation.Id));

            Shift currentShift = rotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Confirmed || s.State == ShiftState.Break);

            return currentShift;
        }

        public Shift GetCurrentRotationShiftByRotationId(Rotation rotation)
        {
            var shift = _repositories.ShiftRepository.Find(t => t.RotationId == rotation.Id).FirstOrDefault();

            return shift;
        }

        /// <summary>
        /// Rotation Has Working Shift
        /// </summary>
        /// <param name="rotation">Rotation entity</param>
        /// <returns></returns>
        public bool RotationHasWorkingShift(Rotation rotation)
        {
            if (rotation.RotationType != RotationType.Shift)
                throw new Exception(string.Format("Rotation with Id:{0} is not Shift type.", rotation.Id));

            if (rotation.RotationShifts.Count(s => s.State == ShiftState.Confirmed || s.State == ShiftState.Break) > 1)
                throw new Exception(string.Format("Rotation with Id:{0} has more then one current shifts.", rotation.Id));

            return rotation.RotationShifts.Any(s => s.State == ShiftState.Confirmed);
        }

        /// <summary>
        /// Return next not confirmed shift
        /// </summary>
        /// <param name="rotation">Source rotation entity for shift</param>
        /// <param name="save">Sinc with database</param>
        /// <returns></returns>
        public Shift GetOrCreateNextShift(Rotation rotation, bool save = true)
        {
            Shift lastShift = rotation.RotationShifts.ToList()
                                                    .OrderBy(s => s.CreatedUtc.Value)
                                                    .LastOrDefault();

            if (lastShift == null && rotation.PrevRotationId.HasValue && rotation.PrevRotation.RotationType == RotationType.Shift)
            {
                lastShift = rotation.PrevRotation.RotationShifts.Where(s => s.State == ShiftState.Created).FirstOrDefault();

                if (lastShift != null)
                {
                    lastShift.RotationId = rotation.Id;
                    _repositories.ShiftRepository.Update(lastShift);
                    _repositories.Save();
                }
            }

            if (lastShift == null || lastShift.State != ShiftState.Created)
            {
                Shift nextShift = new Shift();
                nextShift.Id = Guid.NewGuid();
                //------ Init fields --------------
                nextShift.State = ShiftState.Created;
                nextShift.RotationId = rotation.Id;
                nextShift.Rotation = rotation;
                if (lastShift != null && lastShift.RepeatTimes > 0)
                {
                    nextShift.StartDateTime = lastShift.StartDateTime.Value.AddMinutes(lastShift.WorkMinutes + lastShift.BreakMinutes);
                    nextShift.WorkMinutes = lastShift.WorkMinutes;
                    nextShift.BreakMinutes = lastShift.BreakMinutes;
                    nextShift.RepeatTimes = lastShift.RepeatTimes - 1;
                }
                rotation.RotationShifts.Add(nextShift);
                _repositories.ShiftRepository.Add(nextShift);
                _repositories.RotationRepository.Update(rotation);
                //---------------------------------
                if (save)
                    _repositories.Save();

                return nextShift;
                //---------------------------------
            }

            return lastShift;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceShift"></param>
        /// <param name="destShift"></param>
        /// <param name="save">Sinc with database</param>
        public void SetPopulatedShift(Shift sourceShift, Shift destShift, bool save = true)
        {
            sourceShift.HandoverToShifts.Add(destShift);
            _repositories.ShiftRepository.Update(sourceShift);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Check: is source shift handovered to destination shift
        /// </summary>
        /// <param name="sourceShift">Source shift</param>
        /// <param name="destShift">Destination shift</param>
        /// <returns></returns>
        public bool IsPopulated(Shift sourceShift, Shift destShift)
        {
            return sourceShift.HandoverToShifts.Select(s => s.Id).Contains(destShift.Id);
        }

        /// <summary>
        /// Change finalize Status for Draft topics And tasks in shift
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <param name="status">Finalize status</param>
        public void SetFinalizeStatusForDraftItems(Guid shiftId, StatusOfFinalize status)
        {
            List<RotationTopic> rotationTopics = _logicCore.RotationModuleCore.GetShiftModules(shiftId, TypeOfModuleSource.Draft)
                                                                            .SelectMany(m => m.RotationTopicGroups)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .ToList();

            foreach (RotationTopic rotationTopic in rotationTopics)
            {
                _logicCore.RotationTopicCore.ChangeFinalizeStatusFoRotationTopic(rotationTopic, status, false);

                foreach (RotationTask rotationTask in rotationTopic.RotationTasks.ToList())
                {
                    _logicCore.RotationTaskCore.ChangeFinalizeStatusFoRotationTask(rotationTask, status, false);
                }
            }

            _repositories.Save();
        }

        #region Handover mechanism

        /// <summary>
        /// Handover shift report
        /// </summary>
        /// <param name="shift">Source shift</param>
        public void HandoverReportFromShift(Shift shift)
        {
            List<Guid> recipientsIds = this.GetRecipientsForShift(shift);

            foreach (Guid recipientId in recipientsIds)
            {
                this.HandoverReportFromShiftToRecipient(shift, recipientId);
            }

        }

        /// <summary>
        /// Extract recipients from rotation
        /// </summary>
        /// <param name="shift">Source shift</param>
        /// <returns></returns>
        private List<Guid> GetRecipientsForShift(Shift shift)
        {
            List<Guid> recipients = new List<Guid>();

            List<Guid?> recipientsFoShiftTopics = shift.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Draft && rm.Enabled)
                                                             .SelectMany(rm => rm.RotationTopicGroups.Where(rtg => rtg.Enabled))
                                                             .SelectMany(rtg => rtg.RotationTopics.Where(rt => rt.Enabled))
                                                             .Select(rt => rt.AssignedToId)
                                                             .ToList();

            List<Guid?> recipientsFoShiftTasks = shift.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Draft && rm.Enabled)
                                                             .SelectMany(rm => rm.RotationTopicGroups.Where(rtg => rtg.Enabled))
                                                             .SelectMany(rtg => rtg.RotationTopics.Where(rt => rt.Enabled))
                                                             .SelectMany(rt => rt.RotationTasks.Where(rta => rta.Enabled))
                                                             .Select(rta => rta.AssignedToId)
                                                             .ToList();

            recipientsFoShiftTopics.RemoveAll(item => item == null);
            recipientsFoShiftTasks.RemoveAll(item => item == null);

            recipients.AddRange(recipientsFoShiftTopics.Select(item => item.Value));
            recipients.AddRange(recipientsFoShiftTasks.Select(item => item.Value));

            if (shift.ShiftRecipientId != null)
            {
                recipients.Add(shift.ShiftRecipientId.Value);
            }

            return recipients.Distinct().ToList();
        }

        /// <summary>
        /// Handover shift report to recipient
        /// </summary>
        /// <param name="shift">Source shift</param>
        /// <param name="recipientId">Recipient Id</param>
        private void HandoverReportFromShiftToRecipient(Shift shift, Guid recipientId)
        {
            Shift destShift = this.GetDestinationShift(recipientId);

            if (destShift != null && !this.IsPopulated(shift, destShift))
            {

                HandoverLogger.Info("System send Handover (Draft Report) from {0}, Email: {1} , ShiftId: {2} . Recipient is {3}, email: {4} , ShiftId: {5}",
                                     shift.Rotation.RotationOwner.FullName,
                                     shift.Rotation.RotationOwner.Email,
                                     shift.Id,
                                     destShift.Rotation.RotationOwner.FullName,
                                     destShift.Rotation.RotationOwner.Email,
                                     destShift.Id);

                IEnumerable<RotationModule> sourceModules = _logicCore.RotationModuleCore.GetShiftModules(shift.Id, TypeOfModuleSource.Draft)
                                                                            .Where(rm => rm.Enabled)
                                                                            .ToList();

                foreach (RotationModule sourceModule in sourceModules.ToList())
                {
                    RotationModule destModule = _logicCore.RotationModuleCore.GetOrCreateShiftModule(destShift.Id, sourceModule.Type, TypeOfModuleSource.Received);

                    this.HandoverModule(sourceModule, destModule, recipientId);
                }

                this.SetPopulatedShift(shift, destShift);
            }
        }

        /// <summary>
        /// Get destination shift for recipient
        /// </summary>
        /// <param name="recipientId">Recipient Id</param>
        /// <returns></returns>
        private Shift GetDestinationShift(Guid recipientId)
        {
            UserProfile recipient = _logicCore.UserProfileCore.GetUserProfile(recipientId);

            if (!recipient.DeletedUtc.HasValue
                && recipient.CurrentRotationId.HasValue
                && recipient.CurrentRotation.RotationType == RotationType.Shift)
            {
                Rotation rotation = recipient.CurrentRotation;
                Shift shift = this.GetActiveRotationShift(rotation);

                return shift ?? this.GetOrCreateNextShift(rotation);
            }

            return null;
        }

        /// <summary>
        /// Handove rotation module
        /// </summary>
        /// <param name="sourceModule">Source module</param>
        /// <param name="destModule">Destination module</param>
        /// <param name="recipientId">Recipient Id</param>
        private void HandoverModule(RotationModule sourceModule, RotationModule destModule, Guid recipientId)
        {
            IEnumerable<RotationTopicGroup> sourceTopicGrups = sourceModule.RotationTopicGroups.Where(tg => tg.Enabled);

            foreach (RotationTopicGroup sourceTopicGroup in sourceTopicGrups)
            {
                var sourceTopics = sourceTopicGroup.RotationTopics.Where(t => t.Enabled && t.AssignedToId == recipientId).ToList();

                var sourceTasks = sourceTopicGroup.RotationTopics.Where(t => t.Enabled).SelectMany(t => t.RotationTasks)
                                                                                 .Where(t => t.Enabled && t.AssignedToId == recipientId).ToList();

                if (!sourceTopics.Any() && !sourceTasks.Any())
                    continue;


                RotationTopicGroup destTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateChildTopicGroup(destModule.Id, sourceTopicGroup);


                sourceTasks.ForEach(sourceTask =>
                {
                    RotationTopic destTopic = _logicCore.RotationTopicCore.GetOrCreateChildTopic(destTopicGroup.Id,
                                                                                                    sourceTask.RotationTopic,
                                                                                                    StatusOfFinalize.AutoFinalized);

                    _logicCore.RotationTaskCore.GetOrCreateChildTask(destTopic.Id, sourceTask, StatusOfFinalize.AutoFinalized);
                });


                sourceTopics.ForEach(sourceTopic =>
                {
                    RotationTopic destTopic = _logicCore.RotationTopicCore.GetOrCreateChildTopic(destTopicGroup.Id,
                                                                                                    sourceTopic,
                                                                                                    StatusOfFinalize.AutoFinalized);
                    if (destTopic.IsFeedbackRequired)
                    {
                        _logicCore.RotationTaskCore.GetOrCreateHandBackTask(destTopic);
                    }
                });
            }

        }

        #endregion


        #region End Shift functionality

        public void EndShift(Shift shift)
        {
            if (shift.State == ShiftState.Confirmed)
            {
                DateTime currentTime = _logicCore.AdministrationCore.GetCurrentServerTime(shift.Rotation.RotationOwnerId);

                shift.WorkMinutes = (currentTime - shift.StartDateTime.Value).Minutes;
                shift.BreakMinutes = 1;
                shift.RepeatTimes = 0;


                _logicCore.ShiftCore.AutoFinalizeAll(shift, false);

                _logicCore.ShiftCore.HandoverReportFromShift(shift);

                _logicCore.ShiftCore.SetShiftState(shift, ShiftState.Finished, false);

                _logicCore.SyncWithDatabase();
            }

            if (shift.State == ShiftState.Break)
            {
                _logicCore.ShiftCore.AutoFinalizeAll(shift, false);

                _logicCore.ShiftCore.SetShiftState(shift, ShiftState.Finished, false);

                _logicCore.SyncWithDatabase();
            }

            _logicCore.NotificationCore.NotificationTrigger.Send_TeamMemberHandedOver(shift.Rotation.LineManagerId,
                                                                                  shift.Rotation.RotationOwner.FullName,
                                                                                  shift.ShiftRecipient.FullName,
                                                                                  shift.Rotation.RotationOwnerId);

            _logicCore.NotificationCore.NotificationTrigger.Send_ShiftRecipientSendReport(shift.ShiftRecipient.Id,
                                                                                   shift.Rotation.RotationOwner.FullName,
                                                                                   shift.Rotation.RotationOwnerId, shift.Id);

            var contributorUsers = shift.Rotation.RotationOwner.Contributors;

            if (contributorUsers != null && contributorUsers.Any())
            {
                foreach (var contributorUser in contributorUsers)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_ShiftRecipientSendReport(contributorUser.Id,
                                                                                shift.Rotation.RotationOwner.FullName,
                                                                                shift.Rotation.RotationOwnerId, shift.Id);
                }
            }

        }


        #endregion
    }
}
