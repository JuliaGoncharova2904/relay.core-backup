﻿using MomentumPlus.Core.Interfaces;
using System;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using File = MomentumPlus.Core.Models.File;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class MediaCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public MediaCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public File AddImage(string title, string fileName, string contentType, Stream inputStream, bool save = true)
        {
            MemoryStream memoryStream = new MemoryStream();
            inputStream.CopyTo(memoryStream);

            var httpContext = HttpContext.Current;
            httpContext.Response.AddHeader("content-disposition", "attachment; filename=\"" + title + Path.GetExtension(fileName));

            File image = new File();

            image.Id = Guid.NewGuid();
            image.Title = title;
            image.BinaryData = memoryStream.ToArray();
            image.FileType = Path.GetExtension(fileName);
            image.ContentType = contentType;

            _repositories.FileRepository.Add(image);

            if (save)
                _repositories.Save();

            return image;
        }

        public void RemoveImage(Guid imageId, File fileAvatar = null)
        {
            _repositories.FileRepository.Delete(imageId);
            if (fileAvatar != null)
            {
                _repositories.FileRepository.Update(fileAvatar);
            }
            //_repositories.Save();
        }

        public File AddFile(string name, string fileName, string contentType, Stream inputStream, bool save = true)
        {
            MemoryStream memoryStream = new MemoryStream();
            inputStream.CopyTo(memoryStream);

            //var httpContext = HttpContext.Current;
            //httpContext.Response.AddHeader("content-disposition", "attachment; filename=\"" + name + Path.GetExtension(fileName));


            var httpContext = HttpContext.Current;
            //httpContext.Response.Clear();
            //httpContext.Response.AddHeader("content-disposition", "attachment; filename=\"" + name + Path.GetExtension(fileName));
            //httpContext.Response.ContentType = "application/octet-stream";
            //httpContext.Response.Flush();
            
            httpContext.Response.Clear();
            httpContext.Response.AddHeader("Content-Disposition", "inline; filename=" + String.Concat(name, Path.GetExtension(fileName)));
            httpContext.Response.ContentType = contentType;

            //httpContext.Response.Flush();

            File file = new File();

            file.Id = Guid.NewGuid();
            file.BinaryData = memoryStream.ToArray();
            file.FileType = Path.GetExtension(fileName);
            file.ContentType = contentType;
            file.Title = name;


            _repositories.FileRepository.Add(file);

            if (save)
                _repositories.Save();

            return file;
        }
    }
}