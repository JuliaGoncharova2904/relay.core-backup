﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TeamRotationsCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TeamRotationsCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Return rotations of users team.
        /// </summary>
        /// <param name="user">User entity</param>
        /// <param name="isViewMode">Is view mode flag</param>
        /// <returns></returns>
        public IQueryable<Rotation> GetActiveTeamRotations(UserProfile user, IEnumerable<string> userRoles, bool isViewMode)
        {
            bool userIsManager = userRoles.Contains(iHandoverRoles.Relay.LineManager)
                                  || userRoles.Contains(iHandoverRoles.Relay.HeadLineManager)
                                  || userRoles.Contains(iHandoverRoles.Relay.SafetyManager);

            bool userIsAdmin = userRoles.Contains(iHandoverRoles.Relay.Administrator)
                               || userRoles.Contains(iHandoverRoles.Relay.iHandoverAdmin) || userRoles.Contains(iHandoverRoles.Relay.ExecutiveUser);

            IQueryable<Rotation> rotations = _logicCore.RotationCore.GetAllActiveRotations()
                                                                        .Where(r => r.RotationOwnerId != user.Id
                                                                                    && !r.RotationOwner.DeletedUtc.HasValue);
            if (userIsManager || (isViewMode && userIsAdmin))
            {
                rotations = rotations.Where(r => r.LineManagerId == user.Id);
            }
            else if (!userIsAdmin)
            {
                rotations = null;
            }

            return rotations;
        }


        public IQueryable<Rotation> GetActiveTeamRotationsForSearch(UserProfile user, IEnumerable<string> userRoles, bool isViewMode)
        {
            bool userIsManager = userRoles.Contains(iHandoverRoles.Relay.LineManager)
                                  || userRoles.Contains(iHandoverRoles.Relay.HeadLineManager)
                                  || userRoles.Contains(iHandoverRoles.Relay.SafetyManager);

            bool userIsAdmin = userRoles.Contains(iHandoverRoles.Relay.Administrator)
                               || userRoles.Contains(iHandoverRoles.Relay.iHandoverAdmin) || userRoles.Contains(iHandoverRoles.Relay.ExecutiveUser);

            IQueryable<Rotation> rotations = _logicCore.RotationCore.GetAllActiveRotations()
                                                                        .Where(r => !r.RotationOwner.DeletedUtc.HasValue);
            if (userIsManager || (isViewMode && userIsAdmin))
            {
                rotations = rotations.Where(r => r.LineManagerId == user.Id);
            }
            else if (!userIsAdmin)
            {
                rotations = null;
            }
            
            return rotations;
        }

        public IQueryable<Rotation> GetTeamRotationsForAdmin(UserProfile user)
        {
            return _logicCore.UserProfileCore.GetRelayEmployees()
                                                 .Where(c => c.CurrentRotationId != null)
                                                 .Select(e => e.CurrentRotation);
        }

    }
}
