﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class AdministrationCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public AdministrationCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }


        /// <summary>
        /// Get admin settings
        /// </summary>
        /// <returns> The <see cref="rAdminSettings"/> object. </returns>
        public AdminSettings GetAdminSettings()
        {
            var adminSettings = _repositories.AdminSettingsRepository.GetAll().FirstOrDefault();

            return adminSettings;
        }

        /// <summary>
        /// Return handover trigger time
        /// </summary>
        /// <returns> Handover Trigger Time</returns>
        public DateTime GetHandoverTriggerTime()
        {
            var adminSettings = GetAdminSettings();

            var handoverTriggerTime = string.IsNullOrEmpty(adminSettings.HandoverTriggerTime)
                  ? "11:00pm"
                : adminSettings.HandoverTriggerTime;

            var triggerTime = DateTime.ParseExact(handoverTriggerTime, "hh:mmtt", CultureInfo.InvariantCulture);

            TimeZoneInfo timezoneFromSettings = TimeZoneInfo.FindSystemTimeZoneById(adminSettings.TimeZoneId);

            DateTime triggerTimeWithTimezone = TimeZoneInfo.ConvertTime(triggerTime, timezoneFromSettings);

            return triggerTimeWithTimezone;
        }



        public DateTime GetHandoverTriggerTime(Guid userId)
        {
            var adminSettings = GetAdminSettings();

            var handoverTriggerTime = string.IsNullOrEmpty(adminSettings.HandoverTriggerTime)
                 ? "11:00pm"
                : adminSettings.HandoverTriggerTime;


            var triggerTime = DateTime.ParseExact(handoverTriggerTime, "hh:mmtt", CultureInfo.InvariantCulture);

            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            var workplace = user.Position.Workplace;

            DateTime triggerTimeWithTimezone;

            if (workplace.TimeZoneId != null)
            {
                TimeZoneInfo workplaceTimezone = TimeZoneInfo.FindSystemTimeZoneById(workplace.TimeZoneId);

                triggerTimeWithTimezone = TimeZoneInfo.ConvertTime(triggerTime, workplaceTimezone);
            }
            else
            {
                TimeZoneInfo timezoneFromSettings = TimeZoneInfo.FindSystemTimeZoneById(adminSettings.TimeZoneId);

                triggerTimeWithTimezone = TimeZoneInfo.ConvertTime(triggerTime, timezoneFromSettings);
            }


            return triggerTimeWithTimezone;
        }

        /// <summary>
        /// Get company logo image Id
        /// </summary>
        /// <returns>Company Logo Guid</returns>
        public Guid? GetCompanyLogoImageId()
        {
            var adminSettings = GetAdminSettings();

            return adminSettings.LogoId;
        }



        /// <summary>
        /// Return handover preview time
        /// </summary>
        /// <returns> Handover Preview Time</returns>
        public DateTime? GetHandoverPreviewTime()
        {
            DateTime? triggerTime = null;
            AdminSettings adminSettings = GetAdminSettings();

            if (adminSettings != null)
            {
                if (!string.IsNullOrEmpty(adminSettings.HandoverPreviewTime))
                {
                    triggerTime = DateTime.ParseExact(adminSettings.HandoverPreviewTime, "hh:mmtt", CultureInfo.InvariantCulture);

                    TimeZoneInfo timezoneFromSettings = TimeZoneInfo.FindSystemTimeZoneById(adminSettings.TimeZoneId);

                    triggerTime = TimeZoneInfo.ConvertTime(triggerTime.Value, timezoneFromSettings);
                }

                return triggerTime;
            }
            return DateTime.Now;
        }




        /// <summary>
        /// Return handover preview time by User
        /// </summary>
        /// <returns> Handover Preview Time</returns>
        public DateTime? GetHandoverPreviewTime(Guid userId)
        {
            DateTime? triggerTime = null;
            AdminSettings adminSettings = GetAdminSettings();
            if (adminSettings != null)
            {
                if (!string.IsNullOrEmpty(adminSettings.HandoverPreviewTime))
                {
                    var user = _logicCore.UserProfileCore.GetUserProfile(userId);

                    var workplace = user.Position.Workplace;

                    triggerTime = DateTime.ParseExact(adminSettings.HandoverPreviewTime, "hh:mmtt", CultureInfo.InvariantCulture);

                    if (workplace.TimeZoneId != null)
                    {
                        TimeZoneInfo workplaceTimezone = TimeZoneInfo.FindSystemTimeZoneById(workplace.TimeZoneId);

                        triggerTime = TimeZoneInfo.ConvertTime(triggerTime.Value, workplaceTimezone);
                    }
                    else
                    {
                        TimeZoneInfo timezoneFromSettings = TimeZoneInfo.FindSystemTimeZoneById(adminSettings.TimeZoneId);

                        triggerTime = TimeZoneInfo.ConvertTime(triggerTime.Value, timezoneFromSettings);
                    }

                }

                return triggerTime;
            }
            return DateTime.Now;
        }

        public DateTime GetCurrentServerTime(Guid userId)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                var user = _logicCore.UserProfileCore.GetUserProfile(userId);

                var workplace = user.Position.Workplace;

                DateTime currentTime = DateTime.Now;

                if (workplace.TimeZoneId != null)
                {
                    TimeZoneInfo workplaceTimezone = TimeZoneInfo.FindSystemTimeZoneById(workplace.TimeZoneId);

                    currentTime = TimeZoneInfo.ConvertTime(currentTime, workplaceTimezone);
                }
                else
                {
                    TimeZoneInfo timezoneFromSettings = TimeZoneInfo.FindSystemTimeZoneById(adminSettings.TimeZoneId);

                    currentTime = TimeZoneInfo.ConvertTime(currentTime, timezoneFromSettings);
                }

                return currentTime;
            }

            return DateTime.Now;
        }


        public TimeZoneInfo GetCurrentServerTimeZone(Guid userId)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            var workplace = user.Position.Workplace;

            TimeZoneInfo timezone = TimeZoneInfo.Local;

            if (adminSettings != null || workplace.TimeZoneId != null)
            {
                timezone = TimeZoneInfo.FindSystemTimeZoneById(workplace.TimeZoneId ?? adminSettings.TimeZoneId);
            }

            return timezone;
        }



    }
}
