﻿using MomentumPlus.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Roles;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class UserProfileCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public UserProfileCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<UserProfile> GetRelayEmployees()
        {
            return _repositories.UserProfileRepository.GetAll().Where(u => !u.DeletedUtc.HasValue);
        }

        public IQueryable<UserProfile> GetEmployeesWithoutRotation(IQueryable<UserProfile> employees)
        {
            return employees.Where(c => c.CurrentRotationId == null).Where(u => !u.DeletedUtc.HasValue); ;
        }

        public IQueryable<UserProfile> GetEmployeesByTeam(Guid teamId)
        {
            return this.GetRelayEmployees().Where(e => e.TeamId == teamId);
        }

        public IQueryable<UserProfile> GetEmployeesByIds(IEnumerable<Guid> employeesIds)
        {
            return this.GetRelayEmployees().Where(e => employeesIds.Contains(e.Id));
        }

        public UserProfile GetUserProfile(Guid userId, bool isIdentetyAdmin = false)
        {
            UserProfile userProfile = _repositories.UserProfileRepository.Get(userId);

            if (isIdentetyAdmin && userProfile == null)
            {
                var template = new Template
                {
                    Id = DbInitializerConstants.Templates.iHandoverAdminTemplateId,
                    Name = "iHandover Admin",
                    Description = "Template for iHandover Admin"
                };

                if (_repositories.TemplateRepository.Get(template.Id) == null)
                {
                    _repositories.TemplateRepository.Add(template);
                    _repositories.Save();
                }

                var workPlace = new Workplace
                {
                    Id = DbInitializerConstants.Workplaces.iHandoverWorkplaceId,
                    Name = "iHandover Workplace"
                };

                if (_repositories.WorkplaceRepository.Get(workPlace.Id) == null)
                {
                    _repositories.WorkplaceRepository.Add(workPlace);
                    _repositories.Save();
                }

                var position = new Position
                {
                    Id = DbInitializerConstants.Positions.iHandoverAdminPositionId,
                    Name = "iHandover Admin",
                    WorkplaceId = DbInitializerConstants.Workplaces.iHandoverWorkplaceId,
                    TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId
                };

                if (_repositories.PositionRepository.Get(position.Id) == null)
                {
                    _repositories.PositionRepository.Add(position);
                    _repositories.Save();
                }

                var company = new Company
                {
                    Id = DbInitializerConstants.Companies.iHandoverCompanyId,
                    Name = "iHandover",
                    IsDefault = true
                };

                if (_repositories.CompanyRepository.Get(company.Id) == null)
                {
                    _repositories.CompanyRepository.Add(company);
                    _repositories.Save();
                }

                var team = new Team
                {
                    Id = DbInitializerConstants.Teams.iHandoverTeamId,
                    Name = "iHandover"
                };

                if (_repositories.TeamRepository.Get(team.Id) == null)
                {
                    _repositories.TeamRepository.Add(team);
                    _repositories.Save();
                }

                var workPattern = new RotationPattern
                {
                    Id = DbInitializerConstants.WorkPatterns.FiveWorkTwoOff,
                    DayOff = 2,
                    DayOn = 5
                };

                if (_repositories.RotationPatternRepository.Get(workPattern.Id) == null)
                {
                    _repositories.RotationPatternRepository.Add(workPattern);
                    _repositories.Save();
                }

                var workPattern2 = new RotationPattern
                {
                    Id = DbInitializerConstants.WorkPatterns.NineWorkFiveOff,
                    DayOff = 5,
                    DayOn = 9
                };

                if (_repositories.RotationPatternRepository.Get(workPattern2.Id) == null)
                {
                    _repositories.RotationPatternRepository.Add(workPattern2);
                    _repositories.Save();
                }

                var adminSetting = new AdminSettings
                {
                    Id = DbInitializerConstants.AdminSetting.SettingsId,
                    PlanType = TypeOfPlan.Enterprise,
                    SupportType = TypeOfSupport.Premium,
                    HandoverPreviewType = TypeOfHandoverPreview.PenultimateDayOfSwing,
                    HandoverTriggerType = TypeOfHandoverTrigger.FinalDayOfSwing,
                    HostingProvider = "Azure",
                    TimeZoneId = "GMT Standard Time",
                    HandoverLimit = 100,
                    TemplateLimit = 50,
                    TwilioSettings = new TwilioSettings()
                };

                if (_repositories.AdminSettingsRepository.Get(adminSetting.Id) == null)
                {
                    _repositories.AdminSettingsRepository.Add(adminSetting);
                    _repositories.Save();
                }

                TemplateModule[] baseModules = {
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.HSEModuleId, Type = TypeOfModule.HSE, Enabled = true },
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.CoreModuleId, Type = TypeOfModule.Core, Enabled = true },
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.TeamModuleId, Type = TypeOfModule.Team, Enabled = true },
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.ProjectModuleId, Type = TypeOfModule.Project, Enabled = true }
                 };

                TemplateModule[] iHandoverAdminTemplateModules = {
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.HSEModuleId, Type = TypeOfModule.HSE, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.HSEModuleId},
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.CoreModuleId, Type = TypeOfModule.Core, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.CoreModuleId},
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.TeamModuleId, Type = TypeOfModule.Team, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.TeamModuleId},
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.ProjectModuleId, Type = TypeOfModule.Project, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.ProjectModuleId}
                };

                _repositories.TemplateModuleRepository.Add(iHandoverAdminTemplateModules);
                _repositories.TemplateModuleRepository.Add(baseModules);
                _repositories.Save();

                foreach (MajorHazardV2 majorHazard in DbInitializerConstants.SafetyMessageV2.MajorHazards())
                {
                    if (!_repositories.MajorHazardV2Repository.GetAll().Any(mh => mh.Id == majorHazard.Id))
                    {
                        _repositories.MajorHazardV2Repository.Add(majorHazard);
                        _repositories.Save();
                    }
                }

                var adminUser = new UserProfile
                {
                    Id = DbInitializerConstants.Users.iHandoverAdminUserId,
                    UserName = "admin@ihandover.co",
                    Email = "admin@ihandover.co",
                    PositionId = position.Id,
                    TeamId = team.Id,
                    RotationPatternId = workPattern.Id,
                    CompanyId = company.Id,
                    FirstName = "Admin",
                    LastName = "iHandover",
                    CreatedUtc = DateTime.UtcNow
                };

                if (_repositories.UserProfileRepository.Get(adminUser.Id) == null)
                {
                    _repositories.UserProfileRepository.Add(adminUser);
                    _repositories.Save();
                }

                return adminUser;
            }

            if (userProfile == null)
                throw new ObjectNotFoundException(string.Format("User with Id: {0} was not found.", userId));

            return userProfile;
        }

        public IEnumerable<UserProfile> GetAllUsers(string searchQuery = null)
        {
            IQueryable<UserProfile> users = _repositories.UserProfileRepository.GetAll().Where(u => !u.DeletedUtc.HasValue);

            if (!string.IsNullOrEmpty(searchQuery))
            {
                users = users.Where(f => f.FirstName.Contains(searchQuery) ||
                                        f.LastName.Contains(searchQuery) ||
                                        f.Position.Name.Contains(searchQuery) ||
                                        f.Team.Name.Contains(searchQuery));
            }

            return users.ToList();
        }

        public void UpdateUserAvatar(UserProfile user, File avatar)
        {
            user.Avatar = avatar;

            _repositories.UserProfileRepository.Update(user);

            _repositories.Save();
        }

        public List<UserProfile> GetUsersByCompany(Guid companyId)
        {
            return this.GetRelayEmployees().Where(e => e.CompanyId == companyId).ToList();
        }

        public List<UserProfile> GetUsersByRotationPattern(Guid rotationPatternId)
        {
            return this.GetRelayEmployees().Where(e => e.RotationPatternId == rotationPatternId).ToList();
        }

        public Guid? GetUserCurrentHandoverId(UserProfile user)
        {
            if (user.CurrentRotationId.HasValue)
            {
                if (user.CurrentRotation.RotationType == RotationType.Swing)
                {
                    if (user.CurrentRotation.State == RotationState.Confirmed)
                    {
                        return user.CurrentRotationId;
                    }
   
                }

                if (user.CurrentRotation.RotationType == RotationType.Shift)
                {
                    Shift currentShift = _logicCore.ShiftCore.GetActiveRotationShift(user.CurrentRotation);

                    if (currentShift != null)
                    {
                        return currentShift.Id;
                    }
              
                }
            }

            return null;
        }

    }
}
