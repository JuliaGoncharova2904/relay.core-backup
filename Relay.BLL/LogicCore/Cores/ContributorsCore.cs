﻿using System.Collections.Generic;
using MomentumPlus.Core.Interfaces;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class ContributorsCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public ContributorsCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Method for changing collection of contributors in UserProfile 
        /// </summary>
        /// <param name="userId">Guid userId</param>
        /// <param name="contributorsIds">List[Guid] of contributors</param>
        /// <param name="save">Bool save</param>
        public void SetContributorsForUser(Guid userId, List<Guid> contributorsIds, bool save = true)
        {
            UserProfile userProfile = _logicCore.UserProfileCore.GetRelayEmployees()
                .FirstOrDefault(e => e.Id == userId);

            if (userProfile != null)
            {
                if (contributorsIds.Any())
                {
                    var newContributors = _logicCore.UserProfileCore.GetEmployeesByIds(contributorsIds)
                        .Where(u => u.Id != userId).ToList();

                    var forRemovingContributors = userProfile.Contributors.Except(newContributors).ToList();

                    var forAddingContributors = newContributors.Where(u => !userProfile.Contributors.Contains(u))
                        .ToList();

                    foreach (var contributor in forRemovingContributors)
                    {
                        userProfile.Contributors.Remove(contributor);
                    }

                    foreach (var contributor in forAddingContributors)
                    {
                        userProfile.Contributors.Add(contributor);
                    }
                }
                else
                {
                    userProfile.Contributors.Clear();
                }
            }

            _repositories.UserProfileRepository.Update(userProfile);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Get all users from whom you are selected as a contributor
        /// </summary>
        /// <param name="userId">Guid userId</param>
        /// <returns>IQueryable[UserProfile] of contributed users</returns>
        public IQueryable<UserProfile> GetAllContributerUsers(Guid userId)
        {
            UserProfile userProfile = _logicCore.UserProfileCore.GetRelayEmployees()
                .FirstOrDefault(e => e.Id == userId);

            if (userProfile != null)
            {
                return userProfile.ContributedUsers.Where(c => !c.DeletedUtc.HasValue).AsQueryable();
            }

            return null;
        }

        /// <summary>
        /// Сheck whether the user who logged in system is a contributor from another user
        /// </summary>
        /// <param name="searchedUserId">Guid searchedUserId who logged in system</param>
        /// <param name="swingOwnerId">Guid swingOwnerId who owner rotation</param>
        /// <returns>Bool IsContributor</returns>
        public bool IsContributor(Guid searchedUserId, Guid swingOwnerId)
        {
            if (searchedUserId != swingOwnerId)
            {
                var identityUser = _logicCore.UserProfileCore.GetUserProfile(searchedUserId);
                return _logicCore.UserProfileCore.GetUserProfile(swingOwnerId).Contributors.Contains(identityUser);
            }

            return false;
        }

        /// <summary>
        /// Get all contributor user who edit topic
        /// </summary>
        /// <param name="topic">RotationTopic entity topic</param>
        /// <param name="topicOwnerId">Guid topicOwnerId</param>
        /// <returns>IEnumerable[SelectListItem] ContributorsWhoEditTopic</returns>
        public IEnumerable<SelectListItem> ContributorsWhoEditTopic(RotationTopic topic, Guid identityUserId, Guid? topicOwnerId = null)
        {
            if (topic != null)
            {
                topic = topic.Logs != null && topic.Logs.Any() ? topic : topic.ParentTopic;

                if (topic?.Logs != null && topic.Logs.Any())
                {
                    var logs = topic.Logs.Where(l => l.UserId.HasValue);

                    if (topicOwnerId.HasValue)
                    {
                        if (identityUserId == topicOwnerId)
                        {
                            logs = logs.Where(l => l.User.Id != identityUserId ? IsContributor(l.UserId.Value, identityUserId) : !IsContributor(l.UserId.Value, identityUserId));
                        }
                        else
                        {
                            logs = logs.Where(l => l.User.Id == topicOwnerId ? !IsContributor(l.UserId.Value, topicOwnerId.Value) : IsContributor(l.UserId.Value, topicOwnerId.Value));
                        }
                    }
                    else
                    {
                        logs = logs.Where(l => IsContributor(l.UserId.Value, identityUserId));
                    }

                    var user = logs.Select(l => l.User).Distinct(new UserDistinctItemComparerExtension());

                    if (user != null && user.Any())
                    {
                        IEnumerable<SelectListItem> editors = user.Select(u => new SelectListItem
                        {
                            Text = u.FullName,
                            Value = u.Id.ToString()
                        });

                        return editors;
                    }
                }
            }
            return new List<SelectListItem>();
        }
    }
}
