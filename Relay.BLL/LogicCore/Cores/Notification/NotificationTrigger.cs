﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class NotificationTrigger
    {
        private readonly NotificationCore _notificationCore;
        private readonly INotificationAdapter _notificationAdapter;

        public NotificationTrigger(NotificationCore notificationCore, INotificationAdapter notificationAdapter)
        {
            this._notificationCore = notificationCore;
            this._notificationAdapter = notificationAdapter;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="notification"></param>
        private void SendNotification(Guid recipientId, Notification notification)
        {
            int msgNumber = _notificationCore.CountNotShownNotificationsForRecipient(recipientId);
            _notificationAdapter.PushNotificationInfo(recipientId, msgNumber);

            NotificationMessageViewModel msg = new NotificationMessageViewModel
            {
                Id = notification.Id,
                Message = notification.HtmlMessage,
                UrlImage = notification.IconPath,
                Date = notification.Timestamp,
                IsOpened = notification.IsOpened,
                Type = notification.Type.ToString()
            };

            _notificationAdapter.PushNotification(recipientId, msg);
        }

        #region

        /// <summary>
        /// 1. Day to go – possibility to extend swing.
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="rotationId"></param>
        public void Send_PossibilityToExtendSwing(Guid recipientId, Guid rotationId)
        {
            const string message = "Today is the penultimate day of your swing. Click here if you need to change your rotation dates.";

            Notification notification = new Notification
            {
                Type = NotificationType.PossibilityToExtendSwing,
                IconPath = "/Content/img/Notifications/Categories/Reminder_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = rotationId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 2. Handback tasks still to complete
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="numbeRotationTasks"></param>
        /// <param name="rotationId"></param>
        public void Send_HandbackTasksStillComplete(Guid recipientId, int numbeRotationTasks, Guid rotationId)
        {
            const string message = "You have {0} handback tasks still to complete.";

            Notification notification = new Notification
            {
                Type = NotificationType.HandbackTasksStillComplete,
                IconPath = "/Content/img/Notifications/Categories/Reminder_Icon.svg",
                HtmlMessage = string.Format(message, numbeRotationTasks),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = rotationId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 3. Tasks still to complete
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="numbeRotationTasks"></param>
        /// <param name="rotationId"></param>
        public void Send_TasksStillComplete(Guid recipientId, int numbeRotationTasks, Guid rotationId)
        {
            const string message = "You have {0} tasks still to complete.";

            Notification notification = new Notification
            {
                Type = NotificationType.TasksStillComplete,
                IconPath = "/Content/img/Notifications/Categories/Reminder_Icon.svg",
                HtmlMessage = string.Format(message, numbeRotationTasks),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = rotationId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 4. Manager added a comment to an item
        /// </summary>
        /// <param name="recipientId"></param>
        public void Send_ManagerAddedComment(string userName, string userSurname, Guid recipientId, Guid sourceId, Guid? userId = null)
        {
            string message = userName + " " + userSurname + " has added a comment to {0} of items.";

            Notification notification = _notificationCore.GetLastNotShownNotification(recipientId, NotificationType.ManagerAddedComment);

            if (notification != null)
            {
                List<Guid> sources = JsonConvert.DeserializeObject<List<Guid>>(notification.ServiceData);

                if (!sources.Contains(sourceId))
                {
                    sources.Add(sourceId);
                    notification.ServiceData = JsonConvert.SerializeObject(sources);
                    notification.HtmlMessage = string.Format(message, sources.Count);
                    notification.Timestamp = DateTime.UtcNow;

                    _notificationCore.UpdateNotification(notification);
                }
            }
            else
            {
                notification = new Notification
                {
                    Type = NotificationType.ManagerAddedComment,
                    IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                    HtmlMessage = string.Format(message, 1),
                    Timestamp = DateTime.UtcNow,
                    ServiceData = JsonConvert.SerializeObject(new List<Guid>(new[] { sourceId })),
                    RecipientId = recipientId,
                    RelationId = userId,
                };

                _notificationCore.AddNotification(notification);
            }

            this.SendNotification(recipientId, notification);
        }


        public void Send_AddedAttachment(Guid recipientId, string taskAssigneeName, Guid taskId, TaskBoardFilterOptions filter)
        {
            const string message = "{0} has added an attachment to a task you assigned them.";

            Notification notification = new Notification
            {
                Type = filter == TaskBoardFilterOptions.IncompletedTasks ? NotificationType.AddedAttachmentToIncompleteTask : NotificationType.AddedAttachmentToCompleteTask,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, taskAssigneeName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = taskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 5. You have been assigned tasks
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="numbeRotationTasks"></param>
        public void Send_AssignedTasks(Guid recipientId, int numbeRotationTasks)
        {
            const string message = "You have been assigned {0} tasks.";

            Notification notification = new Notification
            {
                Type = NotificationType.AssignedTasks,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, numbeRotationTasks),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 6. You have been assigned handback tasks.
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="numbeRotationTasks"></param>
        public void Send_AssignedHandbackTasks(Guid recipientId, int numbeRotationTasks)
        {
            const string message = "You have been assigned {0} handback tasks.";

            Notification notification = new Notification
            {
                Type = NotificationType.AssignedHandbackTasks,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, numbeRotationTasks),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 7. A team member has just left site and handed over.
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="teamMemberName"></param>
        /// <param name="hisBackToBackName"></param>
        /// <param name="userId"></param>
        public void Send_TeamMemberHandedOver(Guid recipientId, string teamMemberName, string hisBackToBackName, Guid userId)
        {
            const string message = "{0} has left site and has handed over to {1}.";

            Notification notification = new Notification
            {
                Type = NotificationType.TeamMemberHandedOver,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, teamMemberName, hisBackToBackName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = userId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        public void Send_RecipientSendReport(Guid recipientId, string teamMemberName, Guid userId, Guid? swingId)
        {
            const string message = "{0} has left site and a handover report is available to view.";

            Notification notification = new Notification
            {
                Type = NotificationType.SwingReceivedReport,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, teamMemberName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = userId,
                SwingId = swingId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        public void Send_ShiftRecipientSendReport(Guid recipientId, string teamMemberName, Guid userId, Guid? shiftId)
        {
            const string message = "{0} has left site and a handover report is available to view.";

            Notification notification = new Notification
            {
                Type = NotificationType.ShiftReceivedReport,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, teamMemberName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = userId,
                ShiftId = shiftId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }


        /// <summary>
        /// 8. Manager reassigns handover
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="newBackToBackName"></param>
        public void Send_ManagerReassignsHandover(Guid recipientId, string newBackToBackName)
        {
            const string message = "Your handover recipient has been changed. You will now be handing over to {0}.";

            Notification notification = new Notification
            {
                Type = NotificationType.ManagerReassignsHandover,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, newBackToBackName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        public void Send_CreateOwnHandover(Guid recipientId, string ownerRotation, Guid userId)
        {
            const string message = "{0} has set up their handover.";

            Notification notification = new Notification
            {
                Type = NotificationType.TeamMemberHandedOver,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, ownerRotation),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = userId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 9. Manager reassigns a task
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="taskId"></param>
        public void Send_ManagerReassignsTask(Guid recipientId, Guid taskId)
        {
            const string message = "The assignee of one of your tasks has been changed.";

            Notification notification = new Notification
            {
                Type = NotificationType.ManagerReassignsTask,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = taskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }


        /// <summary>
        /// 22. Status of task is incomplete on the day after the due date
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="numbeRotationTasks"></param>
        /// <param name="sourceTaskId"></param>
        public void Send_StatuOfTaskIncompleteAfterDueDate(Guid recipientId, string taskCreatorName, DateTime dueDate, Guid userId, Guid taskId)
        {
            const string message = "A task assigned to {0} has been left incomplete past the due date.";

            Notification notification = new Notification
            {
                Type = NotificationType.StatuOfTaskIncompleteAfterDueDate,
                IconPath = "/Content/img/Notifications/Categories/Reminder_Icon.svg",
                HtmlMessage = string.Format(message, taskCreatorName, dueDate),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = userId
            };

            _notificationCore.AddNotificationStatuOfTaskIncompleteAfterDueDate(notification, taskId);
            this.SendNotification(recipientId, notification);


        }

        #endregion

        #region Now tasks notifications

        /// <summary>
        /// 10. User has assigned a task for you
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="taskCreatorName"></param>
        /// <param name="dueDate"></param>
        /// <param name="destTaskId"></param>
        public void Send_AssignedNowTaskForYou(Guid recipientId, string taskCreatorName, DateTime dueDate, Guid destTaskId)
        {
            const string message = "{0} has assigned a task for you to complete before {1:dd} {1:MMMM} {1:yyyy}";

            Notification notification = new Notification
            {
                Type = NotificationType.AssignedNowTaskForYou,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, taskCreatorName, dueDate),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = destTaskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 22. Status of task is incomplete on the day after the due date
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="numbeRotationTasks"></param>
        /// <param name="sourceTaskId"></param>
        public void Send_StatuOfTaskIncompleteAfterDueDateForNowTask(Guid recipientId, string taskCreatorName, DateTime dueDate, Guid userId, Guid taskId)
        {
            const string message = "A task assigned to {0} has been left incomplete past the due date.";

            Notification notification = new Notification
            {
                Type = NotificationType.StatuOfTaskIncompleteAfterDueDateFoTaskNow,
                IconPath = "/Content/img/Notifications/Categories/Reminder_Icon.svg",
                HtmlMessage = string.Format(message, taskCreatorName, dueDate),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = userId
            };

            _notificationCore.AddNotificationStatuOfTaskIncompleteAfterDueDate(notification, taskId);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 11. User has not completed your task
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="taskRecipientName"></param>
        /// <param name="sourceTaskId"></param>
        public void Send_YourNowTaskHasNotCompleted(Guid recipientId, string taskRecipientName, Guid sourceTaskId)
        {
            const string message = "{0} has not completed the task that you assigned them yet";

            Notification notification = new Notification
            {
                Type = NotificationType.YourNowTaskHasNotCompleted,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, taskRecipientName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = sourceTaskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 12. You have a task to complete by tomorrow
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="destTaskId"></param>
        public void Send_YouHaveNowTaskToComplete(Guid recipientId, Guid destTaskId)
        {
            const string message = "You have a task to complete by tomorrow";

            Notification notification = new Notification
            {
                Type = NotificationType.YouHaveNowTaskToComplete,
                IconPath = "/Content/img/Notifications/Categories/Reminder_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = destTaskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 13. Creator modified the due date of the task
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="destTaskId"></param>
        public void Send_CreatorModifiedDueDate(Guid recipientId, Guid destTaskId)
        {
            const string message = "Creator modified the due date of the task";

            Notification notification = new Notification
            {
                Type = NotificationType.CreatorModifiedDueDate,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = destTaskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 14. Creator re-assigned the task
        /// </summary>
        /// <param name="recipientId"></param>
        public void Send_CreatorReassignedNowTask(Guid recipientId)
        {
            const string message = "Creator re-assigned the task";

            Notification notification = new Notification
            {
                Type = NotificationType.CreatorReassignedNowTask,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 15. Creator deleted the task
        /// </summary>
        /// <param name="recipientId"></param>
        public void Send_CreatorDeletedNowTask(Guid recipientId)
        {
            const string message = "Creator deleted the task";

            Notification notification = new Notification
            {
                Type = NotificationType.CreatorDeletedNowTask,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        public void Send_CreatorDeletedNowTask1(Guid recipientId, RotationTask rotationTask)
        {
            string message = "Creator deleted the task" + rotationTask.Name;

            Notification notification = new Notification
            {
                Type = NotificationType.CreatorDeletedNowTask,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 18. User has completed the task that you assigned them
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="sourceTaskId"></param>
        /// <param name="userName"></param>
        public void Send_UserHasCompletedNowTask(Guid recipientId, Guid sourceTaskId, string userName)
        {
            const string message = "{0} has completed the task that you assigned them";

            Notification notification = new Notification
            {
                Type = NotificationType.UserHasCompletedTask,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, userName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = sourceTaskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        #endregion

        #region Info Share Notifications

        /// <summary>
        /// 16. Colleague has shared information with you
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="topicOwnerName"></param>
        public void Send_OneSharedInformationWithYou(Guid recipientId, string topicOwnerName)
        {
            // const string message = "Colleague has shared information with you";

            const string message = "A topic from {0}'s report has been shared with you.";

            Notification notification = new Notification
            {
                Type = NotificationType.OneSharedInformationWithYou,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, topicOwnerName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        public void Send_SharedReportWithYou(Guid recipientId, string userName, string userSurname)
        {
            string message = userName + " " + userSurname + " shared a report with you.";

            Notification notification = new Notification
            {
                Type = NotificationType.SharedReportWithYou,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, userName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        /// <summary>
        /// 17. The person has read information you sent and marked as complete
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="complitedTaskId"></param>
        /// <param name="recipientName"></param>
        public void Send_OneReadSharedInfoAndComplete(Guid recipientId, Guid complitedTaskId, string recipientName)
        {
            const string message = "{0} has read information you sent and marked as complete";

            Notification notification = new Notification
            {
                Type = NotificationType.OneReadSharedInfoAndComplete,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = string.Format(message, recipientName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = complitedTaskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        #endregion

        #region Shift notifications

        /// <summary>
        /// 19. Day to go – possibility to extend shift.
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="shiftId"></param>
        public void Send_PossibilityToExtendShift(Guid recipientId)
        {
            const string message = "Your shift will be over soon. Click here if you need to change your shift time.";

            Notification notification = new Notification
            {
                Type = NotificationType.PossibilityToExtendSwing,
                IconPath = "/Content/img/Notifications/Categories/Reminder_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        #endregion

        #region Reasigned and Due Date

        public void Send_TaskDueDateIsChanged(Guid recipientId, string taskCreatorName, Guid taskId)
        {
            const string message = "{0} changed the due date of a task that was assigned to them by you.";

            Notification notification = new Notification
            {
                Type = NotificationType.TaskDueDateIsChanged,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, taskCreatorName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = taskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        public void Send_ReceivedTaskWasReassigned(Guid recipientId, string taskCreatorName, Guid taskId)
        {
            const string message = "{0} reassigned a task that was assigned to them by you.";

            Notification notification = new Notification
            {
                Type = NotificationType.ReceivedTaskWasReassigned,
                IconPath = "/Content/img/Notifications/Categories/Assignment_Icon.svg",
                HtmlMessage = string.Format(message, taskCreatorName),
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId,
                RelationId = taskId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        public void Send_CreatedSectorIhandoverAdmin(Guid recipientId)
        {
            const string message = "IHandover Admin created a new sector.";

            Notification notification = new Notification
            {
                Type = NotificationType.CreateSectorIhandoverAdmin,
                IconPath = "/Content/img/Notifications/Categories/Interactions_Icon.svg",
                HtmlMessage = message,
                Timestamp = DateTime.UtcNow,
                RecipientId = recipientId
            };

            _notificationCore.AddNotification(notification);
            this.SendNotification(recipientId, notification);
        }

        #endregion
    }
}
