﻿using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public static class NotificationTypeExtensions
    {
        public static string GetTypeClass(this NotificationType nType)
        {
            string classType = string.Empty;

            switch (nType)
            {
                case NotificationType.PossibilityToExtendSwing:
                case NotificationType.PossibilityToExtendShift:
                    classType = "edit-shift-rotation";
                    break;
                case NotificationType.HandbackTasksStillComplete:
                case NotificationType.TasksStillComplete:
                case NotificationType.AssignedTasks:
                case NotificationType.AssignedHandbackTasks:
                case NotificationType.CreatorReassignedNowTask:
                case NotificationType.CreatorDeletedNowTask:             
                    classType = "load-checklist";
                    break;
                case NotificationType.OneSharedInformationWithYou:
                    classType = "load-shared-topics";
                    break;
                case NotificationType.SharedReportWithYou:
                    classType = "load-shared-reports";
                    break;
              //  case NotificationType.ManagerAddedComment:
                case NotificationType.ManagerReassignsHandover:
                    classType = "load-report-builder";
                    break;
                case NotificationType.StatuOfTaskIncompleteAfterDueDate:
                    classType = "load-report-builder-task-section-inBox";
                    break;
                case NotificationType.StatuOfTaskIncompleteAfterDueDateFoTaskNow:
                    classType = "load-report-builder-task-section-inBoxNowTask";
                    break;
                case NotificationType.TeamMemberHandedOver:
                case NotificationType.CreateOwnHandover:
                case NotificationType.ManagerAddedComment:
                    classType = "load-report-builder-view-mode";
                    break;
                case NotificationType.ManagerReassignsTask:
                case NotificationType.YourNowTaskHasNotCompleted:
                    classType = "edit-task";
                    break;
                case NotificationType.TaskDueDateIsChanged:
                case NotificationType.ReceivedTaskWasReassigned:
                case NotificationType.UserHasCompletedTask:
                    classType = "view-task";
                    break;
                case NotificationType.AssignedNowTaskForYou:
                case NotificationType.YouHaveNowTaskToComplete:
                case NotificationType.CreatorModifiedDueDate:
                    classType = "complete-task";
                    break;
                case NotificationType.OneReadSharedInfoAndComplete:
                    classType = "edit-topic";
                    break;
                case NotificationType.AddedAttachmentToIncompleteTask:
                    classType = "edit-task";
                    break;
                case NotificationType.AddedAttachmentToCompleteTask:
                    classType = "view-task";
                    break;
                case NotificationType.CreateSectorIhandoverAdmin:
                    classType = "view-task";
                    break;
                case NotificationType.SwingReceivedReport:
                    classType = "load-swing-report-builder";
                    break;
                case NotificationType.ShiftReceivedReport:
                    classType = "load-shift-report-builder";
                    break;

            }

            return classType;
        }
    }
}
