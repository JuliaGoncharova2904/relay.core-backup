﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class NotificationCore
    {
        private readonly INotificationAdapter _notificationAdapter;
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthBridge _authBridge;

        private NotificationTrigger _notificationTrigger;

        /// <summary>
        /// Return notification trigger object
        /// </summary>
        public NotificationTrigger NotificationTrigger
        {
            get
            {
                return _notificationTrigger ?? (_notificationTrigger = new NotificationTrigger(this, _notificationAdapter));
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repositoriesUnitOfWork"></param>
        /// <param name="logicCore"></param>
        public NotificationCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore, INotificationAdapter notificationAdapter, IAuthBridge authBridge)
        {
            this._notificationAdapter = notificationAdapter;
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
            this._authBridge = authBridge;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Notification GetLastNotShownNotification(Guid userId, NotificationType type)
        {
            Notification notification = _repositories.NotificationRepository.Find(n => n.RecipientId == userId &&
                                                                                        !n.IsShown &&
                                                                                        n.Type == type)
                                                                             .OrderByDescending(n => n.Timestamp)
                                                                             .FirstOrDefault();

            return notification;
        }

        /// <summary>
        /// Get notification entity by Id.
        /// </summary>
        /// <param name="notificationId">Id of Notification entity</param>
        /// <returns></returns>
        public Notification GetNotification(Guid notificationId)
        {
            Notification notification = _repositories.NotificationRepository.Get(notificationId);

            if (notification == null)
                throw new ObjectNotFoundException(string.Format("Notification with Id: {0} was not found.", notificationId));

            return notification;
        }

        /// <summary>
        /// Add Notification entity
        /// </summary>
        /// <param name="notification">Notification entity</param>
        /// <param name="save">Sinc with database</param>
        public void AddNotification(Notification notification, bool save = true)
        {
            notification.Id = Guid.NewGuid();
            _repositories.NotificationRepository.Add(notification);

            if (save)
                _repositories.Save();
        }

        public void AddNotificationStatuOfTaskIncompleteAfterDueDate(Notification notification, Guid taskId, bool save = true)
        {
            notification.Id = Guid.NewGuid();
            _repositories.NotificationRepository.Add(notification);

            if (save)
                _repositories.Save();

            _logicCore.TaskBoardCore.SetNotificationForTask(taskId);
        }

        /// <summary>
        /// Update Notification entity
        /// </summary>
        /// <param name="notification">Notification entity</param>
        /// <param name="save">Sinc with database</param>
        public void UpdateNotification(Notification notification, bool save = true)
        {
            _repositories.NotificationRepository.Update(notification);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Remove Notification Relation
        /// </summary>
        /// <param name="relationId">Relation entity Id</param>
        public void RemoveNotificationRelation(Guid relationId)
        {
            List<Notification> notifications = _repositories.NotificationRepository
                                                            .Find(n => n.RelationId.HasValue && n.RelationId.Value == relationId)
                                                            .ToList();

            notifications.ForEach(n =>
            {
                n.RelationId = null;
                _repositories.NotificationRepository.Update(n);
            });

            _repositories.Save();
        }

        /// <summary>
        /// Get notifications for user with pagination.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="page">Page number</param>
        /// <param name="pageSize">Page size</param>
        /// <returns></returns>
        public IEnumerable<Notification> GetNotificationsByUser(Guid userId, int page, int pageSize)
        {
            IQueryable<Notification> notifications = _repositories.NotificationRepository
                                                                    .Find(n => n.RecipientId == userId)
                                                                    .OrderByDescending(n => n.Timestamp)
                                                                    .Skip(page * pageSize)
                                                                    .Take(pageSize);


            var restrictionType = _authBridge.AuthService.GetSubscriptionType(userId);

            if (restrictionType == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                foreach (var notification in notifications)
                {
                    var daysAccessHistory = DaysAccessHistoryForBasicLevel(notification.Timestamp);
                    notifications = notifications.Where(n => daysAccessHistory);
                }
            }
            else if (restrictionType == Authorization.Domain.Entities.RestrictionType.Premium)
            {
                foreach (var notification in notifications)
                {
                    var daysAccessHistory = DaysAccessHistoryForPremiumLevel(notification.Timestamp);
                    notifications = notifications.Where(n => daysAccessHistory);
                }
            }

            return notifications.ToList();
        }

        private bool DaysAccessHistoryForBasicLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddDays(7);
            if (timeStamp <= daysAccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool DaysAccessHistoryForPremiumLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddMonths(3);
            if (timeStamp <= daysAccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get total counter of notification for Recipient
        /// </summary>
        /// <param name="recipientId">Recipient Id</param>
        /// <returns></returns>
        public int GetTotalCountNotificationByUser(Guid recipientId)
        {
            return _repositories.NotificationRepository.GetAll().Count(n => n.RecipientId == recipientId);
        }

        /// <summary>
        /// Count Not Shown Notifications For User
        /// </summary>
        /// <param name="recipientId">Recipient Id</param>
        /// <returns></returns>
        public int CountNotShownNotificationsForRecipient(Guid recipientId)
        {
            return _repositories.NotificationRepository.Find(n => n.RecipientId == recipientId && !n.IsShown).Count();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="save"></param>
        public void MakeLastNotificationsShownForRecipient(Guid recipientId, bool save = true)
        {
            IEnumerable<Notification> notifications = _repositories.NotificationRepository
                                                                    .Find(n => n.RecipientId == recipientId && !n.IsShown)
                                                                    .ToList();
            foreach (Notification notification in notifications)
            {
                notification.IsShown = true;
                _repositories.NotificationRepository.Update(notification);
            }

            _notificationAdapter.PushNotificationInfo(recipientId, 0);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recipientId"></param>
        /// <param name="notificationId"></param>
        /// <param name="save"></param>
        public void MakeNotificationOpenedForRecipient(Guid recipientId, Guid notificationId, bool save = true)
        {
            Notification notification = this.GetNotification(notificationId);

            if (notification.RecipientId != recipientId)
                throw new Exception(string.Format("Notification with Id: {0} does not belong to User with Id: {1}", notificationId, recipientId));

            notification.IsOpened = true;
            this.UpdateNotification(notification, save);
        }

    }
}
