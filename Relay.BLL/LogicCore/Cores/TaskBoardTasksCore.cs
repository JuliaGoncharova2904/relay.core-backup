﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskBoardTasksCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskBoardTasksCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IEnumerable<TaskBoardTaskForHandover> GetTaskBoardTasksForShift(Guid shiftId, ModuleSourceType sourceType, Guid? receivedShiftId = null)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            var shiftStartDateTime = shift.StartDateTime.Value;

            var shiftEndDateTime = shiftStartDateTime.AddMinutes(shift.WorkMinutes + shift.BreakMinutes);

            UserProfile shiftOwner = shift.Rotation.RotationOwner;

            IEnumerable<RotationTask> tasks = new List<RotationTask>();

            var timezoneInfo = _logicCore.AdministrationCore.GetCurrentServerTimeZone(shiftOwner.Id);

            if (sourceType == ModuleSourceType.Draft)
            {
                var outboxTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(shiftOwner, TaskBoardTaskType.Draft);

                var myTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(shiftOwner, TaskBoardTaskType.MyTask);

                var pendingTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(shiftOwner, TaskBoardTaskType.Pending);

                tasks = outboxTaskBoardTasks.Union(myTaskBoardTasks).Union(pendingTaskBoardTasks).Where(t => t.AddToReport == true && t.HandoverId == shiftId).ToList();


                //tasks = tasks.Where(t =>
                //           !t.ModifiedUtc.HasValue &&
                //           TimeZoneInfo.ConvertTimeFromUtc(t.CreatedUtc.Value, timezoneInfo) >= shiftStartDateTime &&
                //           TimeZoneInfo.ConvertTimeFromUtc(t.CreatedUtc.Value, timezoneInfo) <= shiftEndDateTime || t.ModifiedUtc.HasValue && TimeZoneInfo.ConvertTimeFromUtc(t.ModifiedUtc.Value, timezoneInfo) >= shiftStartDateTime && TimeZoneInfo.ConvertTimeFromUtc(t.ModifiedUtc.Value, timezoneInfo) <= shiftEndDateTime).ToList();

            }

            if (sourceType == ModuleSourceType.Received)
            {

                var receivedShifts = shift.HandoverFromShifts;

                foreach (var receivedShift in receivedShifts)
                {
                    var outboxTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(receivedShift.Rotation.RotationOwner, TaskBoardTaskType.Draft);

                    var myTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(receivedShift.Rotation.RotationOwner, TaskBoardTaskType.MyTask);

                    var pendingTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(receivedShift.Rotation.RotationOwner, TaskBoardTaskType.Pending);

                    var receivedTasks = outboxTaskBoardTasks.Union(myTaskBoardTasks).Union(pendingTaskBoardTasks).Where(t => t.AddToReport == true && t.HandoverId == receivedShift.Id);

                    var rotationTasks = tasks.Union(receivedTasks);

                    tasks = rotationTasks.ToList();
                }

                if (receivedShiftId.HasValue)
                {
                    tasks = tasks.Where(t => t.HandoverId == receivedShiftId);
                }

            }
            //foreach (var test in tasks)
            //{
            //    var createTime = TimeZoneInfo.ConvertTimeFromUtc(test.CreatedUtc.Value, timezoneInfo);

            //    var editTime = TimeZoneInfo.ConvertTimeFromUtc(test.ModifiedUtc.Value, timezoneInfo);
            //}



            List<TaskBoardTaskForHandover> model = new List<TaskBoardTaskForHandover>();

            foreach (var taskItem in tasks)
            {
                UserProfile taskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(taskItem);

                var task = new TaskBoardTaskForHandover
                {
                    Name = taskItem.Name,
                    Status = taskItem.Status.ToString(),
                    Priority = (TaskPriority)taskItem.Priority,
                    Notes = taskItem.Description,
                    Deadline = taskItem.Deadline.FormatWithDayAndMonth(),
                    AssignedToId = taskItem.AssignedToId,
                    HasAttachments = taskItem.Attachments != null && taskItem.Attachments.Any(),
                    CompleteStatus = taskItem.IsComplete ? "Complete" : "Incomplete",
                    HasVoiceMessages = taskItem.VoiceMessages != null && taskItem.VoiceMessages.Any(),
                    IsComplete = taskItem.IsComplete,
                    Id = taskItem.Id,
                    RelationId = taskItem.RelationId,
                    OwnerId = shiftOwner.Id,
                    IsFeedbackRequired = taskItem.IsFeedbackRequired,
                    FromTeammateId = taskOwner?.Id,
                    FromTeammateFullName = taskOwner?.FullName,
                    HasComments = taskItem.ManagerComments != null && taskItem.ManagerComments.Any()
                };

                model.Add(task);
            }

            return model;
        }

        public string GetDateForRotationReceivedPanel(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            if (rotation != null)
            {

                return rotation.StartDate.Value.FormatWithDayMonthYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn).FormatWithDayMonthYear();
            }

            return string.Empty;
        }

        public string GetShiftType(Shift shift, Rotation rotation)
        {
            if (shift != null)
            {
                var user = _logicCore.UserProfileCore.GetRelayEmployees().FirstOrDefault(t => t.Id == rotation.RotationOwnerId);

                if (user.ShiftPatternType == "1")
                {
                    TimeSpan time0 = new TimeSpan(0, 0, 0);
                    TimeSpan time12 = new TimeSpan(12, 0, 0);

                    var startTimeDateShift = shift.StartDateTime.Value.AddHours(12);

                    if (time0 <= startTimeDateShift.TimeOfDay && startTimeDateShift.TimeOfDay < time12)
                    {
                        return "Night";
                    }

                    if (time12 <= startTimeDateShift.TimeOfDay && startTimeDateShift.TimeOfDay >= time0)
                    {
                        return "Day";
                    }

                }
            
                if (user.ShiftPatternType == "2")
                {
                    TimeSpan time0 = new TimeSpan(0, 0, 0);
                    TimeSpan time16 = new TimeSpan(16, 0, 0);
                    TimeSpan time15 = new TimeSpan(15, 55, 0);
                    TimeSpan time12 = new TimeSpan(12, 0, 0);
                    TimeSpan time4 = new TimeSpan(4, 0, 0);
                    TimeSpan time8 = new TimeSpan(8, 0, 0);

                    var startTimeDateShift = shift.StartDateTime.Value.AddHours(8);

                    if (time12 <= startTimeDateShift.TimeOfDay && startTimeDateShift.TimeOfDay <= time15)
                    {
                        return "Early";
                    }

                    if (time16 <= startTimeDateShift.TimeOfDay && startTimeDateShift.TimeOfDay >= time0)
                    {
                        return "Late";
                    }

                    if (time4 <= startTimeDateShift.TimeOfDay && startTimeDateShift.TimeOfDay <= time8)
                    {
                        return "Night";
                    }
                }
            }
            return "";
        }

        public IEnumerable<TaskBoardTaskForHandover> GetTaskBoardTasksForRotation(Guid rotationId, ModuleSourceType sourceType, Guid? receivedRotationId = null)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            var rotationStartDate = rotation.StartDate.Value;

            var rotationEndDate = rotationStartDate.AddDays(rotation.DayOn);

            UserProfile rotationOwner = rotation.RotationOwner;

            IEnumerable<RotationTask> tasks = new List<RotationTask>();

            var timezoneInfo = _logicCore.AdministrationCore.GetCurrentServerTimeZone(rotationOwner.Id);

            if (sourceType == ModuleSourceType.Draft)
            {
                var outboxTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(rotationOwner, TaskBoardTaskType.Draft);

                var myTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(rotationOwner, TaskBoardTaskType.MyTask);

                var pendingTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(rotationOwner, TaskBoardTaskType.Pending);

                tasks = outboxTaskBoardTasks.Union(myTaskBoardTasks).Union(pendingTaskBoardTasks).Where(t => t.AddToReport == true && t.HandoverId == rotationId).ToList();


                //tasks = tasks.Where(t =>
                //           !t.ModifiedUtc.HasValue &&
                //           TimeZoneInfo.ConvertTimeFromUtc(t.CreatedUtc.Value, timezoneInfo).Date >= rotationStartDate.Date &&
                //           TimeZoneInfo.ConvertTimeFromUtc(t.CreatedUtc.Value, timezoneInfo).Date <= rotationEndDate.Date || t.ModifiedUtc.HasValue && TimeZoneInfo.ConvertTimeFromUtc(t.ModifiedUtc.Value, timezoneInfo).Date >= rotationStartDate.Date && TimeZoneInfo.ConvertTimeFromUtc(t.ModifiedUtc.Value, timezoneInfo) <= rotationEndDate.Date).ToList();

            }

            if (sourceType == ModuleSourceType.Received)
            {

                var receivedRotations = rotation.HandoverFromRotations;

                foreach (var receivedRotation in receivedRotations)
                {
                    var outboxTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(receivedRotation.RotationOwner, TaskBoardTaskType.Draft);

                    var myTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(receivedRotation.RotationOwner, TaskBoardTaskType.MyTask);

                    var pendingTaskBoardTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(receivedRotation.RotationOwner, TaskBoardTaskType.Pending);

                    var receivedTasks = outboxTaskBoardTasks.Union(myTaskBoardTasks).Union(pendingTaskBoardTasks).Where(t => t.AddToReport == true && t.HandoverId == receivedRotation.Id);

                    var rotationTasks = tasks.Union(receivedTasks);

                    tasks = rotationTasks.ToList();
                }


                if (receivedRotationId.HasValue)
                {
                    tasks = tasks.Where(t => t.HandoverId == receivedRotationId);
                }

            }

            List<TaskBoardTaskForHandover> model = new List<TaskBoardTaskForHandover>();

            foreach (var taskItem in tasks)
            {

                UserProfile taskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(taskItem);

                var task = new TaskBoardTaskForHandover
                {
                    Name = taskItem.Name,
                    Status = taskItem.Status.ToString(),
                    Priority = (TaskPriority)taskItem.Priority,
                    Notes = taskItem.Description,
                    Deadline = taskItem.Deadline.FormatWithDayAndMonth(),
                    AssignedToId = taskItem.AssignedToId,
                    HasAttachments = taskItem.Attachments != null && taskItem.Attachments.Any(),
                    CompleteStatus = taskItem.IsComplete ? "Complete" : "Incomplete",
                    HasVoiceMessages = taskItem.VoiceMessages != null && taskItem.VoiceMessages.Any(),
                    IsComplete = taskItem.IsComplete,
                    Id = taskItem.Id,
                    RelationId = taskItem.RelationId,
                    OwnerId = rotation.RotationOwnerId,
                    IsFeedbackRequired = taskItem.IsFeedbackRequired,
                    FromTeammateId = taskOwner?.Id,
                    FromTeammateFullName = taskOwner?.FullName,
                    HasComments = taskItem.ManagerComments != null && taskItem.ManagerComments.Any()
                };

                model.Add(task);
            }

            return model;
        }


    }
}
