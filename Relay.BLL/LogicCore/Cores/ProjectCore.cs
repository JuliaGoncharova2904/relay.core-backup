﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class ProjectCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repositoriesUnitOfWork"></param>
        /// <param name="logicCore"></param>
        public ProjectCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Return project by Id or generate exception (ObjectNotFoundException)
        /// </summary>
        /// <param name="projectId">Project Id</param>
        /// <returns></returns>
        public Project GetProject(Guid projectId)
        {
            Project project = _repositories.ProjectRepository.Get(projectId);

            if (project == null)
                throw new ObjectNotFoundException(string.Format("Project with Id: {0} was not found.", projectId));

            return project;
        }

        /// <summary>
        /// Add to database new Project entity
        /// </summary>
        /// <param name="project">Project entity</param>
        /// <param name="save">Sinc with database</param>
        public Guid AddProject(Project project, bool save = true)
        {
            project.Id = Guid.NewGuid();

            _repositories.ProjectRepository.Add(project);
            if (save)
                _repositories.Save();

            return project.Id;
        }

        /// <summary>
        /// Update Project entity in database
        /// </summary>
        /// <param name="project">Project entity</param>
        /// <param name="save">Sinc with database</param>
        public void UpdateProject(Project project, bool save = true)
        {
            DateTime currentDate = DateTime.UtcNow.Date;

            if (currentDate >= project.EndDate.Date.AddMonths(3))
            {
                this.DisableProjectModuleForFollowers(project, false);
            }
            else
            {
                this.AddEnableProjectModuleForFollowers(project, false);
            }

            _logicCore.RotationTopicGroupCore.UpdateTopicGroupsNameFromRelation(project.Id, project.Name, false);

            _repositories.ProjectRepository.Update(project);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Return all projects
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Project> GetAllProjets()
        {
            IQueryable<Project> projects = _repositories.ProjectRepository.GetAll();

            return projects.ToList();
        }

        /// <summary>
        /// Return projects for Project Manager
        /// </summary>
        /// <param name="projectManagerId">Project Manager Id</param>
        /// <returns></returns>
        public IEnumerable<Project> GetProjetsByProjectManager(Guid projectManagerId)
        {
            UserProfile projectManager = _logicCore.UserProfileCore.GetUserProfile(projectManagerId);

            return projectManager.ManagedProjects.ToList();
        }

        /// <summary>
        /// Return projects for Line Manager
        /// </summary>
        /// <param name="lineManagerId">Line Manager Id</param>
        /// <returns></returns>
        public IEnumerable<Project> GetProjetsByLineManager(Guid lineManagerId)
        {
            UserProfile lineManager = _logicCore.UserProfileCore.GetUserProfile(lineManagerId);

            List<Project> projects = lineManager.ManagedProjects.ToList();

            projects.AddRange(lineManager.CreatedProjects.ToList());

            return projects.GroupBy(p => p.Id).Select(p => p.First());
        }

        /// <summary>
        /// Return followers for project
        /// </summary>
        /// <param name="projectId">Project Id</param>
        /// <param name="searchQuery">Search query</param>
        /// <returns></returns>
        public IQueryable<UserProfile> GetFollowersOfProject(Guid projectId, string searchQuery)
        {
            IQueryable<UserProfile> projectFollowers = _repositories.UserProfileRepository
                                                                    .Find(up => !up.DeletedUtc.HasValue
                                                                                && up.FollowingProjects.Select(p => p.Id).Contains(projectId));

            if (!string.IsNullOrEmpty(searchQuery))
            {
                projectFollowers = projectFollowers.Where(u => u.FirstName.Contains(searchQuery) ||
                                                            u.LastName.Contains(searchQuery) ||
                                                            u.Position.Name.Contains(searchQuery) ||
                                                            u.Team.Name.Contains(searchQuery));
            }

            return projectFollowers;
        }

        /// <summary>
        /// Add followers to Project
        /// </summary>
        /// <param name="project">Project entity</param>
        /// <param name="followersIds">Followers IDs</param>
        /// <param name="save">Sinc with database</param>
        public void AddFollowersToProject(Project project, IEnumerable<Guid> followersIds, bool save = true)
        {
            var newFollowersIds = followersIds.Where(Id => !project.ProjectFollowers.Any(f => Id == f.Id));

            var newFollowers = _repositories.UserProfileRepository.Find(u => newFollowersIds.Contains(u.Id)).ToList();

            foreach (var follower in newFollowers)
            {
                project.ProjectFollowers.Add(follower);
            }

            this.AddEnableProjectModuleForFollowers(newFollowers, project, false);

            _repositories.ProjectRepository.Update(project);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Remove followers from Project
        /// </summary>
        /// <param name="project">Project entity</param>
        /// <param name="followersIds">Followers IDs</param>
        /// <param name="save">Sinc with database</param>
        public void RemoveFollowersFromProject(Project project, IEnumerable<Guid> followersIds, bool save = true)
        {
            var removeFollowers = from user in project.ProjectFollowers
                                  where followersIds.Contains(user.Id)
                                  select user;



            foreach (UserProfile follower in removeFollowers.ToList())
            {
                project.ProjectFollowers.Remove(follower);
            }

            this.DisableProjectModuleForFollowers(removeFollowers, project, false);

            _repositories.ProjectRepository.Update(project);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Return all teams that is included to project entirely
        /// </summary>
        /// <param name="project">Project entity</param>
        /// <param name="teams">Teams entities</param>
        /// <returns></returns>
        public IEnumerable<Team> GetSelectedTeamsForProject(Project project, IEnumerable<Team> teams)
        {
            List<Team> selectedTeams = new List<Team>();

            foreach (Team team in teams)
            {
                int usersInTeam = team.Users.Where(u => !u.DeletedUtc.HasValue).Count();

                if (usersInTeam != 0)
                {
                    int followersFromTeam = project.ProjectFollowers.Where(f => !f.DeletedUtc.HasValue && f.TeamId == team.Id).Count();

                    if (followersFromTeam == usersInTeam)
                    {
                        selectedTeams.Add(team);
                    }
                }
            }

            return selectedTeams;
        }

        /// <summary>
        /// Add teams to project
        /// </summary>
        /// <param name="project">Project entity</param>
        /// <param name="teamsIds">Teams IDs</param>
        /// <param name="save">Sinc with database</param>
        public void AddTeamsToProject(Project project, IEnumerable<Guid> teamsIds, bool save = true)
        {
            IEnumerable<Team> teams = _repositories.TeamRepository.Find(t => teamsIds.Contains(t.Id)).ToList();

            foreach (Team team in teams)
            {
                List<Guid> addedUsersIds = project.ProjectFollowers.Where(f => !f.DeletedUtc.HasValue && f.TeamId == team.Id)
                                                                    .Select(f => f.Id)
                                                                    .ToList();
                List<UserProfile> newMembers = team.Users.Where(u => !u.DeletedUtc.HasValue && !addedUsersIds.Contains(u.Id)).ToList();

                foreach (UserProfile follower in newMembers)
                {
                    project.ProjectFollowers.Add(follower);
                }

                this.AddEnableProjectModuleForFollowers(newMembers, project, false);
            }

            _repositories.ProjectRepository.Update(project);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Remove teams from Project
        /// </summary>
        /// <param name="project">Project entity</param>
        /// <param name="teamsIds">Teams IDs</param>
        /// <param name="save">Sinc with database</param>
        public void RemoveTeamsFromProject(Project project, IEnumerable<Guid> teamsIds, bool save = true)
        {
            foreach (Guid teamId in teamsIds)
            {
                IEnumerable<UserProfile> removedFollowers = project.ProjectFollowers.Where(f => f.TeamId == teamId).ToList();

                if (removedFollowers.Any())
                {
                    foreach (UserProfile follower in removedFollowers)
                    {
                        project.ProjectFollowers.Remove(follower);
                    }

                    this.DisableProjectModuleForFollowers(removedFollowers, project, false);
                }
            }

            _repositories.ProjectRepository.Update(project);

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Return swing tasks for project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IQueryable<RotationTask> GetSwingAndShiftTasksForProject(Guid projectId)
        {
            List<Guid> usersIds = this.GetFollowersOfProject(projectId, null).Select(u => u.Id).ToList();

            IQueryable<RotationTask> rotationTasks = _repositories.RotationModuleRepository
                                                                    .Find(rm => rm.Type == TypeOfModule.Project
                                                                                && rm.SourceType == TypeOfModuleSource.Draft
                                                                                && (rm.RotationId.HasValue && usersIds.Contains(rm.Rotation.RotationOwnerId)
                                                                                    || rm.ShiftId.HasValue && usersIds.Contains(rm.Shift.Rotation.RotationOwnerId))
                                                                            )
                                                                    .SelectMany(rm => rm.RotationTopicGroups)
                                                                    .Where(tg => tg.RelationId == projectId)
                                                                    .SelectMany(tg => tg.RotationTopics)
                                                                    .SelectMany(t => t.RotationTasks);

            return rotationTasks;
        }

        /// <summary>
        /// Return TaskBoard tasks for Project.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public IQueryable<RotationTask> GetTaskBoardTasksForProject(UserProfile user, Guid projectId)
        {
            IQueryable<RotationTask> rotationTasks = _repositories.RotationTaskRepository.Find(t => t.ProjectId == projectId && t.TaskBoardTaskType != TaskBoardTaskType.Received);

            return rotationTasks;
        }

        #region Managing API of Project Modules for rotation and shift

        /// <summary>
        /// Add/Update of TemplateModule Topic Group of Project for follower.
        /// </summary>
        /// <param name="follower">Follover</param>
        /// <param name="project">Project</param>
        /// <param name="enabled">State of TemplateModule Topic Group</param>
        /// <param name="save">Sinc with database</param>
        private void AddUpdateProjectTopicGroupForFollower(UserProfile follower, Project project, bool enabled, bool save = true)
        {
            if (DateTime.UtcNow.Date <= project.EndDate.Date.AddMonths(3))
            {
                RotationModule projectModule = this.GetActiveProjectModuleForUser(follower);

                if (projectModule != null)
                {
                    RotationTopicGroup relationTopicGroup = projectModule.RotationTopicGroups.FirstOrDefault(tg => tg.RelationId == project.Id);

                    if (relationTopicGroup == null)
                    {
                        relationTopicGroup = new RotationTopicGroup
                        {
                            Id = Guid.NewGuid(),
                            Enabled = enabled,
                            RotationModuleId = projectModule.Id,
                            RelationId = project.Id,
                            Description = project.Description,
                            Name = project.Name,
                            TempateTopicGroupId = null,
                            ParentRotationTopicGroupId = null
                        };

                        _repositories.RotationTopicGroupRepository.Add(relationTopicGroup);
                    }
                    else
                    {
                        relationTopicGroup.Enabled = enabled;
                        _repositories.RotationTopicGroupRepository.Update(relationTopicGroup);
                    }

                    if (save)
                        _repositories.Save();

                }
            }
        }

        /// <summary>
        /// Get Active Project Module For User
        /// </summary>
        /// <param name="user">UserProfile entity</param>
        /// <returns></returns>
        private RotationModule GetActiveProjectModuleForUser(UserProfile user)
        {
            if (user.CurrentRotationId.HasValue)
            {
                Rotation rotation = user.CurrentRotation;
                if (rotation.RotationType == RotationType.Swing)
                {
                    return _logicCore.RotationModuleCore.GetOrCreateRotationModule(rotation.Id, TypeOfModule.Project, TypeOfModuleSource.Draft);
                }
                else if (rotation.RotationType == RotationType.Shift)
                {
                    Shift shift = _logicCore.ShiftCore.GetActiveRotationShift(rotation);

                    if (shift != null)
                    {
                        return _logicCore.RotationModuleCore.GetOrCreateShiftModule(shift.Id, TypeOfModule.Project, TypeOfModuleSource.Draft);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Add/Enable TemplateModule Topic Group of Project for specific followers.
        /// </summary>
        /// <param name="followers">Followers</param>
        /// <param name="project">Project</param>
        /// <param name="save">Sinc with database</param>
        private void AddEnableProjectModuleForFollowers(IEnumerable<UserProfile> followers, Project project, bool save = true)
        {
            followers = followers.Where(f => f.CurrentRotationId.HasValue)
                                .Where(f => !f.CurrentRotation.RotationModules.Any(rm => rm.Type == TypeOfModule.Project &&
                                                                                    rm.RotationTopicGroups.Any(rtg => rtg.RelationId == project.Id && rtg.Enabled)));

            foreach (UserProfile follower in followers)
            {
                this.AddUpdateProjectTopicGroupForFollower(follower, project, true, false);
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Add/Enable TemplateModule Topic Group of Project for all followers.
        /// </summary>
        /// <param name="project">Project</param>
        /// <param name="save">Sinc with database</param>
        public void AddEnableProjectModuleForFollowers(Project project, bool save = true)
        {
            IEnumerable<UserProfile> followers = project.ProjectFollowers
                                                    .Where(f => !f.DeletedUtc.HasValue && f.CurrentRotationId.HasValue)
                                                    .Where(f => !f.CurrentRotation.RotationModules
                                                                    .Any(rm => rm.Type == TypeOfModule.Project &&
                                                                               rm.RotationTopicGroups.Any(rtg => rtg.RelationId == project.Id && rtg.Enabled)))
                                                    .ToList();

            foreach (UserProfile follower in followers)
            {
                this.AddUpdateProjectTopicGroupForFollower(follower, project, true, false);
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Disable TemplateModule Topic Group of Project for specific followers.
        /// </summary>
        /// <param name="followers">Followers</param>
        /// <param name="project">Project</param>
        /// <param name="save">Sinc with database</param>
        private void DisableProjectModuleForFollowers(IEnumerable<UserProfile> followers, Project project, bool save = true)
        {
            List<RotationTopicGroup> folowersTopicGrups = followers.Where(f => f.CurrentRotationId.HasValue)
                                                                    .SelectMany(f => f.CurrentRotation.RotationModules.Where(rm => rm.Type == TypeOfModule.Project))
                                                                    .SelectMany(rm => rm.RotationTopicGroups.Where(rtg => rtg.RelationId == project.Id && rtg.Enabled))
                                                                    .ToList();

            foreach (RotationTopicGroup topicGroup in folowersTopicGrups)
            {
                topicGroup.Enabled = false;
                _repositories.RotationTopicGroupRepository.Update(topicGroup);
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Disable TemplateModule Topic Group of Project for all followers.
        /// </summary>
        /// <param name="project">Project with followers</param>
        /// <param name="save">Sinc with database</param>
        public void DisableProjectModuleForFollowers(Project project, bool save = true)
        {
            List<RotationTopicGroup> folowersTopicGrups = project.ProjectFollowers
                                                                .Where(f => !f.DeletedUtc.HasValue && f.CurrentRotationId.HasValue)
                                                                .SelectMany(f => f.CurrentRotation.RotationModules.Where(rm => rm.Type == TypeOfModule.Project))
                                                                .SelectMany(rm => rm.RotationTopicGroups.Where(rtg => rtg.RelationId == project.Id && rtg.Enabled))
                                                                .ToList();

            foreach (RotationTopicGroup topicGroup in folowersTopicGrups)
            {
                topicGroup.Enabled = false;
                _repositories.RotationTopicGroupRepository.Update(topicGroup);
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Initialization of TemplateModule Topic Group of Project.
        /// </summary>
        /// <param name="user">Rotation owner</param>
        /// <param name="save">Sinc with database</param>
        public void InitProjectModuleTopicGroups(UserProfile user, bool save = true)
        {
            DateTime currentDate = DateTime.UtcNow.Date;

            List<Project> userProjects = user.FollowingProjects
                                                        .Where(p => currentDate <= p.EndDate.Date.AddMonths(3))
                                                        .ToList();

            userProjects.ForEach(userProject => { this.AddUpdateProjectTopicGroupForFollower(user, userProject, true, false); });

            //foreach (Project userProject in userProjects)
            //{
            //    this.AddUpdateProjectTopicGroupForFollower(user, userProject, true, false);
            //}

            if (save)
                _repositories.Save();
        }

        #endregion

    }
}