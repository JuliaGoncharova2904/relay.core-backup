﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class SharingReportCore
    {
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IRepositoriesUnitOfWork _repositories;

        public SharingReportCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            this._repositories = repositoriesUnitOfWork;
        }

        /// <summary>
        /// Return shared rotation reports for User.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public IQueryable<Rotation> GetSharedRotationReportsForUser(Guid userId)
        {
            List<Guid> rotationsIds = _repositories.ReportSharingRelationRepository
                                                    .Find(sr => sr.RecipientId == userId && sr.ReportType == RotationType.Swing)
                                                    .Select(sr => sr.ReportId)
                                                    .ToList();

            return _repositories.RotationRepository.Find(r => rotationsIds.Contains(r.Id));
        }

        /// <summary>
        /// Return shared shift reports for User.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public IQueryable<Shift> GetSharedShiftReportsForUser(Guid userId)
        {
            List<Guid> shiftsIds = _repositories.ReportSharingRelationRepository
                                                    .Find(sr => sr.RecipientId == userId && sr.ReportType == RotationType.Shift)
                                                    .Select(sr => sr.ReportId)
                                                    .ToList();

            return _repositories.ShiftRepository.Find(s => shiftsIds.Contains(s.Id));
        }

        /// <summary>
        /// Count recipients for source.
        /// </summary>
        /// <param name="sourceId">Source Id</param>
        /// <returns></returns>
        public int CountRecipientsBySourceId(Guid sourceId)
        {
            int counter = _repositories.ReportSharingRelationRepository.Find(sr => sr.ReportId == sourceId)
                                                                         .Select(sr => sr.RecipientId)
                                                                         .Distinct()
                                                                         .Count();

            return counter;
        }


        public List<Models.DropDownListItemModel> GetRecipientsBySourceId(Guid sourceId)
        {
            var recipients = _repositories.ReportSharingRelationRepository.Find(sr => sr.ReportId == sourceId)
                                                                         .Select(sr => sr.Recipient)
                                                                         .Distinct().Where(u => !u.DeletedUtc.HasValue).ToList();

            var userList = recipients.Select(t =>
                     new Models.DropDownListItemModel
                     {
                         Text = t.FullName,
                         Value = t.Id.ToString()
                     }).ToList();

            if (userList != null && userList.Any())
            {
                userList.Insert(0, new Models.DropDownListItemModel
                {
                    Text = "Shared with: "
                });
            }
            else
            {
                userList.Insert(0, new Models.DropDownListItemModel
                {
                    Text = "This report is not shared with any users. "
                });
            }

            return userList;
        }



        /// <summary>
        /// Get sharing report relations.
        /// </summary>
        /// <param name="sourceId">Source Id</param>
        /// <returns></returns>
        public IQueryable<RotationReportSharingRelation> GetRotationReportSharingRelations(Guid sourceId)
        {
            return _repositories.ReportSharingRelationRepository.Find(sr => sr.ReportId == sourceId);
        }

        /// <summary>
        /// Change share report relations.
        /// </summary>
        /// <param name="sourceId">Source Id</param>
        /// <param name="recipientsIds"></param>
        /// <param name="deleteShareReportRelationIds"></param>
        /// <param name="save"></param>
        public void ChangeShareReportRelations(Guid sourceId, Models.RotationType sourceType, IEnumerable<Guid> recipientsIds, IEnumerable<Guid> deleteRelationIds, bool save = true)
        {
            if (recipientsIds != null && recipientsIds.Any())
            {
                foreach (var recipientId in recipientsIds)
                {
                    this.AddShareReportRelation(sourceId, (RotationType)sourceType, recipientId, false);
                }
            }

            if (deleteRelationIds != null && deleteRelationIds.Any())
            {
                _repositories.ReportSharingRelationRepository.Delete(deleteRelationIds);
            }

            if (save)
            {
                _repositories.Save();
            }
        }

        /// <summary>
        /// Add share report relation.
        /// </summary>
        /// <param name="sourceId">Source Id</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="recipientId">Recipient Id</param>
        /// <param name="save">Sync with database</param>
        public void AddShareReportRelation(Guid sourceId, RotationType sourceType, Guid recipientId, bool save = true)
        {
            if(_repositories.ReportSharingRelationRepository.GetAll().Any(s => s.ReportId == sourceId && s.RecipientId == recipientId))
            {
                return;
            }

            UserProfile recipient = _logicCore.UserProfileCore.GetUserProfile(recipientId);
            UserProfile reportOwner = null;

            switch (sourceType)
            {
                case RotationType.Swing:
                    reportOwner = _logicCore.RotationCore.GetRotation(sourceId).RotationOwner;
                    break;
                case RotationType.Shift:
                    reportOwner = _logicCore.ShiftCore.GetShift(sourceId).Rotation.RotationOwner;
                    break;
                default:
                    throw new Exception("AddShareReportRelation. Unsupported rotation type.");
            }

            RotationReportSharingRelation newRelation = new RotationReportSharingRelation
            {
                Id = Guid.NewGuid(),
                ReportId = sourceId,
                ReportType = sourceType,
                RecipientId = recipient.Id,
                Recipient = recipient,
                ReportOwnerId = reportOwner.Id,
                ReportOwner = reportOwner
            };

            _repositories.ReportSharingRelationRepository.Add(newRelation);

            if (save)
            {
                _repositories.Save();
            }
        }

    }
}
