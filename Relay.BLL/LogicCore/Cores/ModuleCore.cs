﻿using MomentumPlus.Core.Interfaces;
using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;
using MvcPaging;
using System.Data.Entity;
using MomentumPlus.Core.DataSource;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class ModuleCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public ModuleCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public TemplateModule GetModule(Guid moduleId)
        {
            var module = _repositories.TemplateModuleRepository.Get(moduleId);

            return module;
        }

        public Guid GetBaseModuleIdFromType(ModuleType type)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.Type == (TypeOfModule)type && !m.ParentModuleId.HasValue).FirstOrDefault();

            return module.Id;
        }

        public TemplateModule GetBaseModuleFromType(ModuleType type)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.Type == (TypeOfModule)type && !m.ParentModuleId.HasValue).FirstOrDefault();

            if (module == null)
            {
                TemplateModule[] baseModules = {
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.HSEModuleId, Type = TypeOfModule.HSE, Enabled = true },
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.CoreModuleId, Type = TypeOfModule.Core, Enabled = true },
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.TeamModuleId, Type = TypeOfModule.Team, Enabled = true },
                    new TemplateModule { Id = DbInitializerConstants.BaseModules.ProjectModuleId, Type = TypeOfModule.Project, Enabled = true }
                 };

                TemplateModule[] iHandoverAdminTemplateModules = {
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.HSEModuleId, Type = TypeOfModule.HSE, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.HSEModuleId},
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.CoreModuleId, Type = TypeOfModule.Core, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.CoreModuleId},
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.TeamModuleId, Type = TypeOfModule.Team, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.TeamModuleId},
                    new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.ProjectModuleId, Type = TypeOfModule.Project, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.ProjectModuleId}
                };

                _repositories.TemplateModuleRepository.Add(iHandoverAdminTemplateModules);
                _repositories.TemplateModuleRepository.Add(baseModules);
                _repositories.Save();
            }

            return module;
        }


        IQueryable<TemplateModule> GetBaseModules()
        {
            var modules = _repositories.TemplateModuleRepository.Find(m => !m.ParentModuleId.HasValue && !m.DeletedUtc.HasValue);

            return modules;
        }

        public TemplateModule GetTemplateModule(TypeOfModule type, Guid templateId)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.TemplateId == templateId && m.Type == type && !m.DeletedUtc.HasValue).AsNoTracking().FirstOrDefault();

            return module;
        }

        public IQueryable<TemplateModule> GetTemplateModules(Guid templateId)
        {
            var modules = _repositories.TemplateModuleRepository.Find(m => m.TemplateId == templateId && !m.DeletedUtc.HasValue);

            return modules;
        }


        public int EnabledTopicsGroupsForModuleCounter(Guid baseModuleId, Guid templateId)
        {
            var baseModule = GetModule(baseModuleId);

            var templateModule = GetOrCreateTemplateModule(templateId, baseModule);
            if (templateModule == null || templateModule.TopicGroups == null)
            {
                return 0;
            }

            return templateModule.TopicGroups.Count(tg => tg.Enabled && !tg.DeletedUtc.HasValue);
        }

       
        public List<rTopicGroupViewModel> EnabledTopicsGroupsForModule(Guid baseModuleId, Guid templateId)
        {
            var baseModule = GetModule(baseModuleId);

            var templateModule = GetOrCreateTemplateModule(templateId, baseModule);
            if (templateModule == null || templateModule.TopicGroups == null)
            {
                return null;
            }

            templateModule.TopicGroups = templateModule.TopicGroups.Where(tg => tg.Enabled && !tg.DeletedUtc.HasValue && tg.Topics.Where(t=>t.Enabled).Any()).ToList();

            List<rTopicGroupViewModel> rTopicGroupViewModel = templateModule.TopicGroups.Select(topicItem => new rTopicGroupViewModel
            {
                Description = topicItem.Description,
                Enabled = topicItem.Enabled,
                Id = topicItem.Id,
                IsNullReport = topicItem.IsNullReport,
                ModuleId = topicItem.ModuleId,
                Name = topicItem.Name,
                ParentTopicGroupId = topicItem.ParentTopicGroupId,
                RelationId = topicItem.RelationId,
                Topics = topicItem.Topics.Select(topic => new rTopicViewModel
                {
                    Description = topic.Description,
                    Enabled = topic.Enabled,
                    Id = topic.Id,
                    Name = topic.Name,
                    PlannedActualHas = topic.PlannedActualHas.HasValue ? topic.PlannedActualHas.Value : topic.IsExpandPlannedActualField,
                    ParentTopicId = topic.ParentTopicId,
                    TopicGroupId = topic.TopicGroupId,
                    UnitsSelectedType = topic.UnitsSelectedType
                }).ToList()

            }).ToList();

            return rTopicGroupViewModel;
        }


        public bool CheckChildModuleStatus(Guid baseModuleId, Guid templateId)
        {
            var baseModule = GetModule(baseModuleId);

            var templateModule = GetOrCreateTemplateModule(templateId, baseModule);

            _logicCore.RotationModuleCore.PopulateRotationsModules(templateModule);

            return templateModule.Enabled;
        }

        public TemplateModule AddChildModule(TemplateModule baseModule, Guid templateId, bool status = true)
        {
            var module = new TemplateModule
            {
                Id = Guid.NewGuid(),
                Enabled = status,
                Type = baseModule.Type,
                ParentModuleId = baseModule.Id,
                TemplateId = templateId,
                TopicGroups = null
            };

            _repositories.TemplateModuleRepository.Add(module);
            _repositories.Save();

            return module;
        }
        
        /// <summary>
        /// Get Or Create Template Module From Base Module
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="baseModule"></param>
        /// <returns></returns>

        public TemplateModule GetOrCreateTemplateModule(Guid templateId, TemplateModule baseModule)
        {
            var templateModule = GetTemplateModule(baseModule.Type, templateId) ??
                                 AddChildModule(baseModule, templateId);

            return templateModule;
        }

        public void InitTemplateModules(Guid templateId)
        {
            var baseModules = GetBaseModules();


            foreach (var baseModule in baseModules.ToList())
            {
                GetOrCreateTemplateModule(templateId, baseModule);
            }
        }


    }
}