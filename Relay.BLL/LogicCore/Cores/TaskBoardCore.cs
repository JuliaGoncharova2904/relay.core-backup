﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Data.Entity.Core;
using System.Linq;
using MomentumPlus.Relay.BLL.Extensions.EntityExtensions;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TaskBoardCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        private readonly TaskCompleteCore _completeCore;

        private readonly TaskDeleteCore _deleteCore;

        public TaskBoardCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;

            this._completeCore = new TaskCompleteCore(repositoriesUnitOfWork, _logicCore);
            this._deleteCore = new TaskDeleteCore(repositoriesUnitOfWork, _logicCore);
        }

        #region Private methods

        private IQueryable<RotationTask> GetTasksByTypeFromRotation(Guid rotationId, TaskBoardTaskType type)
        {
            IQueryable<RotationModule> modules = _repositories.RotationModuleRepository.GetAll()
                                                                .Where(m => m.RotationId == rotationId);

            return this.GetTasksByTypeFromModules(modules, type);
        }

        private IQueryable<RotationTask> GetTasksByTypeFromRotationShifts(Guid rotationId, TaskBoardTaskType type)
        {
            IQueryable<RotationModule> modules = _repositories.ShiftRepository.GetAll()
                                                                .Where(s => s.RotationId == rotationId && s.State != ShiftState.Created)
                                                                .SelectMany(s => s.RotationModules);

            return this.GetTasksByTypeFromModules(modules, type);
        }

        private IQueryable<RotationTask> GetTasksByTypeFromModules(IQueryable<RotationModule> modules, TaskBoardTaskType type)
        {
            switch (type)
            {
                case TaskBoardTaskType.Draft:
                    modules = modules.Where(m => m.Enabled && m.SourceType == TypeOfModuleSource.Draft);
                    break;
                case TaskBoardTaskType.Received:
                    modules = modules.Where(m => m.Enabled && m.SourceType == TypeOfModuleSource.Received);
                    break;
                default:
                    throw new Exception("Unsupported type.");
            }

            return modules.SelectMany(m => m.RotationTopicGroups)
                            .Where(rtg => rtg.Enabled)
                            .SelectMany(rtg => rtg.RotationTopics)
                            .Where(tp => tp.Enabled)
                            .SelectMany(tp => tp.RotationTasks)
                            .Where(t => t.Enabled);
        }

        #endregion

        /// <summary>
        /// Return TaskBoard entity by ID.
        /// </summary>
        /// <param name="taskBoardId">TaskBoard entity ID</param>
        /// <returns></returns>
        public TaskBoard GetTaskBoard(Guid taskBoardId)
        {
            TaskBoard taskBoard = _repositories.TaskBoardRepository.Get(taskBoardId);

            if (taskBoard == null)
                throw new ObjectNotFoundException(string.Format("TaskBoard with ID: {0} was not found.", taskBoardId));

            return taskBoard;
        }

        /// <summary>
        /// Return query for all tasks with specific type.
        /// </summary>
        /// <param name="type">Task type</param>
        /// <returns></returns>
        public IQueryable<RotationTask> GetTasksByType(TaskBoardTaskType type)
        {
            return _repositories.RotationTaskRepository.Find(t => t.TaskBoardTaskType == type && t.Enabled);
        }


        /// <summary>
        /// Return tasks of TaskBoard with specific Type.
        /// </summary>
        /// <param name="user">User (TaskBoard owner)</param>
        /// <param name="type">Task Type</param>
        /// <returns></returns>
        public IQueryable<RotationTask> GetTasksByTypeFromTaskBoard(UserProfile user, TaskBoardTaskType type)
        {
            return _repositories.RotationTaskRepository.Find(t => t.TaskBoardId.HasValue
                                                                    && t.TaskBoardId == user.Id
                                                                    && t.TaskBoardTaskType == type
                                                                    && t.Enabled).OrderBy(t=>(DateTime)t.CreatedUtc.Value);
        }

        /// <summary>
        /// Return tasks of Work Item (Swing/Shift) with specific Type.
        /// </summary>
        /// <param name="user">User (Rotation owner)</param>
        /// <param name="type">Task Type</param>
        /// <returns></returns>
        public IQueryable<RotationTask> GetTasksByTypeFromActiveWorkItems(UserProfile user, TaskBoardTaskType type)
        {
            IQueryable<RotationTask> tasks = null;

            if (user.CurrentRotationId.HasValue)
            {
                Rotation rotation = user.CurrentRotation;

                switch (rotation.RotationType)
                {
                    case RotationType.Swing:
                        if (type == TaskBoardTaskType.Draft)
                        {
                            if (rotation.State != RotationState.Created)
                            {
                                tasks = this.GetTasksByTypeFromRotation(rotation.Id, type);

                                if (rotation.State == RotationState.Confirmed && rotation.PrevRotationId.HasValue)
                                {
                                    tasks = tasks.Union(this.GetTasksByTypeFromRotation(rotation.PrevRotationId.Value, type));
                                }
                            }
                            else if (rotation.PrevRotationId.HasValue)
                            {
                                tasks = this.GetTasksByTypeFromRotation(rotation.PrevRotationId.Value, type);
                            }
                        }
                        if (type == TaskBoardTaskType.Received && rotation.State == RotationState.Confirmed)
                        {
                            tasks = this.GetTasksByTypeFromRotation(rotation.Id, type);
                        }
                        break;
                    case RotationType.Shift:
                        if (rotation.State == RotationState.Confirmed || rotation.State == RotationState.SwingEnded)
                        {
                            tasks = this.GetTasksByTypeFromRotationShifts(rotation.Id, type);
                        }
                        break;
                    default:
                        throw new Exception("Unknowen rotation type.");
                }
            }

            return tasks;
        }

        /// <summary>
        /// Return all draft topics of Work Item (Swing/Shift).
        /// </summary>
        /// <param name="user">User entity</param>
        /// <returns></returns>
        public IQueryable<RotationTopic> GetTopicsFromActiveWorkItem(UserProfile user, TypeOfModuleSource sourceType = TypeOfModuleSource.Draft)
        {
            IQueryable<RotationTopic> topics = null;

            if (user.CurrentRotationId.HasValue && user.CurrentRotation.State == RotationState.Confirmed)
            {
                Rotation rotation = user.CurrentRotation;

                switch (rotation.RotationType)
                {
                    case RotationType.Swing:
                        topics = _repositories.RotationModuleRepository.Find(m => m.RotationId == rotation.Id && m.Enabled && m.SourceType == sourceType)
                                                                        .SelectMany(m => m.RotationTopicGroups)
                                                                        .Where(tg => tg.Enabled)
                                                                        .SelectMany(tg => tg.RotationTopics)
                                                                        .Where(t => t.Enabled);
                        break;
                    case RotationType.Shift:
                        Shift shift = _logicCore.ShiftCore.GetActiveRotationShift(rotation);
                        if (shift != null)
                        {
                            topics = _repositories.RotationModuleRepository.Find(m => m.ShiftId == shift.Id && m.Enabled && m.SourceType == sourceType)
                                                                            .SelectMany(m => m.RotationTopicGroups)
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);
                        }
                        break;
                    default:
                        throw new Exception("Unknowen rotation type.");
                }
            }

            return topics;
        }

        /// <summary>
        /// Add new task entity.
        /// </summary>
        /// <param name="task">Task entity</param>
        public void AddTask(RotationTask task)
        {
            task.Id = Guid.NewGuid();
            task.Enabled = true;
            task.IsPinned = false;
            _repositories.RotationTaskRepository.Add(task);
            _repositories.Save();
        }


        public void SetNotificationForTask(Guid taskId)
        {
            var taskBoard = _repositories.RotationTaskRepository.Get(taskId);
            taskBoard.IsHaveNotificationTask = true;
            _repositories.Save();
        }

        public bool IsHaveNotificationForTask(Guid taskId)
        {
            var taskBoard = _repositories.RotationTaskRepository.Get(taskId);
            return taskBoard.IsHaveNotificationTask;
        }


        /// <summary>
        /// Handover TaskBoard task.
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="save">Sync with database</param>
        public void HandoverTask(RotationTask task, bool save = true)
        {
            if (task.TaskBoardTaskType != TaskBoardTaskType.Draft && task.TaskBoardTaskType != TaskBoardTaskType.Pending)
                throw new Exception("Task can not be handovered.");

            if (task.SuccessorTask == null)
            {
                RotationTask handoveredTask = task.Clone();
                handoveredTask.TaskBoardId = task.AssignedToId;
                handoveredTask.TaskBoardTaskType = TaskBoardTaskType.Received;
                handoveredTask.RotationTopicId = null;
                handoveredTask.RotationTopic = null;

                task.SuccessorTaskId = handoveredTask.Id;

                _repositories.RotationTaskRepository.Add(handoveredTask);

                task.TaskBoardTaskType = TaskBoardTaskType.Draft;
                task.DeferredHandoverTime = null;

                _repositories.RotationTaskRepository.Update(task);

                _logicCore.NotificationCore.NotificationTrigger
                                            .Send_AssignedNowTaskForYou(task.AssignedToId.Value,
                                                                        task.TaskBoard.User.FullName,
                                                                        task.Deadline,
                                                                        handoveredTask.Id);

                if (save)
                    _repositories.Save();
            }
        }


        /// <summary>
        /// Update task mechanism
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="owner">Task owner</param>
        /// <param name="assignedToId">Task assignee</param>
        /// <param name="selectedTopicId">Destination topic id for task</param>
        /// <param name="sendDate">Pending date</param>
        /// <param name="save">Sync with database</param>
        public void UpdateTask(RotationTask task, UserProfile owner, Guid assignedToId, Guid? selectedTopicId, DateTime? sendDate, bool save = true)
        {
            RotationTask childTask = task.SuccessorTask;

            TaskBoardTaskType lockTaskType = task.TaskBoardTaskType;

            task.TaskBoardTaskType = TaskBoardTaskType.NotSet;

            switch (task.TaskBoardTaskType)
            {
                case TaskBoardTaskType.NotSet:

                    if (selectedTopicId.HasValue)
                    {
                        task.TaskBoardId = null;
                        task.DeferredHandoverTime = null;
                        task.RotationTopicId = selectedTopicId.Value;
                        task.TaskBoardTaskType = TaskBoardTaskType.NotSet;
                        goto default;
                    }

                    if (lockTaskType == TaskBoardTaskType.MyTask && task.AssignedToId != assignedToId)
                    {
                        task.AssignedToId = assignedToId;
                        task.TaskBoardId = owner.Id;
                        task.AssignedTo = null;
                        task.TaskBoard = owner.TaskBoard;
                        task.IsPinned = false;
                        task.RotationTopicId = null;
                        task.RotationTopic = null;
                        task.DeferredHandoverTime = null;
                        task.TaskBoardTaskType = TaskBoardTaskType.Draft;

                        goto case TaskBoardTaskType.Draft;
                    }

                    task.IsPinned = false;
                    task.RotationTopicId = null;
                    task.RotationTopic = null;
                    task.TaskBoardId = owner.Id;
                    task.TaskBoard = owner.TaskBoard;
                    task.DeferredHandoverTime = null;
                    task.TaskBoardTaskType = TaskBoardTaskType.Draft;

                    if (owner.Id == assignedToId)
                    {
                        goto case TaskBoardTaskType.MyTask;
                    }

                    if (sendDate.HasValue)
                    {
                        goto case TaskBoardTaskType.Pending;
                    }

                    goto case TaskBoardTaskType.Draft;

                case TaskBoardTaskType.MyTask:
                    task.TaskBoardTaskType = TaskBoardTaskType.MyTask;
                    goto default;
                case TaskBoardTaskType.Pending:
                    task.TaskBoardTaskType = TaskBoardTaskType.Pending;
                    task.DeferredHandoverTime = sendDate;
                    goto default;
                case TaskBoardTaskType.Draft:
                    task.TaskBoardTaskType = TaskBoardTaskType.Draft;
                    if (!task.HasParent() && childTask != null)
                    {
                        if (task.AssignedToId != null && task.AssignedToId.Value != assignedToId)
                        {
                            task.AssignedToId = assignedToId;
                            _deleteCore.RemoveTaskAndRelations(childTask);
                            this.HandoverTask(task, false);


                            //var firstTaskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(task);

                            //_logicCore.RotationTaskLogCore.AddMessageToTaskLog(task, $"<strong>{firstTaskOwner.FullName}</strong> reassigned the task to <strong>{task.AssignedTo.FullName}" + "</strong>.");

                        }
                        else
                        {
                            while (childTask != null)
                            {
                                childTask.AssignedToId = assignedToId;
                                childTask.Name = task.Name;
                                childTask.Description = task.Description;
                                childTask.Deadline = task.Deadline;
                                childTask.Priority = task.Priority;
                                childTask.SearchTags = task.SearchTags;
                                childTask.IsFeedbackRequired = task.IsFeedbackRequired;

                                _repositories.RotationTaskRepository.Update(childTask);

                                childTask = childTask.SuccessorTask;
                            }
                        }
                    }
                    else
                    {
                        if (childTask != null)
                        {
                            _deleteCore.RemoveTaskAndRelations(childTask);
                        }
                        this.HandoverTask(task, false);
                    }
                    break;
                default:

                    if (task.AssignedToId != null && task.AssignedToId.Value != assignedToId)
                    {
                        task.AssignedToId = assignedToId;
                    }

                    //Remove Child Tasks
                    if (childTask != null)
                    {
                        _deleteCore.RemoveTaskAndRelations(childTask);
                    }

                    break;
            }

            _repositories.RotationTaskRepository.Update(task);

            if (save)
                _repositories.Save();

        }

        /// <summary>
        /// Save task feedback
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="feedback">Feedback text</param>
        /// <param name="save">Sync with database</param>
        public void SaveTaskFeedback(RotationTask task, string feedback, bool save = true)
        {
            task.Feedback = feedback;
            _repositories.RotationTaskRepository.Update(task);


            RotationTask parentTask = task.AncestorTask;

            while (parentTask != null)
            {
                parentTask.Feedback = feedback;

                _repositories.RotationTaskRepository.Update(parentTask);

                parentTask = parentTask.AncestorTask;
            }

            if (save)
                _repositories.Save();
        }

        /// <summary>
        /// Complete task
        /// </summary>
        /// <param name="task">Task entity</param>
        /// <param name="feedback">Feedback text</param>
        /// <param name="save">Sync with database</param>
        public void CompleteTask(RotationTask task, string feedback, bool save = true)
        {
            if (task.IsComplete)
                return;

            _completeCore.CompleteTask(task, feedback);
        }
    }
}
