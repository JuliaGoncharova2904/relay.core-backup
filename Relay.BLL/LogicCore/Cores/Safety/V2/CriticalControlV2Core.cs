﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class CriticalControlV2Core
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public CriticalControlV2Core(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IEnumerable<CriticalControlV2> GetAllCriticalControls(MajorHazardV2 majorHazard)
        {
            IEnumerable<CriticalControlV2> criticalControls = majorHazard.CriticalControls.ToList();

            return criticalControls;
        }

        public CriticalControlV2 GetCriticalControl(Guid criticalControlId)
        {
            CriticalControlV2 criticalControl = _repositories.CriticalControlV2Repository.Get(criticalControlId);

            if(criticalControl == null)
                throw new ObjectNotFoundException(string.Format("CriticalControlV2 with Id: {0} was not found.", criticalControlId));

            return criticalControl;
        }

        public void AddCriticalControl(CriticalControlV2 criticalControl, bool save = true)
        {
            criticalControl.Id = Guid.NewGuid();

            _repositories.CriticalControlV2Repository.Add(criticalControl);

            if (save)
                _repositories.Save();
        }

        public void UpdateCriticalControl(CriticalControlV2 criticalControl, bool save = true)
        {
            _repositories.CriticalControlV2Repository.Update(criticalControl);

            if (save)
                _repositories.Save();
        }
    }
}
