﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class SafetyStatV2Core
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SafetyStatV2Core(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IQueryable<SafetyMessageV2> GetAllArchivedSafetyMessages()
        {
            var archivedSafetyMessages = _repositories.SafetyStatV2Repository.GetAll().Select(s => s.SafetyMessage).GroupBy(m => m.Id).Select(m => m.FirstOrDefault());

            return archivedSafetyMessages;
        }

        public SafetyRecipientStateV2 CalcViewingState(SafetyMessageV2 safetyMessage, Guid recipientId, DateTime viewDate)
        {
            SafetyRecipientStateV2 result;
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(recipientId);
            Rotation currentRotation = user.CurrentRotation;

            if (currentRotation == null || currentRotation.State == RotationState.Created)
            {
                result = SafetyRecipientStateV2.NoViewed;
            }
            else if (safetyMessage.Date != viewDate)
            {
                result = SafetyRecipientStateV2.ViewedLate;

                if (currentRotation.ConfirmDate.HasValue && currentRotation.ConfirmDate.Value == viewDate)
                {
                    Rotation prevRotation = currentRotation.PrevRotation;
                    if (prevRotation != null)
                    {
                        DateTime currentStartDate = currentRotation.StartDate.Value;
                        DateTime prevSwingEndDate = prevRotation.StartDate.Value.AddDays(prevRotation.DayOn - 1);

                        if (safetyMessage.Date > prevSwingEndDate && safetyMessage.Date < currentStartDate)
                        {
                            return SafetyRecipientStateV2.Viewed;
                        }
                    }
                }
            }
            else
            {
                result = SafetyRecipientStateV2.Viewed;
            }

            return result;
        }

        public void SetRecipientState(SafetyMessageV2 safetyMessage, Guid recipientId, SafetyRecipientStateV2 state, bool save = true)
        {
            SafetyStatV2 safetyStat = safetyMessage.SafetyStats.FirstOrDefault(ss => ss.RecipientId == recipientId);

            if (safetyStat != null)
            {
                safetyStat.State = state;
                _repositories.SafetyStatV2Repository.Update(safetyStat);

                if (save)
                    _repositories.Save();
            }
        }

        public IEnumerable<SafetyMessageV2> GetActiveNotProcessedSafetyMessages()
        {
            DateTime today = DateTime.Today;

            IEnumerable<SafetyMessageV2> safetyMessages = _repositories.SafetyMessageV2Repository
                                                                        .Find(sm => sm.Date <= today && !sm.SafetyStats.Any())
                                                                        .ToList();

            return safetyMessages;
        }

        public void CreateSafetyStatsForMessage(SafetyMessageV2 safetyMessage, bool save = true)
        {
            var recipients = safetyMessage.CriticalControl.Champions;

            recipients.Add(safetyMessage.CriticalControl.Owner);
            recipients.Add(safetyMessage.CriticalControl.MajorHazard.Owner);

            recipients = recipients.ToList().Where(u => u != null && !u.DeletedUtc.HasValue).GroupBy(r => r.Id).Select(r => r.First()).ToList();

            if (!safetyMessage.SafetyStats.Any())
            {
                foreach (var recipient in recipients.GroupBy(r => r.Id).Select(r => r.First()))
                {
                    SafetyStatV2 safetyMessageStat = new SafetyStatV2
                    {
                        Id = Guid.NewGuid(),
                        Recipient = recipient,
                        SafetyMessage = safetyMessage,
                        State = SafetyRecipientStateV2.NoViewed
                    };

                    _repositories.SafetyStatV2Repository.Add(safetyMessageStat);
                }

                if (save)
                {
                    _repositories.Save();
                }
            }
        }

    }
}
