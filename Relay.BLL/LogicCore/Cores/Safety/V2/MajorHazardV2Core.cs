﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class MajorHazardV2Core
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public MajorHazardV2Core(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IEnumerable<MajorHazardV2> GetAllMajorHazards()
        {
            IEnumerable<MajorHazardV2> majorHazards = _repositories.MajorHazardV2Repository.GetAll().ToList();

            return majorHazards;
        }

        public MajorHazardV2 GetMajorHazard(Guid majorHazardId)
        {
            MajorHazardV2 majorHazard = _repositories.MajorHazardV2Repository.Get(majorHazardId);

            if(majorHazard == null)
                throw new ObjectNotFoundException(string.Format("MajorHazardV2 with Id: {0} was not found.", majorHazardId));

            return majorHazard;
        }

        public void UpdateMajorHazard(MajorHazardV2 majorHazard, bool save = true)
        {
            _repositories.MajorHazardV2Repository.Update(majorHazard);

            if (save)
                _repositories.Save();
        }
    }
}
