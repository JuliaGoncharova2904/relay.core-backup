﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class SafetyMessageCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SafetyMessageCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public IEnumerable<SafetyMessage> GetSafetyMessagesForUser(DateTime date, UserProfile user)
        {
            IEnumerable<SafetyMessage> safetyMessages = _repositories.SafetyMessageRepository
                                                                    .Find(sm => sm.Date == date &&
                                                                                sm.Teams.Any(t => t.Id == user.TeamId) &&
                                                                                sm.Users.All(u => u.Id != user.Id))
                                                                    .ToList();
            return safetyMessages;
        }

        public int CountSafetyMessagesForUser(UserProfile user)
        {
            return _repositories.SafetyMessageRepository.Find(sm => sm.Teams.Any(t => t.Id == user.TeamId) &&
                                                                    sm.Users.All(u => u.Id != user.Id))
                                                        .Count();
        }

    }
}
