﻿using System;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TopicHandoverCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;


        public TopicHandoverCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        public RotationTopic GetOrCreateChildTopic(Guid topicGroupId, RotationTopic parenTopic, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            var childTopic = GetChildTopic(topicGroupId, parenTopic) ?? AddChildRotationTopic(topicGroupId, parenTopic, finalizeStatus);

            return childTopic;
        }


        private RotationTopic GetChildTopic(Guid rotationTopicGroupId, RotationTopic parenTopic)
        {

            var childTopic = _repositories.RotationTopicRepository.Find(t => t.RotationTopicGroupId == rotationTopicGroupId
                                     && t.ParentTopicId.HasValue
                                     && t.ParentTopicId == parenTopic.Id ||
                                     t.RotationTopicGroupId == rotationTopicGroupId
                                     && t.AncestorTopicId.HasValue
                                     && t.AncestorTopicId == parenTopic.AncestorTopicId ||
                                        t.RotationTopicGroupId == rotationTopicGroupId
                                        && t.AncestorTopicId.HasValue
                                        && t.AncestorTopicId == parenTopic.Id).FirstOrDefault();

            return childTopic;
        }

        private RotationTopic AddChildRotationTopic(Guid topicGroupId, RotationTopic parentTopic, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            Guid? assignedToId = null;

            if (parentTopic.RotationTopicGroup.RotationModule.RotationId.HasValue)
            {
                assignedToId = parentTopic.RotationTopicGroup.RotationModule.Rotation.NextRotationId.HasValue ?
                                                            parentTopic.RotationTopicGroup.RotationModule.Rotation.NextRotation.DefaultBackToBackId :
                                                            parentTopic.AssignedToId;
            }
            else if (parentTopic.RotationTopicGroup.RotationModule.ShiftId.HasValue)
            {
                assignedToId = parentTopic.RotationTopicGroup.RotationModule.Shift.ShiftRecipientId;
            }
            var newTopic = parentTopic.Clone();
            newTopic.RotationTopicGroupId = topicGroupId;
            newTopic.AssignedToId = assignedToId;
            newTopic.TempateTopicId = null;

            _repositories.RotationTopicRepository.Add(newTopic);

            _repositories.Save();

            parentTopic.SuccessorTopicId = newTopic.Id;

            _repositories.RotationTopicRepository.Update(parentTopic);

            _repositories.Save();

            return newTopic;
        }


    }
}
