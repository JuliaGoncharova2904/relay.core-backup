﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class TopicDeleteCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        private readonly TaskDeleteCore _taskDeleteCore;

        public TopicDeleteCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;

            this._taskDeleteCore = new TaskDeleteCore(repositoriesUnitOfWork, _logicCore);
        }

        public void RemoveTopicAndRelations(RotationTopic topic, bool save = true)
        {
            _repositories.VoiceMessageRepository.Delete(topic.VoiceMessages);
            _repositories.AttachmentRepository.Delete(topic.Attachments);
            _repositories.ManagerCommentsRepository.Delete(topic.ManagerComment);
            topic.IsRecipientsFromPrevRotation = false;
           // _logicCore.RotationTopicSharingRelationCore.DeleteRotationTopicSharingRelations(topic);


            var oldRelatopns = _repositories.TopicSharingRelationRepository.Find(t => t.SourceTopicId == topic.Id).ToList();
            foreach (var oldRelatopn in oldRelatopns)
            {
                _repositories.TopicSharingRelationRepository.Delete(oldRelatopn.Id);
            }

            this.UpdateForkRelation(topic);

            //--- notification -----
            _logicCore.NotificationCore.RemoveNotificationRelation(topic.Id);
            foreach (RotationTask task in topic.RotationTasks)
            {
                _logicCore.NotificationCore.RemoveNotificationRelation(task.Id);
            }
            //----------------------

            _repositories.RotationTaskRepository.Delete(topic.RotationTasks);

            foreach (var rotationTask in topic.RotationTasks)
            {
                _taskDeleteCore.RemoveTaskAndRelations(rotationTask);
            }

            RotationTopic childTopic = topic.SuccessorTopic;

            RotationTopic parentTopic = topic.AncestorTopic;

            if (parentTopic != null)
            {
                parentTopic.SuccessorTopic = null;
                parentTopic.SuccessorTopicId = null;

                _repositories.RotationTopicRepository.Update(parentTopic);
            }

            while (childTopic != null)
            {
                _repositories.VoiceMessageRepository.Delete(childTopic.VoiceMessages);
                _repositories.AttachmentRepository.Delete(childTopic.Attachments);

                _logicCore.NotificationCore.RemoveNotificationRelation(childTopic.Id);

                var topicToDelete = childTopic;

                childTopic = childTopic.SuccessorTopic;

                foreach (var rotationTask in topicToDelete.RotationTasks)
                {
                    _taskDeleteCore.RemoveTaskAndRelations(rotationTask);
                }

                _repositories.RotationTopicRepository.Delete(topicToDelete);
            }

            _repositories.RotationTopicRepository.Delete(topic);

            if (save)
                _repositories.Save();
        }


        private void UpdateForkRelation(RotationTopic topic)
        {
            var parentForkTopic = topic.ForkParentTopic;

            if (parentForkTopic != null)
            {
                if (parentForkTopic.ForkCounter > 0)
                {
                    parentForkTopic.ForkCounter = parentForkTopic.ForkCounter - 1;
                    _repositories.RotationTopicRepository.Update(parentForkTopic);
                }

                foreach (var topicTasks in topic.RotationTasks)
                {
                    var topicForkParentTask = topicTasks.ForkParentTask;

                    if (topicForkParentTask?.ForkCounter > 0)
                    {
                        topicForkParentTask.ForkCounter = topicForkParentTask.ForkCounter - 1;
                        _repositories.RotationTaskRepository.Update(topicForkParentTask);
                    }
                }
            }
        }
    }
}
