﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using System.Data.Entity.Core;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationTopicCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        private readonly TopicForkCore _forkCore;
        private readonly TopicHandoverCore _handoverCore;

        private readonly TopicDeleteCore _deleteCore;

        public RotationTopicCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;

            this._forkCore = new TopicForkCore(repositoriesUnitOfWork, _logicCore);
            this._handoverCore = new TopicHandoverCore(repositoriesUnitOfWork, _logicCore);

            this._deleteCore = new TopicDeleteCore(repositoriesUnitOfWork, _logicCore);
        }


        public IQueryable<RotationTopic> GetAllUserDailyNotes(Guid userId)
        {
            IQueryable<RotationTopic> dailyNotes = _repositories.RotationTopicRepository
                                                                .Find(t => t.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId
                                                                        && t.RotationTopicGroup.RotationModule.Type == TypeOfModule.DailyNote
                                                                        && t.RotationTopicGroup.RotationModule.SourceType == TypeOfModuleSource.Draft);
            return dailyNotes;
        }

        public IQueryable<RotationTopic> GetAllRotationDailyNotes(Guid rotationId)
        {
            IQueryable<RotationTopic> dailyNotes = _repositories.RotationTopicRepository
                                                                .Find(t => t.RotationTopicGroup.RotationModule.RotationId == rotationId
                                                                        && t.RotationTopicGroup.RotationModule.Type == TypeOfModule.DailyNote
                                                                        && t.RotationTopicGroup.RotationModule.SourceType == TypeOfModuleSource.Draft).OrderBy(t => t.Name);
            return dailyNotes;
        }

        public RotationTopic GetDailyNote(Guid dailyNoteId)
        {
            RotationTopic dailyNote = _repositories.RotationTopicRepository.Get(dailyNoteId);

            if (dailyNote == null || dailyNote.RotationTopicGroup.RotationModule.Type != TypeOfModule.DailyNote)
                throw new ObjectNotFoundException(string.Format("DailyNote with Id: {0} was not found.", dailyNoteId));

            return dailyNote;
        }

        public RotationTopic GetTopic(Guid topicId)
        {
            RotationTopic topic = _repositories.RotationTopicRepository.Get(topicId);
            
            if (topic == null)
            {
                return null;
            }
            //throw new ObjectNotFoundException(string.Format("RotationTopic with Id: {0} was not found.", topicId));
            return topic;
        }

        public void UpdateTopic(RotationTopic topic)
        {
            _repositories.RotationTopicRepository.Update(topic);
            _repositories.Save();
        }

        public Guid GetDefaultAssigneeByTopicId(Guid topicId)
        {
            var rotationModule = GetTopic(topicId).RotationTopicGroup.RotationModule;

            if (rotationModule.RotationId.HasValue)
            {
                var rotationBackToBackId = rotationModule.Rotation.DefaultBackToBackId;

                return rotationBackToBackId;
            }

            var shiftRecipientId = rotationModule.Shift.ShiftRecipientId;

            return shiftRecipientId.GetValueOrDefault();
        }


        public IQueryable<RotationTopic> GetActiveRotationTopicsByTemplate(Guid templateTopicId)
        {
            IQueryable<Rotation> activeRotation = _logicCore.RotationCore.GetAllActiveRotations();

            IQueryable<RotationTopic> topics = activeRotation.SelectMany(r => r.RotationModules)
                                                            .SelectMany(rm => rm.RotationTopicGroups)
                                                            .SelectMany(rtg => rtg.RotationTopics)
                                                            .Where(rt => rt.TempateTopicId == templateTopicId);

            return topics;
        }


        public IQueryable<RotationTopic> GetActiveShiftsTopicsByTemplate(Guid templateTopicId)
        {
            IQueryable<Shift> activeShifts = _logicCore.ShiftCore.GetAllActiveShifts();

            IQueryable<RotationTopic> topics = activeShifts.SelectMany(r => r.RotationModules)
                                                            .SelectMany(rm => rm.RotationTopicGroups)
                                                            .SelectMany(rtg => rtg.RotationTopics)
                                                            .Where(rt => rt.TempateTopicId == templateTopicId);

            return topics;
        }

        public RotationTopic GetRotationTopic(Guid templateTopicId, Guid rotationTopicGroupId)
        {
            var topic = _repositories.RotationTopicRepository.Find(t => t.TempateTopicId == templateTopicId && t.RotationTopicGroupId == rotationTopicGroupId && !t.DeletedUtc.HasValue).FirstOrDefault();

            return topic;
        }

        public IQueryable<RotationTopic> GetAllRotationTopic(Guid rotationId, TypeOfModuleSource sourceType)
        {
            var topics = _repositories.RotationTopicRepository.Find(t => t.RotationTopicGroup.RotationModule.RotationId == rotationId
                                                                        && t.RotationTopicGroup.RotationModule.SourceType == sourceType
                                                                        && t.Enabled);
            return topics;
        }

        public RotationTopic AddRotationTopicFromTemplateTopic(TemplateTopic templateTopic, Guid rotationTopicGroupId, IEnumerable<RotationTopicSharingRelation> TopicSharingRelations)
        {
            var topic = new RotationTopic
            {
                Id = Guid.NewGuid(),
                Enabled = templateTopic.Enabled,
                Description = templateTopic.Description,
                Name = templateTopic.Name,
                TempateTopicId = templateTopic.Id,
                RotationTopicGroupId = rotationTopicGroupId,
                AssignedToId = _logicCore.RotationTopicGroupCore.GetDefaultAssigneeByTopicGroupId(rotationTopicGroupId),
                TopicSharingRelations = TopicSharingRelations != null ? TopicSharingRelations.ToList() : null,
                UnitsSelectedType = templateTopic.UnitsSelectedType,
                PlannedActualHas = templateTopic.PlannedActualHas
            };

            topic.IsRecipientsFromPrevRotation = TopicSharingRelations != null && TopicSharingRelations.Any();

            if (templateTopic.TopicGroup.Module.Type == TypeOfModule.Team)
            {
                topic.RelationId =
                    _logicCore.RotationTopicGroupCore.GetDefaultAssigneeByTopicGroupId(rotationTopicGroupId);
            }


            _repositories.RotationTopicRepository.Add(topic);
            _repositories.Save();

            return topic;
        }

        public RotationTopic GetOrCreateRotationTopic(Guid rotationTopicGroupId, TemplateTopic templateTopic, IEnumerable<RotationTopicSharingRelation> TopicSharingRelations = null)
        {
            var rotationTopic = GetRotationTopic(templateTopic.Id, rotationTopicGroupId) ??
                                     AddRotationTopicFromTemplateTopic(templateTopic, rotationTopicGroupId, TopicSharingRelations);

            return rotationTopic;
        }

        public bool IsTopicExist(Guid topicId)
        {
            return _repositories.RotationTopicRepository.Find(t => t.Id == topicId).Any();
        }

        public bool IsTopicExist(Guid topicGroupId, Guid topicId, string name)
        {
            return _repositories.RotationTopicRepository.Find(t => t.Id != topicId && t.RotationTopicGroupId == topicGroupId && t.Name.Equals(name)).Any();
        }


        public RotationTopic CleaRotationTopicData(RotationTopic topic, bool save = true)
        {
            topic.IsFeedbackRequired = false;
            topic.Description = null;
            topic.RotationTasks.Clear();
            topic.Attachments.Clear();
            topic.VoiceMessages.Clear();
            topic.IsPinned = topic.IsPinned != null ? false : (bool?)null;
            topic.ShareSourceTopicId = null;
            topic.Actual = null;
            topic.Planned = null;

            _logicCore.RotationTopicSharingRelationCore.DeleteRotationTopicSharingRelations(topic);


            if (topic.LocationId.HasValue)
            {
                topic.Location = null;
                _repositories.LocationRepository.Delete(topic.LocationId.Value);
            }

            _repositories.RotationTopicRepository.Update(topic);

            if (save)
                _repositories.Save();

            return topic;
        }


        public string GetTopicManagerComments(Guid topicId)
        {
            var topic = GetTopic(topicId);

            return topic.ManagerComments;
        }


        public RotationTopic AddRotationTopic(AddRotationTopicViewModel rotationTopicModel)
        {
            RotationTopic topic = new RotationTopic
            {
                Id = Guid.NewGuid(),
                CreatorId = rotationTopicModel.CreatorId,
                RotationTopicGroupId = rotationTopicModel.RotationTopicGroupId,
                AssignedToId = rotationTopicModel.AssignedToId,
                Description = rotationTopicModel.Notes,
                RelationId = rotationTopicModel.RelationId,
                Name = rotationTopicModel.Name,
                Enabled = true,
                IsPinned = false,
                RotationTasks = new HashSet<RotationTask>()
            };

            _repositories.RotationTopicRepository.Add(topic);
            _repositories.Save();

            _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(rotationTopicModel.CreatorId).FullName}</strong> created the topic.", rotationTopicModel.CreatorId);

            return topic;
        }

        public RotationTopic GetOrCreatePinTopic(Guid topicGroupId, RotationTopic parentTopic, Guid? assignedToId = null)
        {
            return _forkCore.GetOrCreatePinTopic(topicGroupId, parentTopic, assignedToId);
        }

        public RotationTopic GetOrCreateChildTopic(Guid topicGroupId, RotationTopic parenTopic, StatusOfFinalize finalizeStatus = StatusOfFinalize.NotFinalized)
        {
            return _handoverCore.GetOrCreateChildTopic(topicGroupId, parenTopic, finalizeStatus);
        }



        public void ChangeFinalizeStatusFoRotationTopic(RotationTopic topic, StatusOfFinalize status, bool save = true)
        {
            topic.FinalizeStatus = status;

            _repositories.RotationTopicRepository.Update(topic);

            if (save)
            {
                _repositories.Save();
            }

        }

        public RotationTopic CarryforwardTopic(RotationTopic topic, bool save = true)
        {
            return _forkCore.CarryforwardTopic(topic);
        }

        public bool RemoveTopic(RotationTopic topic, bool save = true)
        {
            if (topic.RotationTopicGroup.RotationModule.ShiftId.HasValue? topic.RotationTopicGroup.RotationModule.Shift.Rotation.State == RotationState.Expired :
                                                                                    topic.RotationTopicGroup.RotationModule.Rotation.State == RotationState.Expired)
                return false;

            _deleteCore.RemoveTopicAndRelations(topic, save);

            return true;
        }

        public RotationTopic AddShiftTopic(AddShiftTopicViewModel shiftTopicModel)
        {
            RotationTopicGroup topicGroup = _logicCore.RotationTopicGroupCore.GetTopicGroup(shiftTopicModel.RotationTopicGroupId);
            topicGroup.Enabled = true;

            RotationTopic topic = new RotationTopic
            {
                Id = Guid.NewGuid(),
                RotationTopicGroupId = shiftTopicModel.RotationTopicGroupId,
                AssignedToId = shiftTopicModel.AssignedToId,
                Description = shiftTopicModel.Notes,
                RelationId = shiftTopicModel.RelationId,
                Name = shiftTopicModel.Name,
                Enabled = true,
                IsPinned = false,
                CreatorId = shiftTopicModel.CreatorId
            };

            _repositories.RotationTopicRepository.Add(topic);
            _repositories.Save();

            _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(shiftTopicModel.CreatorId).FullName}</strong> created the topic.", shiftTopicModel.CreatorId);

            return topic;
        }

        public bool RemoveShiftTopic(RotationTopic topic, bool save = true)
        {
            if (topic.RotationTopicGroup.RotationModule.Shift.Rotation.State == RotationState.Expired)
                return false;

            _deleteCore.RemoveTopicAndRelations(topic, save);

            return true;
        }

        /// <summary>
        /// Get all shift topic by shiftId and source type 
        /// </summary>
        /// <param name="shiftId"></param>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        public IQueryable<RotationTopic> GetAllShiftTopic(Guid shiftId, TypeOfModuleSource sourceType)
        {
            var topics = _repositories.RotationTopicRepository.Find(t => t.RotationTopicGroup.RotationModule.ShiftId == shiftId
                                                                        && t.RotationTopicGroup.RotationModule.SourceType == sourceType
                                                                        && t.Enabled);

            return topics;
        }





        public UserProfile GetTopicOwner(RotationTopic topic)
        {
            UserProfile owner = null;

            RotationModule module;

            //--- Get owner module -----
            if (topic.AncestorTopicId.HasValue)
            {
                module = topic.AncestorTopic.RotationTopicGroup.RotationModule;
            }
            else if (topic.ShareSourceTopicId.HasValue)
            {
                module = topic.ShareSourceTopic.RotationTopicGroup.RotationModule;
            }
            else
            {
                module = topic.RotationTopicGroup.RotationModule;
            }



            //--- shift or rotation ----
            if (module.RotationId.HasValue)
            {
                owner = module.Rotation.RotationOwner;
            }
            else if (module.ShiftId.HasValue)
            {
                if (module.Shift.Rotation == null)
                {
                    var rotaion = _repositories.RotationRepository.Get(module.Shift.RotationId);

                    owner = rotaion.RotationOwner;
                }
                else
                {
                    owner = module.Shift.Rotation.RotationOwner;
                }
            }

            return owner;
        }


        public TemplateTopic GetTemplateTopic(Guid topicId)
        {
            return _repositories.TemplateTopicRepository.Get(topicId);
        }
    }
}
