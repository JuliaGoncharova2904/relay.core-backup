﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.BLL.LogicCore.Cores
{
    public class RotationTopicLogCore
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;

        public RotationTopicLogCore(IRepositoriesUnitOfWork repositoriesUnitOfWork, LogicCoreUnitOfWork logicCore)
        {
            this._repositories = repositoriesUnitOfWork;
            this._logicCore = logicCore;
        }

        /// <summary>
        /// Add new message to topic log
        /// </summary>
        /// <param name="topic">Rotation Topic entity</param>
        /// <param name="message">string messege, example: $"<strong> {RotationTopicOwnerFullName} </strong> created the topic." or simple message like: "Topic created."</param>
        /// <param name="userId">Guid? userId can be null, Id user who change topic</param>
        /// <param name="save">bool save</param>
        public void AddMessageToTopicLog(RotationTopic topic, string message, Guid? userId = null, bool save = true)
        {
            RotationTopicLog topicLog = new RotationTopicLog
            {
                Id = Guid.NewGuid(),
                LogDate = DateTime.UtcNow,
                LogMessage = message,
                TopicId = topic.Id,
                RootTopicId = topic.ParentTopicId ?? topic.Id,
                UserId = userId
            };

            _repositories.TopicLogRepository.Add(topicLog);

            if (save)
            {
                _repositories.Save();
            }

        }

        /// <summary>
        /// Get all logs messeges for topic
        /// </summary>
        /// <param name="topicId">Guid RotationTopicId</param>
        /// <returns>IEnumerable strings</returns>
        public IEnumerable<string> GetTopicLogs(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var rootTaskId = topic.ParentTopicId ?? topic.Id;

            var topicLogs = _repositories.TopicLogRepository.Find(log => log.RootTopicId == rootTaskId).ToList().OrderBy(log => log.LogDate);

            return topicLogs.Select(log => log.LogMessage + " " + log.LogDate.FormatWithMonth()).ToList();
        }

    }
}
