﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Roles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.LogicCore.AccessValidation
{
    public static class UserAccessValidation
    {
        public static bool HasAccessToProject(this UserProfile user, IEnumerable<string> roles, Guid projectId)
        {
            return roles.Contains(iHandoverRoles.Relay.Administrator)
                    || roles.Contains(iHandoverRoles.Relay.iHandoverAdmin)
                    || roles.Contains(iHandoverRoles.Relay.HeadLineManager)
                    || roles.Contains(iHandoverRoles.Relay.LineManager)
                    || user.ManagedProjects.Any(p => p.Id == projectId);
        }

        public static bool IsOwnerOfTask(this UserProfile user, RotationTask task)
        {
            bool result = false;

            switch(task.TaskBoardTaskType)
            {
                case TaskBoardTaskType.NotSet:
                    RotationModule module = task.RotationTopic.RotationTopicGroup.RotationModule;
                    Guid ownerId = module.RotationId.HasValue
                                    ? module.Rotation.RotationOwnerId
                                    : module.Shift.Rotation.RotationOwnerId;
                    result = user.Id == ownerId;
                    break;
                case TaskBoardTaskType.Draft:
                case TaskBoardTaskType.Pending:
                case TaskBoardTaskType.MyTask:
                    result = user.Id == task.TaskBoardId;
                    break;
                case TaskBoardTaskType.Received:
                    result = user.Id == task.AncestorTask.TaskBoardId;
                    break;
                default:
                    throw new Exception("Unsupported task type of TaskBoard.");
            }

            return result;
        }


    }
}
