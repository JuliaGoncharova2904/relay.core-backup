﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.ApiServices;
using MomentumPlus.Relay.Models.Api;

namespace MomentumPlus.Relay.BLL.Services.Api
{
    public class NotificationService : INotificationService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public NotificationService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services = null)
        {
            this._mapper = ApiAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public PagedResult<NotificationViewModel> GetNotificationsByUser(Guid userId, int page, int pageSize)
        {
            IEnumerable<Notification> notifications = _logicCore.NotificationCore.GetNotificationsByUser(userId, page - 1, pageSize);

            int totalCount = _logicCore.NotificationCore.GetTotalCountNotificationByUser(userId);

            return new PagedResult<NotificationViewModel>(_mapper.Map<IEnumerable<NotificationViewModel>>(notifications), page, pageSize, totalCount);
        }

        /// <summary>
        /// Make Last Notifications Shown for User
        /// </summary>
        /// <param name="userId">User Id</param>
        public void MakeLastNotificationsShownForUser(Guid userId)
        {
            _logicCore.NotificationCore.MakeLastNotificationsShownForRecipient(userId);
        }
    }
}
