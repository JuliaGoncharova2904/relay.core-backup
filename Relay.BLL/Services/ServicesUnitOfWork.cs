﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.BLL.Services.Web;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces.Logic;

namespace MomentumPlus.Relay.BLL.Services
{
    public class ServicesUnitOfWork : IServicesUnitOfWork
    {
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthBridge _authBridge;

        //////////////////////////////////////////////


        private IUserService _userService;

        private ITeamService _teamService;

        private IRotationService _rotationService;

        private ISiteService _siteService;

        private IPositionService _positionService;

        private IMediaService _mediaService;

        private IAttachmentService _attachmentService;

        private IVoiceMessageService _voiceMessageService;

        private IRotationPatternService _rotationPatternService;

        private IDailyNoteService _dailyNoteService;

        private IModuleService _moduleService;

        private ITopicGroupService _topicGroupService;

        private ITopicService _topicService;

        private ITaskService _taskService;

        private IProjectService _projectService;

        private ITemplateService _templateService;

        private IRotationModuleService _rotationModuleService;

        private IRotationTopicGroupService _rotationTopicGroupService;

        private IRotationTopicService _rotationTopicService;

        private IRotationTaskService _rotationTaskService;

        private IReportService _reportService;

        private ILocationService _locationService;
        // safety
        private IMajorHazardService _majorHazardService;

        private ICriticalControlService _criticalControlService;

        private ISafetyMessageService _safetyMessageService;
        // safety v2
        private IMajorHazardV2Service _majorHazardV2Service;

        private ICriticalControlV2Service _criticalControlV2Service;

        private ISafetyMessageV2Service _safetyMessageV2Service;

        private ISafetyStatV2Service _safetyStatV2Service;
        //
        private IAdministrationService _administrationService;

        private IAccessService _accessService;

        private ITimelineService _timelineService;

        private INotificationService _notificationService;

        private IShiftService _shiftService;

        private IDailyReportService _dailyReportService;

        private ISchedulerService _schedulerService;

        private IGlobalReportsService _globalReportService;

        private ISharingReportService _sharingReportService;

        private ISharingTopicService _sharingTopicService;


        private ICompanyService _companyService;

        private ITaskBoardService _taskBoardService;
        private ITeamRotationsService _teamRotationsService;

        private ITaskLogService _taskLogService;
        private ITopicLogService _topicLogService;

        // Topic Search 
        private ITopicSearchService _topicSearchService;

        private ISearchService _searchService;

        private IRestrictionService _restrictionService;


        // Board Manager Service

        private IBoardManagerService _boardManagerService;

        private IContributorsService _contributorsService;



        // New Task Logic
        private ITaskBoardTaskService _taskBoardTaskService;

        //==================================================

        public ServicesUnitOfWork(ILogicCoreUnitOfWork logicCore,
                                    IAuthBridge authBridge)
        {
            this._logicCore = (LogicCoreUnitOfWork)logicCore;
            this._authBridge = authBridge;

        }

        #region Properties

        public ITaskBoardTaskService TaskBoardTaskService => _taskBoardTaskService ?? (_taskBoardTaskService = new TaskBoardTaskService(this._logicCore, this._authBridge.AuthService));


        public IBoardManagerService BoardManagerService => _boardManagerService ?? (_boardManagerService = new BoardManagerService(this._logicCore, this));

        public ITopicSearchService TopicSearchService => _topicSearchService ?? (_topicSearchService = new TopicSearchService(this._logicCore, this));

        public ISearchService SearchService => _searchService ?? (_searchService = new SearchService(this._logicCore, this, this._authBridge.AuthService));

        public ITaskLogService TaskLogService => _taskLogService ?? (_taskLogService = new TaskLogService(this._logicCore));

        public ICompanyService CompanyService => _companyService ?? (_companyService = new CompanyService(this._logicCore, this));

        public IDailyReportService DailyReportService => _dailyReportService ?? (_dailyReportService = new DailyReportService(this._logicCore, this));

        public ISharingTopicService SharingTopicService => _sharingTopicService ?? (_sharingTopicService = new SharingTopicService(this._logicCore, this._authBridge.AuthService));


        public IAdministrationService AdministrationService => _administrationService ?? (_administrationService = new AdministrationService(this._logicCore, this));


        public IRotationModuleService RotationModuleService => _rotationModuleService ?? (_rotationModuleService = new RotationModuleService(this._logicCore, this));

        public IRotationTopicGroupService RotationTopicGroupService => _rotationTopicGroupService ?? (_rotationTopicGroupService = new RotationTopicGroupService(this._logicCore, this));

        public IRotationTopicService RotationTopicService => _rotationTopicService ?? (_rotationTopicService = new RotationTopicService(this._logicCore, this));

        public IRotationTaskService RotationTaskService => _rotationTaskService ?? (_rotationTaskService = new RotationTaskService(this._logicCore, this));

        public ITemplateService TemplateService => _templateService ?? (_templateService = new TemplateService(this._logicCore, this));

        public IProjectService ProjectService => _projectService ?? (_projectService = new ProjectService(this._logicCore, this, this._authBridge.AuthService));

        public ITopicGroupService TopicGroupService => _topicGroupService ?? (_topicGroupService = new TopicGroupService(this._logicCore, this));

        public ITopicService TopicService => _topicService ?? (_topicService = new TopicService(this._logicCore, this));

        public ITaskService TaskService => _taskService ?? (_taskService = new TaskService(this._logicCore, this));

        public IUserService UserService => _userService ?? (_userService = new UserService(this._logicCore, this, this._authBridge.AuthService));

        public ITeamService TeamService => _teamService ?? (_teamService = new TeamService(this._logicCore, this));

        public IRotationService RotationService => _rotationService ?? (_rotationService = new RotationService(this._logicCore, this, this._authBridge.AuthService));

        public ISiteService SiteService => _siteService ?? (_siteService = new SiteService(this._logicCore, this._authBridge.AuthService));

        public IPositionService PositionService => _positionService ?? (_positionService = new PositionService(this._logicCore, this));

        public IMediaService MediaService => _mediaService ?? (_mediaService = new MediaService(this._logicCore));

        public IRotationPatternService RotationPatternService => _rotationPatternService ?? (_rotationPatternService = new RotationPatternService(this._logicCore, this));

        public IDailyNoteService DailyNoteService => _dailyNoteService ?? (_dailyNoteService = new DailyNoteService(this._logicCore, this));

        public IModuleService ModuleService => _moduleService ?? (_moduleService = new ModuleService(this._logicCore, this));

        public ILocationService LocationService => _locationService ?? (_locationService = new LocationService(this._logicCore));

        public IAttachmentService AttachmentService => _attachmentService ?? (_attachmentService = new AttachmentService(this._logicCore));

        public IReportService ReportService => _reportService ?? (_reportService = new ReportService(this._logicCore, this));

        public IVoiceMessageService VoiceMessageService => _voiceMessageService ?? (_voiceMessageService = new VoiceMessageService(this._logicCore));

        public IMajorHazardService MajorHazardService => _majorHazardService ?? (_majorHazardService = new MajorHazardService(this._logicCore, this));

        public ICriticalControlService CriticalControlService => _criticalControlService ?? (_criticalControlService = new CriticalControlService(this._logicCore, this));

        public ISafetyMessageService SafetyMessageService => _safetyMessageService ?? (_safetyMessageService = new SafetyMessageService(this._logicCore, this));

        public IAccessService AccessService => _accessService ?? (_accessService = new AccessService(this._logicCore, this._authBridge.AuthService));

        public ITimelineService TimelineService => _timelineService ?? (_timelineService = new TimelineService(this._logicCore, this));

        public IMajorHazardV2Service MajorHazardV2Service => _majorHazardV2Service ?? (_majorHazardV2Service = new MajorHazardV2Service(this._logicCore, this, this._authBridge.AuthService));

        public ICriticalControlV2Service CriticalControlV2Service => _criticalControlV2Service ?? (_criticalControlV2Service = new CriticalControlV2Service(this._logicCore, this, this._authBridge.AuthService));

        public ISafetyMessageV2Service SafetyMessageV2Service => _safetyMessageV2Service ?? (_safetyMessageV2Service = new SafetyMessageV2Service(this._logicCore, this, this._authBridge.AuthService));

        public ISafetyStatV2Service SafetyStatV2Service => _safetyStatV2Service ?? (_safetyStatV2Service = new SafetyStatV2Service(this._logicCore, this, this._authBridge.AuthService));

        public INotificationService NotificationService => _notificationService ?? (_notificationService = new NotificationService(this, _logicCore));

        public IShiftService ShiftService => _shiftService ?? (_shiftService = new ShiftService(this._logicCore, this._authBridge.AuthService, this));

        public ISchedulerService SchedulerService => _schedulerService ?? (_schedulerService = new SchedulerService(this._logicCore, this._authBridge.AuthService));

        public IGlobalReportsService GlobalReportService => _globalReportService ?? (_globalReportService = new GlobalReportsService(this._logicCore, this._authBridge.AuthService, this._authBridge.RestrictionProvider));

        public ISharingReportService SharingReportService => _sharingReportService ?? (_sharingReportService = new SharingReportService(this, this._logicCore));

        public ITaskBoardService TaskBoardService => _taskBoardService ?? (_taskBoardService = new TaskBoardService(this._logicCore));

        public ITeamRotationsService TeamRotationsService => _teamRotationsService ?? (_teamRotationsService = new TeamRotationsService(this._logicCore, this, this._authBridge.AuthService));

        public IContributorsService ContributorsService => _contributorsService ?? (_contributorsService = new ContributorsService(this, this._logicCore));

        public ITopicLogService TopicLogService => _topicLogService ?? (_topicLogService = new TopicLogService(this._logicCore));

        public IRestrictionService RestrictionService => _restrictionService ?? (_restrictionService = (this._authBridge.RestrictionProvider == null)
            ? (IRestrictionService)new EmptyRestrictionService()
            : (IRestrictionService)new RestrictionService(this._logicCore, this._authBridge.RestrictionProvider));

        public IAuthService AuthService => this._authBridge.AuthService;

        #endregion
    }
}
