﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Core.Models;
using System.Web.Mvc;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using System.Data.Entity.Core;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TeamService : BaseService, ITeamService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TeamService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services) : base(services.AuthService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public IEnumerable<TeamViewModel> GetAllTeams()
        {
            var teams = _repositories.TeamRepository.GetAll().Where(t => !t.Users.Any() || t.Users.AsQueryable().Any(ApplyUserFilter()));

            return _mapper.Map<IEnumerable<TeamViewModel>>(teams.ToList().OrderBy(t => t.Name));
        }

        public IEnumerable<SelectListItem> GetTeamList()
        {
            var teams = _repositories.TeamRepository.GetAll().Where(t => !t.Users.Any() || t.Users.AsQueryable().Any(ApplyUserFilter()));

            return _mapper.Map<IEnumerable<SelectListItem>>(teams.ToList().OrderBy(t => t.Name));
        }

        public IEnumerable<SelectListItem> GetTeamListByCriticalControl(Guid criticalControlId)
        {
            CriticalControl criticalControl = _repositories.CriticalControlRepository.Get(criticalControlId);
            if (criticalControl != null)
            {
                return _mapper.Map<IEnumerable<SelectListItem>>(criticalControl.Teams.OrderBy(t => t.Name));
            }

            throw new ObjectNotFoundException($"CriticalControl with Id: {criticalControlId} was not found.");
        }

        public string GetTeamName(Guid teamId)
        {
            var teamName = _repositories.TeamRepository.Get(teamId).Name;

            return teamName;
        }

        public bool AddTeam(TeamViewModel team)
        {
            if (!_repositories.TeamRepository.Find(m => m.Name == team.Name).Any())
            {
                Team teamEntity = _mapper.Map<Team>(team);
                teamEntity.Id = Guid.NewGuid();

                _repositories.TeamRepository.Add(teamEntity);
                _repositories.Save();

                return true;
            }

            return false;
        }


        public IEnumerable<SelectListItem> GetTeamMembersList(Guid teamId)
        {
            return _mapper.Map<IEnumerable<SelectListItem>>(_logicCore.TeamCore.GetTeamMembers(teamId));
        }


        public IEnumerable<SelectListItem> GetTeamMembersListWidthOther(Guid teamId)
        {
            IEnumerable<UserProfile> users = _logicCore.TeamCore.GetTeamMembers(teamId).AsQueryable().Where(ApplyUserFilter());
            List<SelectListItem> teamMembers = _mapper.Map<List<SelectListItem>>(users);
            teamMembers = new List<SelectListItem>(teamMembers.OrderBy(t => t.Text))
            {
                new SelectListItem {Text = "Other", Value = Guid.Empty.ToString()}
            };


            return teamMembers;
        }



        public TeamViewModel GetTeam(Guid teamId)
        {
            Team team = _repositories.TeamRepository.Get(teamId);
            if (team != null)
            {
                return _mapper.Map<TeamViewModel>(team);
            }

            return null;
        }

        public bool UpdateTeam(TeamViewModel team)
        {
            if (team.Id.HasValue)
            {
                Team teamEntity = _repositories.TeamRepository.Get(team.Id.Value);
                if (teamEntity != null)
                {
                    _mapper.Map(team, teamEntity);

                    _repositories.TeamRepository.Update(teamEntity);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        //public List<UserProfile> GetUsersByTaskAssigned(Guid userId)
        //{
        //    UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

        //    IEnumerable<Guid> usersIds = _services.AuthService.GetUsersIdsByRole(iHandoverRoles.Relay.iHandoverAdmin, Guid.NewGuid());

        //    IEnumerable<UserProfile> userTeamMembers = _logicCore.TeamCore.GetTeamMembers(user.TeamId)
        //                                                                 .AsQueryable().Where(ApplyUserFilter())
        //                                                                 .Where(UserActiveFilter())
        //                                                                 .Where(u => u.Id != userId)
        //                                                                 .OrderBy(u => u.FirstName).ToList();

        //    return userTeamMembers;
        //}


        public TeammatesDialogViewModel PopulateTeammatesDialogViewModel(Guid userId, bool showMe, bool showOnSite, bool showOffSite, bool showWithoutRotation)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            IEnumerable<Guid> usersIds = _services.AuthService.GetUsersIdsByRole(iHandoverRoles.Relay.iHandoverAdmin, Guid.NewGuid());

            IEnumerable<UserProfile> userTeamMembers = _logicCore.TeamCore.GetTeamMembers(user.TeamId)
                                                                        .AsQueryable().Where(ApplyUserFilter())
                                                                        .Where(UserActiveFilter())
                                                                        .Where(u => u.Id != userId)
                                                                        .OrderBy(u => u.FirstName);

            //var onSiteTeammates = userTeamMembers.Where(u => u.CurrentRotation != null && u.CurrentRotation.State == RotationState.Confirmed)
            //        .Where(u => u.CurrentRotation != null && u.CurrentRotation.RotationShifts.Any() && u.CurrentRotation.RotationShifts
            //        .Where(s => s.StartDateTime.HasValue).OrderBy(s => s.StartDateTime.Value).FirstOrDefault().State == ShiftState.Confirmed);

            //var offSiteTeammates = userTeamMembers.Where(u => u.CurrentRotation != null && u.CurrentRotation.State != RotationState.Confirmed)
            //    .Where(u => u.CurrentRotation != null && u.CurrentRotation.RotationShifts.Any() && u.CurrentRotation.RotationShifts
            //    .Where(s => s.StartDateTime.HasValue).OrderBy(s => s.StartDateTime.Value).FirstOrDefault().State != ShiftState.Confirmed);


            var onSiteTeammates = UsersOnSite(userTeamMembers);

            var offSiteTeammates = UsersOffSite(userTeamMembers);

            var withoutRotation = UsersWithoutRotation(userTeamMembers);

            TeammatesDialogViewModel model = new TeammatesDialogViewModel
            {
                RequestOwner = showMe && !usersIds.Contains(user.Id) ? new TeammateViewModel { Id = user.Id, FullName = user.FullName } : null,
                OnSiteTeammates = showOnSite ? onSiteTeammates.Select(u => new TeammateViewModel { Id = u.Id, FullName = u.FullName }) : null,
                OffSiteTeammates = showOffSite ? offSiteTeammates.Select(u => new TeammateViewModel { Id = u.Id, FullName = u.FullName }) : null,
                WithoutRotation = showWithoutRotation
                                            ? /*withoutRotation.Except(onSiteTeammates).Except(offSiteTeammates)*/
                                            withoutRotation
                                                .Select(u => new TeammateViewModel { Id = u.Id, FullName = u.FullName })
                                            : null
            };

            return model;
        }

        private List<UserProfile> UsersOnSite(IEnumerable<UserProfile> users)
        {
            List<UserProfile> usersOnSite = new List<UserProfile>();
            foreach (var user in users)
            {
                if (user.CurrentRotationId.HasValue)
                {
                    if (_services.TeamRotationsService.IsActiveShiftOrRotation(user.CurrentRotation, user.Id))
                    {
                        usersOnSite.Add(user);
                    }
                }
            }

            return usersOnSite;
        }

        private List<UserProfile> UsersOffSite(IEnumerable<UserProfile> users)
        {
            List<UserProfile> usersOffSite = new List<UserProfile>();
            foreach (var user in users)
            {
                if (user.CurrentRotationId.HasValue)
                {
                    if (!_services.TeamRotationsService.IsActiveShiftOrRotation(user.CurrentRotation, user.Id))
                    {
                        usersOffSite.Add(user);
                    }
                }
            }

            return usersOffSite;
        }


        private List<UserProfile> UsersWithoutRotation(IEnumerable<UserProfile> users)
        {
            List<UserProfile> usersWithoutRotation = new List<UserProfile>();
            foreach (var user in users)
            {
                if (user.CurrentRotation == null /*|| !user.CurrentRotation.RotationShifts.Any()*/)
                {
                    usersWithoutRotation.Add(user);
                }
            }

            return usersWithoutRotation;
        }
    }
}
