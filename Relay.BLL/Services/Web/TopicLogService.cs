﻿using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TopicLogService : ITopicLogService
    {
        private readonly LogicCoreUnitOfWork _logicCore;

        public TopicLogService(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
        }

        public IEnumerable<string> GetTopicLogs(Guid topicId)
        {
            return _logicCore.RotationTopicLogCore.GetTopicLogs(topicId);
        }
    }
}
