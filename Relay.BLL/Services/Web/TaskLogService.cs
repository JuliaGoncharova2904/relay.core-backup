﻿using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TaskLogService : ITaskLogService
    {
        private readonly LogicCoreUnitOfWork _logicCore;

        public TaskLogService(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
        }

        public IEnumerable<string> GetTaskLogs(Guid taskId)
        {
            return _logicCore.RotationTaskLogCore.GetTaskLogs(taskId);
        }
    }
}
