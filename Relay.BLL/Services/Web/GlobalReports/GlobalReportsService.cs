﻿using System;
using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Services.Web.GlobalReports;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class GlobalReportsService : IGlobalReportsService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;
        private readonly IRestrictionProvider _restrictionProvider;

        public GlobalReportsService(LogicCoreUnitOfWork logicCore, IAuthService authService, IRestrictionProvider restrictionProvider)
        {
            this._logicCore = logicCore;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._authService = authService;
            this._restrictionProvider = restrictionProvider;
        }

        /// <summary>
        /// Populate view models for History section.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public IPagedList<HistoryGlobalReportItemViewModel> GetHistoryReportsItemsForUser(Guid userId, int? page, int? pageSize)
        {
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            int? monthRestriction = _restrictionProvider?.IsMonthArchivesLimited() ?? false ? (int?)_restrictionProvider.MonthArchivesLimit() : null;
            HistoryReportsFilter filter = new HistoryReportsFilter(_logicCore, _authService);

            IEnumerable<HistoryGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, userRoles, page, pageSize, monthRestriction);

            return reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize);
        }

        /// <summary>
        /// Populate view models for Received section.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IPagedList<ReceivedGlobalReportItemViewModel> GetReceivedReportsItemsForUser(Guid userId, int? page, int? pageSize)
        {
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            int? monthRestriction = _restrictionProvider?.IsMonthArchivesLimited() ?? false ? (int?)_restrictionProvider.MonthArchivesLimit() : null;
            ReceivedReportsFilter filter = new ReceivedReportsFilter(_logicCore, _authService);

            IEnumerable<ReceivedGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, userRoles, page, pageSize, monthRestriction);

            return reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public IPagedList<SharedGlobalReportItemViewModel> GetSharedReportsItemsForUser(Guid userId, int? page, int? pageSize)
        {
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            int? monthRestriction = _restrictionProvider?.IsMonthArchivesLimited() ?? false ? (int?)_restrictionProvider.MonthArchivesLimit() : null;
            SharedReportsFilter filter = new SharedReportsFilter(_logicCore, _authService);

            IEnumerable<SharedGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, userRoles, page, pageSize, monthRestriction);

            return reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize);
        }


        public IPagedList<SharedTopicGlobalReportItemViewModel> GetSharedTopicItemsForUser(Guid userId, int? page, int? pageSize)
        {
           var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (_authService.GetUserRoles(userId).Any(r => r == iHandoverRoles.Relay.iHandoverAdmin))
            {
                var ownerId = _authService.GetSubscriptionOwnerId();
                user = _logicCore.UserProfileCore.GetUserProfile(ownerId.Value);
            }

            var userRotations = user.WorkRotations;

            var topicsWhichSharedForUser = userRotations.SelectMany(r => r.RotationModules.SelectMany(m => m.RotationTopicGroups.SelectMany(tg => tg.RotationTopics))).Where(t => t.ShareSourceTopicId.HasValue).OrderByDescending(t => t.CreatedUtc).Select(t => t.ShareSourceTopic).ToList();

             if (user.WorkRotations.Count == 0 && user.WorkRotations != null)
             {
                 var rotations = _logicCore.TeamRotationsCore.GetTeamRotationsForAdmin(user).ToList();

                rotations.ForEach(rotation =>
                {
                    var resultTopics = rotation.RotationType == Core.Models.RotationType.Shift ?
                      rotation.RotationShifts.SelectMany(r => r.RotationModules.SelectMany(t => t.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic)))).ToList()
                      : rotation.RotationModules.SelectMany(r => r.RotationTopicGroups.SelectMany(rt => rt.RotationTopics.Where(sh => sh.SharedRecipientId == user.Id).Where(sht => sht.ShareSourceTopicId.HasValue).Select(sht => sht.ShareSourceTopic))).ToList();

                    if (resultTopics != null && resultTopics.Any())
                    {
                        topicsWhichSharedForUser.AddRange(resultTopics);
                    }
                });
                topicsWhichSharedForUser = topicsWhichSharedForUser.Distinct().OrderByDescending(t => t.CreatedUtc).ToList();
             }

            var currentPage = page - 1 ?? 0;
            var currentPageSize = pageSize ?? NumericConstants.Paginator.PaginatorPageSizeForGlobalReport;


            List<SharedTopicGlobalReportItemViewModel> reportItems = topicsWhichSharedForUser.Select(topicWhichSharedForUser => new SharedTopicGlobalReportItemViewModel
            {
                DateString = topicWhichSharedForUser.ModifiedUtc.HasValue ? topicWhichSharedForUser.ModifiedUtc.Value.ToString("dd MMM yy") : topicWhichSharedForUser.CreatedUtc.Value.ToString("dd MMM yy"),
                Id = topicWhichSharedForUser.Id,
                Name = topicWhichSharedForUser.Name,
                Reference = topicWhichSharedForUser.RotationTopicGroup.Name,
                Notes = topicWhichSharedForUser.Description,
                HasAttachments = topicWhichSharedForUser.Attachments.Any(),
                AttachmentsCounter = topicWhichSharedForUser.Attachments.Count + topicWhichSharedForUser.AttachmentsLink.Count,
                TaskCounter = topicWhichSharedForUser.RotationTasks.Count,
                Owner = _logicCore.RotationTopicCore.GetTopicOwner(topicWhichSharedForUser).FullName,
                OwnerId = _logicCore.RotationTopicCore.GetTopicOwner(topicWhichSharedForUser).Id,
                HasComments = topicWhichSharedForUser.ManagerComment.Count > 0 ? true : false,
                ManagerCommentsCounter = topicWhichSharedForUser.ManagerComment.Count
            }).ToList();


            return reportItems.ToPagedList(currentPage, currentPageSize, topicsWhichSharedForUser.Count());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filterUserId"></param>
        /// <param name="filterRotationType"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public GlobalReportsListViewModel GetTeamReportsItemsForUser(Guid userId, Guid? filterUserId, RotationType? filterRotationType, int? page, int? pageSize)
        {
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            int? monthRestriction = _restrictionProvider?.IsMonthArchivesLimited() ?? false ? (int?)_restrictionProvider.MonthArchivesLimit() : null;
            TeamReportsFilter filter = new TeamReportsFilter(_logicCore, _authService);

            IEnumerable<TeamGlobalReportItemViewModel> reportItems = filter.GetReportsPage(userId, userRoles, page, pageSize, monthRestriction, filterRotationType, filterUserId);

            GlobalReportsListViewModel model = new GlobalReportsListViewModel
            {
                Users = _mapper.Map<IEnumerable<DropDownListItemModel>>(filter.Users),
                FilterUserId = filterUserId,
                FilterRotationType = filterRotationType,
                ReportsItems = reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize),
                ShowReports = true
            };

            return model;
        }

        public GlobalReportsListViewModel GetGlobalReportsItemsForUser(Guid userId, Guid? filterUserId, RotationType? filterRotationType, int? page, int? pageSize)
        {
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            int? monthRestriction = _restrictionProvider?.IsMonthArchivesLimited() ?? false ? (int?)_restrictionProvider.MonthArchivesLimit() : null;
            TeamReportsFilter filter = new TeamReportsFilter(_logicCore, _authService);

            IEnumerable<TeamGlobalReportItemViewModel> reportItems = null;

            if (userRoles.Any(r => r == iHandoverRoles.Relay.User))
            {
                reportItems = filter.GetReportsPageForGlobalTab(userId, userRoles, page, pageSize, monthRestriction, filterRotationType, filterUserId);
            }

            else
            {
               reportItems = filter.GetReportsPage(userId, userRoles, page, pageSize, monthRestriction, filterRotationType, filterUserId);
            }

            GlobalReportsListViewModel model = new GlobalReportsListViewModel
            {
                Users = _mapper.Map<IEnumerable<DropDownListItemModel>>(filter.Users),
                FilterUserId = filterUserId,
                FilterRotationType = filterRotationType,
                ReportsItems = reportItems.ToPagedList(filter.Page, filter.PageSize, filter.TotalSize),
                ShowReports = userRoles.Any(r => r == iHandoverRoles.Relay.User) == true ? false : true
            };

            return model;
        }
    }
}
