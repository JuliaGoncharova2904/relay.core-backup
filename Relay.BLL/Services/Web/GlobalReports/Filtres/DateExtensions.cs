﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace iHandover.Relay.BLL.Services.Web.GlobalReports
{
    public static class DateExtensions
    {
        public static string FormatDate(this Rotation rotation)
        {
            if (rotation.State == RotationState.Confirmed)
            {
                return "Live";
            }

            if (rotation.State == RotationState.Created)
            {
                return "-";
            }

            return rotation.StartDate.Value.AddDays(rotation.DayOn).FormatWithDayMonthYear();
        }

        public static string FormatDate(this Shift shift)
        {
            if (shift.State == ShiftState.Confirmed)
            {
                return "Live";
            }

            if (shift.State == ShiftState.Created)
            {
                return "-";
            }

            return shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDayMonthYear();

        }

    }
}
