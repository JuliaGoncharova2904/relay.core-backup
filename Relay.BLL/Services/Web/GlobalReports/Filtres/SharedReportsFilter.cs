﻿using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using System.Collections.Generic;
using iHandover.Relay.BLL.Services.Web.GlobalReports;
using MomentumPlus.Relay.Interfaces.Auth;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public class SharedReportsFilter : ReportsFilteTemplate<SharedGlobalReportItemViewModel>
    {
        public SharedReportsFilter(LogicCoreUnitOfWork logicCore, IAuthService authService) : base(logicCore, authService)
        {
        }

        private Guid _userId;

        protected override IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.SharingReportCore.GetSharedRotationReportsForUser(this._userId);
        }

        protected override IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.SharingReportCore.GetSharedShiftReportsForUser(this._userId);
        }

        protected override IQueryable<UserProfile> BuildUsersQuery(Guid userId, IEnumerable<string> userRoles, Guid? filterUserId)
        {
            this._userId = userId;

            return _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.Id == userId);
        }

        protected override SharedGlobalReportItemViewModel GetReportItemFromRotation(Rotation rotation)
        {
            return new SharedGlobalReportItemViewModel
            {
                CreatedTime = rotation.StartDate.Value,
                DateString = rotation.FormatDate(),
                HandoverCreatorId = rotation.RotationOwnerId,
                HandoverCreatorName = rotation.RotationOwner.FullName,
                HandoverRecipientId = rotation.DefaultBackToBackId,
                HandoverRecipientName = rotation.DefaultBackToBack.FullName,
                PositionName = rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Swing,
                ItemsNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                //TasksNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotation.Id, ModuleSourceType.Draft).Count(),
                SourceId = rotation.Id,
                IsUnfinished = rotation.State == RotationState.Confirmed
            };
        }

        protected override SharedGlobalReportItemViewModel GetReportItemFromShift(Shift shift)
        {
            return new SharedGlobalReportItemViewModel
            {
                CreatedTime = shift.StartDateTime.Value,
                DateString = shift.FormatDate(),
                HandoverCreatorId = shift.Rotation.RotationOwnerId,
                HandoverCreatorName = shift.Rotation.RotationOwner.FullName,
                HandoverRecipientId = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipientId.Value : Guid.Empty,
                HandoverRecipientName = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipient.FullName : string.Empty,
                PositionName = shift.Rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Shift,
                ItemsNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                //TasksNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForShift(shift.Id, ModuleSourceType.Draft).Count(),
                SourceId = shift.Id,
                IsUnfinished = shift.State == ShiftState.Confirmed
            };
        }
    }
}
