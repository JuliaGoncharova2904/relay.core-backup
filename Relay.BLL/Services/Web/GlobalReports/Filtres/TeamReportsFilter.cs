﻿using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Roles;
using System.Collections.Generic;
using iHandover.Relay.BLL.Services.Web.GlobalReports;
using MomentumPlus.Relay.Interfaces.Auth;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public class TeamReportsFilter : ReportsFilteTemplate<TeamGlobalReportItemViewModel>
    {
        private Guid _userId;
        private bool _isAdmin;

        public TeamReportsFilter(LogicCoreUnitOfWork logicCore, IAuthService authService) : base(logicCore, authService)
        {
        }

        protected override IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Swing &&
                                                                            (this._isAdmin || r.LineManagerId == this._userId));
        }

        protected override IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Shift &&
                                                                            (this._isAdmin || r.LineManagerId == this._userId))
                                                                .SelectMany(r => r.RotationShifts)
                                                                .Where(s => s.State != ShiftState.Created);
        }

        protected override IQueryable<UserProfile> BuildUsersQuery(Guid userId, IEnumerable<string> userRoles, Guid? filterUserId)
        {
            //--------- Access rules for user role ------------
            bool isAdmin = userRoles.Contains(iHandoverRoles.Relay.Administrator) || userRoles.Contains(iHandoverRoles.Relay.iHandoverAdmin) || userRoles.Contains(iHandoverRoles.Relay.ExecutiveUser);

            bool isLineManager = userRoles.Contains(iHandoverRoles.Relay.HeadLineManager) || userRoles.Contains(iHandoverRoles.Relay.LineManager);

            if (!isAdmin && !isLineManager)
            {
                //throw new MethodAccessException(string.Format("User with Id: {0} cannot access this method."));
                this._userId = userId;
                this._isAdmin = isAdmin;
                IQueryable<UserProfile> usersList = _logicCore.UserProfileCore.GetRelayEmployees();
                //-------------------------------------------------
                return filterUserId.HasValue ? usersList.Where(u => u.Id == filterUserId.Value) : usersList;

            }

            this._userId = userId;
            this._isAdmin = isAdmin;
            IQueryable<UserProfile> users = _logicCore.UserProfileCore.GetRelayEmployees();
            //-------------------------------------------------
            return filterUserId.HasValue ? users.Where(u => u.Id == filterUserId.Value) : users;
            //-------------------------------------------------
        }

        protected override TeamGlobalReportItemViewModel GetReportItemFromRotation(Rotation rotation)
        {
            return new TeamGlobalReportItemViewModel
            {
                CreatedTime = rotation.StartDate.Value,
                DateString = rotation.FormatDate(),
                HandoverCreatorId = rotation.RotationOwnerId,
                HandoverCreatorName = rotation.RotationOwner.FullName,
                HandoverRecipientId = rotation.DefaultBackToBackId,
                HandoverRecipientName = rotation.DefaultBackToBack.FullName,
                PositionName = rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Swing,
                ItemsNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                //TasksNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotation.Id, ModuleSourceType.Draft).Count(),
                SourceId = rotation.Id,
                IsUnfinished = rotation.State == RotationState.Confirmed
            };
        }

        protected override TeamGlobalReportItemViewModel GetReportItemFromShift(Shift shift)
        {
            return new TeamGlobalReportItemViewModel
            {
                CreatedTime = shift.StartDateTime.Value,
                DateString = shift.FormatDate(),
                HandoverCreatorId = shift.Rotation.RotationOwnerId,
                HandoverCreatorName = shift.Rotation.RotationOwner.FullName,
                HandoverRecipientId = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipientId.Value : Guid.Empty,
                HandoverRecipientName = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipient.FullName : string.Empty,
                PositionName = shift.Rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Shift,
                ItemsNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                //TasksNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForShift(shift.Id, ModuleSourceType.Draft).Count(),
                SourceId = shift.Id,
                IsUnfinished = shift.State == ShiftState.Confirmed,
                ShiftType = _logicCore.TaskBoardTasksCore.GetShiftType(shift, shift.Rotation)
            };
        }

    }
}
