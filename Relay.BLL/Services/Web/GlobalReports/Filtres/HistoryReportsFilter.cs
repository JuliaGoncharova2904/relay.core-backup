﻿using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using System.Collections.Generic;
using iHandover.Relay.BLL.Services.Web.GlobalReports;
using MomentumPlus.Relay.Interfaces.Auth;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public class HistoryReportsFilter : ReportsFilteTemplate<HistoryGlobalReportItemViewModel>
    {
        public HistoryReportsFilter(LogicCoreUnitOfWork logicCore, IAuthService authService) : base(logicCore, authService)
        {
        }

        protected override IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Swing);
        }

        protected override IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Shift)
                                                                .SelectMany(r => r.RotationShifts)
                                                                .Where(s => s.State != ShiftState.Created);
        }

        protected override IQueryable<UserProfile> BuildUsersQuery(Guid userId, IEnumerable<string> userRoles, Guid? filterUserId)
        {
            return _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.Id == userId);
        }

        protected override HistoryGlobalReportItemViewModel GetReportItemFromRotation(Rotation rotation)
        {
            return new HistoryGlobalReportItemViewModel
            {
                CreatedTime = rotation.StartDate.Value,
                DateString = rotation.FormatDate(),
                HandoverToId = rotation.DefaultBackToBackId,
                HandoverToName = rotation.DefaultBackToBack.FullName,
                PositionName = rotation.DefaultBackToBack.Position.Name,
                RotationType = Models.RotationType.Swing,
                ItemsNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                //TasksNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotation.Id, ModuleSourceType.Draft).Count(),
                SourceId = rotation.Id,
                IsUnfinished = rotation.State == RotationState.Confirmed
            };
        }

        protected override HistoryGlobalReportItemViewModel GetReportItemFromShift(Shift shift)
        {
            return new HistoryGlobalReportItemViewModel
            {
                CreatedTime = shift.StartDateTime.Value,
                DateString = shift.FormatDate(),
                HandoverToId = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipientId.Value : Guid.Empty,
                HandoverToName = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipient.FullName : string.Empty,
                PositionName = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipient.Position.Name : string.Empty,
                RotationType = Models.RotationType.Shift,
                ItemsNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                                                .Count(),
                //TasksNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForShift(shift.Id, ModuleSourceType.Draft).Count(),
                SourceId = shift.Id,
                IsUnfinished = shift.State == ShiftState.Confirmed,
                ShiftType = _logicCore.TaskBoardTasksCore.GetShiftType(shift, shift.Rotation)
            };
        }
    }
}
