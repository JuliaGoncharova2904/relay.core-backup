﻿using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using System.Collections.Generic;
using MomentumPlus.Relay.Interfaces.Auth;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public class ReceivedReportsFilter : ReportsFilteTemplate<ReceivedGlobalReportItemViewModel>
    {
        public ReceivedReportsFilter(LogicCoreUnitOfWork logicCore, IAuthService authService) : base(logicCore, authService)
        {
        }

        private Guid _userId;

        protected override IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Swing)
                                                                .SelectMany(r => r.HandoverFromRotations);
        }

        protected override IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users)
        {
            return _logicCore.RotationCore.GetAllRotationsForUsers(users.Select(u => u.Id).ToList())
                                                                .Where(r => r.State != RotationState.Created &&
                                                                            r.RotationType == Core.Models.RotationType.Shift)
                                                                .SelectMany(r => r.RotationShifts)
                                                                .Where(s => s.State != ShiftState.Created)
                                                                .SelectMany(s => s.HandoverFromShifts);
        }

        protected override IQueryable<UserProfile> BuildUsersQuery(Guid userId, IEnumerable<string> userRoles, Guid? filterUserId)
        {
            this._userId = userId;

            return _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.Id == userId);
        }

        protected override ReceivedGlobalReportItemViewModel GetReportItemFromRotation(Rotation rotation)
        {
            var source = rotation.HandoverToRotations.Where(r => r.RotationOwnerId == this._userId).First();

            var rotationModules = _logicCore.RotationModuleCore.GetRotationModule(source.Id, TypeOfModuleSource.Received);

            List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

            foreach (var rotationModule in rotationModules)
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                        .Where(tg => tg.Enabled)
                                                                        .SelectMany(tg => tg.RotationTopics)
                                                                        .Where(t => t.Enabled);

                if (rotationModule.Type == TypeOfModule.Team)
                {
                    var topicsGroupByRelation = rotationTopics.Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.RelationId.Value).ToList().Select(t => t.FirstOrDefault()).ToList();

                    topicsGroupByRelation.ForEach(topicGroupByRelation =>
                    {
                        var topics = _logicCore.RotationModuleCore.GetTopicsByRelationId(topicGroupByRelation.RelationId.Value, topicGroupByRelation.RotationTopicGroupId);
                        sortedRotationTopics.AddRange(topics);
                    });
                }
                else
                {
                    var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.OrderByDescending(r => r.RotationTopics.Count).FirstOrDefault()).ToList();

                    if (topicGroupsByName != null)
                    {
                        topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                        {
                            var topics = topicGroup.RotationTopics.ToList();

                            sortedRotationTopics.AddRange(topics);
                        });
                    }
                }
            }

            var count = sortedRotationTopics.Count();

            return new ReceivedGlobalReportItemViewModel
            {
                CreatedTime = rotation.StartDate.Value,
                DateString = rotation.StartDate.Value.AddDays(rotation.DayOn).FormatWithDayMonthYear(),
                ReceivedFromId = rotation.RotationOwnerId,
                ReceivedFromName = rotation.RotationOwner.FullName,
                PositionName = rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Swing,
                ItemsNumber = count,
                //TasksNumber = rotation.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks.Where(ts => ts.Enabled))
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotation.Id, ModuleSourceType.Draft).Count(),
                SourceId = rotation.HandoverToRotations.Where(r => r.RotationOwnerId == this._userId).First().Id,
                FromSourceId = rotation.Id,
                IsUnfinished = rotation.State == RotationState.Confirmed
            };
        }

        protected override ReceivedGlobalReportItemViewModel GetReportItemFromShift(Shift shift)
        {
            var source = shift.HandoverToShifts.Where(s => s.Rotation.RotationOwnerId == this._userId).First();

            var rotationModules = _logicCore.RotationModuleCore.GetShiftModule(source.Id, TypeOfModuleSource.Received);

            List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

            foreach (var rotationModule in rotationModules)
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                        .Where(tg => tg.Enabled)
                                                                        .SelectMany(tg => tg.RotationTopics)
                                                                        .Where(t => t.Enabled);

                if (rotationModule.Type == TypeOfModule.Team)
                {
                    var topicsGroupByRelation = rotationTopics.Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.RelationId.Value).ToList().Select(t => t.FirstOrDefault()).ToList();

                    topicsGroupByRelation.ForEach(topicGroupByRelation =>
                    {
                        var topics = _logicCore.RotationModuleCore.GetTopicsByRelationId(topicGroupByRelation.RelationId.Value, topicGroupByRelation.RotationTopicGroupId);
                        sortedRotationTopics.AddRange(topics);
                    });
                }
                else
                {
                    var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.OrderByDescending(r => r.RotationTopics.Count).FirstOrDefault()).ToList();

                    if (topicGroupsByName != null)
                    {
                        topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                        {
                            var topics = topicGroup.RotationTopics.ToList();

                            sortedRotationTopics.AddRange(topics);
                        });
                    }
                }
            }

            var count = sortedRotationTopics.Count();

            return new ReceivedGlobalReportItemViewModel
            {
                CreatedTime = shift.StartDateTime.Value,
                DateString = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDayMonthYear(),
                ReceivedFromId = shift.Rotation.RotationOwnerId,
                ReceivedFromName = shift.Rotation.RotationOwner.FullName,
                PositionName = shift.Rotation.RotationOwner.Position.Name,
                RotationType = Models.RotationType.Shift,
                ItemsNumber = count,
                //ItemsNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .Count(),
                //TasksNumber = shift.RotationModules.Where(rm => rm.Enabled && rm.SourceType == TypeOfModuleSource.Draft)
                //                                .SelectMany(rm => rm.RotationTopicGroups.Where(tg => tg.Enabled))
                //                                .SelectMany(tg => tg.RotationTopics.Where(t => t.Enabled))
                //                                .SelectMany(t => t.RotationTasks)
                //                                .Count(),
                TasksNumber = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForShift(shift.Id, ModuleSourceType.Draft).Count(),
                SourceId = shift.HandoverToShifts.Where(s => s.Rotation.RotationOwnerId == this._userId).First().Id,
                FromSourceId = shift.Id,
                IsUnfinished = shift.State == ShiftState.Confirmed
            };
        }
    }
}
