﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web.GlobalReports
{
    public abstract class ReportsFilteTemplate<TModel> where TModel : BaseGlobalReportItemViewModel
    {
        protected class ReportItem
        {
            public Guid Id;
            public Models.RotationType Type;
            public DateTime CreatedTime;
        }

        private int? _monthRestriction;
        protected IQueryable<UserProfile> _users;
        protected readonly LogicCoreUnitOfWork _logicCore;
        protected readonly IAuthService _authService;

        public IEnumerable<UserProfile> Users
        {
            get
            {
                if (_users != null)
                    return _users.ToList().OrderBy(u => u.FullName);
                else
                    return new UserProfile[0];
            }
        }
        public int Page { get; private set; }
        public int PageSize { get; private set; }
        public int TotalSize { get; private set; }

        public ReportsFilteTemplate(LogicCoreUnitOfWork logicCore, IAuthService authService)
        {
            this._logicCore = logicCore;
            this._authService = authService;
        }


        public IEnumerable<TModel> GetReportsPage(Guid userId, IEnumerable<string> userRoles, int? page, int? pageSize, int? monthRestriction = null, Models.RotationType? filterRotationType = null, Guid? filterUserId = null)
        {
            List<TModel> reportsItems = new List<TModel>();

            this._monthRestriction = monthRestriction;
            this.Page = page - 1 ?? 0;
            this.PageSize = pageSize ?? NumericConstants.Paginator.PaginatorPageSizeForGlobalReport;
            this._users = _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.CurrentRotation != null);

            IQueryable<UserProfile> users = this.BuildUsersQuery(userId, userRoles, filterUserId);

            if (AppSession.Current.IsMultiTenant)
            {
                IEnumerable<Guid> usersIds = _authService.GetUsersIdsByRole(iHandoverRoles.Relay.iHandoverAdmin, Guid.NewGuid());

                this._users = _users.Where(u => usersIds.Contains(u.Id) == false);

                users = this.BuildUsersQuery(userId, userRoles, filterUserId).Where(u => usersIds.Contains(u.Id) == false);
            }

            IEnumerable<ReportItem> reportItemsPage = this.ReportsPagination(users, filterRotationType, this.Page, this.PageSize);

            IQueryable<Shift> shiftQuery = _logicCore.ShiftCore.GetShiftsByIds(reportItemsPage.Where(ri => ri.Type == Models.RotationType.Shift).Select(ri => ri.Id));

            IQueryable<Rotation> rotationsQuery = _logicCore.RotationCore.GetRotationsByIds(reportItemsPage.Where(ri => ri.Type == Models.RotationType.Swing).Select(ri => ri.Id));

            var subscriptionType = _authService.GetSubscriptionType(userId);

            shiftQuery.ToList().ForEach(shift =>
            {
                reportsItems.Add(this.GetReportItemFromShift(shift));

                if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForBasicLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }

                else if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Premium)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForPremiumLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }
            });

            rotationsQuery.ToList().ForEach(rotation =>
            {
                reportsItems.Add(this.GetReportItemFromRotation(rotation));

                if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForBasicLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }

                else if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Premium)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForPremiumLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }
            });

            return reportsItems.OrderByDescending(r => r.IsUnfinished).OrderByDescending(r => r.DateString);
        }


        public IEnumerable<TModel> GetReportsPageForGlobalTab(Guid userId, IEnumerable<string> userRoles, int? page, int? pageSize, int? monthRestriction = null, Models.RotationType? filterRotationType = null, Guid? filterUserId = null)
        {
            List<TModel> reportsItems = new List<TModel>();

            this._monthRestriction = monthRestriction;
            this.Page = page - 1 ?? 0;
            this.PageSize = pageSize ?? NumericConstants.Paginator.PaginatorPageSizeForGlobalReport;

            this._users = _logicCore.UserProfileCore.GetRelayEmployees().Where(u => u.CurrentRotation != null);

            IQueryable<UserProfile> users = this.BuildUsersQuery(userId, userRoles, filterUserId);

            if (AppSession.Current.IsMultiTenant)
            {
                IEnumerable<Guid> usersIds = _authService.GetUsersIdsByRole(iHandoverRoles.Relay.iHandoverAdmin, Guid.NewGuid());

                this._users = _users.Where(u => !usersIds.Contains(u.Id));

                users = this.BuildUsersQuery(userId, userRoles, filterUserId).Where(u => !usersIds.Contains(u.Id));
            }

            IEnumerable<ReportItem> reportItemsPage = this.ReportsPaginationForGlobalTab(userId, users, filterRotationType, this.Page, this.PageSize);

            IQueryable<Shift> shiftQuery = _logicCore.ShiftCore.GetShiftsByIds(reportItemsPage.Where(ri => ri.Type == Models.RotationType.Shift)
                                                                                                            .Select(ri => ri.Id));
            IQueryable<Rotation> rotationsQuery = _logicCore.RotationCore.GetRotationsByIds(reportItemsPage.Where(ri => ri.Type == Models.RotationType.Swing)
                                                                                                            .Select(ri => ri.Id));

            var subscriptionType = _authService.GetSubscriptionType(userId);

            shiftQuery.ToList().ForEach(shift =>
            {
                reportsItems.Add(this.GetReportItemFromShift(shift));

                if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForBasicLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }

                else if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Premium)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForPremiumLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }
            });

            rotationsQuery.ToList().ForEach(rotation =>
            {
                reportsItems.Add(this.GetReportItemFromRotation(rotation));

                if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForBasicLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }

                else if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Premium)
                {
                    reportsItems.ForEach(reportsItem =>
                    {
                        var daysAccessHistory = DaysAccessHistoryForPremiumLevel(reportsItem.CreatedTime);
                        reportsItems = reportsItems.Where(n => daysAccessHistory).ToList();
                    });
                }
            });

            return reportsItems.OrderByDescending(r => r.IsUnfinished).ThenByDescending(r => r.DateString).OrderByDescending(r => r.DateString);
        }

        private bool DaysAccessHistoryForBasicLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddMonths(1);

            return timeStamp <= daysAccess ? true : false;
        }

        private bool DaysAccessHistoryForPremiumLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddMonths(6);

            return timeStamp <= daysAccess ? true : false;
        }

        private IEnumerable<ReportItem> ReportsPagination(IQueryable<UserProfile> users, Models.RotationType? filterRotationType, int page, int pageSize)
        {
            List<ReportItem> reportItems = new List<ReportItem>();

            this.TotalSize = 0;

            IQueryable<Shift> shiftQuery = this.BuildShiftsQuery(users);

            IQueryable<Rotation> rotationsQuery = this.BuildRotationsQuery(users);

            if (this._monthRestriction.HasValue)
            {
                DateTime startHideDate = DateTime.Now.AddMonths(-this._monthRestriction.Value);

                shiftQuery = shiftQuery.Where(s => s.StartDateTime > startHideDate);
                rotationsQuery = rotationsQuery.Where(r => r.StartDate.Value > startHideDate);
            }

            if (!filterRotationType.HasValue || filterRotationType.Value == Models.RotationType.Swing)
            {
                this.TotalSize += rotationsQuery.Count();
                reportItems.AddRange(rotationsQuery.Select(r => new ReportItem { Id = r.Id, Type = Models.RotationType.Swing, CreatedTime = r.StartDate.Value }));
            }
            if (!filterRotationType.HasValue || filterRotationType.Value == Models.RotationType.Shift)
            {
                this.TotalSize += shiftQuery.Count();
                reportItems.AddRange(shiftQuery.Select(s => new ReportItem { Id = s.Id, Type = Models.RotationType.Shift, CreatedTime = s.StartDateTime.Value }));
            }

            IEnumerable<ReportItem> reportItemsPage = reportItems.OrderByDescending(rep => rep.CreatedTime)
                                                                    .Skip(page * pageSize)
                                                                    .Take(pageSize);

            return reportItemsPage;
        }


        private IEnumerable<ReportItem> ReportsPaginationForGlobalTab(Guid userId, IQueryable<UserProfile> users, Models.RotationType? filterRotationType, int page, int pageSize)
        {
            List<ReportItem> reportItems = new List<ReportItem>();

            this.TotalSize = 0;

            var usrs = users.ToList();

            List<Shift> shiftQuery = 
                shiftQuery = _logicCore.RotationCore.GetAllRotationsForUsers(users.Where(u => u.Id != userId).Select(u => u.Id).ToList())
                                                                   .Where(r => r.State != RotationState.Created && r.RotationType == Core.Models.RotationType.Shift).SelectMany(r => r.RotationShifts).Where(s => s.State != ShiftState.Created).ToList();


            List<Rotation> rotationsQuery = _logicCore.RotationCore.GetAllRotationsForUsers(users.Where(u => u.Id != userId).Select(u => u.Id).ToList())
                                                               .Where(r => r.RotationType == Core.Models.RotationType.Swing).ToList();

            if (this._monthRestriction.HasValue)
            {
                DateTime startHideDate = DateTime.Now.AddMonths(-this._monthRestriction.Value);

                shiftQuery = shiftQuery.Where(s => s.StartDateTime > startHideDate).ToList();
                rotationsQuery = rotationsQuery.Where(r => r.StartDate.Value > startHideDate).ToList();
            }

            if (!filterRotationType.HasValue || filterRotationType.Value == Models.RotationType.Swing)
            {
                this.TotalSize += rotationsQuery.Count();
                reportItems.AddRange(rotationsQuery.Where(r=> r.StartDate != null).Select(r => new ReportItem { Id = r.Id, Type = Models.RotationType.Swing, CreatedTime = r.StartDate.Value }));
            }
            if (!filterRotationType.HasValue || filterRotationType.Value == Models.RotationType.Shift)
            {
                this.TotalSize += shiftQuery.Count();
                reportItems.AddRange(shiftQuery.Where(s => s.StartDateTime != null).Select(s => new ReportItem { Id = s.Id, Type = Models.RotationType.Shift, CreatedTime = s.StartDateTime.Value }));
            }

            IEnumerable<ReportItem> reportItemsPage = reportItems.OrderByDescending(rep => rep.CreatedTime)
                                                                    .Skip(page * pageSize)
                                                                    .Take(pageSize);
            return reportItemsPage;
        }

        protected abstract IQueryable<Rotation> BuildRotationsQuery(IQueryable<UserProfile> users);

        protected abstract IQueryable<Shift> BuildShiftsQuery(IQueryable<UserProfile> users);

        protected abstract IQueryable<UserProfile> BuildUsersQuery(Guid userId, IEnumerable<string> userRoles, Guid? filterUserId);

        protected abstract TModel GetReportItemFromRotation(Rotation rotation);

        protected abstract TModel GetReportItemFromShift(Shift shift);
    }
}
