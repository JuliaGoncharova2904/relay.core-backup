﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Helpers;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SiteService : BaseService, ISiteService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IAuthService _authService;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SiteService(LogicCoreUnitOfWork logicCore, IAuthService authService) : base(authService)
        {
            this._logicCore = logicCore;
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._authService = authService;
        }

        public IEnumerable<SiteViewModel> GetAllSites()
        {
            var sites = _repositories.WorkplaceRepository.GetAll().Where(s => !s.Positions.Any() || s.Positions.Any(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter())));

            var result = sites.ToList();

            return _mapper.Map<IEnumerable<SiteViewModel>>(sites.ToList().OrderBy(s => s.Name));
        }

        public List<SiteViewModel> GetSites()
        {
            var sites = _repositories.WorkplaceRepository.GetAll().Where(s => !s.Positions.Any() || s.Positions.Any(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter())));

            return _mapper.Map<List<SiteViewModel>>(sites.ToList().OrderBy(s => s.Name));
        }

        public IEnumerable<SelectListItem> GetSitesList()
        {
            var sites = _repositories.WorkplaceRepository.GetAll().Where(s => !s.Positions.Any() || s.Positions.Any(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter())));

            return _mapper.Map<IEnumerable<SelectListItem>>(sites.OrderBy(s => s.Name));
        }


        public SiteViewModel GetSiteForPosition(Guid positionId)
        {
            Position position = _repositories.PositionRepository.Get(positionId);
            if (position != null)
            {
                return _mapper.Map<SiteViewModel>(position.Workplace);
            }

            return null;
        }

        public SiteViewModel PopulateAddSiteModel()
        {
            var model = new SiteViewModel
            {
                TimeZoneId = _logicCore.AdministrationCore.GetAdminSettings().TimeZoneId
            };
            return model;
        }

        public bool AddSite(SiteViewModel site)
        {
            if (!_repositories.WorkplaceRepository.Find(m => m.Name == site.Name).Any())
            {
                Workplace siteEntity = _mapper.Map<Workplace>(site);
                siteEntity.Id = Guid.NewGuid();
                siteEntity.TimeZoneId = site.TimeZoneId;

                _repositories.WorkplaceRepository.Add(siteEntity);
                _repositories.Save();

                return true;
            }

            return false;
        }

        public SiteViewModel GetSite(Guid siteId, Guid userId)
        {
            Workplace site = _repositories.WorkplaceRepository.Get(siteId);
            if (site != null)
            {
                var model = _mapper.Map<SiteViewModel>(site);

                if (model.TimeZoneId == null)
                {
                    var timeZone = _authService.GetTimeZoneFromSubscription(userId);

                    if (timeZone != null)
                    {
                        timeZone = TimeZoneInfo.GetSystemTimeZones().Select(zone =>
                                        new SelectListItem
                                        {
                                            Value = zone.Id,
                                            Text = zone.DisplayName
                                        }).FirstOrDefault(zone => zone.Text == timeZone).Value;

                        model.TimeZoneId = timeZone;
                    }
                    else
                    {
                        model.TimeZoneId = model.TimeZoneId ?? _logicCore.AdministrationCore.GetAdminSettings().TimeZoneId;
                    }

                }
                else
                {
                    model.TimeZoneId = model.TimeZoneId ?? _logicCore.AdministrationCore.GetAdminSettings().TimeZoneId;
                }

                return model;
            }

            return null;
        }

        public bool UpdateSite(SiteViewModel site)
        {
            if (site.Id.HasValue)
            {
                Workplace siteEntity = _repositories.WorkplaceRepository.Get(site.Id.Value);
                if (siteEntity != null)
                {
                    _mapper.Map(site, siteEntity);

                    _repositories.WorkplaceRepository.Update(siteEntity);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        public void RemoveSite(Guid siteId)
        {
            _repositories.WorkplaceRepository.Delete(siteId);
            _repositories.Save();
        }


        public bool CanRemoveSite(Guid siteId)
        {
            var sites = _repositories.WorkplaceRepository.GetAll().Where(s => s.Id == siteId).Where(s => !s.Positions.Any()).ToList();

            if (!sites.Any())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
