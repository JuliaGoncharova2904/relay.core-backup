﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using RotationType = MomentumPlus.Relay.Models.RotationType;
using System.Web.Mvc;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationModuleService : IRotationModuleService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public RotationModuleService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public void ChangeTemplateModuleChildRotationModulesStatus(Guid templateModuleId, bool status)
        {
            var childModulesInRotations = _logicCore.RotationModuleCore.GetActiveRotationsModulesByTemplate(templateModuleId).ToList();

            var childModulesInShifts = _logicCore.RotationModuleCore.GetActiveShiftsModulesByTemplate(templateModuleId).ToList();

            childModulesInRotations.AddRange(childModulesInShifts);

            childModulesInRotations.ForEach(childModule => { ChangeRotationModuleStatus(childModule.Id, status); });
        }


        public void ChangeRotationModuleStatus(Guid moduleId, bool status)
        {
            var module = _logicCore.RotationModuleCore.GetModule(moduleId);

            if (status == false)
            {
                _services.RotationTopicGroupService.UpdateRotationModuleTopicGroupsStatus(moduleId, status);
            }

            module.Enabled = status;

            _repositories.RotationModuleRepository.Update(module);
            _repositories.Save();
        }



        private IEnumerable<RotationTopic> FilterModuleTopics(IEnumerable<RotationTopic> rotationTopic, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            IEnumerable<RotationTopic> result = new List<RotationTopic>();

            if (rotationTopic != null)
            {
                if (filter == HandoverReportFilterType.All)
                {
                    result = rotationTopic;
                }

                if (filter == HandoverReportFilterType.HandoverItems)
                {
                    result = rotationTopic.Where(topic => !topic.IsNullReport);
                }

                if (filter == HandoverReportFilterType.NrItems)
                {
                    result = rotationTopic.Where(topic => topic.IsNullReport);
                }
            }

            return result;
        }

        /// <summary>
        /// Get HSE topics view model
        /// </summary>
        /// <param name="identityUserId">Guid userId who loggined in system</param>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns>IEnumerable[HSETopicViewModel] collection of viewModels</returns>
        public IEnumerable<HSETopicViewModel> GetHseModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                            ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.HSE, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                            : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.HSE, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups.Where(tg => tg.Enabled)
                                                                        .SelectMany(tg => tg.RotationTopics)
                                                                        .Where(t => t.Enabled);

                rotationTopics = sourceType == ModuleSourceType.Draft ? this.FilterModuleTopics(rotationTopics, filter) : sourceType == ModuleSourceType.Received ? this.FilterModuleTopics(rotationTopics, HandoverReportFilterType.All) : rotationTopics;

                rotationTopics = GetOneTopicMultipleReference(rotationTopics, sourceType, filter);

                Guid ownerId = rotationType == RotationType.Swing
                    ? rotationModule.Rotation.RotationOwnerId
                    : rotationModule.Shift.Rotation.RotationOwnerId;

                IEnumerable<HSETopicViewModel> model = rotationTopics.Select(topicItem => new HSETopicViewModel
                {
                    OwnerId = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                        : topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId,
                    Type = topicItem.RotationTopicGroup.Name,
                    IsFirstTopicByTopicGroupName = topicItem.IsFirstTopicByTopicGroupName.HasValue ? true : false,
                    Reference = topicItem.Name,
                    Notes = topicItem.Description,
                    TeammateId = topicItem.AssignedToId,
                    IsCustom = !topicItem.TempateTopicId.HasValue,
                    FromTeammateId = topicItem.AncestorTopicId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                        : (Guid?)null,
                    FromTeammateFullName = topicItem.AncestorTopicId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName
                        : null,
                    FromRotationId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId : null,
                    FromShiftId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId : null,
                    IsFinalized = topicItem.FinalizeStatus == StatusOfFinalize.Finalized || topicItem.FinalizeStatus == StatusOfFinalize.AutoFinalized,
                    HasComments = topicItem.ManagerComment.Count > 0 ? true : false,
                    HasTasks = topicItem.RotationTasks != null && topicItem.RotationTasks.Any(tt => tt.Enabled),
                    HasAttachments = topicItem.Attachments != null && topicItem.Attachments.Any() || topicItem.AttachmentsLink != null && topicItem.AttachmentsLink.Any() ? true : false,
                    //HasVoiceMessages = topicItem.VoiceMessages != null && topicItem.VoiceMessages.Any(),
                    HasLocation = topicItem.LocationId.HasValue,
                    SharedCounter = topicItem.TopicSharingRelations != null && topicItem.TopicSharingRelations.Any() ? topicItem.TopicSharingRelations.GroupBy(t=>t.RecipientId).Select(t=>t.FirstOrDefault()).Count() : 0,
                    IsSharingTopic = topicItem.IsRecipientsFromPrevRotation.HasValue && topicItem.IsRecipientsFromPrevRotation.Value ?  false : topicItem.ShareSourceTopicId.HasValue,
                    CarryforwardCounter = topicItem.ForkParentTopicId.HasValue ? topicItem.ForkParentTopic.ForkCounter : topicItem.ForkCounter,
                    IsPinned = topicItem.IsPinned,
                    IsNullReport = topicItem.IsNullReport,
                    Id = topicItem.Id,
                    CreatorId = topicItem.CreatorId ?? identityUserId,
                    IsContributor = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)
                        : identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId),
                    IsOwnerOrCreator = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? (identityUserId == topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId)
                        : (identityUserId == topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId),
                    EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topicItem, identityUserId, ownerId).ToList(),
                    IsFeedbackRequired = topicItem.IsFeedbackRequired,
                    PlannedActualHas = topicItem.TempateTopic != null && topicItem.TempateTopic.ParentTopic != null && topicItem.TempateTopic.ParentTopic.UnitsSelectedType != null ? true : topicItem.PlannedActualHas.HasValue ? topicItem.PlannedActualHas : false,
                    PlannedActualField = _services.RotationTopicService.GetPlannedVsActualTopicId(topicItem),
                    TagsTopicField = _services.RotationTopicService.GetTagsTopicByTopicId(topicItem),
                    HasTags = topicItem.SearchTags != null ? true : false
                });

                return model;
            }

            return new List<HSETopicViewModel>();
        }

        public IEnumerable<RotationTopic> GetOneTopicMultipleReference(IEnumerable<RotationTopic> rotationTopics, ModuleSourceType moduleSourceType, HandoverReportFilterType filter)
        {
            if (moduleSourceType == ModuleSourceType.Draft)
            {
                var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.FirstOrDefault()).ToList();

                List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

                if (topicGroupsByName != null)
                {
                    topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                    {
                        RotationTopicGroup resultTopicGroup = _services.RotationTopicService.SortByHaveFirstTopicGroupName(topicGroup, moduleSourceType);
                        sortedRotationTopics.AddRange(resultTopicGroup.RotationTopics);
                    });
                }

                return sortedRotationTopics.OrderByDescending(t => t.IsFirstTopicByTopicGroupName.HasValue).OrderBy(t => t.RotationTopicGroup.Name).ThenBy(t => t.Name);
            }
            else
            {
                var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.OrderByDescending(r => r.RotationTopics.Count).FirstOrDefault()).ToList();

                List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

                if (topicGroupsByName != null)
                {
                    topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                    {
                        RotationTopicGroup resultTopicGroup = _services.RotationTopicService.SortByHaveFirstTopicGroupName(topicGroup, moduleSourceType);
                        sortedRotationTopics.AddRange(resultTopicGroup.RotationTopics);
                    });
                }

                return sortedRotationTopics.OrderByDescending(t => t.IsFirstTopicByTopicGroupName.HasValue).OrderBy(t => t.RotationTopicGroup.Name).ThenBy(t => t.Name);
            }
        }

        public IEnumerable<RotationTopic> GetOneTopicMultipleReferenceForTeam(IEnumerable<RotationTopic> rotationTopics)
        {
            List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

            var topicsGroupByRelation = rotationTopics.Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.RelationId.Value).ToList().Select(t => t.FirstOrDefault()).ToList();

            topicsGroupByRelation.ForEach(topicGroupByRelation => 
            {
                var topics = _services.RotationTopicService.GetTopicsByRelationId(topicGroupByRelation.RelationId.Value, topicGroupByRelation.RotationTopicGroupId);

                sortedRotationTopics.AddRange(_services.RotationTopicService.SortByHaveFirstTopicName(topics));
            });

            return sortedRotationTopics.OrderByDescending(t => t.IsFirstTopicByTopicGroupName.HasValue).OrderBy(t => t.RelationId.Value).ThenBy(t => t.Name).ToList();
        }

        public IEnumerable<HSETopicViewModel> GetHseModuleTopicsForReceivedType(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                          ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.HSE, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                          : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.HSE, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);

                rotationTopics = sourceType == ModuleSourceType.Draft ? this.FilterModuleTopics(rotationTopics, filter) : sourceType == ModuleSourceType.Received ? this.FilterModuleTopics(rotationTopics, filter) : rotationTopics;

                rotationTopics = GetOneTopicMultipleReference(rotationTopics, sourceType, filter);

                Guid ownerId = rotationType == RotationType.Swing
                   ? rotationModule.Rotation.RotationOwnerId
                   : rotationModule.Shift.Rotation.RotationOwnerId;

                IEnumerable<HSETopicViewModel> model = rotationTopics.Select(topicItem => new HSETopicViewModel
                {
                    OwnerId = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                        : topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId,
                    Type = topicItem.RotationTopicGroup.Name,
                    Reference = topicItem.Name,
                    IsFirstTopicByTopicGroupName = topicItem.IsFirstTopicByTopicGroupName.HasValue ? true : false,
                    Notes = topicItem.Description,
                    TeammateId = topicItem.AssignedToId,
                    IsCustom = !topicItem.TempateTopicId.HasValue,
                    FromTeammateId = topicItem.AncestorTopicId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                        : (Guid?)null,
                    FromTeammateFullName = topicItem.AncestorTopicId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName
                        : null,
                    FromRotationId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId : null,
                    FromShiftId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId : null,
                    IsFinalized = topicItem.FinalizeStatus == StatusOfFinalize.Finalized || topicItem.FinalizeStatus == StatusOfFinalize.AutoFinalized,
                    HasComments = topicItem.ManagerComment.Count > 0 ? true : false,
                    HasTasks = topicItem.RotationTasks != null && topicItem.RotationTasks.Any(tt => tt.Enabled),
                    HasAttachments = topicItem.Attachments != null && topicItem.Attachments.Any() || topicItem.AttachmentsLink != null && topicItem.AttachmentsLink.Any() ? true : false,
                   // HasVoiceMessages = topicItem.VoiceMessages != null && topicItem.VoiceMessages.Any(),
                    HasLocation = topicItem.LocationId.HasValue,
                    SharedCounter = topicItem.IsRecipientsFromPrevRotation.HasValue && topicItem.IsRecipientsFromPrevRotation.Value ? 0 : topicItem.TopicSharingRelations != null && topicItem.TopicSharingRelations.Any() ? topicItem.TopicSharingRelations.Count : 0,
                    IsSharingTopic = topicItem.IsRecipientsFromPrevRotation.HasValue && topicItem.IsRecipientsFromPrevRotation.Value ? false : topicItem.ShareSourceTopicId.HasValue,
                    CarryforwardCounter = topicItem.ForkParentTopicId.HasValue ? topicItem.ForkParentTopic.ForkCounter : topicItem.ForkCounter,
                    IsPinned = topicItem.IsPinned,
                    IsNullReport = topicItem.IsNullReport,
                    Id = topicItem.Id,
                    CreatorId = topicItem.CreatorId ?? identityUserId,
                    IsContributor = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)
                        : identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId),

                    IsOwnerOrCreator = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? (identityUserId == topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId)
                        : (identityUserId == topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId),
                    EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topicItem, identityUserId, ownerId).ToList(),
                    IsFeedbackRequired = topicItem.IsFeedbackRequired,
                    FirstTagTopic = topicItem.SearchTags != null ? topicItem.SearchTags.Split(',').FirstOrDefault() : null,
                    MoreTags = topicItem.SearchTags != null && topicItem.SearchTags.Split(',').Count() > 1 ? true : false,
                    HasTags = topicItem.SearchTags != null ? true : false,
                    PlannedActualHas = topicItem.TempateTopic != null && topicItem.TempateTopic.ParentTopic != null && topicItem.TempateTopic.ParentTopic.UnitsSelectedType != null ? true : topicItem.PlannedActualHas.HasValue ? topicItem.PlannedActualHas : false,
                    PlannedActualField = _services.RotationTopicService.GetPlannedVsActualTopicId(topicItem),
                    TagsTopicField = _services.RotationTopicService.GetTagsTopicByTopicId(topicItem),
                    Actual = topicItem.Actual,
                    Planned = topicItem.Planned,
                    Variance = topicItem.Variance,
                    UnitsSelectedType = _services.RotationTopicService.GetUnitSelected(topicItem),
                    IsExpandPlannedActualField = topicItem.TempateTopic != null && topicItem.TempateTopic.ParentTopic != null ? _services.RotationTopicService.IsExpandTemplateTopic(topicItem.TempateTopic.ParentTopic.Id) : false
                });

                return model;
            }
            return new List<HSETopicViewModel>();
        }


        /// <summary>
        /// Get Core topics view model
        /// </summary>
        /// <param name="identityUserId">Guid userId who loggined in system</param>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns> IEnumerable[CoreTopicViewModel] collection of viewModels</returns>
        public IEnumerable<CoreTopicViewModel> GetCoreModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                            ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Core, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                            : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Core, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);

                rotationTopics = sourceType == ModuleSourceType.Draft ? this.FilterModuleTopics(rotationTopics, filter) : sourceType == ModuleSourceType.Received ? this.FilterModuleTopics(rotationTopics, HandoverReportFilterType.All) : rotationTopics;

                rotationTopics = GetOneTopicMultipleReference(rotationTopics, sourceType, filter).Where(t => t.Enabled);

                Guid ownerId = rotationType == RotationType.Swing
                    ? rotationModule.Rotation.RotationOwnerId
                    : rotationModule.Shift.Rotation.RotationOwnerId;

                IEnumerable<CoreTopicViewModel> model = rotationTopics.Select(topicItem => new CoreTopicViewModel
                {
                    OwnerId = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                            ? topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                            : topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId,
                    Category = topicItem.RotationTopicGroup.Name,
                    Reference = topicItem.Name,
                    IsFirstTopicByTopicGroupName = topicItem.IsFirstTopicByTopicGroupName.HasValue ? true : false,
                    Notes = topicItem.Description,
                    TeammateId = topicItem.AssignedToId,
                    IsCustom = !topicItem.TempateTopicId.HasValue,
                    FromTeammateId = topicItem.AncestorTopicId.HasValue
                    ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                        : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                    : (Guid?)null,
                    FromTeammateFullName = topicItem.AncestorTopicId.HasValue
                    ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                        : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName
                    : null,
                    FromRotationId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId : null,
                    FromShiftId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId : null,
                    IsFinalized = topicItem.FinalizeStatus == StatusOfFinalize.Finalized || topicItem.FinalizeStatus == StatusOfFinalize.AutoFinalized,
                    HasComments = topicItem.ManagerComment.Count > 0 ? true : false,
                    HasTasks = topicItem.RotationTasks != null && topicItem.RotationTasks.Any(tt => tt.Enabled),
                    HasAttachments = topicItem.Attachments != null && topicItem.Attachments.Any() || topicItem.AttachmentsLink != null && topicItem.AttachmentsLink.Any() ? true : false,
                   // HasVoiceMessages = topicItem.VoiceMessages != null && topicItem.VoiceMessages.Any(),
                    HasLocation = topicItem.LocationId.HasValue,
                    SharedCounter = topicItem.TopicSharingRelations != null && topicItem.TopicSharingRelations.Any() ? topicItem.TopicSharingRelations.Count : 0,
                    IsSharingTopic = topicItem.IsRecipientsFromPrevRotation.HasValue && topicItem.IsRecipientsFromPrevRotation.Value ? false : topicItem.ShareSourceTopicId.HasValue,
                    CarryforwardCounter = topicItem.ForkParentTopicId.HasValue ? topicItem.ForkParentTopic.ForkCounter : topicItem.ForkCounter,
                    IsPinned = topicItem.IsPinned,
                    IsNullReport = topicItem.IsNullReport,
                    IsContributor = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                                ? identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)
                                : identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId),
                    CreatorId = topicItem.CreatorId ?? identityUserId,
                    Id = topicItem.Id,
                    IsOwnerOrCreator = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                    ? (identityUserId == topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId)
                    : (identityUserId == topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId),
                    EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topicItem, identityUserId, ownerId).ToList(),
                    IsFeedbackRequired = topicItem.IsFeedbackRequired,
                    PlannedActualHas = topicItem.TempateTopic != null && topicItem.TempateTopic.ParentTopic != null && topicItem.TempateTopic.ParentTopic.UnitsSelectedType != null ? true : topicItem.PlannedActualHas.HasValue ? topicItem.PlannedActualHas : false,
                    PlannedActualField = _services.RotationTopicService.GetPlannedVsActualTopicId(topicItem),
                    TagsTopicField = _services.RotationTopicService.GetTagsTopicByTopicId(topicItem),
                    HasTags = topicItem.SearchTags != null ? true : false
                });

                return model;
            }

            return new List<CoreTopicViewModel>();
        }

        /// <summary>
        /// Get Team topics view model
        /// </summary>
        /// <param name="identityUserId">Guid userId who loggined in system</param>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns>IEnumerable[TeamTopicViewModel] collection of viewModels</returns>
        public IEnumerable<TeamTopicViewModel> GetTeamModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                            ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                            : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups.Where(tg => tg.Enabled)
                                                                                                .SelectMany(tg => tg.RotationTopics)
                                                                                                .Where(t => t.Enabled);

 
                rotationTopics = sourceType == ModuleSourceType.Draft ? this.FilterModuleTopics(rotationTopics, filter) : sourceType == ModuleSourceType.Received ? this.FilterModuleTopics(rotationTopics, HandoverReportFilterType.All) : rotationTopics;

                rotationTopics = GetOneTopicMultipleReferenceForTeam(rotationTopics);

                Guid ownerId = rotationType == RotationType.Swing
                    ? rotationModule.Rotation.RotationOwnerId
                    : rotationModule.Shift.Rotation.RotationOwnerId;

                IEnumerable<TeamTopicViewModel> model = rotationTopics.Select(topicItem => new TeamTopicViewModel
                {
                    OwnerId = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                                        ? topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                                        : topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId,
                    TeamMember = topicItem.RelationId.HasValue == false || topicItem.RelationId.Value == Guid.Empty
                                        ? "Other"
                                        : _logicCore.UserProfileCore.GetUserProfile(topicItem.RelationId.Value).FullName,
                    Reference = topicItem.Name,
                    IsFirstTopicByTopicGroupName = topicItem.IsFirstTopicByTopicGroupName.HasValue ? true : false,
                    Notes = topicItem.Description,
                    TeammateId = topicItem.AssignedToId,
                    IsCustom = !topicItem.TempateTopicId.HasValue,
                    FromTeammateId = topicItem.AncestorTopicId.HasValue
                                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                                        : (Guid?)null,
                    FromTeammateFullName = topicItem.AncestorTopicId.HasValue
                                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName
                                        : null,
                    FromRotationId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId : null,
                    FromShiftId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId : null,
                    IsFinalized = topicItem.FinalizeStatus == StatusOfFinalize.Finalized || topicItem.FinalizeStatus == StatusOfFinalize.AutoFinalized,
                    HasComments = topicItem.ManagerComment.Count > 0 ? true : false,
                    HasTasks = topicItem.RotationTasks != null && topicItem.RotationTasks.Any(tt => tt.Enabled),
                    HasAttachments = topicItem.Attachments != null && topicItem.Attachments.Any() || topicItem.AttachmentsLink != null && topicItem.AttachmentsLink.Any() ? true : false,
                   // HasVoiceMessages = topicItem.VoiceMessages != null && topicItem.VoiceMessages.Any(),
                    HasLocation = topicItem.LocationId.HasValue,
                    SharedCounter = topicItem.TopicSharingRelations != null && topicItem.TopicSharingRelations.Any() ? topicItem.TopicSharingRelations.Count : 0,
                    IsSharingTopic = topicItem.IsRecipientsFromPrevRotation.HasValue && topicItem.IsRecipientsFromPrevRotation.Value ? false : topicItem.ShareSourceTopicId.HasValue,
                    CarryforwardCounter = topicItem.ForkParentTopicId.HasValue ? topicItem.ForkParentTopic.ForkCounter : topicItem.ForkCounter,
                    IsPinned = topicItem.IsPinned,
                    IsNullReport = topicItem.IsNullReport,
                    Id = topicItem.Id,
                    CreatorId = topicItem.CreatorId ?? identityUserId,
                    IsContributor = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)
                        : identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId),
                    IsOwnerOrCreator = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? (identityUserId == topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId)
                        : (identityUserId == topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId),
                    EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topicItem, identityUserId, ownerId).ToList(),
                    IsFeedbackRequired = topicItem.IsFeedbackRequired,
                    PlannedActualHas = topicItem.TempateTopic != null && topicItem.TempateTopic.ParentTopic != null && topicItem.TempateTopic.ParentTopic.UnitsSelectedType != null ? true : topicItem.PlannedActualHas.HasValue ? topicItem.PlannedActualHas : false,
                    PlannedActualField = _services.RotationTopicService.GetPlannedVsActualTopicId(topicItem),
                    TagsTopicField = _services.RotationTopicService.GetTagsTopicByTopicId(topicItem),
                    HasTags = topicItem.SearchTags != null ? true : false
                });

                return model;
            }

            return new List<TeamTopicViewModel>();
        }

        /// <summary>
        /// Get Project topics view model
        /// </summary>
        /// <param name="identityUserId">Guid userId who loggined in system</param>
        /// <param name="moduleOwnerId">Module owner ID</param>
        /// <param name="sourceType">Source type</param>
        /// <param name="rotationType">Rotation type</param>
        /// <param name="filter">Filter Items Type</param>
        /// <returns>IEnumerable[ProjectsTopicViewModel] collection of viewModels</returns>
        public IEnumerable<ProjectsTopicViewModel> GetProjectModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            RotationModule rotationModule = rotationType == RotationType.Swing
                                                     ? _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Project, moduleOwnerId, (TypeOfModuleSource)sourceType)
                                                     : _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Project, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule != null && rotationModule.RotationTopicGroups.Any())
            {
                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);

                rotationTopics = sourceType == ModuleSourceType.Draft ? this.FilterModuleTopics(rotationTopics, filter) : sourceType == ModuleSourceType.Received ? this.FilterModuleTopics(rotationTopics, HandoverReportFilterType.All) : rotationTopics;

                rotationTopics = GetOneTopicMultipleReference(rotationTopics, sourceType, filter);

                Guid ownerId = rotationType == RotationType.Swing
                    ? rotationModule.Rotation.RotationOwnerId
                    : rotationModule.Shift.Rotation.RotationOwnerId;

                IEnumerable<ProjectsTopicViewModel> model = rotationTopics.Select(topicItem => new ProjectsTopicViewModel
                {
                    OwnerId = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                        : topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId,
                    Project = topicItem.RotationTopicGroup.Name,
                    Reference = topicItem.Name,
                    IsFirstTopicByTopicGroupName = topicItem.IsFirstTopicByTopicGroupName.HasValue ? true : false,
                    Notes = topicItem.Description,
                    TeammateId = topicItem.AssignedToId,
                    IsCustom = !topicItem.TempateTopicId.HasValue,
                    FromTeammateId = topicItem.AncestorTopicId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                        : (Guid?)null,
                    FromTeammateFullName = topicItem.AncestorTopicId.HasValue
                        ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                            ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName
                            : topicItem.AncestorTopic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.FullName
                        : null,
                    FromRotationId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.RotationId : null,
                    FromShiftId = topicItem.AncestorTopicId.HasValue ? topicItem.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId : null,
                    IsFinalized = topicItem.FinalizeStatus == StatusOfFinalize.Finalized || topicItem.FinalizeStatus == StatusOfFinalize.AutoFinalized,
                    HasComments = topicItem.ManagerComment.Count > 0 ? true : false,
                    HasTasks = topicItem.RotationTasks != null && topicItem.RotationTasks.Any(tt => tt.Enabled),
                    HasAttachments = topicItem.Attachments != null && topicItem.Attachments.Any() || topicItem.AttachmentsLink != null && topicItem.AttachmentsLink.Any() ? true : false,
                   // HasVoiceMessages = topicItem.VoiceMessages != null && topicItem.VoiceMessages.Any(),
                    HasLocation = topicItem.LocationId.HasValue,
                    SharedCounter = topicItem.TopicSharingRelations != null && topicItem.TopicSharingRelations.Any() ? topicItem.TopicSharingRelations.Count : 0,
                    IsSharingTopic = topicItem.IsRecipientsFromPrevRotation.HasValue && topicItem.IsRecipientsFromPrevRotation.Value ? false : topicItem.ShareSourceTopicId.HasValue,
                    CarryforwardCounter = topicItem.ForkParentTopicId.HasValue ? topicItem.ForkParentTopic.ForkCounter : topicItem.ForkCounter,
                    IsPinned = topicItem.IsPinned,
                    IsNullReport = topicItem.IsNullReport,
                    Id = topicItem.Id,
                    CreatorId = topicItem.CreatorId ?? identityUserId,
                    IsContributor = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)
                        : identityUserId == topicItem.CreatorId && _logicCore.ContributorsCore.IsContributor(identityUserId, topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId),
                    IsOwnerOrCreator = topicItem.RotationTopicGroup.RotationModule.ShiftId.HasValue
                        ? (identityUserId == topicItem.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId)
                        : (identityUserId == topicItem.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId) || (identityUserId == topicItem.CreatorId),
                    EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topicItem, identityUserId, ownerId).ToList(),
                    IsFeedbackRequired = topicItem.IsFeedbackRequired,
                    PlannedActualHas = topicItem.TempateTopic != null && topicItem.TempateTopic.ParentTopic != null && topicItem.TempateTopic.ParentTopic.UnitsSelectedType != null ? true : topicItem.PlannedActualHas.HasValue ? topicItem.PlannedActualHas : false,
                    PlannedActualField = _services.RotationTopicService.GetPlannedVsActualTopicId(topicItem),
                    TagsTopicField = _services.RotationTopicService.GetTagsTopicByTopicId(topicItem),
                    HasTags = topicItem.SearchTags != null ? true : false
                });

                return model;
            }

            return new List<ProjectsTopicViewModel>();
        }

        public bool IsModuleEnabled(ModuleType moduleType, Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing)
        {
            var rotationModule = rotationType == RotationType.Swing ?
                               _logicCore.RotationModuleCore.GetRotationModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType) :
                               _logicCore.RotationModuleCore.GetShiftModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType);

            return rotationModule != null && rotationModule.Enabled;
        }

        public bool IsTemplateModuleEnabled(ModuleType moduleType, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing)
        {  
            var rotationModule = rotationType == RotationType.Swing ?
                              _logicCore.RotationModuleCore.GetRotationModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType) :
                              _logicCore.RotationModuleCore.GetShiftModule((TypeOfModule)moduleType, moduleOwnerId, (TypeOfModuleSource)sourceType);

            if (rotationModule == null)
            {
                if (rotationType == RotationType.Swing)
                {
                    rotationModule = _repositories.RotationModuleRepository.Find(m => m.RotationId == moduleOwnerId && m.Type == TypeOfModule.HSE && !m.DeletedUtc.HasValue).FirstOrDefault();
                }
                else
                {
                    rotationModule = _repositories.RotationModuleRepository.Find(m => m.ShiftId == moduleOwnerId && m.Type == TypeOfModule.HSE && !m.DeletedUtc.HasValue).FirstOrDefault();
                }
            }

            return rotationModule != null && rotationModule.TempateModuleId.HasValue ? rotationModule.TemplateModule.Enabled : true;
        }

        public bool IsTemplateModuleReceivedEnabled(Guid receivedId, ModuleType moduleType, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing)
        {
            var rotationModule = rotationType == RotationType.Swing ?
                              _logicCore.RotationModuleCore.GetRotationModule((TypeOfModule)moduleType, receivedId, (TypeOfModuleSource)sourceType) :
                              _logicCore.RotationModuleCore.GetShiftModule((TypeOfModule)moduleType, receivedId, (TypeOfModuleSource)sourceType);

            if (rotationModule == null)
            {
                if (rotationType == RotationType.Swing)
                {
                    rotationModule = _repositories.RotationModuleRepository.Find(m => m.RotationId == receivedId && m.Type == TypeOfModule.HSE && !m.DeletedUtc.HasValue).FirstOrDefault();
                }
                else
                {
                    rotationModule = _repositories.RotationModuleRepository.Find(m => m.ShiftId == receivedId && m.Type == TypeOfModule.HSE && !m.DeletedUtc.HasValue).FirstOrDefault();
                }
            }

            return rotationModule != null && rotationModule.TempateModuleId.HasValue ? rotationModule.TemplateModule.Enabled : true;
        }
    }

}
