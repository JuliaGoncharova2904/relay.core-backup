﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ModuleService : IModuleService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public ModuleService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public rModuleViewModel GetBaseModuleFromTypeViewModel(ModuleType type)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.Type == (TypeOfModule)type && !m.ParentModuleId.HasValue).FirstOrDefault();

            return _mapper.Map<rModuleViewModel>(module);
        }

        public rModuleViewModel GetModuleViewModel(Guid moduleId)
        {
            var module = _repositories.TemplateModuleRepository.Get(moduleId);

            return _mapper.Map<rModuleViewModel>(module);
        }

        public IEnumerable<rModuleViewModel> GetBaseModulesViewModels()
        {
            var modules = _repositories.TemplateModuleRepository.Find(m => !m.ParentModuleId.HasValue && !m.DeletedUtc.HasValue && m.Type != TypeOfModule.Project).AsNoTracking();
            modules = modules.OrderByDescending(t => t.Type == TypeOfModule.Core);
            return _mapper.Map<IEnumerable<rModuleViewModel>>(modules);
        }


        public rModuleViewModel GetTemplateModuleViewModel(ModuleType type, Guid templateId)
        {
            var module = _repositories.TemplateModuleRepository.Find(m => m.TemplateId == templateId && m.Type == (TypeOfModule)type && !m.DeletedUtc.HasValue).FirstOrDefault();

            return _mapper.Map<rModuleViewModel>(module);
        }


        public rModuleViewModel PopulateBaseModuleViewModel(ModuleType moduleType, Guid? activeTopicGroupId, Guid? activeTopicId,
                int? topicGroupsPageNo, int? topicsPageNo, int? tasksPageNo)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);

            var model = _mapper.Map<rModuleViewModel>(baseModule);

            if (model != null)
            {
                model.ActiveTopicGroupId = activeTopicGroupId;
                model.ActiveTopicId = activeTopicId;

                model.TopicGroupsList = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id)
                    .ToPagedList(topicGroupsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

                //need rewrite code!!
                model.TopicGroupsPage2 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(10).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage3 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(20).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage4 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(30).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage5 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(40).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage6 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(50).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage7 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(60).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage8 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(70).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage9 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(80).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                model.TopicGroupsPage10 = _services.TopicGroupService.GetModuleTopicGroupsViewModels(baseModule.Id).Skip(90).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();

                if (activeTopicGroupId.HasValue)
                {
                    model.TopicsList = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId)
                        .ToPagedList(topicsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

                    model.TopicPage2 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(10).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage3 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(20).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage4 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(30).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage5 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(40).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage6 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(50).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage7 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(60).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage8 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(70).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage9 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(80).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                    model.TopicPage10 = _services.TopicService.GetTopicGroupTopicsViewModels((Guid)activeTopicGroupId).Skip(90).Take(NumericConstants.Paginator.PaginatorPageSize).ToList();
                }                      

                if (activeTopicId.HasValue)
                {
                    model.TasksList = _services.TaskService.GetTopicTasksViewModels((Guid)activeTopicId)
                        .ToPagedList(tasksPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
                }


                return model;
            }

            return null;
        }

        public rModuleViewModel PopulateTemplateModuleViewModel(Guid templateId, ModuleType moduleType, Guid? activeTopicGroupId,
            Guid? activeTopicId, int? topicGroupsPageNo, int? topicsPageNo, int? tasksPageNo)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);

            var templateModule = _logicCore.ModuleCore.GetOrCreateTemplateModule(templateId, baseModule);

            var model = _mapper.Map<rModuleViewModel>(templateModule);

            model.Enabled = templateModule.Enabled;

            model.ActiveTopicGroupId = activeTopicGroupId;
            model.ActiveTopicId = activeTopicId;

            var topicGroupsViewModels =
                _services.TopicGroupService.PopulateTemplateModuleTopicGroupsViewModels(baseModule.Id, templateModule.Id);

            model.EnabledTopicGroupsCounter = topicGroupsViewModels.Count(m => m.Enabled);

            model.TopicGroupsList = topicGroupsViewModels.ToPagedList(topicGroupsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

            if (activeTopicGroupId.HasValue)
            {
                model.TopicsList = _services.TopicService.PopulateTemplateTopicGroupTopicsViewModels((Guid)activeTopicGroupId, templateModule.Id)
                    .ToPagedList(topicsPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            }

            if (activeTopicId.HasValue)
            {
                model.TasksList = _services.TaskService.PopulateTemplateTopicTasksViewModels((Guid)activeTopicId, templateModule.Id)
                    .ToPagedList(tasksPageNo - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            }


            return model;
        }


        public void ChangeTemplateModuleStatus(Guid baseModuleId, Guid templateId, bool status)
        {
            var baseModule = _logicCore.ModuleCore.GetModule(baseModuleId);

            var templateModule = _logicCore.ModuleCore.GetOrCreateTemplateModule(templateId, baseModule);

            _services.RotationModuleService.ChangeTemplateModuleChildRotationModulesStatus(templateModule.Id, status);

            //----- Adding Module to rotations live ----
            //if (status)
            //{
            //    _services.RotationModuleService.ChangeTemplateModuleChildRotationModulesStatus(templateModule.Id, status);
            //}
            //------------------------------------------
            ChangeModuleStatus(templateModule.Id, status);

            var module = _repositories.TemplateModuleRepository.Find(m => m.Type == TypeOfModule.HSE && !m.ParentModuleId.HasValue).FirstOrDefault();
            module.Enabled = status;
            _repositories.TemplateModuleRepository.Update(module);
            _repositories.Save();

        }


        public void ChangeModuleStatus(Guid moduleId, bool status)
        {
            var module = _logicCore.ModuleCore.GetModule(moduleId);

            if (status == false)
            {
                _services.TopicGroupService.UpdateModuleTopicGroupsStatus(moduleId, status);
            }

            module.Enabled = status;

            if (module.ParentModule != null)
            {
                module.ParentModule.Enabled = status;
            }

            _repositories.TemplateModuleRepository.Update(module);
            _repositories.Save();
        }


    }
}
