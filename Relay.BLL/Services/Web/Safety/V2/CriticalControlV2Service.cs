﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class CriticalControlV2Service : BaseService, ICriticalControlV2Service
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;

        public CriticalControlV2Service(LogicCoreUnitOfWork logicCore,
                            IServicesUnitOfWork services,
                            IAuthService authService) : base(authService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
            this._authService = authService;
        }


        public EditCriticalControlV2ViewModel PopulateCriticalControlEditModel(Guid majorHazardId)
        {
            EditCriticalControlV2ViewModel model = new EditCriticalControlV2ViewModel { MajorHazardId = majorHazardId };

            return this.PopulateCriticalControlEditModel(model);
        }

        public EditCriticalControlV2ViewModel PopulateCriticalControlEditModel(EditCriticalControlV2ViewModel model)
        {
            IEnumerable<UserProfile> allUsers = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter()).ToList().OrderBy(u => u.FullName);

            model.ChampionsIds = new List<Guid>();
            model.AllUsers = _mapper.Map<IEnumerable<SelectListItem>>(allUsers);

            return model;
        }

        public string GetCriticalControlOwnerName(Guid criticalControlId)
        {
            CriticalControlV2 criticalControl = _logicCore.CriticalControlV2Core.GetCriticalControl(criticalControlId);

            return criticalControl.Owner.FullName;
        }

        public IEnumerable<SelectListItem> GetCriticalControlsList(Guid majorHazardId)
        {
            MajorHazardV2 majorHazrd = _logicCore.MajorHazardV2Core.GetMajorHazard(majorHazardId);
            IEnumerable<CriticalControlV2> criticalControls = _logicCore.CriticalControlV2Core.GetAllCriticalControls(majorHazrd);

            return _mapper.Map<IEnumerable<SelectListItem>>(criticalControls);
        }

        public EditCriticalControlV2ViewModel GetCriticalControlForEdit(Guid criticalControlId)
        {
            CriticalControlV2 criticalControl = _logicCore.CriticalControlV2Core.GetCriticalControl(criticalControlId);
            IEnumerable<UserProfile> allUsers = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter()).ToList().OrderBy(u => u.FullName);

            EditCriticalControlV2ViewModel model = new EditCriticalControlV2ViewModel
            {
                Id = criticalControl.Id,
                Name = criticalControl.Name,
                OwnerId = criticalControl.OwnerId ?? Guid.Empty,
                ChampionsIds = criticalControl.Champions.Select(c => c.Id).ToList(),
                AllUsers = _mapper.Map<IEnumerable<SelectListItem>>(allUsers)
            };

            return model;
        }

        public void AddCriticalControl(EditCriticalControlV2ViewModel model)
        {
            MajorHazardV2 majorHazard = _logicCore.MajorHazardV2Core.GetMajorHazard(model.MajorHazardId);


            CriticalControlV2 criticalControl = new CriticalControlV2
            {
                Name = model.Name,
                OwnerId = (model.OwnerId != Guid.Empty) ? (Guid?)model.OwnerId : null,
                MajorHazardId = majorHazard.Id,
                Champions = new HashSet<UserProfile>()
            };

            IEnumerable<UserProfile> champions = _logicCore.UserProfileCore.GetEmployeesByIds(model.ChampionsIds).ToList();

            (criticalControl.Champions as HashSet<UserProfile>).UnionWith(champions);
            _logicCore.CriticalControlV2Core.AddCriticalControl(criticalControl);

        }

        public void UpdateCriticalControl(EditCriticalControlV2ViewModel model)
        {
            CriticalControlV2 criticalControl = _logicCore.CriticalControlV2Core.GetCriticalControl(model.Id);

            criticalControl.Name = model.Name;
            criticalControl.OwnerId = (model.OwnerId != Guid.Empty) ? (Guid?)model.OwnerId : null;
            criticalControl.Champions.Clear();

            IEnumerable<UserProfile> champions = _logicCore.UserProfileCore.GetEmployeesByIds(model.ChampionsIds).ToList();
            (criticalControl.Champions as HashSet<UserProfile>).UnionWith(champions);

            _logicCore.CriticalControlV2Core.UpdateCriticalControl(criticalControl);
        }

        public IEnumerable<string> GetCriticalControlChampions(Guid criticalControlId)
        {
            CriticalControlV2 criticalControl = _logicCore.CriticalControlV2Core.GetCriticalControl(criticalControlId);

            IEnumerable<string> championsNames = criticalControl.Champions.Select(c => c.FullName).ToList();

            return championsNames;
        }
    }
}
