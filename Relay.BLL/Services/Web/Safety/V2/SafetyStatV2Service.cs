﻿using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{ 
    public class SafetyStatV2Service : BaseService, ISafetyStatV2Service
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;

        public SafetyStatV2Service(LogicCoreUnitOfWork logicCore,
                            IServicesUnitOfWork services,
                            IAuthService authService) : base(authService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
            this._authService = authService;
        }


        public IEnumerable<ArchivedSafetyMessageV2ViewModel> PopulateArchivedSafetyMessagesViewModel()
        {
            var archivedSafetyMessages = _logicCore.SafetyMessageStatV2Core.GetAllArchivedSafetyMessages().OrderByDescending(m => m.Date);

            return _mapper.Map<IEnumerable<ArchivedSafetyMessageV2ViewModel>>(archivedSafetyMessages);
        }
    }
}
