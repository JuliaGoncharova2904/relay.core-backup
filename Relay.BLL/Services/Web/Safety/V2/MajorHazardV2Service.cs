﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class MajorHazardV2Service : BaseService, IMajorHazardV2Service
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;

        public MajorHazardV2Service(LogicCoreUnitOfWork logicCore,
                            IServicesUnitOfWork services,
                            IAuthService authService) : base(authService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
            this._authService = authService;
        }


        public IEnumerable<MajorHazardV2ViewModel> GetAllMajorHazards()
        {
            IEnumerable<MajorHazardV2> majorHazards = _logicCore.MajorHazardV2Core.GetAllMajorHazards();

            return _mapper.Map<IEnumerable<MajorHazardV2ViewModel>>(majorHazards);
        }

        public MajorHazardV2ViewModel GetMajorHazard(Guid majorHazardId)
        {
            MajorHazardV2 majorHazard = _logicCore.MajorHazardV2Core.GetMajorHazard(majorHazardId);

            return _mapper.Map<MajorHazardV2ViewModel>(majorHazard);
        }

        public EditMajorHazardV2ViewModel GetMajorHazardForEdit(Guid majorHazardId)
        {
            IEnumerable<UserProfile> allUsers = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter()).ToList().OrderBy(u => u.FullName);
            MajorHazardV2 majorHazard = _logicCore.MajorHazardV2Core.GetMajorHazard(majorHazardId);
            IEnumerable<CriticalControlV2> criticalContols = _logicCore.CriticalControlV2Core.GetAllCriticalControls(majorHazard);

            EditMajorHazardV2ViewModel model = new EditMajorHazardV2ViewModel
            {
                Id = majorHazardId,
                OwnerId = majorHazard.OwnerId.HasValue ? majorHazard.OwnerId.Value : Guid.Empty,
                Owners = _mapper.Map<IEnumerable<SelectListItem>>(allUsers),
                CriticalControls = _mapper.Map<IEnumerable<CriticalControlV2ViewModel>>(criticalContols)
            };

            return model;
        }

        public void UpdateMajorHazard(EditMajorHazardV2ViewModel model)
        {
            MajorHazardV2 majorHazard = _logicCore.MajorHazardV2Core.GetMajorHazard(model.Id);
            majorHazard.OwnerId = model.OwnerId != Guid.Empty ? (Guid?)model.OwnerId : null;
            _logicCore.MajorHazardV2Core.UpdateMajorHazard(majorHazard);
        }
    }
}
