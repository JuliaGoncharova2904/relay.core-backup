﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;
using RotationType = MomentumPlus.Core.Models.RotationType;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class BoardManagerService : BaseService, IBoardManagerService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly IAuthService _authService;

        public BoardManagerService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services) : base(services.AuthService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
            this._authService = services.AuthService;
        }

        #region Step One

        public BoardManagerStepOneViewModel PopulateStepOneViewModel(BoardManagerStepOneViewModel model)
        {
            return model;
        }



        public IEnumerable<SelectListItem> GetPositions(Guid? siteId)
        {
            List<SelectListItem> positionsList = new List<SelectListItem>();

            if (siteId.HasValue)
            {
                var positions = _logicCore.PositionCore.GetRelayPositions().Where(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter()));

                positions = positions.Where(p => p.WorkplaceId == siteId);

                positionsList = positions.OrderBy(p => p.Name).Select(position => new SelectListItem
                {
                    Value = position.Id.ToString(),
                    Text = position.Name
                }).ToList();
            }

            return positionsList;
        }


        public SelectListItem GetPositionTemplate(Guid positionId)
        {
            var positionTemplate = new SelectListItem();

            var template = _repositories.TemplateRepository.Find(t => t.Positions.Any(p => p.Id == positionId)).FirstOrDefault();

            if (template != null)
            {
                positionTemplate = new SelectListItem
                {
                    Value = template.Id.ToString(),
                    Text = template.Name
                };
            }
            return positionTemplate;
        }

        public IEnumerable<SelectListItem> GetTemplates()
        {
            var templates = _repositories.TemplateRepository.Find(p => !p.DeletedUtc.HasValue).Where(t => !t.Positions.Any() || t.Positions.Any(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter())));

            return templates.OrderBy(t => t.Name).Select(template => new SelectListItem
            {
                Value = template.Id.ToString(),
                Text = template.Name
            }).ToList();
        }


        public Guid CreateSitePositionAndTemplate(BoardManagerStepOneViewModel model)
        {
            Workplace workplace = Step1Workplace(model, false);

            Template template = Step1Template(model, false);

            Position position = Step1Position(model, workplace, template);

            return position.Id;
        }


        private Workplace Step1Workplace(BoardManagerStepOneViewModel model, bool save = true)
        {
            Workplace workplace = null;

            if (model.SiteId.HasValue)
            {
                workplace = _repositories.WorkplaceRepository.Get(model.SiteId.Value);
            }

            else if (!string.IsNullOrWhiteSpace(model.CustomSiteName))
            {
                workplace = new Workplace
                {
                    Id = Guid.NewGuid(),
                    Name = model.CustomSiteName
                };

                _repositories.WorkplaceRepository.Add(workplace);
            }

            if (save)
            {
                _repositories.Save();
            }

            return workplace;
        }


        private Position Step1Position(BoardManagerStepOneViewModel model, Workplace workplace, Template template, bool save = true)
        {
            Position position = null;

            if (model.PositionId.HasValue)
            {
                position = _repositories.PositionRepository.Get(model.PositionId.Value);
                position.TemplateId = template.Id;
                position.WorkplaceId = workplace.Id;
                _repositories.PositionRepository.Update(position);
            }

            else if (!string.IsNullOrWhiteSpace(model.CustomPositionName))
            {
                position = new Position
                {
                    Id = Guid.NewGuid(),
                    Name = model.CustomPositionName,
                    TemplateId = template.Id,
                    WorkplaceId = workplace.Id
                };

                _repositories.PositionRepository.Add(position);
            }

            if (save)
            {
                _repositories.Save();
            }

            return position;
        }


        private Template Step1Template(BoardManagerStepOneViewModel model, bool save = true)
        {
            Template template = null;

            if (model.TemplateId.HasValue)
            {
                template = _repositories.TemplateRepository.Get(model.TemplateId.Value);
            }

            else if (!string.IsNullOrWhiteSpace(model.CustomTemplateName))
            {
                template = new Template
                {
                    Id = Guid.NewGuid(),
                    Name = model.CustomPositionName
                };

                _repositories.TemplateRepository.Add(template);

                _logicCore.ModuleCore.InitTemplateModules(template.Id);
            }

            if (save)
            {
                _repositories.Save();
            }

            return template;
        }

        #endregion


        #region Step Two

        public BoardManagerStepTwoViewModel PopulateStepTwoViewModel(Guid positionId)
        {
            var model = new BoardManagerStepTwoViewModel
            {
                UserPositionId = positionId,
                Companies = _services.CompanyService.GetCompaniesList(),
                RotationPatterns = _services.RotationPatternService.GetRotationPatternsList(),
                Teams = _services.TeamService.GetTeamList(),
                UserTypes = _authService.GetAllRolesAccessedByUser(Guid.NewGuid())
                        .Where(r => r != iHandoverRoles.Relay.iHandoverAdmin)
                        .OrderBy(r => r)
                        .Select(r => new SelectListItem { Text = r, Value = r })
            };

            if (_services.CompanyService.IsExistDefaultCompany())
                model.CompanyId = _services.CompanyService.GetDefaultCompanyId();

            return model;
        }


        public BoardManagerStepTwoViewModel PopulateStepTwoViewModel(BoardManagerStepTwoViewModel model)
        {
            model.Companies = _services.CompanyService.GetCompaniesList();
            model.Continue = false;
            model.RotationPatterns = _services.RotationPatternService.GetRotationPatternsList();
            model.Teams = _services.TeamService.GetTeamList();
            model.UserTypes = _authService.GetAllRolesAccessedByUser(Guid.NewGuid())
                .Where(r => r != iHandoverRoles.Relay.iHandoverAdmin)
                .OrderBy(r => r)
                .Select(r => new SelectListItem { Text = r, Value = r });

            return model;
        }


        public Guid CreateUser(BoardManagerStepTwoViewModel model, out Guid userTeamId)
        {
            Guid newUserProfileId = _authService.GetUserIdByLogin(model.Email);

            _authService.AddUserToRole(newUserProfileId, model.UserTypeId);
            _authService.CreateUser(model.Email, model.Password, true);
            var userTeam = Step2Team(model);

            userTeamId = userTeam.Id;

            UserProfile user = new UserProfile
            {
                Id = newUserProfileId,
                Email = model.Email,
                UserName = model.Email,
                FirstName = model.Name,
                LastName = model.Surname,
                PositionId = model.UserPositionId,
                CompanyId = model.CompanyId.Value,
                RotationPatternId = model.RotationPatternId.Value,
                TeamId = userTeam.Id,
                LastPasswordChangedDate = DateTime.Now,
                TaskBoard = new TaskBoard
                {
                    Id = newUserProfileId,
                    Tasks = new HashSet<RotationTask>()
                }
            };



            _repositories.UserProfileRepository.Add(user);
            _repositories.Save();


            return newUserProfileId;
        }


        public void UpdateUserPattern(Guid userId, string shiftPatternType)
        {
            var user = _logicCore.UserProfileCore.GetRelayEmployees().FirstOrDefault(t => t.Id == userId);
            user.ShiftPatternType = shiftPatternType;

            _repositories.UserProfileRepository.Update(user);
            _repositories.Save();
        }

        public SelectListItem GetSelectedTeam(Guid teamId)
        {
            var teamTemplate = new SelectListItem();

            var team = _repositories.TeamRepository.Get(teamId);

            if (team != null)
            {
                teamTemplate = new SelectListItem
                {
                    Value = team.Id.ToString(),
                    Text = team.Name
                };
            }
            return teamTemplate;
        }

        private Team Step2Team(BoardManagerStepTwoViewModel model, bool save = true)
        {
            Team team = null;

            if (model.TeamId.HasValue)
            {
                team = _repositories.TeamRepository.Get(model.TeamId.Value);
            }

            else if (!string.IsNullOrWhiteSpace(model.CustomTeamName))
            {
                team = new Team
                {
                    Id = Guid.NewGuid(),
                    Name = model.CustomTeamName
                };

                _repositories.TeamRepository.Add(team);
            }

            if (save)
            {
                _repositories.Save();
            }

            return team;
        }

        #endregion


        #region Step Three

        public BoardManagerStepThreeViewModel PopulateStepThreeViewModel(Guid userId)
        {
            IQueryable<UserProfile> allEmployees = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter()).Where(UserActiveFilter()).Where(u => u.Id != userId);

            IQueryable<UserProfile> lineManagers = _logicCore.UserProfileCore.GetEmployeesByIds(_authService.GetUsersIdsByRoles(new[] {
                                                            iHandoverRoles.Relay.HeadLineManager,
                                                            iHandoverRoles.Relay.iHandoverAdmin,
                                                            iHandoverRoles.Relay.SafetyManager,
                                                            iHandoverRoles.Relay.Administrator,
                                                            iHandoverRoles.Relay.LineManager
                                                        }, userId)).Where(u => u.Id != userId);

            lineManagers = lineManagers.Where(u => !u.DeletedUtc.HasValue);
            allEmployees = allEmployees.Where(u => !u.DeletedUtc.HasValue);

            var model = new BoardManagerStepThreeViewModel
            {
                UserId = userId,
                Contributors = _logicCore.TeamCore.GetAllTeams().SelectMany(u => u.Users.Where(us => us.DeletedUtc == null)).ToList().Where(u => _services.AuthService.GetUserRoles(u.Id).Any(r => r != iHandoverRoles.Relay.iHandoverAdmin)).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                    .Select(e => new SelectListItem
                    {
                        Text = e.FirstName + " " + e.LastName,
                        Value = e.Id.ToString()
                    }).ToList(),
                LineManagers = lineManagers.Where(ApplyUserFilter())
                        .Where(UserActiveFilter()).ToList().Where(u => _services.AuthService.GetUserRoles(u.Id).Any(r => r != iHandoverRoles.Relay.iHandoverAdmin))
                        .OrderBy(u => u.FirstName)
                        .ThenBy(u => u.LastName)
                        .Select(e => new SelectListItem
                        {
                            Text = e.FirstName + " " + e.LastName,
                            Value = e.Id.ToString()
                        }).ToList(),
                ShiftPatterns = _services.RotationPatternService.GetShiftPatternsList()
            };

            model.HandoverRecipients = model.Contributors;

            return model;
        }


        public BoardManagerStepThreeViewModel PopulateStepThreeViewModel(BoardManagerStepThreeViewModel model)
        {
            IQueryable<UserProfile> allEmployees = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter()).Where(UserActiveFilter()).Where(u => u.Id != model.UserId);

            IQueryable<UserProfile> lineManagers = _logicCore.UserProfileCore.GetEmployeesByIds(_authService.GetUsersIdsByRoles(new[] {
                                                            iHandoverRoles.Relay.HeadLineManager,
                                                            iHandoverRoles.Relay.iHandoverAdmin,
                                                            iHandoverRoles.Relay.SafetyManager,
                                                            iHandoverRoles.Relay.Administrator,
                                                            iHandoverRoles.Relay.LineManager
                                                        }, model.UserId)).Where(u => u.Id != model.UserId);

            lineManagers = lineManagers.Where(u => !u.DeletedUtc.HasValue);
            allEmployees = allEmployees.Where(u => !u.DeletedUtc.HasValue);

            model.Contributors = allEmployees.Where(ApplyUserFilter()).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                .Select(e => new SelectListItem
                {
                    Text = e.FirstName + " " + e.LastName,
                    Value = e.Id.ToString()
                }).ToList();

            model.LineManagers = lineManagers.Where(ApplyUserFilter())
                .Where(UserActiveFilter())
                .OrderBy(u => u.FirstName)
                .ThenBy(u => u.LastName)
                .Select(e => new SelectListItem
                {
                    Text = e.FirstName + " " + e.LastName,
                    Value = e.Id.ToString()
                }).ToList();


            model.HandoverRecipients = model.Contributors;

            model.ShiftPatternType = model.ShiftPatternType;

            model.ShiftPatterns = _services.RotationPatternService.GetShiftPatternsList();

            model.TemplateId = _services.PositionService.GetPositionForEmployee(model.UserId).TemplateId;

            return model;
        }


        public bool CreateRotation(BoardManagerStepThreeViewModel model)
        {
            Rotation rotation = new Rotation
            {
                Id = Guid.NewGuid(),
                RotationType = (RotationType)model.RotationType, 
                RotationOwnerId = model.UserId,
                LineManagerId = model.LineManagerId.Value,
                State = RotationState.Created,
                //DefaultBackToBackId = model.HandoverRecipientId.Value
                DefaultBackToBackId = _services.AuthService.GetSubscriptionOwnerId().Value
            };

            var contributorsIds = model.ContributorsIds?.ToList() ?? new List<Guid>();


            if (model.ContributorEnable)
            {
                _logicCore.ContributorsCore.SetContributorsForUser(model.UserId, contributorsIds);
            }

            return _logicCore.RotationCore.AssignFirstRotation(rotation);
        }

        #endregion
    }
}
