﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web.Projects
{
    public static class ProjectTasksFilter
    {
        public static IQueryable<RotationTask> ApplyTo(IQueryable<RotationTask> tasksList, ProjectTasksFilterOptions filterOptions)
        {
            switch(filterOptions)
            {
                case ProjectTasksFilterOptions.AllActiveTasks:
                    tasksList = tasksList.Where(t => !t.IsInArchive);
                    break;
                case ProjectTasksFilterOptions.ArchivedTasks:
                    tasksList = tasksList.Where(t => t.IsInArchive);
                    break;
                case ProjectTasksFilterOptions.CompletedTasks:
                    tasksList = tasksList.Where(t => t.IsComplete);
                    break;
                case ProjectTasksFilterOptions.IncompleteTasks:
                    tasksList = tasksList.Where(t => !t.IsComplete);
                    break;
                case ProjectTasksFilterOptions.PendingTasks:
                    tasksList = tasksList.Where(t => t.TaskBoardTaskType == TaskBoardTaskType.Pending);
                    break;
            }


            return tasksList;
        }
    }
}
