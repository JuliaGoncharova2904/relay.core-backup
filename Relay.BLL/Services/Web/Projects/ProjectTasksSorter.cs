﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web.Projects
{
    public static class ProjectTasksSorter
    {
        public static IQueryable<RotationTask> ApplyTo(IQueryable<RotationTask> tasksList, ProjectTasksSortOptions sortOptions)
        {
            switch(sortOptions)
            {
                case ProjectTasksSortOptions.PriorityLowToHigh:
                    tasksList = tasksList.OrderBy(t => t.Priority);
                    break;
                case ProjectTasksSortOptions.PriorityHighToLow:
                    tasksList = tasksList.OrderByDescending(t => t.Priority);
                    break;
                case ProjectTasksSortOptions.DueDateLowToHigh:
                    tasksList = tasksList.OrderBy(t => t.Deadline);
                    break;
                case ProjectTasksSortOptions.DueDateHighToLow:
                    tasksList = tasksList.OrderByDescending(t => t.Deadline);
                    break;
                case ProjectTasksSortOptions.Default:
                default:
                    tasksList = tasksList.OrderBy(t => t.CreatedUtc);
                    break;
            }

            return tasksList;
        }
    }
}
