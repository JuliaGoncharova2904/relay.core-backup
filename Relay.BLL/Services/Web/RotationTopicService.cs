﻿using System;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System.Data.Entity.Core;
using MomentumPlus.Core.Models;
using System.Web.Mvc;
using System.Collections.Generic;
using MomentumPlus.Relay.Extensions;
using System.Globalization;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationTopicService : IRotationTopicService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public RotationTopicService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services
            )
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }


        public void UpdateRotationTopicGroupTopicsStatus(Guid rotationTopicGroupId, bool status)
        {
            var rotationTopicGroup = _logicCore.RotationTopicGroupCore.GetTopicGroup(rotationTopicGroupId);

            rotationTopicGroup.RotationTopics.ToList().ForEach(rotationTopic => { UpdateRotationTopicStatus(rotationTopic.Id, status); });

            //foreach (var rotationTopic in rotationTopicGroup.RotationTopics)
            //{
            //    UpdateRotationTopicStatus(rotationTopic.Id, status);
            //}
        }

        public void UpdateRotationTopicStatus(Guid rotationTopicId, bool status)
        {
            var rotationTopic = _logicCore.RotationTopicCore.GetTopic(rotationTopicId);

            if (status == false)
            {
                _services.RotationTaskService.UpdateRotationTopicTasksStatus(rotationTopic.Id, status);
            }

            rotationTopic.Enabled = status;
            _repositories.RotationTopicRepository.Update(rotationTopic);

            _repositories.Save();
        }

        public void UpdateTemplateTopicChildTopicsStatus(Guid templateTopicId, bool status, bool? metrics, string unitSelectedType = null)
        {
            var templateTopic = _logicCore.TopicCore.GetTopic(templateTopicId);

            var rotationTopicGroups =
                _logicCore.RotationTopicGroupCore.GetActiveRotationTopicGroupsByTemplate(templateTopic.TopicGroupId).ToList();

            var shiftTopicGroups = _logicCore.RotationTopicGroupCore.GetActiveShiftTopicGroupsByTemplate(templateTopic.TopicGroupId).ToList();

            rotationTopicGroups.AddRange(shiftTopicGroups);

            foreach (var rotationTopicGroup in rotationTopicGroups)
            {
                var rotationTopic = _logicCore.RotationTopicCore.GetOrCreateRotationTopic(rotationTopicGroup.Id,
                    templateTopic);

                rotationTopic.PlannedActualHas = metrics;
                rotationTopic.UnitsSelectedType = unitSelectedType;
                rotationTopic.IsExpandPlannedActualField = metrics;

                if (status == true)
                {
                    _services.RotationModuleService.ChangeRotationModuleStatus(rotationTopicGroup.RotationModuleId, true);
                    _services.RotationTopicGroupService.UpdateRotationTopicGroupStatus(rotationTopicGroup.Id, true);
                }

                UpdateRotationTopicStatus(rotationTopic.Id, status);
            }
        }

        public bool IsTopicExist(Guid topicId)
        {
            return _logicCore.RotationTopicCore.IsTopicExist(topicId);
        }

        public bool IsTopicExist(Guid topicGroupId, Guid topicId, string name)
        {
            return _logicCore.RotationTopicCore.IsTopicExist(topicGroupId, topicId, name);
        }

        public bool IsTopicAssignedWithSameNameExistByTopic(Guid rotationTopicId, Guid assignedToId, string name)
        {
            return _repositories.RotationTopicRepository.Find(t => t.RelationId.Value == assignedToId && t.Id != rotationTopicId && t.Name.Equals(name)).Any();
        }

        public bool IsTopicAssignedWithSameNameExist(Guid destId, Guid assignedToId, string name, Models.RotationType rotationType = Models.RotationType.Swing)
        {

            RotationTopicGroup teamTopicGroup = (rotationType == Models.RotationType.Swing) ?
                                                    _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, destId, TypeOfModuleSource.Draft)
                                                        .RotationTopicGroups.FirstOrDefault() :
                                                    _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team, destId, TypeOfModuleSource.Draft)
                                                        .RotationTopicGroups.FirstOrDefault();

            return _repositories.RotationTopicRepository.Find(
                    t => t.RelationId == assignedToId && t.RotationTopicGroupId == teamTopicGroup.Id && t.Name.Equals(name)).Any();
        }

        public void UpdateHSETopic(HSETopicDialogViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                var isNameChanged = model.Name != topic.Name;
                var isChangedNotes = model.Notes != topic.Description;

                if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                }
                else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                }

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                if (isNameChanged)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> changed the reference name.", userId);
                }

                if (isChangedNotes)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                }

            }
        }

        public string GetTagsByTopicId(Guid topicId, Guid userId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null)
            {
                 var model = _mapper.Map<HSETopicDialogViewModel>(topic);
                 //topic.SearchTags = model.TagsString;
                return model.TagsString;
            }

            return string.Empty;
        }

        public int? GetShareCounter(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null)
            {
                return topic.TopicSharingRelations.ToList().GroupBy(t => t.RecipientId).Select(t => t.FirstOrDefault()).Count();
            }

            return null;
        }

        public TagsTopic GetTagsTopicByTopicId(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null)
            {
                return new TagsTopic
                {
                    FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault() : topic.FirstTagTopic = null,
                    MoreTags = topic.SearchTags != null && topic.SearchTags.Split(',').Count() > 1 ? true : false,
                    HasTags = topic.SearchTags != null ? true : false
                };
            }

            return null;
        }

        public TagsTopic GetTagsTopicByTopicId(RotationTopic rotationTopic)
        {
            if (rotationTopic != null)
            {
                return new TagsTopic
                {
                    FirstTagTopic = rotationTopic.SearchTags != null ? rotationTopic.SearchTags.Split(',').FirstOrDefault() : rotationTopic.FirstTagTopic = null,
                    MoreTags = rotationTopic.SearchTags != null && rotationTopic.SearchTags.Split(',').Count() > 1 ? true : false,
                    HasTags = rotationTopic.SearchTags != null ? true : false
                };
            }

            return null;
        }

        public void SetTagsByTopicId(Guid topicId, Guid userId, List<string> tags)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null)
            {
                if (topic.SearchTags != null)
                {
                    if (tags.Count() > topic.SearchTags.Split(',').ToList().Count)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the tag \"{ tags.Last()}\".", userId);
                    }
                    else
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> removed the tag.", userId);
                    }
                }
                else
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the tag \"{ tags.Last()}\".", userId);
                }

                topic.Tags = tags;
                topic.HasTags = topic.Tags.Count() > 0 ? true : false;

                string tagsString = string.Empty;

                if (topic.Tags != null)
                {
                    topic.Tags.ToList().ForEach(t =>
                    {
                        if (!string.IsNullOrWhiteSpace(t))
                        {
                            tagsString = tagsString == "" ? string.Format("{0}", t) : string.Format("{0},{1}", tagsString, t);
                        }
                    });

                    if (tagsString != "")
                    {
                        topic.SearchTags = tagsString;
                    }
                    else
                    {
                        topic.SearchTags = null;
                    }

                    topic.FirstTagTopic = topic.Tags.FirstOrDefault();
                    topic.MoreTags = topic.Tags.ToList().Count > 1 ? true : false;

                    _repositories.RotationTopicRepository.Update(topic);
                    _repositories.Save();
                }
            }
        }

        public PlannedActual ChangePlannedByTopicIdNew(Guid topicId, string planned)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null && planned != null)
            {
                topic.Actual = topic.Actual;
                topic.Planned = planned == "" ? topic.Planned = null : topic.Planned = planned;

                if (planned != null && planned != "" && topic.Actual != null && topic.Actual != "")
                {
                    topic.Variance = (float.Parse(topic.Actual) - float.Parse(topic.Planned)).ToString();
                }

                if (planned != null && planned != "" && topic.Actual == null || planned != null && planned != "" && topic.Actual == "")
                {
                    topic.Variance = "-" + topic.Planned;
                }

                if (planned == null && topic.Actual != null && topic.Actual != "" || planned == "" && topic.Actual != null && topic.Actual != "")
                {
                    topic.Variance = topic.Actual;
                }

                topic.UnitsSelectedType = topic.TempateTopic != null && topic.TempateTopic.ParentTopic != null && topic.TempateTopic.ParentTopic.UnitsSelectedType != null ? topic.TempateTopic.ParentTopic.UnitsSelectedType : topic.UnitsSelectedType;

                if (topic.Actual != null || topic.Planned != null || topic.UnitsSelectedType != null)
                {
                    topic.IsExpandPlannedActualField = true;
                    topic.PlannedActualHas = true;
                }
                else
                {
                    topic.IsExpandPlannedActualField = false;
                    topic.PlannedActualHas = false;
                }

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                return new PlannedActual()
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    UnitsSelectedType = topic.UnitsSelectedType,
                    Variance = topic.Variance,
                    Units = GetUnitsPvA()
                };
            }

            return null;
        }

        public PlannedActual ChangeActualByTopicIdNew(Guid topicId, string actual)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null && actual != null)
            {
                topic.Actual = actual == "" ? topic.Actual = null : topic.Actual = actual;
                topic.Planned = topic.Planned;

                if (topic.Planned != null && topic.Planned != "" && actual != null && actual != "")
                {
                    topic.Variance = (float.Parse(actual) - float.Parse(topic.Planned)).ToString();
                }

                if (topic.Planned != null && topic.Planned != "" && actual == null || topic.Planned != null && topic.Planned != "" && actual == "")
                {
                    topic.Variance = "-" + topic.Planned;
                }

                if (topic.Planned == null && topic.Actual != null && actual != "" || topic.Planned == "" && topic.Actual != null && actual != "")
                {
                    topic.Variance = actual;
                }

                topic.UnitsSelectedType = topic.TempateTopic != null && topic.TempateTopic.ParentTopic != null && topic.TempateTopic.ParentTopic.UnitsSelectedType != null ? topic.TempateTopic.ParentTopic.UnitsSelectedType : topic.UnitsSelectedType;

                if (topic.Actual != null || topic.Planned != null || topic.UnitsSelectedType != null)
                {
                    topic.IsExpandPlannedActualField = true;
                    topic.PlannedActualHas = true;
                }
                else
                {
                    topic.IsExpandPlannedActualField = false;
                    topic.PlannedActualHas = false;
                }

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                return new PlannedActual()
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    UnitsSelectedType = topic.UnitsSelectedType,
                    Variance = topic.Variance,
                    Units = GetUnitsPvA()
                };
            }

            return null;
        }

        public PlannedActual ChangeUnitsTypeByTopicIdNew(Guid topicId, string unitsSelected)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null && unitsSelected != null)
            {
                topic.Actual = topic.Actual;
                topic.Planned = topic.Planned;
                topic.UnitsSelectedType = unitsSelected == "" ? topic.UnitsSelectedType = null : topic.UnitsSelectedType = unitsSelected;

               // topic.UnitsSelectedType = topic.TempateTopic != null && topic.TempateTopic.ParentTopic != null && topic.TempateTopic.ParentTopic.UnitsSelectedType != null ? topic.TempateTopic.ParentTopic.UnitsSelectedType : unitsSelected;

                if (topic.Actual != null || topic.Planned != null || unitsSelected != null)
                {
                    topic.IsExpandPlannedActualField = true;
                    topic.PlannedActualHas = true;
                }
                else
                {
                    topic.IsExpandPlannedActualField = false;
                    topic.PlannedActualHas = false;
                }

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                return new PlannedActual()
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    UnitsSelectedType = topic.UnitsSelectedType,
                    Variance = topic.Variance,
                    Units = GetUnitsPvA()
                };
            }
            return null;
        }

        public PlannedActual GetPlannedVsActualByTopicId(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null)
            {
                return new PlannedActual
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    Variance = topic.Variance,
                    Units = GetUnitsPvA(),
                    IsExpandPlannedActualField = topic.IsExpandPlannedActualField,
                    UnitsSelectedType = _services.RotationTopicService.GetUnitSelected(topic)
                };
            }
            return null;
        }

        public bool IsExpandTemplateTopic(Guid topicId)
        {
            TemplateTopic topic = _repositories.TemplateTopicRepository.Get(topicId);

            if (topic != null)
            {
                return topic.IsExpandPlannedActualField.HasValue ? topic.IsExpandPlannedActualField.Value : false;
            }

            return false;
        }

        public string GetUnitSelected(RotationTopic topic)
        {
            if (topic != null)
            {
                return topic.UnitsSelectedType != null ? topic.UnitsSelectedType
                    : topic.TempateTopic != null && topic.TempateTopic.ParentTopic != null ? topic.TempateTopic.ParentTopic.UnitsSelectedType : null;
            }

            return null;
        }

        public PlannedActual GetPlannedVsActualTopicId(RotationTopic topicRotation)
        {
            if (topicRotation != null)
            {
                return new PlannedActual
                {
                    Actual = topicRotation.Actual,
                    Planned = topicRotation.Planned,
                    Units = GetUnitsPvA(),
                    Variance = topicRotation.Variance,
                    IsExpandPlannedActualField = topicRotation.TempateTopic != null && topicRotation.TempateTopic.ParentTopic != null ? _services.RotationTopicService.IsExpandTemplateTopic(topicRotation.TempateTopic.ParentTopic.Id) : false,
                    UnitsSelectedType = topicRotation.TempateTopic != null && topicRotation.TempateTopic.ParentTopic != null ? topicRotation.TempateTopic.ParentTopic.UnitsSelectedType : topicRotation.UnitsSelectedType
                };
            }
            return null;
        }

        public PlannedActual GetPlannedVsActualByTempleteTopicId(Guid topicId)
        {
            TemplateTopic topic = _repositories.TemplateTopicRepository.Get(topicId);

            if (topic != null)
            {
                return new PlannedActual
                {
                    Units = GetUnitsPvA(),
                    UnitsSelectedType = topic.UnitsSelectedType,
                };
            }
            return null;
        }

        public List<SelectListItem> GetUnitsPvA()
        {
            return new List<SelectListItem>()
            {
                new SelectListItem { Value = 0.ToString(), Text = "Seconds" },
                new SelectListItem { Value = 1.ToString(), Text = "Minutes" },
                new SelectListItem { Value = 2.ToString(), Text = "Hours" },
                new SelectListItem { Value = 3.ToString(), Text = "Days" },
                new SelectListItem { Value = 4.ToString(), Text = "GBP" },
                new SelectListItem { Value = 5.ToString(), Text = "USD" },
                new SelectListItem { Value = 6.ToString(), Text = "AUD" },
                new SelectListItem { Value = 7.ToString(), Text = "Grams" },
                new SelectListItem { Value = 8.ToString(), Text = "Kilograms" },
                new SelectListItem { Value = 9.ToString(), Text = "Metres" },
                new SelectListItem { Value = 10.ToString(), Text = "BCM" },
                new SelectListItem { Value = 11.ToString(), Text = "Tonnes" },
                new SelectListItem { Value = 12.ToString(), Text = "t.oz" },
                new SelectListItem { Value = 13.ToString(), Text = "t/h" },
                new SelectListItem { Value = 14.ToString(), Text = "Loads/h" },
                new SelectListItem { Value = 15.ToString(), Text = "metres/h" },
                new SelectListItem { Value = 16.ToString(), Text = "Grade" },
                new SelectListItem { Value = 17.ToString(), Text = "%" }
            };
        }

        public HSETopicViewModel UpdateHSETopic(HSETopicViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                if (topic.Id != null)
                {
                    if (model.Planned != topic.Planned)
                    {
                        model.Planned = topic.Planned;
                        model.PlannedActualHas = true;
                    }

                    if (model.Actual != topic.Actual)
                    {
                        model.Actual = topic.Actual;
                        model.PlannedActualHas = true;
                    }

                    if (model.UnitsSelectedType != topic.UnitsSelectedType)
                    {
                        model.UnitsSelectedType = topic.UnitsSelectedType;
                        model.PlannedActualHas = true;
                    }

                    if(model.UnitsSelectedType == null)
                    {
                        model.UnitsSelectedType = topic.TempateTopicId.HasValue ? topic.TempateTopic.UnitsSelectedType : null;
                        model.PlannedActualHas = model.UnitsSelectedType != null ? true : false;
                    }

                    if (model.Variance != topic.Variance)
                    {
                        model.Variance = topic.Variance;
                    }

                    var isChangedNotes = model.Notes != topic.Description;

                    if (topic.Description != model.Notes)
                    {
                        topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                        model.IsFinalized = false;
                    }

                    if (model.IsPinned.HasValue)
                    {
                        if (model.IsPinned.Value && !topic.IsPinned.Value)
                        {
                            model.IsPinned = true;
                            _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the blue pin.", userId);
                        }
                        else if (!model.IsPinned.Value && topic.IsPinned.Value)
                        {
                            model.IsPinned = false;
                            _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the blue pin.", userId);
                        }
                    }

                    if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                    }
                    else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                    }

                    if (!topic.IsPinned.HasValue)
                    {
                        if (model.IsNullReport && !topic.IsNullReport)
                        {
                            _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> checked.", userId);
                        }
                        else if (!model.IsNullReport && topic.IsNullReport)
                        {
                            _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> unchecked.", userId);
                        }
                    }

                    if (model.HasTags.HasValue && topic.HasTags.HasValue)
                    {
                        if (model.HasTags.Value && !topic.HasTags.Value)
                        {
                            model.HasTags = false;
                        }
                        else if (!model.HasTags.Value && topic.HasTags.Value)
                        {
                            model.HasTags = true;
                        }
                    }

                    model.Type = topic.IsFirstTopicByTopicGroupName == null ? model.Type = null : model.Type;

                    if (topic.IsFirstTopicByTopicGroupName.HasValue && topic.IsFirstTopicByTopicGroupName.Value)
                    {
                        model.Type = topic.RotationTopicGroup.Name;
                        model.IsFirstTopicByTopicGroupName = true;
                    }

                    _mapper.Map(model, topic);

                    if (!model.HasAttachments && topic.AttachmentsLink != null && topic.AttachmentsLink.Any())
                    {
                        model.HasAttachments = true;
                    }

                    if (model.IsNullReport)
                    {
                        _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                        topic.FinalizeStatus = StatusOfFinalize.Finalized;
                    }

                    _repositories.RotationTopicRepository.Update(topic);
                    _logicCore.RotationTopicCore.UpdateTopic(topic);
                    _repositories.Save();

                    if (isChangedNotes)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                    }

                    model = _mapper.Map<HSETopicViewModel>(topic);

                    model.EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topic, userId, topic.CreatorId.HasValue ? topic.CreatorId.Value : (Guid?)null).ToList();

                    if (model.Actual != null || model.Planned != null || model.UnitsSelectedType != null)
                    {
                        model.PlannedActualHas = true;
                    }

                    model.PlannedActualField = new PlannedActual
                    {
                        Actual = topic.Actual,
                        Planned = topic.Planned,
                        Variance = topic.Variance,
                        UnitsSelectedType = topic.UnitsSelectedType,
                        IsExpandPlannedActualField = topic.TempateTopicId.HasValue ? topic.TempateTopic.IsExpandPlannedActualField : topic.IsExpandPlannedActualField
                    };

                    if (!model.HasAttachments && topic.AttachmentsLink != null && topic.AttachmentsLink.Any())
                    {
                        model.HasAttachments = true;
                    }

                    model.Type = topic.IsFirstTopicByTopicGroupName == null ? model.Type = null : model.Type;

                    if (model.IsFirstTopicByTopicGroupName.HasValue && model.IsFirstTopicByTopicGroupName.Value)
                    {
                        model.Type = topic.RotationTopicGroup.Name;
                    }

                   // model.SharedCounter = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? 0 : topic.TopicSharingRelations != null && topic.TopicSharingRelations.Any() ? topic.TopicSharingRelations.Count : 0;
                    model.IsSharingTopic = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? false : topic.ShareSourceTopicId.HasValue;
                }
            }
            return model;
        }

        public List<SelectListItem> GetContributorUsers(HSETopicViewModel model, Guid userId)
        {
            if (model.Id.HasValue)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);
                model.EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topic, userId, topic.CreatorId.HasValue ? topic.CreatorId.Value : (Guid?)null).ToList();
                return model.EditorUsers;
            }
            return null;
        }

        public List<SelectListItem> GetContributorUsersByTopicId(Guid topicId, Guid userId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null)
            {
                var topicOwnreId = topic.RotationTopicGroup.RotationModule.ShiftId.HasValue
                            ? topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId
                            : topic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId;

                //return topic.RotationTopicGroup.RotationModule.ShiftId.HasValue
                //     ? topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.Contributors.Select(t =>
                //         new SelectListItem
                //         {
                //             Text = t.FullName,
                //             Value = t.Id.ToString()
                //         }).ToList()
                // : topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.Contributors.Select(t =>
                //         new SelectListItem
                //         {
                //             Text = t.FullName,
                //             Value = t.Id.ToString()
                //         }).ToList();

                return _logicCore.ContributorsCore.ContributorsWhoEditTopic(topic, userId, topicOwnreId).ToList();
            }
            return null;
        }

        public List<SelectListItem> ShowIconContributorUsers(Guid userId, RotationTopic rotationTopic)
        {
            return _logicCore.ContributorsCore.ContributorsWhoEditTopic(rotationTopic, userId, rotationTopic.CreatorId.HasValue ? rotationTopic.CreatorId.Value : (Guid?)null).ToList();
        }

        public HSETopicViewModel GetHSETopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                var model = _mapper.Map<HSETopicViewModel>(topic);
                model.HasAttachments = topic.Attachments != null && topic.Attachments.Any() || topic.AttachmentsLink != null && topic.AttachmentsLink.Any();
                return model;
            }

            return null;
        }

        public void AddHSETopic(HSEAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue && model.Type.HasValue)
            {
                AddRotationTopicViewModel rotationTopicModel = new AddRotationTopicViewModel
                {
                    RotationId = model.RotationId.Value,
                    RotationTopicGroupId = model.Type.Value,
                    AssignedToId = _logicCore.RotationCore.GetRotation(model.RotationId.Value).DefaultBackToBackId,
                    Name = model.Reference,
                    Notes = model.Notes,
                    CreatorId = model.CreatorId,
                };

                _logicCore.RotationTopicCore.AddRotationTopic(rotationTopicModel);
            }
        }

        public CoreTopicViewModel UpdateCoreTopic(CoreTopicViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                if (topic.Description != model.Notes)
                {
                    topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    model.IsFinalized = false;
                }

                if (model.Planned != topic.Planned)
                {
                    model.Planned = topic.Planned;
                    model.PlannedActualHas = true;
                }

                if (model.Actual != topic.Actual)
                {
                    model.Actual = topic.Actual;
                    model.PlannedActualHas = true;
                }

                if (model.UnitsSelectedType != topic.UnitsSelectedType)
                {
                    model.UnitsSelectedType = topic.UnitsSelectedType;
                    model.PlannedActualHas = true;
                }

                if (model.UnitsSelectedType == null)
                {
                    model.UnitsSelectedType = topic.TempateTopicId.HasValue ? topic.TempateTopic.UnitsSelectedType : null;
                    model.PlannedActualHas = model.UnitsSelectedType != null ? true : false;
                }

                if (model.Variance != topic.Variance)
                {
                    model.Variance = topic.Variance;
                }

                if (model.IsPinned.HasValue)
                {
                    if (model.IsPinned.Value && !topic.IsPinned.Value)
                    {
                        model.IsPinned = true;
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the blue pin.", userId);
                    }
                    else if (!model.IsPinned.Value && topic.IsPinned.Value)
                    {
                        model.IsPinned = false;
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the blue pin.", userId);
                    }
                }

                var isChangedNotes = model.Notes != topic.Description;

                if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                }
                else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                }

                if (!topic.IsPinned.HasValue)
                {
                    if (model.IsNullReport && !topic.IsNullReport)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> checked.", userId);
                    }
                    else if (!model.IsNullReport && topic.IsNullReport)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> unchecked.", userId);
                    }
                }
                if (model.HasTags.HasValue && topic.HasTags.HasValue)
                {
                    if (model.HasTags.Value && !topic.HasTags.Value)
                    {
                        model.HasTags = false;
                    }
                    else if (!model.HasTags.Value && topic.HasTags.Value)
                    {
                        model.HasTags = true;
                    }
                }

                model.PlannedActualField = new PlannedActual
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    UnitsSelectedType = topic.UnitsSelectedType,
                    IsExpandPlannedActualField = topic.TempateTopicId.HasValue ? topic.TempateTopic.IsExpandPlannedActualField : topic.IsExpandPlannedActualField
                };

                model.Category = topic.IsFirstTopicByTopicGroupName == null ? model.Category = null : model.Category;
        
                if (topic.IsFirstTopicByTopicGroupName.HasValue && topic.IsFirstTopicByTopicGroupName.Value)
                {
                    model.Category = topic.RotationTopicGroup.Name;
                    model.IsFirstTopicByTopicGroupName = true;
                }
                _mapper.Map(model, topic);

                if (model.IsNullReport)
                {
                    _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                    topic.FinalizeStatus = StatusOfFinalize.Finalized;
                }

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                if (isChangedNotes)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                }

                model = _mapper.Map<CoreTopicViewModel>(topic);

                model.PlannedActualField = new PlannedActual
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    Variance = topic.Variance,
                    UnitsSelectedType = topic.UnitsSelectedType
                };

                model.EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topic, userId, topic.CreatorId.HasValue ? topic.CreatorId.Value : (Guid?)null).ToList();

                if (!model.HasAttachments && topic.AttachmentsLink != null && topic.AttachmentsLink.Any())
                {
                    model.HasAttachments = true;
                }

                if (model.Actual != null || model.Planned != null || model.UnitsSelectedType != null)
                {
                    model.PlannedActualHas = true;
                }

                model.Category = topic.IsFirstTopicByTopicGroupName == null ? model.Category = null : model.Category;

                if (topic.IsFirstTopicByTopicGroupName.HasValue && topic.IsFirstTopicByTopicGroupName.Value)
                {
                    model.Category = topic.RotationTopicGroup.Name;
                    model.IsFirstTopicByTopicGroupName = true;
                }

              //  model.SharedCounter = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? 0 : topic.TopicSharingRelations != null && topic.TopicSharingRelations.Any() ? topic.TopicSharingRelations.Count : 0;
                model.IsSharingTopic = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? false : topic.ShareSourceTopicId.HasValue;

            }
            return model;
        }

        public CoreTopicViewModel GetCoreTopic(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                var model = _mapper.Map<CoreTopicViewModel>(topic);
                model.HasAttachments = topic.Attachments != null && topic.Attachments.Any() || topic.AttachmentsLink != null && topic.AttachmentsLink.Any();
                return model;
            }

            return null;
        }

        public void AddRotationCoreTopic(RotationCoreAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue && model.ProcessGroup.HasValue)
            {
                AddRotationTopicViewModel rotationTopicModel = new AddRotationTopicViewModel
                {
                    RotationId = model.RotationId.Value,
                    RotationTopicGroupId = model.ProcessGroup.Value,
                    AssignedToId = _logicCore.RotationCore.GetRotation(model.RotationId.Value).DefaultBackToBackId,
                    Name = model.ProcessLocation,
                    Notes = model.Notes,
                    CreatorId = model.CreatorId
                };

                _logicCore.RotationTopicCore.AddRotationTopic(rotationTopicModel);
            }
        }

        public ProjectsTopicViewModel UpdateProjectTopic(ProjectsTopicViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                if (topic.Description != model.Notes)
                {
                    topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    model.IsFinalized = false;
                }

                if (model.Planned != topic.Planned)
                {
                    model.Planned = topic.Planned;
                    model.PlannedActualHas = true;
                }

                if (model.Actual != topic.Actual)
                {
                    model.Actual = topic.Actual;
                    model.PlannedActualHas = true;
                }

                if (model.UnitsSelectedType != topic.UnitsSelectedType)
                {
                    model.UnitsSelectedType = topic.UnitsSelectedType;
                    model.PlannedActualHas = true;
                }

                if (model.Variance != topic.Variance)
                {
                    model.Variance = topic.Variance;
                }

                model.PlannedActualHas = topic.PlannedActualHas;

                model.IsExpandPlannedActualField = topic.IsExpandPlannedActualField;

                var isChangedNotes = model.Notes != topic.Description;

                if (model.IsPinned.HasValue)
                {
                    if (model.IsPinned.Value && !topic.IsPinned.Value)
                    {
                        model.IsPinned = true;
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the blue pin.", userId);
                    }
                    else if (!model.IsPinned.Value && topic.IsPinned.Value)
                    {
                        model.IsPinned = false;
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the blue pin.", userId);
                    }
                }

                if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                }
                else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                }

                if (!topic.IsPinned.HasValue)
                {
                    if (model.IsNullReport && !topic.IsNullReport)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> checked.", userId);
                    }
                    else if (!model.IsNullReport && topic.IsNullReport)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> unchecked.", userId);
                    }
                }
                if (model.HasTags.HasValue && topic.HasTags.HasValue)
                {
                    if (model.HasTags.Value && !topic.HasTags.Value)
                    {
                        model.HasTags = false;
                    }
                    else if (!model.HasTags.Value && topic.HasTags.Value)
                    {
                        model.HasTags = true;
                    }
                }

                model.Project = topic.IsFirstTopicByTopicGroupName == null ? model.Project = null : model.Project;
               
                if (topic.IsFirstTopicByTopicGroupName.HasValue && topic.IsFirstTopicByTopicGroupName.Value)
                {
                    model.Project = topic.RotationTopicGroup.Name;
                    model.IsFirstTopicByTopicGroupName = true;
                }

                _mapper.Map(model, topic);

                if (model.IsNullReport)
                {
                    _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                    topic.FinalizeStatus = StatusOfFinalize.Finalized;
                }

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                if (isChangedNotes)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                }

                model = _mapper.Map<ProjectsTopicViewModel>(topic);

                model.EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topic, userId, topic.CreatorId.HasValue ? topic.CreatorId.Value : (Guid?)null).ToList();

                if (!model.HasAttachments && topic.AttachmentsLink != null && topic.AttachmentsLink.Any())
                {
                    model.HasAttachments = true;
                }

                model.PlannedActualField = new PlannedActual
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    Variance = topic.Variance,
                    UnitsSelectedType = topic.UnitsSelectedType
                };

                model.Project = topic.IsFirstTopicByTopicGroupName == null ? model.Project = null : model.Project;

                if (topic.IsFirstTopicByTopicGroupName.HasValue && topic.IsFirstTopicByTopicGroupName.Value)
                {
                    model.Project = topic.RotationTopicGroup.Name;
                    model.IsFirstTopicByTopicGroupName = true;
                }

                if (model.Actual != null || model.Planned != null || model.UnitsSelectedType != null)
                {
                    model.PlannedActualHas = true;
                }

             //   model.SharedCounter = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? 0 : topic.TopicSharingRelations != null && topic.TopicSharingRelations.Any() ? topic.TopicSharingRelations.Count : 0;
                model.IsSharingTopic = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? false : topic.ShareSourceTopicId.HasValue;
            }
            return model;
        }

        public ProjectsTopicViewModel GetProjectTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                var model = _mapper.Map<ProjectsTopicViewModel>(topic);
                model.HasAttachments = topic.Attachments != null && topic.Attachments.Any() || topic.AttachmentsLink != null && topic.AttachmentsLink.Any();
                return model;
            }

            return null;
        }

        public void AddProjectTopic(ProjectAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue && model.Project.HasValue)
            {
                AddRotationTopicViewModel rotationTopicModel = new AddRotationTopicViewModel
                {
                    RotationId = model.RotationId.Value,
                    RotationTopicGroupId = model.Project.Value,
                    AssignedToId = _logicCore.RotationCore.GetRotation(model.RotationId.Value).DefaultBackToBackId,
                    Name = model.Reference,
                    Notes = model.Notes,
                    CreatorId = model.CreatorId
                };

                _logicCore.RotationTopicCore.AddRotationTopic(rotationTopicModel);
            }
        }

        public TeamTopicViewModel UpdateTeamTopic(TeamTopicViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                if (topic.Description != model.Notes)
                {
                    topic.FinalizeStatus = StatusOfFinalize.NotFinalized;
                    model.IsFinalized = false;
                }

                if (model.Planned != topic.Planned)
                {
                    model.Planned = topic.Planned;
                    model.PlannedActualHas = true;
                }

                if (model.Actual != topic.Actual)
                {
                    model.Actual = topic.Actual;
                    model.PlannedActualHas = true;
                }

                if (model.UnitsSelectedType != topic.UnitsSelectedType)
                {
                    model.UnitsSelectedType = topic.UnitsSelectedType;
                    model.PlannedActualHas = true;
                }

                if (model.Variance != topic.Variance)
                {
                    model.Variance = topic.Variance;
                }

                model.PlannedActualHas = topic.PlannedActualHas;

                model.IsExpandPlannedActualField = topic.IsExpandPlannedActualField;

                var isChangedNotes = model.Notes != topic.Description;

                if (model.IsPinned.HasValue)
                {
                    if (model.IsPinned.Value && !topic.IsPinned.Value)
                    {
                        model.IsPinned = true;
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the blue pin.", userId);
                    }
                    else if (!model.IsPinned.Value && topic.IsPinned.Value)
                    {
                        model.IsPinned = false;
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the blue pin.", userId);
                    }
                }

                if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                }
                else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                }

                if (!topic.IsPinned.HasValue)
                {
                    if (model.IsNullReport && !topic.IsNullReport)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> checked.", userId);
                    }
                    else if (!model.IsNullReport && topic.IsNullReport)
                    {
                        _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> unchecked.", userId);
                    }
                }

                if (model.HasTags.HasValue && topic.HasTags.HasValue)
                {
                    if (model.HasTags.Value && !topic.HasTags.Value)
                    {
                        model.HasTags = false;
                    }
                    else if (!model.HasTags.Value && topic.HasTags.Value)
                    {
                        model.HasTags = true;
                    }
                }

                model.TeamMember = topic.IsFirstTopicByTopicGroupName == null ? model.TeamMember = null : model.TeamMember;

                if (topic.IsFirstTopicByTopicGroupName.HasValue && topic.IsFirstTopicByTopicGroupName.Value)
                {
                    model.TeamMember = topic.RelationId.HasValue == false || topic.RelationId.Value == Guid.Empty
                 ? "Other"
                 : _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName;
                    model.IsFirstTopicByTopicGroupName = true;
                }

                _mapper.Map(model, topic);

                if (model.IsNullReport)
                {
                    _logicCore.RotationTopicCore.CleaRotationTopicData(topic, false);
                    topic.FinalizeStatus = StatusOfFinalize.Finalized;
                }

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                model = _mapper.Map<TeamTopicViewModel>(topic);

                if (isChangedNotes)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                }

                model.TeamMember = topic.RelationId.HasValue == false || topic.RelationId.Value == Guid.Empty
                    ? "Other"
                    : _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName;

                model.EditorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topic, userId, topic.CreatorId.HasValue ? topic.CreatorId.Value : (Guid?)null).ToList();

                if (!model.HasAttachments && topic.AttachmentsLink != null && topic.AttachmentsLink.Any())
                {
                    model.HasAttachments = true;
                }

                model.PlannedActualField = new PlannedActual
                {
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    Variance = topic.Variance,
                    UnitsSelectedType = topic.UnitsSelectedType
                };

                  model.TeamMember = topic.IsFirstTopicByTopicGroupName == null ? model.TeamMember = null : model.TeamMember;

                if (topic.IsFirstTopicByTopicGroupName.HasValue && topic.IsFirstTopicByTopicGroupName.Value)
                {
                    model.TeamMember = topic.RelationId.HasValue == false || topic.RelationId.Value == Guid.Empty
                   ? "Other"
                   : _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName;
                    model.IsFirstTopicByTopicGroupName = true;
                }

                if (model.Actual != null || model.Planned != null || model.UnitsSelectedType != null)
                {
                    model.PlannedActualHas = true;
                }

              //  model.SharedCounter = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? 0 : topic.TopicSharingRelations != null && topic.TopicSharingRelations.Any() ? topic.TopicSharingRelations.Count : 0;
                model.IsSharingTopic = topic.IsRecipientsFromPrevRotation.HasValue && topic.IsRecipientsFromPrevRotation.Value ? false : topic.ShareSourceTopicId.HasValue;
            }
            return model;
        }

        public TeamTopicViewModel GetTeamTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                var model = _mapper.Map<TeamTopicViewModel>(topic);

                model.TeamMember = topic.RelationId.HasValue == false || topic.RelationId.Value == Guid.Empty
                    ? "Other"
                    : _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName;

                model.HasAttachments = topic.Attachments != null && topic.Attachments.Any() || topic.AttachmentsLink != null && topic.AttachmentsLink.Any();

                return model;
            }

            return null;
        }

        public void AddTeamTopic(TeamAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {

                var rotationModule = _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team,
                                                                                        model.RotationId.Value,
                                                                                        TypeOfModuleSource.Draft);

                AddRotationTopicViewModel rotationTopicModel = new AddRotationTopicViewModel
                {
                    RotationId = model.RotationId.Value,
                    RotationTopicGroupId = rotationModule.RotationTopicGroups.FirstOrDefault(tg => tg.Enabled).Id,
                    AssignedToId = rotationModule.Rotation.DefaultBackToBackId,
                    Name = model.Reference,
                    Notes = model.Notes,
                    RelationId = model.TeamMember.HasValue ? model.TeamMember : null,
                    CreatorId = model.CreatorId
                };

                _logicCore.RotationTopicCore.AddRotationTopic(rotationTopicModel);
            }
        }

        public void UpdateCoreTopic(CoreTopicDialogViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                var isNameChanged = model.Name != topic.Name;
                var isChangedNotes = model.Notes != topic.Description;

                if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                }
                else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                }

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                if (isNameChanged)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> changed the reference name.", userId);
                }
                if (isChangedNotes)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                }
            }

        }

        public void UpdateProjectTopic(ProjectTopicDialogViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                var isNameChanged = model.Name != topic.Name;
                var isChangedNotes = model.Notes != topic.Description;

                if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                }
                else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                }

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                if (isNameChanged)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> changed the reference name.", userId);
                }

                if (isChangedNotes)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                }
            }

        }

        public void UpdateTeamTopic(TeamTopicDialogViewModel model, Guid userId)
        {
            if (model.Id != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(model.Id.Value);

                var isNameChanged = model.Name != topic.Name;
                var isChangedNotes = model.Notes != topic.Description;

                if (model.IsFeedbackRequired && !topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> added the hand-back.", userId);
                }
                else if (!model.IsFeedbackRequired && topic.IsFeedbackRequired)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> remove the hand-back.", userId);
                }

                _mapper.Map(model, topic);

                topic.FinalizeStatus = StatusOfFinalize.NotFinalized;

                _repositories.RotationTopicRepository.Update(topic);

                _repositories.Save();

                if (isNameChanged)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> changed the reference name.", userId);
                }

                if (isChangedNotes)
                {
                    _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> edited the topic’s notes.", userId);
                }
            }

        }

        public HSEAddInlineTopicViewModel PopulateHSETopicInlineModel(Guid rotationId, Guid moduleId)
        {
            var model = new HSEAddInlineTopicViewModel
            {
                RotationId = rotationId,
                Types =
                    _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(moduleId)
            };

            return model;
        }

        public HSETopicDialogViewModel PopulateHSETopicDialogModel(Guid topicId, Guid userId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                var model = _mapper.Map<HSETopicDialogViewModel>(topic);

                model.CreatorId = userId;

                model.RotationTopicGroups =
                    _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                        topic.RotationTopicGroup.RotationModuleId);

                return model;
            }
            return null;
        }

        public CoreTopicDialogViewModel PopulateCoreTopicDialogModel(Guid topicId, Guid userId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<CoreTopicDialogViewModel>(topic);

            model.CreatorId = userId;

            model.RotationTopicGroups =
                _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(topic.RotationTopicGroup.RotationModuleId);

            return model;
        }

        public RotationCoreAddInlineTopicViewModel PopulateCoreTopicInlineModel(Guid rotationId, Guid moduleId)
        {
            var model = new RotationCoreAddInlineTopicViewModel
            {
                RotationId = rotationId,

            };

            var processGroups = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Core, rotationId, TypeOfModuleSource.Draft).Id);

            model.ProcessGroups = processGroups;


            return model;
        }

        public ProjectTopicDialogViewModel PopulateProjectTopicDialogModel(Guid topicId, Guid userId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<ProjectTopicDialogViewModel>(topic);

            model.CreatorId = userId;

            model.RotationTopicGroups =
                _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(
                    topic.RotationTopicGroup.RotationModuleId);

            return model;
        }

        public ProjectAddInlineTopicViewModel PopulateProjectTopicInlineModel(Guid rotationId, Guid moduleId)
        {
            var model = new ProjectAddInlineTopicViewModel
            {
                RotationId = rotationId,
                Projects =
                    _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(moduleId)
            };

            return model;
        }

        public TeamTopicDialogViewModel PopulateTeamTopicDialogModel(Guid topicId, Guid userId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<TeamTopicDialogViewModel>(topic);

            model.CreatorId = userId;

            var userTeamId = topic.RotationTopicGroup.RotationModule.Rotation != null ?
                topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.TeamId :
                topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.TeamId;

            if (userTeamId != null)
            {
                model.TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(userTeamId);
            }

            return model;
        }

        /// <summary>
        /// Populate team topic from shift
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns view-model="TeamTopicDialogViewModel"></returns>
        public TeamTopicDialogViewModel PopulateShiftTeamTopicDialogModel(Guid topicId, Guid userId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var model = _mapper.Map<TeamTopicDialogViewModel>(topic);

            model.CreatorId = userId;

            var userTeamId = topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner.TeamId;

            if (userTeamId != null)
            {
                model.TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(userTeamId);
            }

            return model;
        }

        public TeamAddInlineTopicViewModel PopulateTeamTopicInlineModel(Guid rotationId, Guid teamId)
        {

           // var rotation = _logicCore.RotationModuleCore.GetRotationModule(TypeOfModule.Team, rotationId, TypeOfModuleSource.Draft).Rotation;

            var model = new TeamAddInlineTopicViewModel
            {
                RotationId = rotationId,
                TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(teamId)
            };

            return model;
        }



        public ManagerCommentsViewModel PopulateManagerCommentViewModel(Guid topicId)
        {
            var model = new ManagerCommentsViewModel
            {
                Id = topicId,
                Notes = _logicCore.RotationTopicCore.GetTopicManagerComments(topicId)
            };

            return model;

        }

        public IEnumerable<ManagerCommentsViewModel> PopulateManagerCommentsViewModel(Guid topicId, Guid? userId)
        {
            List<ManagerCommentsViewModel> model = new List<ManagerCommentsViewModel>();

            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            foreach (ManagerComments comments in topic.ManagerComment)
            {

                UserProfile user = _logicCore.UserProfileCore.GetUserProfile(comments.CreatorManagerCommentsId.Value);

                model.Add(new ManagerCommentsViewModel
                {
                    Id = comments.Id,
                    CreatorManagerCommentsId = comments.CreatorManagerCommentsId.Value,
                    DateTimeCreatedManagerComments = comments.DateTimeCreatedManagerComments.Value,
                    IsCanRemoveManagerComments = comments.IsCanRemoveManagerComments.Value,
                    IsEditManagerComments = comments.IsEditManagerComments.Value,
                    Notes = comments.Notes,
                    IsLineManager =  comments.IsLinemanager.HasValue ? comments.IsLinemanager.Value : false,
                    FullNameLineManager = user.FullName
                });
            }

            return model;
        }

        public bool IsMyTopic(Guid userId, Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic.RotationTopicGroup.RotationModule.Rotation != null)
            {
                if (topic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId == userId)
                {
                    return true;
                }
            }
            else
            {
                if (topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId)
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsCanChangeTopic(Guid userId, Guid topicId)
        {
            if (userId != null && topicId != null)
            {
                var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

                if (topic != null)
                {

                    var rotationReportSharingRelations = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelations(topicId).FirstOrDefault();
                    if (rotationReportSharingRelations != null)
                    {
                        var isCanChangeTopic = rotationReportSharingRelations.RecipientId == userId;

                        return isCanChangeTopic;
                    }
                    return false;
                }
            }

            return false;
        }

        public bool IsContributersTopicUser(Guid userId, Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            return topic.RotationTopicGroup.RotationModule.ShiftId.HasValue
                         ?_logicCore.ContributorsCore.IsContributor(userId, topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId)
                         :_logicCore.ContributorsCore.IsContributor(userId, topic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId);

        }

        public bool IsMyShiftTopic(Guid userId, Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId == userId)
            {
                return true;
            }
            return false;
        }

        public void RemoveTopicManagerComment(Guid Id, Guid managerCommentId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(Id);

            if (topic != null)
            {
                topic.ManagerComment = topic.ManagerComment.Where(c => c.Id != topic.Id).ToList();

                var comments = _repositories.ManagerCommentsRepository.Get(managerCommentId);

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.ManagerCommentsRepository.Delete(comments);
                _repositories.Save();
            }
        }

        public void UpdateTopicManagerComment(ManagerCommentsViewModel model, Guid identityUserId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(model.Id);

            if (topic != null)
            {
                topic.ManagerComments = model.Notes;

                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                var topicOwner = topic.RotationTopicGroup.RotationModule.Rotation != null
                    ? topic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId
                    : topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwnerId;

                bool isLineManager = false;

                UserProfile user = _logicCore.UserProfileCore.GetUserProfile(identityUserId);
                var userRoles = _services.AuthService.GetUserRoles(identityUserId);

                if (userRoles.Any(r => r == iHandoverRoles.Relay.LineManager || r == iHandoverRoles.Relay.HeadLineManager))
                {
                    isLineManager = true;
                }

                var rotationOwner = _logicCore.UserProfileCore.GetUserProfile(topicOwner);

                _logicCore.NotificationCore.NotificationTrigger.Send_ManagerAddedComment(user.FirstName, user.LastName, topicOwner, topic.Id, rotationOwner.CurrentRotation.RotationOwnerId);

                var contributorUsers = rotationOwner.Contributors;

                //var contributorUsers = _logicCore.ContributorsCore.ContributorsWhoEditTopic(topic, identityUserId, topicOwner).ToList();

                if (contributorUsers != null && contributorUsers.Any())
                {
                    foreach (var contributorUser in contributorUsers)
                    {
                        _logicCore.NotificationCore.NotificationTrigger.Send_ManagerAddedComment(user.FirstName, user.LastName, contributorUser.Id, topic.Id, rotationOwner.CurrentRotation.RotationOwnerId);
                    }
                }

                topic.CreatorManagerCommentsId = user.Id;
                topic.DateTimeCreatedManagerComments = _logicCore.AdministrationCore.GetCurrentServerTime(user.Id);

                topic.ManagerComment.Add(new ManagerComments
                {
                    CreatorManagerCommentsId = user.Id,
                    DateTimeCreatedManagerComments = _logicCore.AdministrationCore.GetCurrentServerTime(user.Id),
                    Id = Guid.NewGuid(),
                    Notes = model.Notes,
                    IsCanRemoveManagerComments = true,
                    IsEditManagerComments = false,
                    IsLinemanager = isLineManager
                });

                topic.ManagerComment = topic.ManagerComment.OrderByDescending(t => t.CreatedUtc).ToList();

                _logicCore.RotationTopicCore.UpdateTopic(topic);
                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();
            }
            else if (model.IsEditManagerComments.Value)
            {
                topic = _logicCore.RotationTopicCore.GetTopic(model.RotationTopicId.Value);

                var topicComment = topic.ManagerComment;
                topic.ManagerComments = model.Notes;
                topic.IsEditManagerComments = true;

                _mapper.Map<ManagerCommentsViewModel>(model);

                _logicCore.RotationTopicCore.UpdateTopic(topic);
                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();
            }
        }

        public ManagerCommentsViewModel GetManagerCommentsViewModel(Guid topicId, Guid managerCommentId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic != null)
            {
                var comment = topic.ManagerComment.FirstOrDefault(c => c.Id == managerCommentId);
         
                comment.IsEditManagerComments = true;
                comment.RotationTopicId = topicId;

                _logicCore.RotationTopicCore.UpdateTopic(topic);
                _repositories.RotationTopicRepository.Update(topic);
                _repositories.Save();

                var model = _mapper.Map<ManagerCommentsViewModel>(comment);
                return model;
            }

            return null;
        }

        public void CarryforwardTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (!topic.TempateTopicId.HasValue)
            {
                _logicCore.RotationTopicCore.CarryforwardTopic(topic);
            }
        }

        public bool RemoveTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            if (topic != null)
            {
                if (this.IsShiftTopic(topicId))
                    return _logicCore.RotationTopicCore.RemoveShiftTopic(topic);
                else
                    return _logicCore.RotationTopicCore.RemoveTopic(topic);
            }
            return false;
        }

        public ModuleType GetTopicModuleType(Guid topicid)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicid);
            ModuleType moduleType = (ModuleType)topic.RotationTopicGroup.RotationModule.Type;

            return moduleType;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="shiftId"></param>
        /// <returns></returns>
        public ShiftCoreAddInlineTopicViewModel PopulateShiftCoreTopicInlineModel(Guid shiftId, Guid moduleId)
        {
            var processGroups = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(moduleId);

            var model = new ShiftCoreAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                ProcessGroups = processGroups
            };

            return model;
        }

        public void AddShiftCoreTopic(ShiftCoreAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue && model.ProcessGroup.HasValue)
            {
                AddShiftTopicViewModel shiftTopicModel = new AddShiftTopicViewModel
                {
                    ShiftId = model.ShiftId.Value,
                    AssignedToId = _logicCore.ShiftCore.GetShift(model.ShiftId.Value).ShiftRecipientId,
                    Name = model.ProcessLocation,
                    Notes = model.Notes,
                    RotationTopicGroupId = model.ProcessGroup.Value,
                    CreatorId = model.CreatorId
                };

                _logicCore.RotationTopicCore.AddShiftTopic(shiftTopicModel);
            }
        }

        public void AddShiftHSETopic(ShiftHSEAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue && model.Type.HasValue)
            {
                AddShiftTopicViewModel shiftTopicModel = new AddShiftTopicViewModel
                {
                    ShiftId = model.ShiftId.Value,
                    Name = model.Reference,
                    Notes = model.Notes,
                    AssignedToId = _logicCore.ShiftCore.GetShift(model.ShiftId.Value).ShiftRecipientId,
                    RotationTopicGroupId = model.Type.Value,
                    CreatorId = model.CreatorId
                };

                _logicCore.RotationTopicCore.AddShiftTopic(shiftTopicModel);
            }
        }

        public ShiftHSEAddInlineTopicViewModel PopulateShiftHSETopicInlineModel(Guid shiftId, Guid moduleId)
        {
            ShiftHSEAddInlineTopicViewModel model = new ShiftHSEAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                Types = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(moduleId)
            };

            return model;
        }

        public ShiftProjectAddInlineTopicViewModel PopulateShiftProjectTopicInlineModel(Guid shiftId, Guid moduleId)
        {
            ShiftProjectAddInlineTopicViewModel model = new ShiftProjectAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                Projects = _services.RotationTopicGroupService.GetRotationModuleTopicGroupsList(moduleId)
            };

            return model;
        }

        public void AddShiftProjectTopic(ShiftProjectAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue && model.Project.HasValue)
            {
                AddShiftTopicViewModel shiftTopicModel = new AddShiftTopicViewModel
                {
                    ShiftId = model.ShiftId.Value,
                    RotationTopicGroupId = model.Project.Value,
                    AssignedToId = _logicCore.ShiftCore.GetShift(model.ShiftId.Value).ShiftRecipientId,
                    Name = model.Reference,
                    Notes = model.Notes,
                    CreatorId = model.CreatorId
                };

                _logicCore.RotationTopicCore.AddShiftTopic(shiftTopicModel);
            }
        }

        public ShiftTeamAddInlineTopicViewModel PopulateShiftTeamTopicInlineModel(Guid shiftId, Guid teamId)
        {
            //Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            ShiftTeamAddInlineTopicViewModel model = new ShiftTeamAddInlineTopicViewModel
            {
                ShiftId = shiftId,
                TeamMembers = _services.TeamService.GetTeamMembersListWidthOther(teamId)
            };

            return model;
        }

        public void AddShiftTeamTopic(ShiftTeamAddInlineTopicViewModel model)
        {

            if (model.ShiftId.HasValue)
            {

                var shiftModule = _logicCore.RotationModuleCore.GetShiftModule(TypeOfModule.Team,
                                                                                        model.ShiftId.Value,
                                                                                        TypeOfModuleSource.Draft);

                AddShiftTopicViewModel shiftTopicModel = new AddShiftTopicViewModel
                {
                    ShiftId = model.ShiftId.Value,
                    RotationTopicGroupId = shiftModule.RotationTopicGroups.FirstOrDefault(tg => tg.Enabled).Id,
                    AssignedToId = shiftModule.Shift.ShiftRecipientId,
                    Name = model.Reference,
                    Notes = model.Notes,
                    RelationId = model.TeamMember,
                    CreatorId = model.CreatorId
                };

                _logicCore.RotationTopicCore.AddShiftTopic(shiftTopicModel);
            }
        }

        /// <summary>
        /// Check whether the topic is Shift
        /// </summary>
        /// <param name="topicId"></param>
        /// <returns></returns>
        public bool IsShiftTopic(Guid topicId)
        {
            return !_logicCore.RotationTopicCore.GetTopic(topicId).RotationTopicGroup.RotationModule.RotationId.HasValue;
        }


        public TopicDetailsDialogViewModel GetModelForTopicDetailsModal(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);
            bool isShowPvA = true;
            var variance = string.Empty;
            var units = string.Empty;

            if (topic.UnitsSelectedType != null)
            {
                units = GetUnitsPvA().FirstOrDefault(u => u.Value == topic.UnitsSelectedType).Text;
            }

            if (topic.Actual != null && topic.Planned != null && topic.Actual != "" && topic.Planned != "")
            {
                variance = (float.Parse(topic.Actual) - float.Parse(topic.Planned)).ToString();

                if (topic.UnitsSelectedType != null)
                {
                    variance = variance + " " + units;
                }
            }
            else if (topic.Actual != null && topic.Actual != "" && topic.Planned == null || topic.Actual != null && topic.Actual != "" && topic.Planned == "")
            {
                variance = topic.Actual;

                if (topic.UnitsSelectedType != null)
                {
                    variance = topic.Actual + " " + units;
                }
            }

            else if (topic.Actual == null && topic.Planned != null && topic.Planned != "" || topic.Actual == "" && topic.Planned != null && topic.Planned != "")
            {
                variance = (float.Parse("0") - float.Parse(topic.Planned)).ToString();

                if (topic.UnitsSelectedType != null)
                {
                    variance = topic.Planned + " " + units;
                }
            }

            if (topic.Actual != null && topic.Actual != "")
            {
                if (topic.UnitsSelectedType != null)
                {
                    topic.Actual = topic.Actual + " " + units;
                }
                else
                {
                    topic.Actual = topic.Actual;
                }
            }
            else
            {
                topic.Actual = "-";
            }
          

            if (topic.Planned != null && topic.Planned != "")
            {
                if (topic.UnitsSelectedType != null)
                {
                    topic.Planned = topic.Planned + " " + units;
                }
                else
                {
                    topic.Planned = topic.Planned;
                }
            }
            else 
            {
                topic.Planned = "-";
            }

            

            if (topic.Planned == "-" && topic.Actual == "-" && variance == "")
            {
                isShowPvA = false;
            }


            var model = new TopicDetailsDialogViewModel
            {
                Id = topic.Id,
                TopicDetails = new TopicDetailsViewModel
                {
                    Section = topic.RotationTopicGroup.RotationModule.Type.GetEnumDescription(),
                    ItemName = topic.Name,
                    Notes = topic.Description,
                    ManagerComments = topic.ManagerComments,
                    Tags = string.IsNullOrEmpty(topic.SearchTags) ? null : topic.SearchTags.Split(','),
                    AttacmentsCounter = topic.Attachments.Count,
                    VoiceMessagesCounter = topic.VoiceMessages.Count,
                    TasksCounter = topic.RotationTasks.Count,
                    IsFeedbackRequired = topic.IsFeedbackRequired,
                    Actual = topic.Actual,
                    Planned = topic.Planned,
                    Variance = variance != null && variance != "" ? variance : "-",
                    IsShowPvA = isShowPvA
                }
            };

            return model;
        }


        public RotationTopicGroup SortByHaveFirstTopicGroupName(RotationTopicGroup rotationTopicGroups, ModuleSourceType moduleSourceType)
        {
            RotationTopic topic = null;

            if (moduleSourceType == ModuleSourceType.Draft)
            {
                topic = rotationTopicGroups.RotationTopics.Where(t =>
                {
                    if (t.TempateTopicId.HasValue)
                    {
                        return t.TempateTopic.Enabled;
                    }
                    else
                    {
                        return t.Enabled;
                    }
                }).Where(t => !t.DeletedUtc.HasValue).OrderBy(t => t.RotationTopicGroup.Name).ThenBy(t => t.Name).Where(t => !t.IsNullReport).ToList().FirstOrDefault();
            }

            if (moduleSourceType == ModuleSourceType.Received)
            {
                topic = rotationTopicGroups.RotationTopics.Where(t =>
                {
                    if (t.TempateTopicId.HasValue)
                    {
                        return t.TempateTopic.Enabled;
                    }
                    else
                    {
                        return t.Enabled;
                    }
                }).Where(t => !t.DeletedUtc.HasValue).OrderBy(t => t.RotationTopicGroup.Name).ThenBy(t => t.Name).ToList().FirstOrDefault();
            }
            if (topic != null)
            {
                topic.IsFirstTopicByTopicGroupName = true;
                _repositories.RotationTopicRepository.Update(topic);

                rotationTopicGroups.RotationTopics.Where(t => t.Id != topic.Id).ToList().ForEach(rotationTopic => 
                {
                    rotationTopic.IsFirstTopicByTopicGroupName = null;
                    _repositories.RotationTopicRepository.Update(rotationTopic);
                });

                _repositories.Save();
            }

            return rotationTopicGroups;
        }

        public List<RotationTopic> SortByHaveFirstTopicName(List<RotationTopic> rotationTopics)
        {
            var topic = rotationTopics.OrderBy(t => t.Name).ToList().FirstOrDefault();

            if (topic != null)
            {
                topic.IsFirstTopicByTopicGroupName = true;
                _repositories.RotationTopicRepository.Update(topic);
                //_repositories.Save();

                rotationTopics.Where(t => t.Id != topic.Id).ToList().ForEach(rotationTopic =>
                {
                    rotationTopic.IsFirstTopicByTopicGroupName = null;
                    _repositories.RotationTopicRepository.Update(rotationTopic);
                   // _repositories.Save();
                });

                _repositories.Save();
            }

            return rotationTopics;
        }

        public List<RotationTopic> GetTopicsByRelationId(Guid relationId, Guid rotationTopicGroupId)
        {
            return _repositories.RotationTopicRepository.Find(tg => tg.RelationId.HasValue && tg.RelationId.Value == relationId && tg.RotationTopicGroupId == rotationTopicGroupId).ToList();
        }

        public bool HaveFirstTopicByTopicGroupName(RotationTopicGroup rotationTopics)
        {
            var topics = rotationTopics.RotationTopics.FirstOrDefault(t => t.IsFirstTopicByTopicGroupName.HasValue && t.IsFirstTopicByTopicGroupName.Value);

            if (topics != null)
            {
                return true;
            }

            return false;
        }

        public RotationTopicGroup GetFirstTopicByTopicGroupName(RotationTopicGroup rotationTopics)
        {
            var result = _repositories.RotationTopicGroupRepository.Find(t => t.RotationTopics.FirstOrDefault().IsFirstTopicByTopicGroupName.HasValue
                                                                                                                && t.RotationTopics.FirstOrDefault().IsFirstTopicByTopicGroupName.Value).FirstOrDefault(t => t.Id == rotationTopics.Id);
            return result;
        }

        public List<RotationTopic> UpdateTopicGroupsNameFromRelation(IEnumerable<RotationTopic> rotationTopics)
        {
            List<RotationTopic> result = new List<RotationTopic>();

            foreach (var rotationTopic in rotationTopics)
            {
                var topic = _logicCore.RotationTopicGroupCore.UpdateTopicGroupsNameFromRelationTeams(rotationTopic.Id, rotationTopic.RelationId.Value, _logicCore.UserProfileCore.GetUserProfile(rotationTopic.RelationId.Value).FullName, true);
                result.Add(topic);
            }
            return result;
        }

        public int CommentCounterTopic(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            return topic.ManagerComment.Count;
        }

        public int AttachCounterTopic(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            return topic.Attachments.Count + topic.AttachmentsLink.Count;
        }
    }
}
