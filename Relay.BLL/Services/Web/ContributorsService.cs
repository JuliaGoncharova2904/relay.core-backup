﻿using System;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System.Linq;
using System.Collections.Generic;
using MomentumPlus.Core.Models;
using MvcPaging;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ContributorsService : IContributorsService
    {
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public ContributorsService(IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
            _services = services;
        }

        /// <summary>
        /// Get list of Contributed Users for Contributed Users section
        /// </summary>
        /// <param name="userId">Guid userId</param>
        /// <param name="page">int page</param>
        /// <param name="pageSize">int pageSize</param>
        /// <returns>Model ContributedUsersViewModel</returns>
        public ContributedUsersViewModel GetContributedUsers(Guid userId, int page, int pageSize)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            IQueryable<UserProfile> allContributerUsers = _logicCore.ContributorsCore.GetAllContributerUsers(user.Id);

            IEnumerable<UserProfile> pagedContributers = allContributerUsers?
                                                                .OrderBy(u => !u.LockDate.HasValue)
                                                                .ThenBy(u => u.FirstName)
                                                                .ThenBy(u => u.LastName)
                                                                .Skip(page * pageSize)
                                                                .Take(pageSize)
                                                                .ToList() ?? new List<UserProfile>();

            allContributerUsers = allContributerUsers.Where(u => !u.DeletedUtc.HasValue);

            ContributedUsersViewModel model = new ContributedUsersViewModel
            {
                OwnerName = user.FullName,
                ContributedUsers = this.MapToViewModel(pagedContributers).ToPagedList(page, pageSize, allContributerUsers.Count())
            };

            return model;
        }

        /// <summary>
        /// Mapping List[UserProfile] to List[ContributedUserViewModel]
        /// </summary>
        /// <param name="userProfiles"></param>
        /// <returns>List[ContributedUserViewModel]</returns>
        private IEnumerable<ContributedUserViewModel> MapToViewModel(IEnumerable<UserProfile> userProfiles)
        {
            List<ContributedUserViewModel> viewModels = new List<ContributedUserViewModel>();

            foreach (UserProfile user in userProfiles)
            {
                UserProfile owner = user;

                viewModels.Add(new ContributedUserViewModel
                {
                    OwnerId = owner.Id,
                    OwnerFullName = owner.FullName,
                    OwnerEmail = owner.Email,
                    BackToBackId = owner.CurrentRotationId.HasValue ? owner.CurrentRotation.DefaultBackToBackId : Guid.Empty,
                    BackToBackName = owner.CurrentRotationId.HasValue ? owner.CurrentRotation.DefaultBackToBack.FullName : "",
                    StateMessage = owner.CurrentRotationId.HasValue ? this.RotationStateInfo(owner.CurrentRotation, owner.Id) : "Rotation not confirmed",
                    IsActive = owner.CurrentRotationId.HasValue ? owner.CurrentRotation.State == RotationState.Confirmed : false                   
                });
            }

            return viewModels;
        }

        /// <summary>
        /// Get rotation state info.
        /// </summary>
        /// <param name="rotation">Rotation entity</param>
        /// <returns>String StateMessage</returns>
        private string RotationStateInfo(Rotation rotation, Guid userId)
        {
            string res = string.Empty;

            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user.CurrentRotationId.HasValue)
            {
                var currentRotation = user.CurrentRotation;

                if (_services.RotationService.IsShiftRotation(currentRotation.Id))
                {
                    var shift = currentRotation.RotationShifts.Any()
                                           ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation) != null
                                               ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation)
                                               : currentRotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Finished)
                                           : null;

                    if (shift != null)
                    {
                        switch (shift.State)
                        {
                            case ShiftState.Created:
                            case ShiftState.Confirmed:
                                res = "Until " + shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDateMonthFullYearAndTime();
                                break;
                            case ShiftState.Break:
                            case ShiftState.Finished:
                                res = "Off-site";
                                break;
                            default:
                                res = "Off-site";
                                break;
                        }
                    }
                    else
                    {
                        res = "Off-site";
                    }

                }
                else
                {
                    switch (currentRotation.State)
                    {
                        case RotationState.Confirmed:
                            res = "Until " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).ToString("dd/MM");
                            break;
                        case RotationState.SwingEnded:
                            res = "Back " + rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff).ToString("dd/MM");
                            break;
                        case RotationState.Expired:
                        case RotationState.Created:
                        default:
                            res = "Rotation not confirmed";
                            break;
                    }
                }
            }
            return res;
        }

        /// <summary>
        /// Does the in UserProfile have a any contributed users
        /// </summary>
        /// <param name="userId">Guid userId</param>
        /// <returns>Bool IsHaveContributedUser</returns>
        public bool IsHaveContributedUser(Guid userId)
        {
            return _logicCore.UserProfileCore.GetUserProfile(userId).ContributedUsers.Any();
        }
    }
}
