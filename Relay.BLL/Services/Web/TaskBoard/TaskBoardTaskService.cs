﻿using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Exceptions;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Extensions;
using MomentumPlus.Relay.Interfaces.Auth;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TaskBoardTaskService : BaseService, ITaskBoardTaskService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;

        public TaskBoardTaskService(LogicCoreUnitOfWork logicCore, IAuthService authService) : base(authService)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._authService = authService;
        }
        public IEnumerable<TaskBoardTaskForHandover> GetTaskBoardTasksForShift(Guid shiftId, ModuleSourceType sourceType, Guid? receivedShiftId = null)
        {
            return _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForShift(shiftId, sourceType, receivedShiftId);
        }


        public IEnumerable<TaskBoardTaskForHandover> GetTaskBoardTasksForRotation(Guid rotationId, ModuleSourceType sourceType, Guid? receivedRotationId = null)
        {
            return _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotationId, sourceType, receivedRotationId);
        }


        public TaskBoardTaskForHandover GetTask(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            UserProfile taskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(task);

            var model = new TaskBoardTaskForHandover
            {
                Name = task.Name,
                Status = task.Status.ToString(),
                Priority = (TaskPriority)task.Priority,
                Notes = task.Description,
                Deadline = task.Deadline.FormatWithDayAndMonth(),
                AssignedToId = task.AssignedToId,
                HasAttachments = task.Attachments != null && task.Attachments.Any() || task.AttachmentsLink != null && task.AttachmentsLink.Any(),
                CompleteStatus = task.IsComplete ? "Complete" : "Incomplete",
                HasVoiceMessages = task.VoiceMessages != null && task.VoiceMessages.Any(),
                IsComplete = task.IsComplete,
                Id = task.Id,
                RelationId = task.RelationId,
                OwnerId = taskOwner.Id,
                IsFeedbackRequired = task.IsFeedbackRequired,
                FromTeammateId = taskOwner?.Id,
                FromTeammateFullName = taskOwner?.FullName,
                HasComments = task.ManagerComments != null && task.ManagerComments.Any(),
                TeammateId = task.AssignedToId
            };

            return model;

        }
    }
}
