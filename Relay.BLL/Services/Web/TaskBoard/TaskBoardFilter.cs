﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    internal static class TaskBoardFilter
    {
        /// <summary>
        /// Apply filter to tasks collection.
        /// </summary>
        /// <param name="tasks">Tasks collection</param>
        /// <param name="filter">Filter</param>
        /// <returns></returns>
        public static IQueryable<RotationTask> ApplyFilter(IQueryable<RotationTask> tasks, TaskBoardFilterOptions filter)
        {
            switch(filter)
            {
                case TaskBoardFilterOptions.AllTasks:
                    break;
                case TaskBoardFilterOptions.ActiveTasks:
                    tasks = tasks.Where(t => !t.IsInArchive);
                    break;
                case TaskBoardFilterOptions.ArchivedTasks:
                    tasks = tasks.Where(t => t.IsInArchive);
                    break;
                case TaskBoardFilterOptions.CompletedTasks:
                    tasks = tasks.Where(t => t.TaskBoardTaskType == TaskBoardTaskType.Draft && t.SuccessorTask !=null && t.SuccessorTask.IsComplete
                                            || t.TaskBoardTaskType == TaskBoardTaskType.Received && t.IsComplete
                                            || t.TaskBoardTaskType == TaskBoardTaskType.MyTask && t.IsComplete
                                            || t.TaskBoardTaskType == TaskBoardTaskType.NotSet 
                                                && (t.IsComplete || t.SuccessorTask != null && t.SuccessorTask.IsComplete));
                    break;
                case TaskBoardFilterOptions.IncompletedTasks:
                    tasks = tasks.Where(t => t.TaskBoardTaskType == TaskBoardTaskType.Draft && (t.SuccessorTask == null || !t.SuccessorTask.IsComplete)
                                            || t.TaskBoardTaskType == TaskBoardTaskType.Received && !t.IsComplete
                                            || t.TaskBoardTaskType == TaskBoardTaskType.MyTask && !t.IsComplete
                                            || t.TaskBoardTaskType == TaskBoardTaskType.NotSet && !t.IsComplete 
                                                && (t.SuccessorTask == null || !t.SuccessorTask.IsComplete));
                    break;
                case TaskBoardFilterOptions.PendingTasks:
                    tasks = tasks.Where(t => t.TaskBoardTaskType == TaskBoardTaskType.Pending);
                    break;
                default:
                    throw new Exception("Unknown TaskBoardFilter option.");
            }

            return tasks;
        } 
    }
}
