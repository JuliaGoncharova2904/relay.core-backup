﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SharingTopicService : BaseService, ISharingTopicService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;

        public SharingTopicService(LogicCoreUnitOfWork logicCore, IAuthService authService) : base(authService)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._authService = authService;
        }

        /// <summary>
        /// Populate ShareTopicViewModel for Share topic dialog.
        /// </summary>
        /// <param name="topicId">Topic Id</param>
        /// <returns></returns>
        public ShareTopicViewModel PopulateShareTopicViewModel(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var topicOwner = topic.RotationTopicGroup.RotationModule.Rotation != null
                ? topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner
                : topic.RotationTopicGroup.RotationModule.Shift.Rotation.RotationOwner;

            IQueryable<UserProfile> users = _logicCore.UserProfileCore.GetRelayEmployees()
                .Where(ApplyUserFilter())
                .Where(u => /*u.CurrentRotationId.HasValue &&*/ u.Id != topicOwner.Id);

            var RotationReportSharingRelations = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelations(topicId).ToList().OrderBy(r => r.Recipient.FullName).ToList();

            var topicRelationsUserIds = RotationReportSharingRelations.Where(r => r.RecipientId.HasValue).Select(r => r.RecipientId.Value).ToList();

            RotationReportSharingRelations = RotationReportSharingRelations.Where(r => r.RecipientId.HasValue).GroupBy(t => t.RecipientId).Select(t => t.FirstOrDefault()).ToList();

            topicRelationsUserIds = topicRelationsUserIds.Distinct().ToList();

            var model = new ShareTopicViewModel
            {
                Recipients = _mapper.Map<IEnumerable<SelectListItem>>(users.Where(u => !topicRelationsUserIds.Contains(u.Id)).ToList().OrderBy(u => u.FullName)),
                TopicRelations = _mapper.Map<IEnumerable<TopicRelationViewModel>>(RotationReportSharingRelations)
            };

            return model;
        }

        /// <summary>
        /// Save sharing topic changes.
        /// </summary>
        /// <param name="model"></param>
        public void ChangeShareTopicRelations(ShareTopicViewModel model)
        {
            model.IsRecipientsFromPrevRotation = false;

            var topic = _logicCore.RotationTopicCore.GetTopic(model.TopicId);

            IEnumerable<string> userRoles = null;

            if (model.RecipientsIds != null)
            {
                foreach (var userId in model.RecipientsIds)
                {
                    userRoles = _authService.GetUserRoles(userId);
                }
            }

            _logicCore.RotationTopicSharingRelationCore.ChangeShareTopicRelations(topic, model.RecipientsIds, model.DeleteTopicRelationsIds, userRoles);
        }

        /// <summary>
        /// Remove sharing topic relation.
        /// </summary>
        /// <param name="topicRelationId">Relation Id</param>
        public void DeleteShareTopicRelation(Guid topicRelationId)
        {
            var shareTopicRelation = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelation(topicRelationId);

            _logicCore.RotationTopicSharingRelationCore.DeleteShareTopicRelation(shareTopicRelation);
        }

        /// <summary>
        /// Populate list model of topic recipients.
        /// </summary>
        /// <param name="topicId">Topic Id</param>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetShareTopicRecipients(Guid topicId)
        {
            var topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var topicOwner = topic.RotationTopicGroup.RotationModule.Rotation.RotationOwner;

            var topicOwnerTeamMembers = topicOwner.Team.Users.Where(u => u.Id != topicOwner.Id && u.CurrentRotationId.HasValue && !u.DeletedUtc.HasValue);

            var RotationReportSharingRelations = _logicCore.RotationTopicSharingRelationCore.GetRotationTopicSharingRelations(topicId);

            var topicRelationsUserIds = RotationReportSharingRelations.Select(r => r.RecipientId).Where(u => u.HasValue);

            return _mapper.Map<IEnumerable<SelectListItem>>(topicOwnerTeamMembers.Where(u => !topicRelationsUserIds.Contains(u.Id)));
        }

        public void AddShareToTopic(ShareTopicViewModel model, Guid topicId, Guid userId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            topic.IsRecipientsFromPrevRotation = false;

            _logicCore.RotationTopicCore.UpdateTopic(topic);

            if (topic.TempateTopicId.HasValue)
            {
                TemplateTopic templateTopic = _logicCore.RotationTopicCore.GetTemplateTopic(topic.TempateTopicId.Value);
                _logicCore.TemplateCore.UpdateTopic(templateTopic);
            }


            _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> Are sharing the topic.", userId);
        }

        public IEnumerable<SelectListItem> GetEmailsList()
        {
            var emails = _logicCore.RotationTopicSharingRelationCore.GetListEmailRecepients();

            return _mapper.Map<IEnumerable<SelectListItem>>(emails.ToList());
        }

        public void SetEmailsRecepient(ShareTopicViewModel model)
        {
            _logicCore.RotationTopicSharingRelationCore.SetEmailsRecepients(model.TopicId, model.CustomRecipientsEmailName, model.EmailsString);
        }

        //test method
        public ShareTopicViewModel SetStringEmailsRecepient(string email)
        {
            List<string> emails = new List<string>();

            emails.Add(email);

            ShareTopicViewModel model = new ShareTopicViewModel
            {
                CustomRecipientsEmailName = email,
                Emails = emails
            };

            return model;
        }

    }
}
