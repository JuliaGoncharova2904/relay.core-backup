﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationPatternService : BaseService, IRotationPatternService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;

        public RotationPatternService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services) : base(services.AuthService)
        {
            this._logicCore = logicCore;
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
        }

        public IEnumerable<RotationPatternViewModel> GetAllRotationPatterns()
        {
            IQueryable<RotationPattern> rotationPatterns = _repositories.RotationPatternRepository.GetAll().Where(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter()));

            return _mapper.Map<IEnumerable<RotationPatternViewModel>>(rotationPatterns.OrderBy(p => p.DayOn));
        }

        public IEnumerable<SelectListItem> GetRotationPatternsList()
        {
            IQueryable<RotationPattern> rotationPatterns = _repositories.RotationPatternRepository.GetAll().Where(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter()));

            return _mapper.Map<IEnumerable<SelectListItem>>(rotationPatterns.OrderBy(p => p.DayOn));
        }

        public List<SelectListItem> GetShiftPatternsList()
        {
            List<SelectListItem> shiftPattern = new List<SelectListItem>();

            shiftPattern.Add(new SelectListItem { Value = 1.ToString(), Text = "12 hours" });
            shiftPattern.Add(new SelectListItem { Value = 2.ToString(), Text = "8 hours" });

            return shiftPattern;
        }


        public bool RotationPatternExist(RotationPatternViewModel model)
        {
            return _repositories.RotationPatternRepository.Find(m => m.DayOn == model.DayOn &&
                                                                        m.DayOff == model.DayOff &&
                                                                        m.Id != model.Id
                                                                ).Any();
        }


        public bool CanRemoveRotationPattern(Guid rotationPatternId)
        {
            var rotationPatterns = _logicCore.UserProfileCore.GetUsersByRotationPattern(rotationPatternId);
         
            if (rotationPatterns.Any())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void RemoveRotationpattern(Guid rotationPatternId)
        {
            _repositories.RotationPatternRepository.Delete(rotationPatternId);
            _repositories.Save();
        }

        public void AddRotationpattern(RotationPatternViewModel model)
        {
            RotationPattern rotationPatern = _mapper.Map<RotationPattern>(model);
            _repositories.RotationPatternRepository.Add(rotationPatern);
            _repositories.Save();
        }

        public RotationPatternViewModel GetRotationPaternById(Guid Id)
        {
            RotationPattern rotationPatern = _repositories.RotationPatternRepository.Get(Id);
            if (rotationPatern != null)
            {
                return _mapper.Map<RotationPatternViewModel>(rotationPatern);
            }

            return null;
        }

        public void UpdateRotationPattern(RotationPatternViewModel model)
        {
            RotationPattern rotationPatern = _mapper.Map<RotationPattern>(model);
            _repositories.RotationPatternRepository.Update(rotationPatern);
            _repositories.Save();
        }
    }
}
