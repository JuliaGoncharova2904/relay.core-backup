﻿using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.BLL.Services.Web.GlobalReports;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SearchService : ISearchService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;
        private readonly IAuthService _authService;

        public SearchService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services, IAuthService _authService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
            this._authService = _authService;
        }

        protected List<string> _nameTopics;

        protected List<UserProfile> _users;
        public IEnumerable<UserProfile> Users
        {
            get
            {
                if (_users != null)
                    return _users.ToList().OrderBy(u => u.FullName);
                else
                    return new UserProfile[0];
            }
        }

        #region Search Sahared Topic

        public List<RotationTopic> GetSharedTopicsByText(Guid currentUserId, SearchViewModel model)
        {
            var userRoles = _services.AuthService.GetUserRoles(currentUserId);

            List<RotationTopic> topics = new List<RotationTopic>();

            var user = _logicCore.UserProfileCore.GetUserProfile(currentUserId);

            var teamUsers = _logicCore.TeamCore.GetAllTeams().FirstOrDefault().Users.Where(u => u.CurrentRotationId.HasValue).ToList();

            if (userRoles.Any(r => r == iHandoverRoles.Relay.User))
            {
                var topicsGroup = _logicCore.TopicGroupCore.FindSharedTopicGroupsByName(model.SearchText, user, false, userRoles, teamUsers);
                var topicsRef = _logicCore.TopicGroupCore.FindReferenceByNameInSharedTopicGroup(model.SearchText, user, false, userRoles, teamUsers);
                var topicsNotes = _logicCore.TopicGroupCore.FindNotesByNameInSharedTopicGroup(model.SearchText, user, false, userRoles, teamUsers);

                topics = topics.Union(topicsGroup).Union(topicsRef).Union(topicsNotes).ToList();
            }
            else if (userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin))
            {
                var topicsGroup = _logicCore.TopicGroupCore.FindSharedTopicGroupsByName(model.SearchText, user, false, userRoles, teamUsers);
                var topicsRef = _logicCore.TopicGroupCore.FindReferenceByNameInSharedTopicGroup(model.SearchText, user, false, userRoles, teamUsers);
                var topicsNotes = _logicCore.TopicGroupCore.FindNotesByNameInSharedTopicGroup(model.SearchText, user, false, userRoles, teamUsers);

                topics = topics.Union(topicsGroup).Union(topicsRef).Union(topicsNotes).ToList();
            }
            else if (userRoles.Any(r => r == iHandoverRoles.Relay.HeadLineManager || r == iHandoverRoles.Relay.LineManager || r == iHandoverRoles.Relay.SafetyManager || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                var topicsGroup = _logicCore.TopicGroupCore.FindSharedTopicGroupsByName(model.SearchText, user, true, userRoles, teamUsers);
                var topicsRef = _logicCore.TopicGroupCore.FindReferenceByNameInSharedTopicGroup(model.SearchText, user, true, userRoles, teamUsers);
                var topicsNotes = _logicCore.TopicGroupCore.FindNotesByNameInSharedTopicGroup(model.SearchText, user, true, userRoles, teamUsers);

                topics = topics.Union(topicsGroup).Union(topicsRef).Union(topicsNotes).ToList();
            }
            else
            {
                throw new Exception("User access denied!");
            }

            return topics;
        }

        public SearchResultsViewModel SearchSharedTopics(Guid currentUserId, SearchViewModel model)
        {
            var topics = this.GetSharedTopicsByText(currentUserId, model);

            var result = new List<SearchResultsItem>();

            model.PageNumber = model.PageNumber - 1 ?? 0;
            model.PageSize = model.PageSize ?? NumericConstants.Paginator.PaginatorPageSizeForTopicSearch;

            if (topics != null)
            {
                this._users = _logicCore.RotationCore.GetAllActiveRotations().Where(r => !r.RotationOwner.DeletedUtc.HasValue).Select(t => t.RotationOwner).ToList();
                this._nameTopics = topics.Select(t => t.RotationTopicGroup.Name).Distinct().ToList();

                if (model.FilterUserId.HasValue)
                {
                    topics = topics.Where(t => _logicCore.RotationTopicCore.GetTopicOwner(t).Id == model.FilterUserId.Value).ToList();
                }
                if (model.SelectedTopicName != null)
                {
                    topics = topics.Where(t => t.RotationTopicGroup.Name == model.SelectedTopicName).ToList();
                }

                topics.ToList().ForEach(topic =>
                {
                    var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);
                    var topicName = topic.RelationId.HasValue ? topic.RelationId != Guid.Empty ? _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName : topic.RotationTopicGroup.Name : topic.RotationTopicGroup.Name;
                    var isHaveTopicInList = result.FirstOrDefault(t => t.CreatorId == topicCreator.Id && t.Name == topicName && t.Reference == topic.Name);

                    if (isHaveTopicInList == null)
                    {
                        result.Add(new SearchResultsItem
                        {
                            Id = topic.Id,
                            Name = topicName,
                            CreatorId = topicCreator.Id,
                            CreatorName = topicCreator.FullName,
                            Notes = topic.Description,
                            Reference = topic.Name,
                            CreatedTime = topic.CreatedUtc != null ? topic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time(),
                            CreatedDate = topic.CreatedUtc != null ? topic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear(),
                            SearchTags = topic.SearchTags,
                            FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault() : null,
                            MoreTags = topic.SearchTags != null ? topic.SearchTags.Split(',').Count() > 1 ? true : false : false,
                            AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                            ManagerCommentsCounter = topic.ManagerComment.Count,
                            RotationId = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? topic.RotationTopicGroup.RotationModule.RotationId : topic.RotationTopicGroup.RotationModule.ShiftId,
                            RotationType = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? Models.RotationType.Swing : Models.RotationType.Shift
                        });
                    }
                });
            }

            model.UsersList = _mapper.Map<IEnumerable<DropDownListItemModel>>(_users);
            model.NameTopics = _nameTopics.Select(t => new DropDownListItemModel
            {
                Text = t,
                Value = t
            });

            SearchResultsViewModel searchResultViewModel = new SearchResultsViewModel
            {
                ResultItems = result.ToPagedList(model.PageNumber.Value, model.PageSize.Value, result.Count),
                Params = model
            };

            return searchResultViewModel;
        }

        #endregion

        #region Search Tags

        public List<RotationTopic> GetTagsBySearchText(Guid currentUserId, SearchViewModel model)
        {
            var userRoles = _services.AuthService.GetUserRoles(currentUserId);

            List<RotationTopic> topics = new List<RotationTopic>();

            if (userRoles.Any(r => r == iHandoverRoles.Relay.User))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.RotationOwnerId == currentUserId
                                                                            && rotation.State != RotationState.Created).AsNoTracking().Select(rotation => rotation.RotationOwnerId)
                                                                            .GroupBy(id => id)
                                                                            .Select(group => group.FirstOrDefault()).ToList();

                topics = _logicCore.TopicGroupCore.FindTopicsByTagName(model.SearchText, usersUnderSupervisionIds.ToArray()).ToList();
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                topics = _logicCore.TopicGroupCore.FindTopicsByTagName(model.SearchText).ToList();
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.LineManager))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.LineManagerId == currentUserId
                                                                           && rotation.State != RotationState.Created).AsNoTracking().Select(rotation => rotation.RotationOwnerId)
                                                                           .GroupBy(id => id)
                                                                           .Select(group => group.FirstOrDefault()).ToList();

                topics = _logicCore.TopicGroupCore.FindTopicsByTagName(model.SearchText, usersUnderSupervisionIds.ToArray()).ToList();
            }

            else
            {
                throw new Exception("User access denied!");
            }

            return topics;
        }

        public SearchResultsViewModel SearchTags(Guid currentUserId, SearchViewModel model)
        {
            var topics = this.GetTagsBySearchText(currentUserId, model);

            var result = new List<SearchResultsItem>();

            model.PageNumber = model.PageNumber - 1 ?? 0;
            model.PageSize = model.PageSize ?? NumericConstants.Paginator.PaginatorPageSizeForTopicSearch;

            if (topics != null)
            {
                this._users = _logicCore.RotationCore.GetAllActiveRotations().Where(r => !r.RotationOwner.DeletedUtc.HasValue).Select(t => t.RotationOwner).ToList();
                this._nameTopics = topics.Select(t => t.RotationTopicGroup.Name).Distinct().ToList();

                if (model.FilterUserId.HasValue)
                {
                    topics = topics.Where(t => _logicCore.RotationTopicCore.GetTopicOwner(t).Id == model.FilterUserId.Value).ToList();
                }
                if (model.SelectedTopicName != null)
                {
                    topics = topics.Where(t => t.RotationTopicGroup.Name == model.SelectedTopicName).ToList();
                }

                topics.ToList().ForEach(topic =>
                {
                    var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);
                    var topicName = topic.RelationId.HasValue ? topic.RelationId != Guid.Empty ? _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName : topic.RotationTopicGroup.Name : topic.RotationTopicGroup.Name;
                    var isHaveTopicInList = result.FirstOrDefault(t => t.CreatorId == topicCreator.Id && t.Name == topicName && t.Reference == topic.Name);

                    if (isHaveTopicInList == null)
                    {
                        result.Add(new SearchResultsItem
                        {
                            Id = topic.Id,
                            Name = topicName,
                            CreatorId = topicCreator.Id,
                            CreatorName = topicCreator.FullName,
                            Notes = topic.Description,
                            Reference = topic.Name,
                            CreatedTime = topic.CreatedUtc != null ? topic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time(),
                            CreatedDate = topic.CreatedUtc != null ? topic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear(),
                            SearchTags = topic.SearchTags,
                            FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault(t => t.Contains(model.SearchText)) : null,
                            MoreTags = topic.SearchTags != null ? topic.SearchTags.Split(',').Count() > 1 ? true : false : false,
                            AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                            ManagerCommentsCounter = topic.ManagerComment.Count,
                            RotationId = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? topic.RotationTopicGroup.RotationModule.RotationId : topic.RotationTopicGroup.RotationModule.ShiftId,
                            RotationType = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? Models.RotationType.Swing : Models.RotationType.Shift
                        });
                    }
                });
            }

            model.UsersList = _mapper.Map<IEnumerable<DropDownListItemModel>>(_users);
            model.NameTopics = _nameTopics.Select(t => new DropDownListItemModel
            {
                Text = t,
                Value = t
            });
            SearchResultsViewModel searchResultViewModel = new SearchResultsViewModel
            {
                ResultItems = result.ToPagedList(model.PageNumber.Value, model.PageSize.Value, result.Count),
                Params = model
            };

            return searchResultViewModel;
        }

        #endregion

        #region Search Tasks

        public List<RotationTask> GetTaskBySearchText(Guid currentUserId, SearchViewModel model)
        {
            var userRoles = _services.AuthService.GetUserRoles(currentUserId);

            List<RotationTask> tasks = new List<RotationTask>();

            var allTasks = _repositories.RotationTaskRepository.GetAll().AsNoTracking().ToList();
            var teamUsers = _logicCore.TeamCore.GetAllTeams().FirstOrDefault().Users.Where(u => u.CurrentRotationId.HasValue).ToList();

            if (userRoles.Any(r => r == iHandoverRoles.Relay.User))
            {
                tasks = _logicCore.TopicGroupCore.FindTasksByName(allTasks, model.SearchText, currentUserId).ToList();

                if (tasks.Count == 0)
                {
                    tasks = _logicCore.TopicGroupCore.FindTasksByDescription(allTasks, model.SearchText, currentUserId).ToList();
                }
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                tasks = _logicCore.TopicGroupCore.FindTasksByName(allTasks, model.SearchText);

                if (tasks.Count == 0)
                {
                    tasks = _logicCore.TopicGroupCore.FindTasksByDescription(allTasks, model.SearchText).ToList();
                }
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.LineManager || r == iHandoverRoles.Relay.HeadLineManager))
            {
                tasks = _logicCore.TopicGroupCore.FindTasksByName(allTasks, model.SearchText, currentUserId, true, userRoles, teamUsers);

                if (tasks.Count == 0)
                {
                    tasks = _logicCore.TopicGroupCore.FindTasksByDescription(allTasks, model.SearchText, currentUserId, true, userRoles, teamUsers).ToList();
                }
            }

            else
            {
                throw new Exception("User access denied!");
            }

            return tasks;
        }


        public SearchTasksResultsViewModel SearchTasks(Guid currentUserId, SearchViewModel model)
        {
            var tasks = this.GetTaskBySearchText(currentUserId, model);

            var result = new List<SearchResultTaskItem>();

            model.PageNumber = model.PageNumber == null || model.PageNumber == 0 ? 0 : model.PageNumber;
            model.PageSize = model.PageSize ?? NumericConstants.Paginator.PaginatorPageSizeForTopicSearch;

            if (tasks != null)
            {
                this._users = _logicCore.RotationCore.GetAllActiveRotations().Where(r => !r.RotationOwner.DeletedUtc.HasValue).Select(t => t.RotationOwner).ToList();

                if (model.FilterUserId.HasValue)
                {
                    tasks = tasks.Where(t => _logicCore.RotationTaskCore.GetOwnerForTask(t).Id == model.FilterUserId.Value).ToList();
                }

                tasks.ToList().ForEach(task =>
                {
                    var taskCreator = _logicCore.RotationTaskCore.GetOwnerForTask(task);
                    var isHaveTopicInList = result.FirstOrDefault(t => t.CreatorId == taskCreator.Id && t.Name == task.Name);

                    if (taskCreator != null && isHaveTopicInList == null)
                    {
                        result.Add(new SearchResultTaskItem
                        {
                            Id = task.Id,
                            AssignedTo = task.AssignedTo,
                            AssignedToId = task.AssignedToId,
                            CreateDate = task.CreatedUtc,
                            CreatorId = taskCreator.Id,
                            Description = task.Description,
                            CreatorName = taskCreator.FullName,
                            Name = task.Name,
                            CompleteStatus = task.TaskBoardTaskType == TaskBoardTaskType.Pending
                                ? "Pending"
                                : task.SuccessorTaskId != null
                                ? task.SuccessorTask.IsComplete
                                ? "Complete"
                                : "Incomplete"
                                : "Incomplete",
                            TaskCounter = task.Attachments.Count,
                        });
                    }
                });
            }

            model.UsersList = _mapper.Map<IEnumerable<DropDownListItemModel>>(_users);

            SearchTasksResultsViewModel searchResultViewModel = new SearchTasksResultsViewModel
            {
                ResultItems = result.ToPagedList(model.PageNumber.Value, model.PageSize.Value, result.Count),
                Params = model
            };

            return searchResultViewModel;
        }
        #endregion

        #region PDF report

        public TopicReportPdfViewModel PopulateSearchReportViewModel(Guid currentUserId, SearchViewModel model)
        {
            List<RotationTopic> topics = new List<RotationTopic>();
            List<RotationTask> tasks = new List<RotationTask>();

            if (model.TypeFilter == TypeFilter.Shared)
            {
                topics = this.GetSharedTopicsByText(currentUserId, model);
            }

            if (model.TypeFilter == TypeFilter.Topics || model.TypeFilter == 0)
            {
                topics = _services.TopicSearchService.GetTopicsBySearctText(currentUserId, model);
            }

            if (model.TypeFilter == TypeFilter.Tags)
            {
                topics = this.GetTagsBySearchText(currentUserId, model);
            }

            if (model.TypeFilter == TypeFilter.Tasks)
            {
                tasks = this.GetTaskBySearchText(currentUserId, model);
            }

            if (model.TypeFilter == TypeFilter.Metrics)
            {
                topics = _services.TopicSearchService.GetTopicMetricsBySearchText(currentUserId, model);
            }

            if (model.DateRangeType != 0 && topics != null)
            {
                DateTime now = DateTime.UtcNow;
                if (model.DateRangeType ==  TopicSearchDateRangeType.Last24hr)
                {
                    var last24h = DateTime.UtcNow.AddHours(-24);
                    topics = topics.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last24h && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.Last48hr)
                {
                    var last48h = DateTime.UtcNow.AddHours(-48);
                    topics = topics.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last48h && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.LastWeek)
                {
                    var last7days = DateTime.UtcNow.AddDays(-7);
                    topics = topics.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last7days && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.LastMonth)
                {
                    var lastMonths = DateTime.UtcNow.AddMonths(-1);
                    topics = topics.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > lastMonths && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.Last6Month)
                {
                    var last6Months = DateTime.UtcNow.AddMonths(-6);
                    topics = topics.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last6Months && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
            }

            if (model.DateRangeType != 0 && tasks != null)
            {
                DateTime now = DateTime.UtcNow;
                if (model.DateRangeType == TopicSearchDateRangeType.Last24hr)
                {
                    var last24h = DateTime.UtcNow.AddHours(-24);
                    tasks = tasks.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last24h && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.Last48hr)
                {
                    var last48h = DateTime.UtcNow.AddHours(-48);
                    tasks = tasks.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last48h && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.LastWeek)
                {
                    var last7days = DateTime.UtcNow.AddDays(-7);
                    tasks = tasks.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last7days && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.LastMonth)
                {
                    var lastMonths = DateTime.UtcNow.AddMonths(-1);
                    tasks = tasks.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > lastMonths && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
                else if (model.DateRangeType == TopicSearchDateRangeType.Last6Month)
                {
                    var last6Months = DateTime.UtcNow.AddMonths(-6);
                    tasks = tasks.Where(t => (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) > last6Months && (t.CreatedUtc.HasValue ? t.CreatedUtc.Value : t.ModifiedUtc.Value) <= now).ToList();
                }
            }

            TopicReportPdfViewModel viewModel = new TopicReportPdfViewModel
            {
                Topics = new List<TopicSearchResultItem>(),
                Tasks = new List<SearchResultTaskItem>(),
                ReportDate = DateTime.UtcNow.FormatDayMonthYear().Trim(),
                CompanyLogoImageId = _logicCore.AdministrationCore.GetCompanyLogoImageId(),
                FileLogo = _logicCore.AdministrationCore.GetAdminSettings().Logo,
                TopicsInSearch = model.SearchText !=  null ? model.SearchText : " - ",
                typeFilter = model.TypeFilter
            };

            if (topics != null)
            {
                topics.ToList().ForEach(topic =>
                {
                    var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);
                    var topicName = topic.RelationId.HasValue ? topic.RelationId != Guid.Empty ? _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName : topic.RotationTopicGroup.Name : topic.RotationTopicGroup.Name;
                    var isHaveTopicInList = viewModel.Topics.FirstOrDefault(t => t.CreatorId == topicCreator.Id && t.Name == topicName && t.Reference == topic.Name);

                    if (model.TypeFilter == TypeFilter.Shared)
                    {
                        if (isHaveTopicInList == null)
                        {
                            viewModel.Topics.Add(new TopicSearchResultItem
                            {
                                Id = topic.Id,
                                Name = topicName,
                                CreatorId = topicCreator.Id,
                                CreatorName = topicCreator.FullName,
                                Notes = string.IsNullOrWhiteSpace(topic.Description) ? string.Empty : topic.Description.Trim(),
                                Reference = string.IsNullOrWhiteSpace(topic.Name) ? string.Empty : topic.Name,
                                CreatedTime = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatTo24Time()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time() : topic.CreatedUtc.FormatTo24Time().Trim(),
                                CreatedDate = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatDayMonthYear()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear() : topic.CreatedUtc.FormatDayMonthYear().Trim(),
                                SearchTags = topic.SearchTags,
                                FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault() : null,
                                MoreTags = topic.SearchTags != null ? topic.SearchTags.Split(',').Count() > 1 ? true : false : false,
                                AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                                ManagerCommentsCounter = topic.ManagerComment.Count
                            });
                        }
                    }

                    if (model.TypeFilter == TypeFilter.Metrics)
                    {
                        topic = _services.TopicSearchService.SetTopicPVA(topic);

                        if (isHaveTopicInList == null)
                        {
                            viewModel.Topics.Add(new TopicSearchResultItem
                            {
                                Id = topic.Id,
                                Name = topicName,
                                CreatorId = topicCreator.Id,
                                CreatorName = topicCreator.FullName,
                                Notes = string.IsNullOrWhiteSpace(topic.Description) ? string.Empty : topic.Description.Trim(),
                                Reference = string.IsNullOrWhiteSpace(topic.Name) ? string.Empty : topic.Name,
                                CreatedTime = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatTo24Time()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time() : topic.CreatedUtc.FormatTo24Time().Trim(),
                                CreatedDate = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatDayMonthYear()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear() : topic.CreatedUtc.FormatDayMonthYear().Trim(),
                                SearchTags = topic.SearchTags,
                                FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault() : null,
                                MoreTags = topic.SearchTags != null ? topic.SearchTags.Split(',').Count() > 1 ? true : false : false,
                                AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                                ManagerCommentsCounter = topic.ManagerComment.Count,
                                Actual = topic.Actual,
                                Planned = topic.Planned,
                                Units = topic.UnitsSelectedType,
                                Variance = topic.Variance
                            });
                        }
                    }

                    if (model.TypeFilter == TypeFilter.Tags)
                    {
                        if (isHaveTopicInList == null)
                        {
                            viewModel.Topics.Add(new TopicSearchResultItem
                            {
                                Id = topic.Id,
                                Name = topicName,
                                CreatorId = topicCreator.Id,
                                CreatorName = topicCreator.FullName,
                                Notes = string.IsNullOrWhiteSpace(topic.Description) ? string.Empty : topic.Description.Trim(),
                                Reference = string.IsNullOrWhiteSpace(topic.Name) ? string.Empty : topic.Name,
                                CreatedTime = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatTo24Time()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time() : topic.CreatedUtc.FormatTo24Time().Trim(),
                                CreatedDate = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatDayMonthYear()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear() : topic.CreatedUtc.FormatDayMonthYear().Trim(),
                                SearchTags = topic.SearchTags,
                                FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault(t => t.Contains(model.SearchText)) : null,
                                MoreTags = topic.SearchTags != null ? topic.SearchTags.Split(',').Count() > 1 ? true : false : false,
                                AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                                ManagerCommentsCounter = topic.ManagerComment.Count
                            });
                        }
                    }

                    if (model.TypeFilter == TypeFilter.Topics || model.TypeFilter == 0)
                    {
                        if (isHaveTopicInList == null)
                        {
                            viewModel.Topics.Add(new TopicSearchResultItem
                            {
                                Id = topic.Id,
                                Name = topicName,
                                CreatorId = topicCreator.Id,
                                CreatorName = topicCreator.FullName,
                                Notes = string.IsNullOrWhiteSpace(topic.Description) ? string.Empty : topic.Description.Trim(),
                                Reference = string.IsNullOrWhiteSpace(topic.Name) ? string.Empty : topic.Name,
                                CreatedTime = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatTo24Time()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time() : topic.CreatedUtc.FormatTo24Time().Trim(),
                                CreatedDate = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatDayMonthYear()) ? topic.TempateTopicId.HasValue ? topic.TempateTopic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear() : topic.CreatedUtc.FormatDayMonthYear().Trim(),
                                SearchTags = topic.SearchTags,
                                FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault() : null,
                                MoreTags = topic.SearchTags != null ? topic.SearchTags.Split(',').Count() > 1 ? true : false : false,
                                AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                                ManagerCommentsCounter = topic.ManagerComment.Count
                            });
                        }
                    }
                });
            }

            if (tasks != null)
            {
                tasks.OrderByDescending(t => t.CreatedUtc).ToList().ForEach(task =>
                {
                    var taskCreator = _logicCore.RotationTaskCore.GetOwnerForTask(task);
                    var isHaveTopicInList = viewModel.Tasks.FirstOrDefault(t => t.CreatorId == taskCreator.Id && t.Name == task.Name);

                    if (taskCreator != null && isHaveTopicInList == null)
                    {
                        viewModel.Tasks.Add(new SearchResultTaskItem
                        {
                            AssignedTo = task.AssignedTo,
                            AssignedToId = task.AssignedToId,
                            CreateDate = task.CreatedUtc,
                            CreatorId = taskCreator.Id,
                            Description = task.Description,
                            CreatorName = taskCreator.FullName,
                            Name = task.Name,
                            CompleteStatus = task.TaskBoardTaskType == TaskBoardTaskType.Pending
                                ? "Pending"
                                : task.SuccessorTaskId != null
                                ? task.SuccessorTask.IsComplete
                                ? "Complete"
                                : "Incomplete"
                                : "Incomplete",
                            TaskCounter = task.Attachments.Count
                        });
                    }
                });
            }

            return viewModel;
        }
        #endregion

        public bool IsActiveRotation(UserProfile topicCreator)
        {
            if (topicCreator.CurrentRotationId.HasValue)
            {
                if (topicCreator.CurrentRotation.RotationType == Core.Models.RotationType.Shift)
                {
                    var shift = _logicCore.ShiftCore.GetActiveRotationShift(topicCreator.CurrentRotation);

                    if (shift != null)
                    {
                        return true;
                    }
                }
                else
                {
                    if (topicCreator.CurrentRotation.State == RotationState.Confirmed)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public List<SelectListItem> GetUnitsPvA()
        {
            return new List<SelectListItem>
            {
                 new  SelectListItem { Value = 0.ToString(), Text = "sec" },
                 new SelectListItem { Value = 1.ToString(), Text = "min" },
                 new SelectListItem { Value = 2.ToString(), Text = "hr" },
                 new SelectListItem { Value = 3.ToString(), Text = "days" },
                 new SelectListItem { Value = 4.ToString(), Text = "GBP" },
                 new SelectListItem { Value = 5.ToString(), Text = "USD" },
                 new SelectListItem { Value = 6.ToString(), Text = "AUD" },
                 new SelectListItem { Value = 7.ToString(), Text = "gm" },
                 new SelectListItem { Value = 8.ToString(), Text = "Kg" },
                 new SelectListItem { Value = 9.ToString(), Text = "m" },
                 new SelectListItem { Value = 10.ToString(), Text = "BCM" },
                 new SelectListItem { Value = 11.ToString(), Text = "Tonnes" },
                 new SelectListItem { Value = 12.ToString(), Text = "t.oz" },
                 new SelectListItem { Value = 13.ToString(), Text = "t/h" },
                 new SelectListItem { Value = 14.ToString(), Text = "Loads/h" },
                 new SelectListItem { Value = 15.ToString(), Text = "metres/h" },
                 new SelectListItem { Value = 16.ToString(), Text = "Grade" },
                 new SelectListItem { Value = 17.ToString(), Text = "%" }
            };
        }
    }
}
