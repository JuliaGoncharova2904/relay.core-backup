﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TopicGroupService : ITopicGroupService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TopicGroupService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public IEnumerable<rTopicGroupViewModel> GetModuleTopicGroupsViewModels(Guid moduleId)
        {
            var model = _repositories.TemplateTopicGroupRepository.Find(tg => tg.ModuleId == moduleId).OrderBy(tg => tg.Name);

            return _mapper.Map<IEnumerable<rTopicGroupViewModel>>(model);
        }


        public rTopicGroupViewModel GetTopicGroupViewModel(Guid topicGroupId)
        {
            var model = _repositories.TemplateTopicGroupRepository.Get(topicGroupId);

            return _mapper.Map<rTopicGroupViewModel>(model);
        }


        public IEnumerable<rTopicGroupViewModel> PopulateTemplateModuleTopicGroupsViewModels(Guid baseModuleId,
            Guid templateModuleId)
        {
            var topicGroups = GetModuleTopicGroupsViewModels(baseModuleId);

            foreach (var topicGroup in topicGroups)
            {
                topicGroup.Enabled = CheckChildTopicGroupStatus(topicGroup.Id, templateModuleId);
            }

            return topicGroups;
        }


        public void UpdateTopicGroupStatus(Guid topicGroupId, bool status)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId);

            if (status == false)
            {
                _services.TopicService.UpdateTopicGroupTopicsStatus(topicGroup.Id, status);
            }

            topicGroup.Enabled = status;
            topicGroup.ParentTopicGroup.Enabled = status;
            _repositories.TemplateTopicGroupRepository.Update(topicGroup);

            _repositories.Save();

            //------- Adding new topic groups to rotations live -------
            if (status)
            {
                _services.RotationTopicGroupService.UpdateTemplateTopicGroupChildRotationTopicGroupsStatus(topicGroupId, status);
            }
            //---------------------------------------------------------
        }

        public void UpdateChildTopicGroupStatus(Guid baseTopicGroupId, Guid templateId, bool status, TypeOfModule typeOfModule = TypeOfModule.HSE)
        {
            var baseTopicGroup = _logicCore.TopicGroupCore.GetTopicGroup(baseTopicGroupId);

            if (baseTopicGroup.Module == null)
            {
                baseTopicGroup.Module = _repositories.TemplateModuleRepository.Find(t => t.Id == baseTopicGroup.ModuleId).FirstOrDefault(t => t.Type == typeOfModule);

                if (baseTopicGroup.Module == null)
                {
                    baseTopicGroup.Module = _repositories.TemplateModuleRepository.Get(baseTopicGroup.ModuleId);
                }

                baseTopicGroup.Module.Type = typeOfModule;
            }

            var templateModule = _logicCore.ModuleCore.GetTemplateModule(baseTopicGroup.Module.Type, templateId);

            if (templateModule == null)
            {
                templateModule = baseTopicGroup.Module;
            }

            var templateTopicGroup = _logicCore.TopicGroupCore.GetOrCreateTemplateTopicGroup(templateModule.Id,
                baseTopicGroup);


            if (status == true)
            {
                _services.ModuleService.ChangeModuleStatus(templateModule.Id, true);
            }

            UpdateTopicGroupStatus(templateTopicGroup.Id, status);
        }

        public TypeOfModule GetTypeOfModule(Guid topicGroupId)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId);

            return topicGroup.Module.Type;
        }

        public void UpdateChildTopicGroupStatusForRemove(Guid baseTopicGroupId, Guid templateId, bool status)
        {
            var baseTopicGroup = _logicCore.TopicGroupCore.GetTopicGroup(baseTopicGroupId);

            if (baseTopicGroup.Module == null)
            {
                if (_repositories.TemplateModuleRepository.Find(t => t.TemplateId == templateId).FirstOrDefault() == null)
                {
                    baseTopicGroup.Module.Type = _repositories.TemplateModuleRepository.Find(t => t.TemplateId == templateId).FirstOrDefault().Type;
                }
                else
                {
                    var modules = _repositories.TemplateModuleRepository.Find(t => t.TemplateId == templateId).FirstOrDefault();
                    var modulesById = _repositories.TemplateModuleRepository.Find(t => t.Id == baseTopicGroup.ModuleId).FirstOrDefault();

                    if (modules != null)
                    {
                        baseTopicGroup.Module.Type = modulesById.Type;
                    }

                    else
                    {
                        baseTopicGroup.Module.Type = modulesById.Type;
                    }
                }

               // baseTopicGroup.Module = _repositories.TemplateModuleRepository.Find(t => t.Id == baseTopicGroup.ModuleId).FirstOrDefault(t => t.Type == typeOfModule);

              //  baseTopicGroup.Module.Type = typeOfModule;
            }

            var templateModule = _logicCore.ModuleCore.GetTemplateModule(baseTopicGroup.Module.Type, templateId);

            var templateTopicGroup = _logicCore.TopicGroupCore.GetOrCreateTemplateTopicGroup(templateModule.Id,
                baseTopicGroup);


            if (status == true)
            {
                _services.ModuleService.ChangeModuleStatus(templateModule.Id, true);
            }

            UpdateTopicGroupStatus(templateTopicGroup.Id, status);
        }

        public bool CheckTopicGroupStatus(Guid topicGroupId)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId);

            if (topicGroup != null)
            {
                return topicGroup.Enabled;
            }

            return false;
        }

        public void UpdateModuleTopicGroupsStatus(Guid moduleId, bool status)
        {
            var module = _logicCore.ModuleCore.GetModule(moduleId);

            module.TopicGroups.ToList().ForEach(topicGroup => { UpdateTopicGroupStatus(topicGroup.Id, status); });

        }


        public bool CheckChildTopicGroupStatus(Guid parentTopicGroupId, Guid moduleId)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetChildTopicGroup(parentTopicGroupId, moduleId);

            if (topicGroup != null)
            {
                return topicGroup.Enabled;
            }

            return false;
        }


        public ProjectTopicGroupViewModel PopulateProjectTopicGroupViewModel(ProjectTopicGroupViewModel topicModel)
        {
            var projectTopicGroupModel = new ProjectTopicGroupViewModel
            {
                Projects = _services.ProjectService.GetProjectList(),
                Name =
                    topicModel.RelationId != null
                        ? _services.ProjectService.GetProjectName((Guid)topicModel.RelationId)
                        : _services.ProjectService.GetProjectName(new Guid(topicModel.SelectedProject)),
                ModuleId = topicModel.ModuleId,
                RelationId = topicModel.RelationId ?? new Guid(topicModel.SelectedProject),
                SelectedProject = topicModel.SelectedProject ?? topicModel.RelationId.ToString(),
                Description = topicModel.Description,
                Id = topicModel.Id
            };

            return projectTopicGroupModel;
        }

        public TeamTopicGroupViewModel PopulateTeamTopicGroupViewModel(TeamTopicGroupViewModel topicModel)
        {
            var teamTopicGroupModel = new TeamTopicGroupViewModel
            {
                Teams = _services.TeamService.GetTeamList(),
                Name =
                    topicModel.RelationId != null
                        ? _services.TeamService.GetTeamName((Guid)topicModel.RelationId)
                        : _services.TeamService.GetTeamName(new Guid(topicModel.SelectedTeam)),
                ModuleId = topicModel.ModuleId,
                RelationId = topicModel.RelationId ?? new Guid(topicModel.SelectedTeam),
                SelectedTeam = topicModel.SelectedTeam ?? topicModel.RelationId.ToString(),
                Description = topicModel.Description,
                Id = topicModel.Id
            };

            return teamTopicGroupModel;
        }

        public Guid AddTopicGroup(rTopicGroupViewModel model)
        {
            var topicGroup = _mapper.Map<TemplateTopicGroup>(model);
            topicGroup.Id = Guid.NewGuid();
            topicGroup.Enabled = false;
            //topicGroup.IsNullReport = model.IsNullReport;
            _repositories.TemplateTopicGroupRepository.Add(topicGroup);
            _repositories.Save();

            return topicGroup.Id;
        }

        public void UpdateTopicGroup(rTopicGroupViewModel model)
        {
            var topicGroup = _mapper.Map<TemplateTopicGroup>(model);

            _repositories.TemplateTopicGroupRepository.Update(topicGroup);
            _repositories.Save();

            _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup);
        }

        public bool TopicGroupExist(rTopicGroupViewModel model)
        {
            return
                _repositories.RotationTopicGroupRepository.Find(
                    tg => tg.Id != model.Id && tg.RotationModuleId == model.ModuleId && tg.Name.Equals(model.Name)).Any();
        }

        public bool TopicGroupRelationExist(rTopicGroupViewModel model)
        {
            return
                _repositories.RotationTopicGroupRepository.Find(
                    tg => tg.Id != model.Id && tg.RotationModuleId == model.ModuleId && tg.RelationId == model.RelationId).Any();
        }

        public void RemoveTopicGroup(Guid topicGroupId)
        {
            var topicGroup = _repositories.TemplateTopicGroupRepository.Get(topicGroupId);

            if (topicGroup.ChildTopicGroups != null)
            {
                var topicChildGroups = topicGroup.ChildTopicGroups;

                foreach (var topicChildGroup in topicChildGroups)
                {
                    if (topicChildGroup.Topics.Any())
                    {
                        var topicChilds = topicChildGroup.Topics;

                        foreach (var topicChild in topicChilds)
                        {
                            var tasksChilds = topicChild.Tasks;

                            if (tasksChilds.Any())
                            {
                                foreach (var taskChild in tasksChilds)
                                {
                                    taskChild.DeletedUtc = DateTime.UtcNow;
                                    taskChild.Enabled = false;
                                    _repositories.TemplateTaskRepository.Update(taskChild);
                                    _logicCore.TaskCore.UpdateChildTasks(taskChild);

                                    topicChild.DeletedUtc = DateTime.UtcNow;
                                    topicChild.Enabled = false;
                                    _repositories.TemplateTopicRepository.Update(topicChild);
                                    _logicCore.TopicCore.UpdateChildTopics(topicChild);

                                    topicChildGroup.DeletedUtc = DateTime.UtcNow;
                                    topicChildGroup.Enabled = false;
                                    _repositories.TemplateTopicGroupRepository.Update(topicChildGroup);
                                    _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicChildGroup);

                                    _repositories.Save();
                                }
                            }
                            else
                            {
                                topicChild.DeletedUtc = DateTime.UtcNow;
                                topicChild.Enabled = false;
                                _repositories.TemplateTopicRepository.Update(topicChild);
                                _logicCore.TopicCore.UpdateChildTopics(topicChild);

                                topicChildGroup.DeletedUtc = DateTime.UtcNow;
                                topicChildGroup.Enabled = false;
                                _repositories.TemplateTopicGroupRepository.Update(topicChildGroup);
                                _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicChildGroup);

                                _repositories.Save();
                            }
                        }
                    }
                    else
                    {
                        topicChildGroup.DeletedUtc = DateTime.UtcNow;
                        topicChildGroup.Enabled = false;
                        _repositories.TemplateTopicGroupRepository.Update(topicChildGroup);
                        _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicChildGroup);

                        _repositories.Save();
                    }
                }
            }
            if (topicGroup.Topics.Any())
            {
                var topics = topicGroup.Topics;

                foreach (var topic in topics)
                {
                    var tasks = topic.Tasks;

                    if (tasks.Any())
                    {
                        foreach (var task in tasks)
                        {
                            task.DeletedUtc = DateTime.UtcNow;
                            task.Enabled = false;
                            _repositories.TemplateTaskRepository.Update(task);
                            _logicCore.TaskCore.UpdateChildTasks(task);

                            topic.DeletedUtc = DateTime.UtcNow;
                            topic.Enabled = false;
                            _repositories.TemplateTopicRepository.Update(topic);
                            _logicCore.TopicCore.UpdateChildTopics(topic);

                            topicGroup.DeletedUtc = DateTime.UtcNow;
                            topicGroup.Enabled = false;
                            _repositories.TemplateTopicGroupRepository.Update(topicGroup);
                            _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup);

                            _repositories.Save();
                        }
                    }
                    else
                    {
                        topic.DeletedUtc = DateTime.UtcNow;
                        topic.Enabled = false;
                        _repositories.TemplateTopicRepository.Update(topic);
                        _logicCore.TopicCore.UpdateChildTopics(topic);

                        topicGroup.DeletedUtc = DateTime.UtcNow;
                        topicGroup.Enabled = false;
                        _repositories.TemplateTopicGroupRepository.Update(topicGroup);
                        _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup);

                        _repositories.Save();
                    }
                }
            }
            else
            {
                topicGroup.DeletedUtc = DateTime.UtcNow;
                topicGroup.Enabled = false;
                _repositories.TemplateTopicGroupRepository.Update(topicGroup);
                _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup);

                _repositories.Save();
            }
        }

        public IEnumerable<SelectListItem> GetTemplateTopicGroups(ModuleType moduleType, Guid templateId)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);
            var topics = _repositories.TemplateTopicGroupRepository.GetAll().Where(tg => tg.ModuleId == baseModule.Id).Where(t => !t.DeletedUtc.HasValue);
            return topics.OrderBy(t => t.Name).Select(m => new SelectListItem { Text = m.Name, Value = m.Id.ToString() });
        }

        public List<TemplateTopicGroup> GetTemplateTopicGroupsByTemplateId(ModuleType moduleType, Guid templateId)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);
            var topics = _repositories.TemplateTopicGroupRepository.GetAll().Where(tg => tg.ModuleId == baseModule.Id).Where(t => !t.DeletedUtc.HasValue);
            return topics.OrderBy(t => t.Name).ToList();
        }


        public IEnumerable<SelectListItem> GetTemplateReferences(ModuleType moduleType)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);
            var references = _repositories.TemplateTopicGroupRepository.GetAll().Where(tg => tg.ModuleId == baseModule.Id).Where(t => !t.DeletedUtc.HasValue).SelectMany(t => t.Topics).Where(t => !t.DeletedUtc.HasValue);
            return references.OrderBy(t => t.Name).Select(m => new SelectListItem { Text = m.Name, Value = m.Id.ToString() });
        }

        public IEnumerable<SelectListItem> GetTemplateReferencesByTopicGroupId(ModuleType moduleType, Guid TopicGroupId)
        {
            var baseModule = _logicCore.ModuleCore.GetBaseModuleFromType(moduleType);
            var references = _repositories.TemplateTopicGroupRepository.GetAll().Where(tg => tg.ModuleId == baseModule.Id && tg.Id == TopicGroupId).Where(t => !t.DeletedUtc.HasValue).SelectMany(t => t.Topics).Where(t => !t.DeletedUtc.HasValue);
            return references.OrderBy(t => t.Name).Select(m => new SelectListItem { Text = m.Name, Value = m.Id.ToString() });
        }

        public string GetNotesReferences(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                var referencesNotes = _repositories.TemplateTopicRepository.Get(topicId.Value).Description;
                return referencesNotes;
            }
            return null;
        }

        public bool? GetMetricsReferences(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                var referencesMetrics = _repositories.TemplateTopicRepository.Get(topicId.Value).IsExpandPlannedActualField;
                return referencesMetrics;
            }
            return null;
        }

        public SelectListItem GetUnitsReferences(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                var units = _repositories.TemplateTopicRepository.Get(topicId.Value).UnitsSelectedType;
                var referencesUnits = _services.RotationTopicService.GetUnitsPvA().FirstOrDefault(t => t.Value == units);
                return referencesUnits;
            }
            return null;
        }
    }
}