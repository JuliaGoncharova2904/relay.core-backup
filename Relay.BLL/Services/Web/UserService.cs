﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;
using MomentumPlus.Core.Interfaces;
using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using System.Web;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class UserService : BaseService, IUserService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;

        public UserService(LogicCoreUnitOfWork logicCore,
                            IServicesUnitOfWork services,
                            IAuthService authService) : base(authService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
            this._authService = authService;
        }

        public EmployeeViewModel PopulateEmployeeModel(Guid userId)
        {
            EmployeeViewModel model = new EmployeeViewModel();

            return PopulateEmployeeModel(model, userId);
        }

        public EmployeeViewModel PopulateEmployeeModel(EmployeeViewModel model, Guid userId)
        {
            model.Companies = _services.CompanyService.GetCompaniesList();
            model.UserTypes = _authService.GetAllRolesAccessedByUser(userId).Where(r => r != iHandoverRoles.Relay.iHandoverAdmin)
                                       .OrderBy(r => r)
                                       .Select(r => new SelectListItem { Text = r, Value = r });
            model.Positions = _services.PositionService.GetPositionsList();
            model.RotationPatterns = _services.RotationPatternService.GetRotationPatternsList();
            model.Teams = _services.TeamService.GetTeamList();

            return model;
        }

        public IEnumerable<SelectListItem> GetEmployeesList()
        {
            IEnumerable<UserProfile> users = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter());
                                                                           
            return _mapper.Map<IEnumerable<SelectListItem>>(users).OrderBy(u => u.Text);
        }

        public EmployeeViewModel GetEmployeeById(Guid Id, Guid userId)
        {
            UserProfile userProfile = _logicCore.UserProfileCore.GetUserProfile(Id);

            EmployeeViewModel employeeViewModel = new EmployeeViewModel
            {
                Id = userProfile.Id,
                Title = userProfile.Title,
                Name = userProfile.FirstName,
                Surname = userProfile.LastName,
                ImageId = userProfile.AvatarId,
                CompanyId = userProfile.CompanyId.ToString(),
                Email = userProfile.Email,
                UserTypeId = _authService.GetUserRoles(userProfile.Id).FirstOrDefault(),
                UserTypes = _authService.GetAllRolesAccessedByUser(userId)
                    //.Where(r => r != iHandoverRoles.Relay.iHandoverAdmin)
                    .OrderBy(r => r)
                    .Select(r => new SelectListItem { Text = r, Value = r }),
                PositionId = userProfile.PositionId.ToString(),
                RotationPatternId = userProfile.RotationPatternId.ToString(),
                TeamId = userProfile.TeamId.ToString(),
                Position = userProfile.PositionId.ToString(),
                EditRoleEnabled = !base.IsMultiTenant || _authService.GetUserRoles(userProfile.Id).Any(/*r => r != iHandoverRoles.Relay.Administrator*/),
                FullName = userProfile.FullName
            };

            return PopulateEmployeeModel(employeeViewModel, userId);
        }

        public IEnumerable<EmployeeViewModel> GetAllEmployees()
        {
            var users = _repositories.UserProfileRepository.GetAll().Where(ApplyUserFilter()).Where(UserActiveFilter());
            
            IEnumerable<EmployeeViewModel> employeeViewModels = users.Select(e => new EmployeeViewModel
            {
                Id = e.Id,
                Name = e.FirstName,
                Surname = e.LastName,
                PositionId = e.PositionId.ToString(),
                Position = e.Position.Name,
            });

            return employeeViewModels;
        }

        public void CreateEmployee(CreateEmployeeViewModel employeeViewModel, HttpPostedFileBase profileImage)
        {
            File image = null;
            Guid newUserProfileId = _authService.GetUserIdByLogin(employeeViewModel.Email);

            _authService.AddUserToRole(newUserProfileId, employeeViewModel.UserTypeId);

            if (profileImage != null)
            {
                image = _logicCore.MediaCore.AddImage("avatar", profileImage.FileName, profileImage.ContentType, profileImage.InputStream);
            }

            UserProfile user = new UserProfile
            {
                Id = newUserProfileId,
                Title = employeeViewModel.Title,
                Email = employeeViewModel.Email,
                UserName = employeeViewModel.Email,
                FirstName = employeeViewModel.Name,
                LastName = employeeViewModel.Surname,
                PositionId = Guid.Parse(employeeViewModel.PositionId),
                CompanyId = Guid.Parse(employeeViewModel.CompanyId),
                RotationPatternId = Guid.Parse(employeeViewModel.RotationPatternId),
                TeamId = Guid.Parse(employeeViewModel.TeamId),
                LastPasswordChangedDate = DateTime.Now,
                Avatar = image,
                AvatarId = image?.Id,
                TaskBoard = new TaskBoard
                {
                    Id = newUserProfileId,
                    Tasks = new HashSet<RotationTask>()
                }
            };

            _repositories.UserProfileRepository.Add(user);
            _repositories.Save();
        }

        public void UpdateEmployee(EmployeeViewModel employeeViewModel, HttpPostedFileBase profileImage)
        {
            UserProfile employeeEntity = _logicCore.UserProfileCore.GetUserProfile(employeeViewModel.Id.Value);

            employeeEntity.Title = employeeViewModel.Title;
            employeeEntity.Email = employeeViewModel.Email;
            employeeEntity.UserName = employeeViewModel.Email;
            employeeEntity.FirstName = employeeViewModel.Name;
            employeeEntity.LastName = employeeViewModel.Surname;
            employeeEntity.PositionId = Guid.Parse(employeeViewModel.PositionId);
            employeeEntity.TeamId = Guid.Parse(employeeViewModel.TeamId);

            if (profileImage != null)
            {
               // if (employeeEntity.AvatarId.HasValue)
                    employeeEntity.Avatar = _logicCore.MediaCore.AddImage("avatar", profileImage.FileName, profileImage.ContentType, profileImage.InputStream);
                //_logicCore.MediaCore.RemoveImage(employeeEntity.AvatarId.Value);

                //employeeEntity.Avatar = _logicCore.MediaCore.AddImage("avatar", profileImage.FileName, profileImage.ContentType, profileImage.InputStream);
                employeeEntity.AvatarId = employeeEntity.Avatar?.Id;
            }


            _repositories.UserProfileRepository.Update(employeeEntity);
            _repositories.Save();
        }

        public SecurityViewModel GetSecurityDetails(Guid personId)
        {
            var person = _logicCore.UserProfileCore.GetUserProfile(personId);

            return _mapper.Map<SecurityViewModel>(person);
        }

        public void UpdateSecurityDetails(Guid personId, SecurityViewModel model)
        {
            UserProfile person = _logicCore.UserProfileCore.GetUserProfile(personId);

            person.Email = model.RelayEmail;
            person.SecondaryEmail = model.SecondaryEmail;

            _repositories.UserProfileRepository.Update(person);
            _repositories.Save();
        }

        public IEnumerable<UserDetailsViewModel> GetUsersDetails()
        {
            IEnumerable<Guid> usersIds = _authService.GetUsersIdsByRoles(new[] { iHandoverRoles.Relay.iHandoverAdmin, iHandoverRoles.Relay.Administrator }, Guid.NewGuid());

            IQueryable<UserProfile> users = _logicCore.UserProfileCore.GetRelayEmployees();

            List<UserDetailsViewModel> usersDetails = users.OrderBy(u => u.FirstName)
                                                            .ThenBy(u => u.LastName)
                                                            .Select(u => new UserDetailsViewModel
                                                            {
                                                                Id = u.Id,
                                                                FullName = u.FirstName + " " + u.LastName,
                                                                Email = u.Email,
                                                                CanDelete = !usersIds.Contains(u.Id)
                                                            }).ToList();

            foreach (UserDetailsViewModel userDetails in usersDetails)
            {
                userDetails.Role = _authService.GetUserRoles(userDetails.Id).FirstOrDefault();
                userDetails.LastLogin = _authService.GetLastLoginDateForUser(userDetails.Id).FormatWithMonth();
                userDetails.IsLock = _authService.IsLockUser(userDetails.Id);
            }

            usersDetails = usersDetails.Where(u => u.Role != iHandoverRoles.Relay.iHandoverAdmin).ToList();

            return usersDetails;
        }

        public void RemoveUser(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            user.DeletedUtc = DateTime.Now;
            _repositories.UserProfileRepository.Update(user);
            _repositories.Save();
        }


        public PersonalDetailsViewModel GetPersonDetails(Guid personId)
        {
            UserProfile person = _repositories.UserProfileRepository.Get(personId);
            if (person != null)
            {
                return _mapper.Map<PersonalDetailsViewModel>(person).FillTitles();
            }

            return null;
        }


        public void InitializeSubscriptionManagerProfile(SubscriptionDetailsModel model)
        {
            Workplace workplace = _repositories.WorkplaceRepository.Add(new Workplace
            {
                Id = Guid.NewGuid(),
                //Name = model.ManagerCompany + " site",
                Name = model.ManagerOfficeLocation
            });

            _repositories.WorkplaceRepository.Add(workplace);

            Company company = _repositories.CompanyRepository.Add(new Company
            {
                Id = Guid.NewGuid(),
                IsDefault = true,
                Name = model.ManagerCompany,
            });

            _repositories.CompanyRepository.Add(company);

            Team team = _repositories.TeamRepository.Add(new Team
            {
                Id = Guid.NewGuid(),
                Name = "Admins"
            });

            _repositories.TeamRepository.Add(team);

            Template template = _repositories.TemplateRepository.Add(new Template
            {
                Id = Guid.NewGuid(),
                Name = "Admin template"
            });
            
            _repositories.TemplateRepository.Add(template);


            Position position = _repositories.PositionRepository.Add(new Position
            {
                Id = Guid.NewGuid(),
                Name = model.ManagerPosition,
                WorkplaceId = workplace.Id,
                TemplateId = template.Id
            });

            _repositories.PositionRepository.Add(position);


            _authService.AddUserToRole(model.ManagerId, iHandoverRoles.Relay.Administrator);

            RotationPattern rotationPattern = _repositories.RotationPatternRepository.Add(new RotationPattern
            {
                Id = Guid.NewGuid(),
                DayOn = 5,
                DayOff = 2
            });

            _repositories.RotationPatternRepository.Add(rotationPattern);

            UserProfile user = new UserProfile
            {
                Id = model.ManagerId,
                Email = model.ManagerEmail,
                UserName = model.ManagerEmail,
                FirstName = model.ManagerName,
                LastName = model.ManagerSurname,
                PositionId = position.Id,
                CompanyId = company.Id,
                RotationPatternId = rotationPattern.Id,
                TeamId = team.Id,
                LastPasswordChangedDate = DateTime.Now,
                TaskBoard = new TaskBoard
                {
                    Id = model.ManagerId,
                    Tasks = new HashSet<RotationTask>()
                }
            };

            _repositories.UserProfileRepository.Add(user);

            _repositories.Save();
        }

        public void UpdatePersonDetails(Guid personId, PersonalDetailsViewModel personDetails, HttpPostedFileBase foto)
        {
            UserProfile person = _logicCore.UserProfileCore.GetUserProfile(personId);

            _mapper.Map(personDetails, person);

            if (foto != null)
            {
               // if (person.AvatarId.HasValue)
                //{
                    //_logicCore.MediaCore.RemoveImage(person.AvatarId.Value, person.Avatar);
                    person.Avatar = _logicCore.MediaCore.AddImage("Avatar", foto.FileName, foto.ContentType, foto.InputStream, false);
                //}

                //person.Avatar = _logicCore.MediaCore.AddImage("Avatar", foto.FileName, foto.ContentType, foto.InputStream, false);
                person.AvatarId = person.Avatar?.Id;
            }

            _repositories.UserProfileRepository.Update(person);
            _repositories.Save();
        }

        public IEnumerable<SelectListItem> GetEmployeesForTeam(Guid teamId)
        {
            IQueryable<UserProfile> users = _repositories.UserProfileRepository.Find(m => m.TeamId == teamId && !m.DeletedUtc.HasValue);

            var userList = new List<SelectListItem>();

            foreach (var user in users)
            {
                string text = user.LockDate.HasValue ? " - [inactive]" : "";
                userList.Add(new SelectListItem
                {
                    Text = user.FirstName + " " + user.LastName + text,
                    Value = user.Id.ToString()
                });
            }

            return userList.OrderBy(em => em.Text);
        }

        public IEnumerable<EmployeeViewModel> GetEmployeesForPosition(Guid positionId)
        {
            return _mapper.Map<IEnumerable<EmployeeViewModel>>(_repositories.UserProfileRepository.Find(c => c.PositionId == positionId));
        }

        public List<TeamForWidgetViewModel> GetTeamForWidjet(Guid? rotationId, Guid? userId)
        {
            if (rotationId.HasValue && userId.HasValue)
            {
                var user = _logicCore.UserProfileCore.GetRelayEmployees().FirstOrDefault(u => u.Id == userId.Value);

                var teamMember = user.Team.Users.Where(u => u.Id != userId.Value && !u.DeletedUtc.HasValue);

                var team = _mapper.Map<List<TeamForWidgetViewModel>>(teamMember.ToList().OrderBy(u => u.FirstName));

                foreach (var userMember in team)
                {
                    userMember.RotationId = rotationId.Value;
                    userMember.TeamStatus = "Team member";
                    if (userMember.UserId == user.CurrentRotation.DefaultBackToBackId)
                    {
                        userMember.TeamStatus = "Back to back";
                    }
                    if (userMember.UserId == user.CurrentRotation.LineManagerId)
                    {
                        userMember.TeamStatus = "Line Manager";
                    }
                }
                return team;
            }
            return null;
        }

        public void UpdateLastLoginDate(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user != null)
            {
                user.LastLoginDate = DateTime.Now;

                _repositories.UserProfileRepository.Update(user);
                _repositories.Save();


                _authService.UpdateLastLoginDateForUser(user.Id);

            }
        }
        public void ReactivateUser(Guid userId, bool status)
        {
            _authService.ReactivateUser(userId, status);

            var user = _repositories.UserProfileRepository.Get(userId);
            user.LockDate = status ? DateTime.Now : new DateTime?();
            _repositories.UserProfileRepository.Update(user);
            _repositories.Save();
        }

        public InlineManualPeopleTrackingViewModel GetUserInfoForInlineManual(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);


            var model = new InlineManualPeopleTrackingViewModel();

            if (user != null)
            {
                model.Uid = user.Id;
                model.Email = user.Email;
                model.Username = user.UserName;
                model.Name = user.FirstName;
                model.Created = user.CreatedUtc.HasValue ? user.CreatedUtc.DateTimeToUnixTimestamp().ToString() : null;
                model.Updated = user.ModifiedUtc.HasValue ? user.ModifiedUtc.DateTimeToUnixTimestamp().ToString() : null;
                model.Role = _authService.GetUserRoles(userId).FirstOrDefault();
                model.Group = user.Team.Name;
                model.Plan = user.CurrentRotationId.HasValue ? user.CurrentRotation.RotationType.ToString() : null;
            }

            return model;
        }
    }
}
