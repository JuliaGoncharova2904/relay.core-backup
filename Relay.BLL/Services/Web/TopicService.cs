﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TopicService : ITopicService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TopicService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
        }

        public IEnumerable<rTopicViewModel> GetTopicGroupTopicsViewModels(Guid topicGroupId)
        {
            var topics = _repositories.TemplateTopicRepository.Find(t => t.TopicGroupId == topicGroupId).OrderBy(t => t.Name).ToList();

            return _mapper.Map<IEnumerable<rTopicViewModel>>(topics);
        }

        public rTopicViewModel GetTopicViewModel(Guid topicId)
        {
            var topic = _repositories.TemplateTopicRepository.Get(topicId);

            var model = _mapper.Map<rTopicViewModel>(topic);

            if (topic.IsExpandPlannedActualField.HasValue && topic.IsExpandPlannedActualField.Value)
            {
                model.Metrics = true;
            }

            if (model.Metrics == null)
            {
                model.Metrics = false;
            }

            if(topic.UnitsSelectedType != null)
            {
                model.Metrics = true;
                model.UnitsSelectedType = topic.UnitsSelectedType;
            }

            model.TemplatesAdded = new List<System.Web.Mvc.SelectListItem>();

            var templates = _repositories.TemplateRepository.GetAll().Where(t => t.Name != "iHandover Admin").ToList();

            foreach (var templateModule in templates.SelectMany(m => m.Modules).ToList())
            {
                var topics = templateModule.TopicGroups.SelectMany(t => t.Topics).Where(t => t.Name == topic.Name).ToList();

                if (topics != null && topics.Any())
                {
                    var template = templateModule.TemplateId.HasValue ? templateModule.Template : null;

                    if (template != null)
                    {
                        model.TemplatesAdded.Add(new System.Web.Mvc.SelectListItem
                        {
                            Text = template.Name, Value = template.Id.ToString()
                        });
                    }
                }
            }

            model.AllTemplates = new List<System.Web.Mvc.SelectListItem>();

            foreach (var template in templates)
            {
                if (model.TemplatesAdded.FirstOrDefault(t => t.Text == template.Name) == null)
                {
                    model.AllTemplates.Add(new System.Web.Mvc.SelectListItem
                    {
                        Text = template.Name,
                        Value = template.Id.ToString()
                    });
                }
            }

            return model;
        }

        public Guid AddTopic(rTopicViewModel model)
        {
            if (model.Metrics.HasValue)
            {
                model.Metrics = model.Metrics.Value;
            }
            else
            {
                model.Metrics = false;
            }

            var topic = _mapper.Map<TemplateTopic>(model);
            topic.Id = Guid.NewGuid();
            topic.Enabled = false;

            if (model.Metrics.HasValue && model.UnitsSelectedType != null)
            {
                topic.UnitsSelectedType = model.UnitsSelectedType;
                topic.PlannedActualHas = true;
            }

            if (model.Metrics.HasValue && model.Metrics.Value)
            {
                topic.IsExpandPlannedActualField = true;
                topic.PlannedActualHas = model.Metrics.Value;
            }

            _repositories.TemplateTopicRepository.Add(topic);
            _repositories.Save();
            //_logicCore.TopicCore.UpdateChildTopics(topic);

            return topic.Id;
        }

        public void UpdateTopic(rTopicViewModel model)
        {
            if (model.Metrics.HasValue)
            {
                model.Metrics = model.Metrics.Value;
            }
            else
            {
                model.Metrics = false;
            }

             var topic = _mapper.Map<TemplateTopic>(model);

            if (model.Metrics.HasValue && model.UnitsSelectedType != null)
            {
                topic.UnitsSelectedType = model.UnitsSelectedType;
            }

            if (model.Metrics.HasValue && model.Metrics.Value)
            {
                topic.IsExpandPlannedActualField = true;
            }

            _repositories.TemplateTopicRepository.Update(topic);
            _repositories.Save();

            _logicCore.TopicCore.UpdateChildTopics(topic);
        }

        public void UpdateTopicFromTamplates(rTopicViewModel model)
        {
            if (model.Metrics.HasValue)
            {
                model.Metrics = model.Metrics.Value;
            }
            else
            {
                model.Metrics = false;
            }

            var topic = _repositories.TemplateTopicRepository.Get(model.Id.Value);
            var topicGroup = _repositories.TemplateTopicGroupRepository.Get(model.TopicGroupId.Value);

            if (topic.ParentTopicId.HasValue)
            {
                var parenTopic = _repositories.TemplateTopicRepository.Get(topic.ParentTopicId.Value);

                if (model.Metrics.HasValue && model.UnitsSelectedType != null)
                {
                    parenTopic.UnitsSelectedType = model.UnitsSelectedType;
                }

                if (model.Metrics.HasValue && model.Metrics.Value)
                {
                    parenTopic.IsExpandPlannedActualField = true;
                }

                if (parenTopic.Description != model.Description)
                {
                    parenTopic.Description = model.Description;
                }

                if (parenTopic.Name != model.Name)
                {
                    parenTopic.Name = model.Name;
                }

                _repositories.TemplateTopicRepository.Update(parenTopic);

                if (parenTopic.TopicGroup.Name != model.TopicGroupName)
                {
                    topicGroup.Name = model.TopicGroupName;

                    _repositories.TemplateTopicGroupRepository.Update(topicGroup);

                    if (topicGroup.ParentTopicGroupId.HasValue)
                    {
                        topicGroup.ParentTopicGroup.Name = model.TopicGroupName;
                        _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup.ParentTopicGroup);
                    }
                    else
                    {
                        _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup);
                    }
                }

                _logicCore.TopicCore.UpdateChildTopics(parenTopic);
            }
            else
            {
                if (model.Metrics.HasValue && model.UnitsSelectedType != null)
                {
                    topic.UnitsSelectedType = model.UnitsSelectedType;
                }

                if (model.Metrics.HasValue && model.Metrics.Value)
                {
                    topic.IsExpandPlannedActualField = true;
                }

                _repositories.TemplateTopicRepository.Update(topic);

                if (topic.TopicGroup.Name != model.TopicGroupName)
                {
                    topicGroup.Name = model.TopicGroupName;

                    _repositories.TemplateTopicGroupRepository.Update(topicGroup);

                    if (topicGroup.ParentTopicGroupId.HasValue)
                    {
                        topicGroup.ParentTopicGroup.Name = model.TopicGroupName;
                        _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup.ParentTopicGroup);
                    }
                    else
                    {
                        _logicCore.TopicGroupCore.UpdateChildTopicGroups(topicGroup);
                    }
                }
            }

            _repositories.Save();

            _logicCore.TopicCore.UpdateChildTopics(topic);
        }

        public bool TopicExist(rTopicViewModel model)
        {
            return _repositories.TemplateTopicRepository.Find(t => t.Id != model.Id && t.TopicGroupId == model.TopicGroupId && t.Name.Equals(model.Name)).Any();
        }


        public IEnumerable<rTopicViewModel> PopulateTemplateTopicGroupTopicsViewModels(Guid baseTopicGroupId, Guid templateModuleId)
        {
            var topics = GetTopicGroupTopicsViewModels(baseTopicGroupId);

            foreach (var topic in topics)
            {
                topic.Enabled = CheckChildTopicStatus(topic.Id.Value, templateModuleId);
            }

            return topics;
        }


        public bool CheckChildTopicStatus(Guid parentTopicId, Guid templateModuleId)
        {
            var topic = _logicCore.TopicCore.GetChildTopicFromModule(parentTopicId, templateModuleId);

            if (topic != null)
            {
                return topic.Enabled;
            }

            return false;
        }


        public void UpdateChildTopicStatus(Guid baseTopicId, Guid templateId, bool status, Guid? topicGroupId, TypeOfModule typeOfModule = TypeOfModule.HSE)
        {
            var baseTopic = _logicCore.TopicCore.GetTopic(baseTopicId);

            if (baseTopic.TopicGroup.Module == null)
            {
                baseTopic.TopicGroup.Module = _repositories.TemplateModuleRepository.Find(t => t.Id == baseTopic.TopicGroup.ModuleId).FirstOrDefault(t => t.Type == typeOfModule);
                baseTopic.TopicGroup.Module.Type = typeOfModule;
            }

            var templateModule = _logicCore.ModuleCore.GetTemplateModule(baseTopic.TopicGroup.Module.Type, templateId);
            TemplateTopicGroup templateTopicGroup = null;

            if (topicGroupId.HasValue)
            {
                var basetopicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId.Value);
                templateTopicGroup = _logicCore.TopicGroupCore.GetOrCreateTemplateTopicGroup(templateModule != null ? templateModule.Id : basetopicGroup.ModuleId, basetopicGroup);
            }
            else
            {
                templateTopicGroup = _logicCore.TopicGroupCore.GetOrCreateTemplateTopicGroup(templateModule.Id, baseTopic.TopicGroup);
            }
         
            var templateTopic = _logicCore.TopicCore.GetOrCreateTemplateTopicGroupTopic(templateTopicGroup.Id, baseTopic);

            if (status == true)
            {
                _services.ModuleService.ChangeModuleStatus(templateTopicGroup.ModuleId, true);
                _services.TopicGroupService.UpdateTopicGroupStatus(templateTopicGroup.Id, true);
            }

            UpdateTopicStatus(templateTopic.Id, status);
        }


        public void UpdateTopicStatus(Guid topicId, bool status)
        {
            var topic = _logicCore.TopicCore.GetTopic(topicId);

            if (status == false)
            {
                _services.TaskService.UpdateTopicTasksStatus(topic.Id, status);
            }

            topic.Enabled = status;
            
            _repositories.TemplateTopicRepository.Update(topic);
            _repositories.Save();

            ///update rotation topic

            //------ Adding new topic items to rotations live -------
            //if (status)
            //{
            //    _services.RotationTopicService.UpdateTemplateTopicChildTopicsStatus(topic.Id, status, false);
            //}
            //-------------------------------------------------------
        }


        public void UpdateTopicGroupTopicsStatus(Guid topicGroupId, bool status)
        {
            var topicGroup = _logicCore.TopicGroupCore.GetTopicGroup(topicGroupId);

            if (topicGroup.Topics != null)
            {
                topicGroup.Topics.ToList().ForEach(topic => { UpdateTopicStatus(topic.Id, status); });
            }

            //foreach (var topic in topicGroup.Topics)
            //{
            //    UpdateTopicStatus(topic.Id, status);
            //}
        }

        public void RemoveTopic(Guid topicId)
        {
            var topic = _repositories.TemplateTopicRepository.Get(topicId);

            if (topic.ChildTopics != null)
            {
                var topicChilds = topic.ChildTopics;

                foreach (var topicChild in topicChilds)
                {
                    var tasksChilds = topicChild.Tasks;

                    if (tasksChilds.Any())
                    {
                        foreach (var taskChild in tasksChilds)
                        {
                            taskChild.DeletedUtc = DateTime.UtcNow;
                            taskChild.Enabled = false;
                            _repositories.TemplateTaskRepository.Update(taskChild);
                            _logicCore.TaskCore.UpdateChildTasks(taskChild);

                            topicChild.DeletedUtc = DateTime.UtcNow;
                            topicChild.Enabled = false;
                            _repositories.TemplateTopicRepository.Update(topicChild);
                            _logicCore.TopicCore.UpdateChildTopics(topicChild);

                            _repositories.Save();
                        }
                    }
                    else
                    {
                        topicChild.DeletedUtc = DateTime.UtcNow;
                        topicChild.Enabled = false;
                        _repositories.TemplateTopicRepository.Update(topicChild);
                        _logicCore.TopicCore.UpdateChildTopics(topicChild);

                        _repositories.Save();
                    }
                }
            }
            var tasks = topic.Tasks;

            if (tasks.Any())
            {
                foreach (var task in tasks)
                {
                    task.DeletedUtc = DateTime.UtcNow;
                    task.Enabled = false;
                    _repositories.TemplateTaskRepository.Update(task);
                    _logicCore.TaskCore.UpdateChildTasks(task);

                    topic.DeletedUtc = DateTime.UtcNow;
                    topic.Enabled = false;
                    _repositories.TemplateTopicRepository.Update(topic);
                    _logicCore.TopicCore.UpdateChildTopics(topic);

                    _repositories.Save();
                }
            }
            else
            {
                topic.DeletedUtc = DateTime.UtcNow;
                topic.Enabled = false;
                _repositories.TemplateTopicRepository.Update(topic);
                _logicCore.TopicCore.UpdateChildTopics(topic);
                _repositories.Save();
            }
        }

        public void UpdateTopicMetrics(Guid topicId, bool metrics, string unitSelectedType)
        {
            var topic = _logicCore.TopicCore.GetTopic(topicId);

            if (topic.ChildTopics.Any())
            {
                foreach (var childTopic in topic.ChildTopics)
                {
                    childTopic.PlannedActualHas = metrics;
                    childTopic.IsExpandPlannedActualField = metrics;
                    childTopic.UnitsSelectedType = unitSelectedType;
                }
            }

            topic.IsExpandPlannedActualField = false;
            topic.PlannedActualHas = false;

            _repositories.TemplateTopicRepository.Update(topic);
            _repositories.Save();
        }


        public void RemoveTopicsCore(Guid topicId, Guid templateId, bool status, Guid? topicGroupId, TypeOfModule typeOfModule, Guid moduleId)
        {
            var baseTopic = _repositories.TemplateTopicRepository.Find(t => t.Id == topicId).FirstOrDefault();

            if (baseTopic.TopicGroup.Module == null)
            {
                baseTopic.TopicGroup.Module.Type = typeOfModule;
            }

            var templateModule = _logicCore.ModuleCore.GetTemplateModule(baseTopic.TopicGroup.Module.Type, templateId);

            var topicGroup = templateModule.Template.Modules.FirstOrDefault(t => t.Type == typeOfModule).TopicGroups.FirstOrDefault(t=>t.Id == topicGroupId);
            var topic = templateModule.Template.Modules.FirstOrDefault(t => t.Type == typeOfModule).TopicGroups.SelectMany(t => t.Topics).FirstOrDefault(t=>t.Id == topicId);

            topicGroup.Enabled = false;
            topic.Enabled = false;
            topicGroup.ParentTopicGroup.Enabled = false;
            topic.ParentTopic.Enabled = false;

            _repositories.TemplateTopicGroupRepository.Update(topicGroup);
            _repositories.TemplateTopicRepository.Update(topic);
            _repositories.Save();

        }


    }
}
