﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Configuration;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public abstract class BaseService
    {
        private readonly IAuthService _authService;

        public  bool IsMultiTenant => bool.Parse(WebConfigurationManager.AppSettings["isMultiTenant"]);

        protected BaseService(IAuthService authService)
        {
            this._authService = authService;
        }

        public Expression<Func<UserProfile, bool>> ApplyUserFilter()
        {
            if (IsMultiTenant)
            {
                IEnumerable<Guid> usersIds = _authService.GetUsersIdsByRole(iHandoverRoles.Relay.iHandoverAdmin, Guid.NewGuid());

                return user => !usersIds.Contains(user.Id);
            }

            return user => true;
        }

        public Expression<Func<UserProfile, bool>> UserActiveFilter()
        {
            if (IsMultiTenant)
            {
                return user => !user.LockDate.HasValue;
            }

            return user => true;
        }
    }
}
