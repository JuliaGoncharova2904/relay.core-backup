﻿using System;
using System.Collections.Generic;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using AutoMapper;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.BLL.Services.Web.TimeLine;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TimelineService : ITimelineService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TimelineService(LogicCoreUnitOfWork logicCore,
                                IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _logicCore = logicCore;
            _services = services;
        }

        public TimelineViewModel GetLastTwoYearsTimeline(Guid rotationId)
        {
            DateTime currentDate = DateTime.Now;
            int currentYear = currentDate.Year;
            UserProfile user = _logicCore.RotationCore.GetRotation(rotationId).RotationOwner;

            Rotation firstRotation = _logicCore.RotationCore.GetFirstUserRotation(user.Id, true);

            int firstYear;

            if (firstRotation.ConfirmDate.HasValue)
            {
                firstYear = firstRotation.ConfirmDate.Value.Year;
            }
            else if (firstRotation.CreatedUtc.HasValue)
            {
                firstYear = firstRotation.CreatedUtc.Value.Year;
            }
            else
            {
                firstYear = currentYear;
            }

            int countYear = currentYear - firstYear;

            List<TimelineViewModel> timelines = new List<TimelineViewModel>();

            if (countYear == 0 || countYear == 1)
            {
                countYear++;
            }

            if (currentDate.Month == 10 || currentDate.Month == 11 || currentDate.Month == 12)
            {
                countYear++;
            }

            for (var i = 0; i < countYear; ++i)
            {
                timelines.Add(new TimeLineSinglePanel(user, firstYear + i, _logicCore).CreateTimeline());
            }

            TimelineViewModel model = TimeLineBuilder.ConcatTimelines(timelines);

            return model;
        }

        public TimelineViewModel GetTimelineForYear(Guid rotationId, int year)
        {
            UserProfile user = _logicCore.RotationCore.GetRotation(rotationId).RotationOwner;
            TimeLineSinglePanel timeLinePanel = new TimeLineSinglePanel(user, year, _logicCore);
            TimelineViewModel model = timeLinePanel.CreateTimeline();

            return model;
        }

    }
}
