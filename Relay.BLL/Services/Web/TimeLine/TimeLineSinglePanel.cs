﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web.TimeLine
{
    internal class TimeLineSinglePanel
    {
        private int year;
        private UserProfile user;
        private DateTime currentDate;
        private List<Rotation> userRotations;
        private TimeLineBuilder timeLineBuilder;
        private LogicCoreUnitOfWork _logicCore;

        public TimeLineSinglePanel(UserProfile user, int year, LogicCoreUnitOfWork logicCore)
        {
            this.user = user;
            this.year = year;
            this.userRotations = new List<Rotation>();
            this.timeLineBuilder = new TimeLineBuilder(year);
            this._logicCore = logicCore;
            this.currentDate = DateTime.Today;
        }

        public TimelineViewModel CreateTimeline()
        {
            this.FillExistingUserRotations();
            this.FillFutureUserRotations();
            this.BuildTimelineFromRotations();

            return this.timeLineBuilder.TimeLine;
        }

        #region Build timeline

        private void BuildTimelineFromRotations()
        {
            bool firstRotation = true;
            DateTime lastEndDate = DateTime.Today;

            List<TimelineRotationInfo> rotationsInfo = this.ProcessingRotationsData(this.userRotations);

            foreach (TimelineRotationInfo rotationInfo in rotationsInfo)
            {
                if (firstRotation)
                {
                    firstRotation = false;

                    if (rotationInfo.HasPrevRotations)
                    {
                        lastEndDate = new DateTime(this.year, 1, 1);
                    }
                    else
                    {
                        lastEndDate = new DateTime(this.year, rotationInfo.StartDate.Month, 1);
                    }
                }

                this.timeLineBuilder.AddPeriod(lastEndDate, rotationInfo.StartDate, TimeLinePeriodType.DayOff);
                lastEndDate = rotationInfo.StartDate.AddDays(rotationInfo.DayOn);
                this.timeLineBuilder.AddPeriod(rotationInfo.StartDate, lastEndDate, TimeLinePeriodType.DayOn);
            }

            if (lastEndDate.Year == this.year)
            {
                this.timeLineBuilder.AddPeriod(lastEndDate, new DateTime(this.year + 1, 1, 1), TimeLinePeriodType.DayOff);
            }
        }

        private List<TimelineRotationInfo> ProcessingRotationsData(IEnumerable<Rotation> rotations)
        {
            TimelineRotationInfo prevRotationInfo = null;
            TimelineRotationInfo nextRotationInfo = null;
            List<TimelineRotationInfo> rotationsInfo = new List<TimelineRotationInfo>();

            foreach (Rotation rotation in rotations)
            {
                if (rotation.DayOn > 0)
                {
                    nextRotationInfo = new TimelineRotationInfo
                    {
                        StartDate = rotation.StartDate.Value.Date,
                        DayOn = rotation.DayOn,
                        HasPrevRotations = prevRotationInfo != null || this.HasPrevRotationInPrevYear(rotation)
                    };

                    if (prevRotationInfo != null)
                    {
                        DateTime prevEndDate = prevRotationInfo.StartDate.AddDays(prevRotationInfo.DayOn);
                        int daysDif = (nextRotationInfo.StartDate - prevEndDate).Days;

                        if (daysDif < 0)
                        {
                            prevRotationInfo.DayOn += daysDif;
                            if (prevRotationInfo.DayOn <= 0)
                            {
                                rotationsInfo.Remove(prevRotationInfo);
                            }
                        }
                    }

                    prevRotationInfo = nextRotationInfo;
                    rotationsInfo.Add(nextRotationInfo);
                }
            }

            return rotationsInfo;
        }

        private bool HasPrevRotationInPrevYear(Rotation rotation)
        {
            for (Rotation r = rotation.PrevRotation; r != null; r = r.PrevRotation)
            {
                int dayOn = r.DayOn > 0 ? r.DayOn : 0;
                int dayOff = r.DayOff > 0 ? r.DayOff : 0;
                DateTime endDate = r.StartDate.Value.AddDays(dayOn + dayOff);

                if ((dayOn != 0 || dayOff != 0) && endDate > r.CreatedUtc.Value && endDate.Year < rotation.StartDate.Value.Year)
                    return true;
            }

            return false;
        }

        #endregion

        #region Fill existing rotations

        private void FillExistingUserRotations()
        {
            var existingRotations = _logicCore.RotationCore.GetAllUserRotations(user.Id, true)
                                                        .ToList()
                                                        .Where(r => r.StartDate.Value.Year == year ||
                                                                    r.StartDate.Value.AddDays(r.DayOn + r.DayOff - 1).Year == year)
            /* exclude of manualy expared rotations */  .Where(r => r.StartDate.Value.AddDays(r.DayOn + r.DayOff - 1) > r.CreatedUtc.Value)
            /* exclude of empty rotations           */  .Where(r => r.DayOn > 0 || r.DayOff > 0)
                                                        .OrderBy(r => r.CreatedUtc.Value);

            this.userRotations.AddRange(existingRotations);
        }

        #endregion

        #region Fill future rotations

        private void FillFutureUserRotations()
        {
            Rotation lastRotation = _logicCore.RotationCore.GetLastConfirmedRotationForUser(user.Id);

            if (lastRotation == null || currentDate.Year > this.year)
                return;

            DateTime lastRotationEndDate = lastRotation.StartDate.Value.AddDays(lastRotation.DayOn + lastRotation.DayOff);

            this.userRotations.AddRange(this.GenerateRepeatedRotations(ref lastRotationEndDate,
                                                                            lastRotation.RepeatTimes,
                                                                            (lastRotation.DayOn >= 0) ? lastRotation.DayOn : 0,
                                                                            (lastRotation.DayOff >= 0) ? lastRotation.DayOff : 0));

            this.userRotations.AddRange(this.GenerateScheduledRotations(ref lastRotationEndDate,
                                                                            user.RotationPattern.DayOn,
                                                                            user.RotationPattern.DayOff));
        }


        private List<Rotation> GenerateRepeatedRotations(ref DateTime lastRotationEndDate, int repeatTimes, int dayOn, int dayOff)
        {
            List<Rotation> repeatedUserRotations = new List<Rotation>();
            int notWorkingDays = (this.currentDate - lastRotationEndDate).Days;

            if (notWorkingDays < 0)
                notWorkingDays = 0;

            if (repeatTimes > 0)
            {
                if (dayOn != 0 || dayOff != 0)
                {
                    int rotationLength = dayOn + dayOff;
                    int missingRotations = notWorkingDays / rotationLength;

                    if (repeatTimes <= missingRotations)
                    {
                        notWorkingDays -= rotationLength * repeatTimes;
                        lastRotationEndDate = lastRotationEndDate.AddDays(rotationLength * repeatTimes);
                    }
                    else
                    {
                        int leftRotations = repeatTimes - missingRotations;
                        notWorkingDays -= rotationLength * missingRotations;
                        lastRotationEndDate = lastRotationEndDate.AddDays(rotationLength * missingRotations);

                        for (int i = 0; i < leftRotations && lastRotationEndDate.Year == this.year; ++i)
                        {
                            repeatedUserRotations.Add(this.GenerateFutureRotation(ref lastRotationEndDate, notWorkingDays, dayOn, dayOff));
                            notWorkingDays = 0;
                        }
                    }
                }
            }

            return repeatedUserRotations;
        }

        private List<Rotation> GenerateScheduledRotations(ref DateTime lastRotationEndDate, int dayOn, int dayOff)
        {
            List<Rotation> scheduledUserRotations = new List<Rotation>();
            int notWorkingDays = this.year >= currentDate.Year
                                    ? (new DateTime(year, 12, 31) - lastRotationEndDate).Days
                                    : (this.currentDate - lastRotationEndDate).Days;

            if (notWorkingDays < 0)
                notWorkingDays = 0;

            if (dayOn != 0 || dayOff != 0)
            {
                int rotationLength = dayOn + dayOff;
                int cutDays = notWorkingDays % rotationLength;

                while (notWorkingDays > 0)
                {
                    int tempYear = lastRotationEndDate.Year;

                    Rotation rotation = this.GenerateFutureRotation(ref lastRotationEndDate, cutDays, dayOn, dayOff);

                    if (this.year == tempYear)
                    {
                        scheduledUserRotations.Add(rotation);
                    }
                    
                    notWorkingDays -= (rotation.DayOn + rotation.DayOff);
                    
                    cutDays = 0;
                }
            }

            return scheduledUserRotations;
        }

        private Rotation GenerateFutureRotation(ref DateTime endDate, int cutDays, int dayOn, int dayOff)
        {
            Rotation newRotation = new Rotation();

            newRotation.DayOn = dayOn;
            newRotation.DayOff = dayOff;

            if (cutDays > 0)
            {
                cutDays = cutDays - newRotation.DayOn;

                if (cutDays <= 0)
                {
                    newRotation.DayOn = -cutDays;
                }
                else
                {
                    newRotation.DayOn = 0;
                    newRotation.DayOff = newRotation.DayOff - cutDays;
                }

                newRotation.StartDate = endDate.AddDays(dayOn + dayOff - newRotation.DayOn - newRotation.DayOff);
            }
            else
            {
                newRotation.StartDate = endDate;
            }

            endDate = newRotation.StartDate.Value.AddDays(newRotation.DayOn + newRotation.DayOff);

            return newRotation;
        }

        #endregion

    }
}
