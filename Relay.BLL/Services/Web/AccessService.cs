﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Linq;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class AccessService : IAccessService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;

        public AccessService(LogicCoreUnitOfWork logicCore, IAuthService authService)
        {
            this._logicCore = logicCore;
            this._repositories = logicCore.Repositories;
            this._authService = authService;
        }

        public bool UserIsProjectManager(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            return user.ManagedProjects != null && user.ManagedProjects.Any();
        }

        public bool UserHasAccessToProject(Guid userId, Guid projectId, bool throwException = false)
        {
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (!user.HasAccessToProject(userRoles, projectId))
            {
                if(throwException)
                {
                    throw new Exception(string.Format("User with ID:{0} is not has access to project with ID:{1}", userId, projectId));
                }
                return false;
            }
            else
            {
                return true;
            }
        }


        public bool SafetyMessageArchiveV2IsEnable()
        {
            var result = _repositories.SafetyStatV2Repository.Count() > 0;

            return result;
        }

        public bool IsGlobalReportHistoryAndReceivedDisable(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);

            bool userIsAdminOrLineManager = userRoles.Contains(iHandoverRoles.Relay.Administrator)
                                            || userRoles.Contains(iHandoverRoles.Relay.iHandoverAdmin)
                                            || userRoles.Contains(iHandoverRoles.Relay.HeadLineManager)
                                            || userRoles.Contains(iHandoverRoles.Relay.LineManager)
                                            || userRoles.Contains(iHandoverRoles.Relay.ExecutiveUser);

            if (userRoles.Contains(iHandoverRoles.Relay.ExecutiveUser))
            {
                return userIsAdminOrLineManager;
            }

            return userIsAdminOrLineManager && !user.CurrentRotationId.HasValue;
        }

        public bool IsGlobalReportAvailable(Guid reportId, Guid userId, Models.RotationType reportType)
        {
            bool isAvailable = false;
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            bool userIsSimpleUser = userRoles.Contains(iHandoverRoles.Relay.User) || userRoles.Contains(iHandoverRoles.Relay.Contributor);

            switch (reportType)
            {
                case Models.RotationType.Swing:
                    Rotation rotation = _logicCore.RotationCore.GetRotation(reportId);
                    isAvailable = rotation.RotationOwnerId == userId || !userIsSimpleUser;
                    break;
                case Models.RotationType.Shift:
                    Shift shift = _logicCore.ShiftCore.GetShift(reportId);
                    isAvailable = shift.Rotation.RotationOwnerId == userId || !userIsSimpleUser;
                    break;
                default:
                    throw new Exception("Unsapported working pattern type.");
            }

            return isAvailable;
        }

        public bool IsReportSharedForUser(Guid reportId, Guid userId)
        {
            return _repositories.ReportSharingRelationRepository.GetAll().Any(rs => rs.ReportId == reportId && rs.RecipientId == userId);
        }
    }
}
