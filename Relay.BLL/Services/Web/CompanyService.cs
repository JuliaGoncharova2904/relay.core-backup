﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.BLL.Services.Web;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL
{
    public class CompanyService : BaseService, ICompanyService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public CompanyService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services) : base(services.AuthService)
        {
            this._logicCore = logicCore;
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
        }

        public CompanyViewModel GetCompany(Guid companyId)
        {
            var company = _repositories.CompanyRepository.Get(companyId);

            return _mapper.Map<CompanyViewModel>(company);
        }

        public IEnumerable<SelectListItem> GetCompaniesList()
        {
            var companies = _repositories.CompanyRepository.GetAll().Where(c => !c.Users.Any() || c.Users.AsQueryable().Any(ApplyUserFilter())).OrderBy(c => c.Name);

            return _mapper.Map<IEnumerable<SelectListItem>>(companies.OrderBy(c => c.Name));
        }


        public IEnumerable<CompanyViewModel> GetCompanies()
        {
            var companies = _repositories.CompanyRepository.GetAll().Where(c => !c.Users.Any() || c.Users.AsQueryable().Any(ApplyUserFilter())).OrderBy(c => c.Name);

            return _mapper.Map<IEnumerable<CompanyViewModel>>(companies);
        }

        public bool CompanyExist(CompanyViewModel model)
        {
            return _repositories.CompanyRepository.Find(c => c.Id != model.Id.Value && c.Name.Equals(model.Name)).Any();
        }


        public void AddCompany(CompanyViewModel model)
        {
            var company = _mapper.Map<Company>(model);

            _repositories.CompanyRepository.Add(company);
            _repositories.Save();
        }


        public void UpdateCompany(CompanyViewModel model)
        {
            var company = _mapper.Map<Company>(model);

            _repositories.CompanyRepository.Update(company);
            _repositories.Save();
        }

        public void RemoveCompany(Guid companyId)
        {
            _repositories.CompanyRepository.Delete(companyId);
            _repositories.Save();
        }


        public bool IsDefaultCompanyExist(CompanyViewModel model)
        {
            var companies = _repositories.CompanyRepository.Find(c => c.Id != model.Id.Value && c.IsDefault);
            return companies.Any();
        }

        public bool IsExistDefaultCompany()
        {
            return _repositories.CompanyRepository.Find(c => c.IsDefault).Any();
        }

        public Guid? GetDefaultCompanyId()
        {
            return _repositories.CompanyRepository
                                .GetAll()
                                .FirstOrDefault(c => c.IsDefault)
                                .Id;
        }

        public bool CanRemoveCompany(Guid companyId)
        {
            var companies = _logicCore.UserProfileCore.GetUsersByCompany(companyId);
            var companyDefault = this.GetDefaultCompanyId();

            if (companies.Any() || companyDefault == companyId)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}