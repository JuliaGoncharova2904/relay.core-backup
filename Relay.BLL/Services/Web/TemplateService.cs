﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TemplateService : BaseService, ITemplateService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;

        public TemplateService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services) : base(services.AuthService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
        }

        public TemplateViewModel GetTemplateViewModel(Guid templateId)
        {
            var model = _repositories.TemplateRepository.Get(templateId);
            return _mapper.Map<TemplateViewModel>(model);
        }

        public IEnumerable<SelectListItem> GetTemplateList()
        {
            var templatesRelay = _services.AuthService.GetAdminTemplates(null);

            List<Template> templates = _repositories.TemplateRepository.Find(p => !p.DeletedUtc.HasValue).Where(t => !t.Positions.Any() || t.Positions.Any(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter()))).ToList();

            if (templatesRelay != null && templatesRelay.Any())
            {
                foreach (var templateRelay in templatesRelay)
                {
                    var findTemplate = _repositories.TemplateRepository.Get(Guid.Parse(templateRelay.Value));

                    if (findTemplate != null && !findTemplate.AddRelayTemplateToTemplates.HasValue)
                    {
                        templates.Remove(findTemplate);
                    }
                }
            }

            return _mapper.Map<IEnumerable<SelectListItem>>(templates.OrderBy(t => t.Name));
        }

        public TemplatesViewModel PopulateTemplatesViewModel(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null)
        {
            var model = new TemplatesViewModel
            {
                Templates = GetTemplateList(),
                SelectedTemplate = selectedTemplate,
                Module = null,
                Template = null
            };

            if (!string.IsNullOrWhiteSpace(selectedTemplate) && selectedTemplate != "undefined")
            {
                model.Template = GetTemplateViewModel(new Guid(selectedTemplate));

                model.Template.Modules = _services.ModuleService.GetBaseModulesViewModels();

                foreach (var module in model.Template.Modules)
                {
                    module.Enabled = _logicCore.ModuleCore.CheckChildModuleStatus(module.Id,
                        new Guid(selectedTemplate));

                    module.EnabledTopicGroupsCounter = _logicCore.ModuleCore.EnabledTopicsGroupsForModuleCounter(module.Id, new Guid(selectedTemplate));
                    module.TopicGroups = _logicCore.ModuleCore.EnabledTopicsGroupsForModule(module.Id, new Guid(selectedTemplate)); 
                }
            }

            if (selectedModule.HasValue && !string.IsNullOrWhiteSpace(selectedTemplate))
            {
                model.Module = _services.ModuleService.PopulateTemplateModuleViewModel(new Guid(selectedTemplate), (ModuleType)selectedModule, topicGroup, topic, topicGroupsPageNo, topicsPageNo, tasksPageNo);
                model.SelectedModule = selectedModule;
            }

            return model;
        }


        public TemplatesViewModel PopulateAdminTemplatesViewModel(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null,
            bool? isViewGlobalTemplates = null)
        {
            var model = new TemplatesViewModel
            {
                Templates = GetAdminTemplateList(null),
                SelectedTemplate = selectedTemplate,
                Module = null,
                Template = null
            };

            if (!string.IsNullOrWhiteSpace(selectedTemplate) && selectedTemplate != "undefined")
            {
                model.Template = GetAdminTemplateViewModel(new Guid(selectedTemplate));
                model.Template.Modules = _services.ModuleService.GetBaseModulesViewModels();

                foreach (var module in model.Template.Modules)
                {
                    module.TopicGroups = GetTemplatesTopicGroupAndTopics(module.Type, new Guid(selectedTemplate));
                }
            }

            if (selectedModule.HasValue && !string.IsNullOrWhiteSpace(selectedTemplate))
            {
                model.SelectedModule = selectedModule;
            }

            return model;
        }

        public ICollection<rTopicGroupViewModel> GetTemplatesTopicGroupAndTopics(ModuleType type, Guid templateId)
        {
            List<AdminTemplateTopicGroups> topicGroups = new List<AdminTemplateTopicGroups>();

            if (type == ModuleType.HSE)
            {
                topicGroups = _services.AuthService.GetAdminTopicGroups(Authorization.Domain.Entities.TypeOfModule.HSE, templateId);
            }

            if (type == ModuleType.Core)
            {
                topicGroups = _services.AuthService.GetAdminTopicGroups(Authorization.Domain.Entities.TypeOfModule.Core, templateId);
            }

            if (topicGroups != null)
            {

                List<rTopicGroupViewModel> rTopicGroupViewModel = topicGroups.Select(topicItem => new rTopicGroupViewModel
                {
                    Description = topicItem.Description,
                    Enabled = topicItem.Enabled,
                    Id = topicItem.Id,
                    IsNullReport = topicItem.IsNullReport,
                    ModuleId = topicItem.ModuleId,
                    Name = topicItem.Name,
                    ParentTopicGroupId = topicItem.ParentTopicGroupId,
                    RelationId = topicItem.RelationId,
                    Topics = topicItem.Topics.Select(topic => new rTopicViewModel
                    {
                        Description = topic.Description,
                        Enabled = topic.Enabled,
                        Id = topic.Id,
                        Name = topic.Name,
                        ParentTopicId = topic.ParentTopicId,
                        TopicGroupId = topic.TopicGroupId,
                        UnitsSelectedType = topic.UnitsSelectedType,
                        PlannedActualHas = topic.IsExpandPlannedActualField
                    }).ToList()

                }).ToList();

                return rTopicGroupViewModel;
            }

            return new List<rTopicGroupViewModel>();
        }


        public void AddTemplate(TemplateViewModel model)
        {
            var template = _mapper.Map<Template>(model);

            if (!model.Id.HasValue)
            {
                template.Id = Guid.NewGuid();
            }

            _repositories.TemplateRepository.Add(template);
            _repositories.Save();

            _logicCore.ModuleCore.InitTemplateModules(template.Id);
        }


        public void AddRelayTemplate(Guid relayTemplateId)
        {
            AdminTemplates adminTemplate = _services.AuthService.GetAdminTemplateById(relayTemplateId);

            var existTemplate = _repositories.TemplateRepository.Find(t => t.Name == adminTemplate.Name && t.AddRelayTemplateToTemplates.HasValue).FirstOrDefault();

            if (existTemplate == null)
            {
                TemplateViewModel model = new TemplateViewModel
                {
                    Id = Guid.NewGuid(),
                    Name = adminTemplate.Name
                };

                var template = _mapper.Map<Template>(model);

                template.AddRelayTemplateToTemplates = true;
                _repositories.TemplateRepository.Add(template);
                _repositories.Save();

                _logicCore.ModuleCore.InitTemplateModules(template.Id);

                CopyTopicsRelayTemplate(adminTemplate.Id, template.Id, null);
            }
            else
            {
                CopyTopicsRelayTemplate(adminTemplate.Id, existTemplate.Id, existTemplate.Modules.ToList(), true);
            }
        }

        public void CopyTopicsRelayTemplate(Guid templateFromId, Guid templateToId, List<TemplateModule> existTemplateModules, bool isExistTemplate = false)
        {
            var topicGroupsHSE = _services.AuthService.GetAdminTopicGroups(Authorization.Domain.Entities.TypeOfModule.HSE, templateFromId);
            var topicGroupsCore = _services.AuthService.GetAdminTopicGroups(Authorization.Domain.Entities.TypeOfModule.Core, templateFromId);

            if (isExistTemplate)
            {
                var existTopicGroupsHSE = existTemplateModules.Where(t => t.Type == Core.Models.TypeOfModule.HSE).SelectMany(t => t.TopicGroups).ToList();
                var existTopicGroupsCore = existTemplateModules.Where(t => t.Type == Core.Models.TypeOfModule.Core).SelectMany(t => t.TopicGroups).ToList();

                topicGroupsHSE = topicGroupsHSE.Where(t => !existTopicGroupsHSE.Select(et => et.Name).Contains(t.Name)).ToList();
                topicGroupsCore = topicGroupsCore.Where(t => !existTopicGroupsCore.Select(et => et.Name).Contains(t.Name)).ToList();
            }

            if (topicGroupsHSE.Any())
            {
                topicGroupsHSE.ForEach(topicGroup =>
                {
                    rTopicGroupViewModel rTopicGroupViewModel = new rTopicGroupViewModel
                    {
                        Name = topicGroup.Name,
                        Enabled = true,
                        ModuleId = topicGroup.ModuleId
                    };

                    var topicGroupId = _services.TopicGroupService.AddTopicGroup(rTopicGroupViewModel);
                    _services.TopicGroupService.UpdateChildTopicGroupStatus(topicGroupId, templateToId, true);

                    topicGroup.Topics.ToList().ForEach(topic =>
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = topic.Name,
                            Enabled = true,
                            TopicGroupId = topicGroupId,
                            Metrics = topic.UnitsSelectedType != null ? true : false,
                            UnitsSelectedType = topic.UnitsSelectedType,
                            PlannedActualHas = topic.UnitsSelectedType != null ? true : false,
                            Description = topic.Description
                        };

                        var topicId = _services.TopicService.AddTopic(rTopicViewModel);
                        _services.TopicService.UpdateChildTopicStatus(topicId, templateToId, true, topicGroupId);
                    });
                });
            }

            if (topicGroupsCore.Any())
            {
                topicGroupsCore.ForEach(topicGroup =>
                {
                    rTopicGroupViewModel rTopicGroupViewModel = new rTopicGroupViewModel
                    {
                        Name = topicGroup.Name,
                        Enabled = true,
                        ModuleId = topicGroup.ModuleId
                    };

                    var topicGroupId = _services.TopicGroupService.AddTopicGroup(rTopicGroupViewModel);
                    _services.TopicGroupService.UpdateChildTopicGroupStatus(topicGroupId, templateToId, true);

                    topicGroup.Topics.ToList().ForEach(topic =>
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = topic.Name,
                            Enabled = true,
                            TopicGroupId = topicGroupId,
                            Metrics = topic.UnitsSelectedType != null ? true : false,
                            UnitsSelectedType = topic.UnitsSelectedType,
                            PlannedActualHas = topic.UnitsSelectedType != null ? true : false,
                            Description = topic.Description
                        };

                        var topicId = _services.TopicService.AddTopic(rTopicViewModel);
                        _services.TopicService.UpdateChildTopicStatus(topicId, templateToId, true, topicGroupId);
                    });
                });
            }
        }

        public void UpdateTemplate(TemplateViewModel model)
        {
            var template = _mapper.Map<Template>(model);

            _repositories.TemplateRepository.Update(template);
            _repositories.Save();
        }

        public bool TemplateExist(TemplateViewModel model)
        {
            return _repositories.TemplateRepository.Find(t => t.Id != model.Id && t.Name.Equals(model.Name)).Any();
        }

        public List<SelectListItem> GetSectionsType()
        {
            return new List<SelectListItem>
            {
                 new SelectListItem { Value = 0.ToString(), Text = "HSE" },
                 new SelectListItem { Value = 1.ToString(), Text = "Core" }
            };
        }


        public CopyTemplateViewModel GetCopyTemplateViewModel(Guid templateId)
        {
            var template = _repositories.TemplateRepository.Get(templateId);

            var templates = _repositories.TemplateRepository.Find(p => !p.DeletedUtc.HasValue).Where(t => !t.Positions.Any() || t.Positions.Any(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter())));
            var resultTemplates = _mapper.Map<IEnumerable<SelectListItem>>(templates.OrderBy(t => t.Name));

            var templatesRT = _services.AuthService.GetAdminTemplates(null);

            resultTemplates = resultTemplates.Where(t => templatesRT.FirstOrDefault(rt => rt.Value == t.Value) == null);

            CopyTemplateViewModel model = new CopyTemplateViewModel
            {
                CopyFromTemplateId = template.Id,
                CopyToTemplateId = null,
                Templates = resultTemplates
            };

            return model;
        }

        public CopyTemplateViewModel CopyTemplateViewModel(CopyTemplateViewModel model)
        {
            var modules = _services.ModuleService.GetBaseModulesViewModels();

            var templateModule = _services.TemplateService.PopulateTemplatesViewModel(model.CopyFromTemplateId.Value.ToString(), null, null, null, null, null, null);

            var templateCopyToModule = _services.TemplateService.PopulateTemplatesViewModel(model.CopyToTemplateId.Value.ToString(), null, null, null, null, null, null);

            templateModule.Template.Modules.ToList().ForEach(module =>
            {
                module.TopicGroups.ToList().ForEach(topicGroup =>
                {
                    _services.TopicGroupService.UpdateChildTopicGroupStatus(topicGroup.Id, model.CopyToTemplateId.Value, true);

                    topicGroup.Topics.ToList().ForEach(topic =>
                    {
                        if (templateCopyToModule.Template.Modules.SelectMany(t => t.TopicGroups).SelectMany(t => t.Topics).FirstOrDefault(t => t.ParentTopicId == topic.Id) == null)
                        {
                            _services.TopicService.UpdateChildTopicStatus(topic.Id.Value, model.CopyToTemplateId.Value, true, topicGroup.Id);
                        }
                    });
                });
            });

            return model;
        }

        public IEnumerable<SelectListItem> GetAdminTemplateList(Guid? sectorId)
        {
            return _services.AuthService.GetAdminTemplates(sectorId);
        }

        public IEnumerable<SelectListItem> GetSectorTemplateList()
        {
            return _services.AuthService.GetAdminSectors();
        }

        public GlobalTemplatesViewModel GetGlobalTemplates()
        {
            GlobalTemplatesViewModel model = new GlobalTemplatesViewModel();

            model.GlobalTemplates = this.GetAdminTemplateList(null);
            model.Id = Guid.NewGuid();
            model.Sectors = this.GetSectorTemplateList();

            return model;
        }

        public bool SectorExist(SectorViewModule model)
        {
            if (_services.AuthService.GetAdminSectors().Any(t => t.Text == model.Name))
            {
                return true;
            }
            return false;
        }

        public void SendNotificationAddedSector()
        {
            var relayUsers = _services.AuthService.GetAdminsOfSunscription();

            if (relayUsers != null)
            {
                foreach (var user in relayUsers)
                {
                    if (_repositories.UserProfileRepository.Get(user.Id) == null)
                    {
                        UserProfile userProfile = new UserProfile
                        {
                            Id = user.Id,
                            Email = user.Email,
                            UserName = user.Email,
                            PositionId = DbInitializerConstants.Positions.iHandoverAdminPositionId,
                            TeamId = DbInitializerConstants.Teams.iHandoverTeamId,
                            RotationPatternId = DbInitializerConstants.WorkPatterns.FiveWorkTwoOff,
                            CompanyId = DbInitializerConstants.Companies.iHandoverCompanyId,
                            LastPasswordChangedDate = DateTime.Now
                        };

                        _repositories.UserProfileRepository.Add(userProfile);
                        _repositories.Save();
                    }
                }
            }
            var usersProfile = _repositories.UserProfileRepository.GetAll().ToList().Where(t => _services.AuthService.GetUserRoles(t.Id).Any(r => r != iHandoverRoles.Relay.iHandoverAdmin)).ToList();

            if (usersProfile != null)
            {
                foreach (var user in usersProfile)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_CreatedSectorIhandoverAdmin(user.Id);
                }
            }
        }
        
        public void AddExistTemplate()
        {
            var templates = _repositories.TemplateRepository.GetAll().ToList().Where(t => t.Name == "iHandover Admin");

            if (templates != null)
            {
                foreach (var template in templates)
                {
                    _services.AuthService.AddExistTemplate(template);
                }
            }
        }

        public TemplateViewModel GetAdminTemplateViewModel(Guid templateId)
        {
            var adminTemplates = _services.AuthService.GetAdminTemplateById(templateId);

            if (_repositories.TemplateRepository.Get(adminTemplates.Id) == null)
            {
                Template model = new Template
                {
                    Id = adminTemplates.Id,
                    Description = adminTemplates.Description,
                    Name = adminTemplates.Name,
                    CreatedUtc = adminTemplates.CreatedUtc,
                    DeletedUtc = adminTemplates.DeletedUtc,
                    ModifiedUtc = adminTemplates.ModifiedUtc
                };

                _repositories.TemplateRepository.Add(model);
                _repositories.Save();

                return _mapper.Map<TemplateViewModel>(model);
            }

            Template templateModel = new Template
            {
                Id = adminTemplates.Id,
                Description = adminTemplates.Description,
                Name = adminTemplates.Name,
                CreatedUtc = adminTemplates.CreatedUtc,
                DeletedUtc = adminTemplates.DeletedUtc,
                ModifiedUtc = adminTemplates.ModifiedUtc
            };

            return _mapper.Map<TemplateViewModel>(templateModel);
        }
        
    }
}
