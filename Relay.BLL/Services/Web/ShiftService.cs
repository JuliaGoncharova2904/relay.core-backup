﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;
using System.Web.Configuration;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class ShiftService : IShiftService
    {
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;
        private readonly IServicesUnitOfWork _services;

        public ShiftService(LogicCoreUnitOfWork logicCore, IAuthService authService, IServicesUnitOfWork services)
        {
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._authService = authService;
            this._services = services;
        }

        /// <summary>
        /// Populate ConfirmShiftViewModel
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public ConfirmShiftViewModel PopulateConfirmShiftViewModel(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (!user.CurrentRotationId.HasValue)
                throw new NullReferenceException(string.Format("User with Id: {0} don't have working pattern.", userId));

            var model = new ConfirmShiftViewModel
            {
                StartLimitDate = _logicCore.RotationCore.RotationLastShiftEndDate(user.CurrentRotation),
                EndLimitDate = _logicCore.RotationCore.RotationSwingEndDate(user.CurrentRotation).AddMinutes(-60),
                BeginningOfNextShiftLimitDate = _logicCore.RotationCore.RotationExpiredDate(user.CurrentRotation)
            };

            Shift nextShift = _logicCore.ShiftCore.GetOrCreateNextShift(user.CurrentRotation);

            if (nextShift.StartDateTime.HasValue)
            {
                model.StartShiftDate = nextShift.StartDateTime;
                model.EndShiftDate = nextShift.StartDateTime.Value.AddMinutes(nextShift.WorkMinutes);
                model.NextShiftDate = nextShift.StartDateTime.Value.AddMinutes(nextShift.WorkMinutes + nextShift.BreakMinutes);
                model.RepeatTimes = nextShift.RepeatTimes;
            }

            return model;
        }

        /// <summary>
        /// Populate Confirm Shift V2 View Model
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <returns></returns>
        public ConfirmShiftV2ViewModel PopulateConfirmShiftV2ViewModel(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (!user.CurrentRotationId.HasValue)
                throw new NullReferenceException($"User with Id: {userId} don't have rotation.");

            var model = new ConfirmShiftV2ViewModel
            {
                TodayShiftEndLimitDateTime = _logicCore.AdministrationCore.GetCurrentServerTime(user.Id),
                ShiftOwnerId = user.Id,
                ShiftPatternType = user.ShiftPatternType
            };


            return model;
        }


        /// <summary>
        /// Populate ChooseShiftRecipientViewModel
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public ChooseShiftRecipientModel PopulateChooseShiftRecipientViewModel(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            Rotation shiftRotation = shift.Rotation;

            UserProfile shiftOwner = shiftRotation.RotationOwner;

            IEnumerable<Guid> usersIds = _authService.GetUsersIdsByRole(iHandoverRoles.Relay.iHandoverAdmin, Guid.NewGuid());

            var rotationShiftsRecipients = shiftRotation.RotationShifts.Where(s => s.ShiftRecipientId.HasValue)
                                                                .Select(s => s.ShiftRecipient)
                                                                .Where(user => user.CurrentRotationId.HasValue
                                                                    && user.CurrentRotation.RotationType == Core.Models.RotationType.Shift);

            var shiftOwnerTeamMembers = _logicCore.TeamCore.GetAllTeams().SelectMany(t => t.Users)
                                                                .Where(user => user.CurrentRotationId.HasValue
                                                                                && user.CurrentRotation.RotationType == Core.Models.RotationType.Shift);


            var resultList = rotationShiftsRecipients.Concat(shiftOwnerTeamMembers)
                                                     .OrderBy(user => user.FirstName)
                                                     .GroupBy(x => x.Id)
                                                     .OrderByDescending(group => group.Count())
                                                     .Select(g => g.First()).ToList();

            resultList = resultList.Where(u => !u.DeletedUtc.HasValue).Where(u => !shiftOwner.Contributors.Contains(u)).ToList();

            var adminIdOfSubscription = _authService.GetIdAdminOfSubscription();

            if (adminIdOfSubscription != null)
            {
                var adminOfSubscription = _logicCore.UserProfileCore.GetUserProfile(adminIdOfSubscription.Value);

                if (adminOfSubscription != null)
                {
                    resultList.Add(adminOfSubscription);
                }
            }

            var usersWitoutRotation = _logicCore.TeamCore.GetAllTeams().SelectMany(t => t.Users).Where(t => !t.CurrentRotationId.HasValue).ToList();

            if (usersWitoutRotation.Any())
            {
                resultList.ToList().AddRange(usersWitoutRotation);
            }

            ChooseShiftRecipientModel model = new ChooseShiftRecipientModel
            {
                ShiftId = shiftId,
                ShiftRecipientId = shift.ShiftRecipientId,
                TeamMembers = _mapper.Map<IEnumerable<DropDownListItemModel>>(resultList)
            };

            return model;
        }

        /// <summary>
        /// Is shift working time ended
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public bool IsShiftWorkTimeEnd(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.State == ShiftState.Break;
        }

        /// <summary>
        /// Is shift ended
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public bool IsShiftEnd(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.State == ShiftState.Finished;
        }

        /// <summary>
        /// Return current shift Id for rotation or NULL if current shift is nit exist.
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public Guid? GetCurrentShiftIdForRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);
            Shift shift = _logicCore.ShiftCore.GetCurrentRotationShift(rotation);

            return (shift != null) ? (Guid?)shift.Id : null;
        }

        public Guid? GetCurrentShiftIdForRotation(Rotation rotation)
        {
            //Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);
            Shift shift = _logicCore.ShiftCore.GetCurrentRotationShift(rotation);
            return (shift != null) ? (Guid?)shift.Id : null;
        }

        public Guid? GetCurrentShiftIdByRotationId(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);
            Shift shift = _logicCore.ShiftCore.GetCurrentRotationShiftByRotationId(rotation);
            return (shift != null) ? (Guid?)shift.Id : null;
        }

        /// <summary>
        /// Populate EditShiftDatesViewModel model from Shift entity.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public EditShiftDatesViewModel GetShiftDates(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);
            Rotation rotation = shift.Rotation;

            EditShiftDatesViewModel model = new EditShiftDatesViewModel
            {
                ShiftId = shift.Id,
                StartShiftDate = shift.StartDateTime.Value,
                EndShiftDate = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes),
                NextShiftDate = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes + shift.BreakMinutes),
                BeginningOfNextShiftLimitDate = _logicCore.RotationCore.RotationExpiredDate(rotation),
                CurrentDate = _logicCore.AdministrationCore.GetCurrentServerTime(rotation.RotationOwnerId),
                EndLimitDate = _logicCore.RotationCore.RotationSwingEndDate(rotation).AddMinutes(-60),
                RepeatTimes = shift.RepeatTimes,
                EndShiftDateReadOnly = shift.State == ShiftState.Break || shift.State == ShiftState.Finished,
                NextShiftDateReadOnly = shift.State == ShiftState.Finished
            };

            return model;
        }


        /// <summary>
        /// Populate EditShiftDatesViewModel model from Shift entity.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public EditShiftDatesV2ViewModel PopulateEditShiftModel(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            //var currentServerTime = _logicCore.AdministrationCore.GetCurrentServerTime(rotation.RotationOwnerId);

            EditShiftDatesV2ViewModel model = new EditShiftDatesV2ViewModel
            {
                ShiftId = shift.Id,
                EndShiftDate = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes),
                ShiftEndHour = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).Hour,
                ShiftEndMinutes = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).Minute,
                /*LimitEndShiftDate = currentServerTime,
                LimitShiftEndHour = currentServerTime.Hour,
                LimitShiftEndMinutes = 5 * (int)Math.Round(currentServerTime.Minute / 5.0),*/
            };

            return model;
        }


        public void UpdateRotationDates(EditShiftDatesViewModel model)
        {
            _logicCore.ShiftCore.UpdateShiftDates(model.ShiftId, model.StartShiftDate.Value, model.EndShiftDate.Value,
                                                            model.NextShiftDate.Value, model.RepeatTimes);
        }

        public void UpdateShiftEndTime(Guid shiftId, DateTime shiftEndTime)
        {
            _logicCore.ShiftCore.UpdateShiftEndDate(shiftId, shiftEndTime);
        }


        /// <summary>
        /// Rotation Has Working Shift
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public bool RotationHasWorkingShift(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _logicCore.ShiftCore.RotationHasWorkingShift(rotation);
        }

        /// <summary>
        /// Shift has recipient
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public bool ShiftHasRecipient(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.ShiftRecipientId.HasValue;
        }

        public Guid GetUserIdByShiftId(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);
            return shift.Rotation.RotationOwnerId;
        }

        public Shift GetShift(Guid shiftId)
        {
            return _logicCore.ShiftCore.GetShift(shiftId);
        }
        /// <summary>
        /// Return rotation Id for shift.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public Guid ShiftRotationId(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            return shift.RotationId;
        }


        public void UpdateShiftRecipient(ChooseShiftRecipientModel model)
        {
            if (model.ShiftRecipientId.HasValue)
            {
                _logicCore.ShiftCore.UpdateShiftRecipient(model.ShiftId, model.ShiftRecipientId.Value);
            }
        }

        public void ConfirmShift(Guid userId, ConfirmShiftViewModel model)
        {
            _logicCore.ShiftCore.ConfirmShift(userId, model.StartShiftDate.Value, model.EndShiftDate.Value,
                                                    model.NextShiftDate.Value, model.RepeatTimes);
        }

        public void ConfirmShiftV2(Guid userId, DateTime startDate, DateTime endTime, Guid identityUserId)
        {
            Guid? adminSubscriptionId = _authService.GetSubscriptionOwnerId();

            _logicCore.ShiftCore.ConfirmShiftV2(userId, startDate, endTime, identityUserId, adminSubscriptionId);
        }

        /// <summary>
        /// Return period of Shift by Shift Id.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public string GetShiftPeriod(Guid? shiftId)
        {
            string period = "...";

            if (shiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(shiftId.Value);

                if (shift.StartDateTime.HasValue)
                {
                    period = shift.StartDateTime.Value.FormatWithDayMonthYear();
                }
            }

            return period;
        }

        /// <summary>
        /// Return Handover Recipient Full Name of Shift by Shift Id.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public string GetShiftHandoverRecipientName(Guid? shiftId)
        {
            string handoverRecipientName = String.Empty;

            if (shiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(shiftId.Value);

                if (shift.ShiftRecipientId.HasValue)
                {
                    handoverRecipientName = shift.ShiftRecipient.FullName;
                }
            }

            return handoverRecipientName;
        }


        /// <summary>
        /// Return Full Name of Shift Owner by Shift Id.
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <returns></returns>
        public string GetShiftOwnerName(Guid shiftId)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);
            var ownerName = shift.Rotation.RotationOwner.FullName;

            return ownerName;
        }

        /// <summary>
        /// End Shift
        /// </summary>
        /// <param name="shiftId"></param>
        /// <returns></returns>
        public void ExpireShift(Guid shiftId, byte[] pdfBytes)
        {
            Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            _logicCore.MailerService.SendShiftRecepintEmail(shift, pdfBytes);

            _logicCore.ShiftCore.EndShift(shift);
        }

        public List<ShiftReceivedSlideViewModel> PopulateReceivedShiftsPanel(Guid shiftId, Guid userId)
        {
            var handoverFromShifts = _logicCore.ShiftCore.GetShift(shiftId).HandoverFromShifts.ToList().OrderByDescending(shift => shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes));
            List<ShiftReceivedSlideViewModel> model = new List<ShiftReceivedSlideViewModel>();

            foreach (var shift in handoverFromShifts)
            {
                List<ShiftContributorForReceivedPanel> contributors = shift.ShiftRecipient.Contributors.ToList().Select(c => new ShiftContributorForReceivedPanel
                {
                    ContributorID = c.Id,
                    ContributorName = c.FullName
                }).ToList();

                var source = shift.HandoverToShifts.Where(s => s.Rotation.RotationOwnerId == shift.ShiftRecipientId).First();

                var rotationModules = _logicCore.RotationModuleCore.GetShiftModule(source.Id, TypeOfModuleSource.Received);

                List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

                foreach (var rotationModule in rotationModules)
                {
                    IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                            .Where(tg => tg.Enabled)
                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                            .Where(t => t.Enabled);
                    if (rotationModule.Type == TypeOfModule.Team)
                    {
                        var topicsGroupByRelation = rotationTopics.Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.RelationId.Value).ToList().Select(t => t.FirstOrDefault()).ToList();

                        topicsGroupByRelation.ForEach(topicGroupByRelation =>
                        {
                            var topics = _logicCore.RotationModuleCore.GetTopicsByRelationId(topicGroupByRelation.RelationId.Value, topicGroupByRelation.RotationTopicGroupId);
                            sortedRotationTopics.AddRange(topics);
                        });
                    }
                    else
                    {
                        var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.OrderByDescending(r => r.RotationTopics.Count).FirstOrDefault()).ToList();

                        if (topicGroupsByName != null)
                        {
                            topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                            {
                                var topics = topicGroup.RotationTopics.ToList();

                                sortedRotationTopics.AddRange(topics);
                            });
                        }
                    }
                }

                var count = sortedRotationTopics.Count();


                ShiftReceivedSlideViewModel slide = new ShiftReceivedSlideViewModel
                {
                    Id = shift.Id,
                    HandoverFromID = shift.Rotation.RotationOwnerId,
                    HandoverFromName = shift.Rotation.RotationOwner.FullName,
                    TopicCounter = count,
                   // TopicCounter = _logicCore.RotationTopicCore.GetAllShiftTopic(shiftId, TypeOfModuleSource.Received)
                    //                                .Count(t => t.Enabled && t.AncestorTopic.RotationTopicGroup.RotationModule.ShiftId == shift.Id && !t.DeletedUtc.HasValue),
                    //TaskCounter = _logicCore.RotationTaskCore.GetAllShiftTasks(shiftId, TypeOfModuleSource.Received)
                    //                                 .Count(t => t.Enabled && t.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.ShiftId == shift.Id && !t.DeletedUtc.HasValue),
                    TaskCounter = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForShift(shiftId, ModuleSourceType.Received, shift.Id).Count(),
                    Date = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDayMonthYear(),
                    Contributors = contributors,
                    ContributorsList = shift.ShiftRecipient.Contributors.Select(t =>
                     new DropDownListItemModel
                     {
                         Text = t.FullName,
                         Value = t.Id.ToString()
                     }).ToList()
                };

                model.Add(slide);
            }

            return model;
        }

        /// <summary>
        /// Change Finalize Status For Draft Topics And Tasks
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <param name="status">Finalize status</param>
        public void SetFinalizeStatusForDraftItems(Guid shiftId, FinalizeStatus status)
        {
            _logicCore.ShiftCore.SetFinalizeStatusForDraftItems(shiftId, (StatusOfFinalize)status);
        }



        /// <summary>
        /// Return Period of received rotation by Shift Id
        /// </summary>
        /// <param name="shiftId">Shift Id</param>
        /// <param name="sourceShiftId">Selected Source Shift Id</param>
        /// <returns></returns>
        public string GetReceivedShiftPeriod(Guid? shiftId, Guid? sourceShiftId)
        {
            string period = null;

            if (sourceShiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(sourceShiftId.Value);

                if (shift.StartDateTime != null)
                    period = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatDayMonthYear();
            }
            else if (shiftId.HasValue)
            {
                Shift shift = _logicCore.ShiftCore.GetShift(shiftId.Value);

                if (shift.HandoverFromShifts.Count() == 1)
                {
                    Shift receivedShift = shift.HandoverFromShifts.First();

                    if (receivedShift.StartDateTime != null)
                        period = receivedShift.StartDateTime.Value.AddMinutes(receivedShift.WorkMinutes + receivedShift.BreakMinutes).FormatDayMonthYear();
                }
            }

            return period;
        }

        public string GetShiftType(Shift shift)
        {
          //  Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            if (shift != null)
            {
                var user = _logicCore.UserProfileCore.GetRelayEmployees().FirstOrDefault(t => t.Id == shift.Rotation.RotationOwnerId);

                if (user.ShiftPatternType != null)
                {
                    if (user.ShiftPatternType == "1")
                    {
                        if (new TimeSpan(0, 0, 0) <= shift.StartDateTime.Value.AddHours(12).TimeOfDay && shift.StartDateTime.Value.AddHours(12).TimeOfDay < new TimeSpan(12, 0, 0))
                        {
                            return "Night";
                        }

                        if (new TimeSpan(12, 0, 0) <= shift.StartDateTime.Value.AddHours(12).TimeOfDay && shift.StartDateTime.Value.AddHours(12).TimeOfDay >= new TimeSpan(0, 0, 0))
                        {
                            return "Day";
                        }

                    }

                    if (user.ShiftPatternType == "2")
                    {
                        if (new TimeSpan(12, 0, 0) <= shift.StartDateTime.Value.AddHours(8).TimeOfDay && shift.StartDateTime.Value.AddHours(8).TimeOfDay <= new TimeSpan(15, 55, 0))
                        {
                            return "Early";
                        }

                        if (new TimeSpan(16, 0, 0) <= shift.StartDateTime.Value.AddHours(8).TimeOfDay && shift.StartDateTime.Value.AddHours(8).TimeOfDay >= new TimeSpan(0, 0, 0))
                        {
                            return "Late";
                        }

                        if (new TimeSpan(4, 0, 0) <= shift.StartDateTime.Value.AddHours(8).TimeOfDay && shift.StartDateTime.Value.AddHours(8).TimeOfDay <= new TimeSpan(8, 0, 0))
                        {
                            return "Night";
                        }
                    }
                }

                return null;
            }
            return null;
        }

        public string GetShiftType(Guid shiftId)
        {
              Shift shift = _logicCore.ShiftCore.GetShift(shiftId);

            if (shift != null)
            {
                var user = _logicCore.UserProfileCore.GetRelayEmployees().FirstOrDefault(t => t.Id == shift.Rotation.RotationOwnerId);

                if (user.ShiftPatternType != null)
                {
                    if (user.ShiftPatternType == "1")
                    {
                        if (new TimeSpan(0, 0, 0) <= shift.StartDateTime.Value.AddHours(12).TimeOfDay && shift.StartDateTime.Value.AddHours(12).TimeOfDay < new TimeSpan(12, 0, 0))
                        {
                            return "Night";
                        }

                        if (new TimeSpan(12, 0, 0) <= shift.StartDateTime.Value.AddHours(12).TimeOfDay && shift.StartDateTime.Value.AddHours(12).TimeOfDay >= new TimeSpan(0, 0, 0))
                        {
                            return "Day";
                        }

                    }

                    if (user.ShiftPatternType == "2")
                    {
                        if (new TimeSpan(12, 0, 0) <= shift.StartDateTime.Value.AddHours(8).TimeOfDay && shift.StartDateTime.Value.AddHours(8).TimeOfDay <= new TimeSpan(15, 55, 0))
                        {
                            return "Early";
                        }

                        if (new TimeSpan(16, 0, 0) <= shift.StartDateTime.Value.AddHours(8).TimeOfDay && shift.StartDateTime.Value.AddHours(8).TimeOfDay >= new TimeSpan(0, 0, 0))
                        {
                            return "Late";
                        }

                        if (new TimeSpan(4, 0, 0) <= shift.StartDateTime.Value.AddHours(8).TimeOfDay && shift.StartDateTime.Value.AddHours(8).TimeOfDay <= new TimeSpan(8, 0, 0))
                        {
                            return "Night";
                        }
                    }
                }

                return null;
            }
            return null;
        }
    }
}
