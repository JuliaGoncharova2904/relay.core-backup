﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Core.Models;
using System.Web.Mvc;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class PositionService : BaseService, IPositionService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly IServicesUnitOfWork _services;
        private readonly LogicCoreUnitOfWork _logicCore;

        public PositionService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services) : base(services.AuthService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
        }


        public IEnumerable<SelectListItem> GetPositionsList()
        {
            var positions = _logicCore.PositionCore.GetRelayPositions().Where(p => !p.Users.Any() || p.Users.AsQueryable().Any(ApplyUserFilter()));

            return _mapper.Map<IEnumerable<SelectListItem>>(positions);
        }


        public PositionViewModel PopulatePositionModel()
        {
            return PopulatePositionModel(new PositionViewModel());
        }

        public PositionViewModel PopulatePositionModel(PositionViewModel model)
        {
            model.Sites = _services.SiteService.GetSitesList();
            model.Templates = _services.TemplateService.GetTemplateList();

            return model;
        }

        public PositionViewModel GetPosition(Guid positionId)
        {
            Position position = _repositories.PositionRepository.Get(positionId);
            if (position != null)
            {
                PositionViewModel model = _mapper.Map<PositionViewModel>(position);
                return this.PopulatePositionModel(model);
            }

            return null;
        }

        public bool UpdatePosition(PositionViewModel model)
        {
            if (model.Id.HasValue)
            {
                Position position = _repositories.PositionRepository.Get(model.Id.Value);
                if (position != null)
                {
                    _mapper.Map(model, position);

                    _repositories.PositionRepository.Update(position);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        public IEnumerable<PositionViewModel> GetPositionsForSite(Guid siteId)
        {
            IQueryable<Position> positions = _logicCore.PositionCore.GetRelayPositions().Where(p => p.WorkplaceId == siteId).OrderBy(p => p.Name);
            return _mapper.Map<IEnumerable<PositionViewModel>>(positions);
        }

        public PositionViewModel GetPositionForEmployee(Guid employeeId)
        {
            UserProfile employee = _repositories.UserProfileRepository.Get(employeeId);
            if (employee != null)
            {
                return _mapper.Map<PositionViewModel>(employee.Position);
            }

            return null;
        }

        public bool AddPosition(PositionViewModel position)
        {
            var siteId = Guid.Parse(position.SiteId);

            Template template = null;

            if (!string.IsNullOrWhiteSpace(position.CustomTemplateName))
            {
                template = new Template
                {
                    Id = Guid.NewGuid(),
                    Name = position.CustomTemplateName
                };

                _repositories.TemplateRepository.Add(template);

                _logicCore.ModuleCore.InitTemplateModules(template.Id);
            }

            if (!_repositories.PositionRepository.Find(m => m.Name == position.Name && m.WorkplaceId == siteId && m.Id != position.Id.Value).Any())
            {
                Position positionEntity = _mapper.Map<Position>(position);
                positionEntity.Id = Guid.NewGuid();
                if (template != null)
                {
                    positionEntity.TemplateId = template.Id;
                }
                else
                {
                    positionEntity.TemplateId = position.TemplateId.Value;
                }

                _repositories.PositionRepository.Add(positionEntity);
                _repositories.Save();
                return true;
            }

            return false;
        }


        public void RemovePosition(Guid positionId)
        {
            _repositories.PositionRepository.Delete(positionId);
            _repositories.Save();
        }


        public bool CanRemovePosition(Guid positionId)
        {
            var positions = _logicCore.PositionCore.GetRelayPositions().Where(p => p.Id == positionId).Where(p => !p.Users.Any()).ToList();

            if (!positions.Any())
            {
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
