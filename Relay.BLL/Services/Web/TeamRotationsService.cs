﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TeamRotationsService : ITeamRotationsService
    {
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IAuthService _authService;
        private readonly IServicesUnitOfWork _services;

        public TeamRotationsService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services, IAuthService authService)
        {
            this._logicCore = logicCore;
            this._authService = authService;
            this._services = services;
        }

        /// <summary>
        /// Create Team Rotations view model.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="isViewMode">Is View mode flag</param>
        /// <param name="page">page number</param>
        /// <param name="pageSize">page size</param>
        /// <returns></returns>
        public MyTeamRotationViewModel GetMyTeamRotations(Guid userId, bool isViewMode, int page, int pageSize, bool IdentityAdmin = false)
        {
            IEnumerable<string> userRoles = _authService.GetUserRoles(userId);
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId, IdentityAdmin);

            IQueryable<Rotation> rotations = _logicCore.TeamRotationsCore.GetActiveTeamRotations(user, userRoles, isViewMode);

            IEnumerable<Rotation> pagedRotations = rotations?
                .OrderBy(r => !r.RotationOwner.LockDate.HasValue)
                .ThenBy(r => r.RotationOwner.FirstName)
                .ThenBy(r => r.RotationOwner.LastName)
                .Skip(page * pageSize)
                .Take(pageSize)
                .ToList() ?? new List<Rotation>();

            MyTeamRotationViewModel model = new MyTeamRotationViewModel
            {
                IsViewMode = isViewMode,
                OwnerName = user.FullName,
                TeamRotations = this.MapRotationsToViewModel(pagedRotations).ToPagedList(page, pageSize, rotations.Count()),
                IsViewTeamBtn = userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin),
                XUser = _services.UserService.GetAllEmployees().Count(),
                YUser = _authService.GetSubscriptionDetailsModel().MaxUsers
            };

            return model;
        }

        /// <summary>
        /// Map rotations to TeamRotation view model.
        /// </summary>
        /// <param name="rotations">Rotation entities</param>
        /// <returns></returns>
        private IEnumerable<TeamRotationViewModel> MapRotationsToViewModel(IEnumerable<Rotation> rotations)
        {
            List<TeamRotationViewModel> viewModels = new List<TeamRotationViewModel>();

            foreach (Rotation rotation in rotations)
            {
                UserProfile owner = rotation.RotationOwner;
                IEnumerable<string> ownerRoles = _authService.GetUserRoles(owner.Id);

                viewModels.Add(new TeamRotationViewModel
                {
                    ID = rotation.Id,
                    OwnerId = owner.Id,
                    OwnerFullName = owner.FullName,
                    OwnerEmail = owner.Email,
                    OwnerPosition = owner.PositionName,
                    BackToBackId = rotation.RotationType == Core.Models.RotationType.Swing ? rotation.DefaultBackToBackId : rotation.RotationShifts.FirstOrDefault(r => r.State == ShiftState.Confirmed) != null ? rotation.RotationShifts.FirstOrDefault(r => r.State == ShiftState.Confirmed).ShiftRecipientId : rotation.DefaultBackToBackId,
                    BackToBackName = rotation.RotationType == Core.Models.RotationType.Swing ? rotation.DefaultBackToBack.FullName : rotation.RotationShifts.FirstOrDefault(r => r.State == ShiftState.Confirmed) != null ? rotation.RotationShifts.FirstOrDefault(r => r.State == ShiftState.Confirmed).ShiftRecipient.FullName : rotation.DefaultBackToBack.FullName,
                    IsActive = IsActiveShiftOrRotation(rotation, owner.Id),
                    StateMessage = this.RotationStateInfo(rotation, owner.Id),
                    IsLineManager = ownerRoles.Contains(iHandoverRoles.Relay.LineManager)
                                    || ownerRoles.Contains(iHandoverRoles.Relay.HeadLineManager)
                                    || ownerRoles.Contains(iHandoverRoles.Relay.SafetyManager),
                    IsAdmin = ownerRoles.Contains(iHandoverRoles.Relay.Administrator)
                              || ownerRoles.Contains(iHandoverRoles.Relay.iHandoverAdmin),
                    IsLock = owner.LockDate.HasValue,
                    IsEnable = ownerRoles.Contains(iHandoverRoles.Relay.LineManager) || ownerRoles.Contains(iHandoverRoles.Relay.SafetyManager),
                    IsHaveContributerUsers = owner.Contributors.Where(t => !t.DeletedUtc.HasValue).Count() > 0 ? true : false,
                    ContributorsUsers = owner.Contributors.Where(t => !t.DeletedUtc.HasValue).ToList(),
                    ContributorsUsersFullName = owner.Contributors.Where(t => !t.DeletedUtc.HasValue).Select(t => "\r\n" + t.FullName + "\r\n").ToList(),
                    ContributorsUsersList = owner.Contributors.Where(t => !t.DeletedUtc.HasValue).Select(t =>
                         new DropDownListItemModel
                         {
                             Text = t.FullName,
                             Value = t.Id.ToString()
                         }
                    )
                });
            }

            return viewModels;
        }


        public bool IsActiveShiftOrRotation(Rotation rotation, Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user.CurrentRotationId.HasValue)
            {
                var currentRotation = user.CurrentRotation;

                if (_services.RotationService.IsShiftRotation(currentRotation.Id))
                {
                    var shift = currentRotation.RotationShifts.Any()
                                           ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation) != null
                                               ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation)
                                               : currentRotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Finished)
                                           : null;
                    var res = _logicCore.ShiftCore.GetActiveRotationShift(currentRotation);

                    if (shift != null)
                    {
                        if (shift.State == ShiftState.Confirmed || shift.State == ShiftState.Created)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    if (rotation.State == RotationState.Confirmed)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Get rotation state info.
        /// </summary>
        /// <param name="rotation">Rotation entity</param>
        /// <returns></returns>
        private string RotationStateInfo(Rotation rotation, Guid userId)
        {
            string res = string.Empty;

            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user.CurrentRotationId.HasValue)
            {
                var currentRotation = user.CurrentRotation;

                if (_services.RotationService.IsShiftRotation(currentRotation.Id))
                {
                    var shift = currentRotation.RotationShifts.Any()
                                           ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation) != null
                                               ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation)
                                               : currentRotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Finished)
                                           : null;

                    if (shift != null)
                    {
                        switch (shift.State)
                        {
                            case ShiftState.Created:
                            case ShiftState.Confirmed:
                                res = "Until " + shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).FormatWithDateMonthFullYearAndTime();
                                break;
                            case ShiftState.Break:
                            case ShiftState.Finished:
                                res = "Off-site";
                                break;
                            default:
                                res = "Off-site";
                                break;
                        }
                    }
                    else
                    {
                        res = "Off-site";
                    }

                }
                else
                {
                    switch (currentRotation.State)
                    {
                        case RotationState.Confirmed:
                            res = "Until " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).ToString("dd/MM");
                            break;
                        case RotationState.SwingEnded:
                            res = "Back " + rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff).ToString("dd/MM");
                            break;
                        case RotationState.Expired:
                        case RotationState.Created:
                        default:
                            res = "Rotation not confirmed";
                            break;
                    }
                }
            }
            return res;
        }
    }
}
