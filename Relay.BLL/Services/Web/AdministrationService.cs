﻿using System.Linq;
using System.Web;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Configuration;
using MomentumPlus.Relay.Helpers;
using System.Web.Mvc;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class AdministrationService : IAdministrationService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;



        public AdministrationService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
        }

        public AccountDetailsViewModel PopulateAccountDetailsViewModel()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                bool isMultiTenant = AppSession.Current.IsMultiTenant;

                var templetesCounter = _repositories.TemplateRepository.Find(t=> t.Description != "Template for iHandover Admin").Count();

                var handoversCounter = _repositories.UserProfileRepository.Find(user => user.Email != "admin@ihandover.co").Count();

                var viewModel = new AccountDetailsViewModel
                {
                    PlanType = adminSettings.PlanType.ToString(),
                    LicenceEndDate = adminSettings.LicenceEndDate.HasValue ? adminSettings.LicenceEndDate.FormatWithDateMonthAndFullYear() : "-",
                    PurchaseDate = adminSettings.PurchaseDate.HasValue ? adminSettings.PurchaseDate.FormatWithDateMonthAndFullYear() : "-",
                    TechnicalSupport = adminSettings.SupportType.ToString(),
                    VersionDetails = "RelayWorks Version v2.1",
                    Templates = adminSettings.TemplateLimit.HasValue ? templetesCounter + "/" + adminSettings.TemplateLimit : "0" + "/" + templetesCounter,
                    Handovers = adminSettings.HandoverLimit.HasValue ? handoversCounter + "/" + adminSettings.HandoverLimit : "0" + "/" + handoversCounter,
                    isMultiTenant = isMultiTenant

                };

                if (isMultiTenant)
                {
                    var subscriptionDetailsModel = _services.AuthService.GetSubscriptionDetailsModel();

                    viewModel.PlanType = subscriptionDetailsModel.PlanType;
                    viewModel.LicenceEndDate = subscriptionDetailsModel.EndDate.HasValue ? subscriptionDetailsModel.EndDate.FormatWithDateMonthAndFullYear() : "-";
                    viewModel.PurchaseDate = subscriptionDetailsModel.StartDate.HasValue ? subscriptionDetailsModel.StartDate.FormatWithDateMonthAndFullYear() : "-";
                    viewModel.TechnicalSupport = null;
                    viewModel.Templates = templetesCounter.ToString() /*+ "/" + (subscriptionDetailsModel.MaxUsers + subscriptionDetailsModel.MaxManagers)*/;
                    viewModel.Handovers = handoversCounter + "/" + (subscriptionDetailsModel.MaxUsers + subscriptionDetailsModel.MaxManagers);
                    viewModel.AutoRenewSubscription = !subscriptionDetailsModel.CancelAtPeriodEnd;
                }

                return viewModel;
            }

            return null;
        }


        public void UpdateAccountDetails(AccountDetailsViewModel model)
        {
            UpgradeSubscriptionViewModel modelSubscription = _services.AuthService.GetUpgradeSubscriptionViewModel();

            var modelSubscriptionEmail = modelSubscription.Email;

            var userId = _services.AuthService.GetUserIdBySubscriptionId(modelSubscription.SubscriptionId.Value);

            if (model.AutoRenewSubscription.HasValue)
            {
                _services.AuthService.CancelSubscriptionAtPeriodEnd(model.AutoRenewSubscription.Value);

                if (!model.AutoRenewSubscription.Value)
                {
                    _services.AuthService.SendAutoRenewSubscriptionMail(userId, modelSubscriptionEmail);
                }
            }
        }


        public VersionDetailsViewModel PopulateVersionDetailsViewModel()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {

                var viewModel = new VersionDetailsViewModel
                {
                    MaximumHandovers = adminSettings.HandoverLimit.Value,
                    MaximumTemplates = adminSettings.TemplateLimit.Value,
                    LicenceEndDate = adminSettings.LicenceEndDate,
                    HostingProvider = adminSettings.HostingProvider,
                    iHandoverAccountManager = adminSettings.AccountManager,
                    DomainUrl = HttpContext.Current.Request.Url.Host
                };

                return viewModel;
            }

            return null;
        }

        public void UpdateVersionDetails(VersionDetailsViewModel model)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                adminSettings.AccountManager = model.iHandoverAccountManager;
                adminSettings.LicenceEndDate = model.LicenceEndDate;
                adminSettings.TemplateLimit = model.MaximumTemplates;
                adminSettings.HandoverLimit = model.MaximumHandovers;

                _repositories.AdminSettingsRepository.Update(adminSettings);
                _repositories.Save();
            }
        }

        public ThirdPartySettingsViewModel PopulateThirdPartySettingsViewModel()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {

                var viewModel = new ThirdPartySettingsViewModel
                {
                    SMSPerUserPerDay = adminSettings.TwilioSettings.NumberSMSPerUserPerDay,
                    TwilioAccount = adminSettings.TwilioSettings.TwilioAccount,
                    SMSGateway = adminSettings.TwilioSettings.SMSGateway,
                    TwilioEndPoint = adminSettings.TwilioSettings.TwilioEndpoint,
                    TwilioPhoneNumber = adminSettings.TwilioSettings.TwilioPhoneNumber
                };

                return viewModel;
            }

            return null;
        }


        public void UpdateThirdPartySettings(ThirdPartySettingsViewModel model)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                adminSettings.TwilioSettings.NumberSMSPerUserPerDay = model.SMSPerUserPerDay;
                adminSettings.TwilioSettings.SMSGateway = model.SMSGateway;
                adminSettings.TwilioSettings.TwilioAccount = model.TwilioAccount;
                adminSettings.TwilioSettings.TwilioEndpoint = model.TwilioEndPoint;
                adminSettings.TwilioSettings.TwilioPhoneNumber = model.TwilioPhoneNumber;

                _repositories.AdminSettingsRepository.Update(adminSettings);
                _repositories.Save();
            }
        }

        public PreferencesSettingsViewModel PopulatePreferencesSettingsViewModel(Guid userId)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                var timeZone = _services.AuthService.GetTimeZoneFromSubscription(userId);

                if (timeZone != null)
                {
                    timeZone = TimeZoneInfo.GetSystemTimeZones().Select(zone =>
                                        new SelectListItem
                                        {
                                            Value = zone.Id,
                                            Text = zone.DisplayName
                                        }).FirstOrDefault(zone => zone.Text == timeZone).Value;

                    adminSettings.TimeZoneId = timeZone;
                }

                var viewModel = _mapper.Map<PreferencesSettingsViewModel>(adminSettings);

                return viewModel;
            }

            return null;
        }

        public void UpdatePreferencesSettings(PreferencesSettingsViewModel model, HttpPostedFileBase logo)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            if (adminSettings != null)
            {
                adminSettings.HandoverPreviewTime = model.HandoverPreviewTime;
                adminSettings.TimeZoneId = model.Timezone;
                adminSettings.HandoverTriggerTime = model.HandoverTriggerTime;
                adminSettings.HandoverPreviewType = (TypeOfHandoverPreview)model.HandoverPreviewType;
                adminSettings.HandoverTriggerType = (TypeOfHandoverTrigger)model.HandoverTriggerType;

                adminSettings.AllowedFileExtensions = !string.IsNullOrEmpty(model.AllowedFileTypesString) ? string.Join(",", model.AllowedFileTypes) : null;

                if (logo != null)
                {
                    adminSettings.Logo = _logicCore.MediaCore.AddImage("Logo", logo.FileName, logo.ContentType, logo.InputStream, false);
                }

                _repositories.AdminSettingsRepository.Update(adminSettings);
                _repositories.Save();
            }
        }


        public DateTime GetCurrentServerTime(Guid userId)
        {
            var serverTime = _logicCore.AdministrationCore.GetCurrentServerTime(userId);

            return serverTime;
        }

        public string GetUserPattern(Guid userId)
        {
            return _logicCore.UserProfileCore.GetRelayEmployees().FirstOrDefault(t => t.Id == userId).ShiftPatternType;
           
        }

    }
}
