﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TaskService : ITaskService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TaskService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._services = services;
            this._logicCore = logicCore;
        }

        public IEnumerable<rTaskViewModel> GetTopicTasksViewModels(Guid topicId)
        {
            var tasks = _repositories.TemplateTaskRepository.Find(t => t.TopicId == topicId).OrderBy(t => t.Name);

            return _mapper.Map<IEnumerable<rTaskViewModel>>(tasks);
        }

        public rTaskViewModel GetTaskViewModel(Guid taskId)
        {
            var task = _repositories.TemplateTaskRepository.Get(taskId);

            return _mapper.Map<rTaskViewModel>(task);
        }

        public void AddTask(rTaskViewModel model)
        {
            var task = _mapper.Map<TemplateTask>(model);
            task.Id = Guid.NewGuid();
            _repositories.TemplateTaskRepository.Add(task);
            _repositories.Save();
        }

        public void UpdateTask(rTaskViewModel model)
        {
            var task = _mapper.Map<TemplateTask>(model);

            _repositories.TemplateTaskRepository.Update(task);
            _repositories.Save();

            _logicCore.TaskCore.UpdateChildTasks(task);
        }

        public bool TaskExist(rTaskViewModel model)
        {
            return _repositories.TemplateTaskRepository.Find(t => t.Id != model.Id && t.TopicId == model.TopicId && t.Name.Equals(model.Name)).Any();
        }

        public IEnumerable<rTaskViewModel> PopulateTemplateTopicTasksViewModels(Guid baseTopicId, Guid templateModuleId)
        {
            var tasks = GetTopicTasksViewModels(baseTopicId);

            foreach (var task in tasks)
            {
                task.Enabled = CheckChildTaskStatus(task.Id, templateModuleId);
            }

            return tasks;
        }

        public bool CheckChildTaskStatus(Guid parentTaskId, Guid templateModuleId)
        {
            var task = _logicCore.TaskCore.GetChildTaskFromModule(parentTaskId, templateModuleId);

            if (task != null)
            {
                return task.Enabled;
            }

            return false;
        }

        public void UpdateTaskStatus(Guid taskId, bool status)
        {
            var task = _logicCore.TaskCore.GetTask(taskId);

            task.Enabled = status;

            _repositories.Save();

            //----- Adding new topic items to rotations live ------
            if (status)
            {
                _services.RotationTaskService.UpdateTemplateTaskChildTaskStatus(task.Id, status);
            }
            //-----------------------------------------------------
        }

        public void UpdateChildTaskStatus(Guid baseTaskId, Guid templateId, bool status)
        {
            var baseTask = _logicCore.TaskCore.GetTask(baseTaskId);

            var templateModule = _logicCore.ModuleCore.GetTemplateModule(baseTask.Topic.TopicGroup.Module.Type, templateId);

            var templateTopicGroup = _logicCore.TopicGroupCore.GetOrCreateTemplateTopicGroup(templateModule.Id, baseTask.Topic.TopicGroup);

            var templateTopic = _logicCore.TopicCore.GetOrCreateTemplateTopicGroupTopic(templateTopicGroup.Id, baseTask.Topic);

            var templateTask = _logicCore.TaskCore.GetOrCreateTemplateTopicTask(templateTopic.Id, baseTask);

            if (status == true)
            {
                _services.ModuleService.ChangeModuleStatus(templateModule.Id, true);
                _services.TopicGroupService.UpdateTopicGroupStatus(templateTopicGroup.Id, true);
                _services.TopicService.UpdateTopicStatus(templateTopic.Id, true);
            }


            UpdateTaskStatus(templateTask.Id, status);
        }

        public void UpdateTopicTasksStatus(Guid topicId, bool status)
        {
            var topic = _logicCore.TopicCore.GetTopic(topicId);

            if (topic.Tasks != null)
            {
                foreach (var task in topic.Tasks)
                {
                    UpdateTaskStatus(task.Id, status);
                }
            }
        }


        public void RemoveTask(Guid taskId)
        {
            var task = _repositories.TemplateTaskRepository.Get(taskId);

            if (task.ChildTasks != null)
            {
                var taskChilds = task.ChildTasks;

                foreach (var taskChild in taskChilds)
                {
                    taskChild.DeletedUtc = DateTime.UtcNow;
                    taskChild.Enabled = false;
                    _repositories.TemplateTaskRepository.Update(taskChild);
                    _logicCore.TaskCore.UpdateChildTasks(taskChild);

                    _repositories.Save();
                }
            }

            task.DeletedUtc = DateTime.UtcNow;
            task.Enabled = false;
            _repositories.TemplateTaskRepository.Update(task);
            _logicCore.TaskCore.UpdateChildTasks(task);

            _repositories.Save();
        }
    }
}