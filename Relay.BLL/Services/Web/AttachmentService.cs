﻿using System;
using System.Collections.Generic;
using AutoMapper;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Models;
using System.Linq;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore.AccessValidation;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class AttachmentService : IAttachmentService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public AttachmentService(LogicCoreUnitOfWork logicCore)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
        }

        public FileViewModel GetAttachment(Guid id)
        {
            Attachment attachment = _repositories.AttachmentRepository.Get(id);

            if (attachment.File != null)
            {
                var model = _mapper.Map<FileViewModel>(attachment.File);

                model.FileName = attachment.FileName;

                return model;

            }

            return null;
        }





        /// <summary>
        /// Return attachments view models for topic.
        /// </summary>
        /// <param name="topicId">Topic ID</param>
        /// <returns></returns>
        public List<AttachmentViewModel> GetAttachmentForTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            return this.MapAttachmentsToViewModel(topic.Attachments);

        }


        public List<AttachmentViewModel> GetAttachmentLinkForTopic(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            return this.MapAttachmentLinksToViewModel(topic.AttachmentsLink);

        }

        public List<AttachmentViewModel> GetAttachmentLinkForTask(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            ICollection<AttachmentsLink> atachmentTask = null;

            if (task.TaskBoardTaskType == TaskBoardTaskType.Received)
            {
                atachmentTask = task.AttachmentsLink;
            }
            else
            {
                atachmentTask = task.AttachmentsLink.ToList();

                if (atachmentTask.Count == 0 && task.ChildTasks.Any())
                {
                    atachmentTask = task.ChildTasks.SelectMany(t => t.AttachmentsLink).ToList();
                }
            }

            return this.MapAttachmentLinksToViewModel(atachmentTask, task);
        }

        /// <summary>
        /// Return attachments view models for task.
        /// </summary>
        /// <param name="taskId">Task ID</param>
        /// <returns></returns>
        public List<AttachmentViewModel> GetAttachmentForTask(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            ICollection<Attachment> atachmentTask = null;

            if (task.TaskBoardTaskType == TaskBoardTaskType.Received)
            {
                atachmentTask = task.Attachments;
            }
            else
            {
                atachmentTask = task.Attachments.ToList();

                if (atachmentTask.Count == 0 && task.ChildTasks.Any())
                {
                    atachmentTask = task.ChildTasks.SelectMany(t => t.Attachments).ToList();
                }
            }

            return this.MapAttachmentsToViewModel(atachmentTask, task);
        }

        /// <summary>
        /// Map attachment entities to view models.
        /// </summary>
        /// <param name="attachments">Attachments collection</param>
        /// <returns></returns>
        private List<AttachmentViewModel> MapAttachmentsToViewModel(IEnumerable<Attachment> attachments, RotationTask task = null)
        {
            List<AttachmentViewModel> attachViewModels = new List<AttachmentViewModel>();

            if (task != null)
            {
                UserProfile taskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(task);

                IQueryable<RotationTask> workItemTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromActiveWorkItems(taskOwner, TaskBoardTaskType.Draft);
                IQueryable<RotationTask> taskBoardDraftTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(taskOwner, TaskBoardTaskType.Draft);
                IQueryable<RotationTask> taskBoardPendingTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(taskOwner, TaskBoardTaskType.Pending);
                List<RotationTask> tasks = new List<RotationTask>();
                if (task.TaskBoardTaskType == TaskBoardTaskType.Received)
                {
                    tasks = (workItemTasks != null ? workItemTasks.Union(taskBoardDraftTasks) : taskBoardDraftTasks).Union(taskBoardPendingTasks).ToList();
                }
                else
                {
                    tasks = (workItemTasks != null ? workItemTasks.Union(taskBoardDraftTasks) : taskBoardDraftTasks).Union(taskBoardPendingTasks).Where(t => t.ChildTasks.Any()).SelectMany(t => t.ChildTasks).ToList();
                }
                //List<RotationTask> tasks = (workItemTasks != null ? workItemTasks.Union(taskBoardDraftTasks) : taskBoardDraftTasks).Union(taskBoardPendingTasks).SelectMany(t=>t.ChildTasks).ToList();

                foreach (Attachment attachment in attachments)
                {
                    if (task.TaskBoardTaskType == TaskBoardTaskType.Received)
                    {
                        var listAttachments = tasks.SelectMany(t => t.Attachments).ToList();

                        var res = listAttachments.Where(t => t.Name == attachment.Name).ToList();

                        attachment.IsReceivedAttachment = res.Count > 0 ? false : true;
                    }
                    else
                    {
                        attachment.IsReceivedAttachment = true;
                    }

                    attachViewModels.Add(new AttachmentViewModel
                    {
                        Id = attachment.Id,
                        Name = attachment.Name,
                        FileName = attachment.FileName,
                        AddedTime = attachment.CreatedUtc.FormatDayMonth(),
                        FileId = attachment.FileId,
                        AbleRemove = attachment.IsReceivedAttachment ?? false,
                        Link = attachment.Link,
                        IsCanEdit = (!task.IsEditable()) ? false : true
                    });
                }
            }
            else
            {
                foreach (Attachment attachment in attachments)
                {
                    attachViewModels.Add(new AttachmentViewModel
                    {
                        Id = attachment.Id,
                        Name = attachment.Name,
                        FileName = attachment.FileName,
                        AddedTime = attachment.CreatedUtc.FormatDayMonth(),
                        FileId = attachment.FileId,
                        Link = attachment.Link,
                        AbleRemove = true
                    });
                }
            }

            return attachViewModels;
        }

        private List<AttachmentViewModel> MapAttachmentLinksToViewModel(IEnumerable<AttachmentsLink> attachmentLinks, RotationTask task = null)
        {
            List<AttachmentViewModel> attachViewModels = new List<AttachmentViewModel>();

            if (task != null)
            {
                UserProfile taskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(task);

                IQueryable<RotationTask> workItemTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromActiveWorkItems(taskOwner, TaskBoardTaskType.Draft);
                IQueryable<RotationTask> taskBoardDraftTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(taskOwner, TaskBoardTaskType.Draft);
                IQueryable<RotationTask> taskBoardPendingTasks = _logicCore.TaskBoardCore.GetTasksByTypeFromTaskBoard(taskOwner, TaskBoardTaskType.Pending);
                List<RotationTask> tasks = new List<RotationTask>();
                if (task.TaskBoardTaskType == TaskBoardTaskType.Received)
                {
                    tasks = (workItemTasks != null ? workItemTasks.Union(taskBoardDraftTasks) : taskBoardDraftTasks).Union(taskBoardPendingTasks).ToList();
                }
                else
                {
                    tasks = (workItemTasks != null ? workItemTasks.Union(taskBoardDraftTasks) : taskBoardDraftTasks).Union(taskBoardPendingTasks).Where(t => t.ChildTasks.Any()).SelectMany(t => t.ChildTasks).ToList();
                }
                //List<RotationTask> tasks = (workItemTasks != null ? workItemTasks.Union(taskBoardDraftTasks) : taskBoardDraftTasks).Union(taskBoardPendingTasks).SelectMany(t=>t.ChildTasks).ToList();

                foreach (AttachmentsLink attachmentLink in attachmentLinks)
                {
                    if (task.TaskBoardTaskType == TaskBoardTaskType.Received)
                    {
                        var listAttachments = tasks.SelectMany(t => t.AttachmentsLink).ToList();

                        var res = listAttachments.Where(t => t.Name == attachmentLink.Name).ToList();

                        attachmentLink.IsReceivedAttachment = res.Count > 0 ? false : true;
                    }
                    else
                    {
                        attachmentLink.IsReceivedAttachment = true;
                    }

                    attachViewModels.Add(new AttachmentViewModel
                    {
                        Id = attachmentLink.Id,
                        Name = attachmentLink.Name,
                        FileName = attachmentLink.Name,
                        AddedTime = attachmentLink.CreatedUtc.FormatDayMonth(),
                        Link = attachmentLink.Link,
                        AbleRemove = attachmentLink.IsReceivedAttachment ?? false,
                        IsCanEdit =  task.IsEditable() ? true : false
                    });
                }
            }
            else
            {
                foreach (AttachmentsLink attachmentLink in attachmentLinks)
                {
                    attachViewModels.Add(new AttachmentViewModel
                    {
                        Id = attachmentLink.Id,
                        Name = attachmentLink.Name,
                        FileName = attachmentLink.Name,
                        AddedTime = attachmentLink.CreatedUtc.FormatDayMonth(),
                        Link = attachmentLink.Link,
                        AbleRemove = true
                    });
                }
            }

            return attachViewModels;
        }

        /// <summary>
        /// Add attachment to topic from view model
        /// </summary>
        /// <param name="attachment">Attachment view model</param>
        /// <returns></returns>
        public void AddAttachmentToTopic(AttachmentViewModel attachment, Guid creatorId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(attachment.TargetId.Value);

            if (attachment.File != null)
            {
                File file = _logicCore.MediaCore.AddFile(attachment.Name, attachment.File.FileName,
                                                        attachment.File.ContentType,
                                                        attachment.File.InputStream,
                                                        false);

                Attachment attachEntity = _logicCore.AttachmentCore.AddAttachment(attachment.Name, attachment.File.FileName, file);

                topic.Attachments.Add(attachEntity);
                _logicCore.RotationTopicCore.UpdateTopic(topic);

                _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(attachment.UserId).FullName}</strong> added the attachment {attachment.Name }.", attachment.UserId);
            }
            else
            {
                AttachmentsLink attachEntity = _logicCore.AttachmentCore.AddAttachmentLink(attachment.Name, attachment.Link);

                topic.AttachmentsLink.Add(attachEntity);
                _logicCore.RotationTopicCore.UpdateTopic(topic);

                _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(attachment.UserId).FullName}</strong> added the attachment {attachment.Name }.", attachment.UserId);
            }
        }

        /// <summary>
        /// Add attachment to task from view model
        /// </summary>
        /// <param name="attachment">Attachment view model</param>
        public void AddAttachmentToTask(AttachmentViewModel attachment, Guid creatorId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(attachment.TargetId.Value);

            if (!task.IsEditable() && !task.IsComplete && creatorId != task.CreatorId)
            {
                attachment.IsCanEdit = false;
                throw new Exception($"Task ID: {task.Id} is not editable");
            }

            if (attachment.File != null)
            {

                File file = _logicCore.MediaCore.AddFile(attachment.Name, attachment.File.FileName,
                                                        attachment.File.ContentType,
                                                        attachment.File.InputStream,
                                                        false);

                Attachment attachEntity = _logicCore.AttachmentCore.AddAttachment(attachment.Name, attachment.File.FileName, file);

                task.Attachments.Add(attachEntity);
                _logicCore.RotationTaskCore.Update(task);
            }
            else
            {
                AttachmentsLink attachEntity = _logicCore.AttachmentCore.AddAttachmentLink(attachment.Name, attachment.Link);

                task.AttachmentsLink.Add(attachEntity);
                _logicCore.RotationTaskCore.Update(task);

            }

            var childTask = task.SuccessorTask;

            while (childTask != null)
            {
                if (attachment.File != null)
                {
                    File file = _logicCore.MediaCore.AddFile(attachment.Name, attachment.File.FileName,
                                                      attachment.File.ContentType,
                                                      attachment.File.InputStream,
                                                      false);

                    Attachment childAttachEntity = _logicCore.AttachmentCore.AddAttachment(attachment.Name, attachment.File.FileName, file);

                    childTask.Attachments.Add(childAttachEntity);
                    _logicCore.RotationTaskCore.Update(childTask);

                    childTask = childTask.SuccessorTask;

                }
                else
                {
                    AttachmentsLink attachEntity = _logicCore.AttachmentCore.AddAttachmentLink(attachment.Name, attachment.Link);

                    childTask.AttachmentsLink.Add(attachEntity);
                    _logicCore.RotationTaskCore.Update(childTask);
                    childTask = childTask.SuccessorTask;

                }

            }

            if (attachment.UserId == task.AssignedToId)
            {
                TaskBoardFilterOptions filterOptions = task.IsComplete ? TaskBoardFilterOptions.CompletedTasks : TaskBoardFilterOptions.IncompletedTasks;

                _logicCore.NotificationCore.NotificationTrigger.Send_AddedAttachment(task.CreatorId.Value, task.AssignedTo.FullName, task.Id, filterOptions);
            }

        }

        /// <summary>
        /// Remove attachment from topic by id.
        /// </summary>
        /// <param name="topicId">Guid TopicId</param>
        /// <param name="attachmentId">Guid AttachmentId</param>
        /// <param name="userId">Guid UserId who deleted attachment from topic</param>
        public void RemoveAttachmentFromTopic(Guid topicId, Guid attachmentId, Guid userId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            if (topic.Attachments.FirstOrDefault(a => a.Id == attachmentId) != null && !topic.Attachments.Any(a => a.Id == attachmentId))
                throw new Exception(string.Format("Topic ID: {0} is not contain attachment with ID: {1}", topic.Id, attachmentId));

            if (topic.Attachments.FirstOrDefault(a => a.Id == attachmentId) != null)
            {
                Attachment attachment = _logicCore.AttachmentCore.GetAttachment(attachmentId);
                _logicCore.AttachmentCore.RemoveAttachment(attachment);

                _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> removed the attachment {attachment.Name}.", userId);
            }

            if (topic.AttachmentsLink.FirstOrDefault(a => a.Id == attachmentId) != null)
            {
                AttachmentsLink attachmentLink = _logicCore.AttachmentCore.GetAttachmentLink(attachmentId);
                _logicCore.AttachmentCore.RemoveAttachmentLink(attachmentLink);

                _logicCore.RotationTopicLogCore.AddMessageToTopicLog(topic, $"<strong>{_logicCore.UserProfileCore.GetUserProfile(userId).FullName}</strong> removed the attachment {attachmentLink.Name}.", userId);
            }
           
        }

        /// <summary>
        /// Remove attachment from task by id.
        /// </summary>
        /// <param name="taskId">Task Id</param>
        /// <param name="attachmentId">Attachment ID</param>
        public void RemoveAttachmentFromTask(Guid taskId, Guid attachmentId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            if (!task.IsEditable())
                throw new Exception($"Task ID: {task.Id} is not editable");

            if (task.Attachments.FirstOrDefault(a => a.Id == attachmentId) != null && !task.Attachments.Any(a => a.Id == attachmentId))
                throw new Exception($"Task ID: {task.Id} is not contain attachment with ID: {attachmentId}");


            if (task.AttachmentsLink.FirstOrDefault(a => a.Id == attachmentId) != null && !task.AttachmentsLink.Any(a => a.Id == attachmentId))
                throw new Exception($"Task ID: {task.Id} is not contain attachment with ID: {attachmentId}");

            if (task.Attachments.FirstOrDefault(a => a.Id == attachmentId) != null)
            {
                Attachment attachment = _logicCore.AttachmentCore.GetAttachment(attachmentId);
                _logicCore.AttachmentCore.RemoveAttachment(attachment);
            }

            if (task.AttachmentsLink.FirstOrDefault(a => a.Id == attachmentId) != null)
            {
                AttachmentsLink attachmentLink = _logicCore.AttachmentCore.GetAttachmentLink(attachmentId);
                _logicCore.AttachmentCore.RemoveAttachmentLink(attachmentLink);
            }

        }
    }
}
