﻿using System;
using AutoMapper;
using System.Linq;
using System.Collections.Generic;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class SharingReportService : BaseService, ISharingReportService
    {
        private readonly IServicesUnitOfWork _services;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;

        public SharingReportService(IServicesUnitOfWork services, LogicCoreUnitOfWork logicCore) : base(services.AuthService)
        {
            this._logicCore = logicCore;
            this._mapper = WebAutoMapperConfig.GetMapper();
            _services = services;
        }

        /// <summary>
        /// Return counter of recipients by sourceId.
        /// </summary>
        /// <param name="sourceId">Source Id</param>
        /// <returns></returns>
        public ShareReportUsersViewModel CountRecipientsBySourceId(Guid sourceId)
        {

            ShareReportUsersViewModel model = new ShareReportUsersViewModel
            {
                 ShareReportCount = _logicCore.SharingReportCore.CountRecipientsBySourceId(sourceId),
                 UsersList = _logicCore.SharingReportCore.GetRecipientsBySourceId(sourceId)
            };

            return model;
        }

        /// <summary>
        /// Populate ViewModel for edit share report dialog.
        /// </summary>
        /// <param name="sourceId">Source Id</param>
        /// <param name="sourceType">Source type</param>
        /// <returns></returns>
        public ShareReportViewModel PopulateShareReportViewModel(Guid sourceId, Models.RotationType sourceType)
        {
            Guid? reportOwnerId = sourceType == Models.RotationType.Shift
                ? _logicCore.ShiftCore.GetShift(sourceId).Rotation.RotationOwnerId
                : _logicCore.RotationCore.GetRotation(sourceId).RotationOwnerId;

            IQueryable<RotationReportSharingRelation> RotationReportSharingRelation = _logicCore.SharingReportCore.GetRotationReportSharingRelations(sourceId);
            List<Guid> reportRelationsUserIds = RotationReportSharingRelation.Select(r => r.RecipientId).Where(u => u != null).Distinct().ToList();

            IOrderedEnumerable<UserProfile> recipients = _logicCore.UserProfileCore.GetRelayEmployees()
                .Where(u => !u.DeletedUtc.HasValue)
                .Where(u => u.Id != reportOwnerId && !reportRelationsUserIds.Contains(u.Id))
                .Where(ApplyUserFilter())
                .ToList()
                .OrderBy(u => u.FullName);

            ShareReportViewModel model = new ShareReportViewModel
            {
                ReportId = sourceId,
                ReportType = sourceType,
                ReportRelations = _mapper.Map<IEnumerable<ShareReportRelationViewModel>>(RotationReportSharingRelation),
                Recipients = _mapper.Map<IEnumerable<SelectListItem>>(recipients),
            };

            return model;
        }

        /// <summary>
        /// Change share report relations
        /// </summary>
        /// <param name="model">View model of share report relations dialog</param>
        public void ChangeShareReportRelations(ShareReportViewModel model, Guid ownerId)
        {
            _logicCore.SharingReportCore.ChangeShareReportRelations(model.ReportId, model.ReportType, model.RecipientsIds, model.DeleteRelationsIds);

            var topicOwnerName = _logicCore.UserProfileCore.GetUserProfile(ownerId);

            if (model.RecipientsIds != null)
            {
                foreach (var recipientId in model.RecipientsIds)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_SharedReportWithYou(recipientId, topicOwnerName.FirstName, topicOwnerName.LastName);
                }
            }

        }
    }
}
