﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Interfaces.Services;
using AutoMapper;
using Microsoft.AspNet.Identity;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.Mapper;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;
using MvcPaging;
using System.Data.Entity;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class TopicSearchService : ITopicSearchService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;

        public TopicSearchService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
        }

        public List<SearchBarItem> GetTopicPeriod(int selectedPeriod)
        {
            var topicGroups = _logicCore.TopicGroupCore.GetBaseTopicGroups();
            DateTime now = DateTime.Now;
            if (selectedPeriod == 1)
            {
                var last24h = DateTime.UtcNow.AddHours(-24);
                topicGroups = topicGroups.Where(t => t.CreatedUtc.Value > last24h && t.CreatedUtc.Value < now).AsQueryable();
            }
            else if(selectedPeriod == 2)
            {
                var last48h = DateTime.UtcNow.AddHours(-48);
                topicGroups = topicGroups.Where(t => t.CreatedUtc.Value > last48h && t.CreatedUtc.Value < now).AsQueryable();
            }
            else if (selectedPeriod == 3)
            {
                var last7days = DateTime.UtcNow.AddDays(-7);
                topicGroups = topicGroups.Where(t => t.CreatedUtc.Value > last7days && t.CreatedUtc.Value < now).AsQueryable();
            }
            else if (selectedPeriod == 4)
            {
                var lastMonths = DateTime.UtcNow.AddMonths(-1);
                topicGroups = topicGroups.Where(t => t.CreatedUtc.Value > lastMonths && t.CreatedUtc.Value < now).AsQueryable();
            }
            else if (selectedPeriod == 5)
            {
                var last6Months = DateTime.UtcNow.AddMonths(-6);
                topicGroups = topicGroups.Where(t => t.CreatedUtc.Value > last6Months && t.CreatedUtc.Value < now).AsQueryable();
            }

            return topicGroups.Select(topic => new SearchBarItem
            {
                Id = topic.Id,
                Name = topic.Name
            }).ToList();

        }


        protected List<string> _nameTopics;

        protected List<UserProfile> _users;

        public List<SearchBarItem> GetBaseTopicGroups()
        {
            var topicGroups = _logicCore.TopicGroupCore.GetBaseTopicGroups();

            return topicGroups.Select(topic => new SearchBarItem
            {
                Id = topic.Id,
                Name = topic.Name
            }).ToList();
        }

        //--------------------------------------------SearchTopicNew-------------------------------------------------------

        public bool IsContainsSearchText(string searchText, string nameTopic)
        {
            if (searchText != null && nameTopic != null)
            {
                if (nameTopic.Replace(" ", string.Empty).ToLower().Contains(searchText.Replace(" ", string.Empty).ToLower()))
                {
                    return true;
                }
            }
            return false;
        }

        public List<RotationTopic> GetTopicsBySearctText(Guid currentUserId, SearchViewModel model)
        {
            var userRoles = _services.AuthService.GetUserRoles(currentUserId);

            List<RotationTopic> topics = null;

            if (userRoles.Any(r => r == iHandoverRoles.Relay.HeadLineManager || r == iHandoverRoles.Relay.LineManager || r == iHandoverRoles.Relay.SafetyManager))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.LineManagerId == currentUserId
                                                                                && rotation.State != RotationState.Created).AsNoTracking().Select(rotation => rotation.RotationOwnerId)
                                                                                .GroupBy(id => id)
                                                                                .Select(group => group.FirstOrDefault()).ToList();

                topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(model.SearchText, usersUnderSupervisionIds.ToArray());
            }

            if (userRoles.Any(r => r == iHandoverRoles.Relay.User))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.RotationOwnerId == currentUserId
                                                                                && rotation.State != RotationState.Created).AsNoTracking().Select(rotation => rotation.RotationOwnerId)
                                                                                .GroupBy(id => id)
                                                                                .Select(group => group.FirstOrDefault()).ToList();

                topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(model.SearchText, usersUnderSupervisionIds.ToArray());
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(model.SearchText);
            }

            else
            {
                throw new Exception("User access denied!");
            }

            return topics;
        }


        public SearchResultsViewModel SearchTopics(Guid currentUserId, SearchViewModel model)
        {
            var topics = this.GetTopicsBySearctText(currentUserId, model);

            var result = new List<SearchResultsItem>();

            var page = model.PageNumber - 1 ?? 0;
            var pageSize = model.PageSize ?? NumericConstants.Paginator.PaginatorPageSizeForTopicSearch;

            model.PageNumber = page;
            model.PageSize = pageSize;

            this._users = _logicCore.RotationCore.GetAllActiveRotations().Select(t => t.RotationOwner).ToList();

            if (topics != null)
            {
                this._nameTopics = topics.Select(t => t.RotationTopicGroup.Name).Distinct().ToList();

                if (model.FilterUserId.HasValue)
                {
                    topics = topics.Where(t => _logicCore.RotationTopicCore.GetTopicOwner(t).Id == model.FilterUserId.Value).ToList();
                }

                if (model.SelectedTopicName != null)
                {
                    topics = topics.Where(t => t.RotationTopicGroup.Name == model.SelectedTopicName).ToList();
                }
 
                topics.Distinct().ToList().ForEach(topic =>
                {
                    var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);
                    var topicName = topic.RelationId.HasValue ? topic.RelationId != Guid.Empty ? _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName : topic.RotationTopicGroup.Name : topic.RotationTopicGroup.Name;
                    var isHaveTopicInList = result.FirstOrDefault(t => t.CreatorId == topicCreator.Id && t.Name == topicName && t.Reference == topic.Name);

                    if (topicCreator.DeletedUtc == null && isHaveTopicInList == null)

                        result.Add(new SearchResultsItem
                        {
                            Id = topic.Id,
                            Name = topicName,
                            CreatorId = topicCreator.Id,
                            CreatorName = topicCreator.FullName,
                            Notes = topic.Description,
                            Reference = topic.Name,
                            CreatedTime = topic.CreatedUtc != null ? topic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time(),
                            CreatedDate = topic.CreatedUtc != null ? topic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear(),
                            SearchTags = topic.SearchTags,
                            FirstTagTopic = topic.SearchTags != null ? topic.SearchTags.Split(',').FirstOrDefault() : null,
                            MoreTags = topic.SearchTags != null ? topic.SearchTags.Split(',').Count() > 1 ? true : false : false,
                            AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                            ManagerCommentsCounter = topic.ManagerComment.Count,
                            RotationId = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? topic.RotationTopicGroup.RotationModule.RotationId : topic.RotationTopicGroup.RotationModule.ShiftId,
                            RotationType = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? Models.RotationType.Swing : Models.RotationType.Shift
                        });
                });
            }

            model.UsersList = _mapper.Map<IEnumerable<DropDownListItemModel>>(_users);

            model.NameTopics = _nameTopics.Select(t => new DropDownListItemModel
            {
                Text = t,
                Value = t
            });

            SearchResultsViewModel searchResultViewModel = new SearchResultsViewModel
            {
                ResultItems = result.ToPagedList(page, pageSize, result.Count),
                Params = model
            };

            return searchResultViewModel;
        }

        public List<RotationTopic> GetTopicMetricsBySearchText(Guid currentUserId, SearchViewModel model)
        {
            var userRoles = _services.AuthService.GetUserRoles(currentUserId);

            List<RotationTopic> topics = null;

            if (userRoles.Any(r => r == iHandoverRoles.Relay.HeadLineManager || r == iHandoverRoles.Relay.LineManager || r == iHandoverRoles.Relay.SafetyManager))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.LineManagerId == currentUserId
                                                                                && rotation.State != RotationState.Created).Select(rotation => rotation.RotationOwnerId)
                                                                                .GroupBy(id => id)
                                                                                .Select(group => group.FirstOrDefault());

                topics = _logicCore.TopicGroupCore.FindMetricsTopicGroupsTopics(model.SearchText, usersUnderSupervisionIds.ToArray());
            }

            if (userRoles.Any(r => r == iHandoverRoles.Relay.User))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.RotationOwnerId == currentUserId
                                                                                && rotation.State != RotationState.Created).Select(rotation => rotation.RotationOwnerId)
                                                                                .GroupBy(id => id)
                                                                                .Select(group => group.FirstOrDefault()).ToList();

                topics = _logicCore.TopicGroupCore.FindMetricsTopicGroupsTopics(model.SearchText, usersUnderSupervisionIds.ToArray());
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                topics = _logicCore.TopicGroupCore.FindMetricsTopicGroupsTopics(model.SearchText);
            }

            else
            {
                throw new Exception("User access denied!");
            }



            return topics;
        }

        public SearchResultsViewModel SearchMetricsTopics(Guid currentUserId, SearchViewModel model)
        {
            var topics = this.GetTopicMetricsBySearchText(currentUserId, model);

            var result = new List<SearchResultsItem>();

            var page = model.PageNumber - 1 ?? 0;
            var pageSize = model.PageSize ?? NumericConstants.Paginator.PaginatorPageSizeForTopicSearch;

            model.PageNumber = page;
            model.PageSize = pageSize;

            this._users = _logicCore.RotationCore.GetAllActiveRotations().Select(t => t.RotationOwner).ToList();

            if (topics != null)
            {
                this._nameTopics = topics.Select(t => t.RotationTopicGroup.Name).Distinct().ToList();

                if (model.FilterUserId.HasValue)
                {
                    topics = topics.Where(t => _logicCore.RotationTopicCore.GetTopicOwner(t).Id == model.FilterUserId.Value).ToList();
                }

                if (model.SelectedTopicName != null)
                {
                    topics = topics.Where(t => t.RotationTopicGroup.Name == model.SelectedTopicName).ToList();
                }

                topics.ToList().ForEach(topic =>
                {
                    var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);
                    var topicName = topic.RelationId.HasValue ? topic.RelationId != Guid.Empty ? _logicCore.UserProfileCore.GetUserProfile(topic.RelationId.Value).FullName : topic.RotationTopicGroup.Name : topic.RotationTopicGroup.Name;
                    var isHaveTopicInList = result.FirstOrDefault(t => t.CreatorId == topicCreator.Id && t.Name == topicName && t.Reference == topic.Name);

                    topic = SetTopicPVA(topic);

                    if (isHaveTopicInList == null)

                        result.Add(new SearchResultsItem
                        {
                            Id = topic.Id,
                            Name = topicName,
                            CreatorId = topicCreator.Id,
                            CreatorName = topicCreator.FullName,
                            Notes = topic.Description,
                            Reference = topic.Name,
                            CreatedTime = topic.CreatedUtc != null ? topic.CreatedUtc.FormatTo24Time() : topic.ModifiedUtc.FormatTo24Time(),
                            CreatedDate = topic.CreatedUtc != null ? topic.CreatedUtc.FormatDayMonthYear() : topic.ModifiedUtc.FormatDayMonthYear(),
                            Actual = topic.Actual,
                            Planned = topic.Planned,
                            UnitsSelectedType = topic.UnitsSelectedType,
                            Variance = topic.Variance,
                            AttachmentsCounter = topic.Attachments.Count + topic.AttachmentsLink.Count,
                            ManagerCommentsCounter = topic.ManagerComment.Count,
                            RotationId = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? topic.RotationTopicGroup.RotationModule.RotationId : topic.RotationTopicGroup.RotationModule.ShiftId,
                            RotationType = topic.RotationTopicGroup.RotationModule.RotationId.HasValue ? Models.RotationType.Swing : Models.RotationType.Shift
                        });
                });
            }

            model.UsersList = _mapper.Map<IEnumerable<DropDownListItemModel>>(_users);
            model.NameTopics = _nameTopics.Select(t => new DropDownListItemModel
            {
                Text = t,
                Value = t
            });

            SearchResultsViewModel searchResultViewModel = new SearchResultsViewModel
            {
                ResultItems = result.ToPagedList(page, pageSize, result.Count),
                Params = model
            };

            return searchResultViewModel;
        }

        public RotationTopic SetTopicPVA(RotationTopic topic)
        {
            var variance = string.Empty;

            string units = topic.UnitsSelectedType != null && topic.UnitsSelectedType != string.Empty ? _services.SearchService.GetUnitsPvA().FirstOrDefault(u => u.Value == topic.UnitsSelectedType).Text : string.Empty;

            if (topic.Actual != null && topic.Actual != string.Empty && topic.Planned != null && topic.Planned != string.Empty)
            {
                variance = (float.Parse(topic.Actual) - float.Parse(topic.Planned)).ToString() + " " + units;

            }
            else if (topic.Actual != null && topic.Actual != string.Empty && topic.Planned == null || topic.Actual != null && topic.Actual != string.Empty && topic.Planned == string.Empty)
            {
                variance = topic.Actual + " " + units;
            }
            else if (topic.Actual == null && topic.Planned != null && topic.Planned != string.Empty || topic.Actual == string.Empty && topic.Planned != null && topic.Planned != string.Empty)
            {
                variance = "-" + topic.Planned + " " + units;
            }

            if (topic.Actual != null && topic.Actual != string.Empty)
            {
                topic.Actual = topic.Actual + " " + units;
            }
            else
            {
                topic.Actual = "-";
            }

            if (topic.Planned != null && topic.Planned != string.Empty)
            {
                topic.Planned = topic.Planned + " " + units;
            }
            else
            {
                topic.Planned = "-";
            }

            topic.Variance = variance != null && variance != "" ? variance : "-";

            return topic;
        }

        //----------------------------------------------------------------------------------------------------------------

        public TopicSearchResultViewModel SearchTopics(Guid currentUserId, TopicSearchViewModel model)
        {
            var userRoles = _services.AuthService.GetUserRoles(currentUserId);

            IQueryable<RotationTopic> topics = null;

            if (userRoles.Any(r => r == iHandoverRoles.Relay.HeadLineManager || r == iHandoverRoles.Relay.LineManager || r == iHandoverRoles.Relay.SafetyManager || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                var usersUnderSupervisionIds = _repositories.RotationRepository.Find(rotation => rotation.LineManagerId == currentUserId
                                                                                && rotation.State != RotationState.Created).Select(rotation => rotation.RotationOwnerId)
                                                                                .GroupBy(id => id)
                                                                                .Select(group => group.FirstOrDefault());

                topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(model.SelectedTopicGroupsIds.ToArray(), usersUnderSupervisionIds.ToArray());
            }

            else if (userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin || r == iHandoverRoles.Relay.ExecutiveUser))
            {
                topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(model.SelectedTopicGroupsIds.ToArray());
            }

            else
            {
                throw new Exception("User access denied!");
            }


            var result = new List<TopicSearchResultItem>();

            var page = model.PageNumber - 1 ?? 0;
            var pageSize = model.PageSize ?? NumericConstants.Paginator.PaginatorPageSizeForTopicSearch;

            model.PageNumber = page;
            model.PageSize = pageSize;

            if (topics != null)
            {

                DateTime now = DateTime.UtcNow;
                if (model.SelectedPeriod == 1)
                {
                    var last24h = DateTime.UtcNow.AddHours(-24);
                    topics = topics.Where(t => t.CreatedUtc.Value > last24h && t.CreatedUtc.Value < now).AsQueryable();
                }
                else if (model.SelectedPeriod == 2)
                {
                    var last48h = DateTime.UtcNow.AddHours(-48);
                    topics = topics.Where(t => t.CreatedUtc.Value > last48h && t.CreatedUtc.Value < now).AsQueryable();
                }
                else if (model.SelectedPeriod == 3)
                {
                    var last7days = DateTime.UtcNow.AddDays(-7);
                    topics = topics.Where(t => t.CreatedUtc.Value > last7days && t.CreatedUtc.Value < now).AsQueryable();
                }
                else if (model.SelectedPeriod == 4)
                {
                    var lastMonths = DateTime.UtcNow.AddMonths(-1);
                    topics = topics.Where(t => t.CreatedUtc.Value > lastMonths && t.CreatedUtc.Value < now).AsQueryable();
                }
                else if (model.SelectedPeriod == 5)
                {
                    var last6Months = DateTime.UtcNow.AddMonths(-6);
                    topics = topics.Where(t => t.CreatedUtc.Value > last6Months && t.CreatedUtc.Value < now).AsQueryable();
                }


                foreach (var topic in topics.OrderByDescending(t => t.CreatedUtc))
                {
                    var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);

                    result.Add(new TopicSearchResultItem
                    {
                        Id = topic.Id,
                        Name = topic.RotationTopicGroup.Name,
                        CreatorId = topicCreator.Id,
                        CreatorName = topicCreator.FullName,
                        Notes = topic.Description,
                        Reference = topic.Name,
                        CreatedTime = topic.CreatedUtc.FormatTo24Time(),
                        CreatedDate = topic.CreatedUtc.FormatDayMonthYear(),
                    });
                }
            }

            TopicSearchResultViewModel searchResultViewModel = new TopicSearchResultViewModel
            {
                ResultItems = result.ToPagedList(page, pageSize, result.Count),
                Params = model
            };

            return searchResultViewModel;
        }

        public TopicReportPdfViewModel PopulateTopicSearchReportViewModel(IEnumerable<Guid?> selectedTopicGroupsIds, IEnumerable<Guid?> selectedUserIds, TopicSearchDateRangeType? dateRangeType, int selectedPeriod = 0)
        {
            List<RotationTopic> topics = _logicCore.TopicGroupCore.FindTopicGroupsTopics(selectedTopicGroupsIds.ToArray()).OrderByDescending(t => t.CreatedUtc).ToList();

            if (selectedPeriod != 0)
            {
                DateTime now = DateTime.Now;
                if (selectedPeriod == 1)
                {
                    var last24h = DateTime.UtcNow.AddHours(-24);
                    topics = topics.Where(t => t.CreatedUtc.Value > last24h && t.CreatedUtc.Value < now).ToList();
                }
                else if (selectedPeriod == 2)
                {
                    var last48h = DateTime.UtcNow.AddHours(-48);
                    topics = topics.Where(t => t.CreatedUtc.Value > last48h && t.CreatedUtc.Value < now).ToList();
                }
                else if (selectedPeriod == 3)
                {
                    var last7days = DateTime.UtcNow.AddDays(-7);
                    topics = topics.Where(t => t.CreatedUtc.Value > last7days && t.CreatedUtc.Value < now).ToList();
                }
                else if (selectedPeriod == 4)
                {
                    var lastMonths = DateTime.UtcNow.AddMonths(-1);
                    topics = topics.Where(t => t.CreatedUtc.Value > lastMonths && t.CreatedUtc.Value < now).ToList();
                }
                else if (selectedPeriod == 5)
                {
                    var last6Months = DateTime.UtcNow.AddMonths(-6);
                    topics = topics.Where(t => t.CreatedUtc.Value > last6Months && t.CreatedUtc.Value < now).ToList();
                }
            }

            List<TemplateTopicGroup> templateTopicGroups = (from selectedTopicGroupsId 
                                                            in selectedTopicGroupsIds
                                                            where selectedTopicGroupsId.HasValue
                                                            select _logicCore.TopicGroupCore.GetTopicGroup((Guid) selectedTopicGroupsId)).ToList();


            TopicReportPdfViewModel viewModel = new TopicReportPdfViewModel
            {
                Topics = new List<TopicSearchResultItem>(),
                ReportDate = DateTime.UtcNow.FormatDayMonthYear().Trim(),
                CompanyLogoImageId = _logicCore.AdministrationCore.GetCompanyLogoImageId(),
                FileLogo = _logicCore.AdministrationCore.GetAdminSettings().Logo,
                TopicsInSearch = templateTopicGroups.Any() ? string.Join(", ", templateTopicGroups.Where(tg => tg != null).Select(tg => tg.Name)) : " - "
            };


            foreach (var topic in topics)
            {
                var topicCreator = _logicCore.RotationTopicCore.GetTopicOwner(topic);

                viewModel.Topics.Add(new TopicSearchResultItem
                {
                    Id = topic.Id,
                    Name = topic.Name,
                    CreatorId = topicCreator.Id,
                    CreatorName = topicCreator.FullName,
                    Notes = string.IsNullOrWhiteSpace(topic.Description) ? string.Empty : topic.Description.Trim(),
                    Reference = string.IsNullOrWhiteSpace(topic.RotationTopicGroup.Name) ? string.Empty : topic.RotationTopicGroup.Name,
                    CreatedTime = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatTo24Time()) ? string.Empty : topic.CreatedUtc.FormatTo24Time().Trim(),
                    CreatedDate = string.IsNullOrWhiteSpace(topic.CreatedUtc.FormatDayMonthYear()) ? string.Empty : topic.CreatedUtc.FormatDayMonthYear().Trim()
                });
            }

            return viewModel;
        }
    }
}
