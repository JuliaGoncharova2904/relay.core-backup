﻿using MomentumPlus.Relay.Interfaces.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Models;
using MomentumPlus.Core.Interfaces;
using AutoMapper;
using MomentumPlus.Relay.BLL.Mapper;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces;
using RotationType = MomentumPlus.Core.Models.RotationType;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.BLL.Services.Web
{
    public class RotationService : BaseService, IRotationService
    {
        private readonly IRepositoriesUnitOfWork _repositories;
        private readonly IMapper _mapper;
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IServicesUnitOfWork _services;
        private readonly IAuthService _authService;

        public RotationService(LogicCoreUnitOfWork logicCore, IServicesUnitOfWork services, IAuthService authService)
            : base(authService)
        {
            this._repositories = logicCore.Repositories;
            this._mapper = WebAutoMapperConfig.GetMapper();
            this._logicCore = logicCore;
            this._services = services;
            this._authService = authService;
        }

        public EditRotationViewModel PopulateEditRotationModel(EditRotationViewModel model, Guid userId)
        {
            IQueryable<UserProfile> allEmployees = _logicCore.UserProfileCore.GetRelayEmployees().Where(u => !u.DeletedUtc.HasValue).Where(ApplyUserFilter());

            IQueryable<UserProfile> lineManagers = _logicCore.UserProfileCore.GetEmployeesByIds(
                                                        _authService.GetUsersIdsByRoles(new[] {
                                                            iHandoverRoles.Relay.HeadLineManager,
                                                            iHandoverRoles.Relay.iHandoverAdmin,
                                                            iHandoverRoles.Relay.SafetyManager,
                                                            iHandoverRoles.Relay.Administrator,
                                                            iHandoverRoles.Relay.LineManager
                                                        }, userId)
                                                    ).Where(u => !u.DeletedUtc.HasValue);


            lineManagers = lineManagers.Where(ApplyUserFilter());

            if (lineManagers != null)
            {
                model.LineManagers = lineManagers.ToList()
                                                    .Select(u => new SelectListItem
                                                    {
                                                        Value = u.Id.ToString(),
                                                        Text = u.FullName
                                                    });
            }

            Guid rotationOwnerId = Guid.Parse(model.RotationOwnerId);

            model.Contributors = _logicCore.TeamCore.GetAllTeams().SelectMany(u => u.Users).Where(u => !u.DeletedUtc.HasValue).Where(u => u.Id != rotationOwnerId).Where(c=> c.FullName != "Admin iHandover").OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                                .Select(e => new SelectListItem
                                                {
                                                    Text = e.FirstName + " " + e.LastName,
                                                    Value = e.Id.ToString()
                                                });

            var currentRotation = _repositories.RotationRepository.Get(model.Id);

            if (currentRotation.RotationType == RotationType.Shift)
            {
                model.BackToBackEmployees = _logicCore.TeamCore.GetAllTeams().SelectMany(t => t.Users)
                                                                   .Where(user => user.CurrentRotationId.HasValue
                                                                                   && user.CurrentRotation.RotationType == Core.Models.RotationType.Shift)
                                                                                   .Select(e => new SelectListItem
                                                                                   {
                                                                                       Text = e.FirstName + " " + e.LastName,
                                                                                       Value = e.Id.ToString()
                                                                                   });
            }
            else
            {
                model.BackToBackEmployees = _logicCore.TeamCore.GetAllTeams().SelectMany(t => t.Users)
                                                                   .Where(user => user.CurrentRotationId.HasValue
                                                                                   && user.CurrentRotation.RotationType == Core.Models.RotationType.Swing)
                                                                                   .Select(e => new SelectListItem
                                                                                   {
                                                                                       Text = e.FirstName + " " + e.LastName,
                                                                                       Value = e.Id.ToString()
                                                                                   });
            }

            var adminIdOfSubscription = _authService.GetIdAdminOfSubscription();

            if (adminIdOfSubscription != null)
            {
                var adminOfSubscription = _logicCore.UserProfileCore.GetUserProfile(adminIdOfSubscription.Value);

                if (adminOfSubscription != null)
                {
                    model.BackToBackEmployees = model.BackToBackEmployees.Concat(new[] { new SelectListItem
                                                                                         {
                                                                                             Text = adminOfSubscription.FirstName + " " + adminOfSubscription.LastName,
                                                                                             Value = adminOfSubscription.Id.ToString()
                                                                                         } });
                }
            }

            model.RotationType = currentRotation != null
                                                ? (Models.RotationType)currentRotation.RotationType
                                                : Models.RotationType.Swing;

            model.ShiftPatterns = _services.RotationPatternService.GetShiftPatternsList();

            return model;
        }

        public CreateRotationViewModel PopulateEditRotationModel(CreateRotationViewModel model, Guid userId)
        {
            Workplace site = null;
            Position position = null;

            IQueryable<Project> allProjects = _repositories.ProjectRepository.GetAll();

            IQueryable<UserProfile> allEmployees = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter()).Where(UserActiveFilter());

            IQueryable<UserProfile> newEmployees = _logicCore.UserProfileCore.GetEmployeesWithoutRotation(allEmployees);
            IQueryable<UserProfile> lineManagers = _logicCore.UserProfileCore.GetEmployeesByIds(
                                                        _authService.GetUsersIdsByRoles(new[] {
                                                            iHandoverRoles.Relay.HeadLineManager,
                                                            iHandoverRoles.Relay.iHandoverAdmin,
                                                            iHandoverRoles.Relay.SafetyManager,
                                                            iHandoverRoles.Relay.Administrator,
                                                            iHandoverRoles.Relay.LineManager
                                                        }, userId)
                                                    );

            lineManagers = lineManagers.Where(u => !u.DeletedUtc.HasValue);
            allEmployees = allEmployees.Where(u => !u.DeletedUtc.HasValue);
            newEmployees = newEmployees.Where(u => !u.DeletedUtc.HasValue);


            model.Contributors = allEmployees.OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                        .Select(e => new SelectListItem
                                        {
                                            Text = e.FirstName + " " + e.LastName,
                                            Value = e.Id.ToString()
                                        }).ToList();

            //model.Contributors = allEmployees.ToList().Where(u => !_authService.GetUserRoles(u.Id).Contains(iHandoverRoles.Relay.LineManager) &&
            //                                             !_authService.GetUserRoles(u.Id).Contains(iHandoverRoles.Relay.HeadLineManager) &&
            //                                             !_authService.GetUserRoles(u.Id).Contains(iHandoverRoles.Relay.SafetyManager) &&
            //                                             !_authService.GetUserRoles(u.Id).Contains(iHandoverRoles.Relay.Administrator)).Select(e => new SelectListItem
            //                                             {
            //                                                 Text = e.FirstName + " " + e.LastName,
            //                                                 Value = e.Id.ToString()
            //                                             }).ToList(); 

            model.LineManagers = lineManagers.Where(ApplyUserFilter()).Where(UserActiveFilter()).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                      .Select(e => new SelectListItem
                                      {
                                          Text = e.FirstName + " " + e.LastName,
                                          Value = e.Id.ToString()
                                      }).ToList();

            model.Projects = allProjects.OrderBy(u => u.Name).Select(p => new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();

            if (model.PositionId.HasValue && (position = _repositories.PositionRepository.Get(model.PositionId.Value)) != null)
            {
                model.RotationOwnerEmployees = newEmployees.Where(c => c.PositionId == model.PositionId.Value)
                                                            .OrderBy(u => u.FirstName)
                                                            .ThenBy(u => u.LastName)
                                                            .Select(e => new SelectListItem
                                                            {
                                                                Text = e.FirstName + " " + e.LastName,
                                                                Value = e.Id.ToString()
                                                            }).ToList();

                //    model.BackToBackEmployees = model.Contributors;

               
                model.BackToBackEmployees = model.RotationType == Models.RotationType.Shift ? allEmployees.Where(u => u.CurrentRotation.RotationType == RotationType.Shift).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                                    .Select(e => new SelectListItem
                                                    {
                                                        Text = e.FirstName + " " + e.LastName,
                                                        Value = e.Id.ToString()
                                                    }) : allEmployees.Where(u => u.CurrentRotation.RotationType == RotationType.Swing).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                                    .Select(e => new SelectListItem
                                                    {
                                                        Text = e.FirstName + " " + e.LastName,
                                                        Value = e.Id.ToString()
                                                    });


                model.PositionName = position.Name;
                model.PositionId = position.Id;

                model.SiteName = position.Workplace.Name;
                model.SiteId = position.Workplace.Id;
                model.ShiftPatterns = _services.RotationPatternService.GetShiftPatternsList();
            }
            else if (model.SiteId.HasValue && (site = _repositories.WorkplaceRepository.Get(model.SiteId.Value)) != null)
            {
                model.RotationOwnerEmployees = allEmployees.Where(c => c.Position.WorkplaceId == model.SiteId.Value)
                                                            .OrderBy(u => u.FirstName)
                                                            .ThenBy(u => u.LastName)
                                                            .Select(e => new SelectListItem
                                                            {
                                                                Text = e.FirstName + " " + e.LastName,
                                                                Value = e.Id.ToString()
                                                            }).ToList();

                model.BackToBackEmployees = new List<SelectListItem>();

                model.SiteName = site.Name;
                model.SiteId = site.Id;
            }
            else
            {
                model.RotationOwnerEmployees = newEmployees.OrderBy(u => u.FirstName)
                                                            .ThenBy(u => u.LastName)
                                                            .Select(e => new SelectListItem
                                                            {
                                                                Text = e.FirstName + " " + e.LastName,
                                                                Value = e.Id.ToString()
                                                            }).ToList();

                model.BackToBackEmployees = new List<SelectListItem>();
                model.ShiftPatterns = _services.RotationPatternService.GetShiftPatternsList();
            }

            return model;
        }

        public IEnumerable<ReceivedHistorySlideViewModel> GetRotationsReceivedHistorySlidesByUser(Guid userId)
        {
            List<ReceivedHistorySlideViewModel> rotationsViewModel = new List<ReceivedHistorySlideViewModel>();
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllUserRotations(userId, true)
                                                                        .OrderByDescending(r => r.CreatedUtc.Value)
                                                                        .ToList();
            foreach (var rotation in rotations)
            {
                var resultRotationsReceiveds = GetRotationsWhoPopulateMyReceivedSection(rotation.Id).ToList();

                int counter = 0;

                foreach (var resultRotationsReceived in resultRotationsReceiveds)
                {
                    counter += resultRotationsReceived.TopicCounter;
                }

                rotationsViewModel.Add(new ReceivedHistorySlideViewModel
                {
                    Id = rotation.Id,
                    // Date = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDayMonthYear(),
                    Date = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn > 0 ? rotation.DayOn - 1 : rotation.DayOn).FormatWithDayMonthYear(),
                    ReceivedFrom = String.Join(" / ", rotation.HandoverFromRotations.Where(r => r.RotationOwnerId != userId).Select(r => r.RotationOwner.FullName)),
                    ReceivedTopicCounter = counter,
                    //ReceivedTopicCounter = _logicCore.RotationTopicCore.GetAllRotationTopic(rotation.Id, TypeOfModuleSource.Received).Count(t => !t.ShareSourceTopicId.HasValue),
                    //ReceivedTaskCounter = _logicCore.RotationTaskCore.GetAllRotationTasks(rotation.Id, TypeOfModuleSource.Received).Count()
                    ReceivedTaskCounter = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotation.Id, ModuleSourceType.Received).Count()
                });

            }

            return rotationsViewModel;
        }

        public IEnumerable<RotationHistorySlideViewModel> GetRotationsHistorySlidesByUser(Guid userId)
        {
            List<RotationHistorySlideViewModel> rotationsViewModel = new List<RotationHistorySlideViewModel>();
            List<Rotation> rotations = _logicCore.RotationCore.GetAllUserRotations(userId, true)
                                                                        .OrderByDescending(r => r.CreatedUtc.Value)
                                                                        .ToList();

            Rotation firstRotation = rotations.FirstOrDefault();

            if (firstRotation != null && firstRotation.State != RotationState.SwingEnded && firstRotation.State != RotationState.Expired)
            {
                rotations.Remove(firstRotation);
            }

            foreach (var rotation in rotations)
            {
                rotationsViewModel.Add(new RotationHistorySlideViewModel
                {
                    Id = rotation.Id,
                    Date = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn > 0 ? rotation.DayOn - 1 : rotation.DayOn).FormatWithDayMonthYear(),
                    Contributors = String.Join(" / ", rotation.RotationOwner.Contributors.Select(u => u.FullName)),
                    //String.Join(" / ", rotation.Contributors.Select(u => u.FullName)),
                    HandoverTo = rotation.DefaultBackToBack.FullName,
                    DraftTopicCounter = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft).SelectMany(m => m.RotationTopicGroups)
                                                                                            .SelectMany(tg => tg.RotationTopics)
                                                                                            .Count(t => t.Enabled),
                    //DraftTaskCounter = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft).SelectMany(m => m.RotationTopicGroups)
                    //                                                                        .SelectMany(tg => tg.RotationTopics)
                    //                                                                        .SelectMany(t => t.RotationTasks)
                    //                                                                        .Count(t => t.Enabled)

                    DraftTaskCounter = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotation.Id, ModuleSourceType.Draft).Count(),
                    ContributorsList = rotation.RotationOwner.Contributors.Select(t =>
                    new Models.DropDownListItemModel
                    {
                        Text = t.FullName,
                        Value = t.Id.ToString()
                    }).ToList()
                });
            }

            return rotationsViewModel;
        }

        public Rotation GetCurrentRotationForUser(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            return user.CurrentRotation;
        }

        public Guid? GetCurrentRotationIdForUser(Guid userId)
        {
            UserProfile user = _logicCore.UserProfileCore.GetUserProfile(userId);

            return user.CurrentRotationId;
        }

        public List<SearchBarItem> GetTagsOrTopicsForRotation(Guid userId)
        {
            var topicGroups = _repositories.RotationTopicGroupRepository.Find(tg => tg.ParentRotationTopicGroupId.HasValue).OrderBy(tg => tg.Name).ToList();

            var tags = _repositories.RotationTopicGroupRepository.Find(tg => tg.ParentRotationTopicGroupId.HasValue)
                                                                 .SelectMany(t => t.RotationTopics)
                                                                 .Where(ts => ts.SearchTags != null).ToList();

            List<SearchBarItem> tagsTopicGroupsResult = new List<SearchBarItem>();

            foreach (var tag in tags.SelectMany(ts => ts.SearchTags.ToString().Split(',')).ToList())
            {
                tagsTopicGroupsResult.Add(new SearchBarItem { Id = Guid.NewGuid(), Name = tag.ToString() });
            }

            foreach (var topicGroup in topicGroups)
            {
                tagsTopicGroupsResult.Add(new SearchBarItem { Id = topicGroup.Id, Name = topicGroup.Name });
            }

            return tagsTopicGroupsResult.ToList();
        }

        public SummaryViewModel GetSummaryForRotation(Guid rotationId, Guid currentUserId, bool viewMode)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            SummaryViewModel model = new SummaryViewModel
            {
                RotationId = rotationId,
                CurrentShiftId = rotation.RotationType == RotationType.Shift ? _services.ShiftService.GetCurrentShiftIdForRotation(rotationId) : null,
                RotarionOwnerId = rotation.RotationOwnerId,
                IsSwingExpired = rotation.State == RotationState.SwingEnded,
                RotationOwnerFullName = rotation.RotationOwner.FullName,
                IsRotationExpiredOrNotStarted = !rotation.StartDate.HasValue || rotation.State == RotationState.Expired,
                IsViewMode = viewMode,
                IsShiftRotation = rotation.RotationType == RotationType.Shift
            };

            return model;
        }

        public SummaryViewModel GetSummaryForRotation(Rotation rotation, Guid currentUserId, bool viewMode)
        {
            SummaryViewModel model = new SummaryViewModel
            {
                RotationId = rotation.Id,
                CurrentShiftId = rotation.RotationType == RotationType.Shift ? _services.ShiftService.GetCurrentShiftIdForRotation(rotation) : null,
                RotarionOwnerId = rotation.RotationOwnerId,
                IsSwingExpired = rotation.State == RotationState.SwingEnded,
                RotationOwnerFullName = rotation.RotationOwner.FullName,
                IsRotationExpiredOrNotStarted = !rotation.StartDate.HasValue || rotation.State == RotationState.Expired,
                IsViewMode = viewMode,
                IsShiftRotation = rotation.RotationType == RotationType.Shift
            };

            return model;
        }

        public Guid GetCurrentRotationId(Guid userAnyoneRotationId)
        {
            var rotation = _logicCore.RotationCore.GetRotation(userAnyoneRotationId);

            if (rotation.RotationOwnerId != null)
            {
                var user = _logicCore.UserProfileCore.GetUserProfile(rotation.RotationOwnerId);

                if (user.CurrentRotationId != null)
                {
                    return user.CurrentRotationId.Value;
                }
            }

            return Guid.Empty;
        }

        public void UpdateSwingRecipient(ChooseSwingRecipientModel model)
        {
            if (model.RotationRecipientId.HasValue)
            {
                this.UpdateSwingRecipient(model.RotationId, model.RotationRecipientId.Value);
            }
        }

        private void UpdateSwingItemsAssignee(Rotation rotation, Guid oldRecipientId, Guid newRecipientId, bool save = true)
        {

            var rotationTopics = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft)
                                   .SelectMany(m => m.RotationTopicGroups)
                                   .SelectMany(tg => tg.RotationTopics)
                                   .Where(topic => topic.AssignedToId.HasValue
                                                && topic.AssignedToId == oldRecipientId);


            var rotationTasks = rotation.RotationModules.Where(m => m.SourceType == TypeOfModuleSource.Draft)
                                  .SelectMany(m => m.RotationTopicGroups)
                                  .SelectMany(tg => tg.RotationTopics)
                                  .SelectMany(t => t.RotationTasks)
                                  .Where(task => task.AssignedToId.HasValue
                                              && task.AssignedToId == oldRecipientId);

            foreach (var rotationTopic in rotationTopics)
            {
                rotationTopic.AssignedToId = newRecipientId;
                _repositories.RotationTopicRepository.Update(rotationTopic);
            }

            foreach (var rotationTask in rotationTasks)
            {
                rotationTask.AssignedToId = newRecipientId;
                _repositories.RotationTaskRepository.Update(rotationTask);
            }

            if (save)
            {
                _repositories.Save();
            }

        }


        private void InitSwingItemsFromTemplate(Rotation rotation)
        {
            var template = _logicCore.TemplateCore.GetRotationTemplate(rotation.Id);

            if (template.Modules == null || template.Modules.Count == 0)
            {
                _logicCore.ModuleCore.InitTemplateModules(template.Id);
            }

            foreach (var templateModule in template.Modules)
            {
                var swingModule = _logicCore.RotationModuleCore.GetOrCreateRotationModule(rotation.Id, templateModule);

                if (templateModule.TopicGroups != null && templateModule.TopicGroups.Any())
                {
                    foreach (var templateTopicGroup in templateModule.TopicGroups)
                    {
                        var swingTopicGroup = _logicCore.RotationTopicGroupCore.GetOrCreateRotationTopicGroup(swingModule.Id, templateTopicGroup);

                        foreach (var templateTopic in templateTopicGroup.Topics)
                        {
                            var swingTopic = _logicCore.RotationTopicCore.GetOrCreateRotationTopic(swingTopicGroup.Id, templateTopic);

                            foreach (var templateTask in templateTopic.Tasks)
                            {
                                _logicCore.RotationTaskCore.GetOrCreateRotationTask(swingTopic.Id, templateTask);
                            }
                        }
                    }
                }
            }
        }

        public void InitPinItems(Rotation newSwing)
        {
            Rotation previousSwing = newSwing.PrevRotation;

            if (previousSwing != null)
            {
                var pinnedTopicGroupsInPrevSwing = _logicCore.RotationTopicGroupCore.GetRotationTopicGroupsWhereIsPinned(previousSwing.Id).Where(tg => tg.Enabled).ToList();

                var currentSwingTopicGroups = _logicCore.RotationTopicGroupCore.GetAllRotationTopicGroups(newSwing.Id, TypeOfModuleSource.Draft).Where(tg => tg.Enabled).ToList();

                foreach (var pinnedTopicGroupInPrevSwing in pinnedTopicGroupsInPrevSwing)
                {
                    //For Template Topic Group

                    if (pinnedTopicGroupInPrevSwing.TempateTopicGroupId.HasValue)
                    {
                        var topicGroupFromTemplateInCurrentSwing = currentSwingTopicGroups.FirstOrDefault(tg => tg.TempateTopicGroupId.HasValue
                                                && tg.TempateTopicGroupId == pinnedTopicGroupInPrevSwing.TempateTopicGroupId);

                        if (topicGroupFromTemplateInCurrentSwing != null)
                        {
                            foreach (var rotationTopic in pinnedTopicGroupInPrevSwing.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.TempateTopicGroupId != null && t.RotationTopicGroup.TempateTopicGroupId == topicGroupFromTemplateInCurrentSwing.TempateTopicGroupId).ToList())
                            {
                                var swingPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupFromTemplateInCurrentSwing.Id, rotationTopic, newSwing.DefaultBackToBackId);

                                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                                {
                                    var swingPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(swingPinTopic.Id, rotationTask, newSwing.DefaultBackToBackId);
                                }
                            }
                        }
                    }


                    // For Relation

                    else if (pinnedTopicGroupInPrevSwing.RelationId.HasValue)
                    {
                        var topicGroupRelationInCurrentSwing = currentSwingTopicGroups.FirstOrDefault(tg => tg.RelationId == pinnedTopicGroupInPrevSwing.RelationId && tg.RelationId.HasValue);

                        if (topicGroupRelationInCurrentSwing != null)
                        {

                            foreach (var rotationTopic in pinnedTopicGroupInPrevSwing.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value && t.RotationTopicGroup.Enabled).ToList())
                            {
                                var swingPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupRelationInCurrentSwing.Id, rotationTopic, newSwing.DefaultBackToBackId);

                                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                                {
                                    var swingPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(swingPinTopic.Id, rotationTask, newSwing.DefaultBackToBackId);
                                }
                            }
                        }
                    }

                    //For Other 

                    else if (pinnedTopicGroupInPrevSwing.Name.Equals("Other"))
                    {
                        var topicGroupOtherInCurrentSwing = currentSwingTopicGroups.FirstOrDefault(tg => tg.RotationModule.Type == pinnedTopicGroupInPrevSwing.RotationModule.Type &&
                        tg.Name.Equals("Other"));

                        if (topicGroupOtherInCurrentSwing != null)
                        {

                            foreach (var rotationTopic in pinnedTopicGroupInPrevSwing.RotationTopics.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                            {
                                var swingPinTopic = _logicCore.RotationTopicCore.GetOrCreatePinTopic(topicGroupOtherInCurrentSwing.Id, rotationTopic, newSwing.DefaultBackToBackId);

                                foreach (var rotationTask in rotationTopic.RotationTasks.Where(t => t.IsPinned.HasValue && t.IsPinned.Value).ToList())
                                {
                                    var swingPinTask = _logicCore.RotationTaskCore.GetOrCreatePinTask(swingPinTopic.Id, rotationTask, newSwing.DefaultBackToBackId);
                                }
                            }
                        }
                    }

                }

                _repositories.Save();

            }
        }


        public void UpdateSwingRecipient(Guid rotationId, Guid rotationRecipientId, bool save = true)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);


            UserProfile newBackToBack = _logicCore.UserProfileCore.GetUserProfile(rotationRecipientId);
            _logicCore.NotificationCore.NotificationTrigger.Send_ManagerReassignsHandover(rotation.RotationOwnerId,
                                                                                            newBackToBack.FullName);

            _logicCore.RotationCore.UpdateRotationItems(rotation, rotation.DefaultBackToBackId, rotationRecipientId, false);

            bool isFirstRecipientSetUp = !(rotation.DefaultBackToBackId != null);

            if (rotation.DefaultBackToBackId != rotationRecipientId)
            {
                var oldRecipientId = rotation.DefaultBackToBackId;
                rotation.DefaultBackToBackId = rotationRecipientId;

                if (oldRecipientId != null)
                    this.UpdateSwingItemsAssignee(rotation, oldRecipientId, rotationRecipientId, false);

                _repositories.RotationRepository.Update(rotation);

                if (save)
                    _repositories.Save();

                if (isFirstRecipientSetUp)
                {
                    this.InitSwingItemsFromTemplate(rotation);
                    InitPinItems(rotation);
                }
            }

        }

        public ChooseSwingRecipientModel PopulateChooseSwingRecipientViewModel(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            UserProfile swingOwner = rotation.RotationOwner;

            IEnumerable<Guid> usersIds = _authService.GetUsersIdsByRole(iHandoverRoles.Relay.iHandoverAdmin, Guid.NewGuid());

            IQueryable<UserProfile> allEmployees = _logicCore.UserProfileCore.GetRelayEmployees().Where(ApplyUserFilter()).Where(UserActiveFilter());


            var rotationSwingRecipients = rotation.RotationType == RotationType.Shift ? allEmployees.Where(u => u.CurrentRotation.RotationType == RotationType.Shift).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                                .Select(e => new SelectListItem
                                                {
                                                    Text = e.FirstName + " " + e.LastName,
                                                    Value = e.Id.ToString()
                                                }).ToList() : allEmployees.Where(u => u.CurrentRotation.RotationType == RotationType.Swing).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                                .Select(e => new SelectListItem
                                                {
                                                    Text = e.FirstName + " " + e.LastName,
                                                    Value = e.Id.ToString()
                                                }).ToList();

            if (!rotationSwingRecipients.Any())
            {
                allEmployees = allEmployees.Where(u => !swingOwner.Contributors.Contains(u));

                rotationSwingRecipients = allEmployees.Where(u => !u.DeletedUtc.HasValue).OrderBy(u => u.FirstName).ThenBy(u => u.LastName)
                                                .Select(e => new SelectListItem
                                                {
                                                    Text = e.FirstName + " " + e.LastName,
                                                    Value = e.Id.ToString()
                                                }).ToList();
               
            }

            var adminIdOfSubscription = _authService.GetIdAdminOfSubscription();

            if (adminIdOfSubscription != null)
            {
                var adminOfSubscription = _logicCore.UserProfileCore.GetUserProfile(adminIdOfSubscription.Value);

                if (adminOfSubscription != null)
                {
                    rotationSwingRecipients.Add(new SelectListItem
                    {
                        Text = adminOfSubscription.FirstName + " " + adminOfSubscription.LastName,
                        Value = adminOfSubscription.Id.ToString()
                    });
                }
            }
        
            var usersWitoutRotation = _logicCore.TeamCore.GetAllTeams().SelectMany(t => t.Users).Where(t => !t.CurrentRotationId.HasValue).ToList();

            if (usersWitoutRotation.Any())
            {
                foreach (var userWitoutRotation in usersWitoutRotation)
                {
                    if (userWitoutRotation != null && !userWitoutRotation.DeletedUtc.HasValue)
                    {
                        rotationSwingRecipients.ToList().Add(new SelectListItem
                        {
                            Text = userWitoutRotation.FirstName + " " + userWitoutRotation.LastName,
                            Value = userWitoutRotation.Id.ToString()
                        });
                    }
                }
            }

            ChooseSwingRecipientModel model = new ChooseSwingRecipientModel
            {
                 RotationId = rotationId,
                 RotationRecipientId = rotation.DefaultBackToBackId,
                 TeamMembers = rotationSwingRecipients
            };

            return model;
        }

        public bool SwingHasRecipient(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);
            var userRoles = _services.AuthService.GetUserRoles(rotation.DefaultBackToBackId);

            return userRoles.Any(r => r == iHandoverRoles.Relay.Administrator || r == iHandoverRoles.Relay.iHandoverAdmin || r == iHandoverRoles.Relay.ExecutiveUser);
        }

        public ConfirmButtonViewModel ConfirmButton(Guid userId)
        {
            return new ConfirmButtonViewModel
            {
                IsShowConfirmRotationButton = this.ShowConfirmRotationButton(userId),
                IsShowShiftRotationButton = this.ShowConfirmShiftButton(userId),
                UserId = userId
            };
        }

        public bool IsNotExistAliveRotationOrShift(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user.CurrentRotationId.HasValue)
            {
                switch (user.CurrentRotation.RotationType)
                {
                    case RotationType.Swing:
                        if (user.CurrentRotation.State == RotationState.Created && !user.CurrentRotation.PrevRotationId.HasValue)
                            return true;
                        break;
                    case RotationType.Shift:
                        if (user.CurrentRotation.State == RotationState.Confirmed && !user.CurrentRotation.RotationShifts.Any())
                            return true;
                        break;
                }
            }

            return false;
        }

        public bool IsExistTemplateTopicsPrevRotationOrShift(UserProfile user, Guid topicId)
        {
            if (user.CurrentRotationId.HasValue)
            {
                if (user.CurrentRotation.RotationType == RotationType.Shift && user.CurrentRotation.RotationShifts != null && user.CurrentRotation.RotationShifts.Any())
                {
                    user.CurrentRotation.RotationShifts = user.CurrentRotation.RotationShifts.Where(t => t.State == ShiftState.Finished).ToList();

                    var modules = user.CurrentRotation.RotationShifts.SelectMany(t => t.RotationModules).ToList();

                    var topicGroups = modules.SelectMany(t => t.RotationTopicGroups).ToList();

                    var isHaveTopicGroups = topicGroups.Where(t => t.Id == topicId).ToList();

                    if (isHaveTopicGroups == null || isHaveTopicGroups.Count == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsExistTemplateTopicInRotationOrShift(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user.CurrentRotationId.HasValue)
            {
                switch (user.CurrentRotation.RotationType)
                {
                    case RotationType.Swing:
                        if (user.CurrentRotation.State == RotationState.Created && !user.CurrentRotation.PrevRotationId.HasValue)
                            return true;
                        break;
                    case RotationType.Shift:
                        if (user.CurrentRotation.State == RotationState.Confirmed && !user.CurrentRotation.RotationShifts.Any())
                            return true;
                        break;
                }
            }

            return false;
        }

        private bool ShowConfirmRotationButton(Guid userId)
        {
            UserProfile employee = _logicCore.UserProfileCore.GetUserProfile(userId);
            Rotation rotation = _logicCore.RotationCore.GetRotation(employee.CurrentRotationId.Value);

            if (employee.CurrentRotationId.HasValue && !(rotation.RotationType == RotationType.Shift))
            {
                //first rotation
                if (!(rotation.PrevRotation != null)
                                       && (rotation.State == RotationState.Created || rotation.State == RotationState.Expired))
                {
                    return true;
                }

                // no first rotation
                if (rotation.PrevRotation != null
                                    && (rotation.State == RotationState.Created || rotation.State == RotationState.Expired))
                {
                    return true;
                }

                if (IsRotationSwingEnd(rotation))
                {
                    return true;
                }
            }
            return false;
        }

        private bool ShowConfirmShiftButton(Guid userId)
        {
            UserProfile employee = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (employee.CurrentRotationId.HasValue && IsShiftRotation(employee.CurrentRotationId.Value))
            {
                //shift user whose rotation has expired
                var currentRotation = employee.CurrentRotation;

                if (currentRotation.PrevRotationId.HasValue && (currentRotation.State == RotationState.Created || currentRotation.State == RotationState.Expired))
                {
                    return true;
                }

                var shift = currentRotation.RotationShifts.Any()
                    ? currentRotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Finished) : null;

                if ((!(currentRotation.PrevRotation != null) && currentRotation.State == RotationState.Confirmed && shift != null) || !currentRotation.RotationShifts.Any())
                {
                    return true;
                }

                if (IsRotationFirstShiftNotConfirmed(currentRotation))
                {
                    return true;
                }
            }
            return false;
        }


        public bool IsShowShiftEditDatesButton(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user.CurrentRotationId.HasValue)
            {
                /*if (_services.RotationService.IsShiftRotation(user.CurrentRotation.Id))
                {
                    var shift = user.CurrentRotation.RotationShifts.Any()
                                           ? _logicCore.ShiftCore.GetActiveRotationShift(user.CurrentRotation) != null
                                               ? _logicCore.ShiftCore.GetActiveRotationShift(user.CurrentRotation)
                                               : user.CurrentRotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Finished)
                                           : null;

                    if (shift != null && shift.State != ShiftState.Finished && shift.State != ShiftState.Created)
                    {
                        return true;
                    }

                }*/

                if (user.CurrentRotation.RotationType == RotationType.Shift)
                {
                    var shift = _logicCore.ShiftCore.GetActiveRotationShift(user.CurrentRotation);

                    if (shift != null)
                    {
                        return true;
                    }
                }
                else
                {
                    return false;
                }
            }

            return false;
        }


        public bool IsShowRotationEditDatesButton(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            //if (user.CurrentRotationId.HasValue)
            //{
            //    var currentRotation = user.CurrentRotation;

            //    if (!_services.RotationService.IsShiftRotation(currentRotation.Id))
            //    {
            //        if (user.CurrentRotation.State != RotationState.Expired && user.CurrentRotation.State != RotationState.Created && user.CurrentRotation.State != RotationState.SwingEnded)
            //        {
            //            return true;
            //        }
            //    }
            //}

            if (user.CurrentRotationId.HasValue)
            {
                if (user.CurrentRotation.RotationType == RotationType.Swing)
                {
                    if (user.CurrentRotation.State == RotationState.Confirmed)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsShowConfirmButton(Guid userId)
        {
            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            if (user.CurrentRotationId.HasValue)
            {
                var currentRotation = user.CurrentRotation;

                if (_services.RotationService.IsShiftRotation(currentRotation.Id))
                {
                    var shift = currentRotation.RotationShifts.Any()
                                            ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation) != null
                                                ? _logicCore.ShiftCore.GetActiveRotationShift(currentRotation)
                                                : currentRotation.RotationShifts.FirstOrDefault(s => s.State == ShiftState.Finished)
                                            : null;

                    if (shift != null && shift.State == ShiftState.Finished)
                        return true;

                    if (shift == null)
                        return true;

                    if (!currentRotation.RotationShifts.Any())
                        return true;
                }
                else
                {
                    if (user.CurrentRotation.State == RotationState.Expired || user.CurrentRotation.State == RotationState.Created || user.CurrentRotation.State == RotationState.SwingEnded)
                        return true;
                }
            }

            return false;
        }


        public ConfirmRotationViewModel PopulateConfirmModelWithCurrentRotation(Guid employeeId)
        {
            UserProfile employee = _logicCore.UserProfileCore.GetUserProfile(employeeId);

            if (employee.CurrentRotationId.HasValue)
            {
                Rotation rotation = employee.CurrentRotation;

                if (rotation.State == RotationState.Created)
                {
                    Rotation prevRotation = rotation.PrevRotation;

                    return new ConfirmRotationViewModel
                    {
                        UserId = employee.Id,
                        StartSwingDate = rotation.StartDate,
                        EndSwingDate = rotation.StartDate.HasValue ? (DateTime?)rotation.StartDate.Value.AddDays(rotation.DayOn) : null,
                        BackOnSiteDate = rotation.StartDate.HasValue ? (DateTime?)rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff) : null,
                        PrevRotationEndDate = (prevRotation != null) ? prevRotation.StartDate.Value.AddDays(prevRotation.DayOn + prevRotation.DayOff) : (DateTime?)null,
                        RepeatTimes = rotation.RepeatTimes
                    };
                }
                if (rotation.State == RotationState.SwingEnded)
                {
                    ConfirmRotationViewModel model = new ConfirmRotationViewModel { PrevRotationEndDate = DateTime.Today };

                    if (rotation.RepeatTimes > 0)
                    {
                        DateTime newStartDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff);

                        model.UserId = employee.Id;
                        model.StartSwingDate = newStartDate;
                        model.EndSwingDate = newStartDate.AddDays(rotation.DayOn);
                        model.BackOnSiteDate = newStartDate.AddDays(rotation.DayOn + rotation.DayOff);
                        model.RepeatTimes = rotation.RepeatTimes - 1;
                    }

                    return model;
                }

            }

            throw new Exception(string.Format("User with Id: {0} does not have current working pattern.", employeeId));
        }

        public EditRotationViewModel GetRotation(Guid Id, Guid userId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(Id);

            EditRotationViewModel model = new EditRotationViewModel
            {
                Id = rotation.Id,
                RotationOwner = rotation.RotationOwner.FullName,
                RotationOwnerId = rotation.RotationOwnerId.ToString(),
                PositionName = rotation.RotationOwner.PositionName,
                PositionId = rotation.RotationOwner.PositionId,
                SiteName = rotation.RotationOwner.Position.Workplace.Name,
                SiteId = rotation.RotationOwner.Position.WorkplaceId,
                ContributorsIds = rotation.RotationOwner.Contributors.Select(c => c.Id).ToList(),
                DefaultBackToBackId = rotation.DefaultBackToBackId.ToString(),
                LineManagerId = rotation.LineManagerId.ToString(),
                ShiftPatterns = _services.RotationPatternService.GetShiftPatternsList(),
                ShiftPatternType = rotation.RotationOwner.ShiftPatternType
            };

            return PopulateEditRotationModel(model, userId);
        }

        public Guid GetRotationOwnerId(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return rotation.RotationOwnerId;
        }

        public MomentumPlus.Core.Models.RotationType GetRotationType(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return rotation.RotationType;
        }

        public FromToBlockViewModel GetFromToBlock(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            var model = _mapper.Map<FromToBlockViewModel>(rotation);

            if (rotation.RotationType == RotationType.Shift)
            {
                var shiftId = _services.ShiftService.GetCurrentShiftIdForRotation(rotationId);

                if (shiftId == null)
                {
                    shiftId = _services.ShiftService.GetCurrentShiftIdByRotationId(rotationId);
                }

                if (shiftId != null)
                {
                    var shift = _logicCore.ShiftCore.GetShift(shiftId.Value);

                    model.RotationBackToBackId = shift.ShiftRecipientId;
                    model.RotationBackToBackFullName = shift.ShiftRecipient != null ? shift.ShiftRecipient.FullName : null;

                    rotation = _logicCore.RotationCore.GetRotation(shift.RotationId);
                    rotation.DefaultBackToBack = shift.ShiftRecipient;
                    rotation.DefaultBackToBackId = shift.ShiftRecipientId != null ? shift.ShiftRecipientId.Value : Guid.Empty;

                    model = _mapper.Map<FromToBlockViewModel>(rotation);
                }
            }

            model.ContributorsUsersList = rotation.RotationOwner.Contributors.Select(t =>
            new DropDownListItemModel
            {
                Text = t.FullName,
                Value = t.Id.ToString()
            });

            model.UsersList = model.ContributorsUsersList.ToList();

            model.UsersList.Insert(0, new DropDownListItemModel
            {
                Text = rotation.RotationOwner.FullName,
                Value = rotation.RotationOwner.Id.ToString()
            });

            return model;
        }


        public FromToBlockViewModel GetFromToBlockShift(Guid shiftId)
        {
            var model = new FromToBlockViewModel();

            var shift = _logicCore.ShiftCore.GetShift(shiftId);

            model.RotationBackToBackId = shift.ShiftRecipientId;
            model.RotationBackToBackFullName = shift.ShiftRecipient != null ? shift.ShiftRecipient.FullName : null;
            model.RotationOwnerId = shift.Rotation.RotationOwner.Id;
            model.RotationOwnerFullName = shift.Rotation.RotationOwner.FullName;

            model.ContributorsUsersList = shift.Rotation.RotationOwner.Contributors.Select(t =>
            new DropDownListItemModel
            {
                Text = t.FullName,
                Value = t.Id.ToString()
            });

            model.UsersList = model.ContributorsUsersList.ToList();

            model.UsersList.Insert(0, new DropDownListItemModel
            {
                Text = shift.Rotation.RotationOwner.FullName,
                Value = shift.Rotation.RotationOwner.Id.ToString()
            });


            return model;
        }

        public Guid? GetRotationOwnerId(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);
                if (rotation != null)
                {
                    return rotation.RotationOwnerId;
                }
            }

            return null;
        }

        public Guid? GetRotationBackToBackId(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);
                if (rotation != null)
                {
                    return rotation.DefaultBackToBackId;
                }
            }

            return null;
        }


        public bool UpdateRotation(EditRotationViewModel model)
        {
            if (model.Id != Guid.Empty)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(model.Id);

                if (rotation != null)
                {
                    if (model.DefaultBackToBackId == null)
                    {
                        model.DefaultBackToBackId = _authService.GetSubscriptionOwnerId().Value.ToString();
                    }
                    //---- notification 4 ------
                    Guid newBackToBackId = Guid.Parse(model.DefaultBackToBackId);

                    if (rotation.DefaultBackToBackId != newBackToBackId)
                    {
                        UserProfile newBackToBack = _logicCore.UserProfileCore.GetUserProfile(newBackToBackId);
                        _logicCore.NotificationCore.NotificationTrigger.Send_ManagerReassignsHandover(rotation.RotationOwnerId,
                                                                                                        newBackToBack.FullName);
                        _logicCore.RotationCore.UpdateRotationItems(rotation, rotation.DefaultBackToBackId, newBackToBackId, true);
                    }

                    if (rotation.RotationType == RotationType.Shift)
                    {
                        var shift = rotation.RotationShifts.FirstOrDefault(sh => sh.State == ShiftState.Confirmed);

                        if (shift != null)
                        {
                            _logicCore.ShiftCore.UpdateShiftRecipient(shift.Id, newBackToBackId, true);
                        }
                    }
                    //--------------------------
                    _mapper.Map(model, rotation);

                    this.FillModelContributorsAndProjects(model, rotation);

                    var contributorsIds = model.ContributorsIds?.ToList() ?? new List<Guid>();


                    if (model.ContributorEnable)
                    {
                        _logicCore.ContributorsCore.SetContributorsForUser(rotation.RotationOwnerId, contributorsIds);
                    }

                    //if (rotation.RotationType != (RotationType)model.RotationType)
                    //{
                    //    _logicCore.RotationCore.UpdateRotationType(rotation, (RotationType)model.RotationType);
                    //}

                    if (rotation.NextRotationId.HasValue)
                    {
                        _mapper.Map(model, rotation.NextRotation);
                        //_logicCore.RotationCore.UpdateRotationType(rotation, (RotationType)model.RotationType);
                        _repositories.RotationRepository.Update(rotation.NextRotation);

                        this.FillModelContributorsAndProjects(model, rotation.NextRotation);
                    }

                    _repositories.RotationRepository.Update(rotation);
                    _repositories.Save();

                    return true;
                }
            }

            return false;
        }

        public string GetRotationOwnerFullName(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return rotation.RotationOwner.FullName;
        }

        private void FillModelContributorsAndProjects(BaseRotationViewModel viewModel, Rotation entity)
        {
        }

        public bool CreateFirstRotation(CreateRotationViewModel model)
        {
            Rotation rotation = _mapper.Map<Rotation>(model);
            rotation.Id = Guid.NewGuid();
            //add Contributors to UserProfile
            if (model.ContributorEnable)
            {
                var contributorsIds = model.ContributorsIds?.ToList() ?? new List<Guid>();
                _logicCore.ContributorsCore.SetContributorsForUser(Guid.Parse(model.RotationOwnerId), contributorsIds);
            }

            //===============================
            rotation.State = RotationState.Created;
            rotation.RotationType = (RotationType)model.RotationType;
            this.FillModelContributorsAndProjects(model, rotation);

            return _logicCore.RotationCore.AssignFirstRotation(rotation);
        }


        public bool CheckRotationPatternCompatibility(Guid employeeId, ConfirmRotationViewModel model)
        {
            if (model.StartSwingDate.HasValue && model.EndSwingDate.HasValue && model.BackOnSiteDate.HasValue)
            {
                return _logicCore.RotationCore.CheckRotationPatternCompatibility(employeeId,
                                                                                model.StartSwingDate.Value,
                                                                                model.EndSwingDate.Value,
                                                                                model.BackOnSiteDate.Value);
            }

            return false;
        }

        public bool ConfirmRotation(Guid employeeId, ConfirmRotationViewModel model, Guid identityUserId)
        {
            if (model.StartSwingDate.HasValue && model.EndSwingDate.HasValue && model.BackOnSiteDate.HasValue)
            {
                return _logicCore.RotationCore.ConfirmRotation(employeeId, model.StartSwingDate.Value,
                                                                                model.EndSwingDate.Value,
                                                                                model.BackOnSiteDate.Value,
                                                                                model.RepeatTimes, identityUserId);
            }

            return false;
        }

        public bool RotationIsConfirmed(Guid rotationid)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationid);

            if (rotation.State != RotationState.Created)
            {
                if (rotation.State == RotationState.Expired)
                    _logicCore.RotationCore.MoveToNextRotation(rotation.RotationOwner);
                else
                    return true;
            }

            return false;
        }


        public bool RotationIsConfirmed(Rotation rotation)
        {
            if (rotation.State != RotationState.Created)
            {
                if (rotation.State == RotationState.Expired)
                    _logicCore.RotationCore.MoveToNextRotation(rotation.RotationOwner);
                else
                    return true;
            }

            return false;
        }
        public IEnumerable<TeamRotationViewModel> GetActiveRotations(Guid? userId)
        {
            if (userId.HasValue && userId.Value != Guid.Empty)
            {
                IEnumerable<string> userRoles = _authService.GetUserRoles(userId.Value);

                if (userRoles.Contains(iHandoverRoles.Relay.LineManager)
                    || userRoles.Contains(iHandoverRoles.Relay.HeadLineManager)
                    || userRoles.Contains(iHandoverRoles.Relay.SafetyManager))
                {
                    IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllActiveRotations()
                        .Where(r => r.LineManagerId == userId.Value && r.RotationOwnerId != userId.Value && !r.RotationOwner.DeletedUtc.HasValue);

                    return _mapper.Map<IEnumerable<TeamRotationViewModel>>(rotations.ToList().OrderBy(r => r.RotationOwner.FirstName));
                }
                else
                {
                    IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllActiveRotations()
                        .Where(r => r.RotationOwnerId != userId.Value && !r.RotationOwner.DeletedUtc.HasValue);

                    return _mapper.Map<IEnumerable<TeamRotationViewModel>>(rotations.ToList().OrderBy(r => r.RotationOwner.FirstName));
                }
            }

            return null;
        }
        public void ChangeFinalizeStatusForDraftTopicsAndTasks(Guid rotationId, FinalizeStatus status)
        {
            _logicCore.RotationCore.ChangeFinalizeStatusForDraftTopicsAndTasks(rotationId, (StatusOfFinalize)status);
        }

        public IEnumerable<ReceivedRotationSlideViewModel> GetRotationsWhoPopulateMyReceivedSection(Guid rotationId)
        {

            List<Rotation> rotationsWhoPopulateMyReceivedSection = _logicCore.RotationCore
                                                                                .GetRotation(rotationId)
                                                                                .HandoverFromRotations
                                                                                .ToList();

            IQueryable<RotationTask> receivedTasks = _logicCore.RotationTaskCore.GetAllRotationTasks(rotationId, TypeOfModuleSource.Received);
            IEnumerable<ReceivedRotationSlideViewModel> rotations = _mapper.Map<IEnumerable<ReceivedRotationSlideViewModel>>(rotationsWhoPopulateMyReceivedSection);

            if (rotations != null)
            {
                if (rotations.Any())
                {
                    foreach (var rotation in rotations)
                    {
                        if (rotationsWhoPopulateMyReceivedSection != null)
                        {
                            if (rotationsWhoPopulateMyReceivedSection.Any())
                            {
                                foreach (var sourceRotation in rotationsWhoPopulateMyReceivedSection)
                                {
                                    if (sourceRotation.HandoverToRotations != null)
                                    {
                                        if (sourceRotation.HandoverToRotations.Any())
                                        {
                                            var source = sourceRotation.HandoverToRotations.FirstOrDefault(r => r.RotationOwnerId == sourceRotation.DefaultBackToBackId);

                                            var rotationModules = _logicCore.RotationModuleCore.GetRotationModule(source != null ? source.Id : sourceRotation.Id, TypeOfModuleSource.Received);

                                            List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

                                            foreach (var rotationModule in rotationModules)
                                            {
                                                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                                                        .Where(tg => tg.Enabled)
                                                                                                        .SelectMany(tg => tg.RotationTopics)
                                                                                                        .Where(t => t.Enabled);

                                                if (rotationModule.Type == TypeOfModule.Team)
                                                {
                                                    var topicsGroupByRelation = rotationTopics.Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.RelationId.Value).ToList().Select(t => t.FirstOrDefault()).ToList();

                                                    topicsGroupByRelation.ForEach(topicGroupByRelation =>
                                                    {
                                                        var topics = _logicCore.RotationModuleCore.GetTopicsByRelationId(topicGroupByRelation.RelationId.Value, topicGroupByRelation.RotationTopicGroupId);
                                                        sortedRotationTopics.AddRange(topics);
                                                    });
                                                }
                                                else
                                                {
                                                    var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.OrderByDescending(r => r.RotationTopics.Count).FirstOrDefault()).ToList();

                                                    if (topicGroupsByName != null)
                                                    {
                                                        topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                                                        {
                                                            var topics = topicGroup.RotationTopics.ToList();

                                                            sortedRotationTopics.AddRange(topics);
                                                        });
                                                    }
                                                }
                                            }

                                            var count = sortedRotationTopics.Count();

                                            rotation.TopicCounter = count;
                                        }
                                    }

                                    else if (sourceRotation.HandoverFromRotations != null)
                                    {
                                        if (sourceRotation.HandoverFromRotations.Any())
                                        {
                                            var source = sourceRotation.HandoverFromRotations.FirstOrDefault(r => r.RotationOwnerId == sourceRotation.DefaultBackToBackId);

                                            var rotationModules = _logicCore.RotationModuleCore.GetRotationModule(source != null ? source.Id : sourceRotation.Id, TypeOfModuleSource.Received);

                                            List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

                                            foreach (var rotationModule in rotationModules)
                                            {
                                                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                                                        .Where(tg => tg.Enabled)
                                                                                                        .SelectMany(tg => tg.RotationTopics)
                                                                                                        .Where(t => t.Enabled);

                                                if (rotationModule.Type == TypeOfModule.Team)
                                                {
                                                    var topicsGroupByRelation = rotationTopics.Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.RelationId.Value).ToList().Select(t => t.FirstOrDefault()).ToList();

                                                    topicsGroupByRelation.ForEach(topicGroupByRelation =>
                                                    {
                                                        var topics = _logicCore.RotationModuleCore.GetTopicsByRelationId(topicGroupByRelation.RelationId.Value, topicGroupByRelation.RotationTopicGroupId);
                                                        sortedRotationTopics.AddRange(topics);
                                                    });
                                                }
                                                else
                                                {
                                                    var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.OrderByDescending(r => r.RotationTopics.Count).FirstOrDefault()).ToList();

                                                    if (topicGroupsByName != null)
                                                    {
                                                        topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                                                        {
                                                            var topics = topicGroup.RotationTopics.ToList();

                                                            sortedRotationTopics.AddRange(topics);
                                                        });
                                                    }
                                                }
                                            }

                                            var count = sortedRotationTopics.Count();

                                            rotation.TopicCounter = count;
                                        }
                                    }
                                    else
                                    {
                                        if (sourceRotation != null)
                                        {
                                            var rotationModules = _logicCore.RotationModuleCore.GetRotationModule(sourceRotation.Id, TypeOfModuleSource.Received);

                                            List<RotationTopic> sortedRotationTopics = new List<RotationTopic>();

                                            foreach (var rotationModule in rotationModules)
                                            {
                                                IEnumerable<RotationTopic> rotationTopics = rotationModule.RotationTopicGroups
                                                                                                        .Where(tg => tg.Enabled)
                                                                                                        .SelectMany(tg => tg.RotationTopics)
                                                                                                        .Where(t => t.Enabled);

                                                if (rotationModule.Type == TypeOfModule.Team)
                                                {
                                                    var topicsGroupByRelation = rotationTopics.Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.RelationId.Value).ToList().Select(t => t.FirstOrDefault()).ToList();

                                                    topicsGroupByRelation.ForEach(topicGroupByRelation =>
                                                    {
                                                        var topics = _logicCore.RotationModuleCore.GetTopicsByRelationId(topicGroupByRelation.RelationId.Value, topicGroupByRelation.RotationTopicGroupId);
                                                        sortedRotationTopics.AddRange(topics);
                                                    });
                                                }
                                                else
                                                {
                                                    var topicGroupsByName = rotationTopics.Select(t => t.RotationTopicGroup).Where(t => !t.DeletedUtc.HasValue).GroupBy(t => t.Name).ToList().Select(t => t.OrderByDescending(r => r.RotationTopics.Count).FirstOrDefault()).ToList();

                                                    if (topicGroupsByName != null)
                                                    {
                                                        topicGroupsByName.Where(t => t.RotationTopics.Any()).ToList().ForEach(topicGroup =>
                                                        {
                                                            var topics = topicGroup.RotationTopics.ToList();

                                                            sortedRotationTopics.AddRange(topics);
                                                        });
                                                    }
                                                }
                                            }

                                            var count = sortedRotationTopics.Count();

                                            rotation.TopicCounter = count;
                                        }
                                    }

                                    /*Rotation source = sourceRotation.HandoverToRotations != null && sourceRotation.HandoverToRotations.Any()
                                            ? sourceRotation.HandoverToRotations.Where(r => r.RotationOwnerId == sourceRotation.DefaultBackToBackId).FirstOrDefault()
                                            : sourceRotation.HandoverFromRotations != null && sourceRotation.HandoverFromRotations.Any()
                                            ? sourceRotation.HandoverFromRotations.Where(r => r.RotationOwnerId == sourceRotation.DefaultBackToBackId).FirstOrDefault() :
                                            sourceRotation;*/

                                   
                                }
                            }
                        }

                        var rotationRecived = rotationsWhoPopulateMyReceivedSection.FirstOrDefault(t => t.Id == rotation.Id);

                        rotation.Date = rotationRecived.StartDate.Value.FormatWithDayMonthYear() + " - " + rotationRecived.StartDate.Value.AddDays(rotationRecived.DayOn > 0 ? rotationRecived.DayOn - 1 : rotationRecived.DayOn).FormatWithDayMonthYear();

                        rotation.TaskCounter = _logicCore.TaskBoardTasksCore.GetTaskBoardTasksForRotation(rotationId, ModuleSourceType.Received, rotation.Id).Count();

                        rotation.ContributorsList = rotationRecived.RotationOwner.Contributors.Select(t =>
                           new Models.DropDownListItemModel
                           {
                               Text = t.FullName,
                               Value = t.Id.ToString()
                           }).ToList();
                    }
                }
            }

            return rotations.OrderBy(r => r.HandoverFrom).ThenByDescending(r => r.TopicCounter);
        }

        /// <summary>
        /// Method only for test in future be removed
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
       // [Obsolete("Testing functionality")]
        public bool ExpireRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _logicCore.RotationCore.ExpireRotation(rotation);
        }

        /// <summary>
        /// Method only for test in future be removed
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        [Obsolete("Testing functionality")]
        public bool EndSwingOfRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _logicCore.RotationCore.EndSwingOfRotation(rotation);
        }

        public bool EndSwing(Guid rotationId, byte[] pdfBytes)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return _logicCore.RotationCore.EndSwingOfRotation(rotation, pdfBytes);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        public bool IsRotationExpiredOrNotStarted(Guid rotationId)
        {
            var rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return (rotation.State == RotationState.Created || rotation.State == RotationState.Expired);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        public bool IsRotationSwingEnd(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            if (rotation.RotationType == RotationType.Shift)
            {
                return rotation.State == RotationState.SwingEnded && !rotation.RotationShifts.Any(s => s.State == ShiftState.Confirmed);
            }
            else
            {
                return rotation.State == RotationState.SwingEnded;
            }
        }

        public bool IsRotationSwingEnd(Rotation rotation)
        {
            if (rotation.RotationType == RotationType.Shift)
            {
                return rotation.State == RotationState.SwingEnded && !rotation.RotationShifts.Any(s => s.State == ShiftState.Confirmed);
            }
            else
            {
                return rotation.State == RotationState.SwingEnded;
            }
        }

        /// <summary>
        /// Return period of Rotation by Rotation Id.
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public string GetRotationPeriod(Guid rotationId)
        {
            string period = "...";
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            if (rotation.StartDate.HasValue)
            {
                period = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                            rotation.StartDate.Value.AddDays(rotation.DayOn > 0 ? rotation.DayOn - 1 : rotation.DayOn).FormatWithDayMonthYear();
            }

            return period;
        }

        public Rotation GetRotation(Guid rotationId)
        {
            return _logicCore.RotationCore.GetRotation(rotationId);
        }

        /// <summary>
        /// Return Period of received rotation by Rotation Id (if received rotation is one)
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <param name="sourceRotationId">Selected Source Rotation Id</param>
        /// <returns></returns>
        public string GetReceivedRotationPeriod(Guid? rotationId, Guid? sourceRotationId)
        {
            string period = "...";

            if (sourceRotationId.HasValue)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(sourceRotationId.Value);

                period = rotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                            rotation.StartDate.Value.AddDays(rotation.DayOn > 0 ? rotation.DayOn - 1 : rotation.DayOn).FormatWithDayMonthYear();
            }
            else if (rotationId.HasValue)
            {
                Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId.Value);

                if (rotation.HandoverFromRotations.Count() == 1)
                {
                    Rotation receivedRotation = rotation.HandoverFromRotations.First();

                    period = receivedRotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                                receivedRotation.StartDate.Value.AddDays(receivedRotation.DayOn > 0 ? receivedRotation.DayOn - 1 : receivedRotation.DayOn).FormatWithDayMonthYear();
                }
            }

            return period;
        }

        public EditRotationDatesViewModel GetRotationDates(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            EditRotationDatesViewModel model = new EditRotationDatesViewModel
            {
                CurrentDate = DateTime.Today,
                StartSwingDate = rotation.StartDate.Value,
                EndSwingDate = rotation.StartDate.Value.AddDays(rotation.DayOn),
                BackOnSiteDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff),
                RepeatTimes = rotation.RepeatTimes
            };

            model.EndSwingDateReadOnly = rotation.State == RotationState.SwingEnded || rotation.State == RotationState.Expired;
            model.BackOnSiteDateReadOnly = rotation.State == RotationState.Expired;
            model.FillRepeatRotationRange();

            return model;
        }

        public bool UpdateRotationDates(EditRotationDatesViewModel model)
        {
            var result = _logicCore.RotationCore.UpdateRotationDates(model.RotationId, model.StartSwingDate.Value, model.EndSwingDate.Value,
                                                                        model.BackOnSiteDate.Value, model.RepeatTimes);

            return result;
        }

        public bool IsShiftRotation(Guid rotationId)
        {
            Rotation rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return (rotation.RotationType == RotationType.Shift);
        }

        public bool IsShiftRotation(Rotation rotation)
        {
            return (rotation.RotationType == RotationType.Shift);
        }

        public IEnumerable<ShiftReceivedSlideViewModel> GetShiftsReceivedHistorySlidesByUser(Guid userId)
        {
            List<ShiftReceivedSlideViewModel> rotationsViewModel = new List<ShiftReceivedSlideViewModel>();
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetAllUserRotations(userId, true)
                                                                        .OrderByDescending(r => r.CreatedUtc.Value)
                                                                        .ToList();

            foreach (var rotation in rotations)
            {
                List<ShiftContributorForReceivedPanel> contributors = rotation.RotationOwner.Contributors.ToList().Select(c => new ShiftContributorForReceivedPanel
                {
                    ContributorID = c.Id,
                    ContributorName = c.FullName
                }).ToList();

                //foreach (var contributor in rotation.Contributors)
                //{
                //    contributors.Add(new ShiftContributorForReceivedPanel
                //    {
                //        ContributorID = contributor.Id,
                //        ContributorName = contributor.FullName
                //    });
                //}

                rotationsViewModel.Add(new ShiftReceivedSlideViewModel
                {
                    Id = rotation.Id,
                    Date = rotation.StartDate.Value.FormatWithMonth() + " - " + rotation.StartDate.Value.AddDays(rotation.DayOn).FormatWithMonth(),
                    HandoverFromName = rotation.RotationOwner.FullName,
                    HandoverFromID = rotation.RotationOwnerId,
                    Contributors = contributors,
                    TopicCounter = _logicCore.RotationTopicCore.GetAllRotationTopic(rotation.Id, TypeOfModuleSource.Received).Count(),
                    TaskCounter = _logicCore.RotationTaskCore.GetAllRotationTasks(rotation.Id, TypeOfModuleSource.Received).Count()
                });
            }

            return rotationsViewModel;
        }

        /// <summary>
        /// Check is rotation have prev rotation
        /// </summary>
        /// <param name="rotationId"></param>
        /// <returns></returns>
        public bool IsRotationHavePrevRotation(Guid rotationId)
        {
            var rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return rotation.PrevRotation != null;
        }

        public bool IsRotationHavePrevRotation(Rotation rotation)
        {
            return rotation.PrevRotation != null;
        }

        /// <summary>
        /// Get all Rotations by position ID
        /// </summary>
        /// <param name="positionId">GUID position ID</param>
        /// <returns>Collection of RotationItemViewModel</returns>
        public IEnumerable<RotationItemViewModel> GetRotationItemsForPosition(Guid positionId)
        {
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetActiveRotationsForPosition(positionId).ToList().OrderBy(r => r.RotationOwner.FirstName);

            IEnumerable<RotationItemViewModel> model = rotations.Select(r =>
                new RotationItemViewModel
                {
                    RotationId = r.Id,
                    RotationOwnerName = r.RotationOwner.LockDate.HasValue ? r.RotationOwner.FullName + " - [inactive]" : r.RotationOwner.FullName,
                    RotationOwnerId = r.RotationOwnerId,
                });

            return model;
        }


        public bool IsRotationFirstShiftNotConfirmed(Guid rotationId)
        {
            var rotation = _logicCore.RotationCore.GetRotation(rotationId);

            return rotation.RotationShifts.Count == 0 || rotation.RotationShifts.Count == 1 && rotation.RotationShifts.Any(s => s.State == ShiftState.Created);
        }

        public bool IsRotationFirstShiftNotConfirmed(Rotation rotation)
        {
            return rotation.RotationShifts.Count == 0 || rotation.RotationShifts.Count == 1 && rotation.RotationShifts.Any(s => s.State == ShiftState.Created);
        }
    }
}

