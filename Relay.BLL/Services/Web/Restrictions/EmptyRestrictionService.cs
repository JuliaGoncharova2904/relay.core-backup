﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.BLL.Services
{
    public class EmptyRestrictionService : IRestrictionService
    {
        public List<RotationModule> GetModulesByRotationId(Guid destId, Models.RotationType rotationType)
        {
            throw new NotImplementedException();
        }

        public void HandoverItemsLimitExceeded(Guid destId, Models.RotationType rotationType)
        { }

        public void HandoverItemsLimitExceededNew(Guid destId, Models.RotationType rotationType, int itemsNumber)
        { }

        public void HandoverTasksLimitExceeded(Guid topicId)
        { }

        public void TasksCarryForwardsLimitExceeded(Guid topicId)
        { }

        public void TopicsCarryForwardsLimitExceeded(Guid taskId)
        { }

        public void UsersLimitExceeded(Guid userId, string role)
        { }

        public void UsersLimitExceededForDeactivateUsers(Guid userId, string role, int userCount)
        { }
    }
}
