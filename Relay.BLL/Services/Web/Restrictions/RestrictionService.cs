﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Exceptions;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces.Services;
using MomentumPlus.Relay.Roles;
using System;
using System.Collections.Generic;
using System.Linq;
using RotationType = MomentumPlus.Relay.Models.RotationType;

namespace MomentumPlus.Relay.BLL.Services
{
    public class RestrictionService : IRestrictionService
    {
        private readonly LogicCoreUnitOfWork _logicCore;
        private readonly IRestrictionProvider _restrictionProvider;

        public RestrictionService(LogicCoreUnitOfWork logicCore, IRestrictionProvider restrictionProvider)
        {
            this._logicCore = logicCore;
            this._restrictionProvider = restrictionProvider;
        }

        /// <summary>
        /// Check Is Users Limit Exceeded
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="role">Role</param>
        public void UsersLimitExceeded(Guid userId, string role)
        {
            switch (role)
            {
                case iHandoverRoles.Relay.HeadLineManager:
                case iHandoverRoles.Relay.LineManager:
                case iHandoverRoles.Relay.SafetyManager:
                case iHandoverRoles.Relay.User:
                case iHandoverRoles.Relay.Administrator:
                case iHandoverRoles.Relay.ExecutiveUser:
                case iHandoverRoles.Relay.iHandoverAdmin:
                    if (_restrictionProvider.UsersLimitExceeded(userId))
                        throw new PaymentRequiredException("User limit reached. Please contact info@ihandover to upgrade.");
                    break;
                default:
                    throw new Exception("Unknown role");
            }
        }


        public void UsersLimitExceededForDeactivateUsers(Guid userId, string role, int userCount)
        {
            switch (role)
            {
                case iHandoverRoles.Relay.HeadLineManager:
                case iHandoverRoles.Relay.LineManager:
                case iHandoverRoles.Relay.SafetyManager:
                case iHandoverRoles.Relay.User:
                case iHandoverRoles.Relay.Administrator:
                case iHandoverRoles.Relay.ExecutiveUser:
                case iHandoverRoles.Relay.iHandoverAdmin:
                    if (_restrictionProvider.UsersLimitExceededForActivate(userId))
                    {
                        break;
                    }
                    else
                    {
                        throw new PaymentRequiredException("");
                    }
                default:
                    throw new Exception("Unknown role");
            }
        }

        /// <summary>
        /// Check Is Handover Items Limit Exceeded
        /// </summary>
        /// <param name="destId">Shift or Rotation ID</param>
        /// <param name="rotationType">Type of Report</param>
        public void HandoverItemsLimitExceeded(Guid destId, Models.RotationType rotationType)
        {
            List<RotationModule> modules = this.ExtractModulesByItem(destId, rotationType);

            int itemsNumber = modules?.Where(m => m.Enabled)
                                        .SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                        .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled)
                                        .Count() ?? 0;

            if (_restrictionProvider.HandoverItemsLimitExceeded(itemsNumber))
                throw new PaymentRequiredException("Handover Items Limit Exceeded");
        }

        public void HandoverItemsLimitExceededNew(Guid destId, Models.RotationType rotationType, int itemsNumber)
        {
            if (_restrictionProvider.HandoverItemsLimitExceeded(itemsNumber))
                throw new PaymentRequiredException("Handover Items Limit Exceeded");
        }

        /// <summary>
        /// Check Is Handover Tasks Limit Exceeded
        /// </summary>
        /// <param name="topicId">User ID</param>
        public void HandoverTasksLimitExceeded(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var rotationType = topic.RotationTopicGroup.RotationModule.RotationId.HasValue
                 ? Models.RotationType.Swing
                 : Models.RotationType.Shift;

            var destId = rotationType == RotationType.Swing
                                      ? topic.RotationTopicGroup.RotationModule.RotationId
                                      : topic.RotationTopicGroup.RotationModule.ShiftId;



            List<RotationModule> modules = this.ExtractModulesByItem(destId.Value, rotationType);

            int tasksNumber = modules?.Where(m => m.Enabled)
                                        .SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                        .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled)
                                        .SelectMany(t => t.RotationTasks).Where(ts => ts.Enabled)
                                        .Count() ?? 0;

            if (_restrictionProvider.HandoverTasksLimitExceeded(tasksNumber))
                throw new PaymentRequiredException("Handover Tasks Limit Exceeded");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="topicId">Carry forwarding topic ID</param>
        public void TopicsCarryForwardsLimitExceeded(Guid topicId)
        {
            RotationTopic topic = _logicCore.RotationTopicCore.GetTopic(topicId);

            var rotationType = topic.RotationTopicGroup.RotationModule.RotationId.HasValue
              ? Models.RotationType.Swing
              : Models.RotationType.Shift;

            var destId = rotationType == RotationType.Swing
                                      ? topic.RotationTopicGroup.RotationModule.RotationId
                                      : topic.RotationTopicGroup.RotationModule.ShiftId;

            List<RotationModule> modules = this.ExtractModulesByItem(destId.Value, rotationType);

            bool topicIsCarryForwarded = modules?.Where(m => m.Enabled)
                                                .SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled)
                                                .Any(t => t.ForkParentTopicId.Value == topicId) ?? false;

            if (!topicIsCarryForwarded)
            {
                if (_restrictionProvider.CarryForwardsLimitExceeded(this.CountCarryForwardsForModules(modules)))
                    throw new PaymentRequiredException("Carry Forwards Limit Exceeded");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskId">Carry forwarding task ID</param>
        public void TasksCarryForwardsLimitExceeded(Guid taskId)
        {
            RotationTask task = _logicCore.RotationTaskCore.GetTask(taskId);

            var rotationType = task.RotationTopic.RotationTopicGroup.RotationModule.RotationId.HasValue
                             ? Models.RotationType.Swing
                             : Models.RotationType.Shift;

            var destId = rotationType == RotationType.Swing
                                      ? task.RotationTopic.RotationTopicGroup.RotationModule.RotationId
                                      : task.RotationTopic.RotationTopicGroup.RotationModule.ShiftId;


            List<RotationModule> modules = this.ExtractModulesByItem(destId.Value, rotationType);

            bool taskIsCarryForwarded = modules?.Where(m => m.Enabled)
                                                .SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled)
                                                .SelectMany(t => t.RotationTasks).Where(ts => ts.Enabled)
                                                .Any(ts => ts.ForkParentTaskId.Value == taskId) ?? false;

            if (!taskIsCarryForwarded)
            {
                if (_restrictionProvider.CarryForwardsLimitExceeded(this.CountCarryForwardsForModules(modules)))
                    throw new PaymentRequiredException("Carry Forwards Limit Exceeded");
            }
        }

        #region Private methods

        /// <summary>
        /// Return number of carry forwarded items fo modules.
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns></returns>
        private int CountCarryForwardsForModules(List<RotationModule> modules)
        {
            int carryForwardedItemsNumber = modules?.Where(m => m.Enabled)
                                        .SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                        .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled)
                                        .Count(t => t.ForkParentTopicId.HasValue) ?? 0;

            int carryForwardedTasksNumber = modules?.Where(m => m.Enabled)
                                        .SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                        .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled)
                                        .SelectMany(t => t.RotationTasks).Where(ts => ts.Enabled)
                                        .Count(ts => ts.ForkParentTaskId.HasValue) ?? 0;

            return carryForwardedItemsNumber + carryForwardedTasksNumber;
        }

        /// <summary>
        /// Extract Modules By Topic from active rotation or shift.
        /// </summary>
        /// <param name="destId"></param>
        /// <param name="rotationType"></param>
        /// <returns></returns>
        private List<RotationModule> ExtractModulesByItem(Guid destId, Models.RotationType rotationType)
        {
            List<RotationModule> modules;

            switch (rotationType)
            {
                case Models.RotationType.Swing:
                    modules = _logicCore.RotationModuleCore.GetRotationModules(destId, TypeOfModuleSource.Draft).ToList();
                    break;
                case Models.RotationType.Shift:
                    modules = _logicCore.RotationModuleCore.GetShiftModules(destId, TypeOfModuleSource.Draft).ToList();
                    break;
                default:
                    throw new Exception("Bad working pattern type.");
            }
            return modules;
        }

        public List<RotationModule> GetModulesByRotationId(Guid destId, Models.RotationType rotationType)
        {
            List<RotationModule> modules;

            switch (rotationType)
            {
                case Models.RotationType.Swing:
                    modules = _logicCore.RotationModuleCore.GetRotationModules(destId, TypeOfModuleSource.Draft).ToList();
                    break;
                case Models.RotationType.Shift:
                    modules = _logicCore.RotationModuleCore.GetShiftModules(destId, TypeOfModuleSource.Draft).ToList();
                    break;
                default:
                    throw new Exception("Bad working pattern type.");
            }
            return modules;
        }

        #endregion

    }
}
