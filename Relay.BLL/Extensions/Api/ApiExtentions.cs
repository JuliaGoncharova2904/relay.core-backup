﻿using System;
using System.Web;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions;

namespace MomentumPlus.Relay.BLL.Extensions.Api
{
    public static class ApiExtentions
    {
        public static string FullImagePath(this string imageUrl)
        {
            if (string.IsNullOrEmpty(imageUrl))
            {
                return null;
            }
            var fullImagePath = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Host}" + imageUrl;

            return fullImagePath;
        }

        public static string GetImagePath(this Guid? avatarId)
        {
            if (!avatarId.HasValue)
            {
                return null;
            }
            var fullImagePath = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Host}" +"/images?" + avatarId;

            return fullImagePath;
        }

        public static string GetRotationPeriod(this Rotation rotation)
        {
            string period = null;
            if (rotation?.StartDate != null)
            {
                period = rotation.StartDate.Value.FormatWithMonth() + " - " +
                            rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithMonth();
            }

            return period;
        }
    }
}
