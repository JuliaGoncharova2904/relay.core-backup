﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Relay.BLL.Extensions.EntityExtensions
{
    public static class ProjectExtensions
    {
        public static ProjectStatus ProjectStatus(this Project project)
        {
            DateTime endDate = project.EndDate;
            DateTime startDate = project.StartDate;
            DateTime currentDate = DateTime.UtcNow.Date;

            if (startDate.Date > currentDate)
                return Relay.ProjectStatus.NotStarted;
            else if (startDate.Date <= currentDate && endDate.Date >= currentDate)
                return Relay.ProjectStatus.Active;
            else if (endDate.Date < currentDate)
                return Relay.ProjectStatus.Complete;
            else
                throw new Exception("Bad project dates.");
        }
    }
}
