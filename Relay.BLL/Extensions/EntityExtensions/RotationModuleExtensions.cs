﻿using System;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.BLL.Extensions.EntityExtensions
{
    public static class RotationModuleExtensions
    {  
        public static Guid GetModuleOwner(this RotationModule module)
        {
            UserProfile owner = null;

            //--- shift or rotation ----
            if (module.RotationId.HasValue)
            {
                owner = module.Rotation.RotationOwner;
            }
            else if (module.ShiftId.HasValue)
            {
                owner = module.Shift.Rotation.RotationOwner;
            }

            return owner.Id;
        }
    }
}
