﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;

namespace MomentumPlus.Relay.BLL.Extensions
{
    public static class EnumerableExtentions
    {
        public static IEnumerable<T> NotNull<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.Where(x => x != null);
        }

        public static IEnumerable<T> Page<T>(this IEnumerable<T> query, int page, int pageSize)
        {
            if (page < 1 || pageSize < 1)
                return null;

            return query.Skip(page * pageSize - pageSize).Take(pageSize);
        }

        public static IOrderedEnumerable<T> OrderBy<T, A>(this IEnumerable<T> query, Func<T, A> expression, SortDirection direction)
        {
            return direction == SortDirection.Ascending ? query.OrderBy<T, A>(expression) : query.OrderByDescending(expression);
        }

        public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (T item in enumeration)
            {
                action(item);
                yield return item;
            }
        }
    }
}
