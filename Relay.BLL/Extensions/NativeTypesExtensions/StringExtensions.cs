﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.BLL.Extensions.NativeTypesExtensions
{
    public static class StringExtensions
    {
        public static bool ToBool(this string stringVal)
        {
            var boolValue = false;
            if (bool.TryParse(stringVal, out boolValue))
            {
                return boolValue;
            }

            return boolValue;
        }
    }
}
