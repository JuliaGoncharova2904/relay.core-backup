﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Helpers;
using MomentumPlus.Relay.Constants;

namespace MomentumPlus.Relay.BLL.Extensions
{
    public static class QueryableExtension
    {
        public static IQueryable<T> Page<T>(this IOrderedQueryable<T> query, int page, int? pageSize)
        {

           var collectionSize = pageSize ?? NumericConstants.Paginator.PaginatorPageSize;

            if (page < 1 || pageSize < 1)
                return null;

            return query.Skip((page - 1) * collectionSize).Take(collectionSize);
        }

        public static IOrderedQueryable<T> OrderBy<T, A>(this IQueryable<T> query, Expression<Func<T, A>> expression, SortDirection direction)
        {
            return direction == SortDirection.Ascending ? query.OrderBy<T, A>(expression) : query.OrderByDescending(expression);
        }
    }
}
