﻿using System.Collections.Generic;
using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Relay.BLL.Extensions
{
    public class UserDistinctItemComparerExtension : IEqualityComparer<UserProfile>
    {

        public bool Equals(UserProfile x, UserProfile y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(UserProfile obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
