﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class HomeController : BaseController
    {
        //IOC
        public HomeController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }


        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            var isMultiTenant = base.IsMultiTenant;

            if (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && isMultiTenant)
            {
                return RedirectToAction("Index", "AdminDashboard");
            }


            Guid userId = Guid.Parse(User.Identity.GetUserId());
            Services.UserService.UpdateLastLoginDate(userId);


            if (User.IsInRole(iHandoverRoles.Relay.LineManager) || User.IsInRole(iHandoverRoles.Relay.HeadLineManager) || User.IsInRole(iHandoverRoles.Relay.SafetyManager))
            {
                return RedirectToAction("Index", "TeamRotations");
            }

            if (User.IsInRole(iHandoverRoles.Relay.Administrator) && isMultiTenant)
            {
                return RedirectToAction("Index", "TeamRotations");
            }

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && isMultiTenant)
            {
                return RedirectToAction("Index", "TeamRotations");
            }

            if (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && isMultiTenant)
            {
                return RedirectToAction("Index", "TeamRotations");
            }


            return RedirectToAction("Index", "Summary");
        }


        [HandleError(View = "CustomErrorView")]
        public ActionResult ThrowException()
        {
            return RedirectToAction("Index");
        }

        [HttpGet]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult InitializeRelay()
        {
            SubscriptionDetailsModel subscriptionInitializeModel = AppSession.Current.Subscription;

            if (subscriptionInitializeModel != null)
            {
                Services.UserService.InitializeSubscriptionManagerProfile(subscriptionInitializeModel);

                return Json(new { status = true, data = new { message = "Registration Success" } }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = false, data = new { message = "Relay not Initialize. App Session empty." } }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult InlineManualPeopleTracking()
        {
            var identityUserId = Guid.Parse(User.Identity.GetUserId());

            var model = Services.UserService.GetUserInfoForInlineManual(identityUserId);

            return PartialView("~/Views/Shared/_InlineManualPeopleTracking.cshtml", model);
        }
    }
}