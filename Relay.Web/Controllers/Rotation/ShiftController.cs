﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Web.Configuration;
using Rotativa;
using Rotativa.Options;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftController : BaseController
    {
        //IOC
        public ShiftController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(Guid? rotationId)
        {
            return RedirectToAction("Index", "Summary", new { rotationId });
        }


        #region Confirm Shift

        [HttpGet]
        [Obsolete]
        public ActionResult ConfirmShiftDialogOld(Guid? userId)
        {
            Guid shiftOwnerId = userId ?? Guid.Parse(User.Identity.GetUserId());

            ConfirmShiftViewModel model = Services.ShiftService.PopulateConfirmShiftViewModel(shiftOwnerId);

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmShiftDialog.cshtml", model);
        }


        [HttpGet]
        public ActionResult ConfirmShiftDialog(Guid? userId)
        {
            Guid shiftOwnerId = userId ?? Guid.Parse(User.Identity.GetUserId());

            ConfirmShiftV2ViewModel model = Services.ShiftService.PopulateConfirmShiftV2ViewModel(shiftOwnerId);

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmShiftV2Dialog.cshtml", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmShiftDialog(ConfirmShiftV2ViewModel model)
        {
            if (ModelState.IsValid)
            {
                var serverTime = Services.AdministrationService.GetCurrentServerTime(Guid.Parse(User.Identity.GetUserId()));

                var shiftEndTime = serverTime.Date.AddHours(model.ShiftEndHour.Value).AddMinutes(model.ShiftEndMinutes.Value).AddDays(model.ShiftEndType == ShiftEndType.Today ? 0 : 1);

                if (serverTime > shiftEndTime)
                {
                    ModelState.AddModelError("Confirm", "Start shift date сan not be less than End shift date");
                }

                else if (shiftEndTime < serverTime)
                {
                    ModelState.AddModelError("Confirm", "End shift date сan not be in the past");
                }
                else
                {
                    model.ShiftPatternType = Services.AdministrationService.GetUserPattern(model.ShiftOwnerId);

                    if (model.ShiftPatternType == "1")
                    {
                        serverTime = shiftEndTime.AddHours(-12);
                    }
                    if (model.ShiftPatternType == "2")
                    {
                        serverTime = shiftEndTime.AddHours(-8);
                    }
                        
                    Services.ShiftService.ConfirmShiftV2(model.ShiftOwnerId, serverTime, shiftEndTime, Guid.Parse(User.Identity.GetUserId()));

                    return Content("ok");
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmShiftV2Dialog.cshtml", model);
        }

        [HttpPost]
        [Obsolete]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmShiftDialogOld(ConfirmShiftViewModel model)
        {
            if (ModelState.IsValid)
            {
                var serverCurrentTime = Services.AdministrationService.GetCurrentServerTime(Guid.Parse(User.Identity.GetUserId()));

                if (model.StartShiftDate > model.EndShiftDate)
                {
                    ModelState.AddModelError("StartShiftDate", "Start shift date сan not be less than End shift date");
                }
                else if (model.EndShiftDate > model.NextShiftDate)
                {
                    ModelState.AddModelError("EndShiftDate", "End shift date сan not be less than end Next shift date");
                }
                else if (model.EndShiftDate < serverCurrentTime)
                {
                    ModelState.AddModelError("EndShiftDate", "End shift date сan not be in the past");
                }
                else if (model.NextShiftDate < serverCurrentTime)
                {
                    ModelState.AddModelError("NextShiftDate", "Next shift date сan not be in the past");
                }
                else
                {
                    Guid userId = Guid.Parse(User.Identity.GetUserId());
                    Services.ShiftService.ConfirmShift(userId, model);

                    return Content("ok");
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmShiftDialog.cshtml", model);
        }

        #endregion

        #region Shift Recipient

        [HttpGet]
        public ActionResult ChooseShiftRecipient(Guid shiftId)
        {
            ChooseShiftRecipientModel model = Services.ShiftService.PopulateChooseShiftRecipientViewModel(shiftId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_HandoverRecipientsDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChooseShiftRecipient(ChooseShiftRecipientModel model)
        {
            if (ModelState.IsValid)
            {
                Services.ShiftService.UpdateShiftRecipient(model);

                return Content("ok");
            }

            model.TeamMembers = Services.ShiftService.PopulateChooseShiftRecipientViewModel(model.ShiftId).TeamMembers;

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_HandoverRecipientsDialog.cshtml", model);
        }


        #endregion

        #region Edit Shift

        [HttpGet]
        public ActionResult EditShiftForm(Guid shiftId)
        {
            EditShiftDatesV2ViewModel model = Services.ShiftService.PopulateEditShiftModel(shiftId);

            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditShiftForm.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditShiftForm(EditShiftDatesV2ViewModel model)
        {
            if (ModelState.IsValid)
            {
                var serverCurrentTime = Services.AdministrationService.GetCurrentServerTime(Guid.Parse(User.Identity.GetUserId()));

                var shiftEndTime = model.EndShiftDate.Date.AddHours(model.ShiftEndHour).AddMinutes(model.ShiftEndMinutes);

                if (shiftEndTime < serverCurrentTime)
                {
                    ModelState.AddModelError("EndShiftDate", "End Shift Time has to be great or equal current Time.");
                }
                else
                {
                    Services.ShiftService.UpdateShiftEndTime(model.ShiftId, shiftEndTime);
                    return Content("ok");
                }

               // return PartialView("~/Views/Rotation/Confirmation/Edit/_EditShiftForm.cshtml", model);

            }

            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditShiftForm.cshtml", model);
        }

        #endregion

        #region Finish Shift

        [HttpPost]
        public ActionResult FinishShift(Guid shiftId)
        {
            string footerUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/GlobalReports/PdfFooter";

            string footerHtml = String.Format(" --footer-html {0} " +
                                              "--enable-local-file-access" +
                                              " --enable-javascript" +
                                              " --footer-spacing 1 " +
                                              " --encoding utf-8", footerUrl);

            var model = Services.ReportService.PopulateHandoverReportViewModel(shiftId, ModuleSourceType.Draft, true, false);

            model.ShiftType = Services.ShiftService.GetShiftType(shiftId);

            var pdfResult = new ViewAsPdf("~/Views/Reports/HandoverReport/PrintHandoverReport.cshtml", model)
            {
                FileName = "Rotation-Report.pdf",
                PageOrientation = Orientation.Portrait,
                MinimumFontSize = 16,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Margins(10, 10, 10, 10)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            //  var filePDF =  File(binary, "application/pdf");

            Services.ShiftService.ExpireShift(shiftId, binary);

            return Content("ok");
        }


        #endregion
    }
}