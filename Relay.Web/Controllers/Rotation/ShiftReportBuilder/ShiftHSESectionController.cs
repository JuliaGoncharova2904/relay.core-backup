﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;
using MomentumPlus.Relay.Roles;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftHSESectionController : BaseController
    {
        // IOC
        public ShiftHSESectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsTemplateModuleEnabled(ModuleType.HSE, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {
                IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                           .GetHseModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                           .OrderBy(h => h.Type);

                if (filter == HandoverReportFilterType.NrItems)
                {
                    return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSE_NR_DraftSection.cshtml", model);
                }

                return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSEDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public async Task<ActionResult> DraftAsync(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsTemplateModuleEnabled(ModuleType.HSE, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {
                IEnumerable<HSETopicViewModel> model = await Task.Run(() => Services.RotationModuleService
                    .GetHseModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                    .OrderBy(h => h.Type));

                if (filter == HandoverReportFilterType.NrItems)
                {
                    return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSE_NR_DraftSection.cshtml", model);
                }

                return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSEDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }



        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            if (Services.RotationModuleService.IsTemplateModuleReceivedEnabled(receivedShiftId.Value, ModuleType.HSE, ModuleSourceType.Draft, RotationType.Shift))
            {
                IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                           .GetHseModuleTopicsForReceivedType(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Received, RotationType.Shift, HandoverReportFilterType.All)
                                                           .OrderBy(h => h.Type);
   
                //if (receivedShiftId.HasValue)
                //{
                //    model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
                //    var res1 = model.ToList();
                //}

                return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSEReceivedSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult UpdateTopic(HSETopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateHSETopic(model, Guid.Parse(User.Identity.GetUserId()));
            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                HSETopicViewModel model = Services.RotationTopicService.GetHSETopic(topicId);

                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic Dialog

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult EditTopicDialog(Guid topic)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            HSETopicDialogViewModel model = Services.RotationTopicService.PopulateHSETopicDialogModel(topic, userId);

            model.IsManagerCommentsEnabled = !Services.RotationTopicService.IsMyShiftTopic(userId, topic)
                                             && (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.SafetyManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(HSETopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Safety Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateHSETopic(model, Guid.Parse(User.Identity.GetUserId()));
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateHSETopicDialogModel((Guid)model.Id, model.CreatorId);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var modules = Services.RestrictionService.GetModulesByRotationId(destId, RotationType.Shift);

            var items = modules?.Where(m => m.Enabled).SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                      .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).ToList();

            Services.RestrictionService.HandoverItemsLimitExceededNew(destId, RotationType.Shift, items.Count);

            var moduleId = modules.Find(m => m.Type == Core.Models.TypeOfModule.HSE && !m.DeletedUtc.HasValue).Id;

            ShiftHSEAddInlineTopicViewModel model = Services.RotationTopicService.PopulateShiftHSETopicInlineModel(destId, moduleId);

            model.CreatorId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Rotation/ShiftReportBuilder/HSESection/_HSEAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftHSEAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Type, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddShiftHSETopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}