﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using Rotativa;
using System.Net;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Mailer.Extensions;
using System.Web.UI;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers.Rotation.ShiftReportBuilder
{
    public class ShiftReportBuilderController : BaseController
    {
        public ShiftReportBuilderController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        [OutputCache(Duration = 3)]
        public ActionResult Index(Guid? shiftId, HandoverReportFilterType? filter)
        {
            if (shiftId.HasValue)
            {
                var shift = Services.ShiftService.GetShift(shiftId.Value);

                ShiftReportPanelViewModel model = new ShiftReportPanelViewModel
                {
                    FilterType = filter ?? HandoverReportFilterType.All,
                    ShiftId = shiftId,
                   HandoverCreatorOrRecipientName = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipient.FullName : null,
                   ShareEnabled = Services.AuthService.GetSubscriptionType(shift.Rotation.RotationOwnerId) == Authorization.Domain.Entities.RestrictionType.Basic ? false : true,
                   IconsPDFEnabled = Services.AuthService.GetSubscriptionType(shift.Rotation.RotationOwnerId) == Authorization.Domain.Entities.RestrictionType.Basic ? false : true
                };

                model.UserId = shift.Rotation.RotationOwnerId;
                model.ShiftType = Services.ShiftService.GetShiftType(shift);
                //model.IsActiveShift = Services.RotationService.IsShowConfirmButton(shift.Rotation.RotationOwnerId);

                if (!shift.ShiftRecipientId.HasValue)
                {
                    return PartialView("~/Views/Rotation/Shared/_EmptyReportBuilder.cshtml", "Handover recipient has not been selected.");
                }

                if (shift.State == ShiftState.Finished || shift.State == ShiftState.Break)
                {
                    return PartialView("~/Views/Rotation/ShiftReportBuilder/_ReadModeShiftReportBuilderPanel.cshtml", model);
                }

                return PartialView("~/Views/Rotation/ShiftReportBuilder/_ShiftReportBuilderPanel.cshtml", model);
            }

            return PartialView("~/Views/Rotation/Shared/_EmptyReportBuilder.cshtml", "Start a new handover to view any reports that you received while you were away.");
        }

        #region Draft Panel

        [OutputCache(Duration = 3)]
        public ActionResult DraftPanel(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            var shift = Services.ShiftService.GetShift(shiftId);

            ShiftDraftPanelViewModel model = new ShiftDraftPanelViewModel
            {
                RotationId = shift.RotationId,
                Filter = filter,
                ShiftId = shiftId,
                ShiftPeriod = shift.StartDateTime.HasValue ? shift.StartDateTime.Value.FormatWithDayMonthYear() : "...",
                HandoverRecipientName = shift.ShiftRecipientId.HasValue ? shift.ShiftRecipient.FullName : null
            };

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_DraftPanel.cshtml", model);
        }

        public ActionResult ReportFilterPopover(Guid shiftId, HandoverReportFilterType filter)
        {
            ShiftReportBuilderFilterViewModel model = new ShiftReportBuilderFilterViewModel
            {
                Id = shiftId,
                Filter = filter
            };

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_ShiftReportFilterPopover.cshtml", model);
        }


        #endregion

        #region Received Panel

        [HttpGet]
        public ActionResult ReceivedPanel(Guid shiftId, Guid? recivedShiftId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            ShiftReceivedPanelViewModel model = new ShiftReceivedPanelViewModel
            {
                ShiftId = shiftId,
                RecivedShiftId = recivedShiftId,
                RecivedShifts = Services.ShiftService.PopulateReceivedShiftsPanel(shiftId, userId)
            };

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_ReceivedPanel.cshtml", model);
        }

        [OutputCache(Duration = 5)]
        public ActionResult FromToBlock(Guid rotationId)
        {
            if (rotationId != null)
            {
                FromToBlockViewModel model = Services.RotationService.GetFromToBlockShift(rotationId);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/_DraftFromToBlock.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public ActionResult ReceivedPanelSections(Guid shiftId, Guid? recivedShiftId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            ShiftReceivedPanelViewModel model = new ShiftReceivedPanelViewModel
            {
                ShiftId = shiftId,
                RecivedShiftId = recivedShiftId,
                RecivedShifts = Services.ShiftService.PopulateReceivedShiftsPanel(shiftId, userId)
            };

            return PartialView("~/Views/Rotation/ShiftReportBuilder/_ReceivedPanelSections.cshtml", model);
        }

        #endregion

        #region Report Preview

        public ActionResult ReportPreview(Guid shiftId, bool isReceived = false, Guid? selectedShiftId = null)
        {
            ShiftReportPreviewViewModel viewModel = Services.ReportService.PopulateShiftReportViewModel(shiftId,
                                                                                    isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft,
                                                                                    selectedShiftId);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/ReportPreview/_ReportPreview.cshtml", viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ReportPrint(Guid shiftId, bool isReceived = false, Guid? selectedShiftId = null)
        {
            string footerUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/ShiftReportBuilder/PdfFooter";

            string footerHtml = String.Format(" --footer-html {0} " +
                                "--enable-local-file-access" +
                                " --enable-javascript" +
                                " --footer-spacing -10 " +
                                " --encoding utf-8", footerUrl);

            ShiftReportPreviewViewModel model= Services.ReportService.PopulateShiftReportViewModel(shiftId, isReceived ? ModuleSourceType.Received 
                                                                                                                       : ModuleSourceType.Draft, selectedShiftId);

            var pdfResult = new ViewAsPdf("~/Views/Rotation/RotationReportBuilder/ReportPreview/_ReportPrint.cshtml", model)
            {
                FileName = "Shift-Report.pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Rotativa.Options.Margins(0, 0, 0, 0)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            return File(binary, "application/pdf");
        }

        [AllowAnonymous]
        public ActionResult ShiftReport(Guid shiftId, bool isReceived = false, Guid? selectedShiftId = null)
        {
            ShiftReportPreviewViewModel viewModel = Services.ReportService.PopulateShiftReportViewModel(shiftId, isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft,
                                                                                                        selectedShiftId);

            return View("~/Views/Rotation/ShiftReportBuilder/ReportPreview/_ReportPrint.cshtml", viewModel);
        }


        [AllowAnonymous]
        public ActionResult PdfFooter()
        {
            return PartialView("~/Views/Shared/Layouts/Base/_PrintFooter.cshtml");
        }

        #endregion

        #region Finalize All

        [HttpPost]
        public ActionResult FinalizeAll(Guid shiftId)
        {
            Services.ShiftService.SetFinalizeStatusForDraftItems(shiftId, FinalizeStatus.Finalized);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion
    }
}