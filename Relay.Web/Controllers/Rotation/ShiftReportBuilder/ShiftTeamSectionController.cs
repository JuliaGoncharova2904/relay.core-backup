﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;
using MomentumPlus.Relay.Roles;
using System.Threading.Tasks;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftTeamSectionController : BaseController
    {
        // IOC
        public ShiftTeamSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsModuleEnabled(ModuleType.Team, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {

                IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                                .GetTeamModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                                .OrderBy(t => t.TeamMember);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public async Task<ActionResult> DraftAsync(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsTemplateModuleEnabled(ModuleType.HSE, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {

                IEnumerable<TeamTopicViewModel> model = await Task.Run(() => Services.RotationModuleService
                                                            .GetTeamModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                            .OrderBy(h => h.TeamMember));

                return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                            .GetTeamModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Received, RotationType.Shift)
                                                            .OrderBy(t => t.TeamMember);

            //if (receivedShiftId.HasValue)
            //{
            //    model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            //}

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(TeamTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateTeamTopic(model, Guid.Parse(User.Identity.GetUserId()));

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                TeamTopicViewModel model = Services.RotationTopicService.GetTeamTopic(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult EditTopicDialog(Guid topic)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            TeamTopicDialogViewModel model = Services.RotationTopicService.PopulateShiftTeamTopicDialogModel(topic, userId);

            model.IsManagerCommentsEnabled = !Services.RotationTopicService.IsMyShiftTopic(userId, topic)
                                             && (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser)
                                                 || User.IsInRole(iHandoverRoles.Relay.SafetyManager));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(TeamTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExistByTopic((Guid)model.Id, (Guid)model.RelationId, model.Name))
                    {
                        ModelState.AddModelError("Name", "Topic Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateTeamTopic(model, Guid.Parse(User.Identity.GetUserId()));
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateShiftTeamTopicDialogModel((Guid)model.Id, model.CreatorId);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var modules = Services.RestrictionService.GetModulesByRotationId(destId, RotationType.Shift);

            var items = modules?.Where(m => m.Enabled).SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                      .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).ToList();

            Services.RestrictionService.HandoverItemsLimitExceededNew(destId, RotationType.Shift, items.Count);

            var teamId = modules.FirstOrDefault(t => t.ShiftId.HasValue).Shift.Rotation.RotationOwner.TeamId;

            ShiftTeamAddInlineTopicViewModel model = Services.RotationTopicService.PopulateShiftTeamTopicInlineModel(destId, teamId);

            model.CreatorId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TeamSection/_TeamAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftTeamAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExist(model.ShiftId.Value, (Guid)model.TeamMember, model.Reference, RotationType.Shift))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }
                    else
                    {
                        Services.RotationTopicService.AddShiftTeamTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}