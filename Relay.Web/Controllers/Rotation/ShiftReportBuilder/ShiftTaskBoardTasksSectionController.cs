﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftTaskBoardTasksSectionController : BaseController
    {
        // IOC
        public ShiftTaskBoardTasksSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<TaskBoardTaskForHandover> model = Services.TaskBoardTaskService
                                                                .GetTaskBoardTasksForShift(shiftId, ModuleSourceType.Draft)
                                                                .OrderBy(t => t.Name);

            return PartialView("~/Views/Rotation/ShiftReportBuilder/TaskBoardTasks/_TaskBoardTasksDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<TaskBoardTaskForHandover> model = Services.TaskBoardTaskService
                                                                .GetTaskBoardTasksForShift(shiftId, ModuleSourceType.Received, receivedShiftId)
                                                                .OrderBy(t => t.Name);


            return PartialView("~/Views/Rotation/ShiftReportBuilder/TaskBoardTasks/_TaskBoardTasksReceivedSection.cshtml", model);
        }


        [HttpPost]
        public ActionResult ReloadTask(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                TaskBoardTaskForHandover model = Services.TaskBoardTaskService.GetTask(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult UpdateTask(TaskBoardTaskForHandover model)
        {

            return Json(model);
        }


    }
}