﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;
using MomentumPlus.Relay.Roles;
using System.Threading.Tasks;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftCoreSectionController : BaseController
    {
        // IOC
        public ShiftCoreSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<CoreTopicViewModel> model = Services.RotationModuleService
                                                            .GetCoreModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                            .OrderBy(c => c.Category).OrderByDescending(t => t.IsFirstTopicByTopicGroupName.HasValue);

            if (filter == HandoverReportFilterType.NrItems)
            {
                return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_Core_NR_DraftSection.cshtml", model);
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreDraftSection.cshtml", model);
        }

        [HttpGet]
        public async Task<ActionResult> DraftAsync(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsTemplateModuleEnabled(ModuleType.HSE, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {

                IEnumerable<CoreTopicViewModel> model = await Task.Run(() => Services.RotationModuleService
                                                            .GetCoreModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                            .OrderBy(h => h.Category));

                if (filter == HandoverReportFilterType.NrItems)
                {
                    return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_Core_NR_DraftSection.cshtml", model);
                }

                return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<CoreTopicViewModel> model = Services.RotationModuleService
                                                            .GetCoreModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Received, RotationType.Shift, HandoverReportFilterType.All)
                                                            .OrderBy(c => c.Category);
            //if (receivedShiftId.HasValue)
            //{
            //    model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            //}

            return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(CoreTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateCoreTopic(model, Guid.Parse(User.Identity.GetUserId()));

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                CoreTopicViewModel model = Services.RotationTopicService.GetCoreTopic(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult EditTopicDialog(Guid topic)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            CoreTopicDialogViewModel model = Services.RotationTopicService.PopulateCoreTopicDialogModel(topic, userId);

            model.IsManagerCommentsEnabled = !Services.RotationTopicService.IsMyShiftTopic(userId, topic)
                                             && (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.SafetyManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreTopicFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(CoreTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Core Topic with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateCoreTopic(model, Guid.Parse(User.Identity.GetUserId()));
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateCoreTopicDialogModel((Guid)model.Id, model.CreatorId);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var modules = Services.RestrictionService.GetModulesByRotationId(destId, RotationType.Shift);

            var items = modules?.Where(m => m.Enabled).SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                      .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).ToList();

            Services.RestrictionService.HandoverItemsLimitExceededNew(destId, RotationType.Shift, items.Count);

            var moduleId = modules.Find(m => m.Type == Core.Models.TypeOfModule.Core && !m.DeletedUtc.HasValue).Id;

            ShiftCoreAddInlineTopicViewModel model = Services.RotationTopicService.PopulateShiftCoreTopicInlineModel(destId, moduleId);

            model.CreatorId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Rotation/ShiftReportBuilder/CoreSection/_CoreAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftCoreAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.ProcessGroup, Guid.NewGuid(), model.ProcessLocation))
                    {
                        ModelState.AddModelError("ProcessLocation", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddShiftCoreTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}