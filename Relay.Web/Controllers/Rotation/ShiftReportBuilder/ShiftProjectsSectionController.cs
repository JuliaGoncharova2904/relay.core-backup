﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;
using MomentumPlus.Relay.Roles;
using System.Threading.Tasks;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShiftProjectsSectionController : BaseController
    {
        // IOC
        public ShiftProjectsSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsModuleEnabled(ModuleType.Project, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {
                IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                    .GetProjectModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                                    .OrderBy(p => p.Project);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectsDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public async Task<ActionResult> DraftAsync(Guid shiftId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsTemplateModuleEnabled(ModuleType.HSE, shiftId, ModuleSourceType.Draft, RotationType.Shift))
            {

                IEnumerable<ProjectsTopicViewModel> model = await Task.Run(() => Services.RotationModuleService
                                                            .GetProjectModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Draft, RotationType.Shift, filter)
                                                            .OrderBy(h => h.Project));

                return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectsDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid shiftId, Guid? receivedShiftId)
        {
            IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                .GetProjectModuleTopics(Guid.Parse(User.Identity.GetUserId()), shiftId, ModuleSourceType.Received, RotationType.Shift)
                                                                .OrderBy(p => p.Project);

            //if (receivedShiftId.HasValue)
            //{
            //    model = model.Where(m => m.FromShiftId == receivedShiftId.Value);
            //}

            return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectsReceivedSection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(ProjectsTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateProjectTopic(model, Guid.Parse(User.Identity.GetUserId()));

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                ProjectsTopicViewModel model = Services.RotationTopicService.GetProjectTopic(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult EditTopicDialog(Guid topic)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            ProjectTopicDialogViewModel model = Services.RotationTopicService.PopulateProjectTopicDialogModel(topic, userId);

            model.IsManagerCommentsEnabled = !Services.RotationTopicService.IsMyShiftTopic(userId, topic)
                                             && (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser)
                                                 || User.IsInRole(iHandoverRoles.Relay.SafetyManager));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(ProjectTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Project Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateProjectTopic(model, Guid.Parse(User.Identity.GetUserId()));
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateProjectTopicDialogModel((Guid)model.Id, model.CreatorId);

                return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var modules = Services.RestrictionService.GetModulesByRotationId(destId, RotationType.Shift);

            var items = modules?.Where(m => m.Enabled).SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                      .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).ToList();

            Services.RestrictionService.HandoverItemsLimitExceededNew(destId, RotationType.Shift, items.Count);

            var moduleId = modules.Find(m => m.Type == Core.Models.TypeOfModule.Project && !m.DeletedUtc.HasValue).Id;

            ShiftProjectAddInlineTopicViewModel model = Services.RotationTopicService.PopulateShiftProjectTopicInlineModel(destId, moduleId);

            model.CreatorId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Rotation/ShiftReportBuilder/ProjectsSection/_ProjectAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ShiftProjectAddInlineTopicViewModel model)
        {
            if (model.ShiftId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Project, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddShiftProjectTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}