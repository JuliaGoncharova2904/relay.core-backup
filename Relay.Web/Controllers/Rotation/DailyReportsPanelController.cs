﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers.Rotation
{
    public class DailyReportsPanelController : BaseController
    {
        // IOC
        public DailyReportsPanelController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult DailyReportsPanel(Guid userId)
        {
            Guid currentUserId = Guid.Parse(User.Identity.GetUserId());

            string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];

            DailyReportsPanelViewModel model = Services.DailyReportService.PopulateDailyReportsPanelViewModel(userId, userId == currentUserId, safetyVersion);

            var subscriptionType = Services.AuthService.GetSubscriptionType(userId);

            if (model.DailyReports.Any())
            {

                if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    //foreach (var dailyReport in model.DailyReports)
                    //{
                    //    var daysAccessHistory = DaysAccessHistoryForBasicLevel(dailyReport.StartDateTime);
                    //    model.DailyReports = model.DailyReports.Where(n => daysAccessHistory);
                    //}

                    model.DailyReports.ToList().ForEach(dailyReport =>
                    {
                        model.DailyReports = model.DailyReports.Where(n => DaysAccessHistoryForBasicLevel(dailyReport.StartDateTime));
                    });
                }

                else if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Premium)
                {
                    //foreach (var dailyReport in model.DailyReports)
                    //{
                    //    var daysAccessHistory = DaysAccessHistoryForPremiumLevel(dailyReport.StartDateTime);
                    //    model.DailyReports = model.DailyReports.Where(n => daysAccessHistory);
                    //}

                    model.DailyReports.ToList().ForEach(dailyReport =>
                    {
                        model.DailyReports = model.DailyReports.Where(n => DaysAccessHistoryForPremiumLevel(dailyReport.StartDateTime));
                    });
                }

               // model.IsActiveShift = Services.RotationService.IsShowConfirmButton(userId);

                return PartialView("~/Views/Rotation/DailyReportsPanel/_DailyReportsPanel.cshtml", model);
            }

            return new EmptyResult();
        }

        private bool DaysAccessHistoryForBasicLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddMonths(1);
            if (timeStamp <= daysAccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool DaysAccessHistoryForPremiumLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddMonths(6);
            if (timeStamp <= daysAccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        [HttpGet]
        public ActionResult DailyReportSlide(Guid shiftId)
        {
            string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];

            DailyReportViewModel model = Services.DailyReportService
                .PopulateDailyReportViewModel(shiftId, Guid.Parse(User.Identity.GetUserId()), safetyVersion);

            return PartialView("~/Views/Rotation/DailyReportsPanel/_DailyReportSlide.cshtml", model);
        }

        [HttpGet]
        public ActionResult CommonReportSlide(Guid userId)
        {
            string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];
            int safetyMessagesCounter = Services.DailyReportService.GetUnreadSafetyMessages(userId, safetyVersion);

            return PartialView("~/Views/Rotation/DailyReportsPanel/_CommonReportSlide.cshtml", safetyMessagesCounter);
        }
    }
}