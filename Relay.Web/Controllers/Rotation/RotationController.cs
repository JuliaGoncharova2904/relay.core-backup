﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Net;
using Microsoft.AspNet.Identity;
using MomentumPlus.Core.Models;
using System.Collections.Generic;
using Rotativa;
using Rotativa.Options;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class RotationController : BaseController
    {
        //IOC
        public RotationController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(Guid? userId)
        {
            return RedirectToAction("Index", "Summary", new { userId });
        }

        #region Test functions

        public ActionResult ExpireRotation(Guid? userId, Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var expired = Services.RotationService.ExpireRotation((Guid)rotationId);

                if (expired)
                {
                    return RedirectToAction("Index", "Summary", new { userId });
                }
            }

            return RedirectToAction("Index", "Summary", new { userId });
        }

        public ActionResult EndSwingOfRotation(Guid? userId, Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var ended = Services.RotationService.EndSwingOfRotation(rotationId.Value);

                if (ended)
                {
                    return RedirectToAction("Index", "Summary", new { userId });
                }
            }

            return RedirectToAction("Index", "Summary", new { userId });
        }

        [HttpPost]
        public ActionResult FinishSwing(Guid? userId)
        {
            Guid? rotationId = Services.RotationService.GetCurrentRotationIdForUser(userId.Value);

            string footerUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/GlobalReports/PdfFooter";

            string footerHtml = String.Format(" --footer-html {0} " +
                                              "--enable-local-file-access" +
                                              " --enable-javascript" +
                                              " --footer-spacing 1 " +
                                              " --encoding utf-8", footerUrl);

            var model = Services.ReportService.PopulateHandoverReportViewModel(rotationId.Value, ModuleSourceType.Draft, false, true);

            var pdfResult = new ViewAsPdf("~/Views/Reports/HandoverReport/PrintHandoverReport.cshtml", model)
            {
                FileName = "Rotation-Report.pdf",
                PageOrientation = Orientation.Portrait,
                MinimumFontSize = 16,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Margins(10, 10, 10, 10)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            Services.RotationService.EndSwing((Guid)rotationId, binary);
            return Content("ok");
        }

        #endregion


        #region Check current period state

        public ActionResult ChooseShiftRecipientWindow()
        {
            Guid employeeId = Guid.Parse(User.Identity.GetUserId());
            var user = RouteData.Values["userId"];

            if (user != null)
            {
                employeeId = Guid.Parse(user.ToString());
            }

            Guid? rotationId = Services.RotationService.GetCurrentRotationIdForUser(employeeId);

            if (rotationId.HasValue)
            {
                if (Services.RotationService.IsShiftRotation(rotationId.Value))
                {
                    Guid? shiftId = Services.ShiftService.GetCurrentShiftIdForRotation(rotationId.Value);
                    if (shiftId.HasValue && !Services.ShiftService.ShiftHasRecipient(shiftId.Value))
                    {
                        return PartialView("~/Views/Rotation/Confirmation/_ChooseShiftRecipient.cshtml", shiftId.Value);
                    }
                }
            }
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult NotBackOnSite()
        {
            OffSiteUserAsked = true;
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #endregion

        #region Confirm Rotation

        public ActionResult ConfirmRotationDialog(Guid? userId)
        {
            Guid employeeId = userId ?? Guid.Parse(User.Identity.GetUserId());

            ConfirmRotationViewModel model = Services.RotationService
                                                     .PopulateConfirmModelWithCurrentRotation(employeeId)
                                                     .FillRepeatRotationRange();

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmRotationDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmRotationDialog(ConfirmRotationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.ConfirmState != ConfirmRotationState.AcceptCustomPatern && !Services.RotationService.CheckRotationPatternCompatibility(model.UserId, model))
                {
                    ModelState.Remove("ConfirmState");
                    model.ConfirmState = ConfirmRotationState.RequestCustomPatern;
                }
                else
                {
                    Guid identityUserId = Guid.Parse(User.Identity.GetUserId());

                    if (Services.RotationService.ConfirmRotation(model.UserId, model, identityUserId))
                    {
                        var rotationId = Services.RotationService.GetCurrentRotationIdForUser(model.UserId);

                        return RedirectToAction("SelectSwingRecipient", "Rotation", new { rotationId = rotationId });
                        //return Content("ok");
                    }
                    else
                    {
                        ModelState.Remove("ConfirmState");
                        model.ConfirmState = ConfirmRotationState.NoOperation;
                        ModelState.AddModelError("Confirm", "Working Pattern was not confirmed.");
                    }
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/_ConfirmRotationDialog.cshtml", model.FillRepeatRotationRange());
        }

        #endregion

        #region Edit Rotation

        [HttpGet]
        public ActionResult EditSwingShiftDialog(Guid userId)
        {
            Guid? rotationId = Services.RotationService.GetCurrentRotationIdForUser(userId);

            var rotation = Services.RotationService.GetCurrentRotationForUser(userId);

            if (rotation != null && rotation.State == RotationState.Confirmed)
            {
                if (rotation.RotationType == Core.Models.RotationType.Shift)
                {
                    Guid? shiftId = Services.ShiftService.GetCurrentShiftIdForRotation(rotation);

                    if (shiftId.HasValue)
                    {
                        return PartialView("~/Views/Rotation/Confirmation/Edit/_EditShiftDatesDialog.cshtml",
                            shiftId.Value);
                    }
                    else
                    {
                        return PartialView("~/Views/Rotation/Confirmation/_WarningEditDatesDialog.cshtml");
                    }
                }

                return PartialView("~/Views/Rotation/Confirmation/Edit/_EditRotationDatesDialog.cshtml", rotationId);
            }

            return PartialView("~/Views/Rotation/Confirmation/_WarningEditDatesDialog.cshtml");
        }


        [HttpGet]
        public ActionResult SelectSwingRecipient(Guid rotationId)
        {
            ChooseSwingRecipientModel model = Services.RotationService.PopulateChooseSwingRecipientViewModel(rotationId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/_SelectHandoverRecipient.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SelectSwingRecipient(ChooseSwingRecipientModel model)
        {
            if (ModelState.IsValid)
            {
                Services.RotationService.UpdateSwingRecipient(model);

                return Content("ok");
            }

            model.TeamMembers = Services.RotationService.PopulateChooseSwingRecipientViewModel(model.RotationId).TeamMembers;

            return PartialView("~/Views/Rotation/RotationReportBuilder/_SelectHandoverRecipient.cshtml", model);
        }


        [HttpGet]
        public ActionResult ChooseSwingRecipient(Guid rotationId)
        {
            ChooseSwingRecipientModel model = Services.RotationService.PopulateChooseSwingRecipientViewModel(rotationId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/_HandoverRecipientsDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChooseSwingRecipient(ChooseSwingRecipientModel model)
        {
            if (ModelState.IsValid)
            {
                Services.RotationService.UpdateSwingRecipient(model);

                return Content("ok");
            }

            model.TeamMembers = Services.RotationService.PopulateChooseSwingRecipientViewModel(model.RotationId).TeamMembers;

            return PartialView("~/Views/Rotation/RotationReportBuilder/_HandoverRecipientsDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditSwingForm(Guid rotationId)
        {
            EditRotationDatesViewModel model = Services.RotationService.GetRotationDates(rotationId);
            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditSwingForm.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSwingForm(EditRotationDatesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var serverCurrentTime = Services.AdministrationService.GetCurrentServerTime(Guid.Parse(User.Identity.GetUserId()));

                if (!model.EndSwingDateReadOnly && model.EndSwingDate.Value.Date < serverCurrentTime.Date)
                {
                    ModelState.AddModelError("EndSwingDate", "End Swing Date has to be great or equal current date.");
                }
                else if (model.BackOnSiteDate.Value.Date <= serverCurrentTime.Date)
                {
                    ModelState.AddModelError("BackOnSiteDate", "Back On Site Date has to be great then current date.");
                }
                else if (Services.RotationService.UpdateRotationDates(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("", "Working pattern was not confirmed.");
                }
            }

            return PartialView("~/Views/Rotation/Confirmation/Edit/_EditSwingForm.cshtml", model.FillRepeatRotationRange());
        }

        #endregion

    }
}