﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TasksSectionController : BaseController
    {
        // IOC
        public TasksSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<TasksTopicViewModel> model = Services.RotationTaskService
                                                                .GetAllRotationTasks(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft)
                                                                .OrderBy(t => t.Name);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TasksSection/_TasksDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable<TasksTopicViewModel> model = Services.RotationTaskService
                                                             .GetAllRotationTasks(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                             .OrderBy(t => t.Name);

            if (receivedRotationId.HasValue)
                model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TasksSection/_TasksReceivedSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<TasksTopicViewModel> model = Services.RotationTaskService
                                                             .GetAllRotationTasks(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft)
                                                             .OrderBy(t => t.Name);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TasksSection/_TasksHistorySection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(TasksTopicViewModel model)
        {
            Guid currentUserId = Guid.Parse(User.Identity.GetUserId());
            model = Services.RotationTaskService.UpdateTaskTopic(model, currentUserId);

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicid)
        {
            if (topicid != Guid.Empty)
            {
                TasksTopicViewModel model = Services.RotationTaskService.GetTaskTopic(topicid, SwingOwnerId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        public ActionResult GetEditCompleteMenuPopover(Guid taskId)
        {
            ReceivedEditCompleteViewModel model = new ReceivedEditCompleteViewModel
            {
                TaskId = taskId
            };
            return PartialView("~/Views/Rotation/RotationReportBuilder/_RotationEditCompleteForReceivedMenuPopover.cshtml", model);
        }
    }
}