﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;
using MomentumPlus.Relay.Roles;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TeamSectionController : BaseController
    {
        // IOC
        public TeamSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsModuleEnabled(ModuleType.Team, rotationId, ModuleSourceType.Draft))
            {

                IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                                .GetTeamModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft, RotationType.Swing, filter)
                                                                .OrderBy(t => t.TeamMember);

                return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                            .GetTeamModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                            .OrderBy(t => t.TeamMember);

            //if (receivedRotationId.HasValue)
            //    model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamReceivedSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<TeamTopicViewModel> model = Services.RotationModuleService
                                                            .GetTeamModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft)
                                                            .OrderBy(t => t.TeamMember);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamHistorySection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(TeamTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateTeamTopic(model, Guid.Parse(User.Identity.GetUserId()));

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                TeamTopicViewModel model = Services.RotationTopicService.GetTeamTopic(topicId);
                if(model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult EditTopicDialog(Guid topic)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            TeamTopicDialogViewModel model = Services.RotationTopicService.PopulateTeamTopicDialogModel(topic, userId);

            model.IsManagerCommentsEnabled = !Services.RotationTopicService.IsMyTopic(userId, topic)
                                             && (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.SafetyManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(TeamTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExistByTopic((Guid)model.Id, (Guid)model.RelationId, model.Name))
                    {
                        ModelState.AddModelError("Name", "Topic Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateTeamTopic(model, Guid.Parse(User.Identity.GetUserId()));
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateTeamTopicDialogModel((Guid)model.Id, model.CreatorId);

                return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var modules = Services.RestrictionService.GetModulesByRotationId(destId, RotationType.Swing);

            var items = modules?.Where(m => m.Enabled).SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                      .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).ToList();

            Services.RestrictionService.HandoverItemsLimitExceededNew(destId, RotationType.Swing, items.Count);

            var teamId = modules.FirstOrDefault(t => t.RotationId.HasValue).Rotation.RotationOwner.TeamId;

            TeamAddInlineTopicViewModel model = Services.RotationTopicService.PopulateTeamTopicInlineModel(destId, teamId);

            model.CreatorId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Rotation/RotationReportBuilder/TeamSection/_TeamAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(TeamAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicAssignedWithSameNameExist(model.RotationId.Value, (Guid)model.TeamMember, model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }
                    else
                    {
                        Services.RotationTopicService.AddTeamTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}