﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Net;
using System.Web.Mvc;
using MomentumPlus.Relay.Helpers;
using Rotativa;
using MomentumPlus.Relay.Mailer.Extensions;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class RotationReportBuilderController : BaseController
    {
        // IOC
        public RotationReportBuilderController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(Guid userId, HandoverReportFilterType? filter)
        {

            RotationReportPanelViewModel model = new RotationReportPanelViewModel
            {
                UserId = userId,
                FilterType = filter ?? HandoverReportFilterType.All,
                ShareEnabled = Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic ? false : true,
                IconsPDFEnabled = Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic ? false : true
            };

            var rotation = Services.RotationService.GetCurrentRotationForUser(userId);
            model.RotationId = rotation.Id;

            if (rotation.State == Core.Models.RotationState.Expired || rotation.State == Core.Models.RotationState.SwingEnded)
            {
                model.IsEndRotation = true;
            }

            if (Services.RotationService.IsRotationSwingEnd(rotation) || (rotation.State == Core.Models.RotationState.Created || rotation.State == Core.Models.RotationState.Expired || rotation.State == Core.Models.RotationState.SwingEnded))
            {
                return PartialView("~/Views/Rotation/RotationReportBuilder/_ReadModeSwingReportBuilderPanel.cshtml", model);
            }

            return PartialView("~/Views/Rotation/RotationReportBuilder/_RotationReportBuilderPanel.cshtml", model);
        }

        #region Draft panel
        /// <summary>
        /// Return draft panel for rotation.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="filter">Fiter Type</param>
        /// <returns></returns>
        public ActionResult DraftPanel(Guid userId, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems)
        {
            if (userId != Guid.Empty)
            {
                var rotation = Services.RotationService.GetCurrentRotationForUser(userId);

                DraftPanelViewModel model = new DraftPanelViewModel
                {
                    RotationId = rotation.Id,
                    Filter = filter,
                    RotationPeriod = rotation.StartDate.HasValue ? rotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                            rotation.StartDate.Value.AddDays(rotation.DayOn).FormatWithDayMonthYear(): "..."
                };

                if (rotation.State == Core.Models.RotationState.Confirmed || rotation.State == Core.Models.RotationState.Expired)
                {
                    model.RotationPeriod = rotation.StartDate.HasValue ? rotation.StartDate.Value.FormatWithDayMonthYear() + " - " +
                            rotation.StartDate.Value.AddDays(rotation.DayOn - 1).FormatWithDayMonthYear() : "...";
                }

                if ((rotation.State == Core.Models.RotationState.Created || rotation.State == Core.Models.RotationState.Expired))
                {
                    model.EmptyPanelMessage = "Your rotation has ended. Please confirm your next rotation start/end dates by starting a new handover.";
                    return PartialView("~/Views/Rotation/RotationReportBuilder/_EmptyDraftPanel.cshtml", model);
                }
                else if (Services.RotationService.IsRotationSwingEnd(rotation))
                {
                    model.EmptyPanelMessage = "Your report has been sent to your back-to-back. You can now view it in the History section.";
                    return PartialView("~/Views/Rotation/RotationReportBuilder/_EmptyDraftPanel.cshtml", model);
                }
                else
                {
                    return PartialView("~/Views/Rotation/RotationReportBuilder/_DraftPanel.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        public ActionResult ReportFilterPopover(Guid userId, HandoverReportFilterType filter)
        {
            SwingReportBuilderFilterViewModel model = new SwingReportBuilderFilterViewModel
            {
                Id = userId,
                Filter = filter
            };

            return PartialView("~/Views/Rotation/RotationReportBuilder/_RotationReportFilterPopover.cshtml", model);
        }

        /// <summary>
        /// Return From -> To block for darft section.
        /// </summary>
        /// <param name="rotationId">Rotation Id</param>
        /// <returns></returns>
        public ActionResult FromToBlock(Guid rotationId)
        {
            if (rotationId != Guid.Empty)
            {
                FromToBlockViewModel model = Services.RotationService.GetFromToBlock(rotationId);

                return PartialView("~/Views/Rotation/RotationReportBuilder/Shared/_DraftFromToBlock.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Received panel

        /// <summary>
        /// Return received panel for user.
        /// </summary>
        /// <param name="userId">User Id</param>
        /// <param name="selectedRotationId">Selected Rotation Id</param>
        /// <param name="receivedRotationId">Selected received Rotation Id</param>
        /// <returns></returns>
        public ActionResult ReceivedPanel(Guid userId, Guid? selectedRotationId, Guid? receivedRotationId)
        {
            if (userId != Guid.Empty)
            {
                ReceivedPanelViewModel model = new ReceivedPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    ReceivedRotationId = receivedRotationId,
                    RotationPeriod = Services.RotationService.GetReceivedRotationPeriod(selectedRotationId, null),
                    Rotations = Services.RotationService.GetRotationsReceivedHistorySlidesByUser(userId),
                    ReceivedRotations = selectedRotationId.HasValue ? Services.RotationService.GetRotationsWhoPopulateMyReceivedSection(selectedRotationId.Value) : null
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_ReceivedPanel.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedRotationId"></param>
        /// <returns></returns>
        public ActionResult ReceivedPanelForSelectedRotation(Guid? selectedRotationId)
        {
            if (selectedRotationId.HasValue)
            {
                ReceivedPanelViewModel model = new ReceivedPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    RotationPeriod = Services.RotationService.GetReceivedRotationPeriod(selectedRotationId, null),
                    ReceivedRotations = Services.RotationService.GetRotationsWhoPopulateMyReceivedSection(selectedRotationId.Value)
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_ReceivedPanelForSelectedRotation.cshtml", model);
            }

            return new EmptyResult();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedRotationId"></param>
        /// <param name="receivedRotationId"></param>
        /// <returns></returns>
        public ActionResult ReceivedPanelForReceivedRotation(Guid selectedRotationId, Guid? receivedRotationId)
        {
            if (selectedRotationId != Guid.Empty)
            {
                ReceivedPanelViewModel model = new ReceivedPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    ReceivedRotationId = receivedRotationId,
                    RotationPeriod = Services.RotationService.GetReceivedRotationPeriod(selectedRotationId, receivedRotationId)
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_ReceivedPanelForReceivedRotation.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region History panel

        public ActionResult HistoryPanel(Guid userId, Guid? selectedRotationId)
        {
            if (userId != Guid.Empty)
            {
                HistoryPanelViewModel model = new HistoryPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    RotationPeriod = selectedRotationId.HasValue ? Services.RotationService.GetRotationPeriod(selectedRotationId.Value) : "...",
                    Rotations = Services.RotationService.GetRotationsHistorySlidesByUser(userId)
                };
                return PartialView("~/Views/Rotation/RotationReportBuilder/_HistoryPanel.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult HistoryPanelSelectedRotation(Guid? selectedRotationId)
        {
            if (selectedRotationId.HasValue)
            {
                HistoryPanelViewModel model = new HistoryPanelViewModel
                {
                    SelectedRotationId = selectedRotationId,
                    RotationPeriod = Services.RotationService.GetRotationPeriod(selectedRotationId.Value),
                };

                return PartialView("~/Views/Rotation/RotationReportBuilder/_HistoryPanelSelectedRotation.cshtml", model);
            }

            return new EmptyResult();
        }

        #endregion

        public ActionResult ReportPreview(Guid rotationId, bool isReceived = false, Guid? selectedRotationId = null)
        {
            var viewModel = Services.ReportService.PopulateRotationReportViewModel(rotationId,
                                                                                    isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft,
                                                                                    selectedRotationId);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ReportPreview/_ReportPreview.cshtml", viewModel);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ReportPrint(Guid rotationId, bool isReceived = false, Guid? selectedRotationId = null)
        {

            string footerUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/RotationReportBuilder/PdfFooter";

            string footerHtml = String.Format(" --footer-html {0} " +
                                "--enable-local-file-access" +
                                " --enable-javascript" +
                                " --footer-spacing -10 " +
                                " --encoding utf-8", footerUrl);


            ReportPreviewViewModel model = Services.ReportService.PopulateRotationReportViewModel(rotationId, isReceived ? ModuleSourceType.Received 
                                                                                                                         : ModuleSourceType.Draft, selectedRotationId);

            var pdfResult = new ViewAsPdf("~/Views/Rotation/RotationReportBuilder/ReportPreview/_ReportPrint.cshtml", model)
            {
                FileName = "Rotation-Report.pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Rotativa.Options.Margins(0, 0, 5, 0)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            return File(binary, "application/pdf");

        }

        [AllowAnonymous]
        public ActionResult RotationReport(Guid rotationId, bool isReceived = false, Guid? selectedRotationId = null)
        {
            ReportPreviewViewModel viewModel = Services.ReportService.PopulateRotationReportViewModel(rotationId,
                                                                                                        isReceived ? ModuleSourceType.Received :
                                                                                                                        ModuleSourceType.Draft,
                                                                                                        selectedRotationId);

            return View("~/Views/Rotation/RotationReportBuilder/ReportPreview/_ReportPrint.cshtml", viewModel);
        }


        [AllowAnonymous]
        public ActionResult PdfFooter()
        {
            return PartialView("~/Views/Shared/Layouts/Base/_PrintFooter.cshtml");
        }

        [HttpPost]
        public ActionResult FinalizeAll(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                try
                {
                    Services.RotationService.ChangeFinalizeStatusForDraftTopicsAndTasks((Guid) rotationId, FinalizeStatus.Finalized);

                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                catch (Exception)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            return  new EmptyResult();
        }
    }
}