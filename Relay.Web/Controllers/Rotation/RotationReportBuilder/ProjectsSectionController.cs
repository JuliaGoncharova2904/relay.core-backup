﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;
using MomentumPlus.Relay.Roles;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ProjectsSectionController : BaseController
    {
        // IOC
        public ProjectsSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                    .GetProjectModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft, RotationType.Swing, filter)
                                                                    .OrderBy(p => p.Project);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectsDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                    .GetProjectModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                                    .OrderBy(p => p.Project);

            //if (receivedRotationId.HasValue)
            //    model = model.Where(m => m.FromRotationId == receivedRotationId.Value);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectsReceivedSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<ProjectsTopicViewModel> model = Services.RotationModuleService
                                                                .GetProjectModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft)
                                                                .OrderBy(p => p.Project);

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectsHistorySection.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateTopic(ProjectsTopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateProjectTopic(model, Guid.Parse(User.Identity.GetUserId()));

            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                ProjectsTopicViewModel model = Services.RotationTopicService.GetProjectTopic(topicId);
                if(model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult EditTopicDialog(Guid topic)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            ProjectTopicDialogViewModel model = Services.RotationTopicService.PopulateProjectTopicDialogModel(topic, userId);

            model.IsManagerCommentsEnabled = !Services.RotationTopicService.IsMyTopic(userId, topic)
                                             && (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
											     || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser)
                                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.SafetyManager));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(ProjectTopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Project Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateProjectTopic(model, Guid.Parse(User.Identity.GetUserId()));
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateProjectTopicDialogModel((Guid)model.Id, model.CreatorId);

                return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectTopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var modules = Services.RestrictionService.GetModulesByRotationId(destId, RotationType.Swing);

            var items = modules?.Where(m => m.Enabled).SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                      .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled).ToList();

            Services.RestrictionService.HandoverItemsLimitExceededNew(destId, RotationType.Swing, items.Count);

            var moduleId = modules.Find(m => m.Type == Core.Models.TypeOfModule.Project && !m.DeletedUtc.HasValue).Id;

            ProjectAddInlineTopicViewModel model = Services.RotationTopicService.PopulateProjectTopicInlineModel(destId, moduleId);

            model.CreatorId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Rotation/RotationReportBuilder/ProjectsSection/_ProjectAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(ProjectAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Project, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddProjectTopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion
    }
}