﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class RotationTaskBoardTasksSectionController : BaseController
    {
        // IOC
        public RotationTaskBoardTasksSectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            IEnumerable<TaskBoardTaskForHandover> model = Services.TaskBoardTaskService
                                                                .GetTaskBoardTasksForRotation(rotationId, ModuleSourceType.Draft)
                                                                .OrderBy(t => t.Name);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TaskBoardTasks/_TaskBoardTasksDraftSection.cshtml", model);
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            IEnumerable < TaskBoardTaskForHandover> model = Services.TaskBoardTaskService
                                                                .GetTaskBoardTasksForRotation(rotationId, ModuleSourceType.Received, receivedRotationId)
                                                                .OrderBy(t => t.Name);

            return PartialView("~/Views/Rotation/RotationReportBuilder/TaskBoardTasks/_TaskBoardTasksReceivedSection.cshtml", model);
        }


        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            IEnumerable<TaskBoardTaskForHandover> model = Services.TaskBoardTaskService
                                                                         .GetTaskBoardTasksForRotation(rotationId, ModuleSourceType.Draft)
                                                                         .OrderBy(t => t.Name);


            return PartialView("~/Views/Rotation/RotationReportBuilder/TaskBoardTasks/_TaskBoardTasksHistorySection.cshtml", model);
        }


        [HttpPost]
        public ActionResult UpdateTask(TaskBoardTaskForHandover model)
        {

            return Json(model);
        }




        [HttpPost]
        public ActionResult ReloadTask(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                TaskBoardTaskForHandover model = Services.TaskBoardTaskService.GetTask(topicId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }
    }
}