﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Extensions;
using MomentumPlus.Relay.Roles;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class HSESectionController : BaseController
    {
        // IOC
        public HSESectionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Draft(Guid rotationId, HandoverReportFilterType filter = HandoverReportFilterType.All)
        {
            if (Services.RotationModuleService.IsTemplateModuleEnabled(ModuleType.HSE, rotationId, ModuleSourceType.Draft, RotationType.Swing))
            {
                IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                            .GetHseModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft, RotationType.Swing, filter)
                                                            .OrderBy(h => h.Type);

                if (filter == HandoverReportFilterType.NrItems)
                {
                    return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSE_NR_DraftSection.cshtml", model);
                }

                return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEDraftSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult Received(Guid rotationId, Guid? receivedRotationId)
        {
            if (Services.RotationModuleService.IsTemplateModuleReceivedEnabled(receivedRotationId.HasValue ? receivedRotationId.Value : rotationId, ModuleType.HSE, ModuleSourceType.Draft, RotationType.Swing))
            {
                IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                            .GetHseModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Received).Where(t => t.IsSharingTopic == false)
                                                            .OrderBy(h => h.Type);

                //if (receivedRotationId.HasValue)
                //{
                //    model = model.Where(m => m.FromRotationId == receivedRotationId.Value);
                //}

                return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEReceivedSection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult History(Guid rotationId)
        {
            if (Services.RotationModuleService.IsTemplateModuleEnabled(ModuleType.HSE, rotationId, ModuleSourceType.Draft, RotationType.Swing))
            {
                IEnumerable<HSETopicViewModel> model = Services.RotationModuleService
                                                            .GetHseModuleTopics(Guid.Parse(User.Identity.GetUserId()), rotationId, ModuleSourceType.Draft, RotationType.Swing, HandoverReportFilterType.All)
                                                            .OrderBy(h => h.Type);

                return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEHistorySection.cshtml", model);
            }

            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult UpdateTopic(HSETopicViewModel model)
        {
            model = Services.RotationTopicService.UpdateHSETopic(model, Guid.Parse(User.Identity.GetUserId()));
            return Json(model);
        }

        [HttpPost]
        public ActionResult ReloadTopic(Guid topicId)
        {
            if(topicId != Guid.Empty)
            {
                HSETopicViewModel model = Services.RotationTopicService.GetHSETopic(topicId);

                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        #region Edit Topic Dialog

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult EditTopicDialog(Guid topic)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            HSETopicDialogViewModel model = Services.RotationTopicService.PopulateHSETopicDialogModel(topic, userId);

            model.IsManagerCommentsEnabled = !Services.RotationTopicService.IsMyTopic(userId, topic)
                                             && (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.SafetyManager)
                                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(HSETopicDialogViewModel model)
        {
            if (model.Id.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist(Guid.Parse(model.RotationTopicGroupId), (Guid)model.Id, model.Name))
                    {
                        ModelState.AddModelError("Name", "Safety Item with this name already exist.");
                    }

                    else
                    {
                        Services.RotationTopicService.UpdateHSETopic(model, Guid.Parse(User.Identity.GetUserId()));
                        return Content("ok");
                    }
                }

                model = Services.RotationTopicService.PopulateHSETopicDialogModel((Guid)model.Id, model.CreatorId);

                return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSETopicFormDialog.cshtml", model);

            }


            return HttpNotFound();
        }

        #endregion

        #region Add Topic

        [HttpGet]
        public ActionResult AddInlineTopic(Guid destId)
        {
            var modules = Services.RestrictionService.GetModulesByRotationId(destId, RotationType.Swing);

            var items = modules?.Where(m => m.Enabled).SelectMany(m => m.RotationTopicGroups).Where(tg => tg.Enabled)
                                                      .SelectMany(tg => tg.RotationTopics).Where(t => t.Enabled);

            Services.RestrictionService.HandoverItemsLimitExceededNew(destId, RotationType.Swing, items.Count());

            var moduleHSE = modules.Find(m => m.Type == Core.Models.TypeOfModule.HSE && !m.DeletedUtc.HasValue);

            HSEAddInlineTopicViewModel model = Services.RotationTopicService.PopulateHSETopicInlineModel(destId, moduleHSE.Id);

            model.CreatorId = Guid.Parse(User.Identity.GetUserId());

            return PartialView("~/Views/Rotation/RotationReportBuilder/HSESection/_HSEAddInlineTopic.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInlineTopic(HSEAddInlineTopicViewModel model)
        {
            if (model.RotationId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    if (Services.RotationTopicService.IsTopicExist((Guid)model.Type, Guid.NewGuid(), model.Reference))
                    {
                        ModelState.AddModelError("Reference", "An item with this category and reference already exists");
                    }

                    else
                    {
                        Services.RotationTopicService.AddHSETopic(model);

                        return new HttpStatusCodeResult(HttpStatusCode.OK);
                    }
                }

                var errorMessages = ModelState.GetValidErrorFromState();

                return Content(errorMessages);
            }

            return HttpNotFound();
        }

        #endregion

    }
}