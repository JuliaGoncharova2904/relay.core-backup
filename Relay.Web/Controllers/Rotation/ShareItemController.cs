﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using Microsoft.AspNet.Identity;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ShareItemController : BaseController
    {
        public ShareItemController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) {}

        #region Share topic

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult ShareTopicFormDialog(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                ShareTopicViewModel model = Services.SharingTopicService.PopulateShareTopicViewModel((Guid)topicId);

                return PartialView("~/Views/Rotation/ShareItem/ShareTopicDialog.cshtml", model);
            }

            return new HttpNotFoundResult();
        }

        public ActionResult SetEmailsString(string email)
        {
            var model = Services.SharingTopicService.SetStringEmailsRecepient(email);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ShareTopicFormDialog(ShareTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
               // Services.SharingTopicService.SetEmailsRecepient(model);
                Services.SharingTopicService.ChangeShareTopicRelations(model);
                Guid userId = Guid.Parse(User.Identity.GetUserId());
                Services.SharingTopicService.AddShareToTopic(model, model.TopicId, userId);
                return Content("ok");
            }

            model = Services.SharingTopicService.PopulateShareTopicViewModel(model.TopicId);

            return PartialView("~/Views/Rotation/ShareItem/ShareTopicDialog.cshtml", model);
        }

        #endregion

        #region Share Report

        [HttpPost]
        public ActionResult ShareReportIcon(Guid sourceId)
        {
            ShareReportUsersViewModel model = Services.SharingReportService.CountRecipientsBySourceId(sourceId);

            return PartialView("~/Views/Rotation/ShareItem/_ShareReportItemIcon.cshtml", model);
        }

        [HttpGet]
        public ActionResult ShareReportFormDialog(Guid sourceId, RotationType sourceType)
        {
            var model = Services.SharingReportService.PopulateShareReportViewModel(sourceId, sourceType);   

            return PartialView("~/Views/Rotation/ShareItem/ShareReportFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult ShareReportFormDialog(ShareReportViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SharingReportService.ChangeShareReportRelations(model, SwingOwnerId);
                return Content("ok");
            }

            model = Services.SharingReportService.PopulateShareReportViewModel(model.ReportId, model.ReportType);

            return PartialView("~/Views/Rotation/ShareItem/ShareReportFormDialog.cshtml", model);
        }

        #endregion

        [HttpGet]
        public ActionResult GetEmails()
        {
            var emails = Services.SharingTopicService.GetEmailsList();

            return Json(emails, JsonRequestBehavior.AllowGet);
        }

    }
}