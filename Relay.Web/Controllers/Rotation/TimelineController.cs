﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TimelineController : BaseController
    {
        public TimelineController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }

        public ActionResult Index(Guid? rotationId)
        {
            if (rotationId.HasValue)
            {
                var model = Services.TimelineService.GetLastTwoYearsTimeline(rotationId.Value);
                return PartialView("~/Views/Rotation/Dashboard/_TimelinePanel.cshtml", model);
            }
            return null;
        }
    }
}