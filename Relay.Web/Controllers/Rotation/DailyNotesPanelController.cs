﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers.Rotation
{
    public class DailyNotesPanelController : BaseController
    {
        // IOC
        public DailyNotesPanelController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult DailyNotesForRotation(Guid rotationId)
        {
            if (rotationId != Guid.Empty)
            {
                string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];
                var userID = Guid.Parse(User.Identity.GetUserId());

                DailyNotesPanelViewModel model = Services.DailyNoteService
                                                        .DailyNotesForPanel(rotationId, userID, safetyVersion);

                var subscriptionType = Services.AuthService.GetSubscriptionType(userID);

                if (model.DailyNotes.Any())
                {
                    if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                    {
                        foreach (var dailyReport in model.DailyNotes)
                        {
                            var daysAccessHistory = DaysAccessHistoryForBasicLevel(dailyReport.NormalDate);
                            model.DailyNotes = model.DailyNotes.Where(n => daysAccessHistory).ToList();
                        }
                    }

                    else if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Premium)
                    {
                        foreach (var dailyReport in model.DailyNotes)
                        {
                            var daysAccessHistory = DaysAccessHistoryForPremiumLevel(dailyReport.NormalDate);
                            model.DailyNotes = model.DailyNotes.Where(n => daysAccessHistory).ToList();
                        }
                    }

                    return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNotesPanel.cshtml", model);
                }
                else
                    return new EmptyResult();
            }

            return HttpNotFound();
        }

        private bool DaysAccessHistoryForBasicLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddMonths(1);
            if (timeStamp <= daysAccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool DaysAccessHistoryForPremiumLevel(DateTime timeStamp)
        {
            var daysAccess = timeStamp.AddMonths(6);
            if (timeStamp <= daysAccess)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpGet]
        public ActionResult DailyNoteSlide(Guid dailyNoteId)
        {
            if (dailyNoteId != Guid.Empty)
            {
                string safetyVersion = ConfigurationManager.AppSettings["safetySectionVersion"];

                DailyNoteViewModel model = Services.DailyNoteService
                    .DailyNoteSlideForPanel(dailyNoteId, Guid.Parse(User.Identity.GetUserId()), safetyVersion);

                return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteSlide.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult DetailsDailyNoteDialog(Guid? Id)
        {
            if (Id.HasValue)
            {
                DailyNoteViewModel model = Services.DailyNoteService.GetDailyNoteById(Id.Value);
                if (model != null)
                {
                    return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteDetailsDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult EditDailyNoteDialog(Guid? Id)
        {
            if (Id.HasValue)
            {
                DailyNoteViewModel model = Services.DailyNoteService.GetDailyNoteById(Id.Value);
                if (model != null)
                {
                    return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDailyNoteDialog(DailyNoteViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.DailyNoteService.UpdateDailyNote(model);
                return Content("ok");
            }

            return PartialView("~/Views/Rotation/DailyNotesPanel/_DailyNoteFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult UpdateEditDailyNoteDialog(Guid? Id, string notes, bool? showInReport)
        {
            if (Id.HasValue)
            {
                DailyNoteViewModel model = Services.DailyNoteService.GetDailyNoteById(Id.Value);
                model.Notes = notes;
                model.ShowInReport = showInReport.HasValue ? showInReport.Value : false;
             
                var dailyNoteModel = Services.DailyNoteService.UpdateDailyNoteContext(model);

                if (dailyNoteModel != null)
                {
                    return PartialView("~/Views/Rotation/DailyNotesPanel/_UpdateAttachCounter.cshtml", dailyNoteModel);
                }
            }

            return HttpNotFound();
        }

    }
}