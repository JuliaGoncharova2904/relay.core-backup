﻿using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class AttachmentsController : BaseController
    {
        // IOC
        public AttachmentsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        #region Topic

        [HttpGet]
        public ActionResult AttachmentsListForTopicDialog(Guid Id)
        {
            List<AttachmentViewModel> modelAttach = Services.AttachmentService.GetAttachmentForTopic(Id);
            List<AttachmentViewModel> modelAttachLinks = Services.AttachmentService.GetAttachmentLinkForTopic(Id);

            List<AttachmentViewModel> model = modelAttach;

            if (modelAttachLinks != null && modelAttachLinks.Count != 0)
            {
                model.AddRange(modelAttachLinks);
            }

            if (model != null)
                return PartialView("~/Views/Rotation/Attachments/_AttachmentsListDialog.cshtml", model);

            return HttpNotFound();
        }

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult AddAttachmentForTopic(Guid Id)
        {
            if (Id != Guid.Empty)
            {
                AttachmentViewModel attachment = new AttachmentViewModel { TargetId = Id, UserId = Guid.Parse(User.Identity.GetUserId()) };
                return PartialView("~/Views/Rotation/Attachments/_NewAddAttachmentForm.cshtml", attachment);
               // return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAttachmentForTopic(AttachmentViewModel attachment)
        {
            if (attachment.TargetId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    Services.AttachmentService.AddAttachmentToTopic(attachment, SwingOwnerId);
                    return Content("ok");
                }
                else
                {
                    return PartialView("~/Views/Rotation/Attachments/_NewAddAttachmentForm.cshtml", attachment);

                   // return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult RemoveAttachmentFromTopic(Guid Id, Guid attachmentId)
        {
            if (Id != Guid.Empty && attachmentId != Guid.Empty)
            {
                Services.AttachmentService.RemoveAttachmentFromTopic(Id, attachmentId, Guid.Parse(User.Identity.GetUserId()));
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Task

        [HttpGet]
        public ActionResult AttachmentsListForTaskDialog(Guid? Id)
        {
            if (Id.HasValue)
            {
                List<AttachmentViewModel> modelAttach = Services.AttachmentService.GetAttachmentForTask(Id.Value);
                List<AttachmentViewModel> modelAttachLinks = Services.AttachmentService.GetAttachmentLinkForTask(Id.Value);

                List<AttachmentViewModel> model = modelAttach;

                if (modelAttachLinks != null && modelAttachLinks.Count != 0)
                {
                    model.AddRange(modelAttachLinks);
                }

                if (model != null)
                    return PartialView("~/Views/Rotation/Attachments/_AttachmentsListDialog.cshtml", model);
            }
            return null;
        }

        [HttpGet]
        public ActionResult AddAttachmentForTask(Guid? Id)
        {
            if (Id.Value != Guid.Empty)
            {
                AttachmentViewModel attachment = new AttachmentViewModel { TargetId = Id, UserId = Guid.Parse(User.Identity.GetUserId()) };
                return PartialView("~/Views/Rotation/Attachments/_NewAddAttachmentForm.cshtml", attachment);
                //return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAttachmentForTask(AttachmentViewModel attachment)
        {
            if (attachment.TargetId.HasValue)
            {
                if (ModelState.IsValid)
                {
                    Services.AttachmentService.AddAttachmentToTask(attachment, SwingOwnerId);
                    return Content("ok");
                }
                else
                {
                    //return PartialView("~/Views/Rotation/Attachments/_AddAttachmentForm.cshtml", attachment);
                      return PartialView("~/Views/Rotation/Attachments/_NewAddAttachmentForm.cshtml", attachment);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult RemoveAttachmentFromTask(Guid Id, Guid attachmentId)
        {
            if (Id != Guid.Empty && attachmentId != Guid.Empty)
            {
                Services.AttachmentService.RemoveAttachmentFromTask(Id, attachmentId);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

    }
}