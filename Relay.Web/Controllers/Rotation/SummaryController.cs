﻿using System;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.UI;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SummaryController : BaseController
    {
        // IOC
        public SummaryController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [OutputCache(VaryByParam = "*", Duration = 3, NoStore = true)]
        public ActionResult Index()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            Guid? viewedRotationId = Services.RotationService.GetCurrentRotationIdForUser(SwingOwnerId);
            var rotation = viewedRotationId.HasValue ? Services.RotationService.GetRotation(viewedRotationId.Value) : null;

            if (!viewedRotationId.HasValue || (!Services.RotationService.RotationIsConfirmed(rotation) && !(rotation.PrevRotation != null))
                || Services.RotationService.IsNotExistAliveRotationOrShift(viewedRotationId.HasValue ? SwingOwnerId : userId))
            {
                ViewModeViewModel viewModeModel = new ViewModeViewModel
                {
                    IsViewMode = IsViewMode,
                    RotationOwnerFullName = !IsViewMode ? string.Empty : rotation.RotationOwner.FullName,
                    UserId = SwingOwnerId,
                    HaveRotation = viewedRotationId.HasValue ? true : false,
                    RotationOwnerName = !IsViewMode ? string.Empty : rotation.RotationOwner.FirstName
                };
                return View("~/Views/Rotation/Shared/NoRotation.cshtml", viewModeModel);
            }

            SummaryViewModel model = Services.RotationService.GetSummaryForRotation(rotation, SwingOwnerId, IsViewMode);

            if (Services.RotationService.IsShiftRotation(rotation))
            {
                if (Services.RotationService.IsRotationFirstShiftNotConfirmed(rotation))
                {
                    ViewModeViewModel viewModeModel = new ViewModeViewModel
                    {
                        IsViewMode = IsViewMode,
                        RotationOwnerFullName = !IsViewMode ? string.Empty : rotation.RotationOwner.FullName,
                        RotationOwnerName = !IsViewMode ? string.Empty : rotation.RotationOwner.FirstName,
                        UserId = SwingOwnerId,
                        HaveRotation = viewedRotationId.HasValue ? true : false
                    };
                    return View("~/Views/Rotation/Shared/NoRotation.cshtml", viewModeModel);
                }
                return View("~/Views/Rotation/Summary/ShiftIndex.cshtml", model);
            }
            else
            {
                return View("~/Views/Rotation/Summary/SwingIndex.cshtml", model);
            }
        }

        [HttpGet]
        [OutputCache(Duration = 10)]
        public ActionResult GetContributorUsersByTopicId(Guid? topicId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (topicId.HasValue)
            {
                var editorUsers = Services.RotationTopicService.GetContributorUsersByTopicId(topicId.Value, userId);

                return PartialView("~/Views/Rotation/Shared/ShowIconContributorUsers.cshtml", editorUsers);
            }
            return null;
        }


        [HttpGet]
        [OutputCache(Duration = 10)]
        public ActionResult GetTagsBodyByTopicId(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                var tagsTopic = Services.RotationTopicService.GetTagsTopicByTopicId(topicId.Value);

                return PartialView("~/Views/Rotation/Shared/ShowTagsTopic.cshtml", tagsTopic);
            }
            return null;
        }


        [HttpGet]
        [OutputCache(Duration = 10)]
        public ActionResult GetTagsReceivedByTopicId(Guid? topicId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
          
            if (topicId.HasValue)
            {
                var tags = Services.RotationTopicService.GetTagsByTopicId(topicId.Value, userId);

                return PartialView("~/Views/Rotation/Shared/RecievedTagsTopic.cshtml", Tuple.Create<string, Guid>(tags, topicId.Value));
            }
            return null;
        }

        [HttpGet]
        [OutputCache(Duration = 10)]
        public ActionResult GetTagsByTopicId(Guid? topicId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (topicId.HasValue)
            {
                var tags = Services.RotationTopicService.GetTagsByTopicId(topicId.Value, userId);
            
               return PartialView("~/Views/Rotation/Shared/TagsTopic.cshtml", Tuple.Create<string, Guid>(tags, topicId.Value));
            }
            return null;
        }

        [HttpGet]
        public ActionResult SetTagsByTopicId(Guid topicId, string tagString)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (topicId != null)
            {
                string _jsonObject = tagString.Replace(@"\", string.Empty);
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<string> tagsSelecton = serializer.Deserialize<List<string>>(_jsonObject);
                Services.RotationTopicService.SetTagsByTopicId(topicId, userId, tagsSelecton);
                return Content("ok");
            }
            return null;
        }

        [OutputCache(Duration = 3)]
        public ActionResult ShowConfirmButton(Guid userId)
        {
            if (Services.RotationService.IsShowConfirmButton(userId))
            {
                ConfirmButtonViewModel confirmButtonModel = Services.RotationService.ConfirmButton(userId);

                return PartialView("~/Views/Rotation/Shared/ShowConfirmButton.cshtml", confirmButtonModel);
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [OutputCache(Duration = 3)]
        public ActionResult ShowEditDatesButton(Guid userId)
        {
            EditDatesButtonViewModel editDatesButtonModel = new EditDatesButtonViewModel()
            {
                IsShowShiftEditDatesButton = Services.RotationService.IsShowShiftEditDatesButton(userId),
                IsShowRotationEditDatesButton = Services.RotationService.IsShowRotationEditDatesButton(userId),
                UserId = userId
            };
            return PartialView("~/Views/Rotation/Shared/ShowEditDatesButton.cshtml", editDatesButtonModel);
            //  return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult ChangePlanndeField(Guid topicId, string planned)
        {
           var plannedActualField = Services.RotationTopicService.ChangePlannedByTopicIdNew(topicId, planned);
  
            return PartialView("~/Views/Rotation/Shared/PvADraft.cshtml", plannedActualField);
        }

        public ActionResult ChangeActualField(Guid topicId, string actual)
        {
            var plannedActualField = Services.RotationTopicService.ChangeActualByTopicIdNew(topicId, actual);

            return PartialView("~/Views/Rotation/Shared/PvADraft.cshtml", plannedActualField);
        }

        public ActionResult ChangeUnitsSelectedTypeField(Guid topicId, string unitsSelected)
        { 
            var plannedActualField = Services.RotationTopicService.ChangeUnitsTypeByTopicIdNew(topicId, unitsSelected);
       
            return PartialView("~/Views/Rotation/Shared/PvADraft.cshtml", plannedActualField);
        }

        [HttpGet]
        [OutputCache(Duration = 10)]
        public ActionResult GetPvAByTopicId(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                var plannedActualField =  Services.RotationTopicService.GetPlannedVsActualByTopicId(topicId.Value);

                return PartialView("~/Views/Rotation/Shared/PvADraft.cshtml", plannedActualField);
            }
            return null;
        }

        [HttpGet]
        [OutputCache(Duration = 10)]
        public ActionResult GetPvANotEditByTopicId(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                var plannedActualField = Services.RotationTopicService.GetPlannedVsActualByTopicId(topicId.Value);

                return PartialView("~/Views/Rotation/Shared/NotEditPvA.cshtml", plannedActualField);
            }
            return null;
        }

        [HttpGet]
        public ActionResult GetSharedCounter(Guid? topicId)
        {
            if (topicId.HasValue)
            {
                var shareCounter = Services.RotationTopicService.GetShareCounter(topicId.Value);
                return Json(shareCounter, JsonRequestBehavior.AllowGet); 
            }
            return null;
        }
    }
}