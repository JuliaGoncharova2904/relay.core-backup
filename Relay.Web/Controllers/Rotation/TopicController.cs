﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using System.Net;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Roles;
using System.Collections.Generic;
using System.Web.UI;
using System.Text.RegularExpressions;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TopicController : BaseController
    {
        // IOC
        public TopicController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult EditTopicDialog(Guid topicid)
        {
            ModuleType section = Services.RotationTopicService.GetTopicModuleType(topicid);

            switch (section)
            {
                case ModuleType.HSE:
                    return RedirectToAction("EditTopicDialog", "HSESection", new { topic = topicid });
                case ModuleType.Core:
                    return RedirectToAction("EditTopicDialog", "CoreSection", new { topic = topicid });
                case ModuleType.Project:
                    return RedirectToAction("EditTopicDialog", "ProjectsSection", new { topic = topicid });
                case ModuleType.Team:
                    return RedirectToAction("EditTopicDialog", "TeamSection", new { topic = topicid });

                case ModuleType.DailyNote:
                default:
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
        }

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult ManagerCommentButton(ManagerCommentButtonViewModel model)
        {
            if (model.ItemId.HasValue)
            {
                Guid userId = Guid.Parse(User.Identity.GetUserId());

                var isMyTopic = Services.RotationTopicService.IsMyTopic(userId, model.ItemId.Value);

                if (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && !isMyTopic ||
                    User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) && !isMyTopic ||
                    User.IsInRole(iHandoverRoles.Relay.Administrator) && !isMyTopic ||
                    User.IsInRole(iHandoverRoles.Relay.HeadLineManager) && !isMyTopic ||
                    User.IsInRole(iHandoverRoles.Relay.LineManager) && !isMyTopic)
                {
                    model.IsActive = true;
                }
            }

            return PartialView("~/Views/Rotation/Shared/ManagerComments/_ManagerCommentButton.cshtml", model);
        }

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult TopicDetailsDialog(Guid topicid)
        {
            var model = Services.RotationTopicService.GetModelForTopicDetailsModal(topicid);
            return PartialView("~/Views/Rotation/Topic/_ViewTopicDetailsDialog.cshtml", model);
        }

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult ManagerCommentsDialog(Guid Id)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            var isEditable = Services.RotationTopicService.IsMyTopic(userId, Id)
                                 || Services.RotationTopicService.IsCanChangeTopic(userId, Id)
                                 || Services.RotationTopicService.IsContributersTopicUser(userId, Id)
                                 || (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser));

            IEnumerable<ManagerCommentsViewModel> model = Services.RotationTopicService.PopulateManagerCommentsViewModel(Id, SwingOwnerId);

            if (isEditable)
            {
                return PartialView("~/Views/Rotation/Shared/ManagerComments/_NewManagerCommentsFormDialog.cshtml", model);
            }

            return PartialView("~/Views/Rotation/Shared/ManagerComments/_NewManagerCommentsDetailsDialog.cshtml", model);
        }


        [HttpGet]
        public ActionResult TopicComments(Guid Id)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            var isEditable = Services.RotationTopicService.IsMyTopic(userId, Id)
                                 || Services.RotationTopicService.IsCanChangeTopic(userId, Id)
                                 || Services.RotationTopicService.IsContributersTopicUser(userId, Id)
                                 || (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                                 || User.IsInRole(iHandoverRoles.Relay.Administrator)
                                 || User.IsInRole(iHandoverRoles.Relay.HeadLineManager)
                                 || User.IsInRole(iHandoverRoles.Relay.LineManager)
                                 || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser));

            IEnumerable<ManagerCommentsViewModel> model = Services.RotationTopicService.PopulateManagerCommentsViewModel(Id, SwingOwnerId);

            return PartialView("~/Views/Rotation/Topic/_TopicComments.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ManagerCommentsDialog(ManagerCommentsViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.RotationTopicService.UpdateTopicManagerComment(model, SwingOwnerId);
                return Content("ok");
            }

            return PartialView("~/Views/Rotation/Shared/ManagerComments/_NewManagerCommentsFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult AddManagerCommentsForTopic(Guid Id)
        {
            if (Id != Guid.Empty)
            {
                ManagerCommentsViewModel managerComments = new ManagerCommentsViewModel {  Id = Id };
                return PartialView("~/Views/Rotation/Shared/ManagerComments/_AddManagerCommentsFormDialog.cshtml", managerComments);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddManagerCommentsForTopic(ManagerCommentsViewModel model)
        {
            if (model.Id != null)
            {
                if (ModelState.IsValid)
                {
                    Services.RotationTopicService.UpdateTopicManagerComment(model, SwingOwnerId);
                    return Content("ok");
                }
                else
                {
                    return PartialView("~/Views/Rotation/Shared/ManagerComments/_AddManagerCommentsFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult RemoveManagerCommentsDialog(Guid? id, Guid? managerCommentId)
        {
            if (id != null && managerCommentId != null)
            {
                Services.RotationTopicService.RemoveTopicManagerComment(id.Value, managerCommentId.Value);
                return Content("ok");
            }

            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult EditManagerCommentsForTopic(Guid topicId, Guid managerCommentId)
        {
            if (topicId != Guid.Empty)
            {
                ManagerCommentsViewModel managerComments = Services.RotationTopicService.GetManagerCommentsViewModel(topicId, managerCommentId);

                return PartialView("~/Views/Rotation/Shared/ManagerComments/_EditManagerComments.cshtml", managerComments);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditManagerCommentsForTopic(ManagerCommentsViewModel model)
        {
            if (model.Id != null)
            {
                if (ModelState.IsValid)
                {
                    Services.RotationTopicService.UpdateTopicManagerComment(model, SwingOwnerId);
                    return Content("ok");
                }
                else
                {
                    return PartialView("~/Views/Rotation/Shared/ManagerComments/_EditManagerCommentsFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult CarryforwardTopic(Guid topicId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            Services.RestrictionService.TopicsCarryForwardsLimitExceeded(topicId);
            Services.RotationTopicService.CarryforwardTopic(topicId);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult RemoveTopic(Guid topicId)
        {
            if (topicId != Guid.Empty && Services.RotationTopicService.RemoveTopic(topicId))
            {
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #region Location

        [HttpGet]
        public ActionResult DetailsLocationFormDialog(Guid topicId)
        {
            LocationViewModel model = Services.LocationService.GetLocationForTopic(topicId);
            if (model != null)
            {
                return PartialView("~/Views/Rotation/Topic/_LocationDetailsFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpGet]
        [OutputCache(Duration = 200, Location = OutputCacheLocation.Client)]
        public ActionResult CreateLocationFormDialog(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                TempData["CreateLocation_TopicId"] = topicId;
                ViewBag.Title = "Create Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml");
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateLocationFormDialog(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (TempData["CreateLocation_TopicId"] != null)
                {
                    Guid userId = Guid.Parse(User.Identity.GetUserId());
                    Guid topicId = (Guid)TempData["CreateLocation_TopicId"];
                    if (Services.LocationService.CreateLocationForTopic(topicId, model, userId))
                    {
                        return Content("ok");
                    }
                }

                return HttpNotFound();
            }
            else
            {
                ViewBag.Title = "Edit Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml", model);
            }
        }

        [HttpGet]
        public ActionResult EditLocationFormDialog(Guid topicId)
        {
            LocationViewModel model = Services.LocationService.GetLocationForTopic(topicId);

            if (model != null)
            {
                Regex regEx = new Regex(@"^(http[s]{0,1}://|[\\]{2})(?:[\w][\w.-]?)+", RegexOptions.Compiled);

                var match = regEx.Match(model.Address);

                if (match.Success)
                {
                    model.IsLink = true;
                }

                TempData["EditLocation_TopicId"] = topicId;
                ViewBag.Title = "Edit Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditLocationFormDialog(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (TempData["EditLocation_TopicId"] != null)
                {
                    Guid topicId = (Guid)TempData["EditLocation_TopicId"];
                    if (Services.LocationService.UpdateLocation(model))
                    {
                        return Content("ok");
                    }
                }

                return HttpNotFound();
            }
            else
            {
                ViewBag.Title = "Edit Location";
                return PartialView("~/Views/Rotation/Topic/_LocationFormDialog.cshtml", model);
            }
        }

        #endregion

        #region TopicLog

        public ActionResult GetTopicLogTab(Guid topicId)
        {
            IEnumerable<string> model = Services.TopicLogService.GetTopicLogs(topicId);

            return PartialView("~/Views/Rotation/Topic/_TopicLogTab.cshtml", model);
        }

        #endregion
    }
}