﻿using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;
using Microsoft.AspNet.Identity;
using Rotativa;
using Rotativa.Options;
using System.Web;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TaskBoardController : BaseController
    {
        public TaskBoardController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }

        public ActionResult Index()
        {
            return RedirectToAction("ReceivedSection");
        }

        #region Sections

        public ActionResult ReceivedSection(int? page, int? pageSize, TaskBoardSortOptions? sortStrategy, TaskBoardFilterOptions? filter)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            TaskBoardViewModel model = new TaskBoardViewModel
            {
                UserId = this.SwingOwnerId,
                IsViewMode = this.IsViewMode,
                OwnerName = Services.UserService.GetEmployeeById(this.SwingOwnerId, userId).FullName,
                Filter = filter,
                SortStrategy = sortStrategy,
                TasksTopics = Services.TaskBoardService.GetReceivedTasks(
                    this.SwingOwnerId,
                    page - 1 ?? 0,
                    pageSize ?? 8,
                    sortStrategy ?? TaskBoardSortOptions.NotSort,
                    filter ?? TaskBoardFilterOptions.IncompletedTasks
                )
            };

            return View("~/Views/Rotation/TaskBoard/Sections/Received.cshtml", model);
        }

        public ActionResult AssignedSection(int? page, int? pageSize, TaskBoardSortOptions? sortStrategy, TaskBoardFilterOptions? filter)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            TaskBoardViewModel model = new TaskBoardViewModel
            {
                UserId = this.SwingOwnerId,
                IsViewMode = this.IsViewMode,
                OwnerName = Services.UserService.GetEmployeeById(this.SwingOwnerId, userId).FullName,
                Filter = filter,
                SortStrategy = sortStrategy,
                TasksTopics = Services.TaskBoardService.GetAssignedTasks(
                    this.SwingOwnerId,
                    page - 1 ?? 0,
                    pageSize ?? 8,
                    sortStrategy ?? TaskBoardSortOptions.NotSort,
                    filter ?? TaskBoardFilterOptions.IncompletedTasks
                )
            };

            return View("~/Views/Rotation/TaskBoard/Sections/Assigned.cshtml", model);
        }

        public ActionResult MyTasksSection(int? page, int? pageSize, TaskBoardSortOptions? sortStrategy, TaskBoardFilterOptions? filter)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            TaskBoardViewModel model = new TaskBoardViewModel
            {
                UserId = this.SwingOwnerId,
                IsViewMode = this.IsViewMode,
                OwnerName = Services.UserService.GetEmployeeById(this.SwingOwnerId, userId).FullName,
                Filter = filter,
                SortStrategy = sortStrategy,
                TasksTopics = Services.TaskBoardService.GetMyTasks(
                    this.SwingOwnerId,
                    page - 1 ?? 0,
                    pageSize ?? 8,
                    sortStrategy ?? TaskBoardSortOptions.NotSort,
                    filter ?? TaskBoardFilterOptions.IncompletedTasks
                )
            };

            return View("~/Views/Rotation/TaskBoard/Sections/MyTasks.cshtml", model);
        }

        #region Update/Reload methods for Section

        [HttpPost]
        public ActionResult ReloadTaskTopic(Guid topicId)
        {
            if (topicId != Guid.Empty)
            {
                TaskBoardTaskTopicViewModel model = Services.TaskBoardService.GetTaskTopic(topicId, SwingOwnerId);
                if (model != null)
                {
                    return Json(model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult UpdateTaskTopic(TaskBoardTaskTopicViewModel model)
        {
            model = Services.TaskBoardService.UpdateTaskTopic(model);

            return Json(model);
        }

        #endregion

        #endregion

        #region Popover Filters

        public ActionResult GetPopoverFilter(TaskBoardSection section, TaskBoardFilterOptions? filter)
        {
            TaskBoardPopoverViewModel model = new TaskBoardPopoverViewModel
            {
                Section = section,
                FilterOption = filter.HasValue ? filter.Value : TaskBoardFilterOptions.IncompletedTasks
            };
            return PartialView("~/Views/Rotation/TaskBoard/_TaskBoardFilterPopover.cshtml", model);
        }

        #endregion

        #region Add Task Dialog

        [HttpGet]
        public ActionResult AddTaskDialog(Guid userId, bool isMyTask = false)
        {
            var creatorId = Guid.Parse(User.Identity.GetUserId());

            TaskBoardAddTaskDialogViewModel model = Services.TaskBoardService.PopulateAddTaskModel(userId, creatorId);

            if (isMyTask)
            {
                model.AssignedTo = model.OwnerId;
            }

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_AddTaskFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTaskDialog(TaskBoardAddTaskDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.SendingOption != TaskSendingOptions.Pending || model.SendDate.HasValue)
                {
                    model.AssignedTo = model.UserSelectedId;
                    Services.TaskBoardService.AddTask(model, Guid.Parse(User.Identity.GetUserId()));
                    return Content("ok");
                }

                ModelState.AddModelError("SendDate", "SendDate is required field");
            }

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_AddTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Task Dialog

        [HttpGet]
        public ActionResult EditTaskDialog(Guid userId, Guid taskId)
        {
            TaskBoardEditTaskDialogViewModel model = Services.TaskBoardService.PopulateEditTaskModel(userId, Guid.Parse(User.Identity.GetUserId()), taskId);

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_EditTaskFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskDialog(TaskBoardEditTaskDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.SendingOption != TaskSendingOptions.Pending || model.SendDate.HasValue)
                {
                    var currentUserId = Guid.Parse(User.Identity.GetUserId());
                    model.AssignedTo = model.UserSelectedId;
                    Services.TaskBoardService.UpdateTask(model, currentUserId);
                    return Content("ok");
                }

                ModelState.AddModelError("SendDate", "SendDate is required field");
            }

            model = Services.TaskBoardService.PopulateEditTaskModel(model);
            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_EditTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Complete Task Checklist Dialog

        [HttpGet]
        public ActionResult CompleteTaskChecklistDialog(Guid TaskId)
        {
            TaskBoardCompleteTaskDialogViewModel model = Services.TaskBoardService.PopulateCompleteChecklistTaskViewModel(TaskId, Guid.Parse(User.Identity.GetUserId()));

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            var currentUserId = Guid.Parse(User.Identity.GetUserId());

            model.Deadline = model.TaskDetailTab.Deadline;

            if (model.TaskDetailTab.CreatorTaskId == currentUserId)
            {
                if (model.TaskDetailTab.Tags != null)
                {
                    model.Tags = model.TaskDetailTab.Tags;
                    model.Deadline = model.TaskDetailTab.Deadline;
                }
            }

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_CompleteTaskFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompleteTaskChecklistDialog(TaskBoardCompleteTaskDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = Guid.Parse(User.Identity.GetUserId());

                TaskBoardEditTaskDialogViewModel editTaskDialogViewModel = new TaskBoardEditTaskDialogViewModel
                {
                    AssignedTo = model.TaskDetailTab.AssignedFrom,
                    ID = model.TaskDetailTab.TaskId,
                    OwnerId = model.TaskDetailTab.CreatorTaskId,
                    AttacmentsCounter = model.TaskDetailTab.AttacmentsCounter,
                    Deadline = model.Deadline,
                    Name = model.TaskDetailTab.Name,
                    Priority = model.TaskDetailTab.Priority,
                    Notes = model.TaskDetailTab.TaskNotes,
                    IsFeedbackRequired = model.TaskDetailTab.IsFeedbackRequired,
                    Tags = model.Tags
                };

                Services.TaskBoardService.UpdateTask(editTaskDialogViewModel, currentUserId);

                if (model.TaskOutcomesTab.IsFeedbackRequired && string.IsNullOrWhiteSpace(model.TaskOutcomesTab.Feedback))
                {
                    ModelState.AddModelError("Feedback", "Handback Notes is required");
                    model.SelectedTabNumber = 2;
                }
                else
                {
                    Services.TaskBoardService.SaveCompleteTaskBoardTask(model.TaskOutcomesTab, model.CompleteOption);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_CompleteTaskFormDialog.cshtml", model);
        }

        #endregion

        #region Complete Handover Task Dialog

        [HttpGet]
        public ActionResult CompleteHandoverTaskDialog(Guid TaskId)
        {
            TaskBoardCompleteHandbackTaskDialogViewModel model = Services.TaskBoardService.PopulateCompleteHandbackTaskViewModel(TaskId, Guid.Parse(User.Identity.GetUserId()));

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_CompleteTaskHanoverFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CompleteHandoverTaskDialog(TaskBoardCompleteHandbackTaskDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.TaskOutcomesTab.IsFeedbackRequired && string.IsNullOrWhiteSpace(model.TaskOutcomesTab.Feedback))
                {
                    ModelState.AddModelError("Feedback", "Handback Notes is required");
                    model.SelectedTabNumber = 2;
                }
                else
                {
                    Services.TaskBoardService.SaveCompleteTaskBoardTask(model.TaskOutcomesTab, model.CompleteOption);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_CompleteTaskHanoverFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Received Task

        public ActionResult GetEditCompleteForReceivedMenuPopover(Guid userId, Guid taskId)
        {
            ReceivedEditCompleteViewModel model = new ReceivedEditCompleteViewModel
            {
                UserId = userId,
                TaskId = taskId
            };
            return PartialView("~/Views/Rotation/TaskBoard/_TaskBoardEditCompleteForReceivedMenuPopover.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditReceivedTaskDialog(Guid userId, Guid taskId)
        {
            TaskBoardEditTaskDialogViewModel model = Services.TaskBoardService.PopulateReceivedEditTaskModel(userId, taskId);

            if (User.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                model.IsExecutiveUser = true;
            }
            else
            {
                model.IsExecutiveUser = false;
            }

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_EditReceivedTaskFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditReceivedTaskDialog(TaskBoardEditTaskDialogViewModel model)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = Guid.Parse(User.Identity.GetUserId());
                model.AssignedTo = model.UserSelectedId;
                Services.TaskBoardService.EditReceivedTask(currentUserId, model);
                return Content("ok");
            }

            Services.TaskBoardService.PopulateReceivedEditTaskModel(model);
            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_EditReceivedTaskFormDialog.cshtml", model);
        }


        #endregion

        public ActionResult GetEditCompleteMenuPopover(Guid userId, Guid taskId)
        {
            TaskBoardEditCompleteMenuViewModel model = new TaskBoardEditCompleteMenuViewModel
            {
                UserId = userId,
                TaskId = taskId
            };
            return PartialView("~/Views/Rotation/TaskBoard/_TaskBoardEditCompleteMenuPopover.cshtml", model);
        }

        [HttpGet]
        public ActionResult TaskDetailsDialog(Guid TaskId)
        {
            TaskBoardTaskDetailsDialogViewModel model = Services.TaskBoardService.PopulateTaskBoardTaskDetailsDialogViewModel(TaskId);

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_TaskDetailsFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult GetTaskBoardTopic(Guid TaskId)
        {
            TaskBoardCompleteItemTabViewModel model = Services.TaskBoardService.PopulateTaskBoardTopicDetailsDialogViewModel(TaskId);

            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_TopicDetailsTab.cshtml", model);
        }

        #region PDF Report

        public ActionResult TaskBoardSectionPdfReport(Guid userId, TaskBoardSection section, TaskBoardFilterOptions filter)
        {
            string footerUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/TaskBoard/PdfFooter";

            string footerHtml = String.Format(" --footer-html {0} " +
                                            "--enable-local-file-access" +
                                            " --enable-javascript" +
                                            " --footer-spacing 1 " +
                                            " --encoding utf-8", footerUrl);




            var model = Services.TaskBoardService.TaskBoardTasksForPdfReport(userId, section, filter);

            var pdfResult = new ViewAsPdf("~/Views/Rotation/TaskBoard/PDFReport/TaskBoardPdfReport.cshtml", model)
            {
                FileName = "TasksReport.pdf",
                PageOrientation = Orientation.Portrait,
                MinimumFontSize = 20,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Margins(10, 10, 10, 10)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            return File(binary, "application/pdf");
        }

        [AllowAnonymous]
        public ActionResult PdfFooter(FooterViewModel model)
        {
            return PartialView("~/Views/Rotation/TaskBoard/PDFReport/_PrintFooter.cshtml", model);
        }

        #endregion

        public ActionResult UpdateAttachmentsCount(Guid Id)
        {
            TaskBoardCompleteTaskDialogViewModel model = Services.TaskBoardService.PopulateCompleteChecklistTaskViewModel(Id, Guid.Parse(User.Identity.GetUserId()));
            return PartialView("~/Views/Rotation/TaskBoard/Dialogs/_UpdateAttachmentsCounter.cshtml", model);
        }
    }
}