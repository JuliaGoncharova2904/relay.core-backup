﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SafetyShedulerV2Controller : BaseController
    {
        //IOC
        public SafetyShedulerV2Controller(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        public ActionResult Index()
        {
            return View("~/Views/Safety/v2/Sheduler/Index.cshtml");
        }

        [HttpGet]
        public ActionResult GetFullCalendar(DateTime? date)
        {
            DateTime calsendarDate = date.HasValue ? date.Value : DateTime.UtcNow;

            SafetyCalendarV2ViewModel model = Services.SafetyMessageV2Service.GetSafetyCalendar(calsendarDate);

            return PartialView("~/Views/Safety/v2/Sheduler/_CalendarFull.cshtml", model);
        }

        [HttpGet]
        public ActionResult GetMinCalendar(DateTime? date)
        {
            DateTime activeDay = DateTime.Today.AddDays(1);
            DateTime calendarDate = (date.HasValue && date.Value >= activeDay) ? date.Value : activeDay;

            SafetyCalendarV2ViewModel model = Services.SafetyMessageV2Service.GetSafetyCalendar(calendarDate);

            ModelState.Remove("Date");

            return PartialView("~/Views/Safety/v2/Sheduler/_CalendarMin.cshtml", model);
        }

        [HttpGet]
        public ActionResult SafetyMessagesForDateDialog(DateTime date)
        {
            var model = Services.SafetyMessageV2Service.GetSafetyMessagesForDate(date);

            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessagesForDateV2Dialog.cshtml", model);
        }

        #region Add Safety Message

        [HttpGet]
        public ActionResult AddSafetyMessageDialog(DateTime? date)
        {
            EditSafetyMessageV2ViewModel model = Services.SafetyMessageV2Service.PopulateEditSafetyMessageModel(date);

            ViewBag.Title = "Schedule Safety Message";
            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessageV2FormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSafetyMessageDialog(EditSafetyMessageV2ViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SafetyMessageV2Service.AddSafetyMessage(model);
                return Content("ok");
            }

            ViewBag.Title = "Schedule Safety Message";
            Services.SafetyMessageV2Service.PopulateEditSafetyMessageModel(model);
            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessageV2FormDialog.cshtml", model);
        }

        #endregion

        #region Edit Safety Message

        [HttpGet]
        public ActionResult EditSafetyMessageDialog(Guid safetyMessageId)
        {
            EditSafetyMessageV2ViewModel model = Services.SafetyMessageV2Service.GetSafetyMessageForEdit(safetyMessageId);

            ViewBag.Title = "Edit Safety Message";
            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessageV2FormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSafetyMessageDialog(EditSafetyMessageV2ViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SafetyMessageV2Service.UpdateSafetyMessage(model);
                return Content("ok");
            }

            ViewBag.Title = "Edit Safety Message";
            Services.SafetyMessageV2Service.PopulateEditSafetyMessageModel(model);
            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessageV2FormDialog.cshtml", model);
        }

        #endregion

        #region Reschedule Safety Message

        [HttpGet]
        public ActionResult RescheduleSafetyMessageDialog(Guid safetyMessageId)
        {
            SafetyMessageRescheduleV2ViewModel model = Services.SafetyMessageV2Service.GetSafetyMessageForRescheduling(safetyMessageId);

            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessageRescheduleV2Dialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RescheduleSafetyMessageDialog(SafetyMessageRescheduleV2ViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SafetyMessageV2Service.RescheduleSafetyMessage(model);
                return Content("ok");
            }

            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessageRescheduleV2Dialog.cshtml", model);
        }

        #endregion
    }

}