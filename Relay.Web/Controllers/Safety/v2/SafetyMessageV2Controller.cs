﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SafetyMessageV2Controller : BaseController
    {
        //IOC
        public SafetyMessageV2Controller(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        public ActionResult SafetyMessagePreviewDialog(Guid safetyMessageId)
        {
            SafetyMessageV2ViewModel safetyMessage = Services.SafetyMessageV2Service.GetSafetyMessage(safetyMessageId);

            return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessagePreviewV2Dialog.cshtml", safetyMessage);
        }

        [HttpGet]
        public ActionResult GetSafetyMessagesForUser(DateTime? date)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (date.HasValue)
            {
                IEnumerable<SafetyMessageV2ViewModel> model = Services.SafetyMessageV2Service.GetSafetyMessagesForUser(date.Value, userId);

                return PartialView("~/Views/Safety/v2/Sheduler/_SafetyMessageV2Dialog.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult SetSafetyMessageShowForUser(Guid safetyMessageId)
        {
            if(safetyMessageId != Guid.Empty)
            {
                Guid userId = Guid.Parse(User.Identity.GetUserId());
                Services.SafetyMessageV2Service.SetSafetyMessageShowForUser(safetyMessageId, userId);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}