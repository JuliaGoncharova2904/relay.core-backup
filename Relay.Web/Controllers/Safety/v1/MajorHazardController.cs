﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MvcPaging;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class MajorHazardController : BaseController
    {
        public MajorHazardController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        public ActionResult Index(int? page)
        {
            var majorHazardsViewModel = Services.MajorHazardService.GetAll().ToPagedList(page - 1 ?? 0, 16);
            return View("~/Views/Safety/v1/MajorHazard/MajorHazardList.cshtml", majorHazardsViewModel);
        }

        public ActionResult NewMajorHazard()
        {
            return PartialView("~/Views/Safety/v1/MajorHazard/CreateMajorHazard.cshtml", new EditMajorHazardViewModel());
        }


        [HttpGet]
        public ActionResult EditMajorHazardDialog(Guid majorHazardId)
        {
            if (majorHazardId != Guid.Empty)
            {
                IEnumerable<CriticalControlViewModel> model = Services.CriticalControlService.GetCriticalControlsByMajorHazard(majorHazardId);
                return PartialView("~/Views/Safety/v1/MajorHazard/_EditMajorHazardDialog.cshtml", model);
            }
            
            return HttpNotFound();
        }

        [HttpGet]
        public ActionResult AddMajorHazard()
        {
            return PartialView("~/Views/Safety/v1/MajorHazard/CreateMajorHazard.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddMajorHazard(EditMajorHazardViewModel model)
        {
            if(ModelState.IsValid)
            {
                Services.MajorHazardService.AddMajorHazard(model);
                return Content("ok");
            }
            
            return PartialView("~/Views/Safety/v1/MajorHazard/CreateMajorHazard.cshtml", model);
        }

    }
}