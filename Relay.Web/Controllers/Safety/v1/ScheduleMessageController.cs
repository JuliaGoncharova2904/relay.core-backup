﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;
using System.Net;
using System.Web.Security;
using MomentumPlus.Relay.Roles;
namespace MomentumPlus.Relay.Web.Controllers
{
    public class ScheduleMessageController : BaseController
    {
        //IOC
        public ScheduleMessageController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult Index()
        {
            SchedulerViewModel model = new SchedulerViewModel
            {
                Teams = Services.TeamService.GetTeamList()
            };

            return View("~/Views/Safety/v1/ScheduleMessage/_Scheduler.cshtml", model);
        }

        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult GetFullCalendar(DateTime? date, Guid? teamId)
        {
            DateTime calsendarDate = date.HasValue ? date.Value : DateTime.UtcNow;

            SafetyCalendarViewModel model = Services.SafetyMessageService.GetSafetyCalendar(calsendarDate, teamId);

            return PartialView("~/Views/Safety/v1/ScheduleMessage/_CalendarFull.cshtml", model);
        }

        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult GetMinCalendar(DateTime? date)
        {
            DateTime calsendarDate = date.HasValue ? date.Value : DateTime.UtcNow;

            SafetyCalendarViewModel model = Services.SafetyMessageService.GetSafetyCalendar(calsendarDate);

            ModelState.Remove("Date");

            return PartialView("~/Views/Safety/v1/ScheduleMessage/_CalendarMin.cshtml", model);
        }

        [HttpPost]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult GetCriticalControlsForMajorHazard(Guid majorHazardId)
        {
            if (majorHazardId != Guid.Empty)
            {
                IEnumerable<SelectListItem> criticalContrils = Services.CriticalControlService.GetCriticalControlsListByMajorHazard(majorHazardId);
                return Json(criticalContrils.Select(cc => new { Id = cc.Value, Name = cc.Text }));
            }

            return HttpNotFound();
        }

        [HttpPost]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult GetTeamsForCriticalControl(Guid criticalControlId)
        {
            if (criticalControlId != Guid.Empty)
            {
                IEnumerable<SelectListItem> teams = Services.TeamService.GetTeamListByCriticalControl(criticalControlId);
                return Json(teams.Select(cc => new { Id = cc.Value, Name = cc.Text }));
            }

            return HttpNotFound();
        }

        #region Add Safety Message

        [HttpGet]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult AddSafetyMessageDialog(DateTime? date)
        {
            EditSafetyMessageViewModel model = Services.SafetyMessageService.PopulateSafetyMessageModel();

            model.Date = date.HasValue ? date.Value : DateTime.Now;

            return PartialView("~/Views/Safety/v1/ScheduleMessage/_SafetyMessageFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult AddSafetyMessageDialog(EditSafetyMessageViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SafetyMessageService.AddSafetyMessage(model);
                return Content("ok");
            }

            Services.SafetyMessageService.PopulateSafetyMessageModel(model);
            return PartialView("~/Views/Safety/v1/ScheduleMessage/_SafetyMessageFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Safety Message

        [HttpGet]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult EditSafetyMessageDialog(Guid safetyMessageId)
        {
            EditSafetyMessageViewModel model = Services.SafetyMessageService.GetSafetyMessage(safetyMessageId);
            if (model != null)
            {
                return PartialView("~/Views/Safety/v1/ScheduleMessage/_SafetyMessageFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult EditSafetyMessageDialog(EditSafetyMessageViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.SafetyMessageService.UpdateSafetyMessage(model);
                return Content("ok");
            }

            Services.SafetyMessageService.PopulateSafetyMessageModel(model);
            return PartialView("~/Views/Safety/v1/ScheduleMessage/_SafetyMessageFormDialog.cshtml", model);
        }

        #endregion

        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult SafetyMessagesForDateDialog(DateTime? date, Guid? teamId)
        {
            if (date.HasValue && teamId.HasValue)
            {
                SafetyMessagesViewModel safetyMessages = Services.SafetyMessageService
                                                                    .GetSafetyMessagesByDateAndTeam(date.Value, teamId.Value);

                return PartialView("~/Views/Safety/v1/ScheduleMessage/_SafetyMessagesForDateDialog.cshtml", safetyMessages);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins)]
        public ActionResult SafetyMessagePreviewDialog(Guid safetyMessageId)
        {
            if (safetyMessageId != Guid.Empty)
            {
                SafetyMessageViewModel safetyMessage = Services.SafetyMessageService.GetSafetyMessageForPreview(safetyMessageId);

                return PartialView("~/Views/Safety/v1/ScheduleMessage/_SafetyMessagePreviewDialog.cshtml", safetyMessage);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        public ActionResult SetSafetyMessageShowForUser(Guid safetyMessageId)
        {
            if (safetyMessageId != Guid.Empty)
            {
                Guid userId = (Guid)Membership.GetUser().ProviderUserKey;
                Services.SafetyMessageService.SetSafetyMessageShowForUser(userId, safetyMessageId);
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpGet]
        public ActionResult GetSafetyMessagesForUser(string date)
        {
            DateTime safetyDate;
            Guid userId = (Guid)Membership.GetUser().ProviderUserKey;

            if(DateTime.TryParse(date, out safetyDate))
            {
                IEnumerable<SafetyMessageViewModel> model = Services.SafetyMessageService.GetSafetyMessageForUser(userId, safetyDate);

                return PartialView("~/Views/Safety/v1/ScheduleMessage/_SafetyMessageDialog.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }

}