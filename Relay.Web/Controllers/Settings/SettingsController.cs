﻿using System.Web.Configuration;
using System.Web.Mvc;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SettingsController : BaseController
    {
        //IOC
        public SettingsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Index()
        {
            //================= Init access rights ==================
            bool all = true;
            bool admin = User.IsInRole(iHandoverRoles.Relay.Administrator);
            bool ihandoverAdmin = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin);

            bool isMultiTenant = AppSession.Current.IsMultiTenant;

            var hideProfile = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) && isMultiTenant;
            bool executiveUser = User.IsInRole(iHandoverRoles.Relay.ExecutiveUser);
            //----------------------------
            SettingsDialogAccessViewModel accessModel = new SettingsDialogAccessViewModel();
            //----------------------------
            accessModel.ProfileTab = !hideProfile;
            accessModel.AdminTab = admin || ihandoverAdmin || executiveUser;
            //----------------------------
            accessModel.Profile.PresonalTab = all;
            accessModel.Profile.SecurityTab = all;
            accessModel.Profile.PreferencesTab = false;
            //----------------------------
            accessModel.Admin.AccountTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.VersionDetailsTab = ihandoverAdmin && !isMultiTenant;
            accessModel.Admin.ThirdPartyTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.PreferencesTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.UsersTab = admin || ihandoverAdmin || executiveUser;
            accessModel.Admin.UpgradeTab = false;          
            //=======================================================
            return PartialView("~/Views/Settings/_SettingsDialog.cshtml", accessModel);
        }

    }
}