﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using MomentumPlus.Relay.Authorization.Helpers;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class UpgradeController : BaseController
    {
        //IOC
        public UpgradeController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }


        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {

            return View("~/Views/Upgrade/Index.cshtml");
        }


        [HttpGet]
        public ActionResult SubscriptionDetails()
        {
            UpgradeSubscriptionViewModel model = Services.AuthService.GetUpgradeSubscriptionViewModel();

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult UpgradeSubscription(UpgradeSubscriptionViewModel model)
        {
            if (!ModelState.IsValid && model.IsTrial && model.UserCountry == null)
            {
                ModelState.AddModelError("UserCountry", @"Please select country.");
            }

            if (ModelState.IsValid)
            {
                if (Services.AuthService.SubscriptionMaxUserCanChange(model.NumberOfusers))
                {
                    Services.AuthService.UpgradeSubscription(model);

                    return Json(new { status = true, data = new { message = "Upgrade Success" } }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ModelState.AddModelError("", @"The number of current active users exceeds the number you are downgrading to. Please delete users before downgrading or select more users.");
                }

            }

            return Json(new { status = false, data = new { message = ModelState.ErrorsMessage(), errors = ModelState.Errors() } }, JsonRequestBehavior.AllowGet);
        }

    }
}