﻿using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Web.Helpers;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ContributedUsersController : BaseController
    {
        public ContributedUsersController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }

        public ActionResult Index(int? page, int? pageSize)
        {
            if (!this.IsViewMode)
                AppSession.Current.ExitRotationViewModeUrl = Request.RawUrl;

            ContributedUsersViewModel model = Services.ContributorsService
                .GetContributedUsers(this.SwingOwnerId,
                    page - 1 ?? 0,
                    pageSize ?? 9);

            return View("~/Views/Contributors/Index.cshtml", model);
        }
    }
}