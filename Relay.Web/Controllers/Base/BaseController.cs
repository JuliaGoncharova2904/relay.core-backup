﻿using System;
using System.Web.Configuration;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using iHandover.Relay.Web.Attributes;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Web.Attributes;

namespace MomentumPlus.Relay.Web.Controllers
{
    [LoadMultiTenancyLayout]
    [LoadUserLayout]
    [LogFilter]
    [PaymentRequiredFilter(RedirectTemplate = "/Restriction/SendMessage?message={0}")]
    public abstract class BaseController : Controller
    {
        #region IOC

        protected IServicesUnitOfWork Services { get; private set; }

        protected BaseController(IServicesUnitOfWork servicesUnitOfWork)
        {
            this.Services = servicesUnitOfWork;
        }

        #endregion

        #region ViewMode Swing Owner ID

        private Guid? _swingOwnerId;

        public Guid SwingOwnerId
        {
            get
            {
                if (!this._swingOwnerId.HasValue)
                {
                    Guid userId;
                    string userIdString = (string)this.RouteData.Values["userId"];

                    if (!string.IsNullOrEmpty(userIdString) && Guid.TryParse(userIdString, out userId))
                    {
                        this._swingOwnerId = userId;
                    }
                    else
                    {
                        this._swingOwnerId = Guid.Parse(User.Identity.GetUserId());
                    }
                }

                return this._swingOwnerId.Value;
            }
        }

        public bool IsViewMode
        {
            get
            {
                return !string.IsNullOrEmpty((string)this.RouteData.Values["userId"]);
            }
        }

        #endregion

        public bool OffSiteUserAsked
        {
            get
            {
                return (bool)(Session["OffSiteUserAsked"] ?? false);
            }
            set
            {
                Session["OffSiteUserAsked"] = value;
            }
        }


        public bool IsMultiTenant => bool.Parse(WebConfigurationManager.AppSettings["isMultiTenant"]);
    }
}