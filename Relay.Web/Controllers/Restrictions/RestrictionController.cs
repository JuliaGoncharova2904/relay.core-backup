﻿using MomentumPlus.Relay.Interfaces;
using System.Net;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class RestrictionController : BaseController
    {
        //IOC
        public RestrictionController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        // GET: Restriction
        public ActionResult SendMessage(string message)
        {
            Response.StatusCode = (int)HttpStatusCode.PaymentRequired;

            return Json(new { message = message }, JsonRequestBehavior.AllowGet);
        }
    }
}