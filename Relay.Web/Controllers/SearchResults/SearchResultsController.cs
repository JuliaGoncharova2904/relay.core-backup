﻿using Microsoft.AspNet.Identity;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Authorization.Controllers;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SearchResultsController : BaseController
    {

        public SearchResultsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        public ActionResult SearchBox()
        {
            return PartialView("~/Views/SearchResults/SearchTopicGroupsTagsTasks.cshtml", string.Empty);
        }

        public ActionResult Index(string searchString, string urlTab, Guid? filterUserId, string selectedTopicName = null)
        {
            SearchViewModel model = new SearchViewModel
            {
                SearchText = searchString,
                FilterUserId = filterUserId,
                UsersList = Enumerable.Empty<DropDownListItemModel>(),
                SelectedTopicName = selectedTopicName,
                NameTopics = Enumerable.Empty<DropDownListItemModel>()
            };

            if (urlTab != null)
            {
                model.TypeFilter = urlTab != null && urlTab == "Tags" ? TypeFilter.Tags : urlTab != null && urlTab == "Tasks" ? TypeFilter.Tasks
                                            : urlTab != null && urlTab == "Shared" ? TypeFilter.Shared : urlTab != null && urlTab == "Metrics" ? TypeFilter.Metrics : TypeFilter.Topics;
            }

            return View("~/Views/SearchResults/Index.cshtml", model);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Metrics(int? page, int? pageSize, string searchText, Guid? filterUserId, string selectedTopicName = null)
        {
            SearchViewModel model = new SearchViewModel
            {
                PageSize = pageSize,
                PageNumber = page,
                SearchText = searchText,
                FilterUserId = filterUserId,
                TypeFilter = TypeFilter.Metrics
            };

            return View("~/Views/SearchResults/Index.cshtml", model);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Topics(int? page, int? pageSize, string searchText, Guid? filterUserId, string selectedTopicName = null)
        {
            SearchViewModel model = new SearchViewModel
            {
                PageSize = pageSize,
                PageNumber = page,
                SearchText = searchText,
                FilterUserId = filterUserId,
                TypeFilter = TypeFilter.Topics
            };

            return View("~/Views/SearchResults/Index.cshtml", model);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult SharedTopics(int? page, int? pageSize, string searchText, Guid? filterUserId, string selectedTopicName = null)
        {
            SearchViewModel model = new SearchViewModel
            {
                PageSize = pageSize,
                PageNumber = page,
                SearchText = searchText,
                FilterUserId = filterUserId,
                TypeFilter = TypeFilter.Shared
            };

            return View("~/Views/SearchResults/Index.cshtml", model);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Tags(int? page, int? pageSize, string searchText, Guid? filterUserId, string selectedTopicName = null)
        {
            SearchViewModel model = new SearchViewModel
            {
                PageSize = pageSize,
                PageNumber = page,
                SearchText = searchText,
                FilterUserId = filterUserId,
                TypeFilter = TypeFilter.Tags
            };

            return View("~/Views/SearchResults/Index.cshtml", model);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Tasks(int? page, int? pageSize, string searchText, Guid? filterUserId, string selectedTopicName = null)
        {
            SearchViewModel model = new SearchViewModel
            {
                PageSize = pageSize,
                PageNumber = page,
                SearchText = searchText,
                FilterUserId = filterUserId,
                TypeFilter = TypeFilter.Tasks
            };

            return View("~/Views/SearchResults/Index.cshtml", model);
        }

        public ActionResult SearchTopics(SearchViewModel model)
        {
            var searchResult = Services.TopicSearchService.SearchTopics(SwingOwnerId, model);

            return PartialView("~/Views/SearchResults/TopicGroupsSearchResult.cshtml", searchResult);
        }

        public ActionResult SearchSharedTopics(SearchViewModel model)
        {
            var searchResult = Services.SearchService.SearchSharedTopics(SwingOwnerId, model);

            return PartialView("~/Views/SearchResults/SharedTopicsSearchResult.cshtml", searchResult);
        }

        public ActionResult SearchMetricsTopics(SearchViewModel model)
        {
            var searchResult = Services.TopicSearchService.SearchMetricsTopics(SwingOwnerId, model);

            return PartialView("~/Views/SearchResults/ResultsSearchMetrics.cshtml", searchResult);
        }

        public ActionResult SearchTags(SearchViewModel model)
        {
            var searchResult = Services.SearchService.SearchTags(SwingOwnerId, model);

            return PartialView("~/Views/SearchResults/ResultSearchTags.cshtml", searchResult);
        }

        public ActionResult SearchTasks(SearchViewModel model)
        {
            var searchResult = Services.SearchService.SearchTasks(SwingOwnerId, model);

            return PartialView("~/Views/SearchResults/ResultSearchTasks.cshtml", searchResult);
        }

        [HttpGet]
        public ActionResult SearchReportPdf(string searchString, TypeFilter typeFilter)
        {
            SearchViewModel model = new SearchViewModel
            {
                SearchText = searchString
            };

            return PartialView("~/Views/SearchResults/ExportPdf.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SearchReportPdf(SearchViewModel model)
        {
            return Content("ok");
        }

        [Route("/SearchResults/SearchResultsReportPdf")]
        [HttpGet]
        [AllowAnonymous]
        public ActionResult SearchResultsReportPdf(string searchString, TypeFilter typeFilter, TopicSearchDateRangeType? dateRangeType = null, TopicSearchPrintType? printType = null)
        {
            SearchViewModel model = new SearchViewModel
            {
                SearchText = searchString,
                TypeFilter = typeFilter,
                DateRangeType = dateRangeType.HasValue ? dateRangeType.Value : TopicSearchDateRangeType.AllTime,
                PrintType = printType.HasValue ? printType.Value : TopicSearchPrintType.portrait
            };

            TopicReportPdfViewModel viewModel = Services.SearchService.PopulateSearchReportViewModel(SwingOwnerId, model);

            ViewAsPdf pdfResult = new ViewAsPdf();

            if (model.PrintType == TopicSearchPrintType.portrait)
            {
                pdfResult = new ViewAsPdf("~/Views/SearchResults/SearchPDFReport/PortraitViewReport.cshtml", viewModel)
                {
                    FileName = "Topic-Search-Report.pdf",
                    PageOrientation = Rotativa.Options.Orientation.Portrait,
                    IsGrayScale = false,
                    IsJavaScriptDisabled = false,
                    PageMargins = new Margins(10, 10, 10, 10)
                };
            }
            else
            {
                pdfResult = new ViewAsPdf("~/Views/SearchResults/SearchPDFReport/LandscapeViewReport.cshtml", viewModel)
                {
                    FileName = "Topic-Search-Report.pdf",
                    PageOrientation = Rotativa.Options.Orientation.Landscape,
                    IsGrayScale = false,
                    IsJavaScriptDisabled = false,
                    PageMargins = new Margins(10, 10, 10, 10)
                };
            }

            var binary = pdfResult.BuildFile(ControllerContext);

            return File(binary, "application/pdf");
        }

        public ActionResult UpdateManagerCount(Guid Id)
        {
            var count = Services.RotationTopicService.CommentCounterTopic(Id);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAttachCount(Guid Id)
        {
            var count = Services.RotationTopicService.AttachCounterTopic(Id);
            return Json(count, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateTaskAttachCount(Guid Id)
        {
            var count = Services.TaskBoardService.GetAttacmentsCounterTask(Id);
            return Json(count, JsonRequestBehavior.AllowGet);
        }
    }
}