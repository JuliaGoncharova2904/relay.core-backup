﻿using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Core.DataSource;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;
using MomentumPlus.Relay.Web.Helpers;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TeamRotationsController : BaseController
    {
        //IOC
        public TeamRotationsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }
   
        public ActionResult Index(int? page, int? pageSize)
        {
            if(!this.IsViewMode)
                AppSession.Current.ExitRotationViewModeUrl = Request.RawUrl;

            Guid userId = Guid.Parse(User.Identity.GetUserId());

            bool IdentityAdmin = userId == DbInitializerConstants.Users.iHandoverAdminUserId ? true : false;

            MyTeamRotationViewModel model = Services.TeamRotationsService
                                                    .GetMyTeamRotations(this.SwingOwnerId,
                                                                        this.IsViewMode,
                                                                        page - 1 ?? 0,
                                                                        pageSize ?? 9, IdentityAdmin);

           
            Services.UserService.UpdateLastLoginDate(userId);

            return View("~/Views/MyTeam/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditRotationDialog(Guid RotationId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            EditRotationViewModel model = Services.RotationService.GetRotation(RotationId, userId);

            if (Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                model.ContributorEnable = false;
            }
            else
            {
                model.ContributorEnable = true;
            }


            model.CanChangeLineManager = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) ||
                                            User.IsInRole(iHandoverRoles.Relay.Administrator) ||
                                            User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) ||
                                            User.IsInRole(iHandoverRoles.Relay.HeadLineManager);

            return PartialView("~/Views/RotationManager/Rotations/_EditRotationFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRotationDialog(EditRotationViewModel model)
        {
            var rotationOwnerId = Services.RotationService.GetRotationOwnerId(model.Id);
            var rotationType = Services.RotationService.GetRotationType(model.Id);

            if (rotationType == Core.Models.RotationType.Shift)
            {
                if (model.ShiftPatternType == null)
                {
                    TempData["BugPattern"] = "The ShiftPatternType* field is required.";
                    ModelState.AddModelError("ShiftPatternType", "The ShiftPatternType* field is required.");
                }
            }

            Guid userId = Guid.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {

                if (rotationType == Core.Models.RotationType.Shift && model.ShiftPatternType != null)
                {
                    Services.BoardManagerService.UpdateUserPattern(rotationOwnerId, model.ShiftPatternType);
                }

                if (Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    model.ContributorEnable = false;
                }
                else
                {
                    model.ContributorEnable = true;
                }

                if (Services.RotationService.UpdateRotation(model))
                {
                    return Content("ok");
                }

                return HttpNotFound();
            }

            model.RotationOwnerId = rotationOwnerId.ToString();
            model = Services.RotationService.PopulateEditRotationModel(model, userId);

            return PartialView("~/Views/RotationManager/Rotations/_EditRotationFormDialog.cshtml", model);
        }
    }
}