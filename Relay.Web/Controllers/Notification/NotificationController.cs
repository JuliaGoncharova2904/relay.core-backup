﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using MomentumPlus.Relay.Constants;
using System.Net;
using System.Linq;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class NotificationController : BaseController
    {
        public NotificationController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork) { }

        public ActionResult Index(int? page, int? pageSize)
        {
            var model =
                Services.NotificationService.GetNotificationsByUser(Guid.Parse(User.Identity.GetUserId()), page - 1 ?? 0, pageSize ?? NumericConstants.Paginator.PaginatorPageSize);

            return View("~/Views/Notification/Index.cshtml", model);
        }

        public ActionResult NotificationWidget()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            int msgNumber = Services.NotificationService.CountNotShownNotificationsForUser(userId);

            return PartialView("~/Views/Notification/HeaderNotification.cshtml", msgNumber);
        }

        public ActionResult GetPopover()
        {
            return PartialView("~/Views/Notification/NotificationDropDown.cshtml");
        }

        public ActionResult GetNotificationMessageForPopover(int? page, int? pageSize)
        {
            int _page = page ?? 0;
            int _pageSize = pageSize ?? 50;
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            var model = Services.NotificationService.GetNotificationsByUserForPopover(userId, _page, _pageSize);

            if (_page == 0 && !model.Any())
            {
                return PartialView("~/Views/Notification/NoMessages.cshtml", model);
            }

            return PartialView("~/Views/Notification/NotificationPopoverContent.cshtml", model);
        }

        [HttpPost]
        public ActionResult MakeLastNotificationsShown()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            Services.NotificationService.MakeLastNotificationsShownForUser(userId);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpPost]
        public ActionResult MakeNotificationOpened(Guid notificationId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            Services.NotificationService.MakeNotificationOpenedForUser(userId, notificationId);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        #region Task dialogs routing

        [HttpGet]
        public ActionResult CompleteTaskDialog(Guid TaskId)
        {
            if (Services.TaskBoardService.IsTaskBoardTask(TaskId))
            {
                return RedirectToAction("CompleteTaskChecklistDialog", "TaskBoard", new { TaskId = TaskId });
            }
            else
            {
                return RedirectToAction("CompleteHandoverTaskDialog", "TaskBoard", new { TaskId = TaskId });
            }
        }

        [HttpGet]
        public ActionResult TaskDetailsDialog(Guid TaskId)
        {
            if (Services.TaskBoardService.IsTaskBoardTask(TaskId))
            {
                return RedirectToAction("TaskDetailsDialog", "TaskBoard", new { TaskId = TaskId });
            }
            else
            {
                return RedirectToAction("TaskDetailsDialog", "Tasks", new { TaskId = TaskId });
            }
        }

        [HttpGet]
        public ActionResult EditTaskDialog(Guid TaskId)
        {
            if (Services.TaskBoardService.IsTaskBoardTask(TaskId))
            {
                Guid userId = Guid.Parse(User.Identity.GetUserId());
                return RedirectToAction("EditTaskDialog", "TaskBoard", new { userId = userId, taskId = TaskId });
            }
            else
            {
                return RedirectToAction("EditTaskFormDialog", "Tasks", new { TaskId = TaskId });
            }
        }

        #endregion

    }
}