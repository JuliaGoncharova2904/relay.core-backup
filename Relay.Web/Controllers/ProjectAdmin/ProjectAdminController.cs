﻿using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using System;
using MomentumPlus.Relay.Models;
using System.Net;
using System.Linq;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class ProjectAdminController : BaseController
    {
        //IOC
        public ProjectAdminController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index()
        {
            return View("~/Views/ProjectAdmin/Index.cshtml");
        }

        #region Projects Panel

        public ActionResult ProjectsPanel()
        {
            ProjectPanelViewModel model = new ProjectPanelViewModel();
            Guid currentUserId = Guid.Parse(User.Identity.GetUserId());

            if (this.UserIsAdmin())
            {
                model.CanCreateProject = true;
                model.Projects = Services.ProjectService.GetProjectsSlides();
            }
            else if(this.UserIsLineManager())
            {
                model.CanCreateProject = true;
                model.Projects = Services.ProjectService.GetProjectsSlidesForLineManager(currentUserId);
            }
            else if (Services.AccessService.UserIsProjectManager(currentUserId))
            {
                model.CanCreateProject = false;
                model.Projects = Services.ProjectService.GetProjectsSlidesForProjectManager(currentUserId);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);
            }

            return PartialView("~/Views/ProjectAdmin/ProjectsPanel/_ProjectsPanel.cshtml", model);
        }

        public ActionResult ProjectSlide(Guid projectId)
        {
            if(!this.UserHasAccess())
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            if (projectId != Guid.Empty)
            {
                ProjectSlideViewModel model = Services.ProjectService.GetProjectSlide(projectId);

                return PartialView("~/Views/ProjectAdmin/ProjectsPanel/_ProjectSlide.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        public ActionResult FullProjectSlide(Guid projectId)
        {
            if (!this.UserHasAccess())
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            if (projectId != Guid.Empty)
            {
                ProjectFullSlideViewModel model = Services.ProjectService.GetFullProjectSlide(projectId);

                return PartialView("~/Views/ProjectAdmin/ProjectsPanel/_ProjectFullSlide.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        #endregion

        #region Add Project

        [HttpGet]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins + "," + iHandoverRoles.RelayGroups.LineManagers)]
        public ActionResult AddProjectDialog()
        {
            ProjectDialogViewModel model = Services.ProjectService.PopulateEditProjectModel();

            return PartialView("~/Views/ProjectAdmin/ProjectsPanel/_AddProjectFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = iHandoverRoles.RelayGroups.Admins + "," + iHandoverRoles.RelayGroups.LineManagers)]
        public ActionResult AddProjectDialog(ProjectDialogViewModel model)
        {
            if(ModelState.IsValid)
            {
                if(model.StartDate > model.EndDate)
                {
                    ModelState.AddModelError("EndDate", "Due Date can't be less then Start Date.");
                }
                else
                {
                    Guid projectId = Services.ProjectService.AddProject(model, Guid.Parse(User.Identity.GetUserId()));
                    return Json(new { action = "/ProjectAdmin#", param = new { ProjectId = projectId, TabType = "Planned" } }, JsonRequestBehavior.AllowGet);
                    //return Content("ok");
                }
            }

            Services.ProjectService.PopulateEditProjectModel(model);

            return PartialView("~/Views/ProjectAdmin/ProjectsPanel/_AddProjectFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Project

        [HttpGet]
        public ActionResult EditProjectDialog(Guid projectId)
        {
            if (!this.UserHasAccess())
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            if (projectId != Guid.Empty)
            {
                ProjectDialogViewModel model = Services.ProjectService.GetProject(projectId);

                return PartialView("~/Views/ProjectAdmin/ProjectsPanel/_EditProjectFormDialog.cshtml", model);
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProjectDialog(ProjectDialogViewModel model)
        {
            if (!this.UserHasAccess())
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            if (ModelState.IsValid)
            {
                if (model.StartDate > model.EndDate)
                {
                    ModelState.AddModelError("EndDate", "Due Date can't be less then Start Date.");
                }
                else
                {
                    Services.ProjectService.UpdateProject(model);
                    return Content("ok");
                }
            }

            Services.ProjectService.PopulateEditProjectModel(model);

            return PartialView("~/Views/ProjectAdmin/ProjectsPanel/_EditProjectFormDialog.cshtml", model);
        }

        #endregion

        private bool UserIsAdmin()
        {
            return User.IsInRole(iHandoverRoles.Relay.Administrator) || User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) || User.IsInRole(iHandoverRoles.Relay.ExecutiveUser);
        }

        private bool UserIsLineManager()
        {
            return User.IsInRole(iHandoverRoles.Relay.HeadLineManager) || User.IsInRole(iHandoverRoles.Relay.LineManager);
        }

        private bool UserHasAccess()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            return this.UserIsAdmin() || this.UserIsLineManager() || Services.AccessService.UserIsProjectManager(userId);
        }

    }
}