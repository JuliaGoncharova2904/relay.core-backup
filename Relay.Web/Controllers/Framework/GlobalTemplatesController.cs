﻿using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Web.Controllers;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iHandover.Relay.Web.Controllers.Framework
{
    public class GlobalTemplatesController : BaseController
    {
        //IOC
        public GlobalTemplatesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(Guid? sectorId, Guid? templateId)
        {
            GlobalTemplatesViewModel model = new GlobalTemplatesViewModel();

            if (sectorId.HasValue)
            {
                model.ActiveSector = sectorId.Value.ToString();
                model.GlobalTemplates = Services.AuthService.GetAdminTemplates(sectorId);
            }

            model.Sectors = Services.AuthService.GetAdminSectors();

            if (templateId.HasValue && sectorId == null)
            {
                model.ActiveSector = Services.AuthService.GetSectorByTemplateId(templateId).ToString();
                model.GlobalTemplates = Services.AuthService.GetAdminTemplates(sectorId);
            }

            if (templateId.HasValue)
            {
                model.ActiveTemplate = templateId.Value.ToString();
            }
            return View("~/Views/Framework/MasterLists/GlobalTemplates/Index.cshtml", model);
        }
    }
}