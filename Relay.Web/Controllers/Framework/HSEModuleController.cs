﻿using System;
using System.Net;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class HSEModuleController : BaseController
    {
        //IOC
        public HSEModuleController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }
        public ActionResult Index(
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null)
        {
            var model = Services.ModuleService.PopulateBaseModuleViewModel(ModuleType.HSE, topicGroup, topic,
                        topicGroupsPageNo, topicsPageNo, tasksPageNo);

            return View("~/Views/Framework/Modules/HSE/Index.cshtml", model);
        }

        #region TopicGroup

        [HttpGet]
        public ActionResult CreateTopicGroupDialog(Guid? module, int? pageNumber)
        {
            if (module != null)
            {
                var model = new rTopicGroupViewModel { ModuleId = module.Value, PageNumber = pageNumber.Value, Enabled = true };

                return PartialView("~/Views/Framework/Modules/HSE/_TopicGroupFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTopicGroupDialog(rTopicGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicGroupService.TopicGroupExist(model))
                {
                    ModelState.AddModelError("Name", "Safety Type with this name already exist.");
                }

                else
                {
                    model.IsNullReport = true;
                    var topicGroupId = Services.TopicGroupService.AddTopicGroup(model);

                    var modelView = Services.ModuleService.PopulateBaseModuleViewModel(ModuleType.HSE, topicGroupId, null, model.PageNumber, null, null);

                    if (modelView.TopicGroupsPage2.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 2;
                    }

                    if (modelView.TopicGroupsPage3.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 3;
                    }

                    if (modelView.TopicGroupsPage4.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 4;
                    }

                    if (modelView.TopicGroupsPage5.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 5;
                    }

                    if (modelView.TopicGroupsPage6.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 6;
                    }

                    if (modelView.TopicGroupsPage7.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 7;
                    }

                    if (modelView.TopicGroupsPage8.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 8;
                    }

                    if (modelView.TopicGroupsPage9.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 9;
                    }

                    if (modelView.TopicGroupsPage10.Find(t => t.Id == topicGroupId) != null)
                    {
                        model.PageNumber = 10;
                    }

                    return Json(new { action = "/Framework/Modules/HSE", param = new { topicGroupsPageNo = model.PageNumber, topicGroup = topicGroupId } }, JsonRequestBehavior.AllowGet);
                    //return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/HSE/_TopicGroupFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTopicGroupDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TopicGroupService.GetTopicGroupViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/HSE/_TopicGroupFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicGroupDialog(rTopicGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicGroupService.TopicGroupExist(model))
                {
                    ModelState.AddModelError("Name", "Safety Type with this name already exist.");
                }

                else
                {
                    Services.TopicGroupService.UpdateTopicGroup(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/HSE/_TopicGroupFormDialog.cshtml", model);
        }


        [HttpPost]
        public ActionResult RemoveTopicGroup(Guid topicGroupId)
        {
            string errorMessage = string.Empty;
            if (topicGroupId != null)
            {
                Services.TopicGroupService.RemoveTopicGroup(topicGroupId);
                
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return HttpNotFound();
        }

        #endregion


        #region Topic

        [HttpGet]
        public ActionResult CreateTopicDialog(Guid? group/*, int? pageNumber*/)
        {
            if (group != null)
            {
                var model = new rTopicViewModel
                {
                    TopicGroupId = group.Value,/* PageNumber = pageNumber.Value,*/
                    Enabled = true,
                    Units = Services.RotationTopicService.GetUnitsPvA(),
                    Metrics = false
                };

                return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTopicDialog(rTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicService.TopicExist(model))
                {
                    ModelState.AddModelError("Name", "Safety Item with this name already exist.");
                }

                else
                {
                    var topicId = Services.TopicService.AddTopic(model);
                    model.Units = Services.RotationTopicService.GetUnitsPvA();

                    var modelView = Services.ModuleService.PopulateBaseModuleViewModel(ModuleType.HSE, model.TopicGroupId, topicId, model.PageNumber, null, null);

                    if (modelView.ActiveTopicGroupId.HasValue)
                    {
                        if (modelView.TopicPage2.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 2;
                        }

                        if (modelView.TopicPage3.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 3;
                        }

                        if (modelView.TopicPage4.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 4;
                        }

                        if (modelView.TopicPage5.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 5;
                        }

                        if (modelView.TopicPage6.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 6;
                        }

                        if (modelView.TopicPage7.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 7;
                        }

                        if (modelView.TopicPage8.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 8;
                        }

                        if (modelView.TopicPage9.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 9;
                        }

                        if (modelView.TopicPage10.Find(t => t.Id == topicId) != null)
                        {
                            model.PageNumber = 10;
                        }
                    }

                    return Json(new { action = "/Framework/Modules/HSE", param = new { topicGroup = model.TopicGroupId, topicsPageNo = model.PageNumber, topic = topicId, tasksPageNo = "1" } }, JsonRequestBehavior.AllowGet);
                   // return Json(new { action = "/Framework/Modules/HSE", param = new { topicGroup = model.TopicGroupId, topic = topicId, tasksPageNo = "1", topicsPageNo = model.PageNumber } }, JsonRequestBehavior.AllowGet);
                    // return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTopicDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TopicService.GetTopicViewModel((Guid)Id);
                model.Units = Services.RotationTopicService.GetUnitsPvA();

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialog(rTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicService.TopicExist(model))
                {
                    ModelState.AddModelError("Name", "Safety Item with this name already exist.");
                }

                else
                {
                    Services.TopicService.UpdateTopic(model);
                    model.Units = Services.RotationTopicService.GetUnitsPvA();

                    if (model.TemplatesIds != null && model.TemplatesIds.Count != 0)
                    {
                        foreach (var templatesId in model.TemplatesIds)
                        {
                            Services.TopicGroupService.UpdateChildTopicGroupStatus(model.TopicGroupId.Value, templatesId, true);
                            Services.TopicService.UpdateChildTopicStatus(model.Id.Value, templatesId, true, model.TopicGroupId.Value);
                        }
                    }

                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TopicFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTopicDialogFromFramework(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TopicService.GetTopicViewModel((Guid)Id);
                model.Units = Services.RotationTopicService.GetUnitsPvA();

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/Shared/_FrameworkEditTopicFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTopicDialogFromFramework(rTopicViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TopicService.TopicExist(model))
                {
                    ModelState.AddModelError("Name", "Safety Item with this name already exist.");
                }

                else
                {
                    Services.TopicService.UpdateTopicFromTamplates(model);
                    model.Units = Services.RotationTopicService.GetUnitsPvA();

                    if (model.TemplatesIds != null && model.TemplatesIds.Count != 0)
                    {
                        foreach (var templatesId in model.TemplatesIds)
                        {
                            Services.TopicGroupService.UpdateChildTopicGroupStatus(model.TopicGroupId.Value, templatesId, true);
                            Services.TopicService.UpdateChildTopicStatus(model.Id.Value, templatesId, true, model.TopicGroupId.Value);
                        }
                    }

                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_FrameworkEditTopicFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveTopic(Guid topicId)
        {
            string errorMessage = string.Empty;
            if (topicId != null)
            {
                Services.TopicService.RemoveTopic(topicId);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return HttpNotFound();
        }

        #endregion


        #region Task

        [HttpGet]
        public ActionResult CreateTaskDialog(Guid? topic)
        {
            if (topic != null)
            {
                var model = new rTaskViewModel { TopicId = topic.Value, Enabled = true };

                return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTaskDialog(rTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TaskService.TaskExist(model))
                {
                    ModelState.AddModelError("Name", "Task with this name already exist.");
                }

                else
                {
                    model.IsNullReport = true;
                    Services.TaskService.AddTask(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTaskDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TaskService.GetTaskViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTaskDialog(rTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TaskService.TaskExist(model))
                {
                    ModelState.AddModelError("Name", "Task with this name already exist.");
                }

                else
                {
                    Services.TaskService.UpdateTask(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Modules/Shared/_TaskFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveTask(Guid taskId)
        {
            string errorMessage = string.Empty;
            if (taskId != null)
            {
                Services.TaskService.RemoveTask(taskId);

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }

            return HttpNotFound();
        }

        #endregion
    }
}