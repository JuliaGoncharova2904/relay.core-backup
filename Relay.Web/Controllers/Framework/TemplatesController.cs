﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Web.Extensions;
using MvcPaging;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class TemplatesController : BaseController
    {
        //IOC
        public TemplatesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Index(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null, 
            bool? isRedirectFromCreatesUser = null,
            bool? isViewGlobalTemplates = null)
        {

            ModelState.Clear();

            TemplatesViewModel model = null;

            if (isViewGlobalTemplates.HasValue)
            {
                model = Services.TemplateService.PopulateAdminTemplatesViewModel(selectedTemplate, selectedModule, topicGroupsPageNo, topicsPageNo, tasksPageNo, topic, topicGroup, isViewGlobalTemplates);
            }
            else
            {
                model = Services.TemplateService.PopulateTemplatesViewModel(selectedTemplate, selectedModule, topicGroupsPageNo, topicsPageNo, tasksPageNo, topic, topicGroup);
            }

            model.IsMultiTenant = IsMultiTenant;
            model.IsRedirectFromCreatesUser = isRedirectFromCreatesUser;
            model.IsViewGlobalTemplates = isViewGlobalTemplates;

            return View("~/Views/Framework/Templates/Index.cshtml", model);
        }


        [HttpGet]
        public ActionResult CopyRelayTemplate(Guid? relayTemplateId)
        {
            if (relayTemplateId.HasValue)
            {
                Services.TemplateService.AddRelayTemplate(relayTemplateId.Value);
                return Content("OK");
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult UpdateTemplateTask(Guid baseTaskId, Guid templateId, bool status)
        {
            try
            {
                Services.TaskService.UpdateChildTaskStatus(baseTaskId, templateId, status);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }

        [HttpPost]
        public ActionResult UpdateTemplateTopicGroup(Guid baseTopicGroupId, Guid templateId, bool status)
        {
            try
            {
                Services.TopicGroupService.UpdateChildTopicGroupStatus(baseTopicGroupId, templateId, status);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }

        [HttpPost]
        public ActionResult UpdateTemplateTopic(Guid baseTopicId, Guid templateId, bool status)
        {
            try
            {

                Services.TopicService.UpdateChildTopicStatus(baseTopicId, templateId, status, null);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }

        [HttpPost]
        public ActionResult UpdateTemplateModule(Guid baseModuleId, Guid templateId, bool status)
        {
            try
            {
                Services.ModuleService.ChangeTemplateModuleStatus(baseModuleId, templateId, status);
                return Content("OK");

            }
            catch (Exception error)
            {
                return Content(error.Message);
            }
        }

        [HttpGet]
        public ActionResult CreateTemplateDialog()
        {
            var model = new TemplateViewModel
            {
                Id = Guid.NewGuid()
            };

            return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTemplateDialog(TemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TemplateService.TemplateExist(model))
                {
                    ModelState.AddModelError("Name", "Template with this name already exist.");
                }

                else
                {
                    Services.TemplateService.AddTemplate(model);
                    return Content("ok");
                }
            }
            return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditTemplateDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TemplateService.GetTemplateViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTemplateDialog(TemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TemplateService.TemplateExist(model))
                {
                    ModelState.AddModelError("Name", "Template with this name already exist.");
                }
                else
                {
                    Services.TemplateService.UpdateTemplate(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/Templates/_TemplateFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult CopyTemplateDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.TemplateService.GetCopyTemplateViewModel((Guid)Id);

                if (model != null)
                {
                    return PartialView("~/Views/Framework/Templates/Shared/_CopyTemplateDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CopyTemplateDialog(CopyTemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.TemplateService.CopyTemplateViewModel(model);
                return Content("ok");
            }

            return PartialView("~/Views/Framework/Templates/Shared/_CopyTemplateDialog.cshtml", model);
        }

        #region Create Topics on Framework

        [HttpGet]
        public ActionResult CreateTemplateHSETopics(string templateId, Guid moduleId)
        {
            var model = new NewTopicViewModules
            {
                Units = Services.RotationTopicService.GetUnitsPvA(),
                Sections = Services.TemplateService.GetSectionsType(),
                Metrics = false,
                SelectSection = 0.ToString(),
                TemplateId = templateId != null && templateId != "undefined" ? Guid.Parse(templateId): (Guid?)null,
                ModuleId = moduleId
            };

            return PartialView("~/Views/Framework/Templates/CreateTemplateHSETopics.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTemplateHSETopics(NewTopicViewModules model)
        {
            if (ModelState.IsValid)
            {
                model.Description = model.Description != null ? model.Description : model.DescriptionExist;

                if (model.CustomTemplateTopicGroupName != null)
                {
                    rTopicGroupViewModel rTopicGroupViewModel = new rTopicGroupViewModel
                    {
                        Name = model.CustomTemplateTopicGroupName,
                        Enabled = true,
                        ModuleId = model.ModuleId.Value
                    };

                    var topicGroupId = Services.TopicGroupService.AddTopicGroup(rTopicGroupViewModel);

                    if (model.CustomTemplateReferenceName != null)
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = topicGroupId,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };
                        var topicId = Services.TopicService.AddTopic(rTopicViewModel);

                        Services.TopicGroupService.UpdateChildTopicGroupStatus(topicGroupId, model.TemplateId.Value, true);
                        Services.TopicService.UpdateChildTopicStatus(topicId, model.TemplateId.Value, true, topicGroupId);
                    }
                }
                else
                {
                    Services.TopicGroupService.UpdateChildTopicGroupStatus(model.TopicGroupId.Value, model.TemplateId.Value, true);

                    if (model.TopicId.HasValue)
                    {
                        Services.TopicService.UpdateChildTopicStatus(model.TopicId.Value, model.TemplateId.Value, true, model.TopicGroupId.Value);
                    }
                    else
                    {
                        if (model.CustomTemplateReferenceName != null)
                        {
                            rTopicViewModel rTopicViewModel = new rTopicViewModel
                            {
                                Name = model.CustomTemplateReferenceName,
                                Enabled = true,
                                TopicGroupId = model.TopicGroupId,
                                Metrics = model.Metrics,
                                UnitsSelectedType = model.UnitsSelectedType,
                                PlannedActualHas = model.Metrics,
                                Description = model.Description
                            };
                            var topicId = Services.TopicService.AddTopic(rTopicViewModel);

                            Services.TopicService.UpdateChildTopicStatus(topicId, model.TemplateId.Value, true, model.TopicGroupId.Value);
                        }
                    }

                    if (model.Metrics.HasValue && model.TopicId.HasValue)
                    {
                        Services.TopicService.UpdateTopicMetrics(model.TopicId.Value, model.Metrics.Value, model.UnitsSelectedType);
                        Services.RotationTopicService.UpdateTemplateTopicChildTopicsStatus(model.TopicId.Value, true, model.Metrics.Value, model.UnitsSelectedType);
                    }
                }

                if (model.SaveAddAnother.HasValue && model.SaveAddAnother.Value)
                {
                    var templateViewModel = Services.TemplateService.PopulateTemplatesViewModel(model.TemplateId.Value.ToString(), ModuleType.HSE, null, null, null);

                    var newTopicViewModules = new NewTopicViewModules
                    {
                        Units = Services.RotationTopicService.GetUnitsPvA(),
                        Sections = Services.TemplateService.GetSectionsType(),
                        MetricsFalse = false,
                        Metrics = false,
                        SelectSection = 0.ToString(),
                        TemplateId = model.TemplateId,
                        ModuleId = model.ModuleId,
                        SaveAddAnother = false,
                        UnitsSelectedType = null,
                        Description = null,
                        DescriptionExist = null
                    };

                    return PartialView("~/Views/Framework/Templates/CreateTemplateHSETopics.cshtml", newTopicViewModules);
                }

                return Json(new { action = "/Templates/Index", param = new { selectedTemplate = model.TemplateId, selectedModule = model.ModuleId } }, JsonRequestBehavior.AllowGet);
            }

            model.Sections = Services.TemplateService.GetSectionsType();
            model.Units = Services.RotationTopicService.GetUnitsPvA();

            return PartialView("~/Views/Framework/Templates/CreateTemplateHSETopics.cshtml", model);
        }

        [HttpGet]
        public ActionResult IfExistTopicAndTopicGroup(Guid? topiId, Guid? topicGroupId, Guid? templateId)
        {
            if (topicGroupId.HasValue && topiId.HasValue)
            {
                var templateViewModel = Services.TemplateService.PopulateTemplatesViewModel(templateId.Value.ToString(), ModuleType.HSE, null, null, null);

                if (templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.HSE).TopicGroups.FirstOrDefault(t => t.ParentTopicGroupId == topicGroupId && t.Enabled) != null
                    && templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.HSE).TopicGroups.SelectMany(t => t.Topics).FirstOrDefault(t => t.ParentTopicId == topiId && t.Enabled) != null)
                {
                    return Json("An item with this topic and refernce already exists", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IfExistTopicAndTopicGroupCore(Guid? topiId, Guid? topicGroupId, Guid? templateId)
        {
            if (topicGroupId.HasValue && topiId.HasValue)
            {
                var templateViewModel = Services.TemplateService.PopulateTemplatesViewModel(templateId.Value.ToString(), ModuleType.Core, null, null, null);

                if (templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.Core).TopicGroups.FirstOrDefault(t => t.ParentTopicGroupId == topicGroupId && t.Enabled) != null
                    && templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.Core).TopicGroups.SelectMany(t => t.Topics).FirstOrDefault(t => t.ParentTopicId == topiId && t.Enabled) != null)
                {
                    return Json("An item with this topic and refernce already exists", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTemplateTopics(ModuleType moduleType, Guid? templateId)
        {
            var topicGroups = Services.TopicGroupService.GetTemplateTopicGroups(moduleType, templateId.Value);

            return Json(topicGroups, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTemplateTopicsCore(ModuleType moduleType, Guid? templateId)
        {
            var topicGroups = Services.TopicGroupService.GetTemplateTopicGroups(ModuleType.Core, templateId.Value);

            return Json(topicGroups, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTemplateReferences(ModuleType moduleType)
        {
            var references = Services.TopicGroupService.GetTemplateReferences(moduleType);
            return Json(references, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTemplateReferencesCore(ModuleType moduleType = ModuleType.Core)
        {
            var references = Services.TopicGroupService.GetTemplateReferences(moduleType);
            return Json(references, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTemplateReferencesByTopicGroupId(Guid? topicGroupId)
        {
            if (topicGroupId.HasValue)
            {
                var references = Services.TopicGroupService.GetTemplateReferencesByTopicGroupId(ModuleType.HSE, topicGroupId.Value);
                return Json(references, JsonRequestBehavior.AllowGet);
            }

            return  Json(new List<SelectListItem>(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetTemplateReferencesByTopicGroupIdCore(Guid? topicGroupId)
        {
            if (topicGroupId.HasValue)
            {
                var references = Services.TopicGroupService.GetTemplateReferencesByTopicGroupId(ModuleType.Core, topicGroupId.Value);
                return Json(references, JsonRequestBehavior.AllowGet);
            }

            return Json(new List<SelectListItem>(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateTemplateCoreTopics(string templateId, Guid moduleId)
        {
            var model = new NewTopicViewModules
            {
                Units = Services.RotationTopicService.GetUnitsPvA(),
                Sections = Services.TemplateService.GetSectionsType(),
                Metrics = false,
                SelectSection = 1.ToString(),
                TemplateId = templateId != null && templateId != "undefined" ? Guid.Parse(templateId) : (Guid?)null,
                ModuleId = moduleId
            };

            return PartialView("~/Views/Framework/Templates/CreateTemplateCoreTopics.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTemplateCoreTopics(NewTopicViewModules model)
        {
            if (ModelState.IsValid)
            {
                model.Description = model.Description != null ? model.Description : model.DescriptionExist;

                if (model.CustomTemplateTopicGroupName != null)
                {
                    rTopicGroupViewModel rTopicGroupViewModel = new rTopicGroupViewModel
                    {
                        Name = model.CustomTemplateTopicGroupName,
                        Enabled = true,
                        ModuleId = model.ModuleId.Value
                    };

                    var topicGroupId = Services.TopicGroupService.AddTopicGroup(rTopicGroupViewModel);

                    if (model.CustomTemplateReferenceName != null)
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = topicGroupId,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };
                        var topicId = Services.TopicService.AddTopic(rTopicViewModel);

                        Services.TopicGroupService.UpdateChildTopicGroupStatus(topicGroupId, model.TemplateId.Value, true, Core.Models.TypeOfModule.Core);
                        Services.TopicService.UpdateChildTopicStatus(topicId, model.TemplateId.Value, true, topicGroupId, Core.Models.TypeOfModule.Core);
                    }
                }
                else
                {
                    Services.TopicGroupService.UpdateChildTopicGroupStatus(model.TopicGroupId.Value, model.TemplateId.Value, true, Core.Models.TypeOfModule.Core);

                    if (model.TopicId.HasValue)
                    {
                        Services.TopicService.UpdateChildTopicStatus(model.TopicId.Value, model.TemplateId.Value, true, model.TopicGroupId.Value, Core.Models.TypeOfModule.Core);
                    }
                    else
                    {
                        if (model.CustomTemplateReferenceName != null)
                        {
                            rTopicViewModel rTopicViewModel = new rTopicViewModel
                            {
                                Name = model.CustomTemplateReferenceName,
                                Enabled = true,
                                TopicGroupId = model.TopicGroupId,
                                Metrics = model.Metrics,
                                UnitsSelectedType = model.UnitsSelectedType,
                                PlannedActualHas = model.Metrics,
                                Description = model.Description
                            };
                            var topicId = Services.TopicService.AddTopic(rTopicViewModel);

                            Services.TopicService.UpdateChildTopicStatus(topicId, model.TemplateId.Value, true, model.TopicGroupId.Value, Core.Models.TypeOfModule.Core);
                        }
                    }

                    if (model.Metrics.HasValue && model.TopicId.HasValue)
                    {
                        Services.TopicService.UpdateTopicMetrics(model.TopicId.Value, model.Metrics.Value, model.UnitsSelectedType);
                        Services.RotationTopicService.UpdateTemplateTopicChildTopicsStatus(model.TopicId.Value, true, model.Metrics.Value, model.UnitsSelectedType);
                    }
                }

                if (model.SaveAddAnother.HasValue && model.SaveAddAnother.Value)
                {
                    var templateViewModel = Services.TemplateService.PopulateTemplatesViewModel(model.TemplateId.Value.ToString(), ModuleType.HSE, null, null, null);

                    var newTopicViewModules = new NewTopicViewModules
                    {
                        Units = Services.RotationTopicService.GetUnitsPvA(),
                        Sections = Services.TemplateService.GetSectionsType(),
                        MetricsFalse = false,
                        Metrics = false,
                        SelectSection = 0.ToString(),
                        TemplateId = model.TemplateId,
                        ModuleId = model.ModuleId,
                        SaveAddAnother = false,
                        UnitsSelectedType = null,
                        Description = null,
                        DescriptionExist = null
                    };

                    return PartialView("~/Views/Framework/Templates/CreateTemplateCoreTopics.cshtml", newTopicViewModules);
                }

                return Json(new { action = "/Templates/Index", param = new { selectedTemplate = model.TemplateId, selectedModule = model.ModuleId } }, JsonRequestBehavior.AllowGet);
            }

            model.Sections = Services.TemplateService.GetSectionsType();
            model.Units = Services.RotationTopicService.GetUnitsPvA();

            return PartialView("~/Views/Framework/Templates/CreateTemplateCoreTopics.cshtml", model);
        }

        public ActionResult GetReferenceNotes(Guid? topicId)
        {
            var referencesNotes = Services.TopicGroupService.GetNotesReferences(topicId);
            return Json(referencesNotes, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetMetricsReferences(Guid? topicId)
        {
            var referencesMetrics = Services.TopicGroupService.GetMetricsReferences(topicId);
            return Json(referencesMetrics, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetUnitsReferences(Guid? topicId)
        {
            var referencesUnits = Services.TopicGroupService.GetUnitsReferences(topicId);
            return Json(referencesUnits, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Global Templates

        [HttpGet]
        public ActionResult GlobalTemplates(string selectedTemplate, Guid? selectSectorId, ModuleType? selectedModule)
        {
            ModelState.Clear();
            var model = Services.TemplateService.PopulateAdminTemplatesViewModel(selectedTemplate, selectedModule, null, null, null, null, null);
            model.Sectors = Services.AuthService.GetAdminSectors();
            model.Templates = Services.AuthService.GetAdminTemplates(selectSectorId);
            model.SelectSectorId = selectSectorId.HasValue ? selectSectorId : null;
            model.IsMultiTenant = IsMultiTenant;

            return PartialView("~/Views/Framework/Templates/AdminGlobalTemplates.cshtml", model);
        }

        [HttpGet]
        public ActionResult SectorCreateFormDialog()
        {
            var model = new SectorViewModule
            {
                Id = Guid.NewGuid()
            };

            return PartialView("~/Views/Framework/Templates/_SectorFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SectorCreateFormDialog(SectorViewModule model)
        {
            if (ModelState.IsValid)
            {
                if (Services.TemplateService.SectorExist(model))
                {
                    ModelState.AddModelError("Name", "Sector with this name already exist.");
                }

                else
                {
                    Services.AuthService.AddSector(model);
                    return Content("ok");
                }
            }
            return PartialView("~/Views/Framework/Templates/_SectorFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult CreateSectorTemplateDialog()
        {
            var model = new TemplateViewModel
            {
                Id = Guid.NewGuid(),
                Sectors = Services.AuthService.GetAdminSectors(),
                Templates = Services.AuthService.GetAdminTemplates(null)
            };

            return PartialView("~/Views/Framework/Templates/_TemplateSectorFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSectorTemplateDialog(TemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                Services.AuthService.AddTemplate(model);
                Services.TemplateService.AddTemplate(model);
                return Content("ok");
            }
            return PartialView("~/Views/Framework/Templates/_TemplateSectorFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult CreateAdminTemplateHSETopics(string templateId, Guid moduleId)
        {
            var model = new NewTopicViewModules
            {
                Units = Services.RotationTopicService.GetUnitsPvA(),
                Sections = Services.TemplateService.GetSectionsType(),
                Metrics = false,
                SelectSection = 0.ToString(),
                TemplateId = templateId != null && templateId != "undefined" ? Guid.Parse(templateId) : (Guid?)null,
                ModuleId = moduleId
            };

            return PartialView("~/Views/Framework/Templates/CreateAdminTemplateHSETopics.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdminTemplateHSETopics(NewTopicViewModules model)
        {
            if (ModelState.IsValid)
            {
                model.Description = model.Description != null ? model.Description : model.DescriptionExist;

                if (model.CustomTemplateTopicGroupName != null)
                {
                    rTopicGroupViewModel rTopicGroupViewModel = new rTopicGroupViewModel
                    {
                        Name = model.CustomTemplateTopicGroupName,
                        Enabled = true,
                        ModuleId = model.ModuleId.Value,
                        TemplateId = model.TemplateId
                    };

                    var topicGroupId = Services.AuthService.AddTopicGroup(rTopicGroupViewModel, Authorization.Domain.Entities.TypeOfModule.HSE);
                    var topiGroupID = Services.TopicGroupService.AddTopicGroup(rTopicGroupViewModel);

                    if (model.CustomTemplateReferenceName != null)
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = topicGroupId,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };
                        var topicId = Services.AuthService.AddTopic(rTopicViewModel, Authorization.Domain.Entities.TypeOfModule.HSE);

                        rTopicViewModel rTopicViewModelNew = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = topiGroupID,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };

                       var topicID =  Services.TopicService.AddTopic(rTopicViewModelNew);
                        Services.TopicService.UpdateChildTopicStatus(topicID, model.TemplateId.Value, true, topiGroupID);
                    }
                }
                else
                {
                    if (model.CustomTemplateReferenceName != null)
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = model.TopicGroupId,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };
                        var topicId = Services.AuthService.AddTopic(rTopicViewModel, Authorization.Domain.Entities.TypeOfModule.HSE);
                        Services.TopicService.AddTopic(rTopicViewModel);
                        Services.TopicService.UpdateChildTopicStatus(topicId, model.TemplateId.Value, true, model.TopicGroupId.Value);
                    }
                }

                if (model.SaveAddAnother.HasValue && model.SaveAddAnother.Value)
                {
                    var templateViewModel = Services.TemplateService.PopulateAdminTemplatesViewModel(model.TemplateId.Value.ToString(), ModuleType.HSE, null, null, null);

                    var newTopicViewModules = new NewTopicViewModules
                    {
                        Units = Services.RotationTopicService.GetUnitsPvA(),
                        Sections = Services.TemplateService.GetSectionsType(),
                        MetricsFalse = false,
                        Metrics = false,
                        SelectSection = 0.ToString(),
                        TemplateId = model.TemplateId,
                        ModuleId = model.ModuleId,
                        SaveAddAnother = false,
                        UnitsSelectedType = null,
                        Description = null,
                        DescriptionExist = null
                    };

                    return PartialView("~/Views/Framework/Templates/CreateAdminTemplateHSETopics.cshtml", newTopicViewModules);
                }

                return Json(new { action = "/Templates/Index", param = new { selectedTemplate = model.TemplateId, selectedModule = model.ModuleId } }, JsonRequestBehavior.AllowGet);
            }

            model.Sections = Services.TemplateService.GetSectionsType();
            model.Units = Services.RotationTopicService.GetUnitsPvA();


            return PartialView("~/Views/Framework/Templates/CreateAdminTemplateHSETopics.cshtml", model);
        }

        [HttpGet]
        public ActionResult IfExistAdminTopicAndTopicGroupHSE(Guid? topiId, Guid? topicGroupId, Guid? templateId)
        {
            if (topicGroupId.HasValue && topiId.HasValue)
            {
                var templateViewModel = Services.TemplateService.PopulateAdminTemplatesViewModel(templateId.Value.ToString(), ModuleType.HSE, null, null, null);

                if (templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.Core).TopicGroups.FirstOrDefault(t => t.ParentTopicGroupId == topicGroupId && t.Enabled) != null
                    && templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.Core).TopicGroups.SelectMany(t => t.Topics).FirstOrDefault(t => t.ParentTopicId == topiId && t.Enabled) != null)
                {
                    return Json("An item with this topic and refernce already exists", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult IfExistAdminTopicAndTopicGroupCore(Guid? topiId, Guid? topicGroupId, Guid? templateId)
        {
            if (topicGroupId.HasValue && topiId.HasValue)
            {
                var templateViewModel = Services.TemplateService.PopulateAdminTemplatesViewModel(templateId.Value.ToString(), ModuleType.Core, null, null, null);

                if (templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.Core).TopicGroups.FirstOrDefault(t => t.ParentTopicGroupId == topicGroupId && t.Enabled) != null
                    && templateViewModel.Template.Modules.FirstOrDefault(t => t.Type == ModuleType.Core).TopicGroups.SelectMany(t => t.Topics).FirstOrDefault(t => t.ParentTopicId == topiId && t.Enabled) != null)
                {
                    return Json("An item with this topic and refernce already exists", JsonRequestBehavior.AllowGet);
                }
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAdminTopicGroupsHSE()
        {
            var topicGroups = Services.AuthService.GetAdminTopicGroupsByType(Authorization.Domain.Entities.TypeOfModule.HSE);

            return Json(topicGroups, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAdminTopicGroupsCore()
        {
            var topicGroups = Services.AuthService.GetAdminTopicGroupsByType(Authorization.Domain.Entities.TypeOfModule.Core);

            return Json(topicGroups, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAdminTopicsHSE()
        {
            var references = Services.AuthService.GetAdminTopicsByType(Authorization.Domain.Entities.TypeOfModule.HSE);
            return Json(references, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAdminTopicsCore()
        {
            var references = Services.AuthService.GetAdminTopicsByType(Authorization.Domain.Entities.TypeOfModule.Core);
            return Json(references, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult CreateAdminTemplateCoreTopics(string templateId, Guid moduleId)
        {
            var model = new NewTopicViewModules
            {
                Units = Services.RotationTopicService.GetUnitsPvA(),
                Sections = Services.TemplateService.GetSectionsType(),
                Metrics = false,
                SelectSection = 1.ToString(),
                TemplateId = templateId != null && templateId != "undefined" ? Guid.Parse(templateId) : (Guid?)null,
                ModuleId = moduleId
            };

            return PartialView("~/Views/Framework/Templates/CreateAdminTemplateCoreTopics.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAdminTemplateCoreTopics(NewTopicViewModules model)
        {
            if (ModelState.IsValid)
            {
                model.Description = model.Description != null ? model.Description : model.DescriptionExist;

                if (model.CustomTemplateTopicGroupName != null)
                {
                    rTopicGroupViewModel rTopicGroupViewModel = new rTopicGroupViewModel
                    {
                        Name = model.CustomTemplateTopicGroupName,
                        Enabled = true,
                        ModuleId = model.ModuleId.Value,
                        TemplateId = model.TemplateId
                    };

                    var topicGroupId = Services.AuthService.AddTopicGroup(rTopicGroupViewModel, Authorization.Domain.Entities.TypeOfModule.Core);
                    var topiGroupID = Services.TopicGroupService.AddTopicGroup(rTopicGroupViewModel);

                    if (model.CustomTemplateReferenceName != null)
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = topicGroupId,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };
                        var topicId = Services.AuthService.AddTopic(rTopicViewModel, Authorization.Domain.Entities.TypeOfModule.Core);

                        rTopicViewModel rTopicViewModelNew = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = topiGroupID,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };

                        var topicID = Services.TopicService.AddTopic(rTopicViewModelNew);
                        Services.TopicService.UpdateChildTopicStatus(topicID, model.TemplateId.Value, true, topiGroupID, Core.Models.TypeOfModule.Core);
                    }
                }
                else
                {
                    if (model.CustomTemplateReferenceName != null)
                    {
                        rTopicViewModel rTopicViewModel = new rTopicViewModel
                        {
                            Name = model.CustomTemplateReferenceName,
                            Enabled = true,
                            TopicGroupId = model.TopicGroupId,
                            Metrics = model.Metrics,
                            UnitsSelectedType = model.UnitsSelectedType,
                            PlannedActualHas = model.Metrics,
                            Description = model.Description
                        };
                        var topicId = Services.AuthService.AddTopic(rTopicViewModel, Authorization.Domain.Entities.TypeOfModule.Core);
                        Services.TopicService.AddTopic(rTopicViewModel);
                        Services.TopicService.UpdateChildTopicStatus(topicId, model.TemplateId.Value, true, model.TopicGroupId.Value);
                    }
                }

                if (model.SaveAddAnother.HasValue && model.SaveAddAnother.Value)
                {
                    var templateViewModel = Services.TemplateService.PopulateAdminTemplatesViewModel(model.TemplateId.Value.ToString(), ModuleType.Core, null, null, null);

                    var newTopicViewModules = new NewTopicViewModules
                    {
                        Units = Services.RotationTopicService.GetUnitsPvA(),
                        Sections = Services.TemplateService.GetSectionsType(),
                        MetricsFalse = false,
                        Metrics = false,
                        SelectSection = 0.ToString(),
                        TemplateId = model.TemplateId,
                        ModuleId = model.ModuleId,
                        SaveAddAnother = false,
                        UnitsSelectedType = null,
                        Description = null,
                        DescriptionExist = null
                    };

                    return PartialView("~/Views/Framework/Templates/CreateAdminTemplateCoreTopics.cshtml", newTopicViewModules);
                }

                return Json(new { action = "/Templates/Index", param = new { selectedTemplate = model.TemplateId, selectedModule = model.ModuleId } }, JsonRequestBehavior.AllowGet);
            }

            model.Sections = Services.TemplateService.GetSectionsType();
            model.Units = Services.RotationTopicService.GetUnitsPvA();


            return PartialView("~/Views/Framework/Templates/CreateAdminTemplateCoreTopics.cshtml", model);
        }

        #endregion
    }
}