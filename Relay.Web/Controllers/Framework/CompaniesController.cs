﻿using System;
using System.Net;
using System.Web.Mvc;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;
using MvcPaging;

namespace MomentumPlus.Relay.Web.Controllers.Framework
{
    [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
    public class CompaniesController : BaseController
    {
        //IOC
        public CompaniesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(int? page)
        {
            var model = Services.CompanyService.GetCompanies().ToPagedList(page - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);

            return View("~/Views/Framework/MasterLists/Companies/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult CreateCompanyDialog()
        {
            ViewBag.Title = "Create Company";
            return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCompanyDialog(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.CompanyService.CompanyExist(model))
                {
                    ModelState.AddModelError("Name", "Company with this name, already exist.");
                }

                if (Services.CompanyService.IsDefaultCompanyExist(model) && model.IsDefaultCompany)
                {
                    ModelState.AddModelError("IsDefaultCompany", "The default company is already installed.");
                }

                else
                {
                    Services.CompanyService.AddCompany(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml", model);
        }



        [HttpGet]
        public ActionResult EditCompanyDialog(Guid? Id)
        {
            if (Id != null)
            {
                var model = Services.CompanyService.GetCompany((Guid)Id);
                ViewBag.Title = "Edit Company";

                if (model != null)
                {
                    return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCompanyDialog(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.CompanyService.CompanyExist(model))
                {
                    ModelState.AddModelError("Name", "Company with this name, already exist.");
                }
                if (Services.CompanyService.IsDefaultCompanyExist(model) && model.IsDefaultCompany)
                {
                    ModelState.AddModelError("IsDefaultCompany", "The default company is already installed.");
                }

                else
                {
                    Services.CompanyService.UpdateCompany(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/MasterLists/Companies/_CompanyFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveCompany(Guid companyId)
        {
            string errorMessage = string.Empty;
            if (companyId != null)
            {
                if (Services.CompanyService.CanRemoveCompany(companyId))
                {
                    Services.CompanyService.RemoveCompany(companyId);

                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    errorMessage = "You can not delete this company.";
                    return Content(errorMessage);
                }
            }
           
            return HttpNotFound();
        }

    }
}