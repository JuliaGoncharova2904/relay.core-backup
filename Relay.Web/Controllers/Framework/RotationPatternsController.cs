﻿using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using MvcPaging;
using MomentumPlus.Relay.Constants;
using System.Net;

namespace MomentumPlus.Relay.Web.Controllers.Framework
{
    public class RotationPatternsController : BaseController
    {
        //IOC
        public RotationPatternsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index(int? page)
        {
            var model = Services.RotationPatternService.GetAllRotationPatterns().ToPagedList(page - 1 ?? 0, NumericConstants.Paginator.PaginatorPageSize);
            return View("~/Views/Framework/MasterLists/RotationPatterns/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult CreateRotationPatternDialog()
        {
            ViewBag.Title = "Create Working Pattern";
            return PartialView("~/Views/Framework/MasterLists/RotationPatterns/_RotationPatternFormDialog.cshtml");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRotationPatternDialog(RotationPatternViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.RotationPatternService.RotationPatternExist(model))
                {
                    ViewBag.Title = "Create Working Pattern";
                    ModelState.AddModelError("AlreadyExist", "Similar Working Pattern, already exist.");
                }
                else
                {
                    Services.RotationPatternService.AddRotationpattern(model);
                    return Content("ok"); 
                }
            }

            return PartialView("~/Views/Framework/MasterLists/RotationPatterns/_RotationPatternFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult EditRotationPatternDialog(Guid? Id)
        {
            if(Id != null)
            {
                RotationPatternViewModel model = Services.RotationPatternService.GetRotationPaternById((Guid)Id);
                ViewBag.Title = "Edit Handover Settings";

                if (model != null)
                {
                    return PartialView("~/Views/Framework/MasterLists/RotationPatterns/_RotationPatternFormDialog.cshtml", model);
                } 
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRotationPatternDialog(RotationPatternViewModel model)
        {
            if (ModelState.IsValid)
            {
                if(Services.RotationPatternService.RotationPatternExist(model))
                {
                    ViewBag.Title = "Edit Handover Settings";
                    ModelState.AddModelError("AlreadyExist", "Similar Working Pattern, already exist.");
                }
                else
                {
                    Services.RotationPatternService.UpdateRotationPattern(model);
                    return Content("ok");
                }
            }

            return PartialView("~/Views/Framework/MasterLists/RotationPatterns/_RotationPatternFormDialog.cshtml", model);
        }


        [HttpPost]
        public ActionResult RemoveRotationPattern(Guid rotationPatternId)
        {
            string errorMessage = string.Empty;
            if (rotationPatternId != null)
            {
                if (Services.RotationPatternService.CanRemoveRotationPattern(rotationPatternId))
                {
                    Services.RotationPatternService.RemoveRotationpattern(rotationPatternId);
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    errorMessage = "You can not delete this working pattern.";
                    return Content(errorMessage);
                }
            }

            return HttpNotFound();
        }
    }
}