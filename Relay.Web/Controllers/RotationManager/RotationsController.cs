﻿using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Linq;
using System.Web.Mvc;
using MomentumPlus.Relay.Web.Helpers;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Controllers
{
    [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
    public class RotationsController : BaseController
    {
        //IOC
        public RotationsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        // GET: Rotations
        public ActionResult Index()
        {
            AppSession.Current.ExitRotationViewModeUrl = Request.RawUrl;

            return View("~/Views/RotationManager/Rotations/Index.cshtml");
        }

        [HttpPost]
        public ActionResult GetRotationsForPosition(int? page, Guid positionId)
        {

            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var rotations = Services.RotationService.GetRotationItemsForPosition(positionId)
                                .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);
            return Json(new
            {
                Rotations = rotations.Select(m => new { Id = m.RotationId, Name = m.RotationOwnerName, userId = m.RotationOwnerId }),
                IsFirstPage = rotations.IsFirstPage,
                IsLastPage = rotations.IsLastPage,
                PageNumber = rotations.PageNumber,
                PageCount = rotations.PageCount
            });
        }

        #region Create Dialog

        [HttpGet]
        [Obsolete]
        public ActionResult CreateRotationDialog(Guid? SiteId, Guid? PositionId)
        {
            ModelState.Clear();
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            CreateRotationViewModel model = Services.RotationService.PopulateEditRotationModel(new CreateRotationViewModel()
            {
                PositionId = PositionId,
                SiteId = SiteId,
                ContributorEnable = Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic ? false : true,
                ShiftPatterns = Services.RotationPatternService.GetShiftPatternsList(),
                DefaultBackToBackId = Services.AuthService.GetSubscriptionOwnerId().Value.ToString()
                //ContributorEnable = Services.AuthService.ContributorEnable(userId)
            }, userId);

            return PartialView("~/Views/RotationManager/Rotations/_CreateRotationFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Obsolete]
        public ActionResult CreateRotationDialog(CreateRotationViewModel model)
        {
            if (model.RotationType == RotationType.Shift)
            {
                if (model.ShiftPatternType == null)
                {
                    TempData["BugPattern"] = "The ShiftPatternType* field is required.";
                    ModelState.AddModelError("ShiftPatternType", "The ShiftPatternType* field is required.");
                }
            }

            if (ModelState.IsValid)
            {
                if (model.RotationType == RotationType.Shift && model.ShiftPatternType != null)
                {
                    Services.BoardManagerService.UpdateUserPattern(Guid.Parse(model.RotationOwnerId), model.ShiftPatternType);
                }

                if (model.DefaultBackToBackId == null)
                {
                    model.DefaultBackToBackId = Services.AuthService.GetSubscriptionOwnerId().Value.ToString();
                }

                if (Services.RotationService.CreateFirstRotation(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("RotationOwnerId", "This employee already have rotation.");
                }
            }

            Guid userId = Guid.Parse(User.Identity.GetUserId());
            model = Services.RotationService.PopulateEditRotationModel(model, userId);

            return PartialView("~/Views/RotationManager/Rotations/_CreateRotationFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Dialog

        [HttpGet]
        public ActionResult EditRotationDialog(Guid RotationId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            EditRotationViewModel model = Services.RotationService.GetRotation(RotationId, userId);

            if (Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                model.ContributorEnable = false;
            }
            else
            {
                model.ContributorEnable = true;
            }


            model.CanChangeLineManager = User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) ||
                                            User.IsInRole(iHandoverRoles.Relay.Administrator) ||
                                            User.IsInRole(iHandoverRoles.Relay.ExecutiveUser) ||
                                            User.IsInRole(iHandoverRoles.Relay.HeadLineManager);

            return PartialView("~/Views/RotationManager/Rotations/_EditRotationFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRotationDialog(EditRotationViewModel model)
        {
            var rotationOwnerId = Services.RotationService.GetRotationOwnerId(model.Id);
            var rotationType = Services.RotationService.GetRotationType(model.Id);

            if (rotationType == Core.Models.RotationType.Shift)
            {
                if (model.ShiftPatternType == null)
                {
                    TempData["BugPattern"] = "The ShiftPatternType* field is required.";
                    ModelState.AddModelError("ShiftPatternType", "The ShiftPatternType* field is required.");
                }
            }

            Guid userId = Guid.Parse(User.Identity.GetUserId());
            if (ModelState.IsValid)
            {

                if (rotationType == Core.Models.RotationType.Shift && model.ShiftPatternType != null)
                {
                    Services.BoardManagerService.UpdateUserPattern(rotationOwnerId, model.ShiftPatternType);
                }

                if (Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    model.ContributorEnable = false;
                }
                else
                {
                    model.ContributorEnable = true;
                }

                if (Services.RotationService.UpdateRotation(model))
                {
                    return Content("ok");
                }

                return HttpNotFound();
            }

            model = Services.RotationService.PopulateEditRotationModel(model, userId);

            return PartialView("~/Views/RotationManager/Rotations/_EditRotationFormDialog.cshtml", model);
        }

        #endregion
    }
}