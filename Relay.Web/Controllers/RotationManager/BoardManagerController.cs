﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class BoardManagerController : BaseController
    {
        //IOC
        public BoardManagerController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        #region Step One

        [HttpGet]
        public ActionResult StepOneDialog()
        {
            var model = new BoardManagerStepOneViewModel();
            
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            Services.RestrictionService.UsersLimitExceeded(userId, iHandoverRoles.Relay.User);

            return PartialView("~/Views/RotationManager/BoardManager/_StepOneFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StepOneDialog(BoardManagerStepOneViewModel model)
        {

            if (!model.SiteId.HasValue && string.IsNullOrWhiteSpace(model.CustomSiteName))
            {
                ModelState.AddModelError("SiteId", "The Location* field is required.");
            }

            if (!model.PositionId.HasValue && string.IsNullOrWhiteSpace(model.CustomPositionName))
            {
                ModelState.AddModelError("PositionId", "The Job Position* field is required.");
            }

            if (!model.TemplateId.HasValue && string.IsNullOrWhiteSpace(model.CustomTemplateName))
            {
                ModelState.AddModelError("TemplateId", "The Template* field is required.");
            }

            if (ModelState.IsValid)
            {
                Guid userPositionId = Services.BoardManagerService.CreateSitePositionAndTemplate(model);

                return Json(new { positionId = userPositionId }, JsonRequestBehavior.AllowGet);
            }

            model = Services.BoardManagerService.PopulateStepOneViewModel(model);

            return PartialView("~/Views/RotationManager/BoardManager/_StepOneFormDialog.cshtml", model);
        }

        [HttpGet]
        public ActionResult Sites()
        {
            var sites = Services.SiteService.GetSitesList();

            return Json(sites, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Positions(Guid? siteId = null)
        {
            var positions = Services.BoardManagerService.GetPositions(siteId);

            return Json(positions, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPositionTemplate(Guid positionId)
        {
            var templates = Services.BoardManagerService.GetPositionTemplate(positionId);

            return Json(templates, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Templates()
        {
            var templates = Services.BoardManagerService.GetTemplates();

            return Json(templates, JsonRequestBehavior.AllowGet);
        }

        #endregion



        #region Step Two

        [HttpGet]
        public ActionResult StepTwoDialog(Guid positionId)
        {
            var model = Services.BoardManagerService.PopulateStepTwoViewModel(positionId);

            return PartialView("~/Views/RotationManager/BoardManager/_StepTwoFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StepTwoDialog(BoardManagerStepTwoViewModel model)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (!model.TeamId.HasValue && string.IsNullOrWhiteSpace(model.CustomTeamName))
            {
                ModelState.AddModelError("CustomTeamName", "The Team* field is required.");
            }

            if (ModelState.IsValid)
            {
                Services.RestrictionService.UsersLimitExceeded(userId, model.UserTypeId);

                if (Services.AuthService.CreateUser(model.Email, model.Password, true))
                {
                    Guid userTeamId;

                    var newUserId = Services.BoardManagerService.CreateUser(model, out userTeamId);

                    var userSecurityStamp = Services.AuthService.GetSecurityStampForUpdatePassword(newUserId);
                    var resetUrl = Url.Action("UpdatePassword", "Account", new { userId = newUserId, updatePasswordHash = userSecurityStamp }, protocol: Request.Url.Scheme);
                    Services.AuthService.SendMail(newUserId, model.Name, resetUrl);

                    if (model.Continue)
                    {
                        return Json(new {userId = newUserId, teamId = userTeamId }, JsonRequestBehavior.AllowGet);
                    }

                    return Json(new { action = "/RotationManager/Employees#", param = new { teamPage = "1", employeePage = "1", teamId = userTeamId } }, JsonRequestBehavior.AllowGet);
                   // return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Email", "User with the same login, already exist.");
                }
            }

            model = Services.BoardManagerService.PopulateStepTwoViewModel(model);

            return PartialView("~/Views/RotationManager/BoardManager/_StepTwoFormDialog.cshtml", model);
        }


        [HttpGet]
        public ActionResult Teams()
        {
            var sites = Services.TeamService.GetTeamList();

            return Json(sites, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSelectedTeam(Guid teamId)
        {
            var selectedTeam = Services.BoardManagerService.GetSelectedTeam(teamId);

            return Json(selectedTeam, JsonRequestBehavior.AllowGet);
        }

        #endregion



        #region Step Three

        [HttpGet]
        public ActionResult StepThreeDialog(Guid userId, Guid teamId)
        {
            var model = Services.BoardManagerService.PopulateStepThreeViewModel(userId);
            model.TeamUserId = teamId;

            var subscriptionType = Services.AuthService.GetSubscriptionType(userId);

            if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                model.ContributorEnable = false;
            }
            else
            {
                model.ContributorEnable = true;
            }

            return PartialView("~/Views/RotationManager/BoardManager/_StepThreeFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult StepThreeDialog(BoardManagerStepThreeViewModel model)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (Services.AuthService.GetSubscriptionType(userId) == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                model.ContributorEnable = false;
            }
            else
            {
                model.ContributorEnable = true;
            }

            if (model.RotationType == RotationType.Shift)
            {
                if (model.ShiftPatternType == null)
                {
                    TempData["BugPattern"] = "The ShiftPatternType* field is required.";
                    ModelState.AddModelError("ShiftPatternType", "The ShiftPatternType* field is required.");
                }
            }

            if (ModelState.IsValid)
            {
                if (Services.BoardManagerService.CreateRotation(model))
                {
                    if (model.RotationType == RotationType.Shift)
                    {
                        Services.BoardManagerService.UpdateUserPattern(model.UserId, model.ShiftPatternType);
                    }

                    model = Services.BoardManagerService.PopulateStepThreeViewModel(model);

                    if (User.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) || User.IsInRole(iHandoverRoles.Relay.Administrator))
                    {
                        return Json(new { action = "/Framework/Templates", param = new { selectedTemplate = model.TemplateId, isRedirectFromCreatesUser = true } }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Content("ok");
                    }

                    //return Json(new { action = "/RotationManager/Employees#", param = new { teamPage = "1", employeePage = "1", teamId = model.TeamUserId } }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ModelState.AddModelError("UserId", "This user already have rotation.");
                }
            }

            model = Services.BoardManagerService.PopulateStepThreeViewModel(model);

            return PartialView("~/Views/RotationManager/BoardManager/_StepThreeFormDialog.cshtml", model);
        }

        #endregion

    }
}