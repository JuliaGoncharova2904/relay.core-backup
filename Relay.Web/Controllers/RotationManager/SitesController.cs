﻿using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using System.Web.Mvc;
using System;
using MomentumPlus.Relay.Constants;
using MvcPaging;
using System.Net;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class SitesController : BaseController
    {
        //IOC
        public SitesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpPost]
        public ActionResult GetAllSites(int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var sites = Services.SiteService.GetAllSites()
                                .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);
            return Json(new
            {
                Sites = sites,
                IsFirstPage = sites.IsFirstPage,
                IsLastPage = sites.IsLastPage,
                PageNumber = sites.PageNumber,
                PageCount = sites.PageCount
            });
        }

        [HttpPost]
        public ActionResult GetSiteForPosition(Guid positionId)
        {
            return Json(Services.SiteService.GetSiteForPosition(positionId));
        }

        [HttpPost]
        public ActionResult CanCreateSiteDialog()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            var restrictionType = Services.AuthService.GetSubscriptionType(userId);

            if (restrictionType == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                return Json(false); 
            }
            else
            {
                return Json(true);
            }
        }


        [HttpPost]
        public ActionResult CreateSiteDialogForIntermediate()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            var restrictionType = Services.AuthService.GetSubscriptionType(userId);

            if (restrictionType == Authorization.Domain.Entities.RestrictionType.Premium)
            {
                var countSites = Services.SiteService.GetSites();

                if (countSites.Count < 3)
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }
            }

            return Json(true);
        }

        #region Create Dialog

        [HttpGet]
        public ActionResult CreateSiteDialog()
        {
            ViewBag.Title = "Create Location";

            var model = Services.SiteService.PopulateAddSiteModel();
     
            return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateSiteDialog(SiteViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.SiteService.AddSite(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Name", "Location with the same Name, already exist.");
                }
            }

            ViewBag.Title = "Create Location";
            return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Dialog

        [HttpGet]
        public ActionResult EditSiteDialog(Guid SiteId)
        {
            if (SiteId != Guid.Empty)
            {
                Guid userId = Guid.Parse(User.Identity.GetUserId());
                SiteViewModel model = Services.SiteService.GetSite(SiteId, userId);
                ViewBag.Title = "Edit Location";
                if (model != null)
                {
                    return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditSiteDialog(SiteViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.SiteService.UpdateSite(model))
                {
                    return Content("ok");
                }

                return HttpNotFound();
            }

            ViewBag.Title = "Edit Location";
            return PartialView("~/Views/RotationManager/Sites/_SiteFormDialog.cshtml", model);
        }

        [HttpPost]
        public ActionResult RemoveSites(Guid siteId)
        {
            string errorMessage = string.Empty;
            if (siteId != null)
            {
                if (Services.SiteService.CanRemoveSite(siteId))
                {
                    Services.SiteService.RemoveSite(siteId);

                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    errorMessage = "You can not delete this location.";
                    return Content(errorMessage);
                }
            }

            return HttpNotFound();
        }

        #endregion
    }
}