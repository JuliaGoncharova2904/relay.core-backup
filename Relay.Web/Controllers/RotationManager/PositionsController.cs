﻿using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class PositionsController : BaseController
    {
        //IOC
        public PositionsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpPost]
        public ActionResult GetPositionsForSite(int? page, Guid siteId)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var positions = Services.PositionService.GetPositionsForSite(siteId)
                                .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);
            return Json(new
            {
                Positions = positions.Select(m => new { Id = m.Id, Name = m.Name }),
                IsFirstPage = positions.IsFirstPage,
                IsLastPage = positions.IsLastPage,
                PageNumber = positions.PageNumber,
                PageCount = positions.PageCount
            });
        }

        [HttpPost]
        public ActionResult GetPositionForEmployee(Guid employeeId)
        {
            return Json(Services.PositionService.GetPositionForEmployee(employeeId));
        }

        #region Create Dialog

        [HttpGet]
        public ActionResult CreatePositionDialog(PositionViewModel model)
        {
            ModelState.Clear();

            model = Services.PositionService.PopulatePositionModel(model);
            ViewBag.Title = "Create Job Title";

            return PartialView("~/Views/RotationManager/Positions/_PositionFormDialog.cshtml", model);
        }

        [HttpPost]
        [ActionName("CreatePositionDialog")]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePositionDialog_post(PositionViewModel model)
        {

            if (!model.TemplateId.HasValue && string.IsNullOrWhiteSpace(model.CustomTemplateName))
            {
                ModelState.AddModelError("TemplateId", "The Template* field is required.");
            }

            if (ModelState.IsValid)
            {
                if (Services.PositionService.AddPosition(model))
                {
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Name", "Job Position with the same Name, already exist.");
                }
            }

            model = Services.PositionService.PopulatePositionModel(model);


            return PartialView("~/Views/RotationManager/Positions/_PositionFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Dialog

        [HttpGet]
        public ActionResult EditPositionDialog(Guid PositionId)
        {
            if (PositionId != Guid.Empty)
            {
                PositionViewModel model = Services.PositionService.GetPosition(PositionId);
                ViewBag.Title = "Edit Job Title";
                if (model != null)
                {
                    return PartialView("~/Views/RotationManager/Positions/_PositionFormDialog.cshtml", model);
                }
            }

            return HttpNotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPositionDialog(PositionViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.PositionService.UpdatePosition(model))
                {
                    return Content("ok");
                }

                return HttpNotFound();
            }

            model = Services.PositionService.PopulatePositionModel(model);

            return PartialView("~/Views/RotationManager/Positions/_PositionFormDialog.cshtml", model);
        }


        [HttpPost]
        public ActionResult RemovePosition(Guid positionId)
        {
            string errorMessage = string.Empty;
            if (positionId != null)
            {
                if (Services.PositionService.CanRemovePosition(positionId))
                {
                    Services.PositionService.RemovePosition(positionId);

                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
                else
                {
                    errorMessage = "You can not delete this job.";
                    return Content(errorMessage);
                }
            }

            return HttpNotFound();
        }

        #endregion
    }
}