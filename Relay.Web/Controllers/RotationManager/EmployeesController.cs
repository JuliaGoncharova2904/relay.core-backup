﻿using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Constants;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MomentumPlus.Relay.Roles;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class EmployeesController : BaseController
    {
        //IOC
        public EmployeesController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        [HttpGet]
        public ActionResult Index()
        {
            return View("~/Views/RotationManager/Employees/Index.cshtml");
        }

        [HttpPost]
        public ActionResult GetEmployeesForTeam(int? page, Guid teamId)
        {
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            IPagedList<SelectListItem> employees = Services.UserService
                                                            .GetEmployeesForTeam(teamId)
                                                            .ToPagedList(currentPageIndex, NumericConstants.Paginator.PaginatorPageSize);

            return Json(new
            {
                Employees = employees.Select(m => new { Id = m.Value, Name = m.Text }),
                IsFirstPage = employees.IsFirstPage,
                IsLastPage = employees.IsLastPage,
                PageNumber = employees.PageNumber,
                PageCount = employees.PageCount
            });
        }

        [HttpPost]
        public ActionResult GetEmployeesForPosition(Guid positionId)
        {
            IEnumerable<EmployeeViewModel> employees = Services.UserService.GetEmployeesForPosition(positionId);

            return Json(employees.Select(m => new { Id = m.Id, Name = m.Name + " " + m.Surname }).OrderBy(em => em.Name));
        }

        [HttpPost]
        public ActionResult GetAllEmployees(Guid positionId, Guid? rotationOwnerId)
        {
            IEnumerable<EmployeeViewModel> employeesList = Services.UserService.GetAllEmployees();

            var employees = rotationOwnerId.HasValue
            //   ? employeesList.Where(e => e.Id != rotationOwnerId).OrderBy(e => e.Name)
                ? employeesList.OrderBy(e => e.Name)
                : employeesList.OrderBy(e => e.Name);
            return Json(employees.Select(m => new { Id = m.Id, Name = m.Name + " " + m.Surname }));
        }

        #region Create Dialog

        [HttpGet]
        public ActionResult CreateEmployeeDialog(CreateEmployeeViewModel model)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            ModelState.Clear();
            model = (CreateEmployeeViewModel)Services.UserService.PopulateEmployeeModel(model, userId);

            return PartialView("~/Views/RotationManager/Employees/_CreateEmployeeFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateEmployeeDialog(CreateEmployeeViewModel model, HttpPostedFileBase profileImage)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                Services.RestrictionService.UsersLimitExceeded(userId, model.UserTypeId);

                if (Services.AuthService.CreateUser(model.Email, model.Password))
                {
                    Services.UserService.CreateEmployee(model, profileImage);
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Email", "User with the same login, already exist.");
                }
            }

            model = (CreateEmployeeViewModel)Services.UserService.PopulateEmployeeModel(model, userId);
            return PartialView("~/Views/RotationManager/Employees/_CreateEmployeeFormDialog.cshtml", model);
        }

        #endregion

        #region Edit Dialog

        [HttpGet]
        public ActionResult EditEmployeeDialog(Guid EmployeeId)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());
            EmployeeViewModel model = Services.UserService.GetEmployeeById(EmployeeId, userId);

            return PartialView("~/Views/RotationManager/Employees/_EditEmployeeFormDialog.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployeeDialog(EmployeeViewModel model, HttpPostedFileBase profileImage)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (ModelState.IsValid)
            {
                if (!model.Id.HasValue)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                else if (Services.AuthService.ChangeUserLogin(model.Id.Value, model.Email))
                {
                    string prevRole = Services.AuthService.GetUserRoles(model.Id.Value).FirstOrDefault();

                    if (!string.IsNullOrEmpty(model.UserTypeId) && prevRole != model.UserTypeId)
                    {
                        Services.RestrictionService.UsersLimitExceeded(userId, model.UserTypeId);
                        Services.AuthService.RemoveUserFromRole(model.Id.Value, prevRole);
                        Services.AuthService.AddUserToRole(model.Id.Value, model.UserTypeId);
                    }

                    Services.UserService.UpdateEmployee(model, profileImage);
                    return Content("ok");
                }
                else
                {
                    ModelState.AddModelError("Email", "User with the same login already exist.");
                }
            }

            model = Services.UserService.PopulateEmployeeModel(model, userId);

            return PartialView("~/Views/RotationManager/Employees/_EditEmployeeFormDialog.cshtml", model);
        }

        #endregion

        #region Change Password Dialog

        [HttpGet]
        [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
        public ActionResult ChangeEmployeePasswordDialog(Guid employee)
        {
            ChangeEmployeePasswordViewModel model = new ChangeEmployeePasswordViewModel { EmployeeId = employee };

            return PartialView("~/Views/RotationManager/Employees/_ChangeEmployeePasswordFormDialog.cshtml", model);
        }

        [HttpPost]
        [Authorize(Roles = iHandoverRoles.Relay.Administrator + "," + iHandoverRoles.Relay.iHandoverAdmin + "," + iHandoverRoles.Relay.ExecutiveUser)]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeEmployeePasswordDialog(ChangeEmployeePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (Services.AuthService.ResetUserPassword(model.EmployeeId, model.NewPassword))
                {
                    return Content("ok");
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }

            return PartialView("~/Views/RotationManager/Employees/_ChangeEmployeePasswordFormDialog.cshtml", model);
        }

        #endregion
    }
}