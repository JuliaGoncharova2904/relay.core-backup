﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class MenuController : BaseController
    {
        public MenuController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        {
        }

        public ActionResult GetNavigationBar()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            ViewBag.isMultiTenancyTheme = IsMultiTenant;
            return PartialView("~/Views/Shared/Layouts/Main/_MainNavigationBar.cshtml", IsViewMode);
        }
    }
}