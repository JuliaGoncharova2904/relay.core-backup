﻿using System;
using System.Web.Mvc;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using MvcPaging;
using Microsoft.AspNet.Identity;
using Rotativa.Options;
using Rotativa;

namespace MomentumPlus.Relay.Web.Controllers
{
    public class GlobalReportsController : BaseController
    {
        //IOC
        public GlobalReportsController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Index()
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            if (Services.AccessService.IsGlobalReportHistoryAndReceivedDisable(userId))
            {
                return RedirectToAction("TeamReport");
            }

            return RedirectToAction("HistoryReport");
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult Global(int? page, int? pageSize, Guid? filterUserId, RotationType? filterRotationType)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            GlobalReportsListViewModel model = Services.GlobalReportService.GetGlobalReportsItemsForUser(userId, filterUserId, filterRotationType, page, pageSize);

            var subscriptionType = Services.AuthService.GetSubscriptionType(userId);

            if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                model.EnabledIcons = false;
            }
            else
            {
                model.EnabledIcons = true;
            }
            return View("~/Views/Reports/GlobalReports/GlobalReport/Index.cshtml", model);
        }


        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult HistoryReport(int? page, int? pageSize)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            IPagedList<HistoryGlobalReportItemViewModel> model = Services.GlobalReportService.GetHistoryReportsItemsForUser(userId, page, pageSize);

            var subscriptionType = Services.AuthService.GetSubscriptionType(userId);

            foreach (var item in model)
            {
                if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    item.EnabledIcons = false;
                }
                else
                {
                    item.EnabledIcons = true;
                }
            }

            return View("~/Views/Reports/GlobalReports/HistoryReport/Index.cshtml", model);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult ReceivedReport(int? page, int? pageSize)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            IPagedList<ReceivedGlobalReportItemViewModel> model = Services.GlobalReportService.GetReceivedReportsItemsForUser(userId, page, pageSize);

            var subscriptionType = Services.AuthService.GetSubscriptionType(userId);

            foreach (var item in model)
            {
                if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
                {
                    item.EnabledIcons = false;
                }
                else
                {
                    item.EnabledIcons = true;
                }
            }

            return View("~/Views/Reports/GlobalReports/ReceivedReport/Index.cshtml", model);
        }

        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult SharedReport(int? page, int? pageSize)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            IPagedList<SharedGlobalReportItemViewModel> model = Services.GlobalReportService.GetSharedReportsItemsForUser(userId, page, pageSize);
           
            var subscriptionType = Services.AuthService.GetSubscriptionType(userId);
            bool sharedEnabled = false;
            if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                sharedEnabled = false;
            }
            else
            {
                sharedEnabled = true;
            }

            //var sharedEnabled = Services.AuthService.ContributorEnable(userId);

            Tuple<IPagedList<SharedGlobalReportItemViewModel>, bool> modelTuple = new Tuple<IPagedList<SharedGlobalReportItemViewModel>, bool>(model, sharedEnabled);

            return View("~/Views/Reports/GlobalReports/SharedReport/Index.cshtml", modelTuple);
        }



        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult SharedTopics(int? page, int? pageSize)
        {
           // Guid userId = Guid.Parse(User.Identity.GetUserId());

            IPagedList<SharedTopicGlobalReportItemViewModel> model = Services.GlobalReportService.GetSharedTopicItemsForUser(SwingOwnerId, page, pageSize);

            return View("~/Views/Reports/GlobalReports/SharedTopics/Index.cshtml", model);
        }



        [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
        public ActionResult TeamReport(int? page, int? pageSize, Guid? filterUserId, RotationType? filterRotationType)
        {
            Guid userId = Guid.Parse(User.Identity.GetUserId());

            GlobalReportsListViewModel model = Services.GlobalReportService.GetTeamReportsItemsForUser(userId, filterUserId, filterRotationType, page, pageSize);

            var subscriptionType = Services.AuthService.GetSubscriptionType(userId);

            if (subscriptionType == Authorization.Domain.Entities.RestrictionType.Basic)
            {
                model.EnabledIcons = false;
            }
            else
            {
                model.EnabledIcons = true;
            }

            return View("~/Views/Reports/GlobalReports/TeamReport/Index.cshtml", model);
        }

        #region QME Report

        [Route("/Reports/QMEReport")]
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ReportPrint(Guid sourceId, bool isShift, bool isReceived = false, bool isHistoryPanel = false, Guid? selectedSourceId = null)
        {
            string footerUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/GlobalReports/PdfFooter";

            string footerHtml = String.Format(" --footer-html {0} " +
                                              "--enable-local-file-access" +
                                              " --enable-javascript" +
                                              " --footer-spacing 1 " +
                                              " --encoding utf-8", footerUrl);

            var model = Services.ReportService.PopulateQMERotationReportViewModel(sourceId, isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft, isShift, isHistoryPanel, selectedSourceId);

            if (isShift)
            {
                model.ShiftType = Services.ShiftService.GetShiftType(sourceId);
            }

            var pdfResult = new ViewAsPdf("~/Views/Reports/QMEReport/PrintQMEReport.cshtml", model)
            {
                FileName = "Rotation-Report.pdf",
                PageOrientation = Orientation.Portrait,
                MinimumFontSize = 18,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Margins(10, 10, 10, 10)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            return File(binary, "application/pdf");
        }

        public ActionResult Report(Guid sourceId, bool isShift, bool isReceived = false, Guid? selectedSourceId = null)
        {
            var model = Services.ReportService.PopulateQMERotationReportViewModel(sourceId, isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft, isShift, false, selectedSourceId);

            return View("~/Views/Reports/QMEReport/PrintQMEReport.cshtml", model);
        }

        [AllowAnonymous]
        public ActionResult PdfFooter(FooterViewModel model)
        {
            return PartialView("~/Views/Rotation/TaskBoard/PDFReport/_PrintFooter.cshtml", model);
        }

        #endregion

        #region Handover Report

        [Route("/Reports/HandoverReport")]
        [HttpGet]
        [AllowAnonymous]
        public ActionResult HandoverReport(Guid sourceId, bool isShift = false, bool isReceived = false, bool isHistoryPanel = false, Guid? selectedSourceId = null, bool isPreviewMail = false)
        {
            string footerUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/GlobalReports/PdfFooter";

            string footerHtml = String.Format(" --footer-html {0} " +
                                              "--enable-local-file-access" +
                                              " --enable-javascript" +
                                              " --footer-spacing 1 " +
                                              " --encoding utf-8", footerUrl);

            var model = Services.ReportService.PopulateHandoverReportViewModel(sourceId, isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft, isShift, isHistoryPanel, selectedSourceId, isPreviewMail);

            if (isShift && isPreviewMail == false)
            {
                model.ShiftType = Services.ShiftService.GetShiftType(sourceId);
            }

            if (isPreviewMail)
            {
                model.IsPreviewMail = isPreviewMail;
            }

            var pdfResult = new ViewAsPdf("~/Views/Reports/HandoverReport/PrintHandoverReport.cshtml", model)
            {
                FileName = "Rotation-Report.pdf",
                PageOrientation = Orientation.Portrait,
                MinimumFontSize = 16,
                IsGrayScale = false,
                IsJavaScriptDisabled = false,
                CustomSwitches = footerHtml,
                PageMargins = new Margins(10, 10, 10, 10)
            };

            var binary = pdfResult.BuildFile(ControllerContext);

            return File(binary, "application/pdf");
        }

        public ActionResult Test(Guid sourceId, bool isShift, bool isReceived = false, Guid? selectedSourceId = null)
        {
            var model = Services.ReportService.PopulateHandoverReportViewModel(sourceId, isReceived ? ModuleSourceType.Received : ModuleSourceType.Draft, isShift, false, selectedSourceId);

            return View("~/Views/Reports/HandoverReport/PrintHandoverReport.cshtml", model);
        }

        #endregion
    }
}
