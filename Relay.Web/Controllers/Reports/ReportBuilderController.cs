﻿using System;
using MomentumPlus.Relay.Interfaces;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Models;


namespace MomentumPlus.Relay.Web.Controllers
{
    public class ReportBuilderController : BaseController
    {
        //IOC
        public ReportBuilderController(IServicesUnitOfWork servicesUnitOfWork) : base(servicesUnitOfWork)
        { }

        public ActionResult Draft(Guid reportId, RotationType reportType)
        {
            Guid currentUserId = Guid.Parse(User.Identity.GetUserId());

            bool isAvailable = Services.AccessService.IsGlobalReportAvailable(reportId, currentUserId, reportType);

            if (isAvailable || Services.AccessService.IsReportSharedForUser(reportId, currentUserId))
            {
                ReportViewModel model = new ReportViewModel
                {
                    ReportId = reportId,
                    ReportType = reportType,
                    Type = ReportType.Draft,
                    ReportPeriod = reportType == RotationType.Swing
                                 ? Services.RotationService.GetRotationPeriod(reportId)
                                 : Services.ShiftService.GetShiftPeriod(reportId),
                    HandoverCreatorOrRecipientName = reportType == RotationType.Swing
                                 ? Services.RotationService.GetFromToBlock(reportId).RotationOwnerFullName
                                 : Services.ShiftService.GetShiftHandoverRecipientName(reportId),
                    IsReadMode = reportType == RotationType.Swing
                                ? Services.RotationService.IsRotationSwingEnd(reportId) || Services.RotationService.IsRotationExpiredOrNotStarted(reportId) || !isAvailable
                                : Services.ShiftService.IsShiftEnd(reportId) || Services.ShiftService.IsShiftWorkTimeEnd(reportId) || !isAvailable,
                    ShareReportBtnIsActive = isAvailable
                };

                return View("~/Views/Reports/ReportBuilder/_DraftReport.cshtml", model);
            }
            else
            {
                return View("~/Views/Reports/ReportBuilder/DraftUnavailable.cshtml");
            }
        }


        public ActionResult DraftForShare (Guid reportId, RotationType reportType)
        {
            Guid currentUserId = Guid.Parse(User.Identity.GetUserId());

            bool isAvailable = Services.AccessService.IsGlobalReportAvailable(reportId, currentUserId, reportType);

            if (isAvailable || Services.AccessService.IsReportSharedForUser(reportId, currentUserId))
            {
                ReportViewModel model = new ReportViewModel
                {
                    ReportId = reportId,
                    ReportType = reportType,
                    Type = ReportType.Draft,
                    ReportPeriod = reportType == RotationType.Swing
                                 ? Services.RotationService.GetRotationPeriod(reportId)
                                 : Services.ShiftService.GetShiftPeriod(reportId),
                    HandoverCreatorOrRecipientName = reportType == RotationType.Swing
                                 ? Services.RotationService.GetFromToBlock(reportId).RotationOwnerFullName
                                 : Services.ShiftService.GetShiftHandoverRecipientName(reportId),
                    IsReadMode = true,
                    ShareReportBtnIsActive = isAvailable
                };

                return View("~/Views/Reports/ReportBuilder/_DraftReport.cshtml", model);
            }
            else
            {
                return View("~/Views/Reports/ReportBuilder/DraftUnavailable.cshtml");
            }
        }






        public ActionResult Received(Guid reportId, RotationType reportType, Guid? fromSourceId)
        {
            ReportViewModel model = new ReportViewModel
            {
                ReportId = reportId,
                ReportType = reportType,
                Type = ReportType.Received,
                FromSourceId = fromSourceId,
                ReportPeriod = reportType == RotationType.Swing
                             ? Services.RotationService.GetReceivedRotationPeriod(reportId, fromSourceId)
                             : Services.ShiftService.GetReceivedShiftPeriod(reportId, fromSourceId),
                HandoverCreatorOrRecipientName = reportType == RotationType.Swing
                             ? fromSourceId.HasValue ? Services.RotationService.GetFromToBlock(fromSourceId.Value).RotationOwnerFullName
                                                     : Services.RotationService.GetFromToBlock(reportId).RotationBackToBackFullName
                             : fromSourceId.HasValue ? Services.ShiftService.GetShiftOwnerName(fromSourceId.Value)
                                                     : Services.ShiftService.GetShiftOwnerName(reportId),
                IsReadMode = reportType == RotationType.Swing
                           ? Services.RotationService.IsRotationExpiredOrNotStarted(reportId) || Services.RotationService.IsRotationSwingEnd(reportId)
                           : Services.ShiftService.IsShiftEnd(reportId) || Services.ShiftService.IsShiftWorkTimeEnd(reportId)
            };

            return View("~/Views/Reports/ReportBuilder/_ReceivedReport.cshtml", model);
        }

    }
}