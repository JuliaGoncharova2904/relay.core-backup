﻿using System;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EFCache;
using JavaScriptEngineSwitcher.Core;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Web.Extensions;
using NLog;

namespace MomentumPlus.Relay.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly Logger MvcApplicationLogger = LogManager.GetLogger("Application.Error");

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            EntityFrameworkCache.Initialize(new InMemoryCache());
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            JsEngineSwitcherConfig.Configure(JsEngineSwitcher.Instance);
        }

        private void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            if (ex is HttpAntiForgeryException)
            {
                Response.Clear();

                Response.Redirect("/");
            }

            HttpException httpException = ex as HttpException;

            if (httpException == null)
            {
                Response.Redirect("/");
            }
            else 
            {
                switch (httpException.GetHttpCode())
                {
                    case 404:
                        Response.Redirect("/");
                        break;
                    case 500:
                        Response.Redirect("/");
                        break;
                    default:
                        Response.Redirect("/");
                        break;
                }
            }

            MvcApplicationLogger.Error(ex, "Application Error.");
        }
    }
}
