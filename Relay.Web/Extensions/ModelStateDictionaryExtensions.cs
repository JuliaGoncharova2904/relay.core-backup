﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Extensions
{
    public static class ModelStateDictionaryExtensions
    {
        public static string GetValidErrorFromState(this ModelStateDictionary modelState)
        {
            IEnumerable<string> errorMessages = modelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage);

            return errorMessages.Aggregate("", (current, message) => current + ("<span style='word-wrap: break-word;'>" + message + "</span><br/>"));
        }

    }
}