﻿using MomentumPlus.Relay.Exceptions;
using System;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class PaymentRequiredFilterAttribute : FilterAttribute, IExceptionFilter
    {
        public string RedirectTemplate { get; set; }

        public void OnException(ExceptionContext filterContext)
        {
            if(filterContext.Exception is PaymentRequiredException)
            {
                Controller controller = filterContext.Controller as Controller;

                controller.HttpContext.Response.RedirectPermanent(string.Format(RedirectTemplate, filterContext.Exception.Message));

                filterContext.ExceptionHandled = true;
            }
        }
    }
}