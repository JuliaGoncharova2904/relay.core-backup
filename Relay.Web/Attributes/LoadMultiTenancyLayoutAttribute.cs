﻿using System.Web.Mvc;
using MomentumPlus.Relay.Helpers;

namespace iHandover.Relay.Web.Attributes
{
    public class LoadMultiTenancyLayoutAttribute : ActionFilterAttribute
    {
        private readonly bool isMultiTenancyTheme;
        public LoadMultiTenancyLayoutAttribute()
        {
            isMultiTenancyTheme = AppSession.Current.IsMultiTenant;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
            var result = filterContext.Result as ViewResult;
            if (result != null)
            {
                result.ViewBag.isMultiTenancyTheme = isMultiTenancyTheme;
            }
        }
    }
}