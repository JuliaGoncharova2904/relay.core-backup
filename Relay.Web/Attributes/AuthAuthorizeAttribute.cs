﻿using System.Collections.Generic;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Attributes
{
    public class AuthAuthorizeAttribute : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var httpContext = filterContext.HttpContext;
            var request = httpContext.Request;
            var response = httpContext.Response;
            var user = httpContext.User;

            if (request.IsAjaxRequest())
            {
                if (user.Identity.IsAuthenticated == false)
                {
                    response.StatusCode = (int)HttpStatusCode.Unauthorized;


                    var alert = new AlertViewModel
                    {
                        Type = AlertType.Warning,
                        Message = "Your session has expired. Please log in again."
                    };

                    List<AlertViewModel> alerts = filterContext.Controller.TempData.ContainsKey("Alerts")
                        ? (List<AlertViewModel>)filterContext.Controller.TempData["Alerts"]
                        : new List<AlertViewModel>();

                    alerts.Insert(0, alert);

                    filterContext.Controller.TempData.Add("Alerts", alerts);

                }
                else
                {
                    if (!response.IsRequestBeingRedirected)
                    {
                        response.StatusCode = (int)HttpStatusCode.Forbidden;

                    }
                }

                response.SuppressFormsAuthenticationRedirect = true;
                response.End();
            }

            base.HandleUnauthorizedRequest(filterContext);


            //string area = "";// leave empty if not using area's
            //string controller = "Account";
            //string action = "Login";
            //var urlHelper = new UrlHelper(filterContext.RequestContext);
            //if (filterContext.HttpContext.Request.IsAjaxRequest())
            //{ // Check if Ajax
            //    if (area == string.Empty)
            //        filterContext.HttpContext.Response.Write($"<script>window.location.reload('{urlHelper.Content(System.IO.Path.Combine(controller, action))}');</script>");
            //    else
            //        filterContext.HttpContext.Response.Write($"<script>window.location.reload('{urlHelper.Content(System.IO.Path.Combine(area, controller, action))}');</script>");
            //}
            //else   // Non Ajax Request                      
            //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { area, controller, action }));




            //base.HandleUnauthorizedRequest(filterContext);
        }
    }
}