﻿using System;
using System.Collections.Generic;
using MvcSiteMapProvider;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MomentumPlus.Relay.Interfaces;
using System.Web.Routing;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Helpers
{
    public class RelaySiteMapNodeVisibilityProvider : FilteredSiteMapNodeVisibilityProvider
    {

        public override bool IsVisible(ISiteMapNode node, IDictionary<string, object> sourceMetadata)
        {
            bool result = base.IsVisible(node, sourceMetadata);

            var services = DependencyResolver.Current.GetService<IServicesUnitOfWork>();

            IPrincipal user = null;

            var httpContext = HttpContext.Current;
            if (httpContext != null)
                user = httpContext.User;

            if (result)
            {
                ProjectAdminSectionRules(ref result, node, user, services);


                string safetySectionVersion = WebConfigurationManager.AppSettings["safetySectionVersion"];

                SafetySectionRules(ref result, node, user, services, safetySectionVersion);
                SafetyV2SectionRules(ref result, node, user, services, safetySectionVersion);

                NotificationRules(ref result, node);

                ReportsRules(ref result, node, user, services);

                TaskBoardRules(ref result, node, user);

                ReportBuilderRules(ref result, node, user);

                ContributorRules(ref result, node, user, services);

                TopicSearchRules(ref result, node, user, services);

                GetAccessToPageUpgradeRules(ref result, node, user, services);

                TopicSearchResultsRules(ref result, node);

                SafetyCCMHideRules(ref result, node, user, services);

            }

            return result;
        }

        private void TopicSearchResultsRules(ref bool result, ISiteMapNode node)
        {
            if (node.Controller == "SearchResults")
            {
                result = true;
            }
        }

        private void SafetyCCMHideRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services)
        {
            if (node.Route == "SafetyV2")
            {
                var subscriptionTypeLevel = services.AuthService.GetSubscriptionType(Guid.Parse(user.Identity.GetUserId()));

                if (subscriptionTypeLevel == Authorization.Domain.Entities.RestrictionType.Ultimate)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
        }

        private void TopicSearchRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services)
        {
            if (node.ParentNode != null && !string.IsNullOrEmpty(node.ParentNode.Key) && node.ParentNode.Key == "Reports")
            {
                RouteValueDictionary routeValueDictionary = ((MvcHandler)HttpContext.Current.Handler).RequestContext.RouteData.Values;

                string controllerName = routeValueDictionary["controller"].ToString();

                if (controllerName == "GlobalReports")
                {
                    if (!node.IsInCurrentPath() && node.Controller == "TopicSearch")
                    {
                        var subscriptionTypeLevel = services.AuthService.GetSubscriptionType(Guid.Parse(user.Identity.GetUserId()));

                        if (subscriptionTypeLevel == Authorization.Domain.Entities.RestrictionType.Basic)
                        {
                            result = false;
                        }
                        else
                        {
                            result = true;
                        }
                    }
                }
            }
        }

        private void ProjectAdminSectionRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services)
        {
            if (node.Route == "ProjectAdmin" && user != null)
            {
                if (!user.IsInRole(iHandoverRoles.Relay.Administrator)
                    && !user.IsInRole(iHandoverRoles.Relay.iHandoverAdmin)
                    && !user.IsInRole(iHandoverRoles.Relay.ExecutiveUser)
                    && !user.IsInRole(iHandoverRoles.Relay.LineManager)
                    //&& !user.IsInRole(iHandoverRoles.Relay.SafetyManager)
                    && !user.IsInRole(iHandoverRoles.Relay.HeadLineManager))
                {
                    result = services.AccessService.UserIsProjectManager(Guid.Parse(user.Identity.GetUserId()));
                }
            }
        }

        private void GetAccessToPageUpgradeRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services)
        {
            if (node.Route == "Upgrade" && user != null)
            {
                result = services.AuthService.GetAccessToPageUpgrade(Guid.Parse(user.Identity.GetUserId()));
            }
        }


        private void SafetySectionRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services, string safetySectionVersion)
        {

            if (node.Route == "SafetyV1" && user != null)
            {
                if (safetySectionVersion == "V1" || safetySectionVersion == "ALL")
                {
                }
                else
                {
                    result = false;
                }
            }
        }

        private void SafetyV2SectionRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services, string safetySectionVersion)
        {
            if (node.Controller == "SafetyArchiveV2")
            {
                result = services.AccessService.SafetyMessageArchiveV2IsEnable();
            }

            if (node.Route == "SafetyV2" && user != null)
            {
                if (safetySectionVersion == "V2" || safetySectionVersion == "ALL")
                {

                }
                else
                {
                    result = false;
                }
            }
        }

        private void ReportsRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services)
        {
            if (node.ParentNode != null && !string.IsNullOrEmpty(node.ParentNode.Key) && node.ParentNode.Key == "Reports")
            {
                RouteValueDictionary routeValueDictionary = ((MvcHandler)HttpContext.Current.Handler).RequestContext.RouteData.Values;

                string controllerName = routeValueDictionary["controller"].ToString();

                if (node.IsInCurrentPath())
                {
                    if (node.Controller != "ReportBuilder" && controllerName == "ReportBuilder")
                    {
                        result = false;
                    }
                }
                else
                {
                    if (node.Controller == "ReportBuilder" || controllerName == "ReportBuilder")
                    {
                        result = false;
                    }
                }
            }

            if (node.Controller == "GlobalReports" && (node.Action == "HistoryReport" || node.Action == "ReceivedReport" || node.Action == "Shared"))
            {
                if (services.AccessService.IsGlobalReportHistoryAndReceivedDisable(Guid.Parse(HttpContext.Current.User.Identity.GetUserId())))
                {
                    result = false;
                }
            }
        }

        private void NotificationRules(ref bool result, ISiteMapNode node)
        {
            if (node.Controller == "Notification")
            {
                result = true;
            }
        }

        private void TaskBoardRules(ref bool result, ISiteMapNode node, IPrincipal user)
        {
            if (node.Controller == "TaskBoard" && Relay.Helpers.AppSession.Current.IsMultiTenant && !node.RouteValues.Keys.Contains("userId") && user.IsInRole(iHandoverRoles.Relay.iHandoverAdmin))
            {
                result = false;
            }

            if (node.Action == "MyTasksSection")
            {
                result = !node.RouteValues.Keys.Contains("userId");
            }
        }

        private void ReportBuilderRules(ref bool result, ISiteMapNode node, IPrincipal user)
        {
            if (node.Controller == "Rotation" && Relay.Helpers.AppSession.Current.IsMultiTenant && !node.RouteValues.Keys.Contains("userId") && user.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) ||
                node.Controller == "Rotation" && Relay.Helpers.AppSession.Current.IsMultiTenant && !node.RouteValues.Keys.Contains("userId") && user.IsInRole(iHandoverRoles.Relay.ExecutiveUser))
            {
                result = false;
            }
        }

        private void ContributorRules(ref bool result, ISiteMapNode node, IPrincipal user, IServicesUnitOfWork services)
        {
            if (node.Route == "ContributedUsers" && user != null)
            {
                if (!services.ContributorsService.IsHaveContributedUser(Guid.Parse(user.Identity.GetUserId())))
                {
                    result = false;
                }
            }
        }
    }
}
