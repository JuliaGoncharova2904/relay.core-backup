﻿using System.Web;

namespace MomentumPlus.Relay.Web.Helpers
{
    public class AppSession
    {
        #region Return URLs

        private string defaultUrl = "/";

        private string _exitRotationViewModeBackUrl;
        public string ExitRotationViewModeUrl
        {
            get { return string.IsNullOrEmpty(_exitRotationViewModeBackUrl) ? defaultUrl : _exitRotationViewModeBackUrl; }
            set { _exitRotationViewModeBackUrl = value; }
        }

        #endregion

        /// <summary>
        /// Get the current session.
        /// </summary>
        public static AppSession Current
        {
            get
            {
                AppSession session = (AppSession)HttpContext.Current.Session["RelaySession"];
                if (session == null)
                {
                    session = new AppSession();
                    HttpContext.Current.Session["RelaySession"] = session;
                }
                return session;
            }
        }
    }
}