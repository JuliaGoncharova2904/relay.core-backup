﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using ImageProcessor.Web.Services;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;
using ImageProcessor;
using ImageProcessor.Imaging;
using MomentumPlus.Relay.CompositionRoot;
using MomentumPlus.Relay.CompositionRoot.IOC;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Roles;

namespace MomentumPlus.Relay.Web.Helpers
{
    public class ImageprocessorProviderEmployee : IImageService
    {
        /// <summary>
        /// Gets or sets the prefix for the given implementation.
        /// <remarks>
        /// This value is used as a prefix for any image requests that should use this service.
        /// </remarks>
        /// </summary>
        public string Prefix { get; set; } = "useravatar";

        /// <summary>
        /// Gets a value indicating whether the image service requests files from
        /// the locally based file system.
        /// </summary>
        public bool IsFileLocalService => false;

        /// <summary>
        /// Gets or sets any additional settings required by the service.
        /// </summary>
        public Dictionary<string, string> Settings { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the white list of <see cref="System.Uri"/>. 
        /// </summary>
        public Uri[] WhiteList { get; set; }

        /// <summary>
        /// Gets a value indicating whether the current request passes sanitizing rules.
        /// </summary>
        /// <param name="path">
        /// The image path.
        /// </param>
        /// <returns>
        /// <c>True</c> if the request is valid; otherwise, <c>False</c>.
        /// </returns>
        public bool IsValidRequest(string path)
        {
            return true;
        }

        /// <summary>
        /// Gets the image using the given identifier.
        /// </summary>
        /// <param name="id">
        /// The value identifying the image to fetch.
        /// </param>
        /// <returns>
        /// The <see cref="byte"/> array containing the image data.
        /// </returns>
        public Task<byte[]> GetImage(object id)
        {

            bool isAuthenticated = HttpContext.Current.User.Identity.IsAuthenticated;

            if (isAuthenticated)
            {
                var services = DependencyResolver.Current.GetService<IServicesUnitOfWork>();

                var userId = Guid.Parse(id.ToString().ToUpper());

                var user = services.UserService.GetEmployeeById(userId, userId);

                if (user != null)
                {
                    if (user.ImageId.HasValue)
                    {
                        var imageId = user.ImageId.Value;

                        FileViewModel img = services.MediaService.GetImage(imageId);

                        return Task.Run(() => img.BinaryData);
                    }
                    else
                    {
                        MemoryStream stream = new MemoryStream();

                        int width = 256;
                        int height = 256;
                        int bufferSize = width * height * 4;
                        byte[] imageBuffer = new byte[bufferSize];

                        for (int i = 0; i < bufferSize; ++i)
                        {
                            if ((i + 1) % 4 == 0)
                                imageBuffer[i] = 255;
                            else
                                imageBuffer[i] = 60;
                        }

                        IntPtr unmanagedPointer = Marshal.AllocHGlobal(imageBuffer.Length);
                        Marshal.Copy(imageBuffer, 0, unmanagedPointer, imageBuffer.Length);

                        using (Bitmap ima = new Bitmap(width, height, 256 * 4, PixelFormat.Format32bppRgb, unmanagedPointer))
                        {
                            ima.Save(stream, ImageFormat.Png);
                        }

                        Marshal.FreeHGlobal(unmanagedPointer);

                        WebImage img = new WebImage(stream);

                        StringBuilder str = new StringBuilder();

                        if (!string.IsNullOrEmpty(user.Name))
                            str.Append(user.Name[0]);

                        if (!string.IsNullOrEmpty(user.Surname))
                            str.Append(user.Surname[0]);

                        img.AddTextWatermark(str.ToString().ToUpper(), fontColor: "White", fontSize: 80, horizontalAlign: "Center", verticalAlign: "Middle");

                        return Task.Run(() => img.GetBytes());
                    }
                }      
            }
            return null;
        }
    }
}
