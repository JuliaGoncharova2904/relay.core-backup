﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using ImageProcessor.Web.Services;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.Helpers
{
    public class ImageprocessorProvider : IImageService
    {
        /// <summary>
        /// Gets or sets the prefix for the given implementation.
        /// <remarks>
        /// This value is used as a prefix for any image requests that should use this service.
        /// </remarks>
        /// </summary>
        public string Prefix { get; set; } = "images";

        /// <summary>
        /// Gets a value indicating whether the image service requests files from
        /// the locally based file system.
        /// </summary>
        public bool IsFileLocalService => false;

        /// <summary>
        /// Gets or sets any additional settings required by the service.
        /// </summary>
        public Dictionary<string, string> Settings { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the white list of <see cref="System.Uri"/>. 
        /// </summary>
        public Uri[] WhiteList { get; set; }

        /// <summary>
        /// Gets a value indicating whether the current request passes sanitizing rules.
        /// </summary>
        /// <param name="path">
        /// The image path.
        /// </param>
        /// <returns>
        /// <c>True</c> if the request is valid; otherwise, <c>False</c>.
        /// </returns>
        public bool IsValidRequest(string path)
        {
            return true;
        }

        /// <summary>
        /// Gets the image using the given identifier.
        /// </summary>
        /// <param name="id">
        /// The value identifying the image to fetch.
        /// </param>
        /// <returns>
        /// The <see cref="byte"/> array containing the image data.
        /// </returns>
        public Task<byte[]> GetImage(object id)
        {
            var services = DependencyResolver.Current.GetService<IServicesUnitOfWork>();

            var imageId = Guid.Parse(id.ToString().ToUpper());

            FileViewModel img = services.MediaService.GetImage(imageId);

            return Task.Run(() => img.BinaryData);

        }

    }
}
