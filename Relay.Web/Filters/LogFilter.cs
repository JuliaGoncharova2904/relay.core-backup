﻿using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json;
using NLog;

public class LogFilter : ActionFilterAttribute
{
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        if (filterContext.HttpContext.Request.HttpMethod != "POST")
        {
            return;
        }

        string controller = filterContext.RouteData.Values["controller"].ToString();

        if (controller == "Account")
        {
            return;
        }


        var logger = LogManager.GetLogger("filter-data-logger");

        var form = filterContext.HttpContext.Request.Form;

        var dictionary = form.AllKeys.ToDictionary(k => k, k => form[k]);

        var jsonPostedData = JsonConvert.SerializeObject(dictionary);

        logger.Debug(jsonPostedData);

        base.OnActionExecuting(filterContext);
    }
}