﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MomentumPlus.Relay.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("favicon.ico");
            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });
            routes.IgnoreRoute("{*staticfile}", new { staticfile = @".*\.(css|js|gif|jpg)(/.*)?" });
            routes.IgnoreRoute("{*robotstxt}", new { robotstxt = @"(.*/)?robots.txt(/.*)?" });
            routes.IgnoreRoute("{*allphp}", new { allphp = @".*\.php(/.*)?" });
            routes.IgnoreRoute("{*allcgi}", new { allcgi = @".*\.cgi(/.*)?" });

            ///////////////////////////////////////////////////////////////////////////////////////////////////


            //routes.MapRoute(
            //    name: "The resource cannot be found.",
            //    url: "Auth/Home",
            //    defaults: new { controller = "Home", action = "Index" });

            routes.MapRoute(
            "Error", "Auth/Home",
            new { controller = "Home", action = "Index" });

            #region Framework Section

            #region Master Lists Section

            routes.MapRoute(
                name: "Framework-Master-Lists-Companies",
                url: "Framework/MasterLists/Companies",
                defaults: new { controller = "Companies", action = "Index" });


            routes.MapRoute(
                name: "Framework-Master-Lists-Rotation-Patterns",
                url: "Framework/MasterLists/RotationPatterns",
                defaults: new { controller = "RotationPatterns", action = "Index" });

            #endregion Master Lists Section

            #region Modules Section

            routes.MapRoute(
                name: "Framework-Modules-HSE",
                url: "Framework/Modules/HSE",
                defaults: new { controller = "HSEModule", action = "Index" });

            routes.MapRoute(
                name: "Framework-Modules-Core",
                url: "Framework/Modules/Core",
                defaults: new { controller = "CoreModule", action = "Index" });

            routes.MapRoute(
                name: "Framework-Modules-Team",
                url: "Framework/Modules/Team",
                defaults: new { controller = "TeamModule", action = "Index" });

            routes.MapRoute(
                name: "Framework-Modules-Project",
                url: "Framework/Modules/Project",
                defaults: new { controller = "ProjectModule", action = "Index" });

            #endregion Modules Section

            routes.MapRoute(
                name: "Framework-Templates",
                url: "Framework/Templates/",
                defaults: new { controller = "Templates", action = "Index" });


            #endregion Framework Section


            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Rotation Manager Section

            routes.MapRoute(
                name: "Rotation-Manager-Rotations",
                url: "RotationManager/Rotations/",
                defaults: new { controller = "Rotations", action = "Index" });

            routes.MapRoute(
                name: "Rotation-Manager-Employees",
                url: "RotationManager/Employees/",
                defaults: new { controller = "Employees", action = "Index" });

            #endregion Rotation Manager Section

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Rotation Section

            routes.MapRoute(
                name: "Rotation-Summary",
                url: "Rotation/{userId}/Summary/",
                defaults: new { controller = "Summary", userId = "", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "Rotation-Checklist",
                url: "Rotation/{userId}/Checklist/",
                defaults: new { controller = "Checklist", userId = "", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "Rotation-Dashboard",
                url: "Rotation/{userId}/Dashboard/",
                defaults: new { controller = "Dashboard", userId = "", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "Rotation-TaskBoard",
                url: "Rotation/{userId}/TaskBoard/{action}",
                defaults: new { controller = "TaskBoard", userId = "", action = "*", id = UrlParameter.Optional });

            #endregion Rotation Section

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Team Rotations

            routes.MapRoute(
                name: "MyTeamRotations",
                url: "MyTeam/{action}/{userId}",
                defaults: new { controller = "TeamRotations", userId = UrlParameter.Optional, action = "Index", id = UrlParameter.Optional });

            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Contributed Users

            routes.MapRoute(
                name: "ContributedUsers",
                url: "ContributedUsers/{action}/{userId}",
                defaults: new { controller = "ContributedUsers", userId = UrlParameter.Optional, action = "Index", id = UrlParameter.Optional });

            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Safety

            routes.MapRoute(
                name: "SafetyV1",
                url: "Safety/Administration",
                defaults: new { controller = "MajorHazard", action = "Index" });

            routes.MapRoute(
                name: "ScheduleMessage",
                url: "Safety/Scheduler",
                defaults: new { controller = "ScheduleMessage", action = "Index" });

            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Safety V2

            routes.MapRoute(
                name: "SafetyV2",
                url: "Safety/v2/MajorHazards",
                defaults: new { controller = "SafetyMajorHazardsV2", action = "Index" });

            routes.MapRoute(
                name: "Safety-Scheduler-V2",
                url: "Safety/v2/Scheduler",
                defaults: new { controller = "SafetyShedulerV2", action = "Index" });

            routes.MapRoute(
                name: "Safety-Archive-V2",
                url: "Safety/v2/Archive",
                defaults: new { controller = "SafetyArchiveV2", action = "Index" });

            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Reports

            #region Global Reports Section

            routes.MapRoute(
                name: "Global-Reports",
                url: "Reports/GlobalReports",
                defaults: new { controller = "GlobalReports", action = "Index" });

            routes.MapRoute(
                name: "Global-Reports-History-Report",
                url: "Reports/GlobalReports/History",
                defaults: new { controller = "GlobalReports", action = "HistoryReport" });

            routes.MapRoute(
                name: "Global-Reports-Received-Report",
                url: "Reports/GlobalReports/Received",
                defaults: new { controller = "GlobalReports", action = "ReceivedReport" });

            routes.MapRoute(
                name: "Global-Reports-Shared-Report",
                url: "Reports/GlobalReports/Shared",
                defaults: new { controller = "GlobalReports", action = "SharedReport" });

            routes.MapRoute(
                         name: "Global-Reports-Shared-Topics",
                         url: "Reports/GlobalReports/SharedTopics",
                         defaults: new { controller = "GlobalReports", action = "SharedTopics" });

            routes.MapRoute(
                name: "Global-Reports-Team-Report",
                url: "Reports/GlobalReports/Team",
                defaults: new { controller = "GlobalReports", action = "TeamReport" });

            routes.MapRoute(
                name: "Global-Reports-Report-Draft",
                url: "Reports/GlobalReports/Report/{reportId}/Draft",
                defaults: new { controller = "ReportBuilder", reportId = "", action = "Draft" });

            routes.MapRoute(
                name: "Global-Reports-Share-Report-Draft",
                url: "Reports/GlobalReports/Report/{reportId}/DraftForShare",
                defaults: new { controller = "ReportBuilder", reportId = "", action = "DraftForShare" });

            routes.MapRoute(
                name: "Global-Reports-Report-Received",
                url: "Reports/GlobalReports/Report/{reportId}/Received",
                defaults: new { controller = "ReportBuilder", reportId = "", action = "Received", fromSourceId = UrlParameter.Optional });

            routes.MapRoute(
                name: "Global-Reports-QME-Report",
                url: "Reports/QMEReport",
                defaults: new { controller = "GlobalReports", action = "ReportPrint" });

            routes.MapRoute(
                name: "Global-Reports-Handover-Report",
                url: "Reports/HandoverReport",
                defaults: new { controller = "GlobalReports", action = "HandoverReport" });


            routes.MapRoute(
              name: "Search-Results",
              url: "SearchResults",
              defaults: new { controller = "SearchResults",  action = "Index" });

            routes.MapRoute(
             name: "Search-Results-Reports",
             url: "SearchResults/SearchResultsReportPdf",
             defaults: new { controller = "SearchResults", action = "SearchResultsReportPdf" });

            #endregion Global Reports Section


            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Project Admin

            routes.MapRoute(
                name: "ProjectAdmin",
                url: "ProjectAdmin",
                defaults: new { controller = "ProjectAdmin", action = "Index" });


            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Notification

            routes.MapRoute(
                name: "Notification",
                url: "Notification",
                defaults: new { controller = "Notification", action = "Index" });


            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Scheduler

            routes.MapRoute(
                name: "Scheduler",
                url: "Scheduler",
                defaults: new { controller = "Scheduler", action = "Index" });


            #endregion

            ///////////////////////////////////////////////////////////////////////////////////////////////////

            #region Upgrade

            routes.MapRoute(
                name: "Upgrade",
                url: "Upgrade",
                defaults: new { controller = "Upgrade", action = "Index" });


            #endregion


            ///////////////////////////////////////////////////////////////////////////////////////////////////

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
