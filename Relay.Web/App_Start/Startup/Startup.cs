﻿using iHandover.Relay.Jobber;
using Microsoft.Owin;
using MomentumPlus.Relay.Api;
using MomentumPlus.Relay.Interfaces.Auth;
using Owin;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(MomentumPlus.Relay.Web.Startup))]
namespace MomentumPlus.Relay.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            DependencyResolver.Current.GetService<IAuthBridge>().RegisterAuthModule(app);

            JobsConfiguration.Configure(app);

            SignalRConfiguration.Configure(app);

            ApiConfiguration.Configure(app);
        }
    }
}