﻿using System.Web.Mvc;
using MomentumPlus.Relay.Web.Attributes;

namespace MomentumPlus.Relay.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());

            // By default, all actions require a logged in user
            filters.Add(new AuthAuthorizeAttribute());
        }
    }
}
