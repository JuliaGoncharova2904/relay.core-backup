﻿using JavaScriptEngineSwitcher.Core;
using JavaScriptEngineSwitcher.Msie;

namespace MomentumPlus.Relay.Web
{
    public class JsEngineSwitcherConfig
    {
        public static void Configure(JsEngineSwitcher engineSwitcher)
        {
            engineSwitcher.EngineFactories
                .AddMsie(new MsieSettings
                {
                    UseEcmaScript5Polyfill = true,
                    UseJson2Library = true,
                    EnableDebugging = true
                });

            engineSwitcher.DefaultEngineName = "MsieJsEngine";

        }
    }
}