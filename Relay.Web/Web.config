﻿<?xml version="1.0"?>
<configuration>
  <configSections>
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=6.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false"/>
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog"/>
    <sectionGroup name="bundleTransformer">
      <section name="core" type="BundleTransformer.Core.Configuration.CoreSettings, BundleTransformer.Core"/>
      <section name="less" type="BundleTransformer.Less.Configuration.LessSettings, BundleTransformer.Less"/>
    </sectionGroup>
    <sectionGroup name="imageProcessor">
      <section name="security" requirePermission="false" type="ImageProcessor.Web.Configuration.ImageSecuritySection, ImageProcessor.Web"/>
      <section name="processing" requirePermission="false" type="ImageProcessor.Web.Configuration.ImageProcessingSection, ImageProcessor.Web"/>
      <section name="caching" requirePermission="false" type="ImageProcessor.Web.Configuration.ImageCacheSection, ImageProcessor.Web"/>
    </sectionGroup>
  </configSections>
  <!-- Database connection -->
  <system.diagnostics>
    <trace>
      <listeners>
        <add type="Microsoft.WindowsAzure.Diagnostics.DiagnosticMonitorTraceListener, Microsoft.WindowsAzure.Diagnostics, Version=2.8.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" name="AzureDiagnostics">
          <filter type=""/>
        </add>
      </listeners>
    </trace>
  </system.diagnostics>
  <connectionStrings configSource="Configuration\Connections\MultiTenancy.Debug.config"/>
  <appSettings>
    <!--================ Owin =====================-->
    <add key="owin:appStartup" value="MomentumPlus.Relay.Web.Startup"/>
    <add key="owin:AutomaticAppStartup" value="true"/>
    <!--===========================================-->
    <!--================ App settings =============-->
    <add key="webpages:Version" value="3.0.0.0"/>
    <add key="webpages:Enabled" value="false"/>
    <add key="ClientValidationEnabled" value="true"/>
    <add key="UnobtrusiveJavaScriptEnabled" value="true"/>
    <!--===========================================-->
    <!--================ MvcSiteMap ===============-->
    <add key="MvcSiteMapProvider_DefaultSiteMapNodeVisibiltyProvider" value="MomentumPlus.Relay.Web.Helpers.RelaySiteMapNodeVisibilityProvider, MomentumPlus.Relay.Web"/>
    <add key="MvcSiteMapProvider_SecurityTrimmingEnabled" value="true"/>
    <add key="MvcSiteMapProvider_UseExternalDIContainer" value="false"/>
    <add key="MvcSiteMapProvider_ScanAssembliesForSiteMapNodes" value="true"/>
    <add key="MvcSiteMapProvider_IncludeAssembliesForScan" value="MomentumPlus.Relay.Web,MomentumPlus.Relay.CustomReports.IndodrillReport"/>
    <add key="MvcSiteMapProvider_VisibilityAffectsDescendants" value="false"/>
    <!--===========================================-->
    <!--================ Relay Settings ===========-->
    <add key="isInTestMode" value="true"/>
    <add key="isMultiTenant" value="true"/>
    <add key="buildMode" value="debug"/>
    <add key="seedDatabase" value="true"/>
    <add key="version" value="2.3.2"/>
    <!--===========================================-->
    <!--================ Safety ===================-->
    <!-- safetySectionVersion: ALL, V1, V2 -->
    <add key="safetySectionVersion" value="ALL"/>
    <!--===========================================-->
    <!--================ Web API Config ===========-->
    <add key="siteUrl" value="http://relay.local"/>
    <add key="audience" value="Relay"/>
    <add key="secret" value="IxrAjDtt2FqElO7IhrSrUJELhUckePEPVpaePlS_Xaw"/>
    <!--===========================================-->
    <!--================ Stripe ===================-->
    <add key="stripeKey" value="sk_test_LFwzGhG9T8dMikhMMjaQ1urD"/>
    <!--===========================================-->
  </appSettings>
  <system.web>
    <compilation debug="true" targetFramework="4.5"/>
    <httpRuntime maxRequestLength="20000" enableVersionHeader="false" targetFramework="4.5" fcnMode="Single"/>
    <pages>
      <namespaces>
        <add namespace="MvcSiteMapProvider.Web.Html"/>
        <add namespace="MvcSiteMapProvider.Web.Html.Models"/>
        <add namespace="MvcPaging"/>
      </namespaces>
    </pages>
    <globalization culture="en-GB" uiCulture="en-GB"/>
    <customErrors mode="Off"/>
    <sessionState timeout="30"/>
    <httpModules>
      <add name="ImageProcessorModule" type="ImageProcessor.Web.HttpModules.ImageProcessingModule, ImageProcessor.Web"/>
    </httpModules>
  </system.web>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-11.0.0.0" newVersion="11.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-1.6.5135.21930" newVersion="1.6.5135.21930"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System" publicKeyToken="b77a5c561934e089" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.Services.Client" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.8.3.0" newVersion="5.8.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.OData" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.8.3.0" newVersion="5.8.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Data.Edm" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.8.3.0" newVersion="5.8.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.1.0.0" newVersion="3.1.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="RazorEngine" publicKeyToken="9ee697374c7e744a" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.10.0.0" newVersion="3.10.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Azure.KeyVault.Core" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="2.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="JavaScriptEngineSwitcher.Core" publicKeyToken="c608b2a8cc9e4472" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.4.10.0" newVersion="2.4.10.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="MsieJavaScriptEngine" publicKeyToken="a3a2846a37ac0d3e" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-2.2.4.0" newVersion="2.2.4.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.1.0.0" newVersion="3.1.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="BundleTransformer.Core" publicKeyToken="973c344c93aac60d" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-1.9.171.0" newVersion="1.9.171.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security.OAuth" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.1.0.0" newVersion="3.1.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Owin.Security.Cookies" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-3.1.0.0" newVersion="3.1.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Cors" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.IdentityModel.Logging" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-1.1.4.0" newVersion="1.1.4.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.IdentityModel.Tokens.Jwt" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0"/>
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" culture="neutral"/>
        <bindingRedirect oldVersion="0.0.0.0-5.2.3.0" newVersion="5.2.3.0"/>
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.SqlConnectionFactory, EntityFramework"/>
    <providers>
      <provider invariantName="System.Data.SqlClient" type="System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer"/>
    </providers>
  </entityFramework>
  <system.webServer>
    <httpErrors existingResponse="PassThrough"/>
    <modules runAllManagedModulesForAllRequests="true">
      <remove name="UrlRoutingModule-4.0"/>
      <remove name="FormsAuthentication"/>
      <add name="UrlRoutingModule-4.0" type="System.Web.Routing.UrlRoutingModule"/>
      <add name="NLog" type="NLog.Web.NLogHttpModule, NLog.Web"/>
      <add name="ImageProcessorModule" type="ImageProcessor.Web.HttpModules.ImageProcessingModule, ImageProcessor.Web"/>
    </modules>
    <staticContent>
      <remove fileExtension=".woff"/>
      <remove fileExtension=".woff2"/>
      <mimeMap fileExtension=".woff" mimeType="application/x-font-woff"/>
      <mimeMap fileExtension=".woff2" mimeType="application/x-font-woff"/>
    </staticContent>
    <security>
      <requestFiltering>
        <fileExtensions>
          <add fileExtension=".woff" allowed="true"/>
          <add fileExtension=".ttf" allowed="true"/>
          <add fileExtension=".woff2" allowed="true"/>
        </fileExtensions>
      </requestFiltering>
    </security>
    <rewrite>
      <rules>
        <rule name="http to https" stopProcessing="true" enabled="false">
          <match url="(.*)"/>
          <conditions>
            <add input="{HTTPS}" pattern="^OFF$"/>
          </conditions>
          <action type="Redirect" url="https://{HTTP_HOST}/{R:1}" redirectType="SeeOther"/>
        </rule>
      </rules>
    </rewrite>
    <directoryBrowse enabled="false"/>
    <handlers>
      <remove name="ExtensionlessUrlHandler-Integrated-4.0"/>
      <remove name="OPTIONSVerbHandler"/>
      <remove name="OPTIONSVerbHandler"/>
      <remove name="TRACEVerbHandler"/>
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="*" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0"/>
      <add name="LessAssetHandler" path="*.less" verb="GET" type="BundleTransformer.Less.HttpHandlers.LessAssetHandler, BundleTransformer.Less" resourceType="File" preCondition=""/>
    </handlers>
    <validation validateIntegratedModeConfiguration="false"/>
  </system.webServer>
  <system.net>
    <mailSettings>
      <smtp from="noreply@ihandover.co">
        <network enableSsl="true" host="smtp.gmail.com" port="587" userName="noreply@ihandover.co" password="Ball00n$%"/>
      </smtp>
    </mailSettings>
  </system.net>
  <bundleTransformer xmlns="http://tempuri.org/BundleTransformer.Configuration.xsd">
    <less useNativeMinification="true" ieCompat="true" strictMath="false" strictUnits="false" dumpLineNumbers="None" javascriptEnabled="true" globalVariables="" modifyVariables="">
      <jsEngine name="MsieJsEngine"/>
    </less>
    <core>
      <css>
        <translators>
          <add name="NullTranslator" type="BundleTransformer.Core.Translators.NullTranslator, BundleTransformer.Core" enabled="false"/>
          <add name="LessTranslator" type="BundleTransformer.Less.Translators.LessTranslator, BundleTransformer.Less"/>
        </translators>
        <fileExtensions>
          <add fileExtension=".css" assetTypeCode="Css"/>
          <add fileExtension=".less" assetTypeCode="Less"/>
        </fileExtensions>
        <postProcessors>
          <add name="UrlRewritingCssPostProcessor" type="BundleTransformer.Core.PostProcessors.UrlRewritingCssPostProcessor, BundleTransformer.Core" useInDebugMode="false"/>
        </postProcessors>
        <minifiers>
          <add name="NullMinifier" type="BundleTransformer.Core.Minifiers.NullMinifier, BundleTransformer.Core"/>
        </minifiers>
      </css>
      <js>
        <translators>
          <add name="NullTranslator" type="BundleTransformer.Core.Translators.NullTranslator, BundleTransformer.Core" enabled="false"/>
        </translators>
        <minifiers>
          <add name="NullMinifier" type="BundleTransformer.Core.Minifiers.NullMinifier, BundleTransformer.Core"/>
        </minifiers>
        <fileExtensions>
          <add fileExtension=".js" assetTypeCode="JavaScript"/>
        </fileExtensions>
      </js>
    </core>
  </bundleTransformer>
  <!-- Log config -->
  <nlog configSource="Configuration\NLog\NLog.development.config"/>
  <imageProcessor>
    <security configSource="Configuration\imageprocessor\security.config"/>
    <caching configSource="Configuration\imageprocessor\cache.config"/>
    <processing configSource="Configuration\imageprocessor\processing.config"/>
  </imageProcessor>
  <system.codedom>
    <compilers>
      <compiler language="c#;cs;csharp" extension=".cs" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.CSharpCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.8.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:default /nowarn:1659;1699;1701"/>
      <compiler language="vb;vbs;visualbasic;vbscript" extension=".vb" type="Microsoft.CodeDom.Providers.DotNetCompilerPlatform.VBCodeProvider, Microsoft.CodeDom.Providers.DotNetCompilerPlatform, Version=1.0.8.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" warningLevel="4" compilerOptions="/langversion:default /nowarn:41008 /define:_MYTYPE=\&quot;Web\&quot; /optionInfer+"/>
    </compilers>
  </system.codedom>
</configuration>