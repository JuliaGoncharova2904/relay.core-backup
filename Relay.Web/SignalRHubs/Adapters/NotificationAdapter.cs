﻿using Microsoft.AspNet.SignalR;
using MomentumPlus.Relay.Interfaces;
using System;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Web.SignalRHubs.Adapters
{
    public class NotificationAdapter : INotificationAdapter
    {
        private IHubContext context;

        public NotificationAdapter()
        {
            this.context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
        }

        public void PushNotification(Guid userId, NotificationMessageViewModel newNotification)
        {
            this.context.Clients.User(userId.ToString()).AddNewNotification(newNotification);
        }

        public void PushNotificationInfo(Guid userId, int msgNumber)
        {
            this.context.Clients.User(userId.ToString()).UpdateNotificationInfo(msgNumber);
        }
    }
}