﻿using Microsoft.AspNet.SignalR;
using System;
using Microsoft.AspNet.Identity;

namespace MomentumPlus.Relay.Web.SignalRHubs.Configuration
{
    public class SignalRUserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            Guid userId = Guid.Parse(request.User.Identity.GetUserId());

            return userId.ToString();
        }
    }
}