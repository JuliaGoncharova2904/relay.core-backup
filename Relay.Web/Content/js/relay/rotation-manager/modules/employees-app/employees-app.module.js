﻿angular.module('EmployeesApp', ['employeesMenu'])

    .component("employeesApp", {

        template: '<div class ="row column-row">' +
                    '<div class ="col-md-1"></div>' +
                    '<div class ="col-md-4">' +
                        '<teams-panel>' +
                            'Loading...' +
                        '</teams-panel>' +
                    '</div>' +
                    '<div class ="col-md-1"></div>' +
                    '<div class ="col-md-4">' +
                        '<employees-panel>' +
                            'Loading...' +
                        '</employees-panel>' +
                    '</div>' +
                    '<div class ="col-md-1"></div>' +
                '</div>'
    });


angular.module('employeesMenu', []);