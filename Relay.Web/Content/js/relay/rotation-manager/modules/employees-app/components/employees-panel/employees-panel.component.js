﻿
; (function () {
    "use strict";

    angular
      .module("employeesMenu")

      .component("employeesPanel", {
          templateUrl: "/Content/js/relay/rotation-manager/modules/employees-app/components/employees-panel/employees-panel.template.html",
          controller: EmployeesPanelController
      });

    EmployeesPanelController.$inject = ["$scope", "$http", "$location"];
    function EmployeesPanelController($scope, $http, $location) {

        var ctrl = this;
        ctrl.scope = $scope;
        ctrl.http = $http;
        ctrl.location = $location;
        ctrl.currentPage = $location.search().employeePage;
        ctrl.currentTeamId = $location.search().teamId;

        ctrl.vm = {
            page: null,
            errorMsg: null
        };

        if (ctrl.currentPage == undefined)
            $location.search('employeePage', 1);

        ctrl.showPrevPage = function () {
            if (!ctrl.vm.page.IsFirstPage) {
                var params = ctrl.location.search();
                $location.search('employeePage', +params.employeePage - 1);
            }
                
        };

        ctrl.showNextpage = function () {
            if (!ctrl.vm.page.IsLastPage) {
                var params = ctrl.location.search();
                $location.search('employeePage', +params.employeePage + 1);
            }

        }

        $scope.$watch(function () { return $location.search() }, function () {
            update(ctrl);
        }, true);
        
        //--- Open modal dialog for employee creating --
        ctrl.createEmployeeDialog = function () {

            var boardManagerStep1Dialog = Relay.BoardManagerDialog({
                url: "/BoardManager/StepOneDialog",
                method: "GET",
                acceptFiles: true,
                dialogId: "stepOneFormDialog"
            });
            boardManagerStep1Dialog.setSubmitEvent(function (step1Modal, result)
            {
                var boardManagerStep2Dialog = Relay.BoardManagerDialog({
                    url: "/BoardManager/StepTwoDialog?positionId=" + result.positionId,
                    method: "GET",
                    acceptFiles: true,
                    dialogId: "stepTwoFormDialog"
                });
                boardManagerStep2Dialog.setSubmitEvent(function (step2Modal, result)
                {
                    if (typeof result.userId !== "undefined") {
                        var boardManagerStep3Dialog = Relay.BoardManagerDialog({
                            url: "/BoardManager/StepThreeDialog?userId=" + result.userId + "&teamId=" + result.teamId,
                            method: "GET",
                            acceptFiles: true,
                            dialogId: "stepThreeFormDialog"
                        });
                        boardManagerStep3Dialog.setSubmitEvent(function (step3Modal, result)
                        {
                            if (typeof result.userId !== "undefined")
                            {
                                $(step3Modal.dialog).modal("hide");
                                location.reload();
                            }
                            else
                            {
                                $(step3Modal.dialog).modal("hide");
                                var url = result.action;

                                $.each(result.param, function (index, value) {
                                    url += !url.includes("?") ? "?" : "&";
                                    url += index + "=" + value;
                                });


                                //window.location.assign(document.location.origin + url);
                                //$location.href = document.location.origin + url;
                                //$window.location.href = document.location.origin + url;
                                document.location.href = document.location.origin + url;
                                location.reload();
                            }
                            
                        });
                       
                        boardManagerStep3Dialog.open();
                        $(step2Modal.dialog).modal("hide");
                    } else {

                        $(step2Modal.dialog).modal("hide");
                        var url = result.action;

                        $.each(result.param, function (index, value) {
                            url += !url.includes("?") ? "?" : "&";
                            url += index + "=" + value;
                        });

                        document.location = document.location.origin + url;
                        location.reload();
                    }
                    
                });
                boardManagerStep2Dialog.open();
                $(step1Modal.dialog).modal("hide");


            });
            boardManagerStep1Dialog.open();
        };

        ctrl.editEmployeeDialog = function (employeeId) {
            var formDialog = Relay.FormDialog({
                url: "/Employees/EditEmployeeDialog",
                method: "GET",
                acceptFiles: true,
                dialogId: "editEmployeeFormDialog",
                data: { EmployeeId: employeeId }
            });
            formDialog.setSubmitEvent(function () {
                update(ctrl, true);
            });
            formDialog.open();
        };

        update(ctrl, true);
    }

    function update(ctrl, reload) {

        var params = ctrl.location.search();

        if (params.teamId && (reload || ctrl.currentPage != params.employeePage || ctrl.currentTeamId != params.teamId)) {

            if (ctrl.currentTeamId != params.teamId && params.employeePage != 1) {
                ctrl.location.search('employeePage', 1);
                return;
            }

            ctrl.http.post("/Employees/GetEmployeesForTeam", {
                teamId: params.teamId,
                page: params.employeePage
            })
                .then(function (response) {
                    ctrl.vm.errorMsg = null;
                    ctrl.vm.page = response.data;
                },
                function (response) {
                    ctrl.vm.page = null;
                    ctrl.vm.errorMsg = "Server error: " + response.status;
                });
        }

        ctrl.currentPage = params.employeePage;
        ctrl.currentTeamId = params.teamId;
    }

})();