﻿; (function () {
    "use strict";

    angular
      .module("RotationsApp")
      .component("sitesPanel", {
          templateUrl: "/Content/js/relay/rotation-manager/modules/rotations-app/components/sites-panel/sites-panel.template.html",
          controller: SitesPanelController
      });

    SitesPanelController.$inject = ["$scope", "$http", "$location"];
    function SitesPanelController($scope, $http, $location) {

        var ctrl = this;
        ctrl.http = $http;
        ctrl.scope = $scope;
        ctrl.location = $location;
        ctrl.currentPage = $location.search().sitePage;
        $scope.isDisabled = false;
        $scope.isCanCreatSite = false;

        ctrl.isCanCreatSite = $http.post("/Sites/CreateSiteDialogForIntermediate")
            .then(function (data) {
                $scope.isCanCreatSite = data.data;
                ctrl.isCanCreatSite = data.data;
            });

        ctrl.isDisabled = $http.post("/Sites/CanCreateSiteDialog")
            .then(function (data) {
                $scope.isDisabled = data.data;
                ctrl.isDisabled = data.data;
            });
        
        ctrl.vm = {
            chosenSiteId: ctrl.location.search().siteId,
            page: null,
            errorMsg: ''
        };

        if (ctrl.currentPage === undefined)
            $location.search('sitePage', 1);

        ctrl.chooseSite = function (siteId) {
            if (ctrl.vm.chosenSiteId != siteId) {
                $location.search('siteId', siteId);
            }
        };

        ctrl.showPrevPage = function () {
            if (!ctrl.vm.page.IsFirstPage) {
                var params = ctrl.location.search();
                $location.search('sitePage', +params.sitePage - 1);
            }
        };

        ctrl.showNextpage = function () {
            if (!ctrl.vm.page.IsLastPage) {
                var params = ctrl.location.search();
                $location.search('sitePage', +params.sitePage + 1);
            }
        }

        $scope.$watch(function () { return $location.search() }, function () {
          //  debugger;
            UpdateSitesPanel(ctrl);
            $scope.isDisabled = false;
            $scope.isCanCreatSite = false;

            ctrl.isDisabled = $http.post("/Sites/CanCreateSiteDialog")
                .then(function (data) {
                    $scope.isDisabled = data.data;
                    ctrl.isDisabled = data.data;
                   // console.log("isDisabled - " + $scope.isDisabled);
                });

            ctrl.isCanCreatSite = $http.post("/Sites/CreateSiteDialogForIntermediate")
                .then(function (data) {
                    $scope.isCanCreatSite = data.data;
                    ctrl.isCanCreatSite = data.data;
                });

        }, true);

        //--- Open modal dialog for site creating --
        ctrl.createSiteDialog = function () {
          //  debugger;
            ctrl.isCanCreatSite = $http.post("/Sites/CreateSiteDialogForIntermediate")
                .then(function (data) {
                    $scope.isCanCreatSite = data.data;
                    ctrl.isCanCreatSite = data.data;
                });
            if (ctrl.isDisabled == true) {
                if ($scope.isCanCreatSite == false) {
                    var confirmDialog = Relay.ConfirmDialog(
                        "You cannot add additional locations on your subscription.",
                        "Please upgrade to add more.",
                        "Ok"
                    );
                    confirmDialog.setOkEvent(function () {
                    }.bind(this));
                    confirmDialog.open();
                }
                else {
                    var formDialog = Relay.FormDialog({
                        url: "/Sites/CreateSiteDialog",
                        method: "GET",
                        dialogId: "siteFormDialog"
                    });
                    formDialog.setSubmitEvent(function () {
                        UpdateSitesPanel(ctrl, true);
                    });
                    formDialog.open();
                }
            }
            else
            {
                var confirmDialog = Relay.ConfirmDialog(
                    "You cannot add additional locations on your subscription.",
                    "Please upgrade to add more.",
                    "Ok"
                );
                confirmDialog.setOkEvent(function () {
                    //----------
                    //----------
                }.bind(this));
                confirmDialog.open();
            }
        };

        ctrl.editSiteDialog = function (siteId) {
            var formDialog = Relay.FormDialog({
                url: "/Sites/EditSiteDialog?SiteId=" + siteId,
                method: "GET",
                dialogId: "siteFormDialog"
            });
            formDialog.setSubmitEvent(function () {
                UpdateSitesPanel(ctrl, true);
            });
            formDialog.open();
        };


        ctrl.removeSiteDialog = function (siteId) {
            var confirmDialog = Relay.ConfirmDialog(
                "Remove Locations",
                "Are you sure that you want to delete the location?",
                "Yes",
                "No"
            );

            confirmDialog.setOkEvent(function () {
                $.ajax({
                    url: "/Sites/RemoveSites",
                    method: "POST",
                    data: { siteId: siteId }
                })
                    .done(function (data) {
                        console.log("data - " + data);
                        if (data == "") {
                            location.reload();
                        }
                        var infoDlg = Relay.InfoDialog(data);
                        infoDlg.open();

                    });
            });
            confirmDialog.setCancelEvent();
            confirmDialog.open();

        };

        UpdateSitesPanel(ctrl, true);
    }

    function UpdateSitesPanel(ctrl, reload) {

        var params = ctrl.location.search();
        if (reload || ctrl.currentPage !== params.sitePage) {
            ctrl.http.post("/Sites/GetAllSites", { page: params.sitePage })
                .then(function (response) {
                    ctrl.vm.errorMsg = null;
                    ctrl.vm.page = response.data;
                },
                function (response) {
                    ctrl.vm.page = null;
                    ctrl.vm.errorMsg = "Server error: " + response.status;
                });
        }

        ctrl.currentPage = params.sitePage;
        ctrl.vm.chosenSiteId = params.siteId;
    }

})();