﻿; (function () {
    "use strict";

    angular.
      module("RotationsApp").
      component("positionsPanel", {
          templateUrl: "/Content/js/relay/rotation-manager/modules/rotations-app/components/positions-panel/positions-panel.template.html",
          controller: PositionsPanelController
      });

    PositionsPanelController.$inject = ["$scope", "$http", "$location"];
    function PositionsPanelController($scope, $http, $location) {

        var ctrl = this;
        ctrl.http = $http;
        ctrl.scope = $scope;
        ctrl.location = $location;
        ctrl.currentPage = $location.search().positionPage;
        ctrl.currentSiteId = $location.search().siteId;

        ctrl.vm = {
            chosenPositionId: null,
            page: null,
            errorMsg: null
        };

        if (ctrl.currentPage === undefined)
            $location.search('positionPage', 1);

        ctrl.choosePosition = function (positionId) {
            if (ctrl.vm.chosenPositionId != positionId) {
                $location.search('positionId', positionId);
            }
        };

        ctrl.showPrevPage = function () {
            if (!ctrl.vm.page.IsFirstPage) {
                var params = ctrl.location.search();
                $location.search('positionPage', +params.positionPage - 1);
            }
        };

        ctrl.showNextpage = function () {
            if (!ctrl.vm.page.IsLastPage) {
                var params = ctrl.location.search();
                $location.search('positionPage', +params.positionPage + 1);
            }
        }

        $scope.$watch(function () { return $location.search() }, function () {
            UpdatePositionsPanel(ctrl);
        }, true);

        //--- Open modal dialog for employee creating --
        ctrl.createPositionDialog = function () {
            var formDialog = Relay.FormDialog({
                url: "/Positions/CreatePositionDialog",
                method: "GET",
                dialogId: "positionFormDialog",
                data: { SiteId: ctrl.currentSiteId }
            });
            formDialog.setSubmitEvent(function () {
                UpdatePositionsPanel(ctrl, true);
            });
            formDialog.open();
        };

        ctrl.editPositionDialog = function (positionId) {
            var formDialog = Relay.FormDialog({
                url: "/Positions/EditPositionDialog?PositionId=" + positionId,
                method: "GET",
                dialogId: "positionFormDialog"
            });
            formDialog.setSubmitEvent(function () {
                UpdatePositionsPanel(ctrl, true);
            });
            formDialog.open();
        };

        ctrl.removePositionDialog = function (positionId) {
            var confirmDialog = Relay.ConfirmDialog(
                "Remove Job",
                "Are you sure that you want to delete the job?",
                "Yes",
                "No"
            );

            confirmDialog.setOkEvent(function () {
                $.ajax({
                    url: "/Positions/RemovePosition",
                    method: "POST",
                    data: { positionId: positionId }
                })
                    .done(function (data) {
                        console.log("data - " + data);
                        if (data == "") {
                            location.reload();
                        }
                        var infoDlg = Relay.InfoDialog(data);
                        infoDlg.open();

                    });
            });
            confirmDialog.setCancelEvent();
            confirmDialog.open();

        };

       
        UpdatePositionsPanel(ctrl, true);
    }

    function UpdatePositionsPanel(ctrl, reload) {

        var params = ctrl.location.search();

        if (params.siteId && (reload || ctrl.currentPage != params.positionPage || ctrl.currentSiteId != params.siteId)) {

            if (ctrl.currentSiteId != params.siteId) {
                if (params.positionId) {
                    ctrl.location.search('positionId', null);
                    return;
                }
                if (params.positionPage != 1) {
                    ctrl.location.search('positionPage', 1);
                    return;
                } 
            }

            ctrl.http.post("/Positions/GetPositionsForSite", { page: params.positionPage, siteId: params.siteId })
                .then(function (response) {
                    ctrl.vm.errorMsg = null;
                    ctrl.vm.page = response.data;
                },
                    function (response) {
                        ctrl.vm.page = null;
                        ctrl.vm.errorMsg = "Server error: " + response.status;
                });
        }

        ctrl.currentPage = params.positionPage;
        ctrl.currentSiteId = params.siteId;
        ctrl.vm.chosenPositionId = params.positionId;
    }

})();