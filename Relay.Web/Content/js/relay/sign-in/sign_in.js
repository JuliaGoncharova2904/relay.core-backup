$(document).ready(function ()
{
    $('.remember-me').click(function ()
    {
        if ($(this).hasClass('remember-me-checked'))
        {
            $(this).removeClass('remember-me-checked');
            $('#RememberMe').prop("checked", false);
            $("#RememberMe").val(false);
        } else
        {
            $(this).addClass('remember-me-checked');
            $('#RememberMe').prop("checked", true);
            $("#RememberMe").val(true);
        }
    });

    window.addEventListener("beforeunload", function (event)
    {
        setTimeout(function ()
        {
            $('#loading-animation').show();
        }, 1000);
    });

})