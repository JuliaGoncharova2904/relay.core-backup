﻿$(document).ready(function () {
    var majorHazardPanel = $("#majorHazardPanel");
    var formContainer = null;

    function Init_AddMechanism() {
        $(".major-hazard-item").click(majorHazardClickHandler);
    }

    Init_AddMechanism();
    //---------------- Edit Major Hazard ---------------------
    function majorHazardClickHandler() {
        var majorHazardId = $(this).attr("id");
        if (majorHazardId) {
            var majorHazardV2Dialog = Relay.MajorHazardV2Dialog(majorHazardId);
            majorHazardV2Dialog.setCloseEvent(function () {
                reloadMajorHazardContent(majorHazardId);
            });
            majorHazardV2Dialog.open();
        }
    }
    //--------------------------------------------------------
    function reloadMajorHazardContent(majorHazardId) {
        $.ajax({
            url: "/SafetyMajorHazardsV2/GetMajorHazardInfo",
            method: "POST",
            data: { majorHazardId: majorHazardId }
        })
        .done(function (data) {
            $('#' + data.Id + " img").attr("src", data.IconUrl);
            $('#' + data.Id + " .tx").text(data.Name);
            $('#' + data.Id).parent().find(".name-field").text(data.OwnerName);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    }
    //--------------------------------------------------------
});