﻿(function () {
    //===========================================================================
    //  majorHazardId
    //===================== Major Hazard Dialog =================================
    function MajorHazardV2Dialog(majorHazardId) {

        this.majorHazardId = majorHazardId;
        this.container = null;

        this.openHandler = null;
        this.closeHandler = null;

        this.slidesContainer = null;
        this.addFormContainer = null;
        this.addButton = null;
        this.saveBtn = null;
        this.closeBtn = null;

        this.criticalControlsSwiper = null;

        this.config = {
            dialogUrl: "/SafetyMajorHazardsV2/EditMajorHazardDialog",
            dialogId: "editMajorHazardV2Dialog",
            addCriticalControlUrl: "/SafetyCriticalControlsV2/CreateCriticalControl",
            editCriticalControlUrl: "/SafetyCriticalControlsV2/EditCriticalControl"
        };
    }
    MajorHazardV2Dialog.prototype.open = function () {
        var _this = this;
        //--------------------------
        $('#loading-animation').show();
        if (_this.openHandler) {
            _this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: this.config.dialogUrl,
            method: "GET",
            data: { majorHazardId: this.majorHazardId }
        })
        .done(function (result) {
            MajorHazardV2Dialog_BuildDialog.call(_this, result);
            MajorHazardV2Dialog_OpenDialog.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    MajorHazardV2Dialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    MajorHazardV2Dialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };

    function MajorHazardV2Dialog_InitSubmitForm() {
        var _this = this;

        var handler = function () {
            //------- Confirm close event --------
            _this.closeBtn.removeAttr("data-dismiss");
            _this.closeBtn.unbind("click");
            _this.closeBtn.bind("click", MajorHazardV2Dialog_CloseEvent.bind(_this));
            //------- Active save button ---------
            _this.saveBtn.addClass("active");
            _this.saveBtn.unbind("click");
            _this.saveBtn.bind("click", MajorHazardV2Dialog_SubmitForm.bind(_this));
            //------------------------------------
        };

        $('.major-hazard-owner select', this.conteiner).bind("change", handler);
    }

    function MajorHazardV2Dialog_SubmitForm() {
        var _this = this;
        var form = $(".major-hazard-owner form", this.conteiner).first();

        $.ajax({
            url: form[0].action,
            type: form[0].method,
            data: form.serialize()
        })
        .done(function (result) {
            _this.saveBtn.removeClass("active");
            _this.saveBtn.unbind("click");
            _this.closeBtn.attr("data-dismiss", "modal");
            _this.closeBtn.unbind("click");
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    }

    function MajorHazardV2Dialog_InitChampionsLists() {
        $(".critical-control-champion", this.slidesContainer).each(function (index, element) {
            var removedElements = 0;
            var elementsHeight = 0;

            $(element).children().each(function (index, element) {
                var elementHeight = $(element).width() + 10;

                if (elementsHeight + elementHeight < 100) {
                    elementsHeight += elementHeight;
                }
                else {
                    $(element).remove();
                    removedElements++;
                }
            });

            if (removedElements) {
                var numberDiv = document.createElement("div");
                numberDiv.className = "team-fill";
                numberDiv.textContent = '+' + removedElements.toString();

                element.appendChild(numberDiv);
            }
        });

        this.criticalControlsSwiper.update(false);
    }

    function MajorHazardV2Dialog_ReloadCriticalControlsList() {
        var _this = this;

        $.ajax({
            url: this.config.dialogUrl,
            method: "GET",
            data: { majorHazardId: this.majorHazardId }
        })
        .done(function (result) {
            _this.slidesContainer.html($(result).find(".swiper-wrapper").children());
            MajorHazardV2Dialog_InitChampionsLists.call(_this);
            $(".critical-control-item", _this.conteiner).click(MajorHazardV2Dialog_EditCriticalControl.bind(_this));
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
    }

    function MajorHazardV2Dialog_BuildDialog(dialogHtml) {
        var _this = this;

        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = dialogHtml;
        document.body.appendChild(this.conteiner);

        Relay.InitInputFields(this.conteiner);

        this.slidesContainer = $(".swiper-wrapper", this.conteiner).first();
        this.addFormContainer = $("#new-safely-popup-safely", this.conteiner);
        this.addButton = $(".btn-plus-white", this.conteiner).first();
        this.saveBtn = $(".btn-save", this.conteiner).first();
        this.closeBtn = $(".btn-close", this.conteiner).first();


        $('#' + this.config.dialogId, this.conteiner).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });

        this.addButton.click(MajorHazardV2Dialog_AddCriticalControl.bind(this));
        $(".critical-control-item", this.conteiner).click(MajorHazardV2Dialog_EditCriticalControl.bind(this));
        MajorHazardV2Dialog_InitSubmitForm.call(this);
    }
    function MajorHazardV2Dialog_OpenDialog() {
        var _this = this;
        //---------- Critical Controls List ------------
        this.criticalControlsSwiper = new Swiper($(".swiper-container", this.conteiner), {
            direction: 'vertical',
            height: 40,
            nextButton: $(".swiper-next", this.conteiner),
            prevButton: $(".swiper-prev", this.conteiner)
        });
        //----------------------------------------------
        this.addFormContainer.slideUp();
        $('#' + this.config.dialogId, this.conteiner).modal('show');

        setTimeout(function () {
            MajorHazardV2Dialog_InitChampionsLists.call(_this);
        }, 150);
    }

    function MajorHazardV2Dialog_EditCriticalControl(event) {
        var _this = this;
        var criticalControlId = $(event.currentTarget).attr("id");
        Relay.DialogLoadingAnimation.show(this.conteiner, 100);
        $.ajax({
            url: this.config.editCriticalControlUrl,
            method: "GET",
            data: { criticalControlId: criticalControlId }
        })
        .done(function (result) {
            _this.addFormContainer.html(result);
            Relay.InitInputFields(_this.addFormContainer);
            _this.addFormContainer.slideDown();
            _this.addButton.css('display', 'block');
            MajorHazardV2Dialog_InitChangeDataEvents.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            Relay.DialogLoadingAnimation.hide();
        });
    }

    function MajorHazardV2Dialog_AddCriticalControl() {
        var _this = this;
        Relay.DialogLoadingAnimation.show(this.conteiner, 100);
        $.ajax({
            url: this.config.addCriticalControlUrl,
            method: "GET",
            data: { majorHazardId: this.majorHazardId }
        })
        .done(function (result) {
            _this.addFormContainer.html(result);
            Relay.InitInputFields(_this.addFormContainer);
            _this.addFormContainer.slideDown();
            _this.addButton.css('display', 'none');
            MajorHazardV2Dialog_InitChangeDataEvents.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            Relay.DialogLoadingAnimation.hide();
        });
    }

    function MajorHazardV2Dialog_SubmitCriticalControlForm() {
        var _this = this;
        var form = $("form", this.addFormContainer).first();

        $.validator.unobtrusive.parse(form);
        if (form.valid()) {
            Relay.DialogLoadingAnimation.show(this.conteiner, 100);
            $.ajax({
                url: form[0].action,
                type: form[0].method,
                processData: false,
                contentType: false,
                data: new FormData(form[0])
            })
            .done(function (result) {
                if (result === "ok") {
                    MajorHazardV2Dialog_ReloadCriticalControlsList.call(_this);
                    _this.addFormContainer.slideUp();
                    _this.addButton.css('display', 'block');
                    _this.saveBtn.removeClass("active");
                    _this.saveBtn.unbind("click");
                    _this.closeBtn.attr("data-dismiss", "modal");
                    _this.closeBtn.unbind("click");
                } else {
                    _this.addFormContainer.html(result);
                    Relay.InitInputFields(_this.addFormContainer);
                    MajorHazardV2Dialog_InitChangeDataEvents.call(_this);
                }
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            })
            .always(function () {
                Relay.DialogLoadingAnimation.hide();
            });
        }
    }

    function MajorHazardV2Dialog_CloseEvent() {
        var confirmDialog = Relay.ConfirmDialog(
            "All changes will be lost. Are you sure you want to exit?",
            "",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            $('#' + this.config.dialogId, this.conteiner).modal('hide');
        }.bind(this));
        confirmDialog.open();
    };

    function MajorHazardV2Dialog_InitChangeDataEvents() {
        //------- Confirm close event --------
        this.closeBtn.removeAttr("data-dismiss");
        this.closeBtn.unbind("click");
        this.closeBtn.bind("click", MajorHazardV2Dialog_CloseEvent.bind(this));
        //------- Active save button ---------
        this.saveBtn.addClass("active");
        this.saveBtn.unbind("click");
        this.saveBtn.bind("click", MajorHazardV2Dialog_SubmitCriticalControlForm.bind(this));
        //------ Unbind change events --------
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.MajorHazardV2Dialog = function (majorHazardId) {
        return new MajorHazardV2Dialog(majorHazardId);
    };
    //===========================================================================
})();