﻿; (function () {
    var dialog = document.getElementById("editSafetyMessageV2Dialog");
    var container = $(".mini-calendar");

    initCalendar();

    function initCalendar() {
        $("td", container).not(".gray, .pagination-cell").click(choseDay);
        $(".calendar-previous", container).click(clickPrevMonth);
        $(".calendar-next", container).click(clickNextMonth);
    }

    function setDataChanged() {
        if(dialog)
            $("button[type=submit]", dialog).trigger("activate");
    }

    function choseDay() {
        var date = $(this).attr("date");
        $("td.blue", container).removeClass("blue");
        $(this).addClass("blue");
        $("#Date", container).val(date);
        setDataChanged();
    }

    function clickNextMonth() {
        var nextDate = new Date($("#Date").val());
        nextDate.setDate(1);
        nextDate.setMonth(nextDate.getMonth() + 1);
        reloadCalendar(nextDate.toISOString());
    }

    function clickPrevMonth() {
        var prevDate = new Date($("#Date").val());
        prevDate.setDate(1);
        prevDate.setMonth(prevDate.getMonth() - 1);
        reloadCalendar(prevDate.toISOString());
    }

    function reloadCalendar(date) {
        $.ajax({
            url: "/SafetyShedulerV2/GetMinCalendar",
            method: "GET",
            data: { date: date }
        })
        .done(function (result) {
            container.html($(result));
            initCalendar();
            setDataChanged();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    }
})();