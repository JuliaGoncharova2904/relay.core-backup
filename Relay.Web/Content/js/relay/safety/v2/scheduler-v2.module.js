﻿$(document).ready(function () {

    var containerCalc = $(".calendar-container").first();

    //======================= Initialization =================================
    Initialization();

    function Initialization() {

        $("td .plus, td .plus-down", containerCalc).not(".view").click(function () {
            var dateString = $(this).parent().attr("date");
            var date = new Date(dateString);

            var safetyForm = Relay.FormDialog({
                url: "/SafetyShedulerV2/AddSafetyMessageDialog",
                method: "GET",
                dialogId: "editSafetyMessageV2Dialog",
                data: { date: date.toISOString() }
            });
            safetyForm.setCloseEvent(reloadCalendar);
            safetyForm.open();
        });

        $("td .many-messages, td img", containerCalc).not(".view").click(function () {
            var dateString = $(this).parent().attr("date");
            var date = new Date(dateString);

            var safetyForm = Relay.FormDialog({
                url: "/SafetyShedulerV2/SafetyMessagesForDateDialog",
                method: "GET",
                dialogId: "safetyDayMessagesV2Dialog",
                data: { date: date.toISOString() }
            });
            safetyForm.setCloseEvent(reloadCalendar);
            safetyForm.open();
        });

        $(".next-page", containerCalc).click(nextMonth);
        $(".prev-page", containerCalc).click(prevMonth);
    }
    //======================== Month Pagination ==========================
    function nextMonth() {
        var nextDate = new Date($("#CalendarDate").val());
        nextDate.setDate(1);
        nextDate.setMonth(nextDate.getMonth() + 1);
        loadCalendar(nextDate.toISOString());
    }

    function prevMonth() {
        var prevDate = new Date($("#CalendarDate").val());
        prevDate.setDate(1);
        prevDate.setMonth(prevDate.getMonth() - 1);
        loadCalendar(prevDate.toISOString());
    }
    //========================== Load Calendar ===========================
    function reloadCalendar() {
        var date = new Date($("#CalendarDate").val());
        loadCalendar(date.toISOString());
    }

    function loadCalendar(date) {
        $.ajax({
            url: "/SafetyShedulerV2/GetFullCalendar",
            method: "GET",
            data: { date: date }
        })
        .done(function (result) {
            containerCalc.html($(result));
            Initialization();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    }
    //====================================================================
});