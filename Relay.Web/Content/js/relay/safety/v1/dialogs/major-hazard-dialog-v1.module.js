﻿(function () {
    //===========================================================================
    //  majorHazardId
    //===================== Major Hazard Dialog =================================
    function MajorHazardDialog(majorHazardId) {

        this.majorHazardId = majorHazardId;
        this.container = null;

        this.openHandler = null;
        this.closeHandler = null;

        this.slidesContainer = null;
        this.addFormContainer = null;
        this.addButton = null;
        this.saveBtn = null;
        this.closeBtn = null;

        this.criticalControlsSwiper = null;

        this.config = {
            dialogUrl: "/MajorHazard/EditMajorHazardDialog",
            dialogId: "editMajorHazardDialog",
            addCriticalControlUrl: "/CriticalControl/CreateCriticalControl",
            editCriticalControlUrl: "/CriticalControl/EditCriticalControl",
            criticalControlDialogId: "editCriticalControlForm"
        };
    }
    MajorHazardDialog.prototype.open = function () {
        var _this = this;
        //--------------------------
        $('#loading-animation').show();
        if (_this.openHandler) {
            _this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: this.config.dialogUrl + "?majorHazardId=" + this.majorHazardId,
            method: "GET"
        })
        .done(function (result) {
            MajorHazardDialog_BuildDialog.call(_this, result);
            MajorHazardDialog_OpenDialog.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    MajorHazardDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    MajorHazardDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };

    function MajorHazardDialog_InitTeamsLists() {
        $(".critical-control-permission", this.slidesContainer).each(function (index, element) {
            var removedElements = 0;
            var elementsHeight = 0;

            $(element).children().each(function (index, element) {
                var elementHeight = $(element).height();

                if (elementsHeight + elementHeight < 55) {
                    elementsHeight += elementHeight;
                }
                else {
                    $(element).remove();
                    removedElements++;
                }
            });

            if (removedElements) {
                var numberDiv = document.createElement("div");
                numberDiv.className = "team-fill";
                numberDiv.textContent = '+' + removedElements.toString();

                element.appendChild(numberDiv);
            }
        });

        this.criticalControlsSwiper.update(false);
    }

    function MajorHazardDialog_ReloadCriticalControlsList() {
        var _this = this;

        $.ajax({
            url: this.config.dialogUrl + "?majorHazardId=" + this.majorHazardId,
            method: "GET"
        })
        .done(function (result) {
            _this.slidesContainer.html($(result).find(".swiper-wrapper").children());
            MajorHazardDialog_InitTeamsLists.call(_this);
            $(".critical-control-item", _this.conteiner).click(MajorHazardDialog_EditCriticalControl.bind(_this));
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
    }

    function MajorHazardDialog_BuildDialog(dialogHtml) {
        var _this = this;

        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = dialogHtml;
        document.body.appendChild(this.conteiner);

        this.slidesContainer = $(".swiper-wrapper", this.conteiner).first();
        this.addFormContainer = $("#new-safely-popup-safely", this.conteiner);
        this.addButton = $(".btn-plus-white", this.conteiner).first();
        this.saveBtn = $(".btn-save", this.conteiner).first();
        this.closeBtn = $(".btn-close", this.conteiner).first();

        $('#' + this.config.dialogId, this.conteiner).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });

        this.addButton.click(MajorHazardDialog_AddCriticalControl.bind(this));
        $(".critical-control-item", this.conteiner).click(MajorHazardDialog_EditCriticalControl.bind(this));
    }
    function MajorHazardDialog_OpenDialog() {
        var _this = this;
        //---------- Critical Controls List ------------
        this.criticalControlsSwiper = new Swiper($(".swiper-container", this.conteiner), {
            direction: 'vertical',
            height: 40,
            nextButton: $(".swiper-next", this.conteiner),
            prevButton: $(".swiper-prev", this.conteiner)
        });
        //----------------------------------------------
        this.addFormContainer.slideUp();
        $('#' + this.config.dialogId, this.conteiner).modal('show');

        setTimeout(function () {
            MajorHazardDialog_InitTeamsLists.call(_this);
        }, 150);
    }

    function MajorHazardDialog_EditCriticalControl(event) {
        var _this = this;
        var criticalControlId = $(event.target).attr("critical-control-id");
        Relay.DialogLoadingAnimation.show(this.conteiner, 100);
        $.ajax({
            url: this.config.editCriticalControlUrl + '?criticalControlId=' + criticalControlId,
            method: "GET"
        })
        .done(function (result) {
            _this.addFormContainer.html(result);
            Relay.InitInputFields(_this.addFormContainer);
            _this.addFormContainer.slideDown();
            _this.addButton.css('display', 'block');
            MajorHazardDialog_InitChangeDataEvents.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            Relay.DialogLoadingAnimation.hide();
        });
    }

    function MajorHazardDialog_AddCriticalControl() {
        var _this = this;
        Relay.DialogLoadingAnimation.show(this.conteiner, 100);
        $.ajax({
            url: this.config.addCriticalControlUrl + '?majorHazardId=' + this.majorHazardId,
            method: "GET"
        })
        .done(function (result) {
            _this.addFormContainer.html(result);
            Relay.InitInputFields(_this.addFormContainer);
            _this.addFormContainer.slideDown();
            _this.addButton.css('display', 'none');
            MajorHazardDialog_InitChangeDataEvents.call(_this);
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            Relay.DialogLoadingAnimation.hide();
        });
    }

    function MajorHazardDialog_SubmitForm() {
        var _this = this;
        var form = $("form", this.addFormContainer).first();

        $.validator.unobtrusive.parse(form);
        if (form.valid()) {
            Relay.DialogLoadingAnimation.show(this.conteiner, 100);
            $.ajax({
                url: form[0].action,
                type: form[0].method,
                processData: false,
                contentType: false,
                data: new FormData(form[0])
            })
            .done(function (result) {
                if (result === "ok") {
                    MajorHazardDialog_ReloadCriticalControlsList.call(_this);
                    _this.addFormContainer.slideUp();
                    _this.addButton.css('display', 'block');
                    _this.saveBtn.removeClass("active");
                    _this.saveBtn.unbind("click");
                    _this.closeBtn.attr("data-dismiss", "modal");
                    _this.closeBtn.unbind("click");
                } else {
                    _this.addFormContainer.html(result);
                    Relay.InitInputFields(_this.addFormContainer);
                    MajorHazardDialog_InitChangeDataEvents.call(_this);
                }
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            })
            .always(function () {
                Relay.DialogLoadingAnimation.hide();
            });
        }
    }

    function MajorHazardDialog_CloseEvent() {
        var confirmDialog = Relay.ConfirmDialog(
            "All changes will be lost. Are you sure you want to exit?",
            "",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            $('#' + this.config.dialogId, this.conteiner).modal('hide');
        }.bind(this));
        confirmDialog.open();
    };

    function MajorHazardDialog_InitChangeDataEvents() {
        var _this = this;
        var teams = JSON.parse($("#TeamsString", this.conteiner).val());
        var allTeams = JSON.parse($("#AllTeamsString", this.conteiner).val());

        //------------------- Init temas field ------------------
        var teamsInput = $("#tags-input", this.conteiner).magicSuggest({
            placeholder: 'Select Teams',
            maxSelection: 7,
            hideTrigger: true,
            toggleOnClick: true,
            allowFreeEntries: false,
            editable: false,
            name: "Teams",
            valueField: 'Id',
            displayField: 'Name',
            value: teams,
            data: allTeams
        });
        //-------------------------------------------------------

        var handler = function () {
            //------- Confirm close event --------
            _this.closeBtn.removeAttr("data-dismiss");
            _this.closeBtn.bind("click", MajorHazardDialog_CloseEvent.bind(_this));
            //------- Active save button ---------
            _this.saveBtn.addClass("active");
            _this.saveBtn.bind("click", MajorHazardDialog_SubmitForm.bind(_this));
            //------ Unbind change events --------
            $('select', _this.conteiner).unbind("change", handler);
            $('.simple-input', _this.conteiner).unbind("input", handler);
            $('.simple-textarea', _this.conteiner).unbind("input", handler);
            $(teamsInput).unbind("selectionchange");
            //------------------------------------
        };

        $('select', this.conteiner).bind("change", handler);
        $('.simple-input', this.conteiner).bind("input", handler);
        $('.simple-textarea', this.conteiner).bind("input", handler);
        $(teamsInput).bind("selectionchange", handler);
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.MajorHazardDialog = function (majorHazardId) {
        return new MajorHazardDialog(majorHazardId);
    };
    //===========================================================================
})();