﻿; (function () {
    var dialog = document.getElementById("editSafetyMessageDialog");
    var majorHazardSelect = document.getElementById("MajorHazard");
    var criticalControlSelect = document.getElementById("CriticalControl");
    var quickAddCriticalControl = dialog.getElementsByClassName("add-new-control")[0];

    var teamsList = initTeamList(dialog);

    majorHazardSelect.addEventListener("change", loadCriticalContrlols);
    criticalControlSelect.addEventListener("change", loadTeams);
    quickAddCriticalControl.addEventListener("click", quickAddCriticalControlHandler);
    //-------------
    function initTeamList(dialog) {
        var teamsString = $("#TeamsString", dialog).val();
        var allTeamsString = $("#AllTeamsString", dialog).val();

        var teamsList = $("#tags-input", dialog).magicSuggest({
            placeholder: 'Select Teams',
            maxSelection: 7,
            hideTrigger: true,
            toggleOnClick: true,
            allowFreeEntries: false,
            editable: false,
            name: "Teams",
            valueField: 'Id',
            displayField: 'Name',
            value: JSON.parse(teamsString),
            data: JSON.parse(allTeamsString)
        });

        var temsSelectHandler = function () {
            $("input[type=submit]", dialog).trigger("activate");
            $(teamsList).unbind("selectionchange", temsSelectHandler);
        }

        $(teamsList).bind("selectionchange", temsSelectHandler);

        return teamsList;
    }
    //-------------
    function quickAddCriticalControlHandler() {
        var majorHazardId = majorHazardSelect.options[majorHazardSelect.selectedIndex].value;

        if (majorHazardId) {
            var majorHazardDialog = Relay.MajorHazardDialog(majorHazardId);
            majorHazardDialog.setCloseEvent(loadCriticalContrlols);
            majorHazardDialog.open();
        }
        else {
            Relay.InfoDialog("Please select Major Hazard before add Critical Control.").open();
        }
    }
    //-------------
    function loadCriticalContrlols() {
        var majorHazardId = majorHazardSelect.options[majorHazardSelect.selectedIndex].value;

        if (majorHazardId) {
            $.ajax({
                url: "/ScheduleMessage/GetCriticalControlsForMajorHazard",
                method: "POST",
                data: { majorHazardId: majorHazardId }
            })
            .done(function (data) {
                if (data) {
                    updateCriticalControls(data);
                    teamsList.clear();
                    teamsList.setData([]);
                }
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            });
        }
        else {
            updateCriticalControls([]);
            teamsList.clear();
            teamsList.setData([]);
        }
    }
    //-------------
    function updateCriticalControls(items) {

        while (criticalControlSelect.firstChild) {
            criticalControlSelect.removeChild(criticalControlSelect.firstChild);
        }

        var defOption = document.createElement("option");
        defOption.textContent = "Select Critical Control";
        defOption.setAttribute("value", "");
        criticalControlSelect.appendChild(defOption);

        for (var i = 0; i < items.length; ++i) {
            var option = document.createElement("option");
            option.setAttribute("value", items[i].Id);
            option.textContent = items[i].Name;
            criticalControlSelect.appendChild(option);
        }

        $(criticalControlSelect).selectpicker('refresh');
    }
    //-------------
    function loadTeams() {
        var criticalControlId = criticalControlSelect.options[criticalControlSelect.selectedIndex].value;

        if (criticalControlId) {
            $.ajax({
                url: "/ScheduleMessage/GetTeamsForCriticalControl",
                method: "POST",
                data: { criticalControlId: criticalControlId }
            })
            .done(function (data) {
                if (data) {
                    teamsList.clear();
                    teamsList.setData(data);
                }
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            });
        }
        else {
            teamsList.clear();
            teamsList.setData([]);
        }
    }
    //-------------
})();