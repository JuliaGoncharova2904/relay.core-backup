﻿; (function () {
    var dialog = document.getElementById("editSafetyMessageDialog");
    var container = $(".mini-calendar", dialog);
    
    initCalendar();

    function initCalendar() {
        $("td", container).not(".gray").click(choseDay);
        $(".calendar-previous", container).click(clickPrevMonth);
        $(".calendar-next", container).click(clickNextMonth);
    }

    function setDataChanged() {
        if(dialog)
            $("input[type=submit]", dialog).trigger("activate");
    }

    function choseDay() {
        var date = $(this).attr("date");
        $("td.blue", container).removeClass("blue");
        $(this).addClass("blue");
        $("#Date", container).val(date);
        setDataChanged();
    }

    function clickNextMonth() {
        var nextDate = new Date($("#Date").val());
        nextDate.setMonth(nextDate.getMonth() + 1);
        nextDate.setDate(1);
        reloadCalendar(nextDate.toISOString());
    }

    function clickPrevMonth() {
        var prevDate = new Date($("#Date").val());
        prevDate.setMonth(prevDate.getMonth() - 1);
        prevDate.setDate(1);
        reloadCalendar(prevDate.toISOString());
    }

    function reloadCalendar(date) {
        $.ajax({
            url: "/ScheduleMessage/GetMinCalendar?date=" + date,
            method: "GET"
        })
        .done(function (result) {
            container.html($(result));
            initCalendar();
            setDataChanged();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    }
})();