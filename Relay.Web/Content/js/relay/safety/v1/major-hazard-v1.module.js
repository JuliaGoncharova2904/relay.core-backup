﻿$(document).ready(function () {
    var addFormUrl = "/MajorHazard/AddMajorHazard";
    var majorHazardPanel = $("#majorHazardPanel");
    var formContainer = null;

    function Init_AddMechanism() {
        formContainer = $('#newForm');
        $('#newItem').click(openAddMajorHazardForm);
        $(".safely-content .majhazard.orange").click(majorHazardClickHandler);
    }

    Init_AddMechanism();
    //------------- Add Major Hazard Confirm -----------------
    function validationDialog() {
        var errors = "";
        $(".field-validation-error", formContainer).each(function (index, element) {
            errors += "<p>" + $(element).children().first().text() + "</p>";
        });
        $(".field-validation-error", formContainer).attr("style", "display: none;");

        formContainer.unbind("clickoutside");

        var confirmDialog = Relay.ConfirmDialog(
            "Validation Error. <br><p>Do you want to continue?</p>",
            //errors + "<br><p>Do you want to continue?</p>",
            "",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            setTimeout(function () {
                formContainer.bind("clickoutside", submitAddMajorHazardForm);
            }, 100);
        });
        confirmDialog.setCancelEvent(function () {
            reloadMajorHazardContent();
        });
        confirmDialog.open();
    }
    //----------------- Add Major Hazard ---------------------
    function submitAddMajorHazardForm() {
        var form = $('form', formContainer)[0];

        $.validator.unobtrusive.parse(form);
        if ($(form).valid()) {
            $.ajax({
                url: form.action,
                method: form.method,
                data: new FormData(form),
                contentType: false,
                processData: false,
            })
            .done(function (result) {
                if (result === "ok") {
                    formContainer.unbind("clickoutside");
                    reloadMajorHazardContent();
                }
                else {
                    formContainer.html($(result));
                    validationDialog();
                }
            })
            .fail(function () {
                var errorDialog = Relay.ErrorDialog();
                errorDialog.setCloseEvent(function () {
                    location.reload();
                });
                errorDialog.open();
            });
        }
        else {
            validationDialog();
        }
    }

    function openAddMajorHazardForm() {
        $.ajax({
            url: addFormUrl,
            method: "GET"
        })
        .done(function (result) {
            formContainer.html($(result));
            setTimeout(function () {
                formContainer.bind("clickoutside", submitAddMajorHazardForm);
            }, 100);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        }); 
    }
    //---------------- Edit Major Hazard ---------------------
    function majorHazardClickHandler() {
        var majorHazardId = $(this).attr("major-hazard-id");
        if (majorHazardId) {
            var majorHazardDialog = Relay.MajorHazardDialog(majorHazardId);
            majorHazardDialog.open();
        }
    }
    //--------------------------------------------------------
    function reloadMajorHazardContent() {
        $.ajax({
            url: "/Safety/Administration",
            method: "GET"
        })
        .done(function (result) {
            majorHazardPanel.html($(result).find("#majorHazardPanel").children());
            Init_AddMechanism();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    }
    //--------------------------------------------------------
});