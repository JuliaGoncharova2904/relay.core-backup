﻿$(document).ready(function () {

    var containerCalc = $(".container-calendar").first();
    //=============== Initialization of team pagination ======================
    var currentTeam = 0;
    var teams = JSON.parse($("#TeamsString").val());

    var teamNameContainer = $(".pagination-pages .page").first();
    var firstTeamPageBtn = $(".pagination-pages .first-page").first();
    var prevTeamPageBtn = $(".pagination-pages .prev-page").first();
    var nextTeamPageBtn = $(".pagination-pages .next-page").first();
    var lastTeamPageBtn = $(".pagination-pages .last-page").first();

    firstTeamPageBtn.click(function () {
        if (currentTeam !== 0) {
            currentTeam = 0;
            loadCalendatWithCurrentTeam();
            teamPagiationAnimation();
        }
    });
    prevTeamPageBtn.click(function () {
        if (currentTeam > 0) {
            --currentTeam;
            loadCalendatWithCurrentTeam();
            teamPagiationAnimation();
        }
    });
    nextTeamPageBtn.click(function () {
        if (currentTeam < teams.length - 1) {
            ++currentTeam;
            loadCalendatWithCurrentTeam();
            teamPagiationAnimation();
        }
    });
    lastTeamPageBtn.click(function () {
        if (currentTeam !== teams.length - 1) {
            currentTeam = teams.length - 1;
            loadCalendatWithCurrentTeam();
            teamPagiationAnimation();
        }
    });
    //======================== Team Pagination ===========================
    function teamPagiationAnimation() {

        teamNameContainer.text(teams[currentTeam].Name);

        if (currentTeam === 0) {
            firstTeamPageBtn.addClass("inactive");
            prevTeamPageBtn.addClass("inactive");
        }
        else {
            firstTeamPageBtn.removeClass("inactive");
            prevTeamPageBtn.removeClass("inactive");
        }

        if (currentTeam === teams.length - 1) {
            lastTeamPageBtn.addClass("inactive");
            nextTeamPageBtn.addClass("inactive");
        }
        else {
            lastTeamPageBtn.removeClass("inactive");
            nextTeamPageBtn.removeClass("inactive");
        }
    }

    function loadCalendatWithCurrentTeam() {
        var date = new Date($("#CalendarDate").val());
        loadCalendar(date.toISOString(), teams[currentTeam].Id);
    }
    //======================= Initialization =================================
    Initialization();

    function Initialization() {

        $(".container-calendar td .plus, td .plusHov").not(".view").click(function () {
            var dateString = $(this).parent().attr("date");
            var date = new Date(dateString);

            var safetyForm = Relay.FormDialog({
                url: "/ScheduleMessage/AddSafetyMessageDialog",
                method: "GET",
                dialogId: "editSafetyMessageDialog",
                data: { date: date.toISOString() }
            });
            safetyForm.setCloseEvent(loadCalendatWithCurrentTeam);
            safetyForm.open();
        });

        $(".container-calendar .massage").click(function () {
            var safetyForm = Relay.FormDialog({
                url: "/ScheduleMessage/AddSafetyMessageDialog",
                method: "GET",
                dialogId: "editSafetyMessageDialog"
            });
            safetyForm.setCloseEvent(loadCalendatWithCurrentTeam);
            safetyForm.open();
        });

        $(".container-calendar td .many-messages,.container-calendar td img").not(".view").click(function () {
            var dateString = $(this).parent().attr("date");
            var date = new Date(dateString);

            var safetyForm = Relay.FormDialog({
                url: "/ScheduleMessage/SafetyMessagesForDateDialog",
                method: "GET",
                dialogId: "safetyDayMessagesDialog",
                data: {
                    date: date.toISOString(),
                    teamId: teams[currentTeam].Id
                }
            });
            safetyForm.setCloseEvent(loadCalendatWithCurrentTeam);
            safetyForm.open();
        });

        $(".clndr-next", containerCalc).click(nextMonth);
        $(".clndr-previous", containerCalc).click(prevMonth);
    }
    //======================== Month Pagination ==========================
    function nextMonth() {
        var nextDate = new Date($("#CalendarDate").val());
        nextDate.setMonth(nextDate.getMonth() + 1);
        nextDate.setDate(1);
        loadCalendar(nextDate.toISOString(), teams[currentTeam].Id);
    }

    function prevMonth() {
        var prevDate = new Date($("#CalendarDate").val());
        prevDate.setMonth(prevDate.getMonth() - 1);
        prevDate.setDate(1);
        loadCalendar(prevDate.toISOString(), teams[currentTeam].Id);
    }
    //========================== Load Calendar ===========================
    function loadCalendar(date, teamId) {
        $.ajax({
            url: "/ScheduleMessage/GetFullCalendar",
            method: "GET",
            data: { date: date, teamId: teamId }
        })
        .done(function (result) {
            containerCalc.html($(result));
            Initialization();
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    }
    //====================================================================
});