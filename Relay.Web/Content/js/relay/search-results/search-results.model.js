﻿$(document).ready(function () {

    var selectUserList = $(".creator-title .select-list");

    var selectNameTopics = $(".topic-name-title .select-list");

    selectUserList.selectpicker({
        style: 'select-class',
        size: 7,
        dropupAuto: false,
        liveSearch: true
    });

    selectNameTopics.selectpicker({
        style: 'select-class',
        size: 7,
        dropupAuto: false,
        liveSearch: true
    });

    selectUserList.bind("change", applaySearchParams);

    selectNameTopics.bind("change", applaySearchParams);

    function applaySearchParams() {
        var match;
        var params = {};
        var re = /\??(.*?)=([^\&]*)&?/gi;
        var search = document.location.search.substr(1);

        while (match = re.exec(search)) {
            params[match[1]] = match[2];
        }

        params["filterUserId"] = selectUserList[0].options[selectUserList[0].selectedIndex].value;

        params["selectedTopicName"] = selectNameTopics[0].options[selectNameTopics[0].selectedIndex].value;

        search = '';

        for (var pKey in params) {
            if (params[pKey]) {
                if (search)
                    search += '&' + pKey + '=' + params[pKey];
                else
                    search += '?' + pKey + '=' + params[pKey];
            }
        }

        location.search = search;
    }

    $(".qme-pdf-report").click(function () {

        var sourceId = $(this).attr("source-id");
        var fromSourceId = $(this).attr("from-source-id");
        var isSwingReport = $(this).attr("source-type") === "Swing";

        var pdfUrl = "/Reports/QMEReport?sourceId=" + sourceId
            + "&isShift=" + !isSwingReport
            + "&isReceived=" + (fromSourceId === undefined ? false : true)
            + "&selectedSourceId=" + (!isSwingReport ? fromSourceId : undefined);

        window.open(pdfUrl, '_blank');
    });

    $(".pdf-report").click(function () {

        var sourceId = $(this).attr("source-id");
        var fromSourceId = $(this).attr("from-source-id");
        var isSwingReport = $(this).attr("source-type") === "Swing";

        var pdfUrl = "/Reports/HandoverReport?sourceId=" + sourceId
            + "&isShift=" + !isSwingReport
            + "&isReceived=" + (fromSourceId === undefined ? false : true)
            + "&selectedSourceId=" + (!isSwingReport ? fromSourceId : undefined);

        window.open(pdfUrl, '_blank');
    });

    $(".report-container").show();
});