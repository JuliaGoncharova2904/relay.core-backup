﻿(function () {
    //=================================================================================
    function UrlHash() {
        this.model = {};
        this.modelChangesMap = {};
        this.modelChangesHandlers = {};

        this.changeHandlers = [];

        window.addEventListener("hashchange", UrlHash_HashChangeHandler.bind(this), false);
    }
    UrlHash.prototype.getHashParams = function () {
        var match, ret = {};
        var re = /\??(.*?)=([^\&]*)&?/gi;
        var hash = document.location.hash.substr(1);

        while (match = re.exec(hash)) {
            ret[match[1]] = match[2];
        }

        return ret;
    };
    UrlHash.prototype.getHashParam = function (key) {
        return this.getHashParams()[key];
    };
    UrlHash.prototype.setHashParam = function (key, val) {
        var hash = "";
        var hashParams = this.getHashParams();
        hashParams[key] = val;

        for (var pKey in hashParams) {
            if (hashParams[pKey]) {
                if (hash)
                    hash += '&' + pKey + '=' + hashParams[pKey];
                else
                    hash += '?' + pKey + '=' + hashParams[pKey];
            }
        }

        document.location.hash = hash;
    };
    UrlHash.prototype.getHashModel = function () {
        UrlHash_AnalyseModel.call(this);
        return Object.assign({}, this.model);
    };
    UrlHash.prototype.updateHashModel = function (model) {
        var hash = "";

        for (var pKey in model) {
            if (model[pKey]) {
                if (hash)
                    hash += '&' + pKey + '=' + model[pKey];
                else
                    hash += '?' + pKey + '=' + model[pKey];
            }
        }

        document.location.hash = hash;
    };
    UrlHash.prototype.addChangeEvent = function (handler) {
        this.changeHandlers.push(handler);
    };
    UrlHash.prototype.removeChangeEvent = function (handler) {
        var index = this.changeHandlers.indexOf(handler);
        if (index !== -1) {
            this.changeHandlers.splice(index, 1);
        }
    };
    UrlHash.prototype.addParamListener = function (paramName, handler) {
        if (!this.modelChangesHandlers[paramName])
            this.modelChangesHandlers[paramName] = [];

        this.modelChangesHandlers[paramName].push(handler);
    };
    UrlHash.prototype.removeParamListener = function (paramName, handler) {
        if (this.modelChangesHandlers[paramName]) {
            var index = this.modelChangesHandlers[paramName].indexOf(handler);
            if (index !== -1) {
                this.modelChangesHandlers[paramName].splice(index, 1);
                if (this.modelChangesHandlers[paramName].length === 0) {
                    delete this.modelChangesHandlers[paramName];
                }
            }
        }
    };
    function UrlHash_HashChangeHandler() {
        //-----------------------------------------------
        UrlHash_AnalyseModel.call(this);
        //------ Execute common change handlers ---------
        for (var i = 0; i < this.changeHandlers.length; ++i) {
            this.changeHandlers[i](this.model, this.modelChangesMap);
        }
        //----- Execute specific change handlers --------
        if (Object.keys(this.modelChangesHandlers).length > 0) {
            UrlHash_ExecModelChangeEvent.call(this);
        }
        //-----------------------------------------------
    }
    function UrlHash_AnalyseModel() {
        //-----------------------------------------------
        var currentModel = this.getHashParams();
        //------------- find changes --------------------
        this.modelChangesMap = {};
        for (var propName in currentModel) {
            if (this.model[propName] !== undefined) {
                if (this.model[propName] === currentModel[propName]) {
                    this.modelChangesMap[propName] = false;
                }
                else {
                    this.modelChangesMap[propName] = true;
                }

                delete this.model[propName];
            }
            else {
                this.modelChangesMap[propName] = true;
            }
        }
        for (var propName in this.model) {
            this.modelChangesMap[propName] = true;
        }
        //------------ save new model -------------------
        this.model = currentModel;
        //-----------------------------------------------
    }
    function UrlHash_ExecModelChangeEvent() {
        for (var propName in this.modelChangesMap) {
            if (this.modelChangesMap[propName] && this.modelChangesHandlers[propName]) {
                for (var i = 0; i < this.modelChangesHandlers[propName].length; ++i) {
                    this.modelChangesHandlers[propName][i](this.model, this.modelChangesMap);
                }
            }
        }
    }
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.UrlHash = new UrlHash();
    //=================================================================================
})();