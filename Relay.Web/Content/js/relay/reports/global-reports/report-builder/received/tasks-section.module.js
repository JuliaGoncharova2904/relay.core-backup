﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  sourceId: (shift or rotation id)
    //  sourceType: (source type: Shift or Swing)
    //  readOnly: (readonly mode flag)
    //==============================  Received Tasks Section ========================================
    //------------------- Name Column Handler -----------------------------
    function NameClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var complitTaskDialog = Relay.FormDialog({
                url: "/Tasks/CompleteTaskDialog",
                data:{
                    TaskId: model.Id
                }
            });
            complitTaskDialog.setSubmitEvent(reload);
            complitTaskDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function GlobalReceivedTasksSection(sectionElement, sourceId, sourceType, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Tasks;
        var controller = sourceType === "Swing" ? "/TasksSection" : "/ShiftTasksSection";
        this.sectionUrl = controller + "/Received";
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.sourceId = sourceId;
        this.sourceType = sourceType;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        GlobalReceivedTasksSection_Initialise.call(this);
    }
    GlobalReceivedTasksSection.prototype.reload = function () {
        var _this = this;
        var isSwingReport = this.sourceType === "Swing";

        $.ajax({
            url: _this.sectionUrl,
            method: "GET",
            data: {
                shiftId: isSwingReport ? undefined : this.sourceId,
                rotationId: isSwingReport ? this.sourceId : undefined,
            }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            GlobalReceivedTasksSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    GlobalReceivedTasksSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    GlobalReceivedTasksSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function GlobalReceivedTasksSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = GlobalReceivedTasksSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function GlobalReceivedTasksSection_BuildTopic(trElement) {

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: this.sourceType === "Swing" ? ".second-td" : ".type-body"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: this.sourceType === "Swing" ? ".third-td" : ".reference-body",
            clickHandler: this.readOnly ? null : NameClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "CompleteStatus",
            elementClass: ".complite-status"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            readOnly: true,
            forTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags({}));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));

        topic.updateView();

        return topic;
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.GlobalReceivedTasksSection = function (sectionElement, sourceId, sourceType, readOnly) {
            return new GlobalReceivedTasksSection(sectionElement, sourceId, sourceType, readOnly);
        };
    }
    else {
        console.error("Received Tasks Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);