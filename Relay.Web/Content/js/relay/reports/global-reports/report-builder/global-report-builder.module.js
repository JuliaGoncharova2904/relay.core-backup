﻿$(document).ready(function () {
    "use strict";
    //===========================================================================
    function GlobalDraftReportBuilder(panelElement) {
        //-------------------------
        var _this = this;
        //--------------
        this.panelElement = panelElement;
        //--------------
        this.sourceId = $(panelElement).attr("source-id");
        this.sourecType = $(panelElement).attr("source-type");
        this.readOnly = $(panelElement).attr("read-mode") === "True";
        this.shareBtnIsActive = $(panelElement).attr("share-btn-active") === "True";
        this.reportType = $(panelElement).attr("report-type");
        this.fromSourceId = $(panelElement).attr("from-source");
        //-----------------------
        this.panelHeading = $(".panel-heading", panelElement);
        this.panelBody = $(".panel-body.main-panel", panelElement);
        this.panelContainer = $("#globalReportBuilderData", panelElement);
        this.pencilBlock = $(".pencil-block", panelElement);
        //-----------------------
        this.plusBtn = $(".plus-btn", panelElement);
        this.filterBtn = $(".filter-btn", panelElement);
        this.documentBtn = $(".document-btn", panelElement);
        this.finaliseAllBtn = $(".finalise-all-btn", panelElement);
        this.expandCollapseAllBtn = $(".expand-all-btn", panelElement);
        this.shareReportBtnController = new Relay.ShareReportBtn($(".share-report-block", panelElement));
        //------- Init Btn --------
        this.documentBtn.click(GlobalDraftReportBuilder_DocumentBtnHandler.bind(this));
        //----- Init tooltips -----
        this.panelContainer.bind("DOMNodeInserted DOMNodeRemoved", function () {
            $(".tooltipster", this).tooltipster({
                debug: false
            });
        });
        //-------------------------
        this.sections = [];
        this.IsExpanded = false;

        if (this.readOnly) {
            this.pencilBlock.hide();
            this.filterBtn.hide();
            this.finaliseAllBtn.hide();
            this.plusBtn.hide();
        }
        else {
            if (this.sourecType === "Swing") {
                this.pencilBlock.hide();
            } else {
                this.pencilBlock.show();
                this.pencilBlock.click(GlobalDraftReportBuilder_EditShiftRecipient.bind(this));
            }
            this.finaliseAllBtn.click(GlobalDraftReportBuilder_FinaliseAllBtnHandler.bind(this));
        }

        if (this.shareBtnIsActive) {
            this.shareReportBtnController.build(this.sourceId, this.sourecType);
        }
        else {
            this.shareReportBtnController.destroy();
        }

        this.expandCollapseAllBtn.click(GlobalDraftReportBuilder_ExpandCollapseAllBtnHandler.bind(this));

        GlobalDraftReportBuilder_InitSections.call(this);
    }
    GlobalDraftReportBuilder.prototype.enable = function () {
        this.panelHeading.removeAttr("style");
        this.panelBody.removeAttr("style");
    };
    GlobalDraftReportBuilder.prototype.disable = function () {
        this.panelHeading.attr("style", "pointer-events:none;");
        this.panelBody.attr("style", "pointer-events:none;");
    };

    function GlobalDraftReportBuilder_ExpandCollapseAllBtnHandler() {
        if (!this.IsExpanded) {
            for (var i = 0; i < this.sections.length; ++i) {
                this.sections[i].expand();
            }

            this.IsExpanded = true;
            this.expandCollapseAllBtn.html("Collapse All");
        }
        else {
            for (var i = 0; i < this.sections.length; ++i) {
                this.sections[i].collapse();
            }

            this.IsExpanded = false;
            this.expandCollapseAllBtn.html("Expand All");
        }
    };
    function GlobalDraftReportBuilder_FinaliseAllBtnHandler() {
        var _this = this;
        var isSwingReport = this.sourecType === "Swing";

        var confirmDialog = Relay.ConfirmDialog(
            "Are you sure you want to finalise every topic and task?",
            "",
            "Yes",
            "No"
        );

        confirmDialog.setOkEvent(function () {
            $.ajax({
                url: (isSwingReport ? "/RotationReportBuilder" : "/ShiftReportBuilder") + "/FinalizeAll",
                method: "POST",
                data: {
                    shiftId: isSwingReport ? undefined : _this.sourceId,
                    rotationId: isSwingReport ? _this.sourceId : undefined
                }
            })
           .done(function (result) {
               location.reload();
           })
           .fail(function () {
               Relay.ErrorDialog().open();
           })
        });

        confirmDialog.open();
    }
    function GlobalDraftReportBuilder_DocumentBtnHandler() {

        //var isSwingReport = this.sourecType === "Swing";

        //Relay.FormDialog({
        //    url: (isSwingReport ? "/RotationReportBuilder" : "/ShiftReportBuilder") + "/ReportPreview",
        //    data: {
        //        isReceived: this.reportType === "Received",
        //        rotationId: isSwingReport ? this.sourceId : undefined,
        //        selectedRotationId: isSwingReport ? this.fromSourceId : undefined,

        //        shiftId: isSwingReport ? undefined : this.sourceId,
        //        selectedShiftId: isSwingReport ? undefined : this.fromSourceId
        //    }
        //}).open();

        var isSwingReport = this.sourecType === "Swing";

        var pdfUrl = "/Reports/HandoverReport?sourceId=" + this.sourceId
            + "&isShift=" + !isSwingReport
            + "&isReceived=" + (this.reportType === "Received")
            + "&selectedSourceId=" + (!isSwingReport ? this.fromSourceId : undefined);

        window.open(pdfUrl, '_blank');
    };
    function GlobalDraftReportBuilder_EditShiftRecipient() {
        var formDialog = Relay.FormDialog({
            url: "/Shift/ChooseShiftRecipient",
            data: { shiftId: this.sourceId }
        });
        formDialog.setSubmitEvent(function () {
            location.reload();
        });
        formDialog.open();
    }
    function GlobalDraftReportBuilder_InitSections() {

        if (this.reportType === "Draft") {

            this.sections.push(Relay.SectionFactory.GlobalDraftCoreSection($("#coreSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalDraftProjectsSection($("#projectsSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalDraftSafetySection($("#safetySection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalDraftTasksSection($("#tasksSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalDraftTeamSection($("#teamSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalDraftTaskboardTasksSection($("#taskboardTasksSection"), this.sourceId, this.sourecType, true));


            if (this.sourecType === "Swing") {
                this.sections.push(Relay.SectionFactory.GlobalDraftDailyNotesSection($("#dailyNotesSection"), this.sourceId, this.sourecType, this.readOnly));
            }

            for (var i = 0; i < this.sections.length; ++i) {
                this.sections[i].reloadExternalSection = GlobalDraftReportBuilder_ReloadHandler.bind(this);
                if (!this.readOnly) {
                    this.sections[i].openInlineTopic = GlobalDraftReportBuilder_OpenInlineTopicHandler.bind(this);
                    this.sections[i].closeInlineTopic = GlobalDraftReportBuilder_CloseInlineTopicHandler.bind(this);
                }
            }
        }
        if (this.reportType === "Received") {

            this.sections.push(Relay.SectionFactory.GlobalReceivedCoreSection($("#coreSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalReceivedProjectsSection($("#projectsSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalReceivedSafetySection($("#safetySection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalReceivedTasksSection($("#tasksSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalReceivedTeamSection($("#teamSection"), this.sourceId, this.sourecType, this.readOnly));
            this.sections.push(Relay.SectionFactory.GlobalReceivedTaskBoardTasksSection($("#taskboardTasksSection"), this.sourceId, this.sourecType, true));


            if (this.sourecType === "Swing") {
                this.sections.push(Relay.SectionFactory.GlobalReceivedDailyNotesSection($("#dailyNotesSection"), this.sourceId, this.sourecType, this.readOnly));
            }
        }

    }
    function GlobalDraftReportBuilder_ReloadHandler(sectionType) {
        for (var i = 0; i < this.sections.length; ++i) {
            if (this.sections[i].type === sectionType)
                this.sections[i].reload();
        }
    }
    function GlobalDraftReportBuilder_OpenInlineTopicHandler() {
        this.disable();
        Relay.Header.disable();
        Relay.NavigationBar.disable();
        for (var i = 0; i < this.sections.length; ++i) {
            this.sections[i].disableTopics();
        }
    }
    function GlobalDraftReportBuilder_CloseInlineTopicHandler() {
        this.enable();
        Relay.Header.enable();
        Relay.NavigationBar.enable();

        for (var i = 0; i < this.sections.length; ++i) {
            this.sections[i].enableTopics();
        }
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.GlobalDraftReportBuilder = new GlobalDraftReportBuilder($("#globalReportBuilderPanel"));
    //===========================================================================
});