﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  sourceId: (shift or rotation id)
    //  sourceType: (source type: Shift or Swing)
    //  readOnly: (readonly mode flag)
    //==============================  Draft Team Section ============================================
    //--------------- Reference Column click handler ----------------------   
    function ReferenceColumnClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var editTeamTopicDialog = Relay.FormDialog({
                url: (this.sourceType === "Swing" ? "/TeamSection" : "/ShiftTeamSection") + "/EditTopicDialog",
                data: {
                    topic: model.Id
                }
            });
            editTeamTopicDialog.setSubmitEvent(reload);
            editTeamTopicDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function GlobalDraftTeamSection(sectionElement, sourceId, sourceType, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Team;
        var controller = sourceType === "Swing" ? "/TeamSection" : "/ShiftTeamSection";
        this.sectionUrl = controller + "/Draft";
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        this.addInlineTopicUrl = controller + "/AddInlineTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.sourceId = sourceId;
        this.sourceType = sourceType;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        this.openInlineTopic = null;
        this.closeInlineTopic = null;
        //----------------------
        GlobalDraftTeamSection_Initialise.call(this);
    }
    GlobalDraftTeamSection.prototype.reload = function () {
        var _this = this;
        var isSwingReport = this.sourceType === "Swing";

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: {
                shiftId: isSwingReport ? undefined : this.sourceId,
                rotationId: isSwingReport ? this.sourceId : undefined,
            }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            GlobalDraftTeamSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    GlobalDraftTeamSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    GlobalDraftTeamSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    GlobalDraftTeamSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    GlobalDraftTeamSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function GlobalDraftTeamSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        if (!$("tbody tr.no-items-message", this.sectionElement).length) {
            $("tbody tr", this.sectionElement).each(function () {
                var topic = GlobalDraftTeamSection_BuildTopic.call(_this, this);

                if (_this.isExpanded) {
                    topic.autoCollapseOff();
                    topic.expand();
                }

                _this.topics.push(topic);
            });
        }
        //-------
        if (this.readOnly) {
            $(".add-item-btn", this.sectionElement).parent().hide();
            
            if ($(".no-items-message", this.sectionElement).length) {
                $(".table", this.sectionElement).hide();
                $(".no-items-message", this.sectionElement).show();
            }
        } else {
            var addInlineTopic = Relay.AddInlineTopic(this.sectionElement, {
                url: this.addInlineTopicUrl,
                destId: this.sourceId,
                topicName: this.type,
                warningMessage: "Please choose a Team Member and enter a Reference. Exit?"
            });
            addInlineTopic.setSaveEvent(this.reload.bind(this));
            addInlineTopic.setOpenEvent(function () {
                if (_this.openInlineTopic)
                    _this.openInlineTopic();
            });
            addInlineTopic.setCloseEvent(function () {
                if (_this.closeInlineTopic)
                    _this.closeInlineTopic();
            });
        }
    }
    function GlobalDraftTeamSection_BuildTopic(trElement) {

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            readOnly: this.readOnly,
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.topic,
            changeHandler: GlobalDraftTeamSection_RmoveTopicHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            readOnly: this.readOnly,
            changeHandler: GlobalDraftTeamSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: this.readOnly,
            changeHandler: GlobalDraftTeamSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            readOnly: this.readOnly,
            changeHandler: GlobalDraftTeamSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "TeamMember",
            elementClass: this.sourceType === "Swing" ? ".second-td" : ".type-body"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: this.sourceType === "Swing" ? ".third-td" : ".reference-body",
            clickHandler: this.readOnly ? null : ReferenceColumnClickHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            dialogType: this.readOnly ? Relay.ManagerCommentsDialog.Types.Received : Relay.ManagerCommentsDialog.Types.Draft,
            container: Relay.ManagerCommentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags({}));
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBtn({
            dialogType: "Draft",
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
            dialogType: this.readOnly ? Relay.TasksListDialog.Types.ReadOnly : Relay.TasksListDialog.Types.Draft,
            changeHandler: GlobalDraftTeamSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: this.readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PlannedActualBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn({
            readOnly: this.readOnly
        }));

      topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopic());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopicReceived());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBody());
        topic.updateView();

        return topic;
    }
    function GlobalDraftTeamSection_TasksAndActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    function GlobalDraftTeamSection_RmoveTopicHandler(model) {
        this.reload();
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.GlobalDraftTeamSection = function (sectionElement, sourceId, sourceType, readOnly) {
            return new GlobalDraftTeamSection(sectionElement, sourceId, sourceType, readOnly);
        };
    }
    else {
        console.error("Draft Team Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);