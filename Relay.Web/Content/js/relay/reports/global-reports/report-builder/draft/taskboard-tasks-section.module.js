﻿; (function (window)
{
  //===============================================================================================
  //  sectionElement: (section div)
  //  sourceId: (shift or rotation id)
  //  sourceType: (source type: Shift or Swing)
  //  readOnly: (readonly mode flag)
  //==============================  Draft Tasks Section ===========================================
  //------------------- Name Column Handlers ----------------------------
  function NameClickHandlerEditMode(model, reload, update)
  {
    if (!model.IsNullReport)
    {
      var tasksListDialog = Relay.FormDialog({
        url: "/TaskBoard/TaskDetailsDialog",
        method: "GET",
        data: { TaskId: model.Id }
      });
    //  tasksListDialog.setCloseEvent(reload);
      tasksListDialog.open();
    }
  }
  function NameClickHandlerReadMode(model, reload, update)
  {
    if (!model.IsNullReport)
    {
      var tasksListDialog = Relay.FormDialog({
        url: "/TaskBoard/TaskDetailsDialog",
        method: "GET",
        data: { TaskId: model.Id }
      });
   //   tasksListDialog.setCloseEvent(reload);
      tasksListDialog.open();
    }
  }
  //---------------------------------------------------------------------
  function GlobalDraftTaskboardTasksSection(sectionElement, sourceId, sourceType, readOnly)
  {
    //----------------------
    this.type = Relay.SectionFactory.Types.Tasks;
    var controller = sourceType === "Swing" ? "/RotationTaskBoardTasksSection" : "/ShiftTaskBoardTasksSection";
    this.sectionUrl = controller + "/Draft";
    this.updateTopicUrl = controller + "/UpdateTask";
    this.reloadTopicUrl = controller + "/ReloadTask";
    //----------------------
    this.topics;
    this.isExpanded = false;
    this.sourceId = sourceId;
    this.sourceType = sourceType;
    this.readOnly = readOnly;
    this.sectionElement = sectionElement;
    this.reloadExternalSection = null;
    //----------------------
    GlobalDraftTaskboardTasksSection_Initialise.call(this);
  }
  GlobalDraftTaskboardTasksSection.prototype.reload = function ()
  {
    var _this = this;
    var isSwingReport = this.sourceType === "Swing";

    $.ajax({
      url: this.sectionUrl,
      method: "GET",
      data: {
        shiftId: isSwingReport ? undefined : this.sourceId,
        rotationId: isSwingReport ? this.sourceId : undefined,
      }
    })
    .done(function (newContent)
    {
      //--- update content -----
      _this.sectionElement.html($(newContent).children());
      //-- Init section ------
      GlobalDraftTaskboardTasksSection_Initialise.call(_this)
      //------------------------
    })
    .fail(function ()
    {
      var errorDialog = Relay.ErrorDialog();
      errorDialog.setCloseEvent(function ()
      {
        location.reload();
      });
      errorDialog.open();
    });
  };
  GlobalDraftTaskboardTasksSection.prototype.expand = function ()
  {
    this.isExpanded = true;
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].autoCollapseOff();
      this.topics[i].expand();
    }
  };
  GlobalDraftTaskboardTasksSection.prototype.collapse = function ()
  {
    this.isExpanded = false;
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].collapse();
      this.topics[i].autoCollapseOn();
    }
  };
  GlobalDraftTaskboardTasksSection.prototype.disableTopics = function ()
  {
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].disable();
    }
  }
  GlobalDraftTaskboardTasksSection.prototype.enableTopics = function ()
  {
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].enable();
    }
  }
  function GlobalDraftTaskboardTasksSection_Initialise()
  {
    var _this = this;
    this.topics = [];
    //-------
    $("tbody tr", this.sectionElement).each(function ()
    {
      var topic = GlobalDraftTaskboardTasksSection_BuildTopic.call(_this, this);

      if (_this.isExpanded)
      {
        topic.autoCollapseOff();
        topic.expand();
      }

      _this.topics.push(topic);
    });

    if (this.readOnly)
    {
      $(".complete-status", this.sectionElement).show();
      $(".no-complete-status", this.sectionElement).hide();
    }
  }
  function GlobalDraftTaskboardTasksSection_BuildTopic(trElement)
  {

    var topic = Relay.Topic(trElement, {
      updateUrl: this.updateTopicUrl,
      reloadUrl: this.reloadTopicUrl
    });

    topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
      readOnly: this.readOnly,
      type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.task,
      changeHandler: function () { location.reload(); }
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
      readOnly: this.readOnly,
      changeHandler: GlobalDraftTaskboardTasksSection_TopicActionsChangeHandler.bind(this)
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
      readOnly: this.readOnly,
      changeHandler: GlobalDraftTaskboardTasksSection_TopicActionsChangeHandler.bind(this)
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.TextField({
      modelPropName: "Location",
      elementClass: this.sourceType === "Swing" ? ".second-td" : ".type-body"
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.TextField({
      modelPropName: "Name",
      elementClass: this.sourceType === "Swing" ? ".third-td" : ".reference-body",
      clickHandler: this.readOnly ? NameClickHandlerReadMode : NameClickHandlerEditMode
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
      readOnly: this.readOnly
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
    topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
      readOnly: this.readOnly
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
      readOnly: this.readOnly,
      forTask: true
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
      readOnly: this.readOnly,
      forDraftTask: true
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
    topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
      dialogType: this.readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
      container: Relay.AttachmentsDialog.Containers.Task
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
      readOnly: this.readOnly
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
      dialogType: Relay.VoiceMessagesDialog.Types.Draft,
      container: Relay.VoiceMessagesDialog.Containers.Task
    }));

    topic.updateView();

    return topic;
  }
  function GlobalDraftTaskboardTasksSection_TopicActionsChangeHandler(model)
  {
    if (this.reloadExternalSection)
      this.reloadExternalSection(Relay.SectionFactory.Types[model.SectionType]);
  }
  //===============================================================================================
  if (window.Relay.SectionFactory)
  {
    window.Relay.SectionFactory.GlobalDraftTaskboardTasksSection = function (sectionElement, sourceId, sourceType, readOnly)
    {
      return new GlobalDraftTaskboardTasksSection(sectionElement, sourceId, sourceType, readOnly);
    };
  }
  else
  {
    console.error("Draft Tasks Section cannot work without SectionFactory.");
  }
  //===============================================================================================
})(window);