﻿$(document).ready(function () {

    var selectUserList = $(".received-from-title .select-list");
    var selectRotationTypeList = $(".rotation-type-title .select-list");

    selectUserList.selectpicker({
        style: 'select-class',
        size: 7,
        dropupAuto: false,
        liveSearch: true
    });

    selectRotationTypeList.selectpicker({
        style: 'select-class',
        size: 7,
        dropupAuto: false
    });

    selectUserList.bind("change", applaySearchParams);
    selectRotationTypeList.bind("change", applaySearchParams);

    function applaySearchParams() {
        var match;
        var params = {};
        var re = /\??(.*?)=([^\&]*)&?/gi;
        var search = document.location.search.substr(1);

        while (match = re.exec(search)) {
            params[match[1]] = match[2];
        }

        params["filterUserId"] = selectUserList[0].options[selectUserList[0].selectedIndex].value;
        params["filterRotationType"] = selectRotationTypeList[0].options[selectRotationTypeList[0].selectedIndex].value;

        search = '';

        for (var pKey in params) {
            if (params[pKey]) {
                if (search)
                    search += '&' + pKey + '=' + params[pKey];
                else
                    search += '?' + pKey + '=' + params[pKey];
            }
        }

        location.search = search;
    }

    //$(".pdf-report").click(function () {

    //    var sourceId = $(this).attr("source-id");
    //    var fromSourceId = $(this).attr("from-source-id");
    //    var isSwingReport = $(this).attr("source-type") === "Swing";

    //    Relay.FormDialog({
    //        url: (isSwingReport ? "/RotationReportBuilder" : "/ShiftReportBuilder") + "/ReportPreview",
    //        data: {
    //            isReceived: fromSourceId === undefined ? false : true,
    //            rotationId: isSwingReport ? sourceId : undefined,
    //            selectedRotationId: isSwingReport ? fromSourceId : undefined,

    //            shiftId: isSwingReport ? undefined : sourceId,
    //            selectedShiftId: isSwingReport ? undefined : fromSourceId
    //        }
    //    }).open();
    //});

    $(".qme-pdf-report").click(function () {

        var sourceId = $(this).attr("source-id");
        var fromSourceId = $(this).attr("from-source-id");
        var isSwingReport = $(this).attr("source-type") === "Swing";

        var pdfUrl = "/Reports/QMEReport?sourceId=" + sourceId
            + "&isShift=" + !isSwingReport
            + "&isReceived=" + (fromSourceId === undefined ? false : true)
            + "&selectedSourceId=" + (!isSwingReport ? fromSourceId : undefined);

        window.open(pdfUrl, '_blank');
    });

    $(".pdf-report").click(function () {

        var sourceId = $(this).attr("source-id");
        var fromSourceId = $(this).attr("from-source-id");
        var isSwingReport = $(this).attr("source-type") === "Swing";

        var pdfUrl = "/Reports/HandoverReport?sourceId=" + sourceId
            + "&isShift=" + !isSwingReport
            + "&isReceived=" + (fromSourceId === undefined ? false : true)
            + "&selectedSourceId=" + (!isSwingReport ? fromSourceId : undefined);

        window.open(pdfUrl, '_blank');
    });

    $(".report-container").show();
});