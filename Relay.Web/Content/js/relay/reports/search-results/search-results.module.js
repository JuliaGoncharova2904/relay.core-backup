﻿;(function () {
    //=======================================================================
    function SearchResultsB(elementContainer) {
        //--------------------
        var controller = "/ShareItem";
        this.btnPartialViewUrl = controller + "/ShareReportIcon";
        this.shareReportDialogUrl = controller + "/ShareReportFormDialog";
        //--------------------
        this.sourceId = null;
        this.sourceType = null;
        this.elementContainer = elementContainer;
        //--------------------
        this.update();
        //--------------------
    }
    ShareReportBtn.prototype.destroy = function () {
        this.sourceId = null;
        this.sourceType = null;

        this.elementContainer.empty();
    };
    ShareReportBtn.prototype.update = function () {
        var _this = this;

        if (!(this.sourceId && this.sourceType))
            return;

        $.ajax({
            url: this.btnPartialViewUrl,
            method: "POST",
            data: {
                sourceId: this.sourceId,
                sourceType: this.sourceType
            }
        })
        .done(function (newContent) {
            _this.elementContainer.html($(newContent));
            $("#shareReportBtn", _this.elementContainer).click(_this.click.bind(_this));
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ShareReportBtn.prototype.click = function () {
        if (!(this.sourceId && this.sourceType))
            return;

        var formDialog = Relay.FormDialog({
            url: "/ShareItem/ShareReportFormDialog",
            data: {
                sourceId: this.sourceId,
                sourceType: this.sourceType
            }
        });

        formDialog.setCloseEvent(this.update.bind(this));
        formDialog.open();
    };
    ShareReportBtn.prototype.build = function (sourceId, sourceType) {
        this.sourceId = sourceId;
        this.sourceType = sourceType;

        this.update();
    }
    //=======================================================================
    window.Relay = window.Relay || {};
    window.Relay.ShareReportBtn = function (elementContainer) {
        return new ShareReportBtn(elementContainer);
    }
    //=======================================================================
})();