﻿$(document).ready(function () {
    //================================================================================
    var availabilityParam = "Availability";
    var rotationTypeParam = "RotationType";
    var selectedTeamsParam = "SelectedTeams";
    //================================================================================
    var Availability = {
        Off: "Off",
        Available: "Available",
        Unavailable: "Unavailable"
    };
    var RotationType = {
        Off: "Off",
        Swing: "Swing",
        Shift: "Shift"
    };
    //================================================================================
    function SchedulerFilter(panelElement) {
        this.panelElement = panelElement;

        this.filterModel = {
            availability: Availability.Off,
            rotationType: RotationType.Off,
            allTeams: true,
            teamsIds: []
        };

        this.availabilityBtn = $(".availability-filter .filter-name", panelElement);
        this.rotationTypeBtn = $(".rotation-filter .filter-name", panelElement);
        this.teamFilterBtn = $(".team-filter .all-btn .filter-name", panelElement);
        this.teamFilterSwiper = $(".team-filter .team-swiper .swiper-wrapper", panelElement);
        this.teamFilterSlides = $(".team-filter .team-swiper .team-name", panelElement);


        var hashModel = Relay.UrlHash.getHashModel();
        SchedulerFilter_ChangeUrlHashHandler.call(this, hashModel, hashModel);

        if (!hashModel[availabilityParam]) {
            hashModel[availabilityParam] = Availability.Off;
            Relay.UrlHash.updateHashModel(hashModel);
        }
        if (!hashModel[rotationTypeParam]) {
            hashModel[rotationTypeParam] = RotationType.Off;
            Relay.UrlHash.updateHashModel(hashModel);
        }

        SchedulerFilter_InitTeamSlider.call(this);
        SchedulerFilter_InitHandlers.call(this);

        Relay.UrlHash.addChangeEvent(SchedulerFilter_ChangeUrlHashHandler.bind(this));
    }
    SchedulerFilter.prototype.applyFilter = function () {
        if (Relay.SchedulerPanels) {
            Relay.SchedulerPanels.reload(this.filterModel);
        }
    };

    function SchedulerFilter_ChangeUrlHashHandler(model, modelChangesMap) {
        if (modelChangesMap[availabilityParam]) {
            this.filterModel.availability = model[availabilityParam];

            switch (this.filterModel.availability) {
                case Availability.Off:
                    this.availabilityBtn.text("Availability");
                    this.availabilityBtn.removeClass("inactive").removeClass("active");
                    break;
                case Availability.Available:
                    this.availabilityBtn.text("Available");
                    this.availabilityBtn.removeClass("inactive").addClass("active");
                    break;
                case Availability.Unavailable:
                    this.availabilityBtn.text("Unavailable");
                    this.availabilityBtn.removeClass("active").addClass("inactive");
                    break;
            }
        }
        if (modelChangesMap[rotationTypeParam]) {
            this.filterModel.rotationType = model[rotationTypeParam];

            switch (this.filterModel.rotationType) {
                case RotationType.Off:
                    this.rotationTypeBtn.text("Working Pattern");
                    this.rotationTypeBtn.removeClass("inactive").removeClass("active");
                    break;
                case RotationType.Swing:
                    this.rotationTypeBtn.text("Swing");
                    this.rotationTypeBtn.removeClass("inactive").addClass("active");
                    break;
                case RotationType.Shift:
                    this.rotationTypeBtn.text("Shift");
                    this.rotationTypeBtn.removeClass("active").addClass("inactive");
                    break;
            }
        }
        if (modelChangesMap[selectedTeamsParam]) {
            if (model[selectedTeamsParam]) {
                var teamsIds = model[selectedTeamsParam].split('+');
                this.filterModel.allTeams = false;
                this.filterModel.teamsIds = teamsIds;

                this.teamFilterBtn.removeClass("active");
                this.teamFilterSlides.removeClass("active");
                for (var i = 0; i < teamsIds.length; ++i) {
                    $("[team-id='" + teamsIds[i] + "'] .team-name", this.teamFilterSwiper).addClass("active");
                }
            }
            else {
                this.filterModel.allTeams = true;
                this.teamFilterBtn.addClass("active");
                this.teamFilterSlides.removeClass("active");
            }
        }

        if (modelChangesMap[availabilityParam] || modelChangesMap[rotationTypeParam] || modelChangesMap[selectedTeamsParam]) {
            this.applyFilter();
        }
    }

    function SchedulerFilter_InitTeamSlider() {
        new Swiper($(".team-swiper .swiper-container", this.panelElement), {
            spaceBetween: 10,
            slidesPerView: "auto",
            touchReleaseOnEdges : true,
            freeMode: true,
            nextButton: $(".team-swiper .swiper-next", this.panelElement),
            prevButton: $(".team-swiper .swiper-prev", this.panelElement),
            breakpoints: {
                320: {
                    slidesPerView: "auto",
                    spaceBetween: 10,
                },
                480: {
                    slidesPerView: "auto",
                    spaceBetween: 10,
                },
                640: {
                    slidesPerView: "auto",
                    spaceBetween: 10,
                },
                768: {
                    slidesPerView: "auto",
                    spaceBetween: 10,
                },
                1024: {
                    slidesPerView: "auto",
                    spaceBetween: 10,
                }
            }
        });
    }

    function SchedulerFilter_InitHandlers() {
        var _this = this;

        this.availabilityBtn.click(function () {
            var hashModel = Relay.UrlHash.getHashModel();

            switch (hashModel[availabilityParam]) {
                case Availability.Off:
                    hashModel[availabilityParam] = Availability.Available;
                    Relay.UrlHash.updateHashModel(hashModel);
                    break;
                case Availability.Available:
                    hashModel[availabilityParam] = Availability.Unavailable;
                    Relay.UrlHash.updateHashModel(hashModel);
                    break;
                case Availability.Unavailable:
                    hashModel[availabilityParam] = Availability.Off;
                    Relay.UrlHash.updateHashModel(hashModel);
                    break;
            }
        });

        this.rotationTypeBtn.click(function () {
            var hashModel = Relay.UrlHash.getHashModel();

            switch (hashModel[rotationTypeParam]) {
                case RotationType.Off:
                    hashModel[rotationTypeParam] = RotationType.Swing;
                    Relay.UrlHash.updateHashModel(hashModel);
                    break;
                case RotationType.Swing:
                    hashModel[rotationTypeParam] = RotationType.Shift;
                    Relay.UrlHash.updateHashModel(hashModel);
                    break;
                case RotationType.Shift:
                    hashModel[rotationTypeParam] = RotationType.Off;
                    Relay.UrlHash.updateHashModel(hashModel);
                    break;
            }
        });

        this.teamFilterBtn.click(function () {
            var hashModel = Relay.UrlHash.getHashModel();

            if (hashModel[selectedTeamsParam]) {
                hashModel[selectedTeamsParam] = null;
                Relay.UrlHash.updateHashModel(hashModel);
            }
        });

        this.teamFilterSlides.click(function () {
            var teamId = $(this).parent().attr("team-id");
            var hashModel = Relay.UrlHash.getHashModel();

            if($(this).hasClass("active")){
                var teamsIds = hashModel[selectedTeamsParam].split('+');
                teamsIds.removeByValue(teamId);
                hashModel[selectedTeamsParam] = teamsIds.length ? teamsIds.join('+') : null;
                Relay.UrlHash.updateHashModel(hashModel);
            }
            else {
                if (hashModel[selectedTeamsParam]) {
                    var teamsIds = hashModel[selectedTeamsParam].split('+');
                    teamsIds.push(teamId);
                    hashModel[selectedTeamsParam] = teamsIds.join('+');
                }
                else {
                    hashModel[selectedTeamsParam] = teamId;
                }
                
                Relay.UrlHash.updateHashModel(hashModel);
            }
        });
    }
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.SchedulerFilter = new SchedulerFilter($(".scheduler-filter"));
    //=================================================================================
});