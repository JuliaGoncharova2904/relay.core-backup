﻿$(document).ready(function ()
{
    //================================================================================
    var selectedUserTimeline = "SelectedTimeline";
    //================================================================================
    function SchedulerPanels(panelElement)
    {
        this.panelElement = panelElement;
        this.isSimpleUserMode = $(panelElement).attr("simple-mode") === "True";
        this.userId = $(panelElement).attr("owner-id");

        SchedulerPanels_InitUserSliders.call(this, panelElement);

        if (!this.isSimpleUserMode) {
            SchedulerPanels_InitHandlers.call(this, panelElement);
            Relay.UrlHash.addChangeEvent(SchedulerPanels_ChangeUrlHashHandler.bind(this));
        }
        else {
            SchedulerPanels_InitHandlersForTimeline.call(this, $(".panel.panel-primary[user-id='" + this.userId + "']", this.panelElement));
        }
    }
    SchedulerPanels.prototype.reload = function (filterModel)
    {
        var _this = this;
        //-----------------------------------
        if (!filterModel)
            return;
        //-----------------------------------
        var loadingAnimationTimeout = setTimeout(function ()
        {
            $('#loading-animation').show();
        }, 100);
        //-----------------------------------
        $.ajax({
            url: "/Scheduler/SchedulerPanels",
            method: "POST",
            data: filterModel
        })
        .done(function (result)
        {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            _this.panelElement.html($(".scheduler-panels", div).children());
            //--------------
            SchedulerPanels_InitUserSliders.call(_this, _this.panelElement);

            if (!_this.isSimpleUserMode) {
                SchedulerPanels_InitHandlers.call(_this, _this.panelElement);
            }
            else {
                SchedulerPanels_InitHandlersForTimeline.call(this, $(".panel.panel-primary[user-id='" + _this.userId + "']", _this.panelElement));
            }
            //--------------
        })
        .fail(function ()
        {
            Relay.ErrorDialog().open();
        })
        .always(function ()
        {
            clearTimeout(loadingAnimationTimeout);
            $('#loading-animation').hide();
        });
        //-----------------------------------
    };

    function SchedulerPanels_ReloadTimelineForUser(container, userId) {
        var _this = this;

        //-----------------------------------
        if (!(userId && container))
            return;
        //-----------------------------------
        $.ajax({
                url: "/Scheduler/TimelinePanel",
                method: "POST",
                data: { userId: userId }
            })
            .done(function(result) {
                //--------------
                var div = document.createElement("div");
                div.innerHTML = result;
                //--------------
                container.html($(div).children().children());
                //--------------
                if (!_this.isSimpleUserMode) {
                    SchedulerPanels_InitHandlers.call(_this, container);
                } else {
                    SchedulerPanels_InitHandlersForTimeline.call(_this, container);
                }
                SchedulerPanels_InitUserSliders.call(_this, container);
                //--------------
            })
            .fail(function() {
                Relay.ErrorDialog().open();
            });
        //-----------------------------------
    }

    function SchedulerPanels_ChangeUrlHashHandler(model, modelChangesMap)
    {
        if (modelChangesMap[selectedUserTimeline])
        {
            $(".panel.panel-primary", this.panelElement).removeClass("active");
            $(".panel.panel-primary[user-id='" + model[selectedUserTimeline] + "']", this.panelElement).addClass("active");
        }
    }
    function SchedulerPanels_InitUserSliders(container)
    {
        $(".scheduler-container", container).each(function ()
        {
            new Swiper($(".swiper-container", this), {
                spaceBetween: 0,
                slidesPerView: "auto",
                freeMode: true,
                nextButton: $(".swiper-next", this),
                prevButton: $(".swiper-prev", this),
                breakpoints: {
                    320: {
                        slidesPerView: "auto",
                        spaceBetween: 0,
                    },
                    480: {
                        slidesPerView: "auto",
                        spaceBetween: 0,
                    },
                    640: {
                        slidesPerView: "auto",
                        spaceBetween: 0,
                    },
                    768: {
                        slidesPerView: "auto",
                        spaceBetween: 0,
                    },
                    1024: {
                        slidesPerView: "auto",
                        spaceBetween: 0,
                    }
                },
                onInit: function (swiper)
                {
                    $(swiper.slides).each(function (slideNumber)
                    {
                        if ($(this).attr("data-active") === 'True')
                        {
                            swiper.slideTo(slideNumber, 1, false);
                        }
                    }).promise().done(function ()
                    {

                    });;
                }
            });
        });
    }
    function SchedulerPanels_InitHandlers(container)
    {
        var _this = this;

        var hashModel = Relay.UrlHash.getHashModel();

        if (hashModel[selectedUserTimeline]) {
            $(".panel.panel-primary[user-id='" + hashModel[selectedUserTimeline] + "']", container).addClass("active");
        }

        $(".panel-body .scheduler-user-info", container).addClass('active');

        $(".panel-body .scheduler-user-info", container).click(function ()
        {
            var hashModel = Relay.UrlHash.getHashModel();
            var rootNode = $(this).parent().parent();

            if (rootNode.hasClass("active"))
            {
                hashModel[selectedUserTimeline] = null;
                Relay.UrlHash.updateHashModel(hashModel);
            }
            else
            {
                hashModel[selectedUserTimeline] = rootNode.attr("user-id");
                Relay.UrlHash.updateHashModel(hashModel);
            }
        });

        $(".panel-body .schedule-edit-timeline", container).click(function ()
        {
            var element = $(this).parent().parent();
            var userId = element.attr("user-id");

            var editDialog = Relay.EditSwingShiftDialog({ userId: userId });
            editDialog.setCloseEvent(function () {
                SchedulerPanels_ReloadTimelineForUser.call(_this, element, userId);
            });
            editDialog.open();
        });
    }
    function SchedulerPanels_InitHandlersForTimeline(timelineContainer) {
        var _this = this;

        $(".scheduler-user-info", timelineContainer).click(function () {
            var rootNode = $(this).parent().parent();

            if (rootNode.hasClass("active")) {
                $(rootNode).removeClass("active");
            }
            else {
                $(rootNode).addClass("active");
            }
        });

        $(".scheduler-user-info", timelineContainer).addClass('active');

        $(".schedule-edit-timeline", timelineContainer).click(function () {
            var element = $(this).parent().parent();
            var userId = element.attr("user-id");

            var editDialog = Relay.EditSwingShiftDialog({ userId: userId });
            editDialog.setCloseEvent(function () {
                SchedulerPanels_ReloadTimelineForUser.call(_this, element, userId);
            });
            editDialog.open();
        });
    }
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.SchedulerPanels = new SchedulerPanels($(".scheduler-panels"));
    //=================================================================================
});