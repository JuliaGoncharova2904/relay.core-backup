﻿; (function (window) {
    //================================================================================
  $(document).ajaxError(function (e, xhr)
  { 
        if (xhr.status === 401) {
            xhr.abort();
            window.location = "/Account/Login";
        }
        else if (xhr.status === 403) {
            xhr.abort();
            Relay.InfoDialog("You have no enough permissions to request this resource.").open();
        }
    });
    //================================================================================
    function GlobalAjaxError(finishHandler) {
        this.finishHandler = finishHandler || null;
    }
    GlobalAjaxError.prototype.handler = function (jqXHR, textStatus, errorThrown)
    {
        if (jqXHR) {
            switch (jqXHR.status)
            {
                case 402:
                    this.error_402(jqXHR);
                    break;
              case 401:
                    break;
                default:
                    this.default(jqXHR);
            }
        }
    };
    GlobalAjaxError.prototype.error_402 = function (xhr) {
        var msg = JSON.parse(xhr.responseText).message;

        var dialog = Relay.InfoDialog(msg);
        dialog.setCloseEvent(this.finishHandler);
        dialog.open();
    };
    GlobalAjaxError.prototype.default = function (xhr) {
        var errorDialog = Relay.ErrorDialog();
        errorDialog.setCloseEvent(this.finishHandler);
        errorDialog.open();
    };
    //================================================================================
    window.Relay = window.Relay || {};
    window.Relay.GlobalAjaxErrorHandler = function (finishHandler) {
        var globalAjaxError = new GlobalAjaxError(finishHandler);

        return globalAjaxError.handler.bind(globalAjaxError);
    }
    //================================================================================
})(window);