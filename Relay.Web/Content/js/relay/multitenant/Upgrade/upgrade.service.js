﻿(function ()
{
    'use strict';

    angular
         .module('UpgradeSubscriptionApp')
          .service('upgradeService', upgradeService);

    upgradeService.$inject = ['$rootScope', '$timeout', '$http', '$q'];

    function upgradeService($rootScope, $timeout, $http, $q)
    {

        var service = {
            getSubscriptionDetails: getSubscriptionDetails,
            upgradeSubscription: upgradeSubscription,
            upgradeTrialSubscription: upgradeTrialSubscription
        };

        return service;


        function getSubscriptionDetails()
        {

            var deferredObject = $q.defer();

            $http.get('/Upgrade/SubscriptionDetails').
            success(function (data)
            {
                deferredObject.resolve(data);
            }).
            error(function ()
            {
                console.log("Error Get price Constants");
            });

            return deferredObject.promise;
        }


        function upgradeSubscription(model)
        {
            var deferredObject = $q.defer();
            $http.post('/Upgrade/UpgradeSubscription', model).
            success(function (data)
            {
                deferredObject.resolve(data);
            }).
            error(function ()
            {
                console.log("Error");
            });

            return deferredObject.promise;
        }

        function upgradeTrialSubscription(model)
        {
            var deferredObject = $q.defer();
            $http.post('/Trial/PayForTrial', model).
            success(function (data)
            {
                deferredObject.resolve(data);
            }).
            error(function ()
            {
                console.log("Error");
            });

            return deferredObject.promise;
        }

    }
})();
