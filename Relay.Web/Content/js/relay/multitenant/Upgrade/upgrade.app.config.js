﻿(function ()
{
    'use strict';

    var appModule = angular.module('UpgradeSubscriptionApp');

    appModule.config(configure);

    appModule.run(run);

    /* @ngInject */
    function configure($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider)
    {
        $urlRouterProvider.otherwise(function ($injector, $location)
        {
            window.localStorage.clear();
            $location.path('/');
        });

        $httpProvider.interceptors.push('httpInterceptor');

        $stateProvider
            .state('app', {
                controller: 'UpgradeCtrl as ctrl',
                url: '/Upgrade',
                resolve: {
                    subscriptionDetails: subscriptionDetails
                }
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }


    ///////////////////////////////////////////////////////////////////////////


    subscriptionDetails.$inject = ['upgradeService'];

    function subscriptionDetails(upgradeService)
    {
        return upgradeService.getSubscriptionDetails();
    }


    ///////////////////////////////////////////////////////////////////////////


    function run($rootScope, $state, $stateParams)
    {

    }


    appModule.filter('range', function ()
    {
        return function (input, min, max)
        {
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i = min; i < max; i++)
                input.push(i);
            return input;
        };
    });

})();