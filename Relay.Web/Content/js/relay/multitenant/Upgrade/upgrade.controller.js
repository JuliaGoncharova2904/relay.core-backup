﻿(function ()
{
    'use strict';

    angular
          .module('UpgradeSubscriptionApp')
          .controller('UpgradeCtrl', UpgradeCtrl);

    UpgradeCtrl.$inject = ['$scope', '$state', '$timeout', 'stripe', 'upgradeService', 'subscriptionDetails'];

    function UpgradeCtrl($scope, $state, $timeout, stripe, upgradeService, subscriptionDetails)
    {
        /* jshint validthis:true */
        var ctrl = this;



        ctrl.Model = {
            CardNumberLast4: subscriptionDetails.CardNumberLast4,
            CardBrand: subscriptionDetails.CardBrand,
            NumberOfusers: subscriptionDetails.NumberOfusers,
            SubscriptionLevelType: 2,
            CurrentPrice: subscriptionDetails.CurrentPrice,
            NewPrice: subscriptionDetails.NewPrice,
            SubscriptionLevelPriceBasic: subscriptionDetails.SubscriptionLevelPriceBasic,
            SubscriptionLevelPricePremium: subscriptionDetails.SubscriptionLevelPricePremium,
            SubscriptionLevelPriceUltimate: subscriptionDetails.SubscriptionLevelPriceUltimate,
            UserPriceForBasicType: subscriptionDetails.UserPriceForBasicType,
            UserPriceForPremiumType: subscriptionDetails.UserPriceForPremiumType,
            UserPriceForUltimateType: subscriptionDetails.UserPriceForUltimateType,
            IsTrial: subscriptionDetails.IsTrial,
            CardNumber: subscriptionDetails.CardNumber,
            ExpDate: subscriptionDetails.ExpDate,
            CVC: subscriptionDetails.CVC,
            StripePublicKey: subscriptionDetails.StripePublicKey,
            VAT: subscriptionDetails.VAT,
            SubscriptionId: subscriptionDetails.SubscriptionId,
            Email: subscriptionDetails.Email,
            Countries: subscriptionDetails.Countries,
            UserCountry: subscriptionDetails.UserCountry
        };


        ctrl.ErrorMessage = "";

        ///////////////////////////////////////////////////////////////////////////

        ctrl.changeSubscriptionLevelType = changeSubscriptionLevelType;


        ctrl.upgradeSubscription = upgradeSubscription;

        ctrl.payAndUpgradeSubscription = payAndUpgradeSubscription;


        ///////////////////////////////////////////////////////////////////////////


        activate();

        ///////////////////////////////////////////////////////////////////////////


        function activate()
        {
        }


        function changeSubscriptionLevelType(subscriptionLevelType)
        {

            ctrl.Model.SubscriptionLevelType = subscriptionLevelType;
        }


        function upgradeSubscription()
        {
            ctrl.ErrorMessage = "";

            upgradeService.upgradeSubscription(ctrl.Model).then(function (response)
            {
                if (response.status === true)
                {
                    window.location.reload();
                }

                if (response.status === false)
                {
                    ctrl.ErrorMessage = response.data.message;
                }
            });
        }

        function payAndUpgradeSubscription(form)
        {
            if (form.$valid)
            {
                ctrl.ErrorMessage = "";

                upgradeService.upgradeTrialSubscription(ctrl.Model).then(function (response)
                {
                    if (response.status === true)
                    {
                        window.location.reload();
                    }

                    if (response.status === false)
                    {
                        ctrl.ErrorMessage = response.data.message;

                        response.data.errors.forEach(function (item, i, arr)
                        {
                            form[item.Key].$setValidity('required', false);
                        });
                    }
                });
            }
        }


        ///////////////////////////////////////////////////////////////////////////


        var unbindWatchUsersCount = $scope.$watch('ctrl.Model.NumberOfusers', function (newValue, oldValue)
        {
                ctrl.Model.SubscriptionLevelPriceBasic = ctrl.Model.UserPriceForBasicType * ctrl.Model.NumberOfusers;
                ctrl.Model.SubscriptionLevelPricePremium = ctrl.Model.UserPriceForPremiumType * ctrl.Model.NumberOfusers;
                ctrl.Model.SubscriptionLevelPriceUltimate = ctrl.Model.UserPriceForUltimateType * ctrl.Model.NumberOfusers;

                switch (ctrl.Model.SubscriptionLevelType)
                {
                    case 0:
                        ctrl.Model.NewPrice = ctrl.Model.NumberOfusers * ctrl.Model.UserPriceForBasicType;
                        break;
                    case 1:
                        ctrl.Model.NewPrice = ctrl.Model.NumberOfusers * ctrl.Model.UserPriceForPremiumType;
                        break;
                    case 2:
                        ctrl.Model.NewPrice = ctrl.Model.NumberOfusers * ctrl.Model.UserPriceForUltimateType;
                        break;
                }
        });


    }

})();


//stripe.setPublishableKey(ctrl.Model.StripePublicKey);
//ctrl.ErrorMessage = "";

//stripe.card.createToken({
//    object: 'card',
//    number: ctrl.Model.CardNumber,
//    cvc: ctrl.Model.CVC,
//    exp_month: ctrl.Model.ExpDate.split('/')[0],
//    exp_year: ctrl.Model.ExpDate.split('/')[1]
//}).then(function (response)
//{
//    console.log(response);

//}).catch(function (error)
//{
//    console.log(error);

//    ctrl.ErrorMessage = error.message;
//});
