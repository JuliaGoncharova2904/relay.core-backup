﻿(function ()
{
    'use strict';

    angular.module('UpgradeSubscriptionApp', [
        'ui.router',
        'rzModule',
        'validation.match',
        'LocalStorageModule',
        'ngSanitize',
        'credit-cards',
        'angular-stripe'
    ]);

})();

