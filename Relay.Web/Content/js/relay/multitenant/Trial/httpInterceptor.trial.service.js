﻿(function () {
    'use strict';

    angular
        .module('RelayMultiTenantAppTrial')
        .service('httpInterceptor.trial', httpInterceptor);

    httpInterceptor.$inject = ['$rootScope', '$q', '$timeout', '$injector'];

    function httpInterceptor($rootScope, $q, $timeout, $injector) {

        var loadingAnimationTimeout;

        ///////////////////////////////////////////////////////////////////////////

        var service = {

            request: request,
            response: response,
            responseError: responseError
        };

        return service;

        ///////////////////////////////////////////////////////////////////////////

        function request(config) {
            $('#loading-animation').show();

            return config;
        }

        function response(response) {
            $('#loading-animation').hide();

            return response;
        }

        function responseError(rejection) {
            $('#loading-animation').hide();

            return $q.reject(rejection);
        }



    }
})();
