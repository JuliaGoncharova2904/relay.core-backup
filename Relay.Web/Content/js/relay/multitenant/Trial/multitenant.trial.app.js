﻿(function () {
    'use strict';

    angular.module('RelayMultiTenantAppTrial', [
        'ui.router',
        'rzModule',
        'validation.match',
        'LocalStorageModule',
        'ngSanitize',
        'credit-cards'
    ]);

})();

