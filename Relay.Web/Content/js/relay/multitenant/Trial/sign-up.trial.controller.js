﻿(function ()
{
    'use strict';

    angular
        .module('RelayMultiTenantAppTrial')
        .controller('SignUpCtrl', SignUpCtrl);

    SignUpCtrl.$inject = ['$scope', '$state', '$timeout', 'signUpService', 'localStorageService', 'priceData'];

    function SignUpCtrl($scope, $state, $timeout, signUpService, localStorageService, priceData)
    {
        /* jshint validthis:true */
        var ctrl = this;

        ///////////////////////////////////////////////////////////////////////////

        var priceConstants = priceData;

        var defaultModel = {
            //Email: 'test@test.com',
            //Name: 'Test',
            //Surname: 'Test',
            //Company: 'Test',
            //Position: 'Test',
            //Password: 'johnDoe123!',
            //PasswordConfirm: 'johnDoe123!',
            //CardNumber: '4242424242424242',
            //NameOnCard: 'Test',
            //StartDate: '07/15',
            //ExpDate: '07/20',
            //IssueNumber: '',
            //CVC: '888',
            BillingPeriod: 0,
            SimpleUsersCount: 5,
            UserSubscriptionType: null,
            SubscriptionLevelType: 2,
            IsAgree: false
        }

        $scope.getSelectClass = function () {
            return defaultModel.SubscriptionLevelType == 2 ? 'selected' : '';
        }

        ///////////////////////////////////////////////////////////////////////////

        ctrl.SignUpModel = {
            UserSubscriptionPrices: {
                SinglePrice: priceConstants.User.Basic + priceConstants.Manager.BasicFirst,
                B2bPrice: priceConstants.User.Basic * 2 + priceConstants.Manager.BasicFirst,
                TeamPrice: priceConstants.User.Basic * 5 + priceConstants.Manager.BasicFirst
            },
            SubscriptionLevelPrices: {
                Basic: priceConstants.User.Basic + priceConstants.Manager.BasicFirst,
                Premium: priceConstants.User.Premium + priceConstants.Manager.PremiumFirst,
                Ultimate: priceConstants.User.Ultimate + priceConstants.Manager.UltimateFirst
            },
            Form: localStorageService.get('SignUpModel') || defaultModel
        };

        ctrl.CurrentStepNumber = localStorageService.get('CurrentStepNumber') || 0;

        ctrl.ErrorMessage = "";

        ///////////////////////////////////////////////////////////////////////////

        ctrl.signUp = signUp;

        ctrl.changeStep = changeStep;

        ctrl.changeBillingPeriod = changeBillingPeriod;

        ctrl.goToSecondStep = goToSecondStep;

        ctrl.changeUserSubscriptionType = changeUserSubscriptionType;

        ctrl.changeSubscriptionLevelType = changeSubscriptionLevelType;


        ///////////////////////////////////////////////////////////////////////////

        activate();

        ///////////////////////////////////////////////////////////////////////////

        function activate()
        {
        }

        function signUp(form)
        {
            if (form.$valid)
            {
                ctrl.ErrorMessage = "";

                signUpService.registration(ctrl.SignUpModel.Form).then(function (response)
                {
                    if (response.status === true)
                    {
                        localStorageService.remove('SignUpModel');
                        localStorageService.remove('CurrentStepNumber');
                        ctrl.SignUpModel.Form = null;

                        unbindWatchSimpleUsersCount();
                        unbindWatchManagersUsersCount();

                        $state.go('login');
                    }

                    if (response.status === false)
                    {
                        ctrl.ErrorMessage = response.data.message;

                        response.data.errors.forEach(function (item, i, arr)
                        {
                            form[item.Key].$setValidity('required', false);
                        });
                    }
                });

            }
        }

        function changeUserSubscriptionType(subscriptionType)
        {
            ctrl.SignUpModel.Form.UserSubscriptionType = subscriptionType;

            localStorageService.set('SignUpModel', getDataForSave());

        }

        function changeSubscriptionLevelType(subscriptionLevelType)
        {

            ctrl.SignUpModel.Form.SubscriptionLevelType = subscriptionLevelType;

            localStorageService.set('SignUpModel', getDataForSave());
        }


        function changeBillingPeriod(type)
        {
            ctrl.SignUpModel.Form.BillingPeriod = type;
            localStorageService.set('SignUpModel', getDataForSave());
        }


        function changeStep(stepNumber)
        {
            ctrl.CurrentStepNumber = stepNumber;

            localStorageService.set('CurrentStepNumber', stepNumber);
        }

        function goToSecondStep()
        {
            ctrl.CurrentStepNumber = 1;

            if (ctrl.SignUpModel.Form.UserSubscriptionType === 0)
            {
                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic + priceConstants.Manager.BasicFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium + priceConstants.Manager.PremiumFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate + priceConstants.Manager.UltimateFirst;
            }

            if (ctrl.SignUpModel.Form.UserSubscriptionType === 1)
            {
                ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * 2 + priceConstants.Manager.BasicFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * 2 + priceConstants.Manager.PremiumFirst;
                ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * 2 + priceConstants.Manager.UltimateFirst;
            }

            localStorageService.set('SignUpModel', getDataForSave());

            localStorageService.set('CurrentStepNumber', 1);
        }


        ///////////////////////////////////////////////////////////////////////////

        $scope.$on("$destroy", function ()
        {
            window.localStorage.clear();
        });

        ///////////////////////////////////////////////////////////////////////////


        var unbindWatchManagersUsersCount = $scope.$watch('ctrl.SignUpModel.Form.ManagersUsersCount', function ()
        {
            ctrl.SignUpModel.UserSubscriptionPrices.TeamPrice = priceConstants.User.Basic * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.BasicFirst;
            ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.BasicFirst;
            ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.PremiumFirst;
            ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.UltimateFirst;

            localStorageService.set('SignUpModel', getDataForSave());
        });


        var unbindWatchSimpleUsersCount = $scope.$watch('ctrl.SignUpModel.Form.SimpleUsersCount', function ()
        {
            ctrl.SignUpModel.UserSubscriptionPrices.TeamPrice = priceConstants.User.Basic * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.BasicFirst;
            ctrl.SignUpModel.SubscriptionLevelPrices.Basic = priceConstants.User.Basic * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.BasicFirst;
            ctrl.SignUpModel.SubscriptionLevelPrices.Premium = priceConstants.User.Premium * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.PremiumFirst;
            ctrl.SignUpModel.SubscriptionLevelPrices.Ultimate = priceConstants.User.Ultimate * ctrl.SignUpModel.Form.SimpleUsersCount + priceConstants.Manager.UltimateFirst;

            localStorageService.set('SignUpModel', getDataForSave());
        });


        function getDataForSave()
        {
            var data = angular.copy(ctrl.SignUpModel.Form);

            data.Password = '',
                data.PasswordConfirm = '';

            return data;
        }
    }

})();
