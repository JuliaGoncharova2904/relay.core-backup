﻿(function ()
{
    'use strict';

    var appModule = angular.module('RelayMultiTenantAppTrial');

    appModule.config(configure);

    appModule.run(run);

    /* @ngInject */
    function configure($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider)
    {

        $urlRouterProvider.otherwise(function ($injector, $location)
        {
            window.localStorage.clear();
            $location.path('/step1');
        });

        $httpProvider.interceptors.push('httpInterceptor.trial');

        $stateProvider
            .state('app', {
                abstract: true,
                controller: 'SignUpCtrl as ctrl',
                resolve: {
                    priceData: priceData
                }
            })
            .state('app.step1', {
                url: '/step1',
                templateUrl: '/Trial/Step1'
            })
            .state('app.step2', {
                url: "/step2",
                templateUrl: '/Trial/Step2'
            })
            .state('app.step3', {
                url: "/step3",
                templateUrl: '/Trial/Step3'
            })
            .state('login', {
                url: '/login',
                templateUrl: '/Trial/Login',
                resolve: {
                    priceData: priceData
                }
            });

    }


    ///////////////////////////////////////////////////////////////////////////


    priceData.$inject = ['signUpService'];

    function priceData(signUpService)
    {
        return signUpService.getPriceConstants();
    }


    ///////////////////////////////////////////////////////////////////////////


    function run($rootScope, $state, $stateParams)
    {

    }

})();