﻿(function () {
    'use strict';

    angular
        .module('RelayMultiTenantApp')
        .service('signUpService', signUpService);

    signUpService.$inject = ['$rootScope', '$timeout', '$http', '$q'];

    function signUpService($rootScope, $timeout, $http, $q) {

        var service = {
            getPriceConstants: getPriceConstants,
            registration: registration
        };

        return service;


        function getPriceConstants() {

            var deferredObject = $q.defer();

            $http.post('/Registration/PriceConstants').
                success(function (data) {

                    var priceConstants = {
                        Manager: {
                            BasicFirst: data.FirstManagerPriceForBasicType,
                            PremiumFirst: data.FirstManagerPriceForPremiumType,
                            UltimateFirst: data.FirstManagerPriceForUltimateType,
                            AdditionalManager: data.AdditionalManagerPrice
                        },
                        User: {
                            Basic: data.UserPriceForBasicType,
                            Premium: data.UserPriceForPremiumType,
                            Ultimate: data.UserPriceForUltimateType
                        },
                        Countries: data.Countries,
                        TimeZones: data.TimeZones

                    }

                    deferredObject.resolve(priceConstants);
                }).
                error(function () {
                    console.log("Error Get price Constants");
                });

            return deferredObject.promise;
        }


        function registration(model) {
            var deferredObject = $q.defer();
            $http.post('/Registration/SignUp', model).
                success(function (data) {
                    deferredObject.resolve(data);
                }).
                error(function (data) {
                    console.log("Error");
                });
            return deferredObject.promise;
        }

    }
})();
