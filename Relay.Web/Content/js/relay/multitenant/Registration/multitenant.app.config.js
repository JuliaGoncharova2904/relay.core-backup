﻿(function ()
{
    'use strict';

    var appModule = angular.module('RelayMultiTenantApp');

    appModule.config(configure);

    appModule.run(run);

    /* @ngInject */
    function configure($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider)
    {
        $urlRouterProvider.otherwise(function ($injector, $location)
        {
            window.localStorage.clear();
            $location.path('/step1');
        });

        $httpProvider.interceptors.push('httpInterceptor');

        $stateProvider
            .state('app', {
                abstract: true,
                controller: 'SignUpCtrl as ctrl',
                resolve: {
                    priceData: priceData
                }
            })      
            .state('app.step1', {
                url: '/step1',
                templateUrl: '/Registration/Step1'
            })
            .state('app.step2', {
                url: "/step2",
                templateUrl: '/Registration/Step2'
            })
            .state('app.step3', {
                url: "/step3",
                templateUrl: '/Registration/Step3'
            })
            .state('app.step4', {
                url: "/step4",
                templateUrl: '/Registration/Step4'
            });


        //$locationProvider.html5Mode(true);

    }


    ///////////////////////////////////////////////////////////////////////////


    priceData.$inject = ['signUpService'];

    function priceData(signUpService)
    {
        return signUpService.getPriceConstants();
    }


    ///////////////////////////////////////////////////////////////////////////


    function run($rootScope, $state, $stateParams)
    {

    }

})();