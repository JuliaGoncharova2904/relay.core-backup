﻿(function ()
{
    'use strict';

    angular.module('RelayMultiTenantApp', [
        'ui.router',
        'rzModule',
        'validation.match',
        'LocalStorageModule',
        'ngSanitize',
        'credit-cards'
    ]);

})();

