﻿; (function () {
    "use strict";
    //================== Notification click handler module =====================
    function AddNotificationClickHandlers(msgContainer) {
        $(".edit-shift-rotation", msgContainer).unbind("click").bind("click", OpenEditShiftRotationDatesDialog);
        $(".load-checklist", msgContainer).unbind("click").bind("click", LoadChecklistPage);
        $(".load-checklist-outbox", msgContainer).unbind("click").bind("click", LoadChecklistAssignedSectionPage);
        $(".load-report-builder", msgContainer).unbind("click").bind("click", LoadReportBuilderPage);
        $(".load-report-builder-view-mode", msgContainer).unbind("click").bind("click", LoadReportBuilderPageViewMode);
        $(".edit-task", msgContainer).unbind("click").bind("click", OpenEditTaskFormDialog);
        $(".complete-task", msgContainer).unbind("click").bind("click", OpenCompleteTaskFormDialog);
        $(".view-task", msgContainer).unbind("click").bind("click", OpenTaskDetailsFormDialog);
        $(".load-report-builder-task-section-inBox", msgContainer).unbind("click").bind("click", LoadReportBuilderPageInBoxViewMode);
        $(".load-report-builder-task-section-inBoxNowTask", msgContainer).unbind("click").bind("click", LoadReportBuilderPageInBoxNowTaskViewMode);
        $(".load-shared-topics", msgContainer).unbind("click").bind("click", LoadSharedTopicsPage);
        $(".load-shared-reports", msgContainer).unbind("click").bind("click", LoadSharedReportsPage);
        $(".view-task", msgContainer).unbind("click").bind("click", LoadGlobalReportPage);

        $(".load-swing-report-builder", msgContainer).unbind("click").bind("click", LoadSwingReceivedReportBuilderPage);
        $(".load-shift-report-builder", msgContainer).unbind("click").bind("click", LoadShiftReportBuilderPageViewMode);
    }



    function LoadSwingReceivedReportBuilderPage() {
        var relationId = getRelationId(this);
        var rotationId = getSwingId(this);
        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            $('#loading-animation').show();
            location.href = "/Rotation/" + relationId + "/Summary" + "#?ReportsType=HistoryPanel&HistorySelectedRotationId=" + rotationId
        }
        else {
            isNotExistDialog();
        }
    }



 function LoadGlobalReportPage() {
        $('#loading-animation').show();
        $(this).hasClass("unread") && MakeNotificationOpened(this);
        location.href = "/GlobalTemplates/Index";
    }

    function MakeNotificationOpened(notifyElement) {
        $.ajax({
            url: "/Notification/MakeNotificationOpened",
            method: "POST",
            data: { notificationId: $(notifyElement).attr("notification-id") }
        })
        .done(function () {
            $(notifyElement).removeClass("unread")
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    }

    function getSwingId(element) {
        var swingId = $(element).attr("swing-id");

        return (swingId === "00000000-0000-0000-0000-000000000000") ? null : swingId;
    }

    function getShiftId(element) {
        var shiftId = $(element).attr("shift-id");

        return (shiftId === "00000000-0000-0000-0000-000000000000") ? null : shiftId;
    }

    function getRelationId(element) {
        var relationId = $(element).attr("relation-id");

        return (relationId === "00000000-0000-0000-0000-000000000000") ? null : relationId;
    }

    function isNotExistDialog() {
        Relay.InfoDialog("This notification link is no longer available.").open();
    }

    //--------------------------------------------------------------------------

    function OpenEditShiftRotationDatesDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            var editDialog = Relay.EditSwingShiftDialog({ userId: relationId });
            editDialog.setCloseEvent(function () { location.reload(); });
            editDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }

    function LoadChecklistPage() {
        $('#loading-animation').show();
        $(this).hasClass("unread") && MakeNotificationOpened(this);
        location.href = "/TaskBoard";
    }

    function LoadChecklistAssignedSectionPage() {
        $('#loading-animation').show();
        $(this).hasClass("unread") && MakeNotificationOpened(this);
        location.href = "/TaskBoard/AssignedSection";
    }

    function LoadSharedTopicsPage()
    {
      $('#loading-animation').show();
      $(this).hasClass("unread") && MakeNotificationOpened(this);
      location.href = "/GlobalReports/SharedTopics";
    }

    function LoadSharedReportsPage() {
        $('#loading-animation').show();
        $(this).hasClass("unread") && MakeNotificationOpened(this);
        location.href = "/GlobalReports/SharedReport";
    }


    function LoadReportBuilderPage() {
        $('#loading-animation').show();
        $(this).hasClass("unread") && MakeNotificationOpened(this);
        location.href = "/Summary";
    }

    function LoadReportBuilderPageInBoxViewMode() {
        var relationId = getRelationId(this);
        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            $('#loading-animation').show();
            location.href = "/TaskBoard/ReceivedSection"
        }
        else {
            isNotExistDialog();
        }
    }

    function LoadReportBuilderPageInBoxNowTaskViewMode() {
        var relationId = getRelationId(this);
        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            $('#loading-animation').show();
            location.href = "/Rotation/" + relationId + "/TaskBoard/ReceivedSection"
        }
        else {
            isNotExistDialog();
        }
    }

    function LoadReportBuilderPageViewMode() {
        var relationId = getRelationId(this);
        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            $('#loading-animation').show();
            location.href = "/Rotation/" + relationId + "/Summary"
        }
        else {
            isNotExistDialog();
        }
    }

    function LoadShiftReportBuilderPageViewMode() {
        var relationId = getRelationId(this);
        var shiftId = getShiftId(this);
        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            $('#loading-animation').show();
            location.href = "/Rotation/" + relationId + "/Summary" + "#?DailyReportShiftId=" + shiftId + "&ReportsType=DraftPanel"
        }
        else {
            isNotExistDialog();
        }
    }

    function OpenEditTaskFormDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if(relationId){
            var tasksListDialog = Relay.FormDialog({
                url: "/Notification/EditTaskDialog",
                data: { TaskId: relationId }
            });
            tasksListDialog.setSubmitEvent(function () {
                location.reload();
            });
            tasksListDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }

    function OpenCompleteTaskFormDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if(relationId){
            var tasksListDialog = Relay.FormDialog({
                url: "/Notification/CompleteTaskDialog",
                data: { TaskId: relationId }
            });
            tasksListDialog.setSubmitEvent(function () {
                location.reload();
            });
            tasksListDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }

    function OpenTaskDetailsFormDialog() {
        var relationId = getRelationId(this);

        $(this).hasClass("unread") && MakeNotificationOpened(this);

        if (relationId) {
            var tasksListDialog = Relay.FormDialog({
                url: "/Notification/TaskDetailsDialog",
                data: { TaskId: relationId }
            });
            tasksListDialog.setSubmitEvent(function () {
                location.reload();
            });
            tasksListDialog.open();
        }
        else {
            isNotExistDialog();
        }
    }
    //==========================================================================
    window.Relay = window.Relay || {};
    window.Relay.AddNotificationClickHandlers = AddNotificationClickHandlers;
    //==========================================================================
})();