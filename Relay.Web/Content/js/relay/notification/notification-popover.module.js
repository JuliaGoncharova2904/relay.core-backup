﻿; (function () {
    "use strict";
    //=============== Notification popover module ============================
    var NotificationTypes = {
        PossibilityToExtendSwing: "edit-rotation",
        HandbackTasksStillComplete: "load-checklist",
        TasksStillComplete: "load-checklist",
        ManagerAddedComment: "load-report-builder",
        AssignedTasks: "load-checklist",
        AssignedHandbackTasks: "load-checklist",
        TeamMemberHandedOver: "load-report-builder-view-mode",
        ManagerReassignsHandover: "load-report-builder",
        ManagerReassignsTask: "edit-task",
        AssignedNowTaskForYou: "complete-task",
        YourNowTaskHasNotCompleted: "edit-task",
        YouHaveNowTaskToComplete: "complete-task",
        CreatorModifiedDueDate: "complete-task",
        CreatorReassignedNowTask: "load-checklist",
        CreatorDeletedNowTask: "load-checklist"
    };
    //-------------------------------------------------------------------
    function NotificationPopover(notifyElement, popoverId) {
        this.popoverId = popoverId;
        //-------------
        this.isOpened = false;
        this.msgContainer;
        this.notifyElement = notifyElement;
        //-------------
        this.timestampAnimation = new TimeshtampAnimation();
        this.notificationPagination = new NotificationPagination(this.timestampAnimation);
        //-------------
        notifyElement.webuiPopover({
            hideEmpty: true,
            style: 'notification',
            trigger: 'click',
            multi: false,
            dismissible: true,
            type: "async",
            cache: false,
            async: {
                type: 'GET',
                success: NotificationPopover_ShowHandler.bind(this)
            },
            url: "/Notification/GetPopover",
            onHide: NotificationPopover_HideHandler.bind(this)
        });
        //-------------
        this.resizeHandler = function () {
            WebuiPopovers.hide(notifyElement);
        }.bind(this);
        //-------------
    }
    NotificationPopover.prototype.AddNewNotification = function (notification) {
        if (this.isOpened && this.msgContainer) {
            var notifyDiv = document.createElement("div");

            notifyDiv.className = "popover-item row no-margin " + NotificationTypes[notification.Type]
            if (!notification.isOpened) {
                notifyDiv.className += " unread";
            }

            notifyDiv.setAttribute("notification-id", notification.Id);
            notifyDiv.setAttribute("relation-id", notification.RelationId);
            notifyDiv.setAttribute("shift-id", notification.ShiftId);
            notifyDiv.setAttribute("swing-id", notification.SwingId);

            notifyDiv.innerHTML = "<div class=\"notification-image col-lg-2 col-md-2 col-sm-2 col-xs-2\">" +
                                        "<img src=\"" + notification.UrlImage + "\" />" +
                                    "</div>" +
                                    "<div class=\"col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding\">" +
                                        "<div class=\"notification-message\">" + notification.Message + "</div>" +
                                        "<div class=\"notification-timestapm\" data-time=\"" + notification.Date + "\"></div>" +
                                    "</div>";

            var container = this.msgContainer[0];
            var firstChild = container.childNodes[0];
            container.insertBefore(notifyDiv, firstChild);

            this.timestampAnimation.updateAnimation(this.msgContainer);
            Relay.AddNotificationClickHandlers(this.msgContainer);
            MakeLastNotificationsShown();
        }
    };

    function NotificationPopover_ShowHandler() {
        if (!this.isOpened) {
            var _this = this;

            this.msgContainer = $("#" + this.popoverId + " .messege-content");
            if (this.msgContainer.children().length > 6) {
                $("#" + this.popoverId + " .see-all").css("display", "block");
            }

            this.msgContainer.click(function () {
                WebuiPopovers.hide(_this.notifyElement);
            });

            this.isOpened = true;
            this.timestampAnimation.runAnimation(this.msgContainer);
            this.notificationPagination.runPagination(this.msgContainer);
            
            Relay.AddNotificationClickHandlers(this.msgContainer);
            MakeLastNotificationsShown();

            window.addEventListener("resize", this.resizeHandler, false);
        }
    }

    function NotificationPopover_HideHandler() {
        if (this.isOpened) {
            this.isOpened = false;
            this.timestampAnimation.stopAnimation();
            this.notificationPagination.stopPagination();

            window.removeEventListener("resize", this.resizeHandler, false);
        }
    }

    function MakeLastNotificationsShown() {
        $.ajax({
            url: "/Notification/MakeLastNotificationsShown",
            method: "POST"
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    }
    //================ Notification timestamp animation ======================
    function TimeshtampAnimation() {
        this.timestamps;
        this.updateIntervalObj;
        //---- Set moment js config ----
        moment.locale("en");
        moment.localeData('en')._relativeTime.s = "%d seconds ago";
        moment.localeData('en')._relativeTime.mm = "%d minutes ago";
    }
    TimeshtampAnimation.prototype.runAnimation = function (msgContainer) {
        var _this = this;
        this.timestamps = $(".notification-timestapm", msgContainer);
        if (!this.updateIntervalObj) {
            this.updateIntervalObj = setInterval(function () {
                UpdateTimeshtamps(_this.timestamps);
            }, 1000);
        }
    };
    TimeshtampAnimation.prototype.updateAnimation = function (msgContainer) {
        this.timestamps = $(".notification-timestapm", msgContainer);
    };
    TimeshtampAnimation.prototype.stopAnimation = function () {
        if (this.updateIntervalObj) {
            clearInterval(this.updateIntervalObj);
            this.updateIntervalObj = null;
        }
    };

    function UpdateTimeshtamps(timestamps) {
        var timesArray = [];

        for (var i = 0 ; i < timestamps.length; i++) { timesArray.push($(timestamps[i]).attr("data-time")); }

        if (timesArray.length == timestamps.length) {
            for (var i = 0; i < timestamps.length; i++) {
                var momentTime = TimeToMoment(timesArray[i]);
                $(timestamps[i]).text(momentTime);
            }
        }
    }

    function TimeToMoment(timeValue) {
        var dateNow = moment();

        var dateObject = moment(timeValue);

        if (dateNow.format("YYYY") != dateObject.format("YYYY")) {
            return dateObject.format("D MMMM YYYY [at] h:mm a");
        }
        else if (dateNow.diff(dateObject, "days") >= 6) {
            return dateObject.format("D MMMM [at] h:mm a");
        }
        else if (dateNow.diff(dateObject, "days") > 1) {
            return dateObject.format("dddd [at] h:mm a");
        }
        else if (dateNow.diff(dateObject.format("D MMMM YYYY"), "days") == 1) {
            return "Yesterday at " + dateObject.format("h:mm a");
        }
        else if (dateNow.format("D MMMM YYYY") == dateObject.format("D MMMM YYYY") && dateNow.diff(dateObject, "hours") > 6) {
            return "Today at " + dateObject.format("h:mm a");
        }
        else if (dateNow.diff(dateObject, "hours") <= 6) {
            return dateObject.fromNow("hh");
        }
        else if (dateNow.diff(dateObject, "minutes") < 59) {
            return dateObject.fromNow("mm");
        }
        else if (dateNow.diff(dateObject, "minutes") <= 1) {
            return dateObject.fromNow("s");
        }
    }
    //================= Notification scroll pagination =======================
    function NotificationPagination(timeshtampAnimation) {
        this.page = 0;
        this.pageSize = 50;
        this.msgHeight = 0;
        this.msgContainer;
        this.timeshtampAnimation = timeshtampAnimation;
    }
    NotificationPagination.prototype.runPagination = function (msgContainer) {
        this.msgContainer = msgContainer;
        NotificationPagination_UpdateMessagesHeight.call(this);
        this.msgContainer.bind("scroll", $.debounce(100, NotificationPagination_ScrollHandler.bind(this)));
    };
    NotificationPagination.prototype.stopPagination = function () {
        this.msgContainer.unbind("scroll");
        this.msgContainer = null;
    };

    function NotificationPagination_UpdateMessagesHeight() {
        this.msgHeight = this.msgContainer.children().length * 59;
    }

    function NotificationPagination_ScrollHandler() {
        var scrollTop = $(this.msgContainer).scrollTop();
        var height = $(this.msgContainer).height();

        if (scrollTop + height > this.msgHeight - 400) {
            NotificationPagination_LoadNextPage.call(this);
        }
    }

    function NotificationPagination_LoadNextPage() {
        var _this = this;

        $.ajax({
            url: "/Notification/GetNotificationMessageForPopover/",
            method: "GET",
            data: {
                page: ++_this.page,
                pageSize: _this.pageSize
            }
        })
            .done(function (newContent) {
            if (newContent.trim()) {
                var content = $(newContent);
                _this.msgContainer.append(content);
                Relay.AddNotificationClickHandlers(_this.msgContainer);
                _this.timeshtampAnimation.updateAnimation(_this.msgContainer);
                NotificationPagination_UpdateMessagesHeight.call(_this);
            }
            else {
                _this.msgContainer.unbind("scroll");
            }
        });
    }
    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.NotificationPopover = function (notifyElement, popoverId) {
        return new NotificationPopover(notifyElement, popoverId);
    };
    //========================================================================
})();