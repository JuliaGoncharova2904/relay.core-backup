﻿$(document).ready(function () {
    "use strict";
    //================== Notification module =================================
    var popoverObj;
    var countNode = $(".notification-container .count");
    var notificationNode = $(".notification-container .notification");
    var notificationPopover = Relay.NotificationPopover(notificationNode, "notificationPopover");
    //---- SignalR ------
    var notificationHub = $.connection.notificationHub;
    $.connection.hub.logging = false;
    $.connection.hub.start();
    notificationHub.client.UpdateNotificationInfo = UpdateIconState;
    notificationHub.client.AddNewNotification = notificationPopover.AddNewNotification.bind(notificationPopover);
    //-------------------
    function UpdateIconState(msgNumber) {
        if (msgNumber) {
            notificationNode.removeClass("notification-inactive").addClass("notification-active");

            countNode.text(msgNumber);
            if (msgNumber > 99) {
                countNode.text("99+");
            }
        }
        else {
            notificationNode.removeClass("notification-active").addClass("notification-inactive");
            countNode.text('');
        }
    }

    UpdateIconState(Number($(".notification-container").attr("msg-count")));
    //========================================================================
});