﻿; (function (window) {
    //=================================================================================
    var sortOptions = {
        Default: "Default",
        PriorityHighToLow: "PriorityHighToLow",
        PriorityLowToHigh: "PriorityLowToHigh",
        DueDateHighToLow: "DueDateHighToLow",
        DueDateLowToHigh: "DueDateLowToHigh"
    };

    function TaskSort(panel, hashParams) {
        this.hashParams = hashParams;
        this.priorityBtn = $(".priority", panel);
        this.deadlineBtn = $(".deadline", panel);

        this.priorityBtn.on("click", function () {
            var hashModel = Relay.UrlHash.getHashModel();
            switch (hashModel[hashParams.SortOptions]) {
                case sortOptions.PriorityLowToHigh:
                    hashModel[hashParams.SortOptions] = sortOptions.PriorityHighToLow;
                    break;
                case sortOptions.PriorityHighToLow:
                    hashModel[hashParams.SortOptions] = sortOptions.Default;
                    break;
                case sortOptions.Default:
                default:
                    hashModel[hashParams.SortOptions] = sortOptions.PriorityLowToHigh;
                    break;
            }
            //hashModel[hashParams.Page] = 1;
            Relay.UrlHash.updateHashModel(hashModel);
        });

        this.deadlineBtn.on("click", function () {
            var hashModel = Relay.UrlHash.getHashModel();
            switch (hashModel[hashParams.SortOptions]) {
                case sortOptions.DueDateLowToHigh:
                    hashModel[hashParams.SortOptions] = sortOptions.DueDateHighToLow;
                    break;
                case sortOptions.DueDateHighToLow:
                    hashModel[hashParams.SortOptions] = sortOptions.Default;
                    break;
                case sortOptions.Default:
                default:
                    hashModel[hashParams.SortOptions] = sortOptions.DueDateLowToHigh;
                    break;
            }
            //hashModel[hashParams.Page] = 1;
            Relay.UrlHash.updateHashModel(hashModel);
        });

        this.hashChangeHandler = function (model, modelChangesMap) {
            if (modelChangesMap[hashParams.SortOptions]) {
                TaskSort_UpdateIcons.call(this, model[hashParams.SortOptions]);
            }
        }.bind(this);

        Relay.UrlHash.addChangeEvent(this.hashChangeHandler);

        var hashModel = Relay.UrlHash.getHashModel();
        TaskSort_UpdateIcons.call(this, hashModel[hashParams.SortOptions]);
    }
    TaskSort.prototype.dispose = function () {
        Relay.UrlHash.removeChangeEvent(this.hashChangeHandler);
        var hashModel = Relay.UrlHash.getHashModel();
        hashModel[this.hashParams.SortOptions] = null;
        Relay.UrlHash.updateHashModel(hashModel);
    };
    function TaskSort_UpdateIcons(sortOption) {
        $(".priority-sort-icon", this.priorityBtn).removeClass("low-to-high").removeClass("high-to-low");
        $(".due-date-sort-icon", this.deadlineBtn).removeClass("low-to-high").removeClass("high-to-low");

        switch (sortOption) {
            case sortOptions.PriorityLowToHigh:
                $(".priority-sort-icon", this.priorityBtn).addClass("low-to-high");
                break;
            case sortOptions.PriorityHighToLow:
                $(".priority-sort-icon", this.priorityBtn).addClass("high-to-low");
                break;
            case sortOptions.DueDateLowToHigh:
                $(".due-date-sort-icon", this.deadlineBtn).addClass("low-to-high");
                break;
            case sortOptions.DueDateHighToLow:
                $(".due-date-sort-icon", this.deadlineBtn).addClass("high-to-low");
                break;
        }
    }
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.TaskSort = function (panel, hashParams) {
        return new TaskSort(panel, hashParams);
    };
    //=================================================================================
})(window);