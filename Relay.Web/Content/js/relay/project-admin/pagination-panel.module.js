﻿; (function (window) {
    //=================================================================================
    function Pagination(panel, pageKey, pageSizeKey) {
        this.panel = panel;
        this.pageKey = pageKey;
        this.pageSizeKey = pageSizeKey;
        this.isOpened = false;
        this.totalCount = 0;

        this.btn_PageSize = $(".count-block", panel);

        this.minPageSize = Number(this.btn_PageSize.attr("min-page-size"));
        this.maxPageSize = Number(this.btn_PageSize.attr("max-page-size"));

        this.btn_FirstPage = $(".first-page", panel);
        this.btn_PrevPage = $(".prev-page", panel);
        this.btn_NextPage = $(".next-page", panel);
        this.btn_LastPage = $(".last-page", panel);

        this.currentPageInfo = $(".current-page", panel);
        this.totalPagesInfo = $(".total-page", panel);

        this.btn_PageSize.find(".counter").on("click", Pagination_SetPageSize.bind(this));

        this.btn_FirstPage.on("click", Pagination_FirstPage.bind(this));
        this.btn_PrevPage.on("click", Pagination_PrevPage.bind(this));
        this.btn_NextPage.on("click", Pagination_NextPage.bind(this));
        this.btn_LastPage.on("click", Pagination_LastPage.bind(this));

        Relay.UrlHash.addChangeEvent(function (model, modelChangesMap) {
            if (modelChangesMap[this.pageKey] || modelChangesMap[this.pageSizeKey]) {
                Pagination_ChangeParams.call(this, Page(model[this.pageKey]), PageSize(model[this.pageSizeKey], this.minPageSize))
            }
        }.bind(this));
    }
    Pagination.prototype.close = function () {
        var hashModel = Relay.UrlHash.getHashModel();
        hashModel[this.pageKey] = null;
        hashModel[this.pageSizeKey] = null;
        Relay.UrlHash.updateHashModel(hashModel);

        this.isOpened = false;
        this.panel.hide();
    }
    Pagination.prototype.open = function (totalCount) {
        this.totalCount = Number(totalCount);

        if (this.totalCount > this.minPageSize) {
            if (!this.isOpened) {
                this.isOpened = true;
                var hashModel = Relay.UrlHash.getHashModel();
                Pagination_ChangeParams.call(this, Page(hashModel[this.pageKey]), PageSize(hashModel[this.pageSizeKey], this.minPageSize))
            }

            this.panel.show();
        }
    };
    function Page(page) {
        return page ? Number(page) : 1;
    }
    function PageSize(pageSize, defaultVal) {
        return pageSize ? Number(pageSize) : defaultVal;
    }
    function CountPages(pageSize, totalCount) {
        return Math.trunc(totalCount / pageSize) + (totalCount % pageSize > 0 ? 1 : 0);
    }
    function Pagination_ChangeParams(page, pageSize) {
        var pages = CountPages(pageSize, this.totalCount);
        this.btn_PageSize.find(".counter").removeClass("active");

        var sizeNode = this.btn_PageSize.find("div:contains('" + pageSize + "')");
        sizeNode = sizeNode.length ? sizeNode : this.btn_PageSize.find("div:contains('All')");
        sizeNode.addClass("active");

        if (!page || page === 1) {
            this.btn_FirstPage.addClass("inactive");
            this.btn_PrevPage.addClass("inactive");
        }
        else {
            this.btn_FirstPage.removeClass("inactive");
            this.btn_PrevPage.removeClass("inactive");
        }

        if (page === pages) {
            this.btn_NextPage.addClass("inactive");
            this.btn_LastPage.addClass("inactive");
        }
        else {
            this.btn_NextPage.removeClass("inactive");
            this.btn_LastPage.removeClass("inactive");
        }

        this.currentPageInfo.text(page);
        this.totalPagesInfo.text(pages);
    }
    function Pagination_SetPageSize(event) {
        var node = $(event.currentTarget);
        var hashModel = Relay.UrlHash.getHashModel();
        var size = node.text();
        hashModel[this.pageSizeKey] = size === "All" 
                                        ? (this.maxPageSize >= this.totalCount) 
                                        ? this.maxPageSize + 1
                                        : this.totalCount
                                        : size;
        hashModel[this.pageKey] = 1;
        Relay.UrlHash.updateHashModel(hashModel);
    }
    function Pagination_FirstPage() {
        var hashModel = Relay.UrlHash.getHashModel();
        hashModel[this.pageKey] = 1;
        Relay.UrlHash.updateHashModel(hashModel);
    }
    function Pagination_PrevPage() {
        var hashModel = Relay.UrlHash.getHashModel();
        var page = Page(hashModel[this.pageKey]);
        if (page > 1) {
            hashModel[this.pageKey] = page - 1;
            Relay.UrlHash.updateHashModel(hashModel);
        }
    }
    function Pagination_NextPage() {
        var hashModel = Relay.UrlHash.getHashModel();
        var page = Page(hashModel[this.pageKey]);
        var pages = CountPages(PageSize(hashModel[this.pageSizeKey], this.minPageSize), this.totalCount);
        if (page < pages) {
            hashModel[this.pageKey] = page + 1;
            Relay.UrlHash.updateHashModel(hashModel);
        }
    }
    function Pagination_LastPage() {
        var hashModel = Relay.UrlHash.getHashModel();
        hashModel[this.pageKey] = CountPages(PageSize(hashModel[this.pageSizeKey], this.minPageSize), this.totalCount);
        Relay.UrlHash.updateHashModel(hashModel);
    }
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.Pagination = function (panel, projectIdKey, taskOwnerIdKey) {
        return new Pagination(panel, projectIdKey, taskOwnerIdKey);
    }
    //=================================================================================
})(window);