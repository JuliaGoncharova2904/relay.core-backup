﻿; (function (window) {
    "use strict";
    //===================================================================
    // config: {
    //      projectId: projectId,
    //      selectedUserId: SelectedUserId,
    // }
    //=================== Choose Teammate Dialog ========================
    function ProjectChooseTeammateDialog(config) {
        this.config = config;
        this.openHandler = null;
        this.chooseHandler = null;
        this.closeHandler = null;
        //-----------------------
        this.chosenId = null;
        //-----------------------
        this.dialogId = "chooseTeammateDialog";
        this.conteiner = null;
        this.dialog = null;
        //-----------------------
    }
    ProjectChooseTeammateDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    ProjectChooseTeammateDialog.prototype.setChooseEvent = function (eventHandler) {
        this.chooseHandler = eventHandler;
    };
    ProjectChooseTeammateDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };
    ProjectChooseTeammateDialog.prototype.open = function () {

        var _this = this;

        $('#loading-animation').show();

        if (_this.openHandler) {
            _this.openHandler();
        }

        $.ajax({
            url: "/ProjectTasks/GetChooseTeammateDialog",
            method: "GET",
            data: { projectId: _this.config.projectId }
        })
        .done(function (data) {
            ProjectChooseTeammateDialog_CreateDialog.call(_this, data);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(_this.closeHandler);
            errorDialog.open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
    };

    function ProjectChooseTeammateDialog_CreateDialog(dialogHtml) {
        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = dialogHtml;
        document.body.appendChild(this.conteiner);

        var _this = this;
        this.dialog = $('#' + this.dialogId, this.conteiner)[0];
        $(this.dialog).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });

        this.chosenId = this.config.selectedUserId;

        $(".user[id='" + this.chosenId + "']", this.dialog).addClass("selected-user");

        $(".user", this.dialog).tooltipster({
            debug: false
        });

        $(".user", this.dialog).on('click', function () {
            $(".user", _this.dialog).removeClass("selected-user");
            $(this).addClass("selected-user");

            _this.chosenId = $(this).prop('id');

            $(".btn-save", _this.dialog).addClass("active");
        });

        $(".btn-save", this.dialog).click(function () {
            if (_this.chooseHandler)
                _this.chooseHandler(_this.chosenId);

            $(_this.dialog).modal("hide");
        });

        $(this.dialog).modal('show');
    }
    //===================================================================
    window.Relay = window.Relay || {};
    window.Relay.ProjectChooseTeammateDialog = function (config) {
        return new ProjectChooseTeammateDialog(config);
    };
    //===================================================================
})(window);