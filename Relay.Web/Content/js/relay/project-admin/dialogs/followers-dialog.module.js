﻿; (function (window) {
    //=================================================================================
    function errorHandler() {
        var errorDialog = Relay.ErrorDialog();
        errorDialog.setCloseEvent(function () {
            location.reload();
        });
        errorDialog.open();
    }
    //=================================================================================
    function FollowersDialog(projectId) {
        this.projectId = projectId;

        this.config = {
            url: "/ProjectFollowers/FollowersDialog",
            id: "projectFollowersDialog"
        };

        this.dialog = null;
        this.currentTab = null;

        this.saveBtn = null;
        this.closeBtn = null;

        this.searchInput = null;

        this.teamsTabContainer = null;
        this.followersTabContainer = null;

        this.openHandler = null;
        this.closeHandler = null;

        this.closeEventBind = FollowersDialog_CloseEvent.bind(this);
    }
    FollowersDialog.prototype.setOpenEvent = function (handler) {
        this.openHandler = handler;
    };
    FollowersDialog.prototype.setCloseEvent = function (handler) {
        this.closeHandler = handler;
    };
    FollowersDialog.prototype.open = function () {
        //--------------------------
        $('#loading-animation').show();
        if (this.openHandler) {
            this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: this.config.url,
            method: "GET",
            data: { projectId: this.projectId }
        })
        .done(FollowersDialog_BuildDialog.bind(this))
        .fail(errorHandler)
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    FollowersDialog.prototype.saveBtnClickHandler = function () {
        this.currentTab.save();
    };
    FollowersDialog.prototype.teamsTabClickHandler = function () {
        this.searchInput.parent().attr("style", "display: none;");
        this.currentTab.save();
        this.currentTab = new TeamsTab(this.teamsTabContainer, FollowersDialog_ContentChangedHandler.bind(this));
        this.currentTab.load(this.projectId);
    };
    FollowersDialog.prototype.followersTabClickHandler = function () {
        this.searchInput.parent().removeAttr("style");
        this.currentTab.save();
        this.currentTab = new FollowersTab(this.followersTabContainer, FollowersDialog_ContentChangedHandler.bind(this));
        this.currentTab.load(this.projectId);
    };
    function FollowersDialog_BuildDialog(dialogHtml) {
        var _this = this;

        var container = document.createElement("div");
        container.innerHTML = dialogHtml;
        document.body.appendChild(container);

        this.dialog = $('#' + this.config.id, container);

        this.dialog.on('hidden.bs.modal', function () {
            $(container).remove();
            if (_this.closeHandler)
                _this.closeHandler();
        });

        this.saveBtn = $("button.btn-save", this.dialog);
        this.closeBtn = $("button.btn-close", this.dialog);

        this.searchInput = $("#search", this.dialog);

        this.teamsTabContainer = $("#teams-tab", this.dialog);
        this.followersTabContainer = $("#team-members-tab", this.dialog);

        this.saveBtn.click(this.saveBtnClickHandler.bind(this));
        $(".nav-tabs li a[href='#teams-tab']", this.dialog).click(this.teamsTabClickHandler.bind(this));
        $(".nav-tabs li a[href='#team-members-tab']", this.dialog).click(this.followersTabClickHandler.bind(this));
        //-------------------- init search ---------------------------
        this.searchInput.parent().attr("style", "display: none;");

        this.searchInput.focusin(function () {
            $(this).parent().addClass("active");
        });
        this.searchInput.focusout(function () {
            if(!$(this).val())
                $(this).parent().removeClass("active");
        });
        this.searchInput.bind("input", $.debounce(700, function () {
            var searchQuery = $(this).val().trim();
            $(this).val(searchQuery);
            _this.currentTab.load(_this.projectId, searchQuery);
        }));
        //------------------------------------------------------------
        this.currentTab = new TeamsTab(this.teamsTabContainer, FollowersDialog_ContentChangedHandler.bind(this));
        this.currentTab.init();

        this.dialog.modal('show');
    }
    function FollowersDialog_ContentChangedHandler(isChanged) {
        if (isChanged) {
            //------- Add confirm close event --------
            this.closeBtn.removeAttr("data-dismiss");
            this.closeBtn.bind("click", this.closeEventBind);
            //--------- Active save button -----------
            this.saveBtn.addClass("active");
        }
        else {
            //----- Remove confirm close event -------
            this.closeBtn.unbind("click", this.closeEventBind);
            this.closeBtn.attr("data-dismiss", "modal");
            //------- Inactive save button -----------
            this.saveBtn.removeClass("active");
        }
    }
    function FollowersDialog_CloseEvent() {
        var confirmDialog = Relay.ConfirmDialog(
            "",
            "All changes will be lost. Are you sure you want to exit?",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function () {
            this.dialog.modal("hide");
        }.bind(this));
        confirmDialog.open();
    };
    //=================================================================================
    function TeamsTab(container, contentChangedHandler) {
        this.container = container;
        this.projectId = null;
        this.contentChangedHandler = contentChangedHandler;

        this.model = {
            SelectedTeams: [],
            UnselectedTeams: []
        };
    }
    TeamsTab.prototype.save = function () {
        if (this.model.SelectedTeams.length > 0 || this.model.UnselectedTeams.length > 0) {
            var _this = this;
            this.model.ProjectId = this.projectId;
            this.model["__RequestVerificationToken"] = $("input[name='__RequestVerificationToken']", this.container).val();

            $.ajax({
                url: "/ProjectFollowers/TeamsTab",
                method: "POST",
                data: this.model
            })
            .done(function () {
                _this.model.SelectedTeams = [];
                _this.model.UnselectedTeams = [];

                if (_this.contentChangedHandler)
                    _this.contentChangedHandler(false);
            })
            .fail(errorHandler);
        }
    };
    TeamsTab.prototype.load = function (projectId) {
        var _this = this;

        $.ajax({
            url: "/ProjectFollowers/TeamsTab",
            method: "GET",
            data: { projectId: projectId }
        })
        .done(function (newContent) {
            $(_this.container).html($(newContent));
            _this.init();
        })
        .fail(errorHandler);
    }
    TeamsTab.prototype.init = function () {
        var _this = this;

        this.projectId = $(this.container).children().first().attr("projectId");

        if (this.contentChangedHandler)
            this.contentChangedHandler(false);

        $(".team-item", this.container).click(function () {
            var teamId = $(this).attr("teamId");

            if ($(this).hasClass("active")) {
                $(this).removeClass("active");

                var selectedIndex = _this.model.SelectedTeams.indexOf(teamId);

                if (selectedIndex != -1) {
                    _this.model.SelectedTeams.splice(selectedIndex, 1);
                }
                else {
                    _this.model.UnselectedTeams.push(teamId);
                }
            }
            else {
                $(this).addClass("active");

                var unselectedIndex = _this.model.UnselectedTeams.indexOf(teamId);

                if (unselectedIndex != -1) {
                    _this.model.UnselectedTeams.splice(unselectedIndex, 1);
                }
                else {
                    _this.model.SelectedTeams.push(teamId);
                }
            }

            if (_this.contentChangedHandler) {
                _this.contentChangedHandler(_this.model.SelectedTeams.length > 0 || _this.model.UnselectedTeams.length > 0)
            }
        });
    };
    //=================================================================================
    function FollowersTab(container, contentChangedHandler) {
        this.container = container;
        this.projectId = null;
        this.contentChangedHandler = contentChangedHandler;

        this.model = {
            SelectedFollowers: [],
            UnselectedFollowers: []
        };
    }
    FollowersTab.prototype.save = function () {
        if (this.model.SelectedFollowers.length > 0 || this.model.UnselectedFollowers.length > 0) {
            var _this = this;
            this.model.ProjectId = this.projectId;
            this.model["__RequestVerificationToken"] = $("input[name='__RequestVerificationToken']", this.container).val();

            $.ajax({
                url: "/ProjectFollowers/FollowersTab",
                method: "POST",
                data: this.model
            })
            .done(function () {
                _this.model.SelectedFollowers = [];
                _this.model.UnselectedFollowers = [];

                if (_this.contentChangedHandler)
                    _this.contentChangedHandler(false);
            })
            .fail(errorHandler);
        }
    };
    FollowersTab.prototype.load = function (projectId, searchQuery) {
        var _this = this;

        $.ajax({
            url: "/ProjectFollowers/FollowersTab",
            method: "GET",
            data: {
                projectId: projectId,
                searchQuery: searchQuery
            }
        })
        .done(function (newContent) {
            $(_this.container).html($(newContent));
            _this.init();
        })
        .fail(errorHandler);
    }
    FollowersTab.prototype.init = function () {
        var _this = this;

        this.projectId = $(this.container).children().first().attr("projectId");

        if (this.contentChangedHandler)
            this.contentChangedHandler(false);

        var teamMemberPageSwiper = new Swiper($(".team-members-swiper", this.container), {
            direction: 'vertical',
            height: 50,
            nextButton: $(".team-members-swiper-next", this.container),
            prevButton: $(".team-members-swiper-prev", this.container)
        });

        $("input[name='Select']", this.container).bind("change", function () {
            var followerId = $(this).parent().attr("followerId");

            if ($(this).prop("checked")) {
                var unselectedIndex = _this.model.UnselectedFollowers.indexOf(followerId);

                if (unselectedIndex != -1) {
                    _this.model.UnselectedFollowers.splice(unselectedIndex, 1);
                }
                else {
                    _this.model.SelectedFollowers.push(followerId);
                }
            }
            else {
                var selectedIndex = _this.model.SelectedFollowers.indexOf(followerId);

                if (selectedIndex != -1) {
                    _this.model.SelectedFollowers.splice(selectedIndex, 1);
                }
                else {
                    _this.model.UnselectedFollowers.push(followerId);
                }
            }

            if (_this.contentChangedHandler) {
                _this.contentChangedHandler(_this.model.SelectedFollowers.length > 0 || _this.model.UnselectedFollowers.length > 0)
            }
        });
    };
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.FollowersDialog = function (projectId) {
        return new FollowersDialog(projectId);
    };
    //=================================================================================
})(window);