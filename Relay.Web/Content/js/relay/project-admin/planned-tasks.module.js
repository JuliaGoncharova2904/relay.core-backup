﻿; (function (window)
{
  //=================================================================================
  function PlannedTasks(panel, pagination, hashParams)
  {
    this.panel = panel;
    this.pagination = pagination;
    this.hashParams = hashParams;

    this.urls = {
      TasksList: "/ProjectTasks/PlannedTasksList",
      UpdateTask: "/ProjectTasks/UpdateTask",
      ReloadTask: "/ProjectTasks/ReloadTask"
    };

    this.sortObject = null;

    this.hashChangeHandler = function (model, modelChangesMap)
    {
      if (modelChangesMap[this.hashParams.SortOptions]
          || modelChangesMap[this.hashParams.FilterOptions]
          || modelChangesMap[this.hashParams.Page]
          || modelChangesMap[this.hashParams.PageSize])
      {

        this.reload(model[this.hashParams.ProjectId],
                    model[this.hashParams.FilterOptions],
                    model[this.hashParams.SortOptions],
                    model[this.hashParams.Page],
                    model[this.hashParams.PageSize]);
      }
    }.bind(this);

    Relay.UrlHash.addChangeEvent(this.hashChangeHandler);

    this.loadList();
  }
  PlannedTasks.prototype.dispose = function ()
  {
    this.panel.empty();
    this.pagination.close();
    if (this.sortObject)
    {
      this.sortObject.dispose();
    }
    Relay.UrlHash.removeChangeEvent(this.hashChangeHandler);
  };
  PlannedTasks.prototype.reload = function (projectId, filterOptions, sortOptions, page, pageSize)
  {
    var _this = this;
    Relay.LoadingAnimation($.ajax({
      url: this.urls.TasksList,
      method: "GET",
      data: {
        projectId: projectId,
        filterOptions: filterOptions,
        sortOptions: sortOptions,
        page: page,
        pageSize: pageSize
      }
    })
    .done(function (newContent)
    {
      //--- update content -----
      var tasks = $(newContent);
      var count = Number(tasks.attr("total-task-count"));
      _this.panel.html(tasks);
      //---- Init section ------
      _this.sortObject = Relay.TaskSort(tasks, _this.hashParams);
      count && PlannedTasks_Initialise.call(_this);
      _this.pagination.open(count);
      //------------------------
    })
    .fail(function ()
    {
      var errorDialog = Relay.ErrorDialog();
      errorDialog.setCloseEvent(function ()
      {
        location.reload();
      });
      errorDialog.open();
    }), 300);
  };
  PlannedTasks.prototype.loadList = function ()
  {
    this.pagination.close();

    var hashModel = Relay.UrlHash.getHashModel();
    this.reload(hashModel[this.hashParams.ProjectId],
                hashModel[this.hashParams.FilterOptions],
                hashModel[this.hashParams.SortOptions],
                hashModel[this.hashParams.Page],
                hashModel[this.hashParams.PageSize]);
  }
  function PlannedTasks_Initialise()
  {
    var _this = this;
    //-------
    $(".tooltipster").tooltipster({
      debug: false
    });

    $("tbody tr", this.panel).each(function ()
    {
      PlannedTasks_BuildTopic.call(_this, this);
    });
  }
  function PlannedTasks_BuildTopic(trElement)
  {
    var _this = this;

    var readOnly = $(trElement).attr("read-only") === "True";

    var task = Relay.Topic(trElement, {
      updateUrl: this.urls.UpdateTask,
      reloadUrl: this.urls.ReloadTask
    });

    if (!readOnly)
    {
      task.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
        type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.task,
        changeHandler: this.loadList.bind(this),
        warningMessage: "Are you sure you want to delete this task?"
      }));
    }

    task.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
      readOnly: readOnly
    }));
    task.AddControl(Relay.Topic.ControlsFactory.TextField({
      modelPropName: "Name",
      elementClass: ".name-td",
      clickHandler: readOnly ? PlannedTasks_ViewTaskHandler : PlannedTasks_EditTaskHandler,
      finalizeOff: true
    }));
    task.AddControl(Relay.Topic.ControlsFactory.TextField({
      modelPropName: "CompleteStatus",
      elementClass: ".complite-status",
      finalizeOff: true
    }));
    task.AddControl(Relay.Topic.ControlsFactory.PriorityField());
      task.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
      task.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags());
    task.AddControl(Relay.Topic.ControlsFactory.TeamBtn({ readOnly: true, forDraftTask: true }));
    task.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
      dialogType: Relay.AttachmentsDialog.Types.Draft,
      container: Relay.AttachmentsDialog.Containers.Task
    }));
    task.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
      dialogType: Relay.VoiceMessagesDialog.Types.Received,
      container: Relay.VoiceMessagesDialog.Containers.Task
    }));
    task.AddControl(Relay.Topic.ControlsFactory.ArchivedBtn({
      completeFieldName: "CompleteStatus"
    }));


    task.updateView();

    return task;
  }
  function PlannedTasks_ViewTaskHandler(model, reload, update)
  {
    var viewTaskDialog = Relay.FormDialog({
      url: "/TaskBoard/TaskDetailsDialog",
      data: { TaskId: model.Id }
    });
    viewTaskDialog.open();
  }
  function PlannedTasks_EditTaskHandler(model, reload, update)
  {
    var editTaskDialog = Relay.FormDialog({
      url: "/ProjectTasks/EditTaskDialog",
      data: { taskId: model.Id }
    });
    editTaskDialog.setErrorEvent(function ()
    {
      location.reload();
    });
    editTaskDialog.setSubmitEvent(function ()
    {
      reload();
    });
    editTaskDialog.open();
  }
  //=================================================================================
  window.Relay = window.Relay || {};
  window.Relay.PlannedTasks = function (panel, pagination, hashParams)
  {
    return new PlannedTasks(panel, pagination, hashParams);
  }
  //=================================================================================
})(window);

