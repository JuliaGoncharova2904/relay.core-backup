﻿; (function (window) {
    //=================================  Task panel =================================================
    function TasksPanel(panel, projectIdKey) {
        var _this = this;
        this.panel = panel;

        this.hashParams = {
            ProjectId: projectIdKey,
            TabType: "TabType",
            SortOptions: "SortOptions",
            FilterOptions: "FilterOptions",
            Page: "TaskPage",
            PageSize: "TaskPageSize"
        };

        this.tabTypes = {
            Planned: "Planned",
            AdHoc: "AdHoc"
        };

        this.btnPlus = $(".plus-btn", panel);
        this.btnFilter = $(".filter-btn", panel);

        this.btnPanel = $(".navbar-brown", panel);
        this.noSelectMsg = $(".empty-project-tasks-label", panel);

        this.btnPlanned = $("#btnPlanned", panel);
        this.btnAdHoc = $("#btnAdHoc", panel);

        this.tabController = null;
        this.tabContainer = $(".table-block", panel);
        this.pagination = new Relay.Pagination($(".tasks-footer", panel), this.hashParams.Page, this.hashParams.PageSize);

        this.btnPlanned.on("click", this.plannedHandler.bind(this));
        this.btnAdHoc.on("click", this.adHocHandler.bind(this));

        this.btnPlus.on("click", function () {
            var hashModel = Relay.UrlHash.getHashModel();

            var addTaskDialog = Relay.FormDialog({
                url: "/ProjectTasks/AddTaskDialog",
                data: { projectId: hashModel[_this.hashParams.ProjectId] }
            });
            addTaskDialog.setSubmitEvent(function () {
                _this.tabController && _this.tabController.loadList();
            });
            addTaskDialog.open();
        });

        this.btnFilter.webuiPopover({
            hideEmpty: true,
            style: 'project-task',
            trigger: 'click',
            multi: false,
            dismissible: true,
            type: "async",
            cache: false,
            width: '118',
            url: "/ProjectTasks/TaskFilter",
            offsetLeft: -8,
            offsetTop: 4,
            placement: 'bottom-left'
        });

        Relay.UrlHash.addChangeEvent(function (model, modelChangesMap) {
            if (modelChangesMap[this.hashParams.ProjectId] || modelChangesMap[this.hashParams.TabType]) {
                this.update(model[this.hashParams.ProjectId], model[this.hashParams.TabType]);
            }
        }.bind(this));

        var hashModel = Relay.UrlHash.getHashModel();
        this.update(hashModel[this.hashParams.ProjectId], hashModel[this.hashParams.TabType]);
    }
    TasksPanel.prototype.update = function (projectId, tabType) {
        if (projectId) {
            this.btnFilter.show();
            this.btnPanel.show();
            this.noSelectMsg.hide();
            this.tabController && this.tabController.dispose();

            switch (tabType) {
                case this.tabTypes.Planned:
                    this.btnPlus.show();
                    this.btnAdHoc.parent().removeClass("active");
                    this.btnPlanned.parent().addClass("active");
                    this.tabController = Relay.PlannedTasks(this.tabContainer, this.pagination, this.hashParams);
                    break;
                case this.tabTypes.AdHoc:
                    this.btnPlus.hide();
                    this.btnAdHoc.parent().addClass("active");
                    this.btnPlanned.parent().removeClass("active");
                    this.tabController = Relay.AdHocTasks(this.tabContainer, this.pagination, this.hashParams);
                    break;
                default:
                    this.tabController = null;
                    this.plannedHandler();
            }
        }
        else {
            this.btnPlus.hide();
            this.btnFilter.hide();
            this.btnPanel.hide();
            this.noSelectMsg.show();
            this.tabController && this.tabController.dispose();
            this.tabController = null;

            var hashModel = Relay.UrlHash.getHashModel();
            hashModel[this.hashParams.TabType] = null;
            hashModel[this.hashParams.FilterOptions] = null;
            Relay.UrlHash.updateHashModel(hashModel);
        }
    };
    TasksPanel.prototype.plannedHandler = function () {
        var hashModel = Relay.UrlHash.getHashModel();
        if (hashModel[this.hashParams.TabType] !== this.tabTypes.Planned) {
            hashModel[this.hashParams.FilterOptions] = null;
            hashModel[this.hashParams.TabType] = this.tabTypes.Planned;
            Relay.UrlHash.updateHashModel(hashModel);
        }
    };
    TasksPanel.prototype.adHocHandler = function () {
        var hashModel = Relay.UrlHash.getHashModel();
        if (hashModel[this.hashParams.TabType] !== this.tabTypes.AdHoc) {
            hashModel[this.hashParams.FilterOptions] = null;
            hashModel[this.hashParams.TabType] = this.tabTypes.AdHoc;
            Relay.UrlHash.updateHashModel(hashModel);
        }
    };
    //===============================================================================================
    window.Relay = window.Relay || {};

    window.Relay.TasksPanel = function (panel, projectIdKey) {
        return new TasksPanel(panel, projectIdKey);
    };
    //===============================================================================================
})(window);