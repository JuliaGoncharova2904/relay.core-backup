﻿; (function ()
{
    //=================================================================
    $.ajaxSetup({ cache: false });
    $.ajaxSettings.traditional = true;

    $(document).on('hidden.bs.modal', '.modal', function ()
    {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

    $(window).on('show.bs.modal', function (e)
    {
        if (e.namespace === 'bs.modal')
        {
            $("body").css({ "overflow": "hidden" });
        }
    });

    $(window).on('hide.bs.modal', function (e)
    {
        if (e.namespace === 'bs.modal')
        {
            $("body").css("overflow", "");
        }
    });

    //=================================================================
})();