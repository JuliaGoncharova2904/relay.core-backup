﻿(function () {
    //=================================================================================
    var UrlQuery = {
        getQueryModel: function () {
            var match, ret = {};
            var re = /\??(.*?)=([^\&]*)&?/gi;
            var hash = document.location.search.substr(1);

            while (match = re.exec(hash)) {
                ret[match[1]] = match[2];
            }

            return ret;
        },
        updateQueryModel: function (model) {
            var query = "";

            for (var pKey in model) {
                if (model[pKey]) {
                    query += (query ? '&' : '')  + pKey + '=' + model[pKey];
                }
            }

            document.location.search = query;
        }
    };
    //=================================================================================
    window.Relay = window.Relay || {};
    window.Relay.UrlQuery = UrlQuery;
    //=================================================================================
})();