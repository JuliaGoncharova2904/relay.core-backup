﻿(function () {
    //===========================================================================
    //  id - cintainer ID
    //  type - mode type
    //  container - container type
    //===========================================================================
    var Types = {
        Draft: 1,
        Received: 2
    };
    var Containers = {
        Topic: 1,
        Task: 2
    }
    //===================== Manager Comments Dialog ==================================
    function ManagerCommentsDialog(id, type, container) {
        this.id = id;
        this.type = type;
        this.container = container;
        this.openHandler = null;
        this.closeHandler = null;
    }
    ManagerCommentsDialog.prototype.open = function () {
        var _this = this;
        //--------------------------
        var configDialog = {
            id: this.id,
            type: this.type,
            dialogId: "managerCommentsDetailsDialog"
        };

        switch (this.container) {
            case Containers.Topic:
                configDialog.dialogUrl = "/Topic/ManagerCommentsDialog";
                configDialog.addFormUrl = "/Topic/AddManagerCommentsForTopic";
                configDialog.removeUrl = "/Topic/RemoveManagerCommentsDialog";
                break;
            default:
                return;
        }
        //--------------------------
        $('#loading-animation').show();
        if (_this.openHandler) {
            _this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: configDialog.dialogUrl + '/' + configDialog.id,
            method: "GET"
        })
        .done(function (result) {
            var formDialog = new _Dialog(configDialog);
            formDialog.buildDialog(result);
            formDialog.closeHandler = _this.closeHandler;
            formDialog.open();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    ManagerCommentsDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    ManagerCommentsDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };
    //===========================================================================
    function _Dialog(config) {
        this.config = config;
        this.conteiner = null;
        this.slidesContainer = null;
        this.addFormContainer = null;
        this.addButton = null;
        this.editButton = null;
        this.closeHandler = null;
    }
    _Dialog.prototype.open = function () {
        //----------- Voice Messages List --------------
        new Swiper($(".swiper-container", this.conteiner), {
            // slidesPerView: 2,
            direction: 'vertical',
            height: 80,
            nextButton: $(".swiper-next", this.conteiner),
            prevButton: $(".swiper-prev", this.conteiner)
        })
        //----------------------------------------------
        this.addFormContainer.slideUp();
        $('#' + this.config.dialogId, this.conteiner).modal('show');
    };

    _Dialog.prototype.buildDialog = function (windowHtml) {
        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = windowHtml;
        document.body.appendChild(this.conteiner);

        this.slidesContainer = $(".swiper-wrapper", this.conteiner).first();
        this.addFormContainer = $(".add-manager-comments", this.conteiner).first();
        this.addButton = $(".add-manager-comments-btn", this.conteiner).first();
        this.editButton = $(".manager-comments-edit-btn", this.conteiner).first();

        var _this = this;

        $('#' + this.config.dialogId, this.conteiner).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });

        switch (this.config.type) {
            case Types.Draft:
                this.addButton.click(this.openAddManagerCommentsForm.bind(this));
                this.editButton.click(this.openEditManagerCommentsForm.bind(this));
                $(".remove-manager-comments-btn", this.slidesContainer).click(this.removeManagerComments.bind(this));
                break;
            case Types.Received:
                this.addButton.click(this.openAddManagerCommentsForm.bind(this));
                $(".remove-manager-comments-btn", this.slidesContainer).click(this.removeManagerComments.bind(this));
                $(".horizontal-title-container").addClass("removed");
                break;
        }
    };
    _Dialog.prototype.removeManagerComments = function (event) {

        var _this = this;
        var managerCommentId = event.currentTarget.id;

        var confirmDialog = Relay.ConfirmDialog(
                                                    "",
                                                    "Comment will be removed. Do you agree with it?",
                                                    "Yes",
                                                    "No"
                                                );

        confirmDialog.setOkEvent(function () {
            $.ajax({
                url: _this.config.removeUrl,
                method: "POST",
                data: { id: _this.config.id, managerCommentId: managerCommentId }
            })
            .done(function (result) {
                _this.reloadManagerCommentsList();
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            });
        });

        confirmDialog.open();
    };

    _Dialog.prototype.reloadManagerCommentsList = function () {
        var _this = this;
        $.ajax({
            url: this.config.dialogUrl + '/' + this.config.id,
            method: "GET"
        })
            .done(function (result) {
                var slides = $(result).find(".swiper-wrapper");
                _this.slidesContainer.html(slides.html());

             $(".popup-loading-animation").css('display', 'none');

            switch (_this.config.type) {
                case Types.Draft:
                    $(".remove-manager-comments-btn", _this.slidesContainer).click(_this.removeManagerComments.bind(_this));
                    break;
                case Types.Received:
                    $(".remove-manager-comments-btn", _this.slidesContainer).click(_this.removeManagerComments.bind(_this));
                    break;
            }
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    };

    _Dialog.prototype.openEditManagerCommentsForm = function (event) {
        var _this = this;
        var managerCommentId = this.editButton[0].attributes[1].nodeValue;
        Relay.DialogLoadingAnimation.show(this.conteiner, 100);
        $.ajax({
            url: "/Topic/EditManagerCommentsForTopic?topicId=" + this.config.id + "&managerCommentId=" + managerCommentId,
            method: "GET"
        })
            .done(function (result) {
                _this.addFormContainer.html(result);
                $(".btn-file", _this.addFormContainer).click(_this.submitAddForm.bind(_this));
                Relay.InitInputFields(_this.addFormContainer);
                _this.addFormContainer.slideDown();
                _this.reloadManagerCommentsList();
                _this.addButton.css('display', 'none');
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            })
            .always(function () {
                Relay.DialogLoadingAnimation.hide();
            });
    }

    _Dialog.prototype.openAddManagerCommentsForm = function (event) {
        var _this = this;
        Relay.DialogLoadingAnimation.show(this.conteiner, 100);
        $.ajax({
            url: this.config.addFormUrl + '/' + this.config.id,
            method: "GET"
        })
            .done(function (result) {
            _this.addFormContainer.html(result);
            $(".btn-file", _this.addFormContainer).click(_this.submitAddForm.bind(_this));
            Relay.InitInputFields(_this.addFormContainer);
            _this.addFormContainer.slideDown();
            _this.addButton.css('display', 'none');
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            Relay.DialogLoadingAnimation.hide();
        });
    }
    _Dialog.prototype.submitAddForm = function () {
        var _this = this;
        var form = $("form", this.addFormContainer).first();
        $.validator.unobtrusive.parse(form);
        if (form.valid()) {
            Relay.DialogLoadingAnimation.show(_this.conteiner, 100);
            $.ajax({
                url: _this.config.addFormUrl,
                method: "POST",
                processData: false,
                contentType: false,
                data: new FormData(form[0])
            })
                .done(function (result) {
                    if (result === "ok") {
                        var nextButton = $(".swiper-next", _this.conteiner);
                        var prevButton = $(".swiper-prev", _this.conteiner);
                        nextButton.removeClass('swiper-button-disabled');
                        prevButton.removeClass('swiper-button-disabled');

                        _this.reloadManagerCommentsList();
                        _this.addFormContainer.slideUp();
                        _this.addButton.css('display', 'block');
                       
                    } else {
                        _this.addFormContainer.html(result);
                        $(".btn-file", _this.addFormContainer).click(_this.submitAddForm.bind(_this));
                        Relay.InitInputFields(_this.addFormContainer);
                    }
                })
                .fail(function () {
                    Relay.ErrorDialog().open();
                })
                .always(function () {
                    Relay.DialogLoadingAnimation.hide();
                    $(".popup-loading-animation").css('display', 'none');
                });
        }
       
    };
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.ManagerCommentsDialog = function (id, type, container) {
        return new ManagerCommentsDialog(id, type, container);
    };
    //---------------
    window.Relay.ManagerCommentsDialog.Types = Types;
    window.Relay.ManagerCommentsDialog.Containers = Containers;
    //===========================================================================
})();