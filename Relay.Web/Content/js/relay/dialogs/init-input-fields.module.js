﻿(function ()
{
    'use strict';
    //====================== Init input fields ===============================
    function initInputFields(rootNode)
    {
        //--------- Simple input and textarea ----------
        var simpleInputs = $('.simple-input', rootNode);
        simpleInputs.focus(focusHandler);
        simpleInputs.focusout(focusoutHandler);

        var simpleTextareas = $('.simple-textarea', rootNode);
        simpleTextareas.focus(focusHandler);
        simpleTextareas.focusout(focusoutHandler);
        autosize(simpleTextareas);


        function focusHandler()
        {
            if ($(this).parent().hasClass("field-container"))
            {
                $(this).parent().addClass("focus");
            } else
            {
                $(this).parent().parent().addClass("focus");
            }
        }

        function focusoutHandler()
        {
            if ($(this).parent().hasClass("field-container"))
            {
                $(this).parent().removeClass("focus");
            } else
            {
                $(this).parent().parent().removeClass("focus");
            }
        }
        //--------------- Dropdown list ----------------
        $('.dropdown-toggle', rootNode).click(function ()
        {
            $('.select-type').removeClass('focus');
            $(this).parent().parent().addClass("focus");
        });
        $('.select-list', rootNode).dropdown();
        $('.select-list', rootNode).selectpicker({
            style: 'select-class',
            size: 7,
            dropupAuto: false
        });
        //--------------- Tags component ---------------
        $('.tags-input').each(function ()
        {
            var tagsInput = this;
            $(tagsInput).on('focus', function (c)
            {
                $('.select-type', tagsInput).removeClass('focus');
                $(tagsInput).parent().addClass('focus');
            });
            $(tagsInput).on('blur', function (c)
            {
                $(tagsInput).parent().removeClass('focus');
            });
        });
        //-------- Datepicker Z-index handlers ---------
        $(".datepicker", rootNode).on("show", showHandler);
        $(".datepicker", rootNode).on("hide", hideHandler);

        function showHandler() {
            $(rootNode).attr("style", "display: block; z-index: 2500 !important;");
        }

        function hideHandler() {
            $(rootNode).attr("style", "display: block;");
        }
        //----------------------------------------------
    }
    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.InitInputFields = initInputFields;
    //========================================================================
})();
