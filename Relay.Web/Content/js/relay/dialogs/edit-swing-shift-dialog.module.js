﻿; (function ()
{
    //===========================================================================
    //  config = {
    //              userId: (User ID)
    //          }
    //===========================================================================
    function EditSwingShiftDialog(config)
    {
        this.config = config || {}
        this.config.url = "/Rotation/EditSwingShiftDialog";

        this.dialog = null;
        this.openHandler = null;
        this.closeHandler = null;

        this.saveBtn = null;
        this.closeBtn = null;

        this.swingTab = null;
        this.shiftTab = null;

        this.changeStatusFlag = false;

        this.closeEventBind = EditSwingShiftDialog_CloseEvent.bind(this);
    }
    EditSwingShiftDialog.prototype.setOpenEvent = function (handler)
    {
        this.openHandler = handler;
    };
    EditSwingShiftDialog.prototype.setCloseEvent = function (handler)
    {
        this.closeHandler = handler;
    };
    EditSwingShiftDialog.prototype.open = function ()
    {
        //--------------------------
        $('#loading-animation').show();
        if (this.openHandler)
        {
            this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: this.config.url,
            method: "GET",
            data: { userId: this.config.userId }
        })
        .done(EditSwingShiftDialog_BuildDialog.bind(this))
        .fail(function ()
        {
            Relay.ErrorDialog().open();
        })
        .always(function ()
        {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    EditSwingShiftDialog.prototype.ChangeStatus = function (isChanged)
    {
        if (isChanged)
        {
            if (!this.saveBtn.hasClass("active"))
            {
                //------- Add confirm close event --------
                this.closeBtn.removeAttr("data-dismiss");
                this.closeBtn.bind("click", this.closeEventBind);
                //--------- Active save button -----------
                this.saveBtn.addClass("active");
            }
        }
        else
        {
            //----- Remove confirm close event -------
            this.closeBtn.unbind("click", this.closeEventBind);
            this.closeBtn.attr("data-dismiss", "modal");
            //------- Inactive save button -----------
            this.saveBtn.removeClass("active");
        }

        this.changeStatusFlag = isChanged;
    };
    EditSwingShiftDialog.prototype.Save = function (event)
    {
        var _this = this;

        if (!this.changeStatusFlag)
        {
            return;
        }

        if (this.swingTab && this.swingTab.isActive())
        {
            this.swingTab.submit()
                .then(function ()
                {
                    if (_this.shiftTab)
                    {
                        _this.swingTab.reload();
                    }
                    else
                    {
                        _this.dialog.modal("hide");
                    }

                    _this.ChangeStatus(false);
                })
                .fail(function ()
                {
                    _this.ChangeStatus(false);
                });
        }
        else if (this.shiftTab && this.shiftTab.isActive() || this.shiftTab && this.swingTab == null)
        {
            this.shiftTab.submit()
                .then(function ()
                {
                    _this.shiftTab.reload();
                    _this.ChangeStatus(false);
                    _this.dialog.modal("hide");
                })
                .fail(function ()
                {
                    _this.ChangeStatus(false);
                });
        }
    };

    function EditSwingShiftDialog_CloseEvent()
    {
        var confirmDialog = Relay.ConfirmDialog(
            "All changes on tab will be lost. Are you sure?",
            "",
            "Yes",
            "No"
        );
        confirmDialog.setOkEvent(function ()
        {
            this.dialog.modal("hide");
        }.bind(this));
        confirmDialog.open();
    };
    function EditSwingShiftDialog_BuildDialog(dialogHtml)
    {
        var _this = this;
        //-----------------------------------
        var container = document.createElement("div");
        container.innerHTML = dialogHtml;
        document.body.appendChild(container);
        //-----------------------------------
        this.dialog = $(container).children().first();

        this.dialog.on('hidden.bs.modal', function ()
        {
            $(container).remove();
            if (_this.closeHandler)
                _this.closeHandler();
        });
        //-----------------------------------
        this.saveBtn = $("button.btn-save", this.dialog);
        this.closeBtn = $("button.btn-close", this.dialog);

        this.saveBtn.click(this.Save.bind(this));
        //-----------------------------------
        var swingElement = $("#swing", container);
        if (swingElement.length)
        {
            this.swingTab = new SwingTab(swingElement, this);
            this.swingTab.isActive(true);
        }

        var shiftElement = $("#shift", container);
        if (shiftElement.length)
        {
            this.shiftTab = new ShiftTab(shiftElement, this);
        }
        //-----------------------------------
        var retToPrevTab = false;
        $("a[data-toggle='tab']", container).on('shown.bs.tab', function (event)
        {

            if (retToPrevTab)
            {
                retToPrevTab = false;
                return;
            }

            var target = $(event.target).attr("href")

            switch (target)
            {
                case "#swing":
                    if (_this.changeStatusFlag)
                    {
                        var confirmDialog = Relay.ConfirmDialog(
                            "All changes on tab will be lost. Are you sure?",
                            "",
                            "Yes",
                            "No"
                        );
                        confirmDialog.setOkEvent(function ()
                        {
                            _this.shiftTab.isActive(false);
                            _this.swingTab.isActive(true);
                            _this.swingTab.reload();
                        });
                        confirmDialog.setCancelEvent(function ()
                        {
                            $('a[href="#shift"]', container).tab('show');
                            retToPrevTab = true;
                        });
                        confirmDialog.open();
                    }
                    else
                    {
                        _this.shiftTab.isActive(false);
                        _this.swingTab.isActive(true);
                        _this.swingTab.reload();
                    }
                    break;
                case "#shift":
                    if (_this.changeStatusFlag)
                    {
                        var confirmDialog = Relay.ConfirmDialog(
                            "All changes on tab will be lost. Are you sure?",
                            "",
                            "Yes",
                            "No"
                        );
                        confirmDialog.setOkEvent(function ()
                        {
                            _this.swingTab.isActive(false);
                            _this.shiftTab.isActive(true);
                            _this.shiftTab.reload();
                        });
                        confirmDialog.setCancelEvent(function ()
                        {
                            $('a[href="#swing"]', container).tab('show');
                            retToPrevTab = true;
                        });
                        confirmDialog.open();
                    }
                    else
                    {
                        _this.swingTab.isActive(false);
                        _this.shiftTab.isActive(true);
                        _this.shiftTab.reload();
                    }
                    break;
            }
        });
        //-----------------------------------
        this.dialog.modal('show');
    }
    //===========================================================================
    function SwingTab(tabElement, dialog)
    {
        this.dialog = dialog;
        this.tabElement = tabElement;
        this.rotationId = null;
        this.calendarContainer = null;
        this.isActiveFlag = false;

        SwingTab_Init.call(this);
    }
    SwingTab.prototype.isActive = function (state)
    {
        if (state !== undefined)
            this.isActiveFlag = state;

        return this.isActiveFlag;
    };
    SwingTab.prototype.reload = function ()
    {
        var _this = this;
        $.ajax({
            url: "/Rotation/EditSwingForm",
            method: "GET",
            data: { rotationId: this.rotationId }
        })
        .done(function (result)
        {
            _this.tabElement.html($(result));
            SwingTab_Init.call(_this);
            _this.dialog.ChangeStatus(false);
        })
        .fail(function ()
        {
            Relay.ErrorDialog().open();
        })
    };
    SwingTab.prototype.submit = function ()
    {
        var _this = this;
        var form = $("form", _this.tabElement);

        if (form.length)
        {
            return $.Deferred(function (defer)
            {

                $.validator.unobtrusive.parse(form);
                if (form.valid())
                {
                    $.ajax({
                        url: "/Rotation/EditSwingForm",
                        method: "POST",
                        data: form.serialize()
                    })
                    .done(function (result)
                    {
                        if (result === "ok")
                        {
                            defer.resolve();
                        } else
                        {
                            _this.tabElement.html($(result));
                            SwingTab_Init.call(_this);
                            defer.reject();
                        }
                    })
                    .fail(function ()
                    {
                        defer.reject();
                        Relay.ErrorDialog().open();
                    })
                }
                else
                {
                    defer.reject();
                }
            }).promise();
        }

        return $.Deferred(function (defer) { defer.resolve(); }).promise();
    };

    function SwingTab_Init()
    {
        this.rotationId = $("#RotationId", this.tabElement).val();
        this.calendarContainer = $("#miniCalendar", this.tabElement);

        var _this = this;
        var chooseHandlerId = null;
        var currentDate = Relay.DotNetDate.dateStringToDate($("#CurrentDate", this.tabElement).val());
        var endSwingDate = Relay.DotNetDate.dateStringToDate($("#EndSwingDate", this.tabElement).val());
        var backOnSiteDateReadOnly = $("#BackOnSiteDateReadOnly", this.tabElement).val();
        var endSwingDateReadOnly = $("#EndSwingDateReadOnly", this.tabElement).val();

        var calendar = $(this.calendarContainer).datepicker({ todayBtn: false });

        calendar.on('changeDate', function ()
        {
            if (chooseHandlerId)
            {
                switch (chooseHandlerId)
                {
                    case "EndSwingDate":
                        $("#BackOnSiteDate", this.tabElement).val('');
                        SwingTab_UpdateDateField.call(_this, "BackOnSiteDate");
                }

                $('#' + chooseHandlerId, this.tabElement).val(Relay.DotNetDate.dateToDateString($(this).datepicker('getDate')));
                SwingTab_UpdateDateField.call(_this, chooseHandlerId);

                _this.dialog.ChangeStatus(true);
            }
        });

        $(".prev", calendar).text("");
        $(".next", calendar).text("");

        SwingTab_UpdateDateField.call(_this, "StartSwingDate");
        SwingTab_UpdateDateField.call(_this, "EndSwingDate");
        SwingTab_UpdateDateField.call(_this, "BackOnSiteDate");

        if (backOnSiteDateReadOnly === "False" || endSwingDateReadOnly === "False")
        {
            this.tabElement.bind("click", function ()
            {
                //$('#StartSwingDateDay', this.tabElement).text("DD");
                //$('#StartSwingDateMonth', this.tabElement).text("MMM");
                //$('#StartSwingDateYear', this.tabElement).text("YYYY");

                $('#EndSwingDateDay', this.tabElement).text("DD");
                $('#EndSwingDateMonth', this.tabElement).text("MMM");
                $('#EndSwingDateYear', this.tabElement).text("YYYY");

                $('#BackOnSiteDateDay', this.tabElement).text("DD");
                $('#BackOnSiteDateMonth', this.tabElement).text("MMM");
                $('#BackOnSiteDateYear', this.tabElement).text("YYYY");

                SwingTab_OffCalendar.call(_this);
                $(".field-container", this.tabElement).removeClass("active");


                //if (currentDate) {
                //    var startEndDate = new Date(currentDate);
                //    startEndDate.setDate(startEndDate.getDate() + 1);
                //    calendar.datepicker("setStartDate", startEndDate);

                //    SwingTab_OnCalendar.call(_this);
                //    $(".field-container", this.tabElement).removeClass("active");
                //    $(this.tabElement).parent().addClass("active");
                //    chooseHandlerId = "StartSwingDate";
                //}

                //event.stopPropagation();

            });

            if (endSwingDateReadOnly === "False")
            {
                $(".end-date-btn", this.tabElement).bind("click", function (event)
                {
                    $('#EndSwingDateDay', this.tabElement).text("DD");
                    $('#EndSwingDateMonth', this.tabElement).text("MMM");
                    $('#EndSwingDateYear', this.tabElement).text("YYYY");

                    $('#BackOnSiteDateDay', this.tabElement).text("DD");
                    $('#BackOnSiteDateMonth', this.tabElement).text("MMM");
                    $('#BackOnSiteDateYear', this.tabElement).text("YYYY");

                    if (currentDate)
                    {
                        var startEndDate = new Date(currentDate);
                        startEndDate.setDate(startEndDate.getDate() + 1);
                        calendar.datepicker("setStartDate", startEndDate);

                        SwingTab_OnCalendar.call(_this);
                        $(".field-container", this.tabElement).removeClass("active");
                        $(this).parent().addClass("active");
                        chooseHandlerId = "EndSwingDate";
                    }

                    event.stopPropagation();
                });
            }

            if (backOnSiteDateReadOnly === "False")
            {
                $(".back-date-btn", this.tabElement).bind("click", function (event)
                {
                    $('#BackOnSiteDateDay', this.tabElement).text("DD");
                    $('#BackOnSiteDateMonth', this.tabElement).text("MMM");
                    $('#BackOnSiteDateYear', this.tabElement).text("YYYY");
                    var endSwingDate = Relay.DotNetDate.dateStringToDate($("#EndSwingDate", this.tabElement).val());

                    if (endSwingDate)
                    {
                        var startBackDate = new Date(endSwingDate);
                        startBackDate.setDate(startBackDate.getDate() + 1);
                        calendar.datepicker("setStartDate", startBackDate);

                        SwingTab_OnCalendar.call(_this);
                        $(".field-container", this.tabElement).removeClass("active");
                        $(this).parent().addClass("active");
                        chooseHandlerId = "BackOnSiteDate";
                    }

                    event.stopPropagation();
                });
            }
        }

        Relay.InitInputFields(this.tabElement);

        $("select", this.tabElement).on("change", function ()
        {
            _this.dialog.ChangeStatus(true);
        });
    }
    function SwingTab_UpdateDateField(id)
    {
        var date = Relay.DotNetDate.dateStringToDate($('#' + id, this.tabElement).val());

        if (date)
        {
            
            $('#' + id + "Day", this.tabElement).text(moment(date).format("DD"));
            $('#' + id + "Month", this.tabElement).text(moment(date).format("MMM"));
            $('#' + id + "Year", this.tabElement).text(moment(date).format("YYYY"));

            if (id == "StartSwingDate")
            {
                $(".start-date-btn").parent().removeClass("active");
            }

            if (id == "EndSwingDate")
            {
                $(".end-date-btn").parent().removeClass("active");
            }

            if (id == "BackOnSiteDate")
            {
                $(".back-date-btn").parent().removeClass("active");
            }
           
        }
        else
        {
            $('#' + id + "Day", this.tabElement).text("DD");
            $('#' + id + "Month", this.tabElement).text("MMM");
            $('#' + id + "Year", this.tabElement).text("YYYY");
        }
    }
    function SwingTab_OnCalendar()
    {
        this.calendarContainer.removeAttr("style");
    }
    function SwingTab_OffCalendar()
    {
        this.calendarContainer.attr("style", "pointer-events: none;");
    }
    //===========================================================================
    var TimeSelectType = {
        Hour: 0,
        Minute: 1
    };
    //---------------------------------------------
    function ShiftTab(tabElement, dialog)
    {
        this.dialog = dialog;
        this.tabElement = tabElement;
        this.shiftId = null;
        this.calendarContainer = null;
        this.isActiveFlag = false;

        ShiftTab_Init.call(this);
    }
    ShiftTab.prototype.isActive = function (state)
    {
        if (state !== undefined)
            this.isActiveFlag = state;

        return this.isActiveFlag;
    };
    ShiftTab.prototype.reload = function ()
    {
        var _this = this;

        $.ajax({
            url: "/Shift/EditShiftForm",
            method: "GET",
            data: { shiftId: this.shiftId }
        })
        .done(function (result)
        {
            _this.tabElement.empty();
            _this.tabElement.html($(result));
            ShiftTab_Init.call(_this);
            _this.dialog.ChangeStatus(false);
        })
        .fail(function ()
        {
            Relay.ErrorDialog().open();
        })
    };
    ShiftTab.prototype.submit = function ()
    {
        var _this = this;
        var form = $("form", _this.tabElement);

        if (form.length)
        {
            return $.Deferred(function (defer)
            {

                $.validator.unobtrusive.parse(form);
                if (form.valid())
                {
                    $.ajax({
                        url: "/Shift/EditShiftForm",
                        method: "POST",
                        data: form.serialize()
                    })
                    .done(function (result)
                    {
                        if (result === "ok")
                        {
                            defer.resolve();
                        } else
                        {
                            _this.tabElement.html($(result));
                            ShiftTab_Init.call(_this);
                            defer.reject();
                        }
                    })
                    .fail(function ()
                    {
                        defer.reject();
                        Relay.ErrorDialog().open();
                    })
                }
                else
                {
                    defer.reject();
                }
            }).promise();
        }

        return $.Deferred(function (defer) { defer.resolve(); }).promise();
    };

    function ShiftTab_Init()
    {
        var _this = this;
        this.chooseHandlerId = null;
        this.shiftId = $("#ShiftId", this.tabElement).val();

        this.calendarContainer = $("#miniCalendar", this.tabElement);
        var calendar = $(this.calendarContainer).datepicker({ todayBtn: false });

        ////------------------ Functions -------------------------------------
        calendar.on('changeDate', function ()
        {
            if (_this.chooseHandlerId)
            {

                $('#' + _this.chooseHandlerId, _this.tabElement).val(Relay.DotNetDate.dateToDateString($(this).datepicker('getDate')));
                ShiftTab_UpdateEndShiftDate.call(_this);
                ShiftTab_ClearEndShiftTime(_this);
                $(".end-time-btn").trigger("click");
                _this.dialog.ChangeStatus(true);
                $('[data-toggle="dropdown"]').parent().removeClass('open');
            }
        });

        $(".prev", calendar).text("");
        $(".next", calendar).text("");

        $('.select-list', this.tabElement).selectpicker({
            style: 'select-class',
            size: 7,
            dropupAuto: false
        });


        ShiftTab_UpdateEndShiftDate.call(this);


        //-------------------- Select Hours an Minutes handlers -------------------------
        $("select.select-list.hours-list", this.tabElement).on("show.bs.select", CheckLimitHours.bind(this));
        $("select.select-list.minutes-list", this.tabElement).on("show.bs.select", CheckLimitMinutes.bind(this));

        $("select.select-list.hours-list", this.tabElement).on('change', function ()
        {
            $("#ShiftEndHour", this.tabElement).val($(this).val());
            _this.dialog.ChangeStatus(true);

            var limitEndDate = moment(Relay.DotNetDate.dateStringToDate($("#LimitEndShiftDate").val()));

            var shiftEndHour = $("#ShiftEndHour", this.tabElement).val();

            var shiftEndDate = moment(Relay.DotNetDate.dateStringToDate($("#EndShiftDate").val()));

            var limitEndHour = $("#LimitShiftEndHour").val();

            if (limitEndDate.format('MM/DD/YYYY') === shiftEndDate.format('MM/DD/YYYY') && limitEndHour === shiftEndHour)
            {
                $("select.select-list.minutes-list", this.tabElement).selectpicker('val', ['']);
            }

            $('select.select-list.minutes-list').selectpicker('val', ['0']);
        });

        $("select.select-list.minutes-list", this.tabElement).on('change', function ()
        {
            $(".end-time-btn").parent().removeClass("active");
            $("#ShiftEndMinutes", this.tabElement).val($(this).val());
            _this.dialog.ChangeStatus(true);
        });

        $(".bootstrap-select ul.dropdown-menu", this.tabElement).each(function ()
        {
            //$("li", this).first().attr("style", "display: none;");
            $("li", this).first().remove();
        });


        //------------------------------------------------------  

        $(".end-date-btn", this.tabElement).bind("click", function (event)
        {
            $('#EndShiftDateDay', this.tabElement).text("DD");
            $('#EndShiftDateMonth', this.tabElement).text("MMM");
            $('#EndShiftDateYear', this.tabElement).text("YYYY");

            var limitEndShiftDate = $("#LimitEndShiftDate", _this.tabElement).val();

            if (limitEndShiftDate)
            {
              var limitEndDate = Relay.DotNetDate.dateStringToDate(limitEndShiftDate);

                calendar.datepicker("setStartDate", limitEndDate);

                ShiftTab_OnCalendar.call(_this);

                $(this).parent().addClass("active");

                _this.chooseHandlerId = "EndShiftDate";
            }

            event.stopPropagation();
        });

        $(".end-time-btn", this.tabElement).bind("click", function (event)
        {
            $("select.select-list.hours-list", this.tabElement).selectpicker('val', ['']);
            $("select.select-list.minutes-list", this.tabElement).selectpicker('val', ['']);

           
            $(this).parent().addClass("active");

            $('.time-select').css({ 'pointer-events': 'auto' });

            event.stopPropagation();
        });
    }
    //------------------------------------------------------  
    function ShiftTab_UpdateEndShiftDate()
    {
    //  var endDate = Relay.DotNetDate.dateStringToDate($("#EndShiftDate", this.tabElement).val());

      var endDate = $("#EndShiftDate", this.tabElement).val();

      var momentDate = moment(endDate, "DD/MM/YYYY");

      $("#EndShiftDateDay", this.tabElement).text(momentDate.format("DD"));
      $("#EndShiftDateMonth", this.tabElement).text(momentDate.format("MMM"));
      $("#EndShiftDateYear", this.tabElement).text(momentDate.format("YYYY"));

      $(".end-date-btn").parent().removeClass("active");
    }


    function ShiftTab_ClearEndShiftTime()
    {
        $("#ShiftEndHour", this.tabElement).val('');
        $("#ShiftEndMinutes", this.tabElement).val('');

        $("select.select-list.hours-list", this.tabElement).selectpicker('val', ['']);
        $("select.select-list.minutes-list", this.tabElement).selectpicker('val', ['']);
    }


    function ShiftTab_OnCalendar()
    {
        this.calendarContainer.removeAttr("style");
    }
    function ShiftTab_OffCalendar()
    {
        this.calendarContainer.attr("style", "pointer-events: none;");
    }


    function CheckLimitHours(event)
    {
        var hoursPickerList = $('.hours-list ul.dropdown-menu');

        var limitEndDate = moment(Relay.DotNetDate.dateStringToDate($("#LimitEndShiftDate").val()));

        var shiftEndDate = moment(Relay.DotNetDate.dateStringToDate($("#EndShiftDate").val()));

        if (limitEndDate.format('MM/DD/YYYY') === shiftEndDate.format('MM/DD/YYYY'))
        {
            var limitEndHour = $("#LimitShiftEndHour").val();

            hoursPickerList.children('li').each(function (index)
            {
                if (limitEndHour > index)
                {
                    var hourLiElement = hoursPickerList.find("li")[index];

                    $(hourLiElement).hide();
                }
            });
        }
        else
        {
            hoursPickerList.children('li').each(function (index)
            {
                var hourLiElement = hoursPickerList.find("li")[index];

                $(hourLiElement).show();
            });
        }



        event.stopPropagation();
    }

    function CheckLimitMinutes(event)
    {
        var minutesPickerList = $('.minutes-list ul.dropdown-menu');

        var limitEndDate = moment(Relay.DotNetDate.dateStringToDate($("#LimitEndShiftDate").val()));

        var shiftEndDate = moment(Relay.DotNetDate.dateStringToDate($("#EndShiftDate").val()));

        var limitEndHour = $("#LimitShiftEndHour").val();

        var shiftEndHour = $("#ShiftEndHour").val();

        if (limitEndDate.format('MM/DD/YYYY') === shiftEndDate.format('MM/DD/YYYY') && limitEndHour === shiftEndHour)
        {
            var limitEndMinutes = $("#LimitShiftEndMinutes").val();

            minutesPickerList.children('li').each(function (index)
            {
                if (limitEndMinutes >= index * 5)
                {
                    var minutesLiElement = minutesPickerList.find("li")[index];

                    $(minutesLiElement).hide();
                }
            });
        }
        else
        {
            minutesPickerList.children('li').each(function (index)
            {
                var minutesLiElement = minutesPickerList.find("li")[index];

                $(minutesLiElement).show();
            });
        }

        event.stopPropagation();
    }


    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.EditSwingShiftDialog = function (config)
    {
        return new EditSwingShiftDialog(config);
    };
    //===========================================================================
})();