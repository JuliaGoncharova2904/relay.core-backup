﻿$(document).ready(function () {
    $.validator.setDefaults({
        debug: false,
        highlight: function (element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).closest(".field-container").addClass("validation-error");
            $(element).closest(".select-type").addClass("validation-error");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
            $(element).closest(".field-container").removeClass("validation-error");
            $(element).closest(".select-type").removeClass("validation-error");
        }
    });
});