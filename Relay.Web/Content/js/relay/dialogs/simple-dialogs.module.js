﻿; (function (window)
{
    "use strict";
    //===================== Info Dialog ===========================
    function InfoDialog(message)
    {
        this.message = message;
        this.openHandler = null;
        this.closeHandler = null;
    }
    InfoDialog.prototype.setOpenEvent = function (eventHandler)
    {
        this.openHandler = eventHandler;
    };
    InfoDialog.prototype.setCloseEvent = function (eventHandler)
    {
        this.closeHandler = eventHandler;
    };
    InfoDialog.prototype.open = function ()
    {
        var _this = this;
        var tempDiv = document.createElement("div");
        tempDiv.innerHTML = createInfoModalWindowHtml(this.message);
        document.body.appendChild(tempDiv);

        $('#infoDialog', tempDiv).on('hidden.bs.modal', function ()
        {
            $(tempDiv).remove();
            if (_this.closeHandler)
            {
                _this.closeHandler();
            }
        });

        if (this.openHandler)
        {
            this.openHandler();
        }
        $('#infoDialog', tempDiv).modal('show');
    };
    function createInfoModalWindowHtml(message)
    {
        return '<div id="infoDialog" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false" class="modal fade info-dialog">' +
        '<div class="modal-dialog">' +
                            '<div class="modal-content">' +
                '<div class ="modal-body">' +
                   ' <div class="row">' +
                        '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">' +
                            '<div class="message">' + message + '</span>' +
                        ' </div>' +
                         '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-top-30 text-center">' +
                              '<button type="button" data-dismiss="modal" aria-hidden="true" class="btn-ok">Ok</button>' +
                        ' </div>' +
                   ' </div>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';

    }
    //================= Confirmation Dialog =======================
    function ConfirmDialog(title, message, okBtnText, cancelBtnText)
    {
        this.title = title;
        this.message = message;
        this.okBtnText = okBtnText || "OK";
        this.cancelBtnText = cancelBtnText || "Cancel";
        //-- handlers -------
        this.openHandler = null;
        this.okHandler = null;
        this.cancelHandler = null;
    }
    ConfirmDialog.prototype.setOpenEvent = function (eventHandler)
    {
        this.openHandler = eventHandler;
    };
    ConfirmDialog.prototype.setOkEvent = function (eventHandler)
    {
        this.okHandler = eventHandler;
    };
    ConfirmDialog.prototype.setCancelEvent = function (eventHandler)
    {
        this.cancelHandler = eventHandler;
    };
    ConfirmDialog.prototype.open = function ()
    {
        var tempDiv = document.createElement("div");
        tempDiv.innerHTML = createConfirmModalWindowHtml(this);
        document.body.appendChild(tempDiv);

        if (this.okHandler)
        {
            $(".btn-ok", tempDiv).click(this.okHandler);
        }

        if (this.cancelHandler)
        {
            $(".btn-cancel", tempDiv).click(this.cancelHandler);
        }

        $('#confirmDialog', tempDiv).on('hidden.bs.modal', function ()
        {
            $(tempDiv).remove();
        });

        if (this.openHandler)
        {
            this.openHandler();
        }
        $('#confirmDialog', tempDiv).modal('show');
    };
    function createConfirmModalWindowHtml(confirmDialog)
    {
        return '<div id="confirmDialog" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false" class="modal fade confirm-dialog">' +
                '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<div class="row vertical-center">' +
                            '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">' +
                                '<div id="modalTitle" class="modal-title">' + confirmDialog.title + '</div>' +
                           ' </div>' +
                      '</div>' +
                        '</div>' +
                        '<div class ="modal-body">' +
                           ' <div class="row">' +
                                '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">' +
                                    '<div class="message">' + confirmDialog.message + '</span>' +
                                ' </div>' +
                                 '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding margin-top-30">' +
                                     ' <div class="row vertical-center">' +
                                            '<div class="col col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding text-center">' +
                                              '<button type="button" data-dismiss="modal" aria-hidden="true" class="btn-ok">' + confirmDialog.okBtnText + '</button>' +
                                            ' </div>' +
                                              '<div class="col col-lg-2 col-md-2 col-sm-2 col-xs-2 no-padding text-center">' +
                                                '<span class="separator">|</span>'+
                                            ' </div>' +
                                            '<div class="col col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding text-center">' +
                                              '<button type="button" data-dismiss="modal" aria-hidden="true" class="btn-cancel">' + confirmDialog.cancelBtnText + '</button>' +
                                            ' </div>' +
                                     ' </div>' +
                                ' </div>' +
                           ' </div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';



    }
    //==================== Error Dialog ===========================
    function ErrorDialog()
    {
        this.openHandler = null;
        this.closeHandler = null;
    }
    ErrorDialog.prototype.setOpenEvent = function (eventHandler)
    {
        this.openHandler = eventHandler;
    };
    ErrorDialog.prototype.setCloseEvent = function (eventHandler)
    {
        this.closeHandler = eventHandler;
    };
    ErrorDialog.prototype.open = function ()
    {
        var _this = this;
        var tempDiv = document.createElement("div");
        tempDiv.innerHTML = createErrorModalWindowHtml();
        document.body.appendChild(tempDiv);

        $('#errorDialog', tempDiv).on('hidden.bs.modal', function ()
        {
            $(tempDiv).remove();
            if (_this.closeHandler)
            {
                _this.closeHandler();
            }
        });

        if (this.openHandler)
        {
            this.openHandler();
        }
        $('#errorDialog', tempDiv).modal('show');
    };
    function createErrorModalWindowHtml()
    {
        return '<div id="errorDialog" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true" class="modal fade error-dialog">' +
           '<div class="modal-dialog">' +
               '<div class="modal-content">' +
                   '<div class ="modal-body">' +
                      ' <div class="row">' +
                           '<div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding text-center">' +
                                  '<div class="oops">' +
                                        'Oops!' +
                                    '</div>' +

                                '<p style="color: white;">An error has occured.</p>' +
                                    '<p class="no-margin" style="color: white;margin-bottom: 0px;">' +
                                        'Please contact ' +
                                        '<a href="#" class="link">info@ihandover.co</a>' +
                                    '</p>' +
                           ' </div>' +
                      ' </div>' +
                   '</div>' +
               '</div>' +
           '</div>' +
       '</div>';

    }
    //=============================================================
    window.Relay = window.Relay || {};
    window.Relay.InfoDialog = function (message)
    {
        return new InfoDialog(message);
    };
    window.Relay.ConfirmDialog = function (title, message, okBtnText, cancelBtnText)
    {
        return new ConfirmDialog(title, message, okBtnText, cancelBtnText);
    };
    window.Relay.ErrorDialog = function ()
    {
        return new ErrorDialog();
    };
    //=============================================================
})(window);