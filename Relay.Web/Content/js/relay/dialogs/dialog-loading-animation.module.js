﻿; (function () {
    //=============================================================
    var dialogLoadingAnimation = {
        show: showAnimation,
        hide: hideAnimation
    };

    var animationHtml = "<div class='popup-loading-animation' style='display: block;'>" +
                            "<div class='spinner'>" +
                                "<div class='spinner-container container1'>" +
                                    "<div class='circle1'></div>" +
                                    "<div class='circle2'></div>" +
                                    "<div class='circle3'></div>" +
                                    "<div class='circle4'></div>" +
                                "</div>" +
                                "<div class='spinner-container container2'>" +
                                    "<div class='circle1'></div>" +
                                    "<div class='circle2'></div>" +
                                    "<div class='circle3'></div>" +
                                    "<div class='circle4'></div>" +
                                "</div>" +
                                "<div class='spinner-container container3'>" +
                                    "<div class='circle1'></div>" +
                                    "<div class='circle2'></div>" +
                                    "<div class='circle3'></div>" +
                                    "<div class='circle4'></div>" +
                                "</div>" +
                            "</div>" +
                        "</div>";

    var timer = null;
    var animationDiv = null;

    function showAnimation(dialog, delay) {
        if (!dialog)
            return;

        if (delay) {
            timer = setTimeout(function () {
                appendAnimation(dialog);
            }, delay);
        }
        else {
            appendAnimation(dialog);
        }
    }

    function appendAnimation(dialog) {
        animationDiv = document.createElement("div");
        animationDiv.innerHTML = animationHtml;

        $(".modal-body", dialog).append(animationDiv);
    }

    function hideAnimation() {

        if (timer) {
            clearTimeout(timer);
            timer = null;
        }

        if (animationDiv) {
            $(animationDiv).remove();
        }
    }

    //=============================================================
    window.Relay = window.Relay || {};
    window.Relay.DialogLoadingAnimation = dialogLoadingAnimation;
    //=============================================================
})();