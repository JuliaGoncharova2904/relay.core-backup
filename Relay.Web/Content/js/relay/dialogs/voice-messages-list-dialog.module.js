﻿(function () {
    //===========================================================================
    //  id - cintainer ID
    //  type - mode type
    //  container - container type
    //===========================================================================
    var Types = {
        Draft: 1,
        Received: 2
    };
    var Containers = {
        Topic: 1,
        Task: 2
    }
    //===================== Attachments Dialog ==================================
    function VoiceMessagesDialog(id, type, container) {
        this.id = id;
        this.type = type;
        this.container = container;
        this.openHandler = null;
        this.closeHandler = null;
    }
    VoiceMessagesDialog.prototype.open = function () {
        var _this = this;
        //--------------------------
        var configDialog = {
            id: this.id,
            type: this.type,
            dialogId: "voiceMessagesListDialog"
        };

        switch (this.container) {
            case Containers.Topic:
                configDialog.dialogUrl = "/VoiceMessages/VoiceMessagesListForTopicDialog";
                configDialog.addFormUrl = "/VoiceMessages/AddVoiceMessageForTopic";
                configDialog.removeUrl = "/VoiceMessages/RemoveVoiceMessageFromTopic";
                break;
            case Containers.Task:
                configDialog.dialogUrl = "/VoiceMessages/VoiceMessagesListForTaskDialog";
                configDialog.addFormUrl = "/VoiceMessages/AddVoiceMessageForTask";
                configDialog.removeUrl = "/VoiceMessages/RemoveVoiceMessageFromTask";
                break;
            default:
                return;
        }
        //--------------------------
        $('#loading-animation').show();
        if (_this.openHandler) {
            _this.openHandler();
        }
        //--------------------------
        $.ajax({
            url: configDialog.dialogUrl + '/' + configDialog.id,
            method: "GET"
        })
        .done(function (result) {
            var formDialog = new _Dialog(configDialog);
            formDialog.buildDialog(result);
            formDialog.closeHandler = _this.closeHandler;
            formDialog.open();
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        })
        .always(function () {
            $('#loading-animation').hide();
        });
        //--------------------------
    };
    VoiceMessagesDialog.prototype.setOpenEvent = function (eventHandler) {
        this.openHandler = eventHandler;
    };
    VoiceMessagesDialog.prototype.setCloseEvent = function (eventHandler) {
        this.closeHandler = eventHandler;
    };
    //===========================================================================
    function _Dialog(config) {
        this.config = config;
        this.conteiner = null;

        this.slidesContainer = null;
        this.addFormContainer = null;
        this.addButton = null;

        this.closeHandler = null;
    }
    _Dialog.prototype.open = function () {
        //----------- Voice Messages List --------------
        new Swiper($(".swiper-container", this.conteiner), {
            // slidesPerView: 2,
            direction: 'vertical',
            height: 30,
            nextButton: $(".swiper-next", this.conteiner),
            prevButton: $(".swiper-prev", this.conteiner)
        })
        //----------------------------------------------
        this.addFormContainer.slideUp();
        $('#' + this.config.dialogId, this.conteiner).modal('show');
    };
    _Dialog.prototype.buildDialog = function (windowHtml) {
        this.conteiner = document.createElement("div");
        this.conteiner.innerHTML = windowHtml;
        document.body.appendChild(this.conteiner);

        this.slidesContainer = $(".swiper-wrapper", this.conteiner).first();
        this.addFormContainer = $(".add-voice-message", this.conteiner).first();
        this.addButton = $(".add-voice-message-btn", this.conteiner).first();

        var _this = this;

        $('#' + this.config.dialogId, this.conteiner).on('hidden.bs.modal', function () {
            $(_this.conteiner).remove();
            if (_this.closeHandler) {
                _this.closeHandler();
            }
        });



        switch (this.config.type) {
            case Types.Draft:
                this.addButton.click(this.openAddVoiceMessageForm.bind(this));
                $(".remove-message-btn", this.slidesContainer).click(this.removeVoiceMessage.bind(this));
                break;
            case Types.Received:
                this.addButton.remove();
                $(".remove-message-btn", this.slidesContainer).children().remove();
                break;
        }
    };
    _Dialog.prototype.removeVoiceMessage = function (event) {

        var _this = this;
        var voiceMessageId = event.currentTarget.id;
        var voiceMessageName = $(event.currentTarget).parent().find(".name-block .note").html();

        var confirmDialog = Relay.ConfirmDialog(
                                                    "Remove Attachment",
                                                    "Attachment: '" + voiceMessageName + "' will be removed. Do you agree with it?",
                                                    "Yes",
                                                    "No"
                                                );

        confirmDialog.setOkEvent(function () {
            $.ajax({
                url: _this.config.removeUrl,
                method: "POST",
                data: { id: _this.config.id, voiceMessageId: voiceMessageId }
            })
            .done(function (result) {
                _this.reloadVoiceMessagesList();
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            });
        });

        confirmDialog.open();
    };
    _Dialog.prototype.reloadVoiceMessagesList = function () {

        var _this = this;

        $.ajax({
            url: this.config.dialogUrl + '/' + this.config.id,
            method: "GET"
        })
        .done(function (result) {
            var slides = $(result).find(".swiper-wrapper");
            _this.slidesContainer.html(slides.html());
            switch (_this.config.type) {
                case Types.Draft:
                    $(".remove-message-btn", _this.slidesContainer).click(_this.removeVoiceMessage.bind(_this));
                    break;
                case Types.Received:
                    $(".remove-message-btn", _this.slidesContainer).children().remove();
                    break;
            }
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    };
    _Dialog.prototype.openAddVoiceMessageForm = function (event) {
        var _this = this;

        $.ajax({
            url: this.config.addFormUrl + '/' + this.config.id,
            method: "GET"
        })
        .done(function (result) {
            _this.addFormContainer.html(result);
            $(".btn-file", _this.addFormContainer).click(_this.submitAddForm.bind(_this));

            Relay.InitInputFields(_this.addFormContainer);

            _this.addFormContainer.slideDown();
            _this.addButton.css('display', 'none');
        })
        .fail(function () {
            Relay.ErrorDialog().open();
        });
    }
    _Dialog.prototype.submitAddForm = function () {

        var _this = this;
        var form = $("form", this.addFormContainer).first();

        $.validator.unobtrusive.parse(form);
        if (form.valid()) {
            $.ajax({
                url: _this.config.addFormUrl,
                method: "POST",
                processData: false,
                contentType: false,
                data: new FormData(form[0])
            })
            .done(function (result) {
                if (result === "ok") {
                    _this.reloadVoiceMessagesList();
                    _this.addFormContainer.slideUp();
                    _this.addButton.css('display', 'block');
                } else {
                    _this.addFormContainer.html(result);
                    $(".btn-file", _this.addFormContainer).click(_this.submitAddForm.bind(_this));
                    Relay.InitInputFields(_this.addFormContainer);
                }
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            });
        }
    };
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.VoiceMessagesDialog = function (id, type, container) {
        return new VoiceMessagesDialog(id, type, container);
    };
    //---------------
    window.Relay.VoiceMessagesDialog.Types = Types;
    window.Relay.VoiceMessagesDialog.Containers = Containers;
    //===========================================================================
})();