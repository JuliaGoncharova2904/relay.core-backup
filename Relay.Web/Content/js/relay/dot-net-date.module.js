﻿; (function (window)
{
  "use strict";
  //=================================================================================
  function dateStringToDate(dateString)
  {

    if (!dateString)
      return null;

    var momentDate = moment(dateString, "DD/MM/YYYY HH:mm:ss");

    var dateAndTimeStrings = dateString.split(' ');
    var dateParams = dateAndTimeStrings[0].split('/');
    var timeParams = dateAndTimeStrings[1].split(':');


    //var jsDate = createDateAsUTC(dateParams[2] + '-' + dateParams[1] + '-' + dateParams[0]);

    var jsDate = momentDate.toDate();

    jsDate.setHours(timeParams[0]);
    jsDate.setMinutes(timeParams[1]);
    jsDate.setSeconds(timeParams[2]);

    return jsDate;
  }


  function createDateAsUTC(date)
  {
    return new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
  }

  function convertDateToUTC(date)
  {
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
  }


  function dateToDateString(date)
  {

    if (!date)
      return '';

    return formatDateNumber(date.getDate()) +
            '/'
            + formatDateNumber(date.getMonth() + 1) +
            '/' + date.getFullYear() +
            ' ' + formatDateNumber(date.getHours()) +
            ':' + formatDateNumber(date.getMinutes()) +
            ':' + formatDateNumber(date.getSeconds());
  }

  function formatDateNumber(dateNumber)
  {
    if (dateNumber < 10)
    {
      return "0" + dateNumber;
    }

    return dateNumber.toString();
  }
  //=================================================================================
  window.Relay = window.Relay || {};
  window.Relay.DotNetDate = {
    dateStringToDate: dateStringToDate,
    dateToDateString: dateToDateString
  };
  //=================================================================================
})(window);