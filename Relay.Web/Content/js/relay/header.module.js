﻿$(document).ready(function () {
    "use strict";
    //================== Header module =======================================
    var headerPanel = $(".main-nav-bar").first();
    var mainMenuBtn = $("#main-menu-btn");
    var mainMenu = $("#main-menu");
    var userFoto = $("#user-info .user-photo-container");
    var userData = $("#user-info .user-data");
    var isMultiTenancy = $($(mainMenu).parent()).attr("is-multi-tenancy") === "True";
    //-----------------------------------------
    var isOpened = false;
    //-----------------------------------------
    var header = {
        open: openMenu,
        close: closeMenu,
        disable: Header_DisableAllEvents,
        enable: Header_EnableAllEvents
    };
    initialize();
    //-----------------------------------------
    function initialize() {
      
        mainMenuBtn.click(function () {
            if (isOpened)
                $.debounce(400, closeMenu);
            else
                openMenu();
        });

        userFoto.click(function () {
            var formDialog = Relay.SettingsDialog();
            formDialog.setCloseEvent(reloadUserInfo);
            formDialog.open();
        });

        //var searchText = window.location.search.split('SearchText=')[1];
        //  debugger;

        //$.ajax({
        //    url: "/SearchResults/GetSearchStrig",
        //    method: "GET"
        //})
        //    .done(function (data) {
        //        debugger;
        //    });

    }
    //-----------------------------------------
    function reloadUserInfo() {
        updateUserFoto();
        if (Relay.ReportBuilderPanel) {
            Relay.ReportBuilderPanel.reload();
        }
        //updateUserData();
    }
    function updateUserFoto() {
        var fotoImg = $("img", userFoto).first();
        var fotoSrc = fotoImg.attr("src");
        var randNumber = Math.floor(Math.random() * 1000);
        fotoImg.attr("src", fotoSrc + '&' + randNumber.toString());
    }
    function updateUserData() {
        $.ajax({
            url: "/Profile/GetEmployeeInfo",
            method: "POST"
        })
        .done(function (data) {
            var name = document.createElement("span");
            name.innerText = data.Name;

            var position = document.createElement("span");
            position.innerText = data.Position;

            userData.empty();
            userData.append(name);
            userData.append(position);
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    }
    //-----------------------------------------
    function closeMenu() {
        isOpened = false;
        mainMenu.unbind("clickoutside");
        animate(animate.Close);
    }
    function openMenu() {
        isOpened = true;
        animate(animate.Open);
        setTimeout(function () {
            mainMenu.bind("clickoutside", $.debounce(400, closeMenu));
        }, 100);
    }
    //---------------------
    function animate(action) {
        var top;

        if (isMultiTenancy) {
            if (action) {
                mainMenu.attr("style", "display: block;");
            } else {
                mainMenu.attr("style", "display: none;");
            }

            //top = action ? "-1px" : "-1040px";
        } else {
            top = action ? "+=1040" : "-=1040";

            mainMenu.animate({
                top: top
            }, {
                duration: 500,
                specialEasing: {
                    top: "linear"
                }
            });
        }
        
    }
    animate.Open = true;
    animate.Close = false;
    //-----------------------------------------
    function Header_DisableAllEvents() {
        headerPanel.attr("style", "pointer-events:none;");
    }
    function Header_EnableAllEvents() {
        headerPanel.removeAttr("style");
    }
    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.Header = header;
    //========================================================================
});