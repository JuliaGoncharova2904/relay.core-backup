﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //      completeFieldName: 'name'
    //  }
    //================================== Share Button ================================================
    var dataAttributeName = "topic-val";
    //-------------------------------------------------
    function ArchivedBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }

    ArchivedBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $(".archive-btn", topic.topicElement).first();

        topic.model.IsInArchive = control.attr(dataAttributeName) === "True";
        if (control.length) {
            this.control = control;
            //----------
            if (!this.config.readOnly) {
                control.click(function ()
                {
                    if (!_this.config.completeFieldName || topic.model[_this.config.completeFieldName] == "Complete") {
                        topic.model.IsInArchive = !topic.model.IsInArchive;
                        topic.updateServerData();
                        _this.update();
                    }
                    else {
                        Relay.InfoDialog("Task cannot be archived because is not completed.").open();
                    }
                });
            }
            //----------
            return true;
        }

        return false;
    };
    ArchivedBtn.prototype.update = function () {
        if (this.topic.model.IsInArchive) {
            this.control.removeClass("archive-inactive-btn");
            this.control.addClass("archive-active-btn");
        }
        else {
            this.control.removeClass("archive-active-btn");
            this.control.addClass("archive-inactive-btn");
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.ArchivedBtn = function (config) {
            return new ArchivedBtn(config);
        };
    }
    else {
        console.error("Archived Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);