﻿; (function (window) {
    //========================================================================================
    //  config {
    //  }
    //=============================== Text Field =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function NowTaskStatusField(config) {
        this.config = config;
        this.topic = null;
        this.control = null;
    }
    NowTaskStatusField.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $(".status", topic.topicElement).first();
        if (control.length) {
            this.control = control;
            topic.model.Status = control.attr("status");
            topic.model.ChildCompleteStatus = control.attr(dataAttributeName);

            return true;
        }

        return false;
    };
    NowTaskStatusField.prototype.update = function () {
        //-------------------
        if (this.topic.model.Status == "Now") {
            this.control.text(this.topic.model.ChildCompleteStatus);
        }
        else {
            this.control.html("<div class=\"btn none-btn btn-circle\"><span class=\"glyphicon span\"></span></div>");
        }
        //-------------------
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.NowTaskStatusField = function (config) {
            return new NowTaskStatusField(config);
        };
    }
    else {
        console.error("Now Task Status Field control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);