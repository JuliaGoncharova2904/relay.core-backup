﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      dialogType: (type)
    //      changeHandler: (handler)
    //  }
    //=================================== Tasks Button ==============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function TasksBtn(config) {
        this.config = config;
        this.topic = null;
        this.control = null;
    }
    TasksBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.tasks-btn', topic.topicElement).first();
        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            this.control = control;
            //----------
            topic.model.HasTasks = control.attr(dataAttributeName) === "true";

            control.click(function () {
                if (topic.model.HasTasks === false) {
                    var tasksListDialog = Relay.FormDialog({
                        url: "/Tasks/AddTaskFormDialog",
                        method: "GET",
                        data: { TopicId: topic.model.Id }
                    });

                    tasksListDialog.setSubmitEvent(function () {
                        topic.reloadServerData();
                        _this.update();
                    });

                    tasksListDialog.open();
                }
                else {
                    if (!topic.model.IsNullReport) {
                        var tasksListDialog = Relay.TasksListDialog(_this.config.dialogType, topic.model.Id);
                        tasksListDialog.setCloseEvent(function () {
                            if (_this.config.changeHandler)
                                _this.config.changeHandler();
                            topic.reloadServerData();
                        });
                        tasksListDialog.open();
                    }
                }

            });
            //if (topic.model.HasTasks === false) {
            //    control.click(function () {
            //        var tasksListDialog = Relay.FormDialog({
            //            url: "/Tasks/AddTaskFormDialog",
            //            method: "GET",
            //            data: { TopicId: topic.model.Id }
            //        });

            //        tasksListDialog.setSubmitEvent(function () {
            //            topic.reloadServerData();
            //            _this.update();
            //            location.reload();
            //        });

            //        tasksListDialog.open();
            //    });
            //}
            //else {
            //    //----------
            //    control.click(function () {
            //        if (!topic.model.IsNullReport) {
            //            var tasksListDialog = Relay.TasksListDialog(_this.config.dialogType, topic.model.Id);
            //            tasksListDialog.setCloseEvent(function () {
            //                if (_this.config.changeHandler)
            //                    _this.config.changeHandler();
            //                topic.reloadServerData();
            //            });
            //            tasksListDialog.open();
            //        }
            //    });
            //}
            //----------
            return true;
        }

        return false;
    };
    TasksBtn.prototype.update = function () {
        if (this.topic.model.HasTasks) {
            //-- Rotation builder classes ---
            this.control.removeClass('ok-inactive-action-btn');
            this.control.addClass('ok-active-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('task-inactive-btn');
            this.control.addClass('task-active-btn');
        }
        else {
            //-- Rotation builder classes ---
            this.control.removeClass('ok-active-action-btn');
            this.control.addClass('ok-inactive-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('task-active-btn');
            this.control.addClass('task-inactive-btn');
        }
    };


    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TasksBtn = function (clickHandler) {
            return new TasksBtn(clickHandler);
        };
    }
    else {
        console.error("Tasks Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);