﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //      changeHandler: (handler)
    //  }
    //============================== Transfered Button ==============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function TransferredBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }
    TransferredBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.arrows-btn', topic.topicElement).first();
        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            //----------
            this.control = control;
            topic.model.IsFeedbackRequired = control.attr(dataAttributeName) === "true";
            //----------
            if (!this.config.readOnly) {
                control.click(function () {
                    if (!topic.model.IsNullReport) {
                        topic.model.IsFeedbackRequired = !topic.model.IsFeedbackRequired;
                        _this.update();

                        topic.addChangeEventLisetner(_this.config.changeHandler);
                        topic.updateServerData();
                    }
                });
            }
            //----------
            return true;
        }

        return false;
    };
    TransferredBtn.prototype.update = function () {
        if (this.topic.model.IsFeedbackRequired) {
            //-- Rotation builder classes ---
            this.control.removeClass('arrows-inactive-btn');
            this.control.addClass('arrows-active-btn');
            //-- Shift builder classes ------
            this.control.removeClass('hand-back-inactive-btn');
            this.control.addClass('hand-back-active-btn');
        }
        else {
            //-- Rotation builder classes ---
            this.control.removeClass('arrows-active-btn');
            this.control.addClass('arrows-inactive-btn');
            //-- Shift builder classes ------
            this.control.removeClass('hand-back-active-btn');
            this.control.addClass('hand-back-inactive-btn');
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TransferredBtn = function (config) {
            return new TransferredBtn(config);
        };
    }
    else {
        console.error("Transfered Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);