﻿; (function (window) {
    //========================================================================================
    //  config {
    //      modelPropName: "",
    //      elementClass: "",
    //      finalizeOff: false,
    //      clickHandler: handler
    //  }
    //=============================== Text Field =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function TextField(config) {
        this.config = config;
        this.topic = null;
        this.control = null;
    }
    TextField.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $(this.config.elementClass, topic.topicElement).first();
        if (control.length) {
            this.control = control;
            topic.model[this.config.modelPropName] = control.text().trim();
            //----------
            if (this.config.clickHandler) {
                control.click(function () {
                    if (!topic.model.IsNullReport)
                        _this.config.clickHandler(topic.model,
                                                  topic.reloadServerData.bind(topic),
                                                  topic.updateServerData.bind(topic));
                });
            }
            //----------
            return true;
        }

        return false;
    };
    TextField.prototype.update = function () {

        var test = this.controlPvA;
        var topicElem = this.topic;

        //$.ajax({
        //    url: "/Summary/GetPvAByTopicId",
        //    method: "GET",
        //    data: { topicId: this.topic.model.Id }
        //})
        //    .done(function (data) {
        //        $(test).empty().html(data);
        //    });

        //-------------------
        this.control.text(this.topic.model[this.config.modelPropName]);
        //-------------------
        if (this.topic.model.IsNullReport) {
            this.control.removeClass('text-blue');
            this.control.parent().removeAttr("style");
        }
        else {
            if (this.config.clickHandler)
                this.control.parent().attr("style", "cursor: pointer;");
            else
                this.control.parent().removeAttr("style");

            if (this.topic.model.IsFinalized || this.config.finalizeOff)
                this.control.removeClass('text-blue');
            else
                this.control.addClass('text-blue');
        }
        //-------------------
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TextField = function (config) {
            return new TextField(config);
        };
    }
    else {
        console.error("Text Column control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);