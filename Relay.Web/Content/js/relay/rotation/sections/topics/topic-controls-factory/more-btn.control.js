﻿; (function (window) {
    //==================================== More Button ==============================================
    var dataAttributeName = "topic-val";

    //--------------------------------------
    function MoreBtn(config) {
        this.topic = null;
        this.control = null;
        this.config = config;
    }
    MoreBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.more-btn', topic.topicElement).first();
        if (control.length !== 0) {
            this.control = control;
            //----------
            control.click(function () {
                if (!topic.model.IsNullReport || topic.Notes != null && topic.Notes.Contains("All good.")) {
                    if (topic.IsExpanded) {
                        topic.collapse();
                       // debugger;
                       // _this.update();
                        //topic.updateServerData();
                    }
                    else {
                        topic.expand();
                    }
                }
            });
            //----------
            return true;
        }

        return false;
    };
    MoreBtn.prototype.update = function () {
        if (this.topic.model.IsFeedbackRequired || this.topic.model.HasLocation || this.topic.model.SharedCounter > 0 || this.topic.model.HasAttachments || this.topic.model.HasVoiceMessages || this.topic.model.HasTags || this.topic.model.PlannedActualHas) {
            this.control.removeClass('more-inactive-btn');
            this.control.addClass('more-active-btn');
        }
        else {
            this.control.removeClass('more-active-btn');
            this.control.addClass('more-inactive-btn');
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.MoreBtn = function () {
            return new MoreBtn();
        };
    }
    else {
        console.error("More Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);