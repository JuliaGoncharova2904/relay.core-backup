﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false,
    //      type: (type),
    //      warningMessage: "",
    //      changeHandler: (handler)
    //  }
    //============================== Remove Topic Button ============================================
    var Type = {
        task: 0,
        topic: 1
    };
    //----------------------------------------------
    function RemoveTopicBtn(config) {
        this.config = config || {};
        if (!this.config.warningMessage) {
            this.config.warningMessage = "Are you sure you want to delete this item?";
        }
        this.topic = null;
        this.control = null;
    }
    RemoveTopicBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.remove-btn', topic.topicElement).first();

        if (control.length) {
            //----------
            this.control = control;
            //----------
            if (!this.config.readOnly) {
                var ajaxConfig;
                if (_this.config.type === Type.task) {
                    ajaxConfig = {
                        url: "/Tasks/RemoveTask",
                        method: "POST",
                        data: { taskId: topic.model.Id }
                    };
                }
                else if (_this.config.type === Type.topic) {
                    ajaxConfig = {
                        url: "/Topic/RemoveTopic",
                        method: "POST",
                        data: { topicId: topic.model.Id }
                    };
                }
                //----------
                if (ajaxConfig) {
                    control.click(function () {
                        var confirmDialog = Relay.ConfirmDialog(
                                                                    _this.config.warningMessage,
                                                                    "",
                                                                    "Yes",
                                                                    "No"
                                                                );
                        confirmDialog.setOkEvent(function () {
                            //----------
                            $.ajax(ajaxConfig)
                            .done(function () {
                                if (_this.config.changeHandler)
                                    _this.config.changeHandler();
                            })
                            .fail(function () {
                                var errorDialog = Relay.ErrorDialog();
                                errorDialog.setCloseEvent(function () {
                                    location.reload();
                                });
                                errorDialog.open();
                            });
                            //----------
                        });
                        confirmDialog.open();
                    });
                }
            }
            else {
                control.addClass("none-btn").removeClass("remove-btn");
             //   control.tooltipster('destroy');
            }
            //----------
            return true;
        }


        return false;
    };
    RemoveTopicBtn.prototype.update = function () { };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.RemoveTopicBtn = function (config) {
            return new RemoveTopicBtn(config);
        };

        window.Relay.Topic.ControlsFactory.RemoveTopicBtn.Type = Type;
    }
    else {
        console.error("Remove Topic Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);