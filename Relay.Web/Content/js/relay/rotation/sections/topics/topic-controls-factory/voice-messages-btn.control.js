﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      dialogType: (type),
    //      container: (container)
    //      changeHandler: (handler)
    //  }
    //============================== Voice Messages Button ==========================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function VoiceMessagesBtn(config) {
        this.topic = null;
        this.control = null;
        this.config = config;
    }
    VoiceMessagesBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.voice-msg-btn', topic.topicElement).first();
        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            this.control = control;
            //----------
            topic.model.HasVoiceMessages = control.attr(dataAttributeName) === "true";
            //----------
            control.click(function () {
                if (!topic.model.IsNullReport) {
                    var voiceMessagesDialog = Relay.VoiceMessagesDialog(topic.model.Id,
                                                                    _this.config.dialogType,
                                                                    _this.config.container);

                    if (_this.config.dialogType === Relay.VoiceMessagesDialog.Types.Draft) {
                        voiceMessagesDialog.setCloseEvent(function () {
                            topic.reloadServerData();
                            if (_this.config.changeHandler)
                                _this.config.changeHandler();
                        });
                    }

                    voiceMessagesDialog.open();
                }
            });
            //----------
            return true;
        }

        return false;
    };
    VoiceMessagesBtn.prototype.update = function () {
        if (this.topic.model.HasVoiceMessages) {
            //-- Rotation builder classes ---
            this.control.removeClass('sound-inactive-action-btn');
            this.control.addClass('sound-active-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('voice-messages-inactive-btn');
            this.control.addClass('voice-messages-active-btn');
        }
        else {
            //-- Rotation builder classes ---
            this.control.removeClass('sound-active-action-btn');
            this.control.addClass('sound-inactive-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('voice-messages-active-action-btn');
            this.control.addClass('voice-messages-inactive-btn');
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.VoiceMessagesBtn = function (config) {
            return new VoiceMessagesBtn(config);
        };
    }
    else {
        console.error("Voice Messages Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);