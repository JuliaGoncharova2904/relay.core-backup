﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //  }
    //================================= Location Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function PlannedActualBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }
    PlannedActualBtn.prototype.initialize = function (topic) {
        var action;
        var _this = this;
        this.topic = topic;
        var control = $('.planned-actual-btn', topic.topicElement).first();
        this.control = control;

        var controlPvA = $('.plannedActual', topic.topicElement).first();
        this.controlPvA = controlPvA;

        var controlPvANotEdit = $('.plannedActualNoEdit', topic.topicElement).first();
        this.controlPvANotEdit = controlPvANotEdit;


        topic.model.PlannedActualHas = control.attr(dataAttributeName) === "true";
      
        control.click(function () {
            if (!topic.model.IsNullReport) {
                $('.plannedActual', topic.topicElement).toggle(showField);
            }
        });

        controlPvANotEdit.click(function () {
            if (!topic.model.IsNullReport) {
                topic.expand();
            }
        });

        return true;
    };


    function showField() {
        $('.notes-body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    };

    PlannedActualBtn.prototype.update = function () {
        var test = this.controlPvA;
        var test1 = this.controlPvANotEdit;

        var topicElem = this.topic;

        $.ajax({
            url: "/Summary/GetPvAByTopicId",
            method: "GET",
            data: { topicId: this.topic.model.Id }
        })
            .done(function (data) {
                $(test).empty().html(data);
            });

        $.ajax({
            url: "/Summary/GetPvANotEditByTopicId",
            method: "GET",
            data: { topicId: this.topic.model.Id }
        })
            .done(function (data) {
                $(test1).empty().html(data);
            });

        if (this.topic.model.PlannedActualHas) {
           this.control.removeClass('planned-actual-inactive-btn');
           this.control.addClass('planned-actual-active-btn');
        }
        else {
             this.control.removeClass('planned-actual-active-btn');
            this.control.addClass('planned-actual-inactive-btn');
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.PlannedActualBtn = function (config) {
            return new PlannedActualBtn(config);
        };
    }
    else {
        console.error("Planned & Actual Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);