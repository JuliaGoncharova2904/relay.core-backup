﻿(function () {
    //-------------------------------------------------
    function TagsBody(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }

    TagsBody.prototype.initialize = function (topic) {
        this.topic = topic;
        var _this = this;
        this.control = $(".tags-body", topic.topicElement).first();
        return true;
    };

    TagsBody.prototype.update = function () {
        var test = this.control;
        var topic = this.topic;

        $.ajax({
            url: "/Summary/GetTagsBodyByTopicId",
            method: "GET",
            data: { topicId: this.topic.model.Id }
        })
            .done(function (data) {
                $(test).empty().html(data);  
            });
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TagsBody = function (config) {
            return new TagsBody(config);
        };
    }
    else {
        console.error("Error in get tag body.");
    }
    //===============================================================================================
})();