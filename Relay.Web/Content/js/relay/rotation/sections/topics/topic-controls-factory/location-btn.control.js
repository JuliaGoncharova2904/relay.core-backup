﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //  }
    //================================= Location Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function LocationBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }
    LocationBtn.prototype.initialize = function (topic) {
        var action;
        var _this = this;
        this.topic = topic;
        var control = $('.location-btn', topic.topicElement).first();
        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            this.control = control;
            //----------
            topic.model.HasLocation = control.attr(dataAttributeName) === "true";
            //----------
            if (!this.config.readOnly || topic.model.HasLocation) {
                control.click(function () {
                    if (!topic.model.IsNullReport) {

                        if (_this.config.readOnly)
                            action = "DetailsLocationFormDialog";
                        else
                            action = topic.model.HasLocation ? "EditLocationFormDialog" : "CreateLocationFormDialog";

                        var formDialog = Relay.FormDialog({
                            url: "/Topic/" + action + "?topicId=" + topic.model.Id,
                            method: "GET",
                            dialogId: "locationFormDialog"
                        })
                        formDialog.setSubmitEvent(topic.reloadServerData.bind(topic));
                        formDialog.open();
                    }
                });
            }
            //----------
            return true;
        }

        return false;
    };
    LocationBtn.prototype.update = function () {
        if (this.topic.model.HasLocation) {
            //-- Rotation builder classes ---
            this.control.removeClass('pin-inactive-action-btn');
            this.control.addClass('pin-active-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('location-inactive-btn');
            this.control.addClass('location-active-btn');
        }
        else {
            //-- Rotation builder classes ---
            this.control.removeClass('pin-active-action-btn');
            this.control.addClass('pin-inactive-action-btn');
            //-- Shift builder classes ------
            this.control.removeClass('location-active-btn');
            this.control.addClass('location-inactive-btn');
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.LocationBtn = function (config) {
            return new LocationBtn(config);
        };
    }
    else {
        console.error("Location Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);