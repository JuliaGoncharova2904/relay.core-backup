﻿; (function (window) {

    //-------------------------------------------------
    function ContributorIcon(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }

    ContributorIcon.prototype.initialize = function (topic) {
        this.topic = topic;
        this.control = $(".contributes-users", topic.topicElement).first();
        return true;
    };

    ContributorIcon.prototype.update = function () {
        var test = this.control;

        $.ajax({
            url: "/Summary/GetContributorUsersByTopicId",
            method: "GET",
            data: { topicId: this.topic.model.Id }
        })
            .done(function (data)
            {
                $(test).empty().html(data);
            });
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.ContributorIcon = function (config) {
            return new ContributorIcon(config);
        };
    }
    else {
        console.error("Error in get contributor icon.");
    }
    //===============================================================================================
})(window);