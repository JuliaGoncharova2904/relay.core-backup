﻿//import { debuglog } from "util";

; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //  }
    //================================== Share Button ================================================
    var dataAttributeName = "topic-val";
    //-------------------------------------------------
    function ShareBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }

    ShareBtn.prototype.initialize = function (topic) {
        
        this.topic = topic;
        var control = $('.share-btn', topic.topicElement).first();

        topic.model.SharedCounter = control.attr(dataAttributeName);

        if (control.length) {
            this.control = control;
            var _this = this;
            var topicId = this.topic.model.Id;
            //----------
            if (!this.config.readOnly) {
                control.click(function ()
                {
                    if (!topic.model.IsNullReport)
                    {
                        var shareTopicDialog = Relay.FormDialog({
                            url: "/ShareItem/ShareTopicFormDialog?topicId=" + topic.model.Id,
                            method: "GET",
                            dialogId: "shareTopicDialog"
                        });
                        shareTopicDialog.setSubmitEvent(function ()
                        {
                            topic.reloadServerData();
                        });
                        shareTopicDialog.open();
                    }
                });
            }

            //----------
            return true;
        }

        return false;
    };
    ShareBtn.prototype.update = function () {
       var test = this.topic.model;
        var control = this.control;

        if (this.topic.model.SharedCounter > 0) {
            //-----------------------
            this.control.removeClass("share-inactive-btn");
            this.control.addClass("share-active-btn");
            //-----------------------

            var shareCount = 0;
            $.ajax({
                url: "/Summary/GetSharedCounter",
                method: "GET",
                data: { topicId: this.topic.model.Id }
            })
                .done(function (data) {
                    var sharedCounter = data;
                    var counterNode = control.children();

                    if (sharedCounter > 99) {
                        counterNode.text("99+");
                    }
                    else {
                        counterNode.text(sharedCounter);
                    }
               });

            //var sharedCounter = this.topic.model.SharedCounter;
            //var counterNode = this.control.children();

            //if (sharedCounter > 99) {
            //    counterNode.text("99+");
            //}
            //else {
            //    counterNode.text(sharedCounter);
            //}
            //-----------------------
        }
        else {
            this.control.removeClass("share-active-btn");
            this.control.addClass("share-inactive-btn");
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.ShareBtn = function (config) {
            return new ShareBtn(config);
        };
    }
    else {
        console.error("Share Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);