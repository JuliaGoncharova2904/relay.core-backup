﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //      changeHandler: (handler)
    //  }
    //================================= Notes Field =================================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function NotesField(config) {
        this.config = config || {};
        this.topic = null;
        this.staticText = null;
        this.textArea = null;
        this.hasPvA = null;
    }
    NotesField.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var staticText = $('.note-short', topic.topicElement).first();

        var textArea = $(".field-of-edit .change-note", topic.topicElement).first();
        if (staticText.length && textArea.length) {
            this.staticText = staticText;
            this.textArea = textArea;
            //----------
            topic.model.Notes = staticText.text();
            textArea.val(topic.model.Notes);

            //----------
            formatText(staticText);
            //----------
            if (!this.config.readOnly) {
                textArea.focusout(function () {
                    if (!topic.model.IsNullReport) {
                        topic.model.Notes = textArea.val();
                        staticText.text(topic.model.Notes);
                        topic.addChangeEventLisetner(_this.config.changeHandler);
                        topic.updateServerData();
                    }
                });
            }
            else {
                this.textArea.attr("readonly", "");
            }
            //----------
            staticText.click(function () {
                if (!topic.model.IsNullReport) {
                    topic.expand();
                }
            });
            //----------
            return true;
        }

        return false;
    };
    NotesField.prototype.update = function () {

        var test = this.controlPvA;
        var topicElem = this.topic;

        //$.ajax({
        //    url: "/Summary/GetPvAByTopicId",
        //    method: "GET",
        //    data: { topicId: this.topic.model.Id }
        //})
        //    .done(function (data) {
        //        $(test).empty().html(data);
        //    });

        if (this.topic.model.IsNullReport) {
            if (this.staticText) {
                this.topic.model.Notes = '';
                this.staticText.removeClass('text-blue');
                this.staticText.text("All good.");
            }
        }
        else {
            if (this.staticText && this.textArea) {
                //------------------------
                if (!this.config.readOnly) {
                    this.staticText.text(this.topic.model.Notes);
                    this.textArea.val(this.topic.model.Notes);
                    formatText(this.staticText);
                }
                //------------------------
                if (this.topic.model.IsFinalized) {
                    this.staticText.removeClass('text-blue');
                    this.textArea.removeClass('text-blue');
                }
                else {
                    this.staticText.addClass('text-blue');
                    this.textArea.addClass('text-blue');
                }
                //------------------------
            }
        }
        if (this.config.readOnly) {
            this.staticText.removeClass('text-blue');
            this.textArea.removeClass('text-blue');
        }
    };
    //------------- Format text logic --------------------------
    function formatText(textElement) {
        textElement.html(convertTextToHtml(textElement.text()));

        textElement.attr("style", "display: inline;");
        textElement.dotdotdot({ height: 40 });
        textElement.removeAttr("style");
    }
    function convertTextToHtml(text) {

        var continueList = false;
        var resultHtml = "";
        var lines = text.split("\n");

        lines.forEach(function (line, index, lines) {
            var listElements = line.trim().split("*");

            if (listElements[0].length > 0) {
                if (continueList) {
                    continueList = false;
                    resultHtml += "</ul>";
                }

                 resultHtml += ("<div>" + listElements[0] + "</div>");
            }

            if (listElements.length > 1) {
                if (!continueList) {
                    continueList = true;
                    resultHtml += "<ul>";
                }

                for (var i = 1; i < listElements.length; ++i) {
                    resultHtml += ("<li>" + listElements[i] + "</li>");
                }
            }
        });

        return resultHtml;
    }
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.NotesField = function (config) {
            return new NotesField(config);
        };
    }
    else {
        console.error("Notes Field control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);