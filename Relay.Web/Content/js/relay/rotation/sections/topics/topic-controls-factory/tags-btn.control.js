﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //  }
    //================================= Location Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function TagsBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }
    TagsBtn.prototype.initialize = function (topic) {
        var action;
        var _this = this;
        this.topic = topic;
        var control = $('.tags-btn', topic.topicElement).first();
        this.control = control;

        topic.model.HasTags = control.attr(dataAttributeName) === "true";

        control.click(function () {
            if (!topic.model.IsNullReport) {
                var res = _this.config.dialogType;

                if (res == "Draft") {
                    $('.tags', topic.topicElement).toggle(showTagField);
                }
                if (res == "Received") {
                    $('.tags-recieved', topic.topicElement).toggle(showTagField);
                }
                
            }
        });

        return true;
    };

    function showTagField() {
        $('.notes-body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    }

    TagsBtn.prototype.update = function () {
        if (this.topic.model.HasTags) {
           this.control.removeClass('tags-inactive-btn');
            this.control.addClass('tags-active-btn');
        }
        else {
             this.control.removeClass('tags-active-btn');
            this.control.addClass('tags-inactive-btn');
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TagsBtn = function (config) {
            return new TagsBtn(config);
        };
    }
    else {
        console.error("Tags Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);