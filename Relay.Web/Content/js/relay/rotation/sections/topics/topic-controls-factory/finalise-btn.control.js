﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //  }
    //============================== Null Report Button =============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function FinaliseBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }
    FinaliseBtn.prototype.initialize = function (topic) {
        this.topic = topic;
        var _this = this;
        var control = $('.finalise-btn', topic.topicElement).first();
        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            this.control = control;
            //----------
            topic.model.IsFinalized = control.attr(dataAttributeName) === "true";
            //----------
            if (this.config.readOnly) {
                control.remove();
            }
            else{
                control.click(function () {
                    if (!topic.model.IsNullReport) {
                        if (!topic.model.IsFinalized) {
                            topic.model.IsFinalized = true;
                            topic.collapse();
                            topic.updateServerData();
                        }
                        else {
                            topic.collapse();
                            _this.update();
                            topic.updateServerData();
                        }
                    }
                });
            }
            //----------
            return true;
        }

        return false;
    };
    FinaliseBtn.prototype.update = function () {
        if (!this.config.readOnly) {
            if (this.topic.model.IsFinalized) {
                this.control.removeClass('accept-btn-blue');
                this.control.addClass('accept-btn-grey');
            }
            else {
                this.control.removeClass('accept-btn-blue');
                this.control.addClass('accept-btn-blue');
            }
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.FinaliseBtn = function (config) {
            return new FinaliseBtn(config);
        };
    }
    else {
        console.error("Finalise Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);