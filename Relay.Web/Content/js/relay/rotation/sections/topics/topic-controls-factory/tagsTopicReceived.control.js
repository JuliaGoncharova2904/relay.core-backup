﻿; (function (window) {
    var dataAttributeName = "topic-val";
    var dataAttributeNameTagsMore = "topic-MoreTags";
    //-------------------------------------------------
    function TagsTopicReceived(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }

    TagsTopicReceived.prototype.initialize = function (topic) {
        this.topic = topic;
        var _this = this;
        this.control = $(".tags-recieved", topic.topicElement).first();
        topic.model.FirstTagTopic = this.control.attr(dataAttributeName) === "true";

        return true;
    };

    TagsTopicReceived.prototype.update = function () {
        var test = this.control;
        $.ajax({
            url: "/Summary/GetTagsReceivedByTopicId",
            method: "GET",
            data: { topicId: this.topic.model.Id }
        })
            .done(function (data) {
                $(test).empty().html(data);
            });
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TagsTopicReceived = function (config) {
            return new TagsTopicReceived(config);
        };
    }
    else {
        console.error("Error in get tag topic received.");
    }
    //===============================================================================================
})(window);