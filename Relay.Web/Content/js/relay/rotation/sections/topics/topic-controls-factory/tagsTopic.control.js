﻿; (function (window) {
    var dataAttributeName = "topic-val";
    var dataAttributeNameTagsMore = "topic-MoreTags";
    //-------------------------------------------------
    function TagsTopic(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }

    TagsTopic.prototype.initialize = function (topic) {
        this.topic = topic;
        var _this = this;
        this.control = $(".tags", topic.topicElement).first();
        topic.model.FirstTagTopic = this.control.attr(dataAttributeName) === "true";

        return true;
    };

    TagsTopic.prototype.update = function () {
        var test = this.control;
        $.ajax({
            url: "/Summary/GetTagsByTopicId",
            method: "GET",
            data: { topicId: this.topic.model.Id }
        })
            .done(function (data) {
                $(test).empty().html(data);
            });
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.TagsTopic = function (config) {
            return new TagsTopic(config);
        };
    }
    else {
        console.error("Error in get tag topic.");
    }
    //===============================================================================================
})(window);