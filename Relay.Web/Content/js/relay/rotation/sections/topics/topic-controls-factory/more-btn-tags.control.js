﻿; (function (window) {
    //==================================== More Button ==============================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function MoreBtnTags() {
        this.topic = null;
        this.control = null;
    }

    MoreBtnTags.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $(".more-btn-tags", topic.topicElement).first();
        this.control = control;

        if (control.length !== 0) {
            this.control = control;
            //----------
            control.click(function () {
                if (!topic.model.IsNullReport) {
                    if (topic.IsExpanded) {
                        topic.collapse();
                    }
                    else {
                        topic.expand();
                    }
                }
            });
            //----------
        }
        return true;

    };
    MoreBtnTags.prototype.update = function () {
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.MoreBtnTags = function () {
            return new MoreBtnTags();
        };
    }
    else {
        console.error("More Button Tags control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);