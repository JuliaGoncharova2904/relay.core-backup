﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false
    //      type: (task, topic)
    //      changeHandler: (handler)
    //  }
    //============================== Carryforward Button ============================================
    var dataAttributeName = "topic-val";
    var Types = {
        task: 1,
        topic: 2
    }
    //--------------------------------------
    function CarryforwardBtn(config) {
        this.config = config || {};
        this.topic = null;
        this.control = null;
    }
    CarryforwardBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var dataWaiting = false;
        var carryforwardHandler;
        var control = $('.arrow-btn', topic.topicElement).first();

        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            //----------
            this.control = control;
            topic.model.CarryforwardCounter = +control.attr(dataAttributeName);
            //----------
            if (!this.config.readOnly) {
                //----------
                switch (this.config.type) {
                    case Types.task:
                        carryforwardHandler = CarryforwardTask;
                        break;
                    case Types.topic:
                        carryforwardHandler = CarryforwardTopic;
                        break;
                }
                //----------
                control.click(function () {
                    if (!topic.model.IsNullReport && !dataWaiting) {
                        dataWaiting = true;

                        carryforwardHandler(topic.model.Id, function () {
                            dataWaiting = false;
                            topic.addReloadEventLisetner(_this.config.changeHandler)
                            topic.reloadServerData();
                        });

                        topic.model.CarryforwardCounter++;
                        _this.update();
                    }
                });
                //----------
            }
            //----------
            return true;
        }

        return false;
    };
    CarryforwardBtn.prototype.update = function () {
        var counter = this.topic.model.CarryforwardCounter;
        var stateClass = "none-btn btn-circle";

        if (counter === 0) {
            stateClass = "arrow-gray-btn";
        }
        else if (counter === 1) {
            stateClass = "arrow-green-btn";
        }
        else if (counter === 2) {
            stateClass = "arrow-yellow-btn";
        }
        else if (counter >= 3) {
            stateClass = "arrow-pink-btn";
        }

        this.control.removeClass();
        this.control.addClass("btn " + stateClass);
    };
    //-----------------------------------------------------------------
    function CarryforwardTopic(topicId, resultHandler) {
        $.ajax({
            url: "/Topic/CarryforwardTopic",
            method: "POST",
            data: { topicId: topicId }
        })
        .done(resultHandler)
        .fail(errorHandler);
    }
    //---------
    function CarryforwardTask(taskId, resultHandler) {
        $.ajax({
            url: "/Tasks/CarryforwardTask",
            method: "POST",
            data: { taskId: taskId }
        })
        .done(resultHandler)
        .fail(errorHandler);
    }
    //---------
    function errorHandler() {
        var errorDialog = Relay.ErrorDialog();
        errorDialog.setCloseEvent(function () {
            location.reload();
        });
        errorDialog.open();
    }
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.CarryforwardBtn = function (config) {
            return new CarryforwardBtn(config);
        };

        window.Relay.Topic.ControlsFactory.CarryforwardBtn.Types = Types;
    }
    else {
        console.error("Carryforward Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);