﻿; (function (window) {
    //===============================================================================================
    //  config {
    //      readOnly: false,
    //      forTask: (true),
    //      forTopic: (true)
    //  }
    //============================== Mnager Comments  Button ========================================
    var dataAttributeName = "topic-val";
    //--------------------------------------
    function ManagerCommentsBtn(config) {
        this.config = config;
        this.topic = null;
        this.control = null;
    }
    ManagerCommentsBtn.prototype.initialize = function (topic) {
        var _this = this;
        this.topic = topic;
        var control = $('.comments-btn', topic.topicElement).first();

        if (control.length !== 0 && control[0].hasAttribute(dataAttributeName)) {
            //----------
            this.control = control;
            topic.model.HasComments = control.attr(dataAttributeName) === "true";
            //----------
                //var controller = null;
                //if (this.config.forTask) controller = "/Tasks";
                //else if (this.config.forTopic) controller = "/Topic";
                ////----------
                //if (controller) {
                //    control.click(function () {
                //        var formDialog = Relay.FormDialog({
                //            url: controller + "/ManagerCommentsDialog/" + topic.model.Id,
                //            method: "GET",
                //            dialogId: "managerCommentsDetailsDialog"
                //        });
                //        formDialog.setSubmitEvent(topic.reloadServerData.bind(topic))
                //        formDialog.open();
                //    });
                //}

            control.click(function () {
                if (!topic.model.IsNullReport) {
                    var managerCommentsDialog = Relay.ManagerCommentsDialog(topic.model.Id,
                        _this.config.dialogType,
                        _this.config.container);
                    managerCommentsDialog.setCloseEvent(function () {
                        topic.reloadServerData();
                        if (this.config.changeHandler)
                            this.config.changeHandler();
                    });
                    managerCommentsDialog.open();
                }
            });
   
            //----------
            return true;
        }

        return false;
    };
    ManagerCommentsBtn.prototype.update = function () {
        if (this.control) {
            if (this.topic.model.HasComments) {
                this.control.removeClass('comment-inactive-btn');
                this.control.addClass('comment-active-btn');
            }
            else {
                this.control.removeClass('comment-active-btn');
                this.control.addClass('comment-inactive-btn');
            }
        }
    };
    //===============================================================================================
    if (window.Relay.Topic) {
        window.Relay.Topic.ControlsFactory = window.Relay.Topic.ControlsFactory || {};

        window.Relay.Topic.ControlsFactory.ManagerCommentsBtn = function (clickHandler) {
            return new ManagerCommentsBtn(clickHandler);
        };
    }
    else {
        console.error("Manager Comments Button control cannot work without Topic control.");
    }
    //===============================================================================================
})(window);