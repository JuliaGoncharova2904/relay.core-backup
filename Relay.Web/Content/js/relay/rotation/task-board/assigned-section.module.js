﻿$(document).ready(function () {
    "use strict";
    //================== Header module =======================================
    function AssignedSection(panel) {
        this.taskBoardPanel = panel;
        this.userId = panel.attr("user-id");
        var controller = "/TaskBoard";
        this.updateTopicUrl = controller + "/UpdateTaskTopic";
        this.reloadTopicUrl = controller + "/ReloadTaskTopic";

        $(".plus-btn", this.taskBoardPanel).show();

        var queryModel = Relay.UrlQuery.getQueryModel();

        $(".pdf-report", panel).click(function () {
            var queryModel = Relay.UrlQuery.getQueryModel();

            var pdfUrl = "/TaskBoard/TaskBoardSectionPdfReport?userId=" + panel.attr("user-id") + "&section=Assigned&filter=" + (queryModel.filter || 'IncompletedTasks');

            window.open(pdfUrl, '_blank');
        });

        var filterBtn = $(".filter-btn", panel);

        function resizeHandler() {
            WebuiPopovers.hide(filterBtn);
        };

        filterBtn.webuiPopover({
            hideEmpty: true,
            placement:'bottom',
            style: 'task-board',
            trigger: 'click',
            multi: false,
            dismissible: true,
            type: "async",
            cache: false,
            width: '118',
            url: "/TaskBoard/GetPopoverFilter?section=Assigned&filter=" + (queryModel.filter || 'IncompletedTasks'),
            offsetLeft: -3,
            onShow: function () {
                window.addEventListener("resize", resizeHandler, false);
            },
            onHide: function () {
                window.removeEventListener("resize", resizeHandler, false);
            }
        });

        $(".tooltipster", panel).tooltipster({
            debug: false
        });
        
        AssignedSection_Initialise.call(this);
        InitSortMechanism.call(this);

        var res = $(".plus-btn", this.taskBoardPanel);
        var res1 = $(".plus-btn", this.taskBoardPanel).first();

        $(".plus-btn", this.taskBoardPanel).click(this.addBtnClick.bind(this));
    }
    AssignedSection.prototype.addBtnClick = function () {
        var addTaskDialog = Relay.FormDialog({
            url: "/TaskBoard/AddTaskDialog",
            data: { userId: this.userId }
        });
        addTaskDialog.setSubmitEvent(function () {
            location.reload();
        });
        addTaskDialog.open();
    }

    function InitSortMechanism() {
        $(".priority-title", this.taskBoardPanel).click(function () {
            var prioritySort = Relay.UrlQuery.getQueryModel();

            if ($(".priority-sort-icon", this).hasClass("high-to-low")) {
                prioritySort.sortStrategy = "PriorityReverse";
            }
            else if ($(".priority-sort-icon", this).hasClass("low-to-high")) {
                prioritySort.sortStrategy = null;
            }
            else {
                prioritySort.sortStrategy = "PriorityDirect";
            }

            Relay.UrlQuery.updateQueryModel(prioritySort);
        });

        $(".due-date-title", this.taskBoardPanel).click(function () {
            var dueDateSort = Relay.UrlQuery.getQueryModel();

            if ($(".due-date-sort-icon", this).hasClass("high-to-low")) {
                dueDateSort.sortStrategy = "DueDateReverse";
            }
            else if ($(".due-date-sort-icon", this).hasClass("low-to-high")) {
                dueDateSort.sortStrategy = null;
            }
            else {
                dueDateSort.sortStrategy = "DueDateDirect";
            }

            Relay.UrlQuery.updateQueryModel(dueDateSort);
        });
    }

    function NameEditClickHandler(model, reload, update) {
        var editTaskDialog = Relay.FormDialog({
            url: "/TaskBoard/EditTaskDialog",
            data: {
                userId: this.userId,
                taskId: model.Id
            }
        });
        editTaskDialog.setErrorEvent(function () {
            location.reload();
        });
        editTaskDialog.setSubmitEvent(function () {
            location.reload();
        });
        editTaskDialog.open();
    }
    function NameDetailsClickHandler(model, reload, update) {
        var tasksDetailsDialog = Relay.FormDialog({
            url: "/Tasks/TaskDetailsDialog",
            method: "GET",
            data: { TaskId: model.Id }
        });
        tasksDetailsDialog.setErrorEvent(function () {
            location.reload();
        });
        tasksDetailsDialog.open();
    }
    function NameDetailsTaskBoardClickHandler(model, reload, update) {
        var tasksDetailsDialog = Relay.FormDialog({
            url: "/TaskBoard/TaskDetailsDialog",
            method: "GET",
            data: { TaskId: model.Id }
        });
        tasksDetailsDialog.setErrorEvent(function () {
            location.reload();
        });
        tasksDetailsDialog.open();
    }

    function AssignedSection_Initialise() {
        var _this = this;
        this.tasks = [];
        //-------
        $("tbody tr", this.taskBoardPanel).each(function() {
            var task = AssignedSection_BuildTaskItem.call(_this, this);
            _this.tasks.push(task);
        });
    }
    function AssignedSection_BuildTaskItem(trElement) {
        var readOnly = $(trElement).attr("read-only") === "True";
        var isTaskBoardTask = $(trElement).attr("is-task-board") === "True";

        var task = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        if (!readOnly) {
            task.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
                type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.task,
                changeHandler: function () { location.reload(); },
                warningMessage: "Are you sure you want to delete this task?"
            }));
        }

        task.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: readOnly
        }));
        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".handover-reference-body",
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".task-name-body",
            clickHandler: readOnly 
                            ? isTaskBoardTask
                                ? NameDetailsTaskBoardClickHandler
                                : NameDetailsClickHandler
                            : NameEditClickHandler.bind(this),
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "CompleteStatus",
            elementClass: ".status-body",
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            readOnly: readOnly
        }));
        task.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        task.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags());
        task.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        task.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));
        task.AddControl(Relay.Topic.ControlsFactory.CarryforwardBtn({
            readOnly: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.ArchivedBtn({
            completeFieldName: "CompleteStatus"
        }));

        task.updateView();

        return task;
    }
    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.AssignedSection = new AssignedSection($(".task-board-page").first());
    //========================================================================
});