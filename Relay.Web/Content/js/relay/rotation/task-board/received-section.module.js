﻿$(document).ready(function () {
    "use strict";
    //================== Header module =======================================
    function ReceivedSection(panel) {
        this.taskBoardPanel = panel;
        this.userId = panel.attr("user-id");
        var controller = "/TaskBoard";
        this.updateTopicUrl = controller + "/UpdateTaskTopic";
        this.reloadTopicUrl = controller + "/ReloadTaskTopic";

        var queryModel = Relay.UrlQuery.getQueryModel();

        var filterBtn = $(".filter-btn", panel);

        $(".pdf-report", panel).click(function () {
            var queryModel = Relay.UrlQuery.getQueryModel();
            var pdfUrl = "/TaskBoard/TaskBoardSectionPdfReport?userId=" + panel.attr("user-id") + "&section=Received&filter=" + (queryModel.filter || 'IncompletedTasks');

            window.open(pdfUrl, '_blank');
        });

        function resizeHandler() {
            WebuiPopovers.hide(filterBtn);
        };

        filterBtn.webuiPopover({
            hideEmpty: true,
            placement: 'bottom',
            style: 'task-board',
            trigger: 'click',
            multi: false,
            dismissible: true,
            type: "async",
            cache: false,
            width: '118',
            url: "/TaskBoard/GetPopoverFilter?section=Received&filter=" + (queryModel.filter || 'IncompletedTasks'),
            offsetLeft: -3,
            onShow: function () {
                window.addEventListener("resize", resizeHandler, false);
            },
            onHide: function () {
                window.removeEventListener("resize", resizeHandler, false);
            }
        });

        $(".tooltipster", panel).tooltipster({
            debug: false
        });

        ReceivedSection_Initialise.call(this);
        InitSortMechanism.call(this);
    }

    function InitSortMechanism() {
        $(".priority-title", this.taskBoardPanel).click(function () {
            var prioritySort = Relay.UrlQuery.getQueryModel();

            if ($(".priority-sort-icon", this).hasClass("high-to-low")) {
                prioritySort.sortStrategy = "PriorityReverse";
            }
            else if ($(".priority-sort-icon", this).hasClass("low-to-high")) {
                prioritySort.sortStrategy = null;
            }
            else {
                prioritySort.sortStrategy = "PriorityDirect";
            }

            Relay.UrlQuery.updateQueryModel(prioritySort);
        });

        $(".due-date-title", this.taskBoardPanel).click(function () {
            var dueDateSort = Relay.UrlQuery.getQueryModel();

            if ($(".due-date-sort-icon", this).hasClass("high-to-low")) {
                dueDateSort.sortStrategy = "DueDateReverse";
            }
            else if ($(".due-date-sort-icon", this).hasClass("low-to-high")) {
                dueDateSort.sortStrategy = null;
            }
            else {
                dueDateSort.sortStrategy = "DueDateDirect";
            }

            Relay.UrlQuery.updateQueryModel(dueDateSort);
        });
    }

    function NameCompleteClickHandler(model, reload, update) {
        var tasksDetailsDialog = Relay.FormDialog({
            url: "/Tasks/CompleteTaskDialog",
            method: "GET",
            data: { TaskId: model.Id }
        });
        tasksDetailsDialog.setErrorEvent(function () {
            location.reload();
        });
        tasksDetailsDialog.open();
    }

    function NameClickTaskBoardCompleteHandler(model, reload, update) {
        var completeTaskDialog = Relay.FormDialog({
            url: "/TaskBoard/CompleteTaskChecklistDialog",
            data: {
                taskId: model.Id
            }
        });
        completeTaskDialog.setErrorEvent(function () {
            location.reload();
        });
        completeTaskDialog.setSubmitEvent(function () {
            location.reload();
        });
        completeTaskDialog.open();
    }

    function ReceivedSection_Initialise() {
        var _this = this;
        this.tasks = [];
        //-------
        $("tbody tr", this.taskBoardPanel).each(function () {
            var task = ReceivedSection_BuildTaskItem.call(_this, this);
            _this.tasks.push(task);
        });
    }
    function ReceivedSection_BuildTaskItem(trElement) {
        var readOnly = $(trElement).attr("read-only") === "True";
        var isTaskBoardTask = $(trElement).attr("is-task-board") === "True";

        var task = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });
        task.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: true
        }));

        if (!readOnly) {
            var taskName = $(".task-name-body", trElement);

            function resizeEditCompletePopoverHandler() {
                WebuiPopovers.hide(taskName);
            };

            taskName.webuiPopover({
                hideEmpty: true,
                selector: true,
                placement: "bottom-right",
                style: 'task-board-menu-popover',
                trigger: 'click',
                multi: false,
                dismissible: true,
                type: "async",
                cache: false,
                width: '83',
                offsetTop: -10,
                offsetLeft: 0,
                url: "/TaskBoard/GetEditCompleteForReceivedMenuPopover?userId=" + this.userId + "&taskId=" + task.model.Id,
                onShow: function () {
                    window.addEventListener("resize", resizeEditCompletePopoverHandler, false);
                },
                onHide: function () {
                    window.removeEventListener("resize", resizeEditCompletePopoverHandler, false);
                }
            });
        }

        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".handover-reference-body",
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".task-name-body",
            clickHandler: readOnly
                ? isTaskBoardTask ? NameClickTaskBoardCompleteHandler : NameCompleteClickHandler
                : null,
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        task.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "CompleteStatus",
            elementClass: ".complete-status",
            finalizeOff: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            readOnly: true
        }));
        task.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        task.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags());
        task.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        task.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));
        task.AddControl(Relay.Topic.ControlsFactory.ArchivedBtn({
            completeFieldName: "CompleteStatus"
        }));
        task.updateView();

        return task;
    }
    //========================================================================
    window.Relay = window.Relay || {};
    window.Relay.ReceivedSection = new ReceivedSection($(".task-board-page").first());
    //========================================================================
});