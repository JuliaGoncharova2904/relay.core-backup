﻿; (function (window) {
    //===========================================================================
    function SwingHandoverFinishSwingBtn(controlElement, userId) {
        this.userId = userId;
        this.readOnly = null;
        this.controlElement = controlElement;
    }


    SwingHandoverFinishSwingBtn.prototype.update = function () {
        var _this = this;

        $(this.controlElement[0]).click(function () {
            var confirmDialog = Relay.ConfirmDialog(
                "Are you sure you want to finalise and send your report now?",
                "",
                "Yes",
                "No"
            );

            confirmDialog.setOkEvent(function () {
                $.ajax({
                    url: "/Rotation/FinishSwing",
                    method: "POST",
                    data: { userId: _this.userId }
                })
                    .done(function (result) {
                        if (result === 'ok') {
                            location.reload();
                        }
                    })
                    .fail(function () {
                        Relay.ErrorDialog().open();
                    });
            });

            confirmDialog.open();
        });
    };

    SwingHandoverFinishSwingBtn.prototype.build = function () {
        this.update();
    }

    //===============================================================================================
    window.Relay = window.Relay || {};
    window.Relay.SwingHandoverFinishSwingBtn = function (controlElement, userId) {
        return new SwingHandoverFinishSwingBtn(controlElement, userId);
    }
    //===============================================================================================
})(window);


