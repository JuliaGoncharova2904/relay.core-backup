﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Draft Tasks Section ===========================================
    //------------------- Name Column Handler -----------------------------
    function NameClickHandler(model, reload, update) {
            var tasksListDialog = Relay.FormDialog({
              url: "/TaskBoard/TaskDetailsDialog",
                method: "GET",
                data: { TaskId: model.Id }
            });
          //  tasksListDialog.setCloseEvent(reload);
            tasksListDialog.open();
     
    }
    //---------------------------------------------------------------------
    function DraftTaskboardTasksSection(sectionElement, rotationId)
    {
        //----------------------
        this.type = Relay.SectionFactory.Types.Tasks;
        var controller = "/RotationTaskBoardTasksSection";
        this.sectionUrl = controller + "/Draft?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTask";
        this.reloadTopicUrl = controller + "/ReloadTask";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        DraftTaskboardTasksSection_Initialise.call(this);
    }
    DraftTaskboardTasksSection.prototype.reload = function ()
    {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            DraftTaskboardTasksSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    DraftTaskboardTasksSection.prototype.expand = function ()
    {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    DraftTaskboardTasksSection.prototype.collapse = function ()
    {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    DraftTaskboardTasksSection.prototype.disableTopics = function ()
    {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    DraftTaskboardTasksSection.prototype.enableTopics = function ()
    {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftTaskboardTasksSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
          var topic = DraftTaskboardTasksSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function DraftTaskboardTasksSection_BuildTopic(trElement)
    {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.task,
            changeHandler: DraftTaskboardTasksSection_ReloadAll
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            changeHandler: DraftTaskboardTasksSection_TopicActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            changeHandler: DraftTaskboardTasksSection_TopicActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: ".second-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".third-td",
            clickHandler: NameClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
          readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NowTaskStatusField());
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            forTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            forDraftTask: false
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
          dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));

        topic.updateView();

        return topic;
    }
    function DraftTaskboardTasksSection_TopicActionsChangeHandler(model)
    {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types[model.SectionType]);
    }
    function DraftTaskboardTasksSection_ReloadAll() {
        Relay.ReportBuilderPanel.reload();
        Relay.DailyNotesPanel.reloadPanel();
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.DraftTaskboardTasksSection = function (sectionElement, rotationId) {
          return new DraftTaskboardTasksSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Draft Tasks Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);