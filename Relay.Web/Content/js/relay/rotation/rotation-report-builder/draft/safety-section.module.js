﻿;(function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Draft Safety Section ==========================================
    //----------------- Reference Column Handler --------------------------
    function ReferenceColumnClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var editHSETopicDialog = Relay.FormDialog({
                url: "/HSESection/EditTopicDialog?topic=" + model.Id,
                method: "GET",
                dialogId: "HSETopicFormDialog"
            });
            editHSETopicDialog.setSubmitEvent(reload);
            editHSETopicDialog.open();
        }
    }
    function ReferenceColumnClickHandlerReceivedDraft(model, reload) {
        if (!model.IsNullReport) {
            var editHSETopicDialog = Relay.FormDialog({
                url: "/Topic/TopicDetailsDialog?topicid=" + model.Id,
                method: "GET",
                dialogId: "topicDetailsDialog"
            });
            editHSETopicDialog.setSubmitEvent(reload);
            editHSETopicDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function DraftSafetySection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.HSE;
        var controller = "/HSESection";
        this.sectionUrl = controller + "/Draft?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        this.addInlineTopicUrl = controller + "/AddInlineTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        this.openInlineTopic = null;
        this.closeInlineTopic = null;
        //----------------------
        DraftSafetySection_Initialise.call(this);
    }
    DraftSafetySection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            DraftSafetySection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    DraftSafetySection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    DraftSafetySection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    DraftSafetySection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    DraftSafetySection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftSafetySection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = DraftSafetySection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
        //-------
        var addInlineTopic = Relay.AddInlineTopic(this.sectionElement, {
            url: this.addInlineTopicUrl,
            destId: this.rotationId,
            topicName: this.type,
            warningMessage: "Please choose a Safety Type and enter a Reference. Exit?"
        });
        addInlineTopic.setSaveEvent(this.reload.bind(this));
        addInlineTopic.setOpenEvent(function () {
            if (_this.openInlineTopic)
                _this.openInlineTopic();
        });
        addInlineTopic.setCloseEvent(function () {
            if (_this.closeInlineTopic)
                _this.closeInlineTopic();
        });
    }
    function DraftSafetySection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.topic,
            changeHandler: DraftSafetySection_RmoveTopicHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            changeHandler: DraftSafetySection_ReloadTopicHandler.bind(this),
            rotationType: "Swing",
            type: Relay.SectionFactory.Types.HSE
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            changeHandler: DraftSafetySection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            changeHandler: DraftSafetySection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Type",
            elementClass: ".type-body",
            clickHandler: this.readOnly ? ReferenceColumnClickHandlerReceivedDraft : ReferenceColumnClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".reference-body",
            clickHandler: this.readOnly ? ReferenceColumnClickHandlerReceivedDraft : ReferenceColumnClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            dialogType: this.readOnly ? Relay.ManagerCommentsDialog.Types.Received : Relay.ManagerCommentsDialog.Types.Draft,
            container: Relay.ManagerCommentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBtn({
            dialogType: "Draft",
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PlannedActualBtn({
            readOnly: this.readOnly
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
        //    dialogType: Relay.TasksListDialog.Types.Draft,
        //    changeHandler: DraftSafetySection_TasksAndActionsChangeHandler.bind(this)
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
        //    dialogType: Relay.VoiceMessagesDialog.Types.Draft,
        //    container: Relay.VoiceMessagesDialog.Containers.Topic
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopic());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopicReceived());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBody());
        topic.updateView();

        return topic;
    }
    function DraftSafetySection_TasksAndActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    function DraftSafetySection_RmoveTopicHandler(model) {
        this.reload();
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }

    function DraftSafetySection_ReloadTopicHandler(model) {
        if (this.filterType === 'HandoverItems') {
            this.reload();
        }
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.DraftSafetySection = function (sectionElement, rotationId) {
            return new DraftSafetySection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Draft Safety Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);