﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Draft Core Section ============================================
    //-----------  Process Location Column click handler ------------------
    function ProcessLocationColumnClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var editCoreTopicDialog = Relay.FormDialog({
                url: "/CoreSection/EditTopicDialog?topic=" + model.Id,
                method: "GET",
                dialogId: "CoreTopicFormDialog"
            });
            editCoreTopicDialog.setSubmitEvent(reload);
            editCoreTopicDialog.open();
        }
    }

    function ReferenceColumnClickHandlerReceivedDraft(model, reload) {
        if (!model.IsNullReport) {
            var editHSETopicDialog = Relay.FormDialog({
                url: "/Topic/TopicDetailsDialog?topicid=" + model.Id,
                method: "GET",
                dialogId: "topicDetailsDialog"
            });
            editHSETopicDialog.setSubmitEvent(reload);
            editHSETopicDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function DraftCoreSection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Core;
        var controller = "/CoreSection";
        this.sectionUrl = controller + "/Draft?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        this.addInlineTopicUrl = controller + "/AddInlineTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        this.openInlineTopic = null;
        this.closeInlineTopic = null;
        //----------------------
        DraftCoreSection_Initialise.call(this);
    }
    DraftCoreSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            DraftCoreSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    DraftCoreSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    DraftCoreSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    DraftCoreSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    DraftCoreSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftCoreSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = DraftCoreSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
        //-------
        var addInlineTopic = Relay.AddInlineTopic(this.sectionElement, {
            url: this.addInlineTopicUrl,
            destId: this.rotationId,
            topicName: this.type,
            warningMessage: "Please choose a Process Group and enter a Process. Exit?"
        });
        addInlineTopic.setSaveEvent(this.reload.bind(this));
        addInlineTopic.setOpenEvent(function () {
            if (_this.openInlineTopic)
                _this.openInlineTopic();
        });
        addInlineTopic.setCloseEvent(function () {
            if (_this.closeInlineTopic)
                _this.closeInlineTopic();
        });
    }
    function DraftCoreSection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.topic,
            changeHandler: DraftCoreSection_RemoveTopicHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            changeHandler: DraftCoreSection_RemoveTopicHandler.bind(this),
            rotationType: "Swing",
            type: Relay.SectionFactory.Types.Core
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            changeHandler: DraftCoreSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            changeHandler: DraftCoreSection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Category",
            elementClass: ".type-body",
            clickHandler: this.readOnly ? ReferenceColumnClickHandlerReceivedDraft : ProcessLocationColumnClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".reference-body",
            clickHandler: this.readOnly ? ReferenceColumnClickHandlerReceivedDraft : ProcessLocationColumnClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            dialogType: this.readOnly ? Relay.ManagerCommentsDialog.Types.Received : Relay.ManagerCommentsDialog.Types.Draft,
            container: Relay.ManagerCommentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBtn({
            dialogType: "Draft",
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PlannedActualBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn());
        //topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
        //    dialogType: Relay.TasksListDialog.Types.Draft,
        //    changeHandler: DraftCoreSection_TasksAndActionsChangeHandler.bind(this)
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
        //    dialogType: Relay.VoiceMessagesDialog.Types.Draft,
        //    container: Relay.VoiceMessagesDialog.Containers.Topic
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn());

        topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopic());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopicReceived());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBody());
        topic.updateView();

        return topic;
    }
    function DraftCoreSection_TasksAndActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    function DraftCoreSection_RemoveTopicHandler(model) {
        this.reload();
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }

    function DraftSafetySection_ReloadTopicHandler(model) {
        if (this.filterType === 'HandoverItems') {
            this.reload();
        }
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.DraftCoreSection = function (sectionElement, rotationId) {
            return new DraftCoreSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Draft Core Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);