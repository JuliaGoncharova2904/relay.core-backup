﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Draft Daily Notes Section =====================================
    function DraftDailyNotesSection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.DailyNote;
        var controller = "/DailyNotesSection";
        this.sectionUrl = controller + "/Draft?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        //----------------------
        DraftDailyNotesSection_Initialise.call(this);
    }
    DraftDailyNotesSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            DraftDailyNotesSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    DraftDailyNotesSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    DraftDailyNotesSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    DraftDailyNotesSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    DraftDailyNotesSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftDailyNotesSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = DraftDailyNotesSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function DraftDailyNotesSection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Date",
            elementClass: ".second-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            changeHandler: DraftDailyNotesSection_ChangeHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            forTopic: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags({}));
        topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
            dialogType: Relay.TasksListDialog.Types.Draft,
            changeHandler: DraftDailyNotesSection_TasksChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic,
            changeHandler: DraftDailyNotesSection_ChangeHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Topic,
            changeHandler: DraftDailyNotesSection_ChangeHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopic());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopicReceived());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBody());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags());
        topic.updateView();

        return topic;
    }
    function DraftDailyNotesSection_ChangeHandler(model) {
        Relay.DailyNotesPanel.reloadPanel();
    }
    function DraftDailyNotesSection_TasksChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);

        Relay.DailyNotesPanel.reloadPanel();
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.DraftDailyNotesSection = function (sectionElement, rotationId) {
            return new DraftDailyNotesSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Draft Daily Notes Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);