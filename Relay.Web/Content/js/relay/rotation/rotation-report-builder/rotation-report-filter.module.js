﻿; (function (window) {
    //===========================================================================
    function RotationHandoverFilterBtn(controlElement) {
        this.userId = null;
        this.rotationDraftSubpanel = null;
        this.filterType = null;
        this.controlElement = controlElement;
    }

    RotationHandoverFilterBtn.prototype.destroy = function () {
        this.userId = null;
        this.rotationDraftSubpanel = null;
    };

    RotationHandoverFilterBtn.prototype.hide = function () {
        if (this.controlElement)
            this.controlElement.hide();
    };

    RotationHandoverFilterBtn.prototype.update = function () {
        var _this = this;

        if (!(this.userId && this.filterType))
            return;

        var hashModel = Relay.UrlHash.getHashModel();
        _this.filterType = hashModel["filter"] || 'HandoverItems';
        Relay.UrlHash.setHashParam('filter', _this.filterType);

        $(_this.controlElement).webuiPopover('destroy');

        $(_this.controlElement).webuiPopover({
            hideEmpty: true,
            placement: 'bottom',
            style: "rotation-filter",
            trigger: 'click',
            multi: false,
            dismissible: true,
            type: "async",
            cache: false,
            width: '118',
            url: "/RotationReportBuilder/ReportFilterPopover?userId=" + _this.userId + "&filter=" + _this.filterType,
            offsetLeft: 1,
            async: {
                success: function (that, data) {
                    _this.bind();
                }
            },
            onShow: function () {
                window.addEventListener("resize", resizeHandler, false);
            },
            onHide: function () {
                window.removeEventListener("resize", resizeHandler, false);    
            }
        });

        function resizeHandler() {
            WebuiPopovers.hide(_this.controlElement);
        };
    };

    RotationHandoverFilterBtn.prototype.bind = function () {
        var _this = this;

        $("#handoverReport").on('click', function () {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel["filter"] = "HandoverItems";
            Relay.UrlHash.updateHashModel(hashModel);
            WebuiPopovers.hide(_this.controlElement);

            _this.update();
            
            _this.rotationDraftSubpanel.load();
        });

        $("#nrTopics").on('click', function () {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel["filter"] = "NrItems";
            Relay.UrlHash.updateHashModel(hashModel);
            WebuiPopovers.hide(_this.controlElement);

            _this.update();

            _this.rotationDraftSubpanel.load();
        });
    }

    RotationHandoverFilterBtn.prototype.build = function (userId, rotationDraftSubpanel) {
        this.destroy();
        //--------------------
        this.userId = userId;
        this.rotationDraftSubpanel = rotationDraftSubpanel;
        this.filterType = Relay.UrlHash.getHashParam('filter') || 'HandoverItems';
        Relay.UrlHash.setHashParam('filter', this.filterType);
        //--------------------
        this.update();
        //--------------------
    }
    //===============================================================================================
    window.Relay = window.Relay || {};
    window.Relay.RotationHandoverFilterBtn = function (controlElement) {
        return new RotationHandoverFilterBtn(controlElement);
    }
    //===============================================================================================
})(window);


