﻿$(document).ready(function ()
{
    //================================================================================
    function DailyNote(dailyNoteElement)
    {
        this.id = $(dailyNoteElement).prop("id");
        this.dailyNoteElement = dailyNoteElement;
        this.init();
    }
    DailyNote.prototype.init = function ()
    {
        var slide = $(this.dailyNoteElement).children().first();
        var editable = slide.attr("data-editable") === 'True';
        var isFuture = slide.attr("data-is-future") === 'True';
        var hasContent = slide.attr("has-content") === 'True';

        if (hasContent) {
            $(this.dailyNoteElement).addClass("blue-slide");
        }
        else {
            $(this.dailyNoteElement).removeClass("blue-slide");
        }
      

        if (slide.hasClass("safety-item"))
        {
            if (!isFuture)
            {
                var safetyVersion = $(".show-safety-message", slide).attr("safety-version");

                switch (safetyVersion)
                {
                    case "V1":
                        slide.click(this.showSafetyMessageV1.bind(this));
                        break;
                    case "V2":
                        slide.click(this.showSafetyMessageV2.bind(this));
                        break;
                }
            }
        }
        else
        {
            if (editable)
            {
                $(".edit-daily-note", slide).click(this.editDailyNote.bind(this));
                $(".tasks-btn", slide).click(this.openTaskListDialog.bind(this));
                $(".attach-btn", slide).click(this.openAttachmentListDialog.bind(this));
            }
            else if (!isFuture)
            {
                $(".edit-daily-note", slide).click(this.detailsDailyNote.bind(this));
                $(".tasks-btn", slide).click(this.openTaskListDialogForRead.bind(this));
                $(".attach-btn", slide).click(this.openAttachmentListDialog.bind(this));
            }
        }

    };
    DailyNote.prototype.reload = function ()
    {
        var _this = this;

        $.ajax({
            url: "/DailyNotesPanel/DailyNoteSlide",
            method: "GET",
            data: { dailyNoteId: this.id }
        })
        .done(function (newContent)
        {
            $(_this.dailyNoteElement).html($(newContent).children());
            _this.init();
        })
        .fail(function ()
        {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function ()
            {
                location.reload();
            });
            errorDialog.open();
        });
    };

    DailyNote.prototype.editDailyNote = function ()
    {
        var _this = this;
        var dailyNotesDialog = Relay.FormDialog({
            url: "/DailyNotesPanel/EditDailyNoteDialog/" + this.id,
            method: "GET",
            dialogId: "dailyNoteFormDialog"
        });

        dailyNotesDialog.setSubmitEvent(function ()
        {
            _this.reload();
            Relay.ReportBuilderPanel.reloadSection(Relay.SectionFactory.Types.DailyNote);
        });

        dailyNotesDialog.open();
    };

    DailyNote.prototype.detailsDailyNote = function ()
    {
        var detailsDailyNote = Relay.FormDialog({
            url: "/DailyNotesPanel/DetailsDailyNoteDialog/" + this.id,
            method: "GET",
            dialogId: "dailyNoteDetailsDialog"
        });

        detailsDailyNote.open();
    };
    DailyNote.prototype.showSafetyMessageV1 = function (event)
    {
        var _this = this;
        var date = $(event.currentTarget).attr('date');

        var formDialog = Relay.FormDialog({
            url: "/ScheduleMessage/GetSafetyMessagesForUser",
            method: "GET",
            dialogId: "safetyMessageDialog",
            data: { date: date }
        });
        formDialog.setCloseEvent(function ()
        {
            _this.reload();
            _this.editDailyNote();
        });
        formDialog.open();
    };
    DailyNote.prototype.showSafetyMessageV2 = function (event)
    {
        var _this = this;
        var date = $(event.currentTarget).attr('date');

        var formDialog = Relay.FormDialog({
            url: "/SafetyMessageV2/GetSafetyMessagesForUser",
            method: "GET",
            dialogId: "safetyMessageV2Dialog",
            data: { date: date }
        });
        formDialog.setCloseEvent(function ()
        {
            _this.reload();
            _this.editDailyNote();
        });
        formDialog.open();
    };
    DailyNote.prototype.openTaskListDialog = function ()
    {
        var _this = this;

        var tasksListDialog = Relay.TasksListDialog(Relay.TasksListDialog.Types.Draft, this.id);
        tasksListDialog.setCloseEvent(function ()
        {
            _this.reload();
            Relay.ReportBuilderPanel.reloadSection(Relay.SectionFactory.Types.DailyNote);
            Relay.ReportBuilderPanel.reloadSection(Relay.SectionFactory.Types.Tasks);
        });
        tasksListDialog.open();
    };

    DailyNote.prototype.openAttachmentListDialog = function ()
    {
        var _this = this;
        var attachmentsDialog = Relay.AttachmentsDialog(_this.id,
                                                        Relay.AttachmentsDialog.Types.Draft,
                                                        Relay.AttachmentsDialog.Containers.Topic);
        attachmentsDialog.setCloseEvent(function ()
        {
            _this.reload();
            DailyNotesPanel.reload();
            Relay.ReportBuilderPanel.reloadSection(Relay.SectionFactory.Types.DailyNote);
        });
        attachmentsDialog.open();
    };

    DailyNote.prototype.openTaskListDialogForRead = function ()
    {
        var tasksListDialog = Relay.TasksListDialog(Relay.TasksListDialog.Types.ReadOnly, this.id);
        tasksListDialog.open();
    };
    DailyNote.prototype.openAttachmentListDialogForRead = function ()
    {
        var attachmentsDialog = Relay.AttachmentsDialog(this.id, Relay.AttachmentsDialog.Types.Received, Relay.AttachmentsDialog.Containers.Topic);
        attachmentsDialog.open();
    };
    //================================================================================
    function DailyNotesPanel(dailyNotesPanelElement, rotationId)
    {
        this.rotationId = rotationId;
        this.panel = dailyNotesPanelElement;
        this.Id = null;
    }

    DailyNotesPanel.prototype.init = function ()
    {
        //-----------------------------------------------
        $(".panel-body", this.panel.conteiner).removeAttr("style");

        var editRotationDatesBtn = $("#editRotationDates", this.panel)[0];
        if ($(editRotationDatesBtn).length > 0) {
            editRotationDatesBtn.addEventListener("click", EditRotationDatesDialog, false);
        }

        //------------- Daily Notes List ----------------
        var dailyNotesSlider = new Swiper($(".rotation-page-swiper", this.panel.conteiner), {
            slidesPerView: 8,
            spaceBetween: 5,
            centeredSlides: true,
            hashnav: false,
            // Navigation arrows
            nextButton: $(".rotation-page-swiper-next", this.panel.conteiner),
            prevButton: $(".rotation-page-swiper-prev", this.panel.conteiner),
            breakpoints: {
                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetweenSlides: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 2,
                    spaceBetweenSlides: 20
                },
                // when window width is <= 640px
                640: {
                    slidesPerView: 3,
                    spaceBetweenSlides: 30
                },
                991: {
                    slidesPerView: 4,
                    spaceBetweenSlides: 30
                },
                1010: {
                    slidesPerView: 5,
                    spaceBetweenSlides: 30
                },
                1199: {
                    slidesPerView: 6,
                    spaceBetweenSlides: 30
                }
            }
        });

        //-----------------------------------------------
        $(".swiper-slide", this.panel.conteiner).each(function (slideNumber)
        {
            var isActive = $(this).attr("data-active") === 'True' ? true : false;

            if (isActive)
            {
                dailyNotesSlider.slideTo(slideNumber, 1, false);
            }
            
            new DailyNote(this);
        });
        //-----------------------------------------------
        return this;
        //-----------------------------------------------
    };

    DailyNotesPanel.prototype.enable = function ()
    {
        this.panel.removeAttr("style");
    };
    DailyNotesPanel.prototype.disable = function ()
    {
        this.panel.attr("style", "pointer-events:none;");
    };
    DailyNotesPanel.prototype.reloadPanel = function ()
    {
        var _this = this;
        $.ajax({
            url: "/DailyNotesPanel/DailyNotesForRotation?rotationId=" + _this.rotationId,
            method: "GET"
        })
        .done(function (newContent)
        {
            _this.panel.empty();
            $(newContent).children().each(function ()
            {
                _this.panel.append(this);
            });
            _this.init();
        })
        .fail(function ()
        {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function ()
            {
                location.reload();
            });
            errorDialog.open();
        });
    };

    function EditRotationDatesDialog()
    {
        var userId = $(this).attr('user-id');
        var editDialog = Relay.EditSwingShiftDialog({ userId: userId });

        editDialog.setCloseEvent(function ()
        {
            location.reload();
        });

        editDialog.open();
    }

    //================================================================================
    window.Relay = window.Relay || {};
    window.Relay.DailyNotesPanel = new DailyNotesPanel($("#dailyNotesPanel"), $("#RotationId").val()).init();
    //================================================================================
});