﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //===============================  History Tasks Section ========================================
    //------------------- Name Column Handler -----------------------------
    function NameClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var complitTaskDialog = Relay.FormDialog({
                url: "/Tasks/TaskDetailsDialog?TaskId=" + model.Id,
                method: "GET",
                dialogId: "taskDetailsDialog"
            }).open();
        }
    }
    //---------------------------------------------------------------------
    function HistoryTasksSection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Tasks;
        var controller = "/TasksSection";
        this.sectionUrl = controller + "/History?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        HistoryTasksSection_Initialise.call(this);
    }
    HistoryTasksSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            HistoryTasksSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    HistoryTasksSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    HistoryTasksSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function HistoryTasksSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = HistoryTasksSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function HistoryTasksSection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: ".second-td"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".third-td",
            clickHandler: NameClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "ChildCompleteStatus",
            elementClass: ".complite-status"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            forTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags({}));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Received,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.CarryforwardBtn({
            type: Relay.Topic.ControlsFactory.CarryforwardBtn.Types.task,
            changeHandler: HistoryTasksSection_TopicActionsChangeHandler.bind(this)
        }));

        topic.updateView();

        return topic;
    }
    function HistoryTasksSection_TopicActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types[model.SectionType]);
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.HistoryTasksSection = function (sectionElement, rotationId) {
            return new HistoryTasksSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("History Tasks Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);