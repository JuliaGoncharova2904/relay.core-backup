﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  rotationId: (rotation id)
    //==============================  Received Team Section =========================================
    function ReferenceColumnClickHandlerReceivedDraft(model, reload) {
        if (!model.IsNullReport) {
            var editHSETopicDialog = Relay.FormDialog({
                url: "/Topic/TopicDetailsDialog?topicid=" + model.Id,
                method: "GET",
                dialogId: "topicDetailsDialog"
            });
            editHSETopicDialog.setSubmitEvent(reload);
            editHSETopicDialog.open();
        }
    }

    function ReceivedTeamSection(sectionElement, rotationId) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Team;
        var controller = "/TeamSection";
        this.sectionUrl = controller + "/Received?rotationId=" + rotationId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.rotationId = rotationId;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        ReceivedTeamSection_Initialise.call(this);
    }
    ReceivedTeamSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.empty();
            $(newContent).children().each(function () {
                _this.sectionElement.append(this);
            });
            //---- Init section ------
            ReceivedTeamSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ReceivedTeamSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ReceivedTeamSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function ReceivedTeamSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = ReceivedTeamSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function ReceivedTeamSection_BuildTopic(trElement) {
        var _this = this;

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "TeamMember",
            elementClass: ".second-td",
            clickHandler: ReferenceColumnClickHandlerReceivedDraft
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".reference-body",
            clickHandler: ReferenceColumnClickHandlerReceivedDraft
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            dialogType: this.readOnly ? Relay.ManagerCommentsDialog.Types.Received : Relay.ManagerCommentsDialog.Types.Draft,
            container: Relay.ManagerCommentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtnTags({}));
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: true
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
        //    dialogType: Relay.TasksListDialog.Types.Received
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
        //    dialogType: Relay.VoiceMessagesDialog.Types.Received,
        //    container: Relay.VoiceMessagesDialog.Containers.Topic
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon({}));
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopic());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBody());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBtn({
            dialogType: "Received",
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopicReceived({
            dialogType: "Received",
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PlannedActualBtn({
            readOnly: this.readOnly
        }));
        topic.updateView();

        return topic;
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ReceivedTeamSection = function (sectionElement, rotationId) {
            return new ReceivedTeamSection(sectionElement, rotationId);
        };
    }
    else {
        console.error("Received Team Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);