﻿; (function (window)
{
  //===============================================================================================
  //  sectionElement: (section div)
  //  rotationId: (rotation id)
  //==============================  Draft Tasks Section ===========================================
  //------------------- Name Column Handler -----------------------------
  function NameClickHandler(model, reload, update)
  {
    var tasksListDialog = Relay.FormDialog({
      url: "/TaskBoard/TaskDetailsDialog",
      method: "GET",
      data: { TaskId: model.Id }
    });
    //  tasksListDialog.setCloseEvent(reload);
    tasksListDialog.open();

  }
  //---------------------------------------------------------------------
  function ReceivedTaskboardTasksSection(sectionElement, rotationId)
  {
    //----------------------
    this.type = Relay.SectionFactory.Types.Tasks;
    var controller = "/RotationTaskBoardTasksSection";
    this.sectionUrl = controller + "/Received?rotationId=" + rotationId;
    this.updateTopicUrl = controller + "/UpdateTask";
    this.reloadTopicUrl = controller + "/ReloadTask";
    //----------------------
    this.topics;
    this.isExpanded = false;
    this.rotationId = rotationId;
    this.sectionElement = sectionElement;
    this.reloadExternalSection = null;
    //----------------------
    ReceivedTaskboardTasksSection_Initialise.call(this);
  }
  ReceivedTaskboardTasksSection.prototype.reload = function ()
  {
    var _this = this;

    $.ajax({
      url: _this.sectionUrl,
      method: "GET"
    })
    .done(function (newContent)
    {
      //--- update content -----
      _this.sectionElement.empty();
      $(newContent).children().each(function ()
      {
        _this.sectionElement.append(this);
      });
      //---- Init section ------
      ReceivedTaskboardTasksSection_Initialise.call(_this)
      //------------------------
    })
    .fail(function ()
    {
      var errorDialog = Relay.ErrorDialog();
      errorDialog.setCloseEvent(function ()
      {
        location.reload();
      });
      errorDialog.open();
    });
  };
  ReceivedTaskboardTasksSection.prototype.expand = function ()
  {
    this.isExpanded = true;
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].autoCollapseOff();
      this.topics[i].expand();
    }
  };
  ReceivedTaskboardTasksSection.prototype.collapse = function ()
  {
    this.isExpanded = false;
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].collapse();
      this.topics[i].autoCollapseOn();
    }
  };
  ReceivedTaskboardTasksSection.prototype.disableTopics = function ()
  {
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].disable();
    }
  }
  ReceivedTaskboardTasksSection.prototype.enableTopics = function ()
  {
    for (var i = 0; i < this.topics.length; ++i)
    {
      this.topics[i].enable();
    }
  }
  function ReceivedTaskboardTasksSection_Initialise()
  {
    var _this = this;
    this.topics = [];
    //-------
    $("tbody tr", this.sectionElement).each(function ()
    {
      var topic = ReceivedTaskboardTasksSection_BuildTopic.call(_this, this);

      if (_this.isExpanded)
      {
        topic.autoCollapseOff();
        topic.expand();
      }

      _this.topics.push(topic);
    })
  }
  function ReceivedTaskboardTasksSection_BuildTopic(trElement)
  {
    var _this = this;

    var topic = Relay.Topic(trElement, {
      updateUrl: this.updateTopicUrl,
      reloadUrl: this.reloadTopicUrl
    });

    topic.AddControl(Relay.Topic.ControlsFactory.TextField({
      modelPropName: "Location",
      elementClass: ".second-td"
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.TextField({
      modelPropName: "Name",
      elementClass: ".third-td",
      clickHandler: NameClickHandler
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
      readOnly: true
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
    topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
      readOnly: true
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.TextField({
      modelPropName: "CompleteStatus",
      elementClass: ".complite-status"
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
      forTask: true
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn());
    topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
    topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
      dialogType: Relay.AttachmentsDialog.Types.Received,
      container: Relay.AttachmentsDialog.Containers.Task
    }));
    topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
      dialogType: Relay.VoiceMessagesDialog.Types.Received,
      container: Relay.VoiceMessagesDialog.Containers.Task
    }));


    topic.updateView();

    return topic;
  }
  function ReceivedTaskboardTasksSection_TopicActionsChangeHandler(model)
  {
    if (this.reloadExternalSection)
      this.reloadExternalSection(Relay.SectionFactory.Types[model.SectionType]);
  }
  function ReceivedTaskboardTasksSection_ReloadAll()
  {
    Relay.ReportBuilderPanel.reload();
    Relay.DailyNotesPanel.reloadPanel();
  }
  //===============================================================================================
  if (window.Relay.SectionFactory)
  {
    window.Relay.SectionFactory.ReceivedTaskboardTasksSection = function (sectionElement, rotationId)
    {
      return new ReceivedTaskboardTasksSection(sectionElement, rotationId);
    };
  }
  else
  {
    console.error("Draft Tasks Section cannot work without SectionFactory.");
  }
  //===============================================================================================
})(window);