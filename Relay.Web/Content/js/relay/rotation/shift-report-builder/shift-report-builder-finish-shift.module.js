﻿; (function (window)
{
    //===========================================================================
    function ShiftHandoverFinishShiftBtn(controlElement, shiftId)
    {
        this.shiftId = shiftId;
        this.readOnly = null;
        this.controlElement = controlElement;
    }


    ShiftHandoverFinishShiftBtn.prototype.update = function ()
    {
        var _this = this;

        $(this.controlElement[0]).click(function ()
        {
            var confirmDialog = Relay.ConfirmDialog(
                "Are you sure you want to finish current shift?",
                "",
                "Yes",
                "No"
            );

            confirmDialog.setOkEvent(function ()
            {
                $.ajax({
                    url: "/Shift/FinishShift",
                    method: "POST",
                    data: { shiftId: _this.shiftId }
                })
                    .done(function (result)
                    {
                        if (result === 'ok')
                        {
                            location.reload();
                        }
                    })
                    .fail(function ()
                    {
                        Relay.ErrorDialog().open();
                    });
            });

            confirmDialog.open();
        });
    };

    ShiftHandoverFinishShiftBtn.prototype.build = function ()
    {
        this.update();
    }

    //===============================================================================================
    window.Relay = window.Relay || {};
    window.Relay.ShiftHandoverFinishShiftBtn = function (controlElement, shiftId)
    {
        return new ShiftHandoverFinishShiftBtn(controlElement, shiftId);
    }
    //===============================================================================================
})(window);


