﻿(function ()
{
  "use strict";
  //===========================================================================
  var receivedSelectedShiftVarName = "ReceivedSelectedShiftId";
  //===========================================================================
  function ShiftReceivedSubpanel(shiftId, controls, readOnly)
  {

    this.shiftId = shiftId;
    this.controls = controls;
    this.readOnly = readOnly;

    this.sections = [];
    this.IsExpanded = false;

    this.reportPanel = null;
    this.receivedPanelSelectedShift = null;

    controls.documentBtn.prop('disabled', true);
    controls.qmePdfReportBtn.prop('disabled', true);
    controls.finaliseAllBtn.hide();
    controls.pencilBlock.hide();
    controls.expandCollapseAllBtn.html("Expand All");
    controls.expandCollapseAllBtn.show();
    controls.shareReportBtnController.destroy();
    controls.draftFromToBlock.hide();

    if (readOnly)
    {
      controls.filterBtn.hide();
      controls.plusBtn.hide();
    }

    this.expandCollapseHandler = ReceivedSubpanel_ExpandCollapseAllBtnHandler.bind(this);
    controls.expandCollapseAllBtn[0].addEventListener("click", this.expandCollapseHandler, false);

    this.execHashParamsHandler = ReceivedSubpanel_ExecHashParams.bind(this);
    Relay.UrlHash.addChangeEvent(this.execHashParamsHandler);

    this.load();
  }
  ShiftReceivedSubpanel.prototype.dispose = function ()
  {

    Relay.UrlHash.removeChangeEvent(this.execHashParamsHandler);
    this.controls.expandCollapseAllBtn[0].removeEventListener('click', this.expandCollapseHandler, false);

    this.controls = null;
    this.sections = null;

    var hashModel = Relay.UrlHash.getHashModel();
    hashModel[receivedSelectedShiftVarName] = null;
    Relay.UrlHash.updateHashModel(hashModel);
  };
  ShiftReceivedSubpanel.prototype.getReceivedShiftId = function ()
  {
    var hashModel = Relay.UrlHash.getHashModel();
    return hashModel[receivedSelectedShiftVarName];
  };
  ShiftReceivedSubpanel.prototype.getShiftId = function ()
  {
    return this.shiftId;
  };

  ShiftReceivedSubpanel.prototype.load = function ()
  {
    var _this = this;
    var hashModel = Relay.UrlHash.getHashModel();


    if (hashModel[receivedSelectedShiftVarName])
    {
      this.controls.documentBtn.prop('disabled', false);
      this.controls.qmePdfReportBtn.prop('disabled', false);
    }

    //-----------------------------------
    var loadingAnimationTimeout = setTimeout(function ()
    {
      $('#loading-animation').show();
    }, 100);
    //-----------------------------------
    $.ajax({
      url: "/ShiftReportBuilder/ReceivedPanel",
      method: "GET",
      data: {
        shiftId: this.shiftId,
        recivedShiftId: hashModel[receivedSelectedShiftVarName]
      }
    })
    .done(function (result)
    {
      //--------------
      var div = document.createElement(div);
      div.innerHTML = result;
      //--------------
      _this.reportPanel = $(".report-panel", div);
      _this.receivedPanelSelectedShift = $(".received-panel-selected-shift", div);
      _this.controls.panelContainer.html(_this.reportPanel);

      if ($(".received-history-container", _this.reportPanel).length)
      {
        ReceivedSubpanel_InitReceivedSwiper.call(_this);
        ReceivedSubpanel_Init.call(_this);
      }
      else
      {
        _this.controls.dateBlock.html("&mdash;");
        _this.controls.nameBlock.html("&mdash;");
        _this.controls.documentBtn.prop('disabled', true);
        _this.controls.qmePdfReportBtn.prop('disabled', true);
      }
      //--------------
    })
    .fail(function ()
    {
      Relay.ErrorDialog().open();
    })
    .always(function ()
    {
      clearTimeout(loadingAnimationTimeout);
      $('#loading-animation').hide();
    });
    //-----------------------------------
  }
  ShiftReceivedSubpanel.prototype.reloadSection = function (sectionType) { };

  function ReceivedSubpanel_ExecHashParams(model, modelChangesMap)
  {
    if (modelChangesMap[receivedSelectedShiftVarName])
    {

      ReceivedSubpanel_LoadForSelectedShift.call(this, model);

      var historySection = $(".received-history-container", this.reportPanel);
      var activeSlide = $(".swiper-slide[shift-id='" + model[receivedSelectedShiftVarName] + "']", historySection);

      $(".swiper-slide", historySection).removeClass("active");

      if (activeSlide.length)
      {
        activeSlide.addClass("active");
        this.controls.dateBlock.text($(".date-body", activeSlide).text());
        this.controls.nameBlock.text("From " + $(".handover-creator-body .user-name", activeSlide).text());
      }
      else
      {
        this.controls.dateBlock.html("&mdash;");
        this.controls.nameBlock.html("&mdash;");
      }
    }
  }

  function ReceivedSubpanel_LoadForSelectedShift(hashModel)
  {
    var _this = this;
    //-----------------------------------
    this.IsExpanded = false;
    this.controls.expandCollapseAllBtn.html("Expand All");
    //-----------------------------------
    var loadingAnimationTimeout = setTimeout(function ()
    {
      $('#loading-animation').show();
    }, 100);
    //-----------------------------------
    $.ajax({
      url: "/ShiftReportBuilder/ReceivedPanelSections",
      method: "GET",
      data: {
        shiftId: this.shiftId,
        recivedShiftId: hashModel[receivedSelectedShiftVarName]
      }
    })
    .done(function (result)
    {
      //--------------
      var div = document.createElement(div);
      div.innerHTML = result;
      //--------------
      var receivedPanelSelectedShift = $(".received-panel-selected-shift", div);


      _this.receivedPanelSelectedShift.remove();
      _this.receivedPanelSelectedShift = receivedPanelSelectedShift;
      _this.reportPanel.append(_this.receivedPanelSelectedShift);

      if (receivedPanelSelectedShift.length)
      {
        _this.receivedPanelSelectedShift.html(receivedPanelSelectedShift.children());
        ReceivedSubpanel_Init.call(_this);
      }
      else
      {
        _this.receivedPanelSelectedShift.empty();
      }
      //--------------
    })
    .fail(function ()
    {
      Relay.ErrorDialog().open();
    })
    .always(function ()
    {
      clearTimeout(loadingAnimationTimeout);
      $('#loading-animation').hide();
    });
    //-----------------------------------
  }

  function ReceivedSubpanel_InitReceivedSwiper(shiftContainer)
  {
    var _this = this;

    var historySection = $(".received-history-container", this.reportPanel);

    var swiper = new Swiper($(".received-swiper", historySection), {
      direction: "vertical",
      height: 50,
      nextButton: $(".received-swiper-next", historySection),
      prevButton: $(".received-swiper-prev", historySection)
    });

    var recivedShiftId = Relay.UrlHash.getHashModel()[receivedSelectedShiftVarName];

    if ($(".swiper-slide[shift-id='" + recivedShiftId + "']", historySection).length)
    {
      $(".swiper-slide", historySection).each(function (index)
      {
        if (recivedShiftId === $(this).attr("shift-id"))
        {
          $(this).addClass("active");
          _this.controls.dateBlock.text($(".date-body", this).text());
          _this.controls.nameBlock.text("From " + $(".handover-creator-body .user-name", this).text());
          swiper.slideTo(index, 0, true);
        }
      });
    }
    else
    {
      this.controls.dateBlock.html("&mdash;");
      this.controls.nameBlock.html("&mdash;");
    }

    $(".swiper-slide", historySection).click(function ()
    {
      var hashModel = Relay.UrlHash.getHashModel();
      hashModel[receivedSelectedShiftVarName] = null;
      hashModel[receivedSelectedShiftVarName] = $(this).attr("shift-id");
      Relay.UrlHash.updateHashModel(hashModel);

      _this.controls.documentBtn.prop('disabled', false);
      _this.controls.qmePdfReportBtn.prop('disabled', false);

    });
  }

  function ReceivedSubpanel_ExpandCollapseAllBtnHandler()
  {
    if (!this.IsExpanded)
    {
      for (var i = 0; i < this.sections.length; ++i)
      {
        this.sections[i].expand();
      }

      this.IsExpanded = true;
      this.controls.expandCollapseAllBtn.html("Collapse All");
    }
    else
    {
      for (var i = 0; i < this.sections.length; ++i)
      {
        this.sections[i].collapse();
      }

      this.IsExpanded = false;
      this.controls.expandCollapseAllBtn.html("Expand All");
    }
  };

  function ReceivedSubpanel_Init()
  {
    this.sections = [];

    this.sections.push(Relay.SectionFactory.ShiftReceivedCoreSection($("#coreSection"), this.shiftId, this.readOnly));
    this.sections.push(Relay.SectionFactory.ShiftReceivedProjectsSection($("#projectsSection"), this.shiftId, this.readOnly));
    this.sections.push(Relay.SectionFactory.ShiftReceivedSafetySection($("#safetySection"), this.shiftId, this.readOnly));
    this.sections.push(Relay.SectionFactory.ShiftReceivedTasksSection($("#tasksSection"), this.shiftId, this.readOnly));
    this.sections.push(Relay.SectionFactory.ShiftReceivedTeamSection($("#teamSection"), this.shiftId, this.readOnly));
    this.sections.push(Relay.SectionFactory.ShiftReceivedTaskBoardTaskBoardTasksSection($("#taskboardTasksSection"), this.shiftId, true));

  }
  //===========================================================================
  window.Relay = window.Relay || {};
  window.Relay.ShiftReceivedSubpanel = function (shiftId, controls, readOnly)
  {
    return new ShiftReceivedSubpanel(shiftId, controls, readOnly);
  };
  //===========================================================================
})();