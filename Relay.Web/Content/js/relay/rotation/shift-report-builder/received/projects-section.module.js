﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  shiftId: (shift id)
    //  readOnly: (readonly mode flag)
    //==============================  Received Projects Section =====================================

    function ReferenceColumnClickHandlerReceivedDraft(model, reload) {
        if (!model.IsNullReport) {
            var editHSETopicDialog = Relay.FormDialog({
                url: "/Topic/TopicDetailsDialog?topicid=" + model.Id,
                method: "GET",
                dialogId: "topicDetailsDialog"
            });
            editHSETopicDialog.setSubmitEvent(reload);
            editHSETopicDialog.open();
        }
    }
    function ShiftReceivedProjectsSection(sectionElement, shiftId, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Project;
        var controller = "/ShiftProjectsSection";
        this.sectionUrl = controller + "/Received?shiftId=" + shiftId;
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.shiftId = shiftId;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        ReceivedProjectsSection_Initialise.call(this);
    }
    ShiftReceivedProjectsSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: _this.sectionUrl,
            method: "GET"
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            ReceivedProjectsSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ShiftReceivedProjectsSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ShiftReceivedProjectsSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    function ReceivedProjectsSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = ReceivedProjectsSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        })
    }
    function ReceivedProjectsSection_BuildTopic(trElement) {
        
        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: true,
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Project",
            elementClass: ".type-body",
            clickHandler: ReferenceColumnClickHandlerReceivedDraft
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".reference-body",
            clickHandler: ReferenceColumnClickHandlerReceivedDraft
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            dialogType: this.readOnly ? Relay.ManagerCommentsDialog.Types.Received : Relay.ManagerCommentsDialog.Types.Draft,
            container: Relay.ManagerCommentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBtn({
            dialogType: "Received",
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PlannedActualBtn({
            readOnly: this.readOnly
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
        //    dialogType: this.readOnly ? Relay.TasksListDialog.Types.ReadOnly : Relay.TasksListDialog.Types.Received
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: Relay.AttachmentsDialog.Types.Received,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
        //    dialogType: Relay.VoiceMessagesDialog.Types.Received,
        //    container: Relay.VoiceMessagesDialog.Containers.Topic
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopic());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopicReceived());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBody());
        topic.updateView();

        return topic;
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ShiftReceivedProjectsSection = function (sectionElement, shiftId, readOnly) {
            return new ShiftReceivedProjectsSection(sectionElement, shiftId, readOnly);
        };
    }
    else {
        console.error("Received Projects Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);