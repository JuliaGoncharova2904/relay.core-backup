﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  shiftId: (shift id)
    //  readOnly: (readonly mode flag)
    //==============================  Draft Safety Section ==========================================
    //----------------- Reference Column Handler --------------------------
    function ReferenceColumnClickHandler(model, reload, update) {
        if (!model.IsNullReport) {
            var editHSETopicDialog = Relay.FormDialog({
                url: "/ShiftHSESection/EditTopicDialog?topic=" + model.Id,
                method: "GET",
                dialogId: "HSETopicFormDialog"
            });
            editHSETopicDialog.setSubmitEvent(reload);
            editHSETopicDialog.open();
        }
    }

    function ReferenceColumnClickHandlerReceivedDraft(model, reload) {
        if (!model.IsNullReport) {
            var editHSETopicDialog = Relay.FormDialog({
                url: "/Topic/TopicDetailsDialog?topicid=" + model.Id,
                method: "GET",
                dialogId: "topicDetailsDialog"
            });
            editHSETopicDialog.setSubmitEvent(reload);
            editHSETopicDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function ShiftDraftSafetySection(sectionElement, shiftId, readOnly, filter) {
        //----------------------
        this.filterType = Relay.UrlHash.getHashParam('filter') || 'All';
        this.type = Relay.SectionFactory.Types.HSE;
        var controller = "/ShiftHSESection";
        this.sectionUrl = controller + "/Draft";
        this.updateTopicUrl = controller + "/UpdateTopic";
        this.reloadTopicUrl = controller + "/ReloadTopic";
        this.addInlineTopicUrl = controller + "/AddInlineTopic";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.shiftId = shiftId;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        this.openInlineTopic = null;
        this.closeInlineTopic = null;
        //----------------------
        DraftSafetySection_Initialise.call(this);
    }
    ShiftDraftSafetySection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: { shiftId: this.shiftId }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //---- Init section ------
            DraftSafetySection_Initialise.call(_this);
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
        //-----------------------------
        if (Relay.DailyReportsPanel) {
            Relay.DailyReportsPanel.reloadSlideById(this.shiftId);
        }
        //-----------------------------
    };
    ShiftDraftSafetySection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ShiftDraftSafetySection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    ShiftDraftSafetySection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    ShiftDraftSafetySection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftSafetySection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        if (!$("tbody tr.no-items-message", this.sectionElement).length) {
            $("tbody tr", this.sectionElement).each(function () {
                var topic = DraftSafetySection_BuildTopic.call(_this, this);
                if (_this.isExpanded) {
                    topic.autoCollapseOff();
                    topic.expand();
                }

                _this.topics.push(topic);
            });
        }
        //-------
        if (this.readOnly) {
            $(".add-item-btn", this.sectionElement).parent().hide();
            
            if ($(".no-items-message", this.sectionElement).length) {
                $(".table", this.sectionElement).hide();
                $(".no-items-message", this.sectionElement).show();
            }
        } else {
            var addInlineTopic = Relay.AddInlineTopic(this.sectionElement, {
                url: this.addInlineTopicUrl,
                destId: this.shiftId,
                topicName: this.type,
                warningMessage: "Please choose a Safety Type and enter a Reference. Exit?"
            });
            addInlineTopic.setSaveEvent(this.reload.bind(this));
            addInlineTopic.setOpenEvent(function () {
                if (_this.openInlineTopic)
                    _this.openInlineTopic();
            });
            addInlineTopic.setCloseEvent(function () {
                if (_this.closeInlineTopic)
                    _this.closeInlineTopic();
            });
        }
    }
    function DraftSafetySection_BuildTopic(trElement) {
        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            readOnly: this.readOnly,
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.topic,
            changeHandler: DraftSafetySection_RemoveTopicHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NullReportBtn({
            readOnly: this.readOnly,
            changeHandler: DraftSafetySection_ReloadTopicHandler.bind(this),
            rotationType: "Shift",
            type: Relay.SectionFactory.Types.HSE
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: this.readOnly,
            changeHandler: DraftSafetySection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            readOnly: this.readOnly,
            changeHandler: DraftSafetySection_TasksAndActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Type",
            elementClass: ".type-body",
            clickHandler: this.readOnly ? ReferenceColumnClickHandlerReceivedDraft : ReferenceColumnClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Reference",
            elementClass: ".reference-body",
            clickHandler: this.readOnly ? ReferenceColumnClickHandlerReceivedDraft : ReferenceColumnClickHandler
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            dialogType: this.readOnly ? Relay.ManagerCommentsDialog.Types.Received : Relay.ManagerCommentsDialog.Types.Draft,
            container: Relay.ManagerCommentsDialog.Containers.Topic
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBtn({
            dialogType: "Draft",
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PlannedActualBtn({
            readOnly: this.readOnly
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.TasksBtn({
        //    dialogType: this.readOnly ? Relay.TasksListDialog.Types.ReadOnly : Relay.TasksListDialog.Types.Draft,
        //    changeHandler: DraftSafetySection_TasksAndActionsChangeHandler.bind(this)
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: this.readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Topic
        }));
        //topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
        //    dialogType: Relay.VoiceMessagesDialog.Types.Draft,
        //    container: Relay.VoiceMessagesDialog.Containers.Topic
        //}));
        topic.AddControl(Relay.Topic.ControlsFactory.ShareBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopicReceived());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsTopic());
        topic.AddControl(Relay.Topic.ControlsFactory.TagsBody());

        topic.updateView();

        return topic;
    }

    function DraftSafetySection_TasksAndActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }
    function DraftSafetySection_RemoveTopicHandler(model) {
        this.reload();
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }

    function DraftSafetySection_ReloadTopicHandler(model) {
        if (this.filterType === 'HandoverItems') {
            this.reload();
        }
        //if (this.reloadExternalSection)
        //    this.reloadExternalSection(Relay.SectionFactory.Types.Tasks);
    }

    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ShiftDraftSafetySection = function (sectionElement, shiftId, readOnly) {
            return new ShiftDraftSafetySection(sectionElement, shiftId, readOnly);
        };
    }
    else {
        console.error("Draft Safety Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);