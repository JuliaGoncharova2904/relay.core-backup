﻿; (function (window) {
    //===============================================================================================
    //  sectionElement: (section div)
    //  shiftId: (shift id)
    //  readOnly: (readonly mode flag)
    //==============================  Draft Tasks Section ===========================================
    //------------------- Name Column Handlers ----------------------------
    function NameClickHandlerEditMode(model, reload, update) {
        if (!model.IsNullReport) {
            var tasksListDialog = Relay.FormDialog({
              url: "/TaskBoard/TaskDetailsDialog",
                method: "GET",
                data: { TaskId: model.Id }
            });
          //  tasksListDialog.setCloseEvent(reload);
            tasksListDialog.open();
        }
    }
    function NameClickHandlerReadMode(model, reload, update) {
        if (!model.IsNullReport) {
            var tasksListDialog = Relay.FormDialog({
              url: "/TaskBoard/TaskDetailsDialog",
                method: "GET",
                data: { TaskId: model.Id }
            });
       //     tasksListDialog.setCloseEvent(reload);
            tasksListDialog.open();
        }
    }
    //---------------------------------------------------------------------
    function ShiftDraftTaskBoardTasksSection(sectionElement, shiftId, readOnly) {
        //----------------------
        this.type = Relay.SectionFactory.Types.Tasks;
        var controller = "/ShiftTaskBoardTasksSection";
        this.sectionUrl = controller + "/Draft?shiftId=" + shiftId;
        this.updateTopicUrl = controller + "/UpdateTask";
        this.reloadTopicUrl = controller + "/ReloadTask";
        //----------------------
        this.topics;
        this.isExpanded = false;
        this.shiftId = shiftId;
        this.readOnly = readOnly;
        this.sectionElement = sectionElement;
        this.reloadExternalSection = null;
        //----------------------
        DraftTasksSection_Initialise.call(this);
    }
    ShiftDraftTaskBoardTasksSection.prototype.reload = function () {
        var _this = this;

        $.ajax({
            url: this.sectionUrl,
            method: "GET",
            data: { shiftId: this.shiftId }
        })
        .done(function (newContent) {
            //--- update content -----
            _this.sectionElement.html($(newContent).children());
            //-- Init section ------
            DraftTasksSection_Initialise.call(_this)
            //------------------------
        })
        .fail(function () {
            var errorDialog = Relay.ErrorDialog();
            errorDialog.setCloseEvent(function () {
                location.reload();
            });
            errorDialog.open();
        });
    };
    ShiftDraftTaskBoardTasksSection.prototype.expand = function () {
        this.isExpanded = true;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].autoCollapseOff();
            this.topics[i].expand();
        }
    };
    ShiftDraftTaskBoardTasksSection.prototype.collapse = function () {
        this.isExpanded = false;
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].collapse();
            this.topics[i].autoCollapseOn();
        }
    };
    ShiftDraftTaskBoardTasksSection.prototype.disableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].disable();
        }
    }
    ShiftDraftTaskBoardTasksSection.prototype.enableTopics = function () {
        for (var i = 0; i < this.topics.length; ++i) {
            this.topics[i].enable();
        }
    }
    function DraftTasksSection_Initialise() {
        var _this = this;
        this.topics = [];
        //-------
        $("tbody tr", this.sectionElement).each(function () {
            var topic = DraftTasksSection_BuildTopic.call(_this, this);

            if (_this.isExpanded) {
                topic.autoCollapseOff();
                topic.expand();
            }

            _this.topics.push(topic);
        });

        if (this.readOnly) {
            $(".complete-status", this.sectionElement).show();
            $(".no-complete-status", this.sectionElement).hide();
        }
    }
    function DraftTasksSection_BuildTopic(trElement) {

        var topic = Relay.Topic(trElement, {
            updateUrl: this.updateTopicUrl,
            reloadUrl: this.reloadTopicUrl
        });

        topic.AddControl(Relay.Topic.ControlsFactory.RemoveTopicBtn({
            readOnly: this.readOnly,
            type: Relay.Topic.ControlsFactory.RemoveTopicBtn.Type.task,
            changeHandler: DraftTasksSection_ReloadAll
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TransferredBtn({
            readOnly: this.readOnly,
            changeHandler: DraftTasksSection_TopicActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PinnedBtn({
            readOnly: this.readOnly,
            changeHandler: DraftTasksSection_TopicActionsChangeHandler.bind(this)
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Location",
            elementClass: ".type-body"
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TextField({
            modelPropName: "Name",
            elementClass: ".reference-body",
            clickHandler: this.readOnly ? NameClickHandlerReadMode : NameClickHandlerEditMode
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.NotesField({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.PriorityField());
        topic.AddControl(Relay.Topic.ControlsFactory.FinaliseBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.ManagerCommentsBtn({
            readOnly: this.readOnly,
            forTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.TeamBtn({
            readOnly: this.readOnly,
            forDraftTask: true
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.MoreBtn());
        topic.AddControl(Relay.Topic.ControlsFactory.AttachmentsBtn({
            dialogType: this.readOnly ? Relay.AttachmentsDialog.Types.Received : Relay.AttachmentsDialog.Types.Draft,
            container: Relay.AttachmentsDialog.Containers.Task
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.LocationBtn({
            readOnly: this.readOnly
        }));
        topic.AddControl(Relay.Topic.ControlsFactory.VoiceMessagesBtn({
            dialogType: Relay.VoiceMessagesDialog.Types.Draft,
            container: Relay.VoiceMessagesDialog.Containers.Task
        }));

        topic.AddControl(Relay.Topic.ControlsFactory.ContributorIcon());

        topic.updateView();

        return topic;
    }
    function DraftTasksSection_TopicActionsChangeHandler(model) {
        if (this.reloadExternalSection)
            this.reloadExternalSection(Relay.SectionFactory.Types[model.SectionType]);
    }
    function DraftTasksSection_ReloadAll() {
        Relay.ShiftReportBuilderPanel.reload();
    }
    //===============================================================================================
    if (window.Relay.SectionFactory) {
        window.Relay.SectionFactory.ShiftDraftTaskBoardTasksSection = function (sectionElement, shiftId, readOnly) {
            return new ShiftDraftTaskBoardTasksSection(sectionElement, shiftId, readOnly);
        };
    }
    else {
        console.error("Draft Tasks Section cannot work without SectionFactory.");
    }
    //===============================================================================================
})(window);