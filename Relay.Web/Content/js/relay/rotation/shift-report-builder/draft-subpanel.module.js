﻿(function ()
{
    "use strict";
    //===========================================================================
    function ShiftDraftSubpanel(shiftId, controls, readOnly)
    {
        this.shiftId = shiftId;
        this.controls = controls;
        this.readOnly = readOnly;

        this.sections = [];
        this.IsExpanded = false;
        this.shareReportIcon = null;

        controls.documentBtn.prop('disabled', false);
        controls.qmePdfReportBtn.prop('disabled', false);
        controls.finaliseAllBtn.show();
        controls.expandCollapseAllBtn.html("Expand All");
        controls.expandCollapseAllBtn.show();
        controls.pencilBlock.show();
        controls.shareReportBtnController.build(shiftId, "Shift");
        controls.filterBtn.build(shiftId, readOnly, this);
        controls.draftFromToBlock.show();

        if (readOnly)
        {
            controls.pencilBlock.hide();
            //controls.filterBtn.hide();
            controls.finaliseAllBtn.hide();
            controls.plusBtn.hide();
        }
        else
        {
            this.finaliseHandler = DraftSubpanel_FinaliseAllBtnHandler.bind(this);
            controls.finaliseAllBtn[0].addEventListener("click", this.finaliseHandler, false);
            this.editShiftRecipient = DraftSubpanel_EditShiftRecipient.bind(this);
            controls.pencilBlock[0].addEventListener("click", this.editShiftRecipient, false);
        }

        this.expandCollapseHandler = DraftSubpanel_ExpandCollapseAllBtnHandler.bind(this);
        controls.expandCollapseAllBtn[0].addEventListener("click", this.expandCollapseHandler, false);

        this.load();
    }
    ShiftDraftSubpanel.prototype.dispose = function ()
    {
        if (!this.readOnly)
        {
            this.controls.finaliseAllBtn[0].removeEventListener('click', this.finaliseHandler, false);
            this.controls.pencilBlock[0].removeEventListener('click', this.editShiftRecipient, false);
        }

        this.controls.expandCollapseAllBtn[0].removeEventListener('click', this.expandCollapseHandler, false);

        this.controls = null;
        this.sections = null;
    };
    ShiftDraftSubpanel.prototype.getShiftId = function ()
    {
        return this.shiftId;
    };
    ShiftDraftSubpanel.prototype.load = function ()
    {
        var _this = this;

        var filterType = Relay.UrlHash.getHashParam('filter') || 'HandoverItems';

        if (filterType === 'NrItems' && _this.readOnly === false)
        {
            if (_this.controls)
            {
                _this.controls.finaliseAllBtn.hide();
                _this.controls.expandCollapseAllBtn.hide();
            }

        } else if (_this.readOnly === false)
        {
            if (_this.controls)
            {
                _this.controls.finaliseAllBtn.show();
                _this.controls.expandCollapseAllBtn.show();
            }
        }
        //-----------------------------------
        var loadingAnimationTimeout = setTimeout(function ()
        {
            $('#loading-animation').show();
        }, 200);

      //  $('#loading-animation').show();
        //-----------------------------------
        $.ajax({
            url: "/ShiftReportBuilder/DraftPanel",
            method: "GET",
            data: { shiftId: this.shiftId, filter: filterType }
        })
        .done(function (result)
        {
            //--------------
            var div = document.createElement(div);
            div.innerHTML = result;
            //--------------
            var reportPanel = $(".report-panel", div);
            var rotationId = reportPanel.attr("rotation-id");

            if (_this.controls !== null)
            {
                DraftSubpanel_LoadFromToBlock.call(_this, rotationId);
                _this.controls.panelContainer.empty();
                _this.controls.panelContainer.html(reportPanel);
                _this.controls.dateBlock.html(reportPanel.attr("shift-period"));
                _this.controls.nameBlock.html(reportPanel.attr("shift-recipient"));
                DraftSubpanel_Init.call(_this);

                clearTimeout(loadingAnimationTimeout);
                $('#loading-animation').hide();
            }
            //--------------
        })
        .fail(function ()
        {
            Relay.ErrorDialog().open();
        })
        .always(function ()
        {
            //clearTimeout(loadingAnimationTimeout);
            //$('#loading-animation').hide();
        });
        //-----------------------------------
    }
    ShiftDraftSubpanel.prototype.reloadSection = function (sectionType)
    {
        DraftSubpanel_ReloadHandler.call(this, sectionType);
    }

    function DraftSubpanel_ExpandCollapseAllBtnHandler()
    {
        if (!this.IsExpanded)
        {
            for (var i = 0; i < this.sections.length; ++i)
            {
                this.sections[i].expand();
            }

            this.IsExpanded = true;
            this.controls.expandCollapseAllBtn.html("Collapse All");
        }
        else
        {
            for (var i = 0; i < this.sections.length; ++i)
            {
                this.sections[i].collapse();
            }

            this.IsExpanded = false;
            this.controls.expandCollapseAllBtn.html("Expand All");
        }
    };

    function DraftSubpanel_LoadFromToBlock(rotationId) {
        var _this = this;
        //-----------------------------------
        $.ajax({
            url: "/ShiftReportBuilder/FromToBlock",
            method: "GET",
            data: { rotationId: _this.shiftId }
        })
            .done(function (result) {
                //--------------
                var div = document.createElement(div);
                div.innerHTML = result;
                //--------------
                if (_this.controls != null &&_this.controls.draftFromToBlock != null) {
                    _this.controls.draftFromToBlock.html($(div).html());
                }
                //--------------
            })
            .fail(function () {
                Relay.ErrorDialog().open();
            });
        //-----------------------------------
    }

    function DraftSubpanel_FinaliseAllBtnHandler()
    {
        var _this = this;

        var confirmDialog = Relay.ConfirmDialog(
            "Are you sure you want to finalise every topic and task?",
            "",
            "Yes",
            "No"
        );

        confirmDialog.setOkEvent(function ()
        {
            $.ajax({
                url: "/ShiftReportBuilder/FinalizeAll",
                method: "POST",
                data: { shiftId: _this.shiftId }
            })
           .done(function (result)
           {
               _this.load();
           })
           .fail(function ()
           {
               Relay.ErrorDialog().open();
           })
        });

        confirmDialog.open();
    }

    function DraftSubpanel_Init()
    {
        this.sections = [];

        this.sections.push(Relay.SectionFactory.ShiftDraftCoreSection($("#coreSection"), this.shiftId, this.readOnly));
        this.sections.push(Relay.SectionFactory.ShiftDraftProjectsSection($("#projectsSection"), this.shiftId, this.readOnly));
        this.sections.push(Relay.SectionFactory.ShiftDraftSafetySection($("#safetySection"), this.shiftId, this.readOnly));
        this.sections.push(Relay.SectionFactory.ShiftDraftTasksSection($("#tasksSection"), this.shiftId, this.readOnly));
        this.sections.push(Relay.SectionFactory.ShiftDraftTeamSection($("#teamSection"), this.shiftId, this.readOnly));
        this.sections.push(Relay.SectionFactory.ShiftDraftTaskBoardTasksSection($("#taskboardTasksSection"), this.shiftId, true));

        for (var i = 0; i < this.sections.length; ++i)
        {
            this.sections[i].reloadExternalSection = DraftSubpanel_ReloadHandler.bind(this);
            if (!this.readOnly)
            {
                this.sections[i].openInlineTopic = DraftSubpanel_OpenInlineTopicHandler.bind(this);
                this.sections[i].closeInlineTopic = DraftSubpanel_CloseInlineTopicHandler.bind(this);
            }
        }
    }
    function DraftSubpanel_ReloadHandler(sectionType)
    {
        for (var i = 0; i < this.sections.length; ++i)
        {
            if (this.sections[i].type === sectionType)
                this.sections[i].reload();
        }
    }
    function DraftSubpanel_OpenInlineTopicHandler()
    {
        Relay.ShiftReportBuilderPanel.disable();
        Relay.Header.disable();
        Relay.NavigationBar.disable();
        for (var i = 0; i < this.sections.length; ++i)
        {
            this.sections[i].disableTopics();
        }
    }
    function DraftSubpanel_CloseInlineTopicHandler()
    {
        Relay.ShiftReportBuilderPanel.enable();
        Relay.Header.enable();
        Relay.NavigationBar.enable();

        for (var i = 0; i < this.sections.length; ++i)
        {
            this.sections[i].enableTopics();
        }
    }

    function DraftSubpanel_EditShiftRecipient()
    {
        var formDialog = Relay.FormDialog({
            url: "/Shift/ChooseShiftRecipient",
            data: { shiftId: this.shiftId }
        });
        formDialog.setSubmitEvent(function ()
        {
            location.reload();
        });
        formDialog.open();
    }
    //===========================================================================
    window.Relay = window.Relay || {};
    window.Relay.ShiftDraftSubpanel = function (userId, controls, readOnly)
    {
        return new ShiftDraftSubpanel(userId, controls, readOnly);
    };
    //===========================================================================
})();