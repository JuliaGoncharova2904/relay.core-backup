﻿; (function (window)
{
    //===========================================================================
    function ShiftHandoverFilterBtn(controlElement)
    {
        this.shiftId = null;
        this.readOnly = null;
        this.shiftDraftSubpanel = null;
        this.filterType = null;
        this.controlElement = controlElement;
    }

    ShiftHandoverFilterBtn.prototype.destroy = function ()
    {
        this.shiftId = null;
        this.readOnly = null;
        this.shiftDraftSubpanel = null;
    };

    ShiftHandoverFilterBtn.prototype.hide = function () {
        if (this.controlElement)
            this.controlElement.hide();
    };

    ShiftHandoverFilterBtn.prototype.update = function ()
    {
        var _this = this;

        if (!(this.shiftId && this.filterType))
            return;

        var hashModel = Relay.UrlHash.getHashModel();
        _this.filterType = hashModel["filter"] || 'HandoverItems';
        Relay.UrlHash.setHashParam('filter', _this.filterType);

        $(_this.controlElement).webuiPopover('destroy');

        $(_this.controlElement).webuiPopover({
            hideEmpty: true,
            placement: 'bottom',
            style: "shift-filter",
            trigger: 'click',
            multi: false,
            dismissible: true,
            type: "async",
            cache: false,
            width: '118',
            url: "/ShiftReportBuilder/ReportFilterPopover?shiftId=" + _this.shiftId + "&filter=" + _this.filterType,
            offsetLeft: 0,
            async: {
                success: function (that, data)
                {
                    _this.bind(data);
                }
            },
            onShow: function ()
            {
                window.addEventListener("resize", resizeHandler, false);
            },
            onHide: function ()
            {
                window.removeEventListener("resize", resizeHandler, false);
            }
        });
        function resizeHandler()
        {
            WebuiPopovers.hide(_this.controlElement);
        };
        //--------------------
    };

    ShiftHandoverFilterBtn.prototype.bind = function ()
    {
        var _this = this;

        $("#handoverReport").on('click', function ()
        {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel["filter"] = "HandoverItems";
            Relay.UrlHash.updateHashModel(hashModel);
            WebuiPopovers.hide(_this.controlElement);

            _this.update();

            _this.shiftDraftSubpanel.load();
        });

        $("#nrTopics").on('click', function ()
        {
            var hashModel = Relay.UrlHash.getHashModel();
            hashModel["filter"] = "NrItems";
            Relay.UrlHash.updateHashModel(hashModel);
            WebuiPopovers.hide(_this.controlElement);

            _this.update();

            _this.shiftDraftSubpanel.load();
        });
    }

    ShiftHandoverFilterBtn.prototype.build = function (shiftId, readOnly, shiftDraftSubpanel)
    {
        this.destroy();
        this.shiftId = shiftId;
        this.readOnly = readOnly;
        this.shiftDraftSubpanel = shiftDraftSubpanel;
        this.filterType = Relay.UrlHash.getHashParam('filter') || 'HandoverItems';
        Relay.UrlHash.setHashParam('filter', this.filterType);
        //--------------------
        this.update();
        //--------------------
    }
    //===============================================================================================
    window.Relay = window.Relay || {};
    window.Relay.ShiftHandoverFilterBtn = function (controlElement)
    {
        return new ShiftHandoverFilterBtn(controlElement);
    }
    //===============================================================================================
})(window);


