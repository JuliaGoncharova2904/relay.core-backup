'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');



var paths = {
    mainLess: ['./Content/styles/main.less'],
    watchLess: ['./Content/styles/**/*.less']
};

var enterpriseAuthPaths = {
    mainLess: ['./Content/styles/Enterprise.Auth.less'],
    watchLess: ['./Content/styles/auth/Enterprise/**/*.less']
};

var multiTenancyAuthPaths = {
    mainLess: ['./Content/styles/MultiTenancy.Auth.less'],
    watchLess: ['./Content/styles/auth/MultiTenancy/**/*.less']
};


var themeDialogPaths = {
    whiteStaticPath: ['./Content/styles/white-theme.less'],
    blackStaticPath: ['./Content/styles/black-theme.less'],
    watchPath: ['./Content/styles/**/*.less']
};

/*
 | --- DEFAULT -----------------------------------------------
 */

gulp.task('default', ['less', 'lint']);


/*
 | --- Styles -----------------------------------------------
 */


gulp.task('less', function (done)
{
    gulp.src(paths.mainLess)
    .pipe(concat('main.css'))
    .pipe(less())
    .pipe(gulp.dest('./Content/styles'))
    .on('end', done);
});


gulp.task('enterprise-auth-less', function (done)
{
    gulp.src(enterpriseAuthPaths.mainLess)
    .pipe(concat('Enterprise.Auth.css'))
    .pipe(less())
    .pipe(gulp.dest('./Content/styles'))
    .on('end', done);
});

gulp.task('enterprise-auth-less-watch', function ()
{
    gulp.watch(enterpriseAuthPaths.watchLess, ['enterprise-auth-less-watch']);
});

gulp.task('multiTenancy-auth-less', function (done)
{
    gulp.src(multiTenancyAuthPaths.mainLess)
    .pipe(concat('MultiTenancy.Auth.css'))
    .pipe(less())
    .pipe(gulp.dest('./Content/styles'))
    .on('end', done);
});

gulp.task('multiTenancy-auth-less-watch', function ()
{
    gulp.watch(multiTenancyAuthPaths.watchLess, ['multiTenancy-auth-less']);
});


/*
 * --- Black-theme-dialogs ---------------------------
 */

gulp.task('black-theme', function (done) {
    gulp.src(themeDialogPaths.blackStaticPath)
        .pipe(concat('black-theme.css'))
        .pipe(less())
        .pipe(gulp.dest('./Content/styles'))
        .on('end', done);
});

/*
 * --- White-theme-dialogs ---------------------------
 */

gulp.task('white-theme', function (done) {
    gulp.src(themeDialogPaths.whiteStaticPath)
        .pipe(concat('white-theme.css'))
        .pipe(less())
        .pipe(gulp.dest('./Content/styles'))
        .on('end', done);
});


/*
 | --- JSHint ------------------------------------------
 */

gulp.task('lint', function ()
{
    return gulp.src('./Content/js/**/*.js')
      .pipe(jshint())
      .pipe(jshint.reporter('jshint-stylish'));
});


/*
 | --- WATCH ------------------------------------------
 */

gulp.task('watch', function ()
{
    gulp.watch(paths.watchLess, ['less']);
    gulp.watch(themeDialogPaths.watchPath, ['white-theme']);
    gulp.watch(themeDialogPaths.watchPath, ['black-theme']);
});