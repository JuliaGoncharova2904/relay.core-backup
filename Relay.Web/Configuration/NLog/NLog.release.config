﻿<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      autoReload="true"
      throwExceptions="true">

  <variable name="logDirectory" value="${basedir}/Logs"/>

  <targets>
    <target name="main"
            xsi:type="File"
            layout="${longdate}${newline}${level:upperCase=true}${newline}${message}${newline}(${stacktrace})${newline}${exception:format=tostring}"
            fileName="${logDirectory}/${shortdate}/${shortdate}.log"
            keepFileOpen="true">
    </target>

    <target name="individual-files"
        xsi:type="File"
        layout="${longdate}${newline}${level:upperCase=true}${newline}${message}${newline}(${stacktrace})${newline}${exception:format=tostring}"
        fileName="${logDirectory}/${shortdate}/${logger}.log"
        keepFileOpen="true">
    </target>


    <target name="mail-logger"
            xsi:type="File"
            layout="${longdate} - ${message}"
            fileName="${logDirectory}/${shortdate}/${shortdate}.mail.log"
			keepFileOpen="true">
    </target>

    <target name="mailbuffer" xsi:type="BufferingWrapper" slidingTimeout="false" bufferSize="500" flushTimeout="-1">
      <target name="mail"
            xsi:type="Mail"
            useSystemNetMailSettings="true"
            subject="${machinename} - Logs (${shortdate:format=dd. MM. yyyy})"
            from="noreply@ihandover.co"
            to="anatoliy.malinovskiy@ihandover.co;sergey.pavlenko@ihandover.co;peter.glazunov@ihandover.co;andrey.zlenko@ihandover.co"
            layout="${longdate}${newline}${uppercase:${level}}${newline}MESSAGE=${message}${newline}EXCEPTION=${exception:format=tostring,StackTrace}">
      </target>
    </target>

    <target name="handover-logger"
			xsi:type="File"
			layout="${longdate} - ${message}"
			fileName="${logDirectory}/${shortdate}/${shortdate}.handover.log"
			keepFileOpen="true">
    </target>



    <target name="topic-logger"
      xsi:type="File"
      layout="${longdate} - ${message}"
      fileName="${logDirectory}/${shortdate}/${shortdate}.topic.log">
    </target>


    <target name="filter-data-logger"
      xsi:type="File"
      layout="${longdate}. 
      User Agent - ${aspnet-request-useragent}
      Ip - ${aspnet-Request-IP}. 
      User id - ${aspnet-User-Identity}.
      Url : ${aspnet-request:serverVariable=HTTP_URL}${aspnet-request:queryString}
      Form Data - ${message}"
      fileName="${logDirectory}/${shortdate}/${shortdate}.filter-data-logger.log">
    </target>

  </targets>


  <rules>
    <logger name="*" minlevel="Info" writeTo="main"/>
    <logger name="*" minlevel="Warn" writeTo="individual-files"/>
    <logger name="*" minlevel="Fatal" writeTo="mailbuffer"/>
    <logger name="mail-logger" minlevel="Info" writeTo="mail-logger" />
    <logger name="handover-logger" minlevel="Info" writeTo="handover-logger" />
    <logger name="topic-logger" minlevel="Info" writeTo="topic-logger" />
    <logger name="filter-data-logger" writeTo="filter-data-logger" />

  </rules>

</nlog>