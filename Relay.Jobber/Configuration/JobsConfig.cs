﻿using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;

namespace iHandover.Relay.Jobber.Configuration
{
    public class JobsConfig
    {
        #region Singleton

        /// <summary>
        /// Sinc object
        /// </summary>
        private static object syncRoot = new Object();

        /// <summary>
        /// Instance of JobsConfig
        /// </summary>
        private static volatile JobsConfig _jobsConfig;

        /// <summary>
        /// Return JobsConfig object
        /// </summary>
        /// <returns></returns>
        public static JobsConfig GetConfig()
        {
            if (_jobsConfig == null)
            {
                lock (syncRoot)
                {
                    if (_jobsConfig == null)
                    {
                        _jobsConfig = new JobsConfig(GetConfiguration().SelectSingleNode("//jobsConfig"));
                    }
                }
            }

            return _jobsConfig;
        }

        /// <summary>
        /// Find configuration file and extract xml
        /// </summary>
        /// <returns></returns>
        private static XmlNode GetConfiguration()
        {
            string configFolderPath = AppDomain.CurrentDomain.BaseDirectory + "Configuration";
            string configFilePath = Path.Combine(configFolderPath, "Task.config");

            if (File.Exists(configFilePath))
            {
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(configFilePath);

                return xDoc;
            }

            throw new FileNotFoundException("Task.config file was not found.");
        }

        #endregion

        /// <summary>
        /// All jobs configurations
        /// </summary>
        public IEnumerable<Job> Jobs { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="jobsNode">Jobs XmlNode from common configuration</param>
        private JobsConfig(XmlNode jobsNode)
        {
            this.Jobs = new List<Job>();

            if (jobsNode != null)
            {
                XmlNodeList jobNodes = jobsNode.SelectNodes("//job");

                foreach (XmlNode job in jobNodes)
                {
                    (this.Jobs as List<Job>).Add(new Job(job));
                }
            }
        }

    }
}
