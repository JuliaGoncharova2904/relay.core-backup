﻿using Hangfire;
using Hangfire.Storage;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.CompositionRoot.IOC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web.Configuration;
using NLog;
using iHandover.Relay.Jobber.Configuration;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.DataSource;

namespace iHandover.Relay.Jobber
{
    /// <summary>
    /// Jobs Manager
    /// </summary>
    public static class JobsRunner
    {

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Initialize all jobs in site.
        /// </summary>
        public static void InitializeJobs()
        {
            JobsConfig jobsConfig = JobsConfig.GetConfig();

            if (jobsConfig != null)
            {
                List<RecurringJobDto> recurringJobs = JobStorage.Current.GetConnection().GetRecurringJobs();

                recurringJobs.Where(rj => !jobsConfig.Jobs.Select(j => j.Id).Contains(rj.Id)).ToList().ForEach(rj => RecurringJob.RemoveIfExists(rj.Id));

                foreach (Job job in jobsConfig.Jobs)
                {
                    RecurringJob.RemoveIfExists(job.Id);
                    RecurringJob.AddOrUpdate(job.Id, () => Job(job.Assembly, job.Class, job.Method), job.Cron, TimeZoneInfo.Local);
                }
            }

        }

        /// <summary>
        /// Job executer.
        /// </summary>
        /// <param name="assembly">Assembly name</param>
        /// <param name="className">Class name</param>
        /// <param name="method">Method name (Job method)</param>
        [DisplayName("{2}")]
        public static void Job(string assembly, string className, string method)
        {
            try
            {
                using (RelayLightInject.JobContainer.BeginScope())
                {

                    IAuthBridge authBridge = (IAuthBridge)RelayLightInject.JobContainer.GetInstance(typeof(IAuthBridge));

                    foreach (string conStr in authBridge.GetAllRelayDbConnectionStrings())
                    {
                        Type jobType = Assembly.Load(assembly).GetType(className);

                        if (jobType != null)
                        {
                            INotificationAdapter notificationAdapter = (INotificationAdapter)RelayLightInject.JobContainer.GetInstance(typeof(INotificationAdapter));

                            IRepositoriesUnitOfWork repositoriesUnitOfWork = new RepositoriesUnitOfWork(new MomentumContext(conStr));
                            LogicCoreUnitOfWork logicCoreUnitOfWork = new LogicCoreUnitOfWork(repositoriesUnitOfWork, notificationAdapter, authBridge);


                            ConstructorInfo jobConstructor = jobType.GetConstructor(new[] { typeof(LogicCoreUnitOfWork) });
                            if (jobConstructor != null)
                            {
                                object jobObject = jobConstructor.Invoke(new object[] { logicCoreUnitOfWork });

                                MethodInfo jobMethod = jobType.GetMethod(method);
                                jobMethod?.Invoke(jobObject, new object[] { });
                            }
                            else
                            {
                               var isMultiTenant = bool.Parse(WebConfigurationManager.AppSettings["isMultiTenant"]);

                                if (isMultiTenant)
                                {
                                    MethodInfo jobMethod = jobType.GetMethod(method);

                                    ParameterInfo[] parameters = jobMethod.GetParameters();
                                    object classInstance = Activator.CreateInstance(jobType, null);
                                    if (parameters.Length == 0)
                                    {
                                        jobMethod.Invoke(classInstance, null);
                                    }
                                }                         
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Job Exception.");
            }
        }

    }
}
