﻿using System.Web;
using Hangfire.Annotations;
using Hangfire.Dashboard;
using MomentumPlus.Relay.Roles;

namespace iHandover.Relay.Jobber
{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            return HttpContext.Current.GetOwinContext().Authentication.User?.IsInRole(iHandoverRoles.Relay.iHandoverAdmin) ?? false;
        }
    }
}
