﻿using System;
using System.Linq;
using System.Collections.Generic;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Core.Models;

namespace iHandover.Relay.Jobber.Jobs
{
    public class RotationJobs : JobsBase
    {

        public RotationJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        /// <summary>
        /// Job => is send report to back to back when swing is end
        /// </summary>
        public void HandoverTriggerJob()
        {
            NotificationJobs notificationJobs = new NotificationJobs(_logicCore);

            IEnumerable<Rotation> notEndedRotations = _logicCore.RotationCore.GetRotationsByState(RotationState.Confirmed).Where(r => r.RotationType == RotationType.Swing).ToList();

            foreach (Rotation rotation in notEndedRotations)
            {
                DateTime currentDateTime = base.GetCurrentDateTime(rotation.RotationOwnerId);

                DateTime handoverTime = _logicCore.AdministrationCore.GetHandoverTriggerTime(rotation.RotationOwnerId);

                if (handoverTime <= currentDateTime)
                {
                    var rotationEndDate = rotation.StartDate.Value.AddDays(rotation.DayOn - 1);

                    _logicCore.RotationCore.CopyToRelayRotation(rotation);
                    _logicCore.MailerService.SendRotationShareReportEmail(rotation.Id);

                    if (rotationEndDate.Date <= currentDateTime.Date)
                    {
                        _logicCore.RotationCore.AutoFinalizeAll(rotation, false);
                        _logicCore.RotationCore.SetRotationState(rotation, RotationState.SwingEnded, false);
                        _logicCore.SyncWithDatabase();

                        _logicCore.RotationCore.CopyToRelayRotation(rotation);
                        _logicCore.MailerService.SendRotationShareReportEmail(rotation.Id);

                        _logicCore.RotationCore.HandoverReportFromRotation(rotation);

                        if (rotation.RotationType == RotationType.Shift)
                        {
                            _logicCore.RotationCore.EndAllRotationShifts(rotation);
                        }

                        notificationJobs.EndOfSwingNotificationSubJob(rotation);

                        _logicCore.SyncWithDatabase();
                    }

                }
            }
        }

        /// <summary>
        /// Job => expire rotations
        /// </summary>
        public void ExpireRotationsJob()
        {
            IEnumerable<Rotation> swingEndedRotations = _logicCore.RotationCore.GetRotationsByState(RotationState.SwingEnded).Where(r => r.RotationType == RotationType.Swing).ToList();

            foreach (Rotation rotation in swingEndedRotations)
            {
                DateTime currentDateTime = base.GetCurrentDateTime(rotation.RotationOwnerId).Date;

                if (rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff).Date <= currentDateTime)
                {
                    _logicCore.RotationCore.SetRotationState(rotation, RotationState.Expired, false);

                    _logicCore.NotificationCore.NotificationTrigger.Send_RecipientSendReport(rotation.DefaultBackToBack.Id,
                                                                                    rotation.RotationOwner.FullName,
                                                                                    rotation.RotationOwnerId, rotation.Id);

                    var contributorUsers = rotation.RotationOwner.Contributors;

                    if (contributorUsers != null && contributorUsers.Any())
                    {
                        foreach (var contributorUser in contributorUsers)
                        {
                            _logicCore.NotificationCore.NotificationTrigger.Send_RecipientSendReport(contributorUser.Id,
                                                                                        rotation.RotationOwner.FullName,
                                                                                        rotation.RotationOwnerId, rotation.Id);
                        }
                    }
                }
            }

            _logicCore.SyncWithDatabase();
        }

    }
}
