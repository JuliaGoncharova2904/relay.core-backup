﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Core.Models;

namespace iHandover.Relay.Jobber.Jobs
{
    public class MailJobs : JobsBase
    {
        public MailJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        {
        }

        /// <summary>
        /// Job => is send Handover Preview Mail to Default B2b of Rotation
        /// </summary>
        public void HandoverPreviewMailJob()
        {
            DateTime? handoverPreviewTime = _logicCore.AdministrationCore.GetHandoverPreviewTime();

            if (handoverPreviewTime.HasValue)
            {
                IEnumerable<Rotation> notEndedRotations = _logicCore.RotationCore.GetRotationsByState(RotationState.Confirmed);

                foreach (Rotation rotation in notEndedRotations)
                {
                    if (rotation.RotationType != RotationType.Shift)
                    {
                        DateTime currentDateTime = base.GetCurrentDateTime(rotation.RotationOwnerId).Date;

                        if (rotation.StartDate.Value.AddDays(rotation.DayOn - 2).Date == currentDateTime)
                        {
                            DateTime? handoverPreviewTimeByUser = _logicCore.AdministrationCore.GetHandoverPreviewTime(rotation.RotationOwnerId);

                            if (handoverPreviewTimeByUser.HasValue)
                            {
                                DateTime currentTime = base.GetCurrentDateTime(rotation.RotationOwnerId);

                                if (handoverPreviewTimeByUser.Value <= currentTime && handoverPreviewTimeByUser.Value.AddHours(1) > currentTime)
                                {
                                    _logicCore.RotationCore.CopyToRelayRotation(rotation);
                                    _logicCore.MailerService.SendHandoverPreviewEmail(rotation.Id);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Job => is send Shift Handover Preview Mail to Recipient
        /// </summary>
        public void HandoverShiftPreviewMailJob()
        {
            IEnumerable<Shift> notEndedShifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Confirmed).ToList();

            foreach (Shift shift in notEndedShifts)
            {
                DateTime currentDateTime = base.GetCurrentDateTime(shift.Rotation.RotationOwnerId).AddMinutes(60);

                if (shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes) <= currentDateTime &&
                    shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes + 6) > currentDateTime)
                {
                    _logicCore.RotationCore.CopyToRelayShift(shift);
                    _logicCore.MailerService.SendShiftHandoverPreviewEmail(shift.Id);
                }

            }
        }
    }
}
