﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Relay.Roles;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iHandover.Relay.Jobber.Jobs
{
    public class NotificationJobs : JobsBase
    {
        public NotificationJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        /// <summary>
        /// 
        /// </summary>
        public void PenultimateDayOfSwingJob()
        {
            IEnumerable<Rotation> rotations = _logicCore.RotationCore.GetRotationsByState(RotationState.Confirmed);

            foreach (Rotation rotation in rotations)
            {

                DateTime currentDateTime = base.GetCurrentDateTime(rotation.RotationOwnerId);

                if (currentDateTime.Date == rotation.StartDate.Value.AddDays(rotation.DayOn - 2).Date)
                {
                    Guid recipientId = rotation.RotationOwnerId;
                    //---- 1 ----
                    _logicCore.NotificationCore.NotificationTrigger.Send_PossibilityToExtendSwing(recipientId, rotation.RotationOwnerId);
                    //---- 2 ----
                    int notComplitedHandbackTasks = rotation.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Received)
                                                                            .SelectMany(rm => rm.RotationTopicGroups)
                                                                            .SelectMany(rtg => rtg.RotationTopics)
                                                                            .SelectMany(rt => rt.RotationTasks)
                                                                            .Count(rt => !rt.IsComplete && rt.IsFeedbackRequired);
                    if (notComplitedHandbackTasks > 0)
                    {
                        _logicCore.NotificationCore.NotificationTrigger.Send_HandbackTasksStillComplete(recipientId, notComplitedHandbackTasks, rotation.Id);
                    }
                    //---- 3 ----
                    int notComplitedTasks = rotation.RotationModules.Where(rm => rm.SourceType == TypeOfModuleSource.Received)
                                                                    .SelectMany(rm => rm.RotationTopicGroups)
                                                                    .SelectMany(rtg => rtg.RotationTopics)
                                                                    .SelectMany(rt => rt.RotationTasks)
                                                                    .Count(rt => !rt.IsComplete && !rt.IsFeedbackRequired);
                    if (notComplitedTasks > 0)
                    {
                        _logicCore.NotificationCore.NotificationTrigger.Send_TasksStillComplete(recipientId, notComplitedTasks, rotation.Id);
                    }
                }
        
            }
        }


        public void StatuOfTaskIncompleteAfterDueDateJob()
        {
            List<RotationTask> tasks = _logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.Received)
                                                                 .Where(t => !t.IsComplete && t.Deadline != null)
                                                                 .ToList();
            tasks.AddRange(_logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.MyTask)
                                                                .Where(t => !t.IsComplete && t.Deadline != null)
                                                                .ToList());

            foreach (RotationTask task in tasks)
            {
                DateTime today = DateTime.UtcNow;
                
                if(today > task.Deadline && task.FinalizeStatus == StatusOfFinalize.NotFinalized)
                {
                    UserProfile lineManager = null;
                    UserProfile userOwnerId = null;
                    if (task.AssignedTo.CurrentRotation == null)
                    {
                        lineManager = task.TaskBoard?.User;
                        userOwnerId = task.TaskBoard?.User;
                        //---- notification 22 ----
                        //   _logicCore.NotificationCore.NotificationTrigger.Send_StatuOfTaskIncompleteAfterDueDate(lineManager.Id, task.AssignedTo.FirstName, task.Deadline, task.AssignedToId.Value, task.Id);
                        if (!_logicCore.TaskBoardCore.IsHaveNotificationForTask(task.Id))
                        {
                            //---- notification 22 ----
                            _logicCore.NotificationCore.NotificationTrigger.Send_StatuOfTaskIncompleteAfterDueDate(lineManager.Id, task.AssignedTo.FirstName, task.Deadline, task.AssignedToId.Value, task.Id);
                        }
                    }
                    else
                    {
                        lineManager = task.AssignedTo.CurrentRotation.LineManager;
                        userOwnerId = task.AssignedTo.CurrentRotation.RotationOwner;
                        //---- notification 22 ----
                        //_logicCore.NotificationCore.NotificationTrigger.Send_StatuOfTaskIncompleteAfterDueDateForNowTask(lineManager.Id, task.AssignedTo.FirstName, task.Deadline, userOwnerId.Id, task.Id);
                        if (!_logicCore.TaskBoardCore.IsHaveNotificationForTask(task.Id))
                        {
                            //---- notification 22 ----
                            _logicCore.NotificationCore.NotificationTrigger.Send_StatuOfTaskIncompleteAfterDueDateForNowTask(lineManager.Id, task.AssignedTo.FirstName, task.Deadline, userOwnerId.Id, task.Id);
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public void EndOfSwingNotificationSubJob(Rotation rotation)
        {
            IEnumerable<RotationTask> assignedTasks = _logicCore.RotationModuleCore.GetRotationModules(rotation.Id, TypeOfModuleSource.Draft)
                                                                                            .Where(rm => rm.Enabled)
                                                                                            .SelectMany(rm => rm.RotationTopicGroups)
                                                                                            .SelectMany(rtg => rtg.RotationTopics)
                                                                                            .SelectMany(rt => rt.RotationTasks)
                                                                                            .Where(t => t.Enabled && t.AssignedToId.HasValue)
                                                                                            .ToList();

            IEnumerable<Guid> recipients = assignedTasks.Select(t => t.AssignedToId.Value).Distinct();

            foreach (Guid recipientId in recipients)
            {
                //---- 5 ----
                int assignedTasksNumber = assignedTasks.Count(t => t.AssignedToId == recipientId && !t.IsFeedbackRequired);
                if (assignedTasksNumber > 0)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_AssignedTasks(recipientId, assignedTasksNumber);
                }
                //---- 6 ----
                int assignedHandbackTasksNumber = assignedTasks.Count(t => t.AssignedToId == recipientId && t.IsFeedbackRequired);
                if (assignedHandbackTasksNumber > 0)
                {
                    _logicCore.NotificationCore.NotificationTrigger.Send_AssignedHandbackTasks(recipientId, assignedHandbackTasksNumber);
                }
            }


            _logicCore.NotificationCore.NotificationTrigger.Send_TeamMemberHandedOver(rotation.LineManagerId,
                                                                                        rotation.RotationOwner.FullName,
                                                                                        rotation.DefaultBackToBack.FullName,
                                                                                        rotation.RotationOwnerId);
        }

        /// <summary>
        /// 
        /// </summary>
        public void TaskBoardTaskIsNotCompletedJob()
        {
            List<RotationTask> tasks = _logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.Received)
                                                                .Where(t => !t.IsComplete && t.Deadline != null)
                                                                .ToList();
            tasks.AddRange(_logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.MyTask)
                                                                .Where(t => !t.IsComplete && t.Deadline != null)
                                                                .ToList());

            foreach (RotationTask task in tasks)
            {
                DateTime penultimateDayOfDedline = base.GetCurrentDateTime(_logicCore.RotationTaskCore.GetOwnerForTask(task).Id).Date.AddDays(1);

                if (penultimateDayOfDedline.Day == task.Deadline.Day)
                {
                    UserProfile recipient = task.TaskBoard?.User;
                    if (recipient == null) continue;

                    //---- notification 12 ----
                    _logicCore.NotificationCore.NotificationTrigger.Send_YouHaveNowTaskToComplete(recipient.Id, task.Id);
                    //---- notification 11 ----
                    for (RotationTask parentTask = task.AncestorTask; parentTask != null; parentTask = parentTask.AncestorTask)
                    {
                        _logicCore.NotificationCore.NotificationTrigger.Send_YourNowTaskHasNotCompleted(parentTask.TaskBoardId.Value, recipient.FullName, parentTask.Id);
                    }
                    //-------------------------
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void NowTaskIsNotCompletedJob()
        {
            IEnumerable<RotationTask> tasks = _logicCore.RotationTaskCore.GetActiveReceivedNowTasks()
                                                             .ToList()
                                                             .Where(t => !t.IsComplete);

            foreach (RotationTask task in tasks)
            {
                DateTime penultimateDayOfDedline = base.GetCurrentDateTime(_logicCore.RotationTaskCore.GetOwnerForTask(task).Id).Date.AddDays(1);

                if (task.Deadline.Day == penultimateDayOfDedline.Day)
                {
                    Guid ownerId = task.AncestorTask.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId;
                    Guid recipientId = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwnerId;
                    string recipientName = task.RotationTopic.RotationTopicGroup.RotationModule.Rotation.RotationOwner.FullName;
                    //---- notification 11 ----
                    _logicCore.NotificationCore.NotificationTrigger.Send_YourNowTaskHasNotCompleted(ownerId, recipientName, task.AncestorTaskId.Value);
                    //---- notification 12 ----
                    _logicCore.NotificationCore.NotificationTrigger.Send_YouHaveNowTaskToComplete(recipientId, task.Id);
                    //-------------------------
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public void ShiftOverSoonJob()
        {
            IEnumerable<Shift> shifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Confirmed)
                                                            .ToList()
                                                            .Where(s =>
                                                            {
                                                                DateTime endShiftTime = s.StartDateTime.Value.AddMinutes(s.WorkMinutes);

                                                                DateTime currentDateTime = base.GetCurrentDateTime(s.Rotation.RotationOwnerId);

                                                                return endShiftTime.AddMinutes(120) > currentDateTime && endShiftTime.AddMinutes(90) < currentDateTime;
                                                            });

            foreach (Shift shift in shifts)
            {
                _logicCore.NotificationCore.NotificationTrigger.Send_PossibilityToExtendShift(shift.Rotation.RotationOwnerId);
            }

        }


    }
}
