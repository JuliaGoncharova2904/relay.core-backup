﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iHandover.Relay.Jobber.Jobs
{
    public class ShiftJobs : JobsBase
    {
        public ShiftJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        /// <summary>
        /// Job => is send report to recipient when shift is end
        /// </summary>
        public void HandoverTriggerJob()
        {
            IEnumerable<Shift> notEndedShifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Confirmed).ToList();

            foreach (Shift shift in notEndedShifts)
            {
                DateTime currentDateTime = base.GetCurrentDateTime(shift.Rotation.RotationOwnerId);
               
                if (shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes + 60) <= currentDateTime)
                {
                    _logicCore.ShiftCore.AutoFinalizeAll(shift, false);
                    _logicCore.ShiftCore.SetShiftState(shift, ShiftState.Break, false);
                    _logicCore.ShiftCore.HandoverReportFromShift(shift);
                }
            }

            _logicCore.SyncWithDatabase();
        }


        /// <summary>
        /// Job => Expire shifts
        /// </summary>
        public void FinishShiftsJob()
        {
            IEnumerable<Shift> breakShifts = _logicCore.ShiftCore.GetShiftsByState(ShiftState.Break).ToList();

            foreach (Shift shift in breakShifts)
            {
                DateTime currentDateTime = base.GetCurrentDateTime(shift.Rotation.RotationOwnerId);

                if (shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes + shift.BreakMinutes) <= currentDateTime)
                {
                    _logicCore.ShiftCore.SetShiftState(shift, ShiftState.Finished, false);

                    _logicCore.NotificationCore.NotificationTrigger.Send_RecipientSendReport(shift.ShiftRecipient.Id,
                                                                                  shift.Rotation.RotationOwner.FullName,
                                                                                  shift.Rotation.RotationOwnerId, shift.Id);

                    var contributorUsers = shift.Rotation.RotationOwner.Contributors;

                    if (contributorUsers != null && contributorUsers.Any())
                    {
                        foreach (var contributorUser in contributorUsers)
                        {
                            _logicCore.NotificationCore.NotificationTrigger.Send_RecipientSendReport(contributorUser.Id,
                                                                                        shift.Rotation.RotationOwner.FullName,
                                                                                        shift.Rotation.RotationOwnerId, shift.Id);
                        }
                    }
                }
            }

            _logicCore.SyncWithDatabase();
        }

    }
}
