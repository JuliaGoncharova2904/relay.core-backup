﻿using System;
using MomentumPlus.Relay.BLL.LogicCore;

namespace iHandover.Relay.Jobber.Jobs
{
    public abstract class JobsBase
    {
        protected LogicCoreUnitOfWork _logicCore;

        protected JobsBase(LogicCoreUnitOfWork logicCore)
        {
            this._logicCore = logicCore;
        }

        protected DateTime GetCurrentDateTime()
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            DateTime currentTime = DateTime.Now;

            TimeZoneInfo timezoneFromSettings = TimeZoneInfo.FindSystemTimeZoneById(adminSettings.TimeZoneId);

            currentTime = TimeZoneInfo.ConvertTime(currentTime, timezoneFromSettings);

            return currentTime;
        }


        protected DateTime GetCurrentDateTime(Guid userId)
        {
            var adminSettings = _logicCore.AdministrationCore.GetAdminSettings();

            var user = _logicCore.UserProfileCore.GetUserProfile(userId);

            var workplace = user.Position.Workplace;

            DateTime currentTime = DateTime.Now;

            if (workplace.TimeZoneId != null)
            {
                TimeZoneInfo workplaceTimezone = TimeZoneInfo.FindSystemTimeZoneById(workplace.TimeZoneId);

                currentTime = TimeZoneInfo.ConvertTime(currentTime, workplaceTimezone);
            }
            else
            {
                TimeZoneInfo timezoneFromSettings = TimeZoneInfo.FindSystemTimeZoneById(adminSettings.TimeZoneId);

                currentTime = TimeZoneInfo.ConvertTime(currentTime, timezoneFromSettings);
            }

            return currentTime;
        }
    }
}
