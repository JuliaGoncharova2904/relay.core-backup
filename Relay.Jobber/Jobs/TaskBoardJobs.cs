﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.BLL.LogicCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace iHandover.Relay.Jobber.Jobs
{
    public class TaskBoardJobs : JobsBase
    {
        public TaskBoardJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        public void HandowerPendingTasksJob()
        {
            IEnumerable<RotationTask> pendingTasksForHandover = _logicCore.TaskBoardCore.GetTasksByType(TaskBoardTaskType.Pending).ToList();

            foreach (RotationTask task in pendingTasksForHandover)
            {
                UserProfile taskOwner = _logicCore.RotationTaskCore.GetOwnerForTask(task);

                DateTime currentDate = base.GetCurrentDateTime(taskOwner.Id).Date;

                if (task.DeferredHandoverTime <= currentDate)
                {
                    _logicCore.TaskBoardCore.HandoverTask(task, false);
                }
            }

            _logicCore.SyncWithDatabase();
        }
    }
}
