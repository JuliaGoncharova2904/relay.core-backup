﻿using System;
using System.Collections.Generic;
using System.Linq;
using MomentumPlus.Relay.BLL.LogicCore;
using MomentumPlus.Core.Models;

namespace iHandover.Relay.Jobber.Jobs
{
    /// <summary>
    /// Jobs for processing information of Projects.
    /// </summary>
    public class ProjectJobs : JobsBase
    {
        public ProjectJobs(LogicCoreUnitOfWork logicCore) : base(logicCore)
        { }

        private const int ProcessingDays = 3;

        /// <summary>
        /// Job => is finding finished projects and process of they information.
        /// </summary>
        public void EndProjectsJob()
        {
            DateTime currentDate = base.GetCurrentDateTime();

            List<Project> endedProjects = _logicCore.ProjectCore.GetAllProjets()
                                                        .Where(p => currentDate >= p.EndDate.Date.AddMonths(3) && p.EndDate.Date.AddMonths(3).AddDays(ProcessingDays) >= currentDate)
                                                        .ToList();

            foreach (Project project in endedProjects)
            {
                _logicCore.ProjectCore.DisableProjectModuleForFollowers(project, false);
            }

            _logicCore.SyncWithDatabase();
        }
    }
}
