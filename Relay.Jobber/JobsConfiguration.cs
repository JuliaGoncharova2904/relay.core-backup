﻿using Hangfire;
using Hangfire.SqlServer;
using Owin;
using System;
using Hangfire.Logging;
using Hangfire.Logging.LogProviders;
using LightInject;
using MomentumPlus.Relay.CompositionRoot.IOC;

namespace iHandover.Relay.Jobber
{
    public static class JobsConfiguration
    {
        public static void Configure(IAppBuilder app)
        {
            var options = new SqlServerStorageOptions
            {
                QueuePollInterval = TimeSpan.FromSeconds(15),
                JobExpirationCheckInterval = TimeSpan.FromDays(5),
                DashboardJobListLimit = 50000,
                TransactionTimeout = TimeSpan.FromMinutes(1)
            };

            GlobalConfiguration.Configuration.UseSqlServerStorage("JobsContext", options);
            //GlobalConfiguration.Configuration.UseLightInjectActivator((ServiceContainer)RelayLightInject.JobContainer);


            JobsRunner.InitializeJobs();

            LogProvider.SetCurrentLogProvider(new NLogLogProvider());

            app.UseHangfireServer();

            app.UseHangfireDashboard("/jobs", new DashboardOptions()
            {
                Authorization = new[] { new HangFireAuthorizationFilter() }
            });

        }
    }
}
