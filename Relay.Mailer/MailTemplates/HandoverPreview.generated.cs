﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ASP
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Mvc.Ajax;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using System.Web.Security;
    using System.Web.UI;
    using System.Web.WebPages;
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("RazorGenerator", "2.0.0.0")]
    [System.Web.WebPages.PageVirtualPathAttribute("~/MailTemplates/HandoverPreview.cshtml")]
    public partial class _MailTemplates_HandoverPreview_cshtml : System.Web.Mvc.WebViewPage<MomentumPlus.Relay.Mailer.Models.HandoverPreviewEmail>
    {
        public _MailTemplates_HandoverPreview_cshtml()
        {
        }
        public override void Execute()
        {
            
            #line 3 "..\..\MailTemplates\HandoverPreview.cshtml"
  
    var returnText = " on " + @Model.ReturnDate;

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\nTo: ");

            
            #line 7 "..\..\MailTemplates\HandoverPreview.cshtml"
Write(Model.To);

            
            #line default
            #line hidden
WriteLiteral("\r\nFrom: noreply@ihandover.co\r\nSubject: Handover report\r\n\r\nHi ");

            
            #line 11 "..\..\MailTemplates\HandoverPreview.cshtml"
Write(Model.RotationBackToBackName);

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\nAttached is a draft copy of the handover report being created by ");

            
            #line 13 "..\..\MailTemplates\HandoverPreview.cshtml"
                                                            Write(Model.RotationOwnerName);

            
            #line default
            #line hidden
WriteLiteral(" for your return to site ");

            
            #line 13 "..\..\MailTemplates\HandoverPreview.cshtml"
                                                                                                             Write(returnText);

            
            #line default
            #line hidden
WriteLiteral("\r\n\r\nWithin this report you will be able to get an idea of how operations have pro" +
"gressed since you left site.\r\n\r\nYou can also view the progress of tasks that you" +
" have assigned to your team members by clicking here. ");

            
            #line 17 "..\..\MailTemplates\HandoverPreview.cshtml"
                                                                                                 Write(Model.HistorySectionUrl);

            
            #line default
            #line hidden
WriteLiteral(".\r\n\r\n\r\nThanks\r\n\r\nRelay Team\r\n\r\n\r\n\r\n");

        }
    }
}
#pragma warning restore 1591
