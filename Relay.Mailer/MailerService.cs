﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Mvc;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Interfaces;
using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Mailer.Extensions;
using MomentumPlus.Relay.Mailer.Models;
using NLog;
using Postal;
using Attachment = System.Net.Mail.Attachment;

namespace MomentumPlus.Relay.Mailer
{
    public class MailerService : IMailerService
    {
        private readonly EmailService _emailService;
        private static readonly Logger Logger = LogManager.GetLogger("mail-logger");
        private readonly IRepositoriesUnitOfWork _repositoriesUnitOfWork;

        public MailerService(IRepositoriesUnitOfWork repositories)
        {
            var viewsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"bin\MailTemplates");

            var engines = new ViewEngineCollection { new FileSystemRazorViewEngine(viewsPath) };
            var emailService = new EmailService(engines);
            this._emailService = emailService;
            this._repositoriesUnitOfWork = repositories;
        }

        public void SendHandoverPreviewEmail(Guid rotationId)
        {
            Rotation rotation = _repositoriesUnitOfWork.RotationRepository.Get(rotationId);

            Rotation backRotation = rotation.DefaultBackToBack.CurrentRotation;

            string siteUrl = WebConfigurationManager.AppSettings["siteUrl"];

            string backToBackReturnDate = null;

            if (backRotation != null)
            {
                backToBackReturnDate = RotationExpiredDate(backRotation).FormatWithDayAndMonth();
            }

            var email = new HandoverPreviewEmail
            {
                To = rotation.DefaultBackToBack.Email,
                RotationOwnerName = rotation.RotationOwner.FirstName,
                RotationBackToBackName = rotation.DefaultBackToBack.FirstName,
                ReturnDate = backToBackReturnDate,
                SiteUrl = siteUrl,
                RotationId = rotationId,
                HistorySectionUrl = siteUrl != null ? siteUrl + $"/Reports/HandoverReport?sourceId={rotationId}&isHistoryPanel=true" : null,
                BackHaveRotation = backRotation != null
            };

            var reportUrl = $"{email.SiteUrl}/Reports/HandoverReport?sourceId={email.RotationId}&isPreviewMail=true";

            byte[] pdfBytes;
            using (var webClient = new WebClient())
            {
                pdfBytes = webClient.DownloadData(reportUrl);
            }

            Logger.Info("System send Handover Preview Email to {0}, email: {1} . Creator is {2}, rotationId: {3} . Url for report - {4}",
                email.RotationBackToBackName,
                email.To,
                email.RotationOwnerName,
                email.RotationId,
                reportUrl);


            email.Attach(new Attachment(new MemoryStream(pdfBytes), rotation.RotationOwner.FirstName + " Draft Handover Report.pdf"));

            try
            {
                _emailService.Send(email);
            }
            catch (Exception)
            {

            }
        }

        public void SendShiftHandoverPreviewEmail(Guid shiftId)
        {
            Shift shift = _repositoriesUnitOfWork.ShiftRepository.Get(shiftId);

            string siteUrl = WebConfigurationManager.AppSettings["siteUrl"];

            var shiftRecipient = shift.ShiftRecipient;

            if (shiftRecipient != null && shiftRecipient.CurrentRotation != null)
            {
                var shiftRecipientPreviousShift = shiftRecipient.CurrentRotation.RotationShifts.OrderBy(s => s.CreatedUtc.Value).LastOrDefault(s => s.StartDateTime.HasValue
                                                                                    && s.State == ShiftState.Finished
                                                                                    && s.CreatedUtc.Value < DateTime.Now);
                var email = new ShiftPreviewEmail
                {
                    To = shiftRecipient.Email,
                    ShiftOwnerName = shift.Rotation.RotationOwner.FirstName,
                    ShiftRecipientName = shiftRecipient.FirstName,
                    ShiftId = shift.Id,
                    SiteUrl = siteUrl,
                    ShiftEndTime = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).ToString("HH:mm"),
                    IsRecipientHasPreviousShift = shiftRecipientPreviousShift != null
                };

                var reportUrl = $"{email.SiteUrl}/Reports/HandoverReport?sourceId={email.ShiftId}&isShift=true&isPreviewMail=true";

                if (shiftRecipientPreviousShift != null)
                {
                    var shiftRecipientPreviousShiftUrl = $"{email.SiteUrl}/Summary#?DailyReportShiftId={shiftRecipientPreviousShift.Id}";

                    email.RecipientPreviousShiftUrl = shiftRecipientPreviousShiftUrl;
                }

                byte[] pdfBytes;
                using (var webClient = new WebClient())
                {
                    pdfBytes = webClient.DownloadData(reportUrl);
                }

                Logger.Info("System send Handover Shift Preview Email to {0}, email: {1} . Creator is {2}, shiftId: {3} . Url for report - {4}",
                    email.ShiftRecipientName,
                    email.To,
                    email.ShiftOwnerName,
                    email.ShiftId,
                    reportUrl);


                email.Attach(new Attachment(new MemoryStream(pdfBytes), shift.Rotation.RotationOwner.FirstName + " Shift Report.pdf"));

                try
                {
                    _emailService.Send(email);
                }
                catch (Exception)
                {
                    
                }
            
            }
        }

      
        public void SendShiftRecepintEmail(Shift shift, byte[] pdfBytes)
        {
            string siteUrl = WebConfigurationManager.AppSettings["siteUrl"];

            var shiftRecipient = shift.ShiftRecipient;

            if (shiftRecipient != null && shiftRecipient.CurrentRotation != null)
            {
                var shiftRecipientPreviousShift = shiftRecipient.CurrentRotation.RotationShifts.OrderBy(s => s.CreatedUtc.Value).LastOrDefault(s => s.StartDateTime.HasValue
                                                                                    && s.State == ShiftState.Finished
                                                                                    && s.CreatedUtc.Value < DateTime.Now);
                var email = new ShiftPreviewEmail
                {
                    To = shiftRecipient.Email,
                    ShiftOwnerName = shift.Rotation.RotationOwner.FirstName,
                    ShiftRecipientName = shiftRecipient.FirstName,
                    ShiftId = shift.Id,
                    SiteUrl = siteUrl,
                    ShiftEndTime = shift.StartDateTime.Value.AddMinutes(shift.WorkMinutes).ToString("HH:mm"),
                    IsRecipientHasPreviousShift = shiftRecipientPreviousShift != null
                };

                var reportUrl = $"{email.SiteUrl}/Reports/HandoverReport?sourceId={email.ShiftId}&isShift=true&isReceived=false&selectedSourceId=null";

                if (shiftRecipientPreviousShift != null)
                {
                    var shiftRecipientPreviousShiftUrl = $"{email.SiteUrl}/Summary#?DailyReportShiftId={shiftRecipientPreviousShift.Id}";

                    email.RecipientPreviousShiftUrl = shiftRecipientPreviousShiftUrl;
                }


                Logger.Info("System send Handover Shift Preview Email to {0}, email: {1} . Creator is {2}, shiftId: {3} . Url for report - {4}",
                    email.ShiftRecipientName,
                    email.To,
                    email.ShiftOwnerName,
                    email.ShiftId,
                    reportUrl);

                email.Attach(new Attachment(new MemoryStream(pdfBytes), shift.Rotation.RotationOwner.FullName + "Shift Report.pdf", "application/pdf"));

                try
                {
                    _emailService.Send(email);
                }
                catch (Exception)
                {

                }

            }
        }


        public void SendRecepientRotationReportEmail(Rotation rotation, byte[] pdfBytes)
        {
            UserProfile rotationOwner = rotation.RotationOwner;

            UserProfile rotationOwnerLineManager = rotation.LineManager;

            var reportSharingRecipientsIds = rotationOwner.SendedReports.Where(report => report.ReportId == rotation.Id
                                             && report.RecipientId != rotationOwnerLineManager.Id
                                             && report.RecipientId != rotation.DefaultBackToBackId).Select(user => user.RecipientId);

            var reportCCs = _repositoriesUnitOfWork.UserProfileRepository.Find(u => reportSharingRecipientsIds.Contains(u.Id)).ToList();

            reportCCs.Add(rotationOwnerLineManager);

            reportCCs.Add(rotationOwner);

            string reportCCsEmails = string.Join(",", reportCCs.Select(x => x.Email));

            string siteUrl = WebConfigurationManager.AppSettings["siteUrl"];

            var email = new RotationShareReport
            {
                To = rotation.DefaultBackToBack.Email,
                Subject = $"Finalised RelayWorks Report - {DateTime.UtcNow.FormatWithDayAndMonth()} - {rotation.RotationOwner.FirstName}",
                NameOfDrafter = rotation.RotationOwner.FirstName,
                ReportRecipientName = rotation.DefaultBackToBack.FirstName,
                HandoverStartDate = rotation.StartDate.FormatWithDayAndMonth(),
                HandoverEndDate = RotationSwingEndDate(rotation).FormatWithDayAndMonth(),
                DateOfHandover = DateTime.UtcNow.FormatWithDayAndMonth(),
                CC = reportCCsEmails,
                ReportUrl = siteUrl != null ? siteUrl + $"/Reports/GlobalReports/Report/{rotation.Id}/Draft?reportType=Swing" : null

            };

            Logger.Info("System send Rotation Share Report Email to {0}, email: {1} . Creator is {2}, rotationId: {3} . Url for share report - {4} . CC`s - {5}",
              email.ReportRecipientName,
              email.To,
              email.NameOfDrafter,
              rotation.Id,
              email.ReportUrl,
              email.CC);

            email.Attach(new Attachment(new MemoryStream(pdfBytes), rotation.RotationOwner.FullName + " Draft Handover Report.pdf"));

            try
            {
                _emailService.Send(email);
            }
            catch (Exception)
            {

            }
        }

        public void SendRotationShareReportEmail(Guid rotationId)
        {
            Rotation rotation = _repositoriesUnitOfWork.RotationRepository.Get(rotationId);

            UserProfile rotationOwner = rotation.RotationOwner;

            UserProfile rotationOwnerLineManager = rotation.LineManager;

            var reportSharingRecipientsIds = rotationOwner.SendedReports.Where(report => report.ReportId == rotationId
                                             && report.RecipientId != rotationOwnerLineManager.Id
                                             && report.RecipientId != rotation.DefaultBackToBackId).Select(user => user.RecipientId);

            var reportCCs = _repositoriesUnitOfWork.UserProfileRepository.Find(u => reportSharingRecipientsIds.Contains(u.Id)).ToList();

            reportCCs.Add(rotationOwnerLineManager);

            reportCCs.Add(rotationOwner);

            string reportCCsEmails = string.Join(",", reportCCs.Select(x => x.Email));

            string siteUrl = WebConfigurationManager.AppSettings["siteUrl"];

            var email = new RotationShareReport
            {
                To = rotation.DefaultBackToBack.Email,
                Subject = $"Finalised RelayWorks Report - {DateTime.UtcNow.FormatWithDayAndMonth()} - {rotation.RotationOwner.FirstName}",
                NameOfDrafter = rotation.RotationOwner.FirstName,
                ReportRecipientName = rotation.DefaultBackToBack.FirstName,
                HandoverStartDate = rotation.StartDate.FormatWithDayAndMonth(),
                HandoverEndDate = RotationSwingEndDate(rotation).FormatWithDayAndMonth(),
                DateOfHandover = DateTime.UtcNow.FormatWithDayAndMonth(),
                CC = reportCCsEmails,
                ReportUrl = siteUrl != null ? siteUrl + $"/Reports/GlobalReports/Report/{rotation.Id}/Draft?reportType=Swing" : null

            };

            Logger.Info("System send Rotation Share Report Email to {0}, email: {1} . Creator is {2}, rotationId: {3} . Url for share report - {4} . CC`s - {5}",
              email.ReportRecipientName,
              email.To,
              email.NameOfDrafter,
              rotationId,
              email.ReportUrl,
              email.CC);

            //var userSubscriptionId = _authBridge.AuthService.GetUserSubscriptionId(rotationOwner.Id);

            //AppSession.Current.CurrentSubscriptionId = userSubscriptionId;

            //if (userSubscriptionId.HasValue)
            //{
            //    var reportPdfUrl = $"{siteUrl}/RotationReportBuilder/ReportPrint/?rotationId={rotation.Id}&userSubscriptionId={userSubscriptionId.Value}";

            //    byte[] pdfBytes;

            //    using (var webClient = new WebClient())
            //    {
            //        pdfBytes = webClient.DownloadData(reportPdfUrl);
            //    }

            //    email.Attach(new Attachment(new MemoryStream(pdfBytes), rotation.RotationOwner.FirstName + " Draft Handover Report.pdf"));
            //}

            var reportUrl = $"{siteUrl}/Reports/HandoverReport?sourceId={rotationId}&isHistoryPanel=true&isPreviewMail=true";

            byte[] pdfBytes;
            using (var webClient = new WebClient())
            {
                pdfBytes = webClient.DownloadData(reportUrl);
            }

            email.Attach(new Attachment(new MemoryStream(pdfBytes), rotation.RotationOwner.FirstName + " Draft Handover Report.pdf"));

            try
            {
                _emailService.Send(email);
            }
            catch (Exception)
            {

            }
        }

        ///////////////////////////////////////////
        public DateTime RotationExpiredDate(Rotation rotation)
        {
            if (rotation.StartDate.HasValue)
            {
                DateTime expireDate = rotation.StartDate.Value.AddDays(rotation.DayOn + rotation.DayOff);

                return expireDate;
            }

            return rotation.CreatedUtc.Value.AddDays(rotation.DayOn + rotation.DayOff);
        }

        public DateTime RotationSwingEndDate(Rotation rotation)
        {
            if (rotation.StartDate.HasValue)
            {
                DateTime expireDate = rotation.StartDate.Value.AddDays(rotation.DayOn - 1);

                return expireDate;
            }

            return rotation.CreatedUtc.Value.AddDays(rotation.DayOn - 1);
        }
    }
}