﻿using Postal;

namespace MomentumPlus.Relay.Mailer.Models
{
    public class RotationShareReport : Email
    {
        public string To { get; set; }

        public string Subject { get; set; }

        public string CC { get; set; }

        public string DateOfHandover { get; set; }

        public string NameOfDrafter { get; set; }

        public string ReportRecipientName { get; set; }

        public string HandoverStartDate { get; set; }

        public string HandoverEndDate { get; set; }

        public string ReportUrl { get; set; }
    }
}
