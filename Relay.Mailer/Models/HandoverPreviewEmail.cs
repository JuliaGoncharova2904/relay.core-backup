﻿using System;
using Postal;

namespace MomentumPlus.Relay.Mailer.Models
{
    public class HandoverPreviewEmail : Email
    {
        public string To { get; set; }

        public string RotationOwnerName { get; set; }

        public string RotationBackToBackName { get; set; }

        public string SiteUrl { get; set; }

        public string HistorySectionUrl { get; set; }

        public string ReturnDate { get; set; }

        public bool BackHaveRotation { get; set; }

        public Guid RotationId { get; set; }
    }
}
