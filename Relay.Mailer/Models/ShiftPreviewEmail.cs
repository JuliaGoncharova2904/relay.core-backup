﻿using System;
using Postal;

namespace MomentumPlus.Relay.Mailer.Models
{
    public class ShiftPreviewEmail : Email
    {
        public string To { get; set; }

        public string ShiftOwnerName { get; set; }

        public string ShiftRecipientName { get; set; }

        public string ShiftEndTime { get; set; }

        public bool IsRecipientHasPreviousShift { get; set; }

        public string RecipientPreviousShiftUrl { get; set; }

        public string SiteUrl { get; set; }

        public Guid ShiftId { get; set; }
    }
}
