namespace MomentumPlus.Core.DataSource.DbInit
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdminSettings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PurchaseDate = c.DateTime(),
                        LicenceEndDate = c.DateTime(),
                        DomainUrl = c.String(),
                        TimeZoneId = c.String(),
                        TemplateLimit = c.Int(),
                        HandoverLimit = c.Int(),
                        AccountManager = c.String(),
                        Version = c.String(),
                        HostingProvider = c.String(),
                        FileExtensions = c.String(),
                        AllowedFileExtensions = c.String(),
                        PlanType = c.Int(nullable: false),
                        SupportType = c.Int(nullable: false),
                        HandoverTriggerType = c.Int(nullable: false),
                        HandoverTriggerTime = c.String(),
                        HandoverPreviewType = c.Int(nullable: false),
                        HandoverPreviewTime = c.String(),
                        LogoId = c.Guid(),
                        TwilioAccount = c.String(),
                        TwilioEndpoint = c.String(),
                        TwilioPhoneNumber = c.String(),
                        SMSGateway = c.String(),
                        NumberSMSPerUserPerDay = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.LogoId)
                .Index(t => t.LogoId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BinaryData = c.Binary(nullable: false),
                        FileType = c.String(),
                        ContentType = c.String(),
                        Title = c.String(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        FileName = c.String(),
                        Link = c.String(),
                        FileId = c.Guid(nullable: false),
                        RotationTaskId = c.Guid(),
                        RotationTopicId = c.Guid(),
                        IsReceivedAttachment = c.Boolean(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.FileId)
                .ForeignKey("dbo.RotationTasks", t => t.RotationTaskId, cascadeDelete: true)
                .ForeignKey("dbo.RotationTopics", t => t.RotationTopicId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RotationTaskId)
                .Index(t => t.RotationTopicId);
            
            CreateTable(
                "dbo.RotationTasks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        SearchTags = c.String(),
                        IsReceivedTask = c.Boolean(),
                        Priority = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Deadline = c.DateTime(nullable: false),
                        CompleteDateUtc = c.DateTime(),
                        IsComplete = c.Boolean(nullable: false),
                        FinalizeStatus = c.Int(nullable: false),
                        IsNullReport = c.Boolean(nullable: false),
                        IsFeedbackRequired = c.Boolean(nullable: false),
                        IsHaveNotificationTask = c.Boolean(nullable: false),
                        IsPinned = c.Boolean(),
                        ManagerComments = c.String(),
                        Feedback = c.String(),
                        Enabled = c.Boolean(nullable: false),
                        AssignedToId = c.Guid(),
                        CreatorId = c.Guid(),
                        RelationId = c.Guid(),
                        RotationTopicId = c.Guid(),
                        TemplateTaskId = c.Guid(),
                        AncestorTaskId = c.Guid(),
                        SuccessorTaskId = c.Guid(),
                        ParentTaskId = c.Guid(),
                        ForkCounter = c.Int(nullable: false),
                        ForkParentTaskId = c.Guid(),
                        TaskBoardId = c.Guid(),
                        TaskBoardTaskType = c.Int(nullable: false),
                        DeferredHandoverTime = c.DateTime(),
                        IsInArchive = c.Boolean(nullable: false),
                        ProjectId = c.Guid(),
                        ParentRotationTaskId = c.Guid(),
                        CopyTimesCounter = c.Int(nullable: false),
                        AddToReport = c.Boolean(nullable: false),
                        HandoverId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTasks", t => t.AncestorTaskId)
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .ForeignKey("dbo.UserProfiles", t => t.AssignedToId)
                .ForeignKey("dbo.RotationTasks", t => t.ForkParentTaskId)
                .ForeignKey("dbo.RotationTasks", t => t.ParentRotationTaskId)
                .ForeignKey("dbo.RotationTasks", t => t.ParentTaskId)
                .ForeignKey("dbo.RotationTopics", t => t.RotationTopicId)
                .ForeignKey("dbo.RotationTasks", t => t.SuccessorTaskId)
                .ForeignKey("dbo.TaskBoards", t => t.TaskBoardId)
                .ForeignKey("dbo.TemplateTasks", t => t.TemplateTaskId)
                .Index(t => t.AssignedToId)
                .Index(t => t.RotationTopicId)
                .Index(t => t.TemplateTaskId)
                .Index(t => t.AncestorTaskId)
                .Index(t => t.SuccessorTaskId)
                .Index(t => t.ParentTaskId)
                .Index(t => t.ForkParentTaskId)
                .Index(t => t.TaskBoardId)
                .Index(t => t.ProjectId)
                .Index(t => t.ParentRotationTaskId);
            
            CreateTable(
                "dbo.UserProfiles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        AvatarId = c.Guid(),
                        PositionId = c.Guid(nullable: false),
                        RotationPatternId = c.Guid(nullable: false),
                        ShiftPatternType = c.String(),
                        TeamId = c.Guid(nullable: false),
                        CompanyId = c.Guid(nullable: false),
                        CurrentRotationId = c.Guid(),
                        PasswordResetGuid = c.Guid(),
                        Email = c.String(),
                        SecondaryEmail = c.String(),
                        LastLoginDate = c.DateTime(),
                        LastPasswordChangedDate = c.DateTime(),
                        LastPasswordFailureDate = c.DateTime(),
                        LockDate = c.DateTime(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.AvatarId, cascadeDelete: true)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: true)
                .ForeignKey("dbo.Rotations", t => t.CurrentRotationId)
                .ForeignKey("dbo.Positions", t => t.PositionId)
                .ForeignKey("dbo.RotationPatterns", t => t.RotationPatternId, cascadeDelete: true)
                .ForeignKey("dbo.Teams", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.AvatarId)
                .Index(t => t.PositionId)
                .Index(t => t.RotationPatternId)
                .Index(t => t.TeamId)
                .Index(t => t.CompanyId)
                .Index(t => t.CurrentRotationId);
            
            CreateTable(
                "dbo.SafetyV2CriticalControls",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        OwnerId = c.Guid(),
                        MajorHazardId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SafetyV2MajorHazards", t => t.MajorHazardId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.OwnerId)
                .Index(t => t.OwnerId)
                .Index(t => t.MajorHazardId);
            
            CreateTable(
                "dbo.SafetyV2MajorHazards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        IconPath = c.String(),
                        OwnerId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.OwnerId)
                .Index(t => t.OwnerId);
            
            CreateTable(
                "dbo.SafetyV2Messages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Message = c.String(),
                        CriticalControlId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SafetyV2CriticalControls", t => t.CriticalControlId, cascadeDelete: true)
                .Index(t => t.CriticalControlId);
            
            CreateTable(
                "dbo.SafetyV2Stats",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SafetyMessageId = c.Guid(nullable: false),
                        RecipientId = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.RecipientId)
                .ForeignKey("dbo.SafetyV2Messages", t => t.SafetyMessageId, cascadeDelete: true)
                .Index(t => t.SafetyMessageId)
                .Index(t => t.RecipientId);
            
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Notes = c.String(),
                        IsDefault = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Rotations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RotationOwnerId = c.Guid(nullable: false),
                        LineManagerId = c.Guid(nullable: false),
                        DefaultBackToBackId = c.Guid(nullable: false),
                        StartDate = c.DateTime(),
                        ConfirmDate = c.DateTime(),
                        DayOn = c.Int(nullable: false),
                        DayOff = c.Int(nullable: false),
                        RepeatTimes = c.Int(nullable: false),
                        State = c.Int(nullable: false),
                        RotationType = c.Int(nullable: false),
                        PrevRotationId = c.Guid(),
                        NextRotationId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.DefaultBackToBackId)
                .ForeignKey("dbo.UserProfiles", t => t.LineManagerId)
                .ForeignKey("dbo.Rotations", t => t.NextRotationId)
                .ForeignKey("dbo.Rotations", t => t.PrevRotationId)
                .ForeignKey("dbo.UserProfiles", t => t.RotationOwnerId, cascadeDelete: true)
                .Index(t => t.RotationOwnerId)
                .Index(t => t.LineManagerId)
                .Index(t => t.DefaultBackToBackId)
                .Index(t => t.PrevRotationId)
                .Index(t => t.NextRotationId);
            
            CreateTable(
                "dbo.RotationModules",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        SourceType = c.Int(nullable: false),
                        TempateModuleId = c.Guid(),
                        RotationId = c.Guid(),
                        ShiftId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rotations", t => t.RotationId)
                .ForeignKey("dbo.Shifts", t => t.ShiftId)
                .ForeignKey("dbo.TemplateModules", t => t.TempateModuleId)
                .Index(t => t.TempateModuleId)
                .Index(t => t.RotationId)
                .Index(t => t.ShiftId);
            
            CreateTable(
                "dbo.RotationTopicGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        SearchTags = c.String(),
                        RelationId = c.Guid(),
                        RotationModuleId = c.Guid(nullable: false),
                        TempateTopicGroupId = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        ParentRotationTopicGroupId = c.Guid(),
                        SearchText = c.String(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTopicGroups", t => t.ParentRotationTopicGroupId)
                .ForeignKey("dbo.RotationModules", t => t.RotationModuleId, cascadeDelete: true)
                .ForeignKey("dbo.TemplateTopicGroups", t => t.TempateTopicGroupId)
                .Index(t => t.RotationModuleId)
                .Index(t => t.TempateTopicGroupId)
                .Index(t => t.ParentRotationTopicGroupId);
            
            CreateTable(
                "dbo.RotationTopics",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Variance = c.String(),
                        Planned = c.String(),
                        Actual = c.String(),
                        UnitsTypeId = c.Guid(),
                        UnitsSelectedType = c.String(),
                        IsExpandPlannedActualField = c.Boolean(),
                        IsFirstTopicByTopicGroupName = c.Boolean(),
                        CreatorManagerCommentsId = c.Guid(),
                        DateTimeCreatedManagerComments = c.DateTime(),
                        IsCanRemoveManagerComments = c.Boolean(),
                        IsLineManager = c.Boolean(),
                        FullNameLineManager = c.String(),
                        IsEditManagerComments = c.Boolean(),
                        HasTags = c.Boolean(),
                        PlannedActualHas = c.Boolean(),
                        FirstTagTopic = c.String(),
                        SearchText = c.String(),
                        MoreTags = c.Boolean(),
                        IsPinned = c.Boolean(),
                        IsNullReport = c.Boolean(nullable: false),
                        IsFeedbackRequired = c.Boolean(nullable: false),
                        Description = c.String(),
                        SearchTags = c.String(),
                        ManagerComments = c.String(),
                        Feedback = c.String(),
                        Enabled = c.Boolean(nullable: false),
                        FinalizeStatus = c.Int(nullable: false),
                        AssignedToId = c.Guid(),
                        CreatorId = c.Guid(),
                        LocationId = c.Guid(),
                        RelationId = c.Guid(),
                        RotationTopicGroupId = c.Guid(nullable: false),
                        TempateTopicId = c.Guid(),
                        AncestorTopicId = c.Guid(),
                        SuccessorTopicId = c.Guid(),
                        ParentTopicId = c.Guid(),
                        ForkCounter = c.Int(nullable: false),
                        ForkParentTopicId = c.Guid(),
                        SharedRecipientId = c.Guid(),
                        ShareSourceTopicId = c.Guid(),
                        ParentRotationTopicId = c.Guid(),
                        CopyTimesCounter = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTopics", t => t.AncestorTopicId)
                .ForeignKey("dbo.UserProfiles", t => t.AssignedToId)
                .ForeignKey("dbo.RotationTopics", t => t.ForkParentTopicId)
                .ForeignKey("dbo.Locations", t => t.LocationId)
                .ForeignKey("dbo.RotationTopics", t => t.ParentRotationTopicId)
                .ForeignKey("dbo.RotationTopics", t => t.ParentTopicId)
                .ForeignKey("dbo.RotationTopicGroups", t => t.RotationTopicGroupId, cascadeDelete: true)
                .ForeignKey("dbo.RotationTopics", t => t.ShareSourceTopicId)
                .ForeignKey("dbo.RotationTopics", t => t.SuccessorTopicId)
                .ForeignKey("dbo.TemplateTopics", t => t.TempateTopicId)
                .Index(t => t.AssignedToId)
                .Index(t => t.LocationId)
                .Index(t => t.RotationTopicGroupId)
                .Index(t => t.TempateTopicId)
                .Index(t => t.AncestorTopicId)
                .Index(t => t.SuccessorTopicId)
                .Index(t => t.ParentTopicId)
                .Index(t => t.ForkParentTopicId)
                .Index(t => t.ShareSourceTopicId)
                .Index(t => t.ParentRotationTopicId);
            
            CreateTable(
                "dbo.AttachmentsLinks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Link = c.String(),
                        RotationTaskId = c.Guid(),
                        RotationTopicId = c.Guid(),
                        IsReceivedAttachment = c.Boolean(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTasks", t => t.RotationTaskId, cascadeDelete: true)
                .ForeignKey("dbo.RotationTopics", t => t.RotationTopicId, cascadeDelete: true)
                .Index(t => t.RotationTaskId)
                .Index(t => t.RotationTopicId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        Address = c.String(),
                        Description = c.String(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RotationTopicLogs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TopicId = c.Guid(),
                        LogMessage = c.String(),
                        LogDate = c.DateTime(nullable: false),
                        RootTopicId = c.Guid(nullable: false),
                        UserId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTopics", t => t.RootTopicId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.UserId)
                .Index(t => t.RootTopicId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ManagerComments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Notes = c.String(nullable: false, maxLength: 300),
                        CreatorManagerCommentsId = c.Guid(),
                        DateTimeCreatedManagerComments = c.DateTime(),
                        IsCanRemoveManagerComments = c.Boolean(),
                        IsEditManagerComments = c.Boolean(),
                        IsLinemanager = c.Boolean(),
                        FullNameLineManager = c.String(),
                        RotationTopicId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTopics", t => t.RotationTopicId, cascadeDelete: true)
                .Index(t => t.RotationTopicId);
            
            CreateTable(
                "dbo.TemplateTopics",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 150),
                        Description = c.String(),
                        TopicGroupId = c.Guid(nullable: false),
                        ParentTopicId = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        PlannedActualHas = c.Boolean(),
                        UnitsSelectedType = c.String(),
                        IsExpandPlannedActualField = c.Boolean(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TemplateTopics", t => t.ParentTopicId)
                .ForeignKey("dbo.TemplateTopicGroups", t => t.TopicGroupId, cascadeDelete: true)
                .Index(t => t.TopicGroupId)
                .Index(t => t.ParentTopicId);
            
            CreateTable(
                "dbo.TemplateTasks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        Priority = c.Int(nullable: false),
                        TopicId = c.Guid(nullable: false),
                        ParentTaskId = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        IsNullReport = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TemplateTasks", t => t.ParentTaskId)
                .ForeignKey("dbo.TemplateTopics", t => t.TopicId, cascadeDelete: true)
                .Index(t => t.TopicId)
                .Index(t => t.ParentTaskId);
            
            CreateTable(
                "dbo.TemplateTopicGroups",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        IsNullReport = c.Boolean(nullable: false),
                        ModuleId = c.Guid(nullable: false),
                        RelationId = c.Guid(),
                        ParentTopicGroupId = c.Guid(),
                        Enabled = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TemplateModules", t => t.ModuleId, cascadeDelete: true)
                .ForeignKey("dbo.TemplateTopicGroups", t => t.ParentTopicGroupId)
                .Index(t => t.ModuleId)
                .Index(t => t.ParentTopicGroupId);
            
            CreateTable(
                "dbo.TemplateModules",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        TemplateId = c.Guid(),
                        ParentModuleId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TemplateModules", t => t.ParentModuleId)
                .ForeignKey("dbo.Templates", t => t.TemplateId, cascadeDelete: true)
                .Index(t => t.TemplateId)
                .Index(t => t.ParentModuleId);
            
            CreateTable(
                "dbo.Templates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Positions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        PositionId = c.Guid(),
                        CustomPositionName = c.String(maxLength: 50),
                        WorkplaceId = c.Guid(nullable: false),
                        TemplateId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Templates", t => t.TemplateId, cascadeDelete: true)
                .ForeignKey("dbo.Workplaces", t => t.WorkplaceId, cascadeDelete: true)
                .Index(t => t.WorkplaceId)
                .Index(t => t.TemplateId);
            
            CreateTable(
                "dbo.Workplaces",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                        Address = c.String(maxLength: 750),
                        MapLink = c.String(maxLength: 2000),
                        TimeZoneId = c.String(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RotationTopicSharingRelations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SourceTopicId = c.Guid(nullable: false),
                        DestinationTopicId = c.Guid(),
                        RecipientId = c.Guid(),
                        RecipientEmail = c.String(),
                        IsRecipientsFromPrevRotation = c.Boolean(),
                        TemplateTopicId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTopics", t => t.DestinationTopicId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.RecipientId)
                .ForeignKey("dbo.RotationTopics", t => t.SourceTopicId)
                .ForeignKey("dbo.TemplateTopics", t => t.TemplateTopicId)
                .Index(t => t.SourceTopicId)
                .Index(t => t.DestinationTopicId)
                .Index(t => t.RecipientId)
                .Index(t => t.TemplateTopicId);
            
            CreateTable(
                "dbo.VoiceMessages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        FileId = c.Guid(nullable: false),
                        RotationTaskId = c.Guid(),
                        RotationTopicId = c.Guid(),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.FileId)
                .ForeignKey("dbo.RotationTasks", t => t.RotationTaskId, cascadeDelete: true)
                .ForeignKey("dbo.RotationTopics", t => t.RotationTopicId, cascadeDelete: true)
                .Index(t => t.FileId)
                .Index(t => t.RotationTaskId)
                .Index(t => t.RotationTopicId);
            
            CreateTable(
                "dbo.Shifts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StartDateTime = c.DateTime(),
                        WorkMinutes = c.Int(nullable: false),
                        BreakMinutes = c.Int(nullable: false),
                        RepeatTimes = c.Int(nullable: false),
                        ShiftRecipientId = c.Guid(),
                        RotationId = c.Guid(nullable: false),
                        State = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rotations", t => t.RotationId, cascadeDelete: true)
                .ForeignKey("dbo.UserProfiles", t => t.ShiftRecipientId)
                .Index(t => t.ShiftRecipientId)
                .Index(t => t.RotationId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        CreatorId = c.Guid(nullable: false),
                        ProjectManagerId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.CreatorId)
                .ForeignKey("dbo.UserProfiles", t => t.ProjectManagerId)
                .Index(t => t.CreatorId)
                .Index(t => t.ProjectManagerId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        IconPath = c.String(),
                        HtmlMessage = c.String(),
                        Timestamp = c.DateTime(nullable: false),
                        RecipientId = c.Guid(nullable: false),
                        IsShown = c.Boolean(nullable: false),
                        IsOpened = c.Boolean(nullable: false),
                        ServiceData = c.String(),
                        RelationId = c.Guid(),
                        PermissionsBasic = c.Boolean(nullable: false),
                        PermissionsUltimate = c.Boolean(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.RecipientId, cascadeDelete: true)
                .Index(t => t.RecipientId);
            
            CreateTable(
                "dbo.RotationReportSharingRelations",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ReportId = c.Guid(nullable: false),
                        ReportType = c.Int(nullable: false),
                        RecipientId = c.Guid(nullable: false),
                        ReportOwnerId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.RecipientId)
                .ForeignKey("dbo.UserProfiles", t => t.ReportOwnerId)
                .Index(t => t.RecipientId)
                .Index(t => t.ReportOwnerId);
            
            CreateTable(
                "dbo.RotationPatterns",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DayOn = c.Int(nullable: false),
                        DayOff = c.Int(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TaskBoards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserProfiles", t => t.Id, cascadeDelete: true)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SafetyCriticalControls",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        MajorHazardId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SafetyMajorHazards", t => t.MajorHazardId, cascadeDelete: true)
                .Index(t => t.MajorHazardId);
            
            CreateTable(
                "dbo.SafetyMajorHazards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        IconId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SafetyBankOfIcons", t => t.IconId)
                .Index(t => t.IconId);
            
            CreateTable(
                "dbo.SafetyBankOfIcons",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        ImageId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Files", t => t.ImageId)
                .Index(t => t.ImageId);
            
            CreateTable(
                "dbo.SafetyMessages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Message = c.String(),
                        OpenCounter = c.Int(nullable: false),
                        DayUseCounter = c.Int(nullable: false),
                        CriticalControlId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SafetyCriticalControls", t => t.CriticalControlId, cascadeDelete: true)
                .Index(t => t.CriticalControlId);
            
            CreateTable(
                "dbo.RotationTaskLogs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        TaskId = c.Guid(),
                        LogMessage = c.String(),
                        LogDate = c.DateTime(nullable: false),
                        RootTaskId = c.Guid(nullable: false),
                        CreatedUtc = c.DateTime(),
                        ModifiedUtc = c.DateTime(),
                        DeletedUtc = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.RotationTasks", t => t.RootTaskId, cascadeDelete: true)
                .Index(t => t.RootTaskId);
            
            CreateTable(
                "dbo.SafetyV2CriticalControlChampions",
                c => new
                    {
                        CriticalControlId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CriticalControlId, t.UserId })
                .ForeignKey("dbo.SafetyV2CriticalControls", t => t.CriticalControlId)
                .ForeignKey("dbo.UserProfiles", t => t.UserId)
                .Index(t => t.CriticalControlId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.RotationContributors",
                c => new
                    {
                        RotationId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RotationId, t.UserId })
                .ForeignKey("dbo.Rotations", t => t.RotationId)
                .ForeignKey("dbo.UserProfiles", t => t.UserId)
                .Index(t => t.RotationId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.RotationsRelations",
                c => new
                    {
                        RotationId = c.Guid(nullable: false),
                        RelatedRotationId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.RotationId, t.RelatedRotationId })
                .ForeignKey("dbo.Rotations", t => t.RotationId)
                .ForeignKey("dbo.Rotations", t => t.RelatedRotationId)
                .Index(t => t.RotationId)
                .Index(t => t.RelatedRotationId);
            
            CreateTable(
                "dbo.ShiftsRelations",
                c => new
                    {
                        ShiftId = c.Guid(nullable: false),
                        RelatedShiftId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ShiftId, t.RelatedShiftId })
                .ForeignKey("dbo.Shifts", t => t.ShiftId)
                .ForeignKey("dbo.Shifts", t => t.RelatedShiftId)
                .Index(t => t.ShiftId)
                .Index(t => t.RelatedShiftId);
            
            CreateTable(
                "dbo.UserContributors",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        ContributorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.ContributorId })
                .ForeignKey("dbo.UserProfiles", t => t.UserId)
                .ForeignKey("dbo.UserProfiles", t => t.ContributorId)
                .Index(t => t.UserId)
                .Index(t => t.ContributorId);
            
            CreateTable(
                "dbo.ProjectFollowers",
                c => new
                    {
                        ProjectId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProjectId, t.UserId })
                .ForeignKey("dbo.Projects", t => t.ProjectId)
                .ForeignKey("dbo.UserProfiles", t => t.UserId)
                .Index(t => t.ProjectId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SafetyMessageTeamRelations",
                c => new
                    {
                        SafetyMessageId = c.Guid(nullable: false),
                        TeamId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.SafetyMessageId, t.TeamId })
                .ForeignKey("dbo.SafetyMessages", t => t.SafetyMessageId)
                .ForeignKey("dbo.Teams", t => t.TeamId)
                .Index(t => t.SafetyMessageId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "dbo.SafetyMessageUserRelations",
                c => new
                    {
                        SafetyMessageId = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.SafetyMessageId, t.UserId })
                .ForeignKey("dbo.SafetyMessages", t => t.SafetyMessageId)
                .ForeignKey("dbo.UserProfiles", t => t.UserId)
                .Index(t => t.SafetyMessageId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SafetyCriticalControlTeamRelations",
                c => new
                    {
                        CriticalControlId = c.Guid(nullable: false),
                        TeamId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.CriticalControlId, t.TeamId })
                .ForeignKey("dbo.SafetyCriticalControls", t => t.CriticalControlId)
                .ForeignKey("dbo.Teams", t => t.TeamId)
                .Index(t => t.CriticalControlId)
                .Index(t => t.TeamId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Attachments", "RotationTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.Attachments", "RotationTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.RotationTasks", "TemplateTaskId", "dbo.TemplateTasks");
            DropForeignKey("dbo.RotationTasks", "TaskBoardId", "dbo.TaskBoards");
            DropForeignKey("dbo.RotationTasks", "SuccessorTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.RotationTasks", "RotationTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTasks", "ParentTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.RotationTasks", "ParentRotationTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.RotationTaskLogs", "RootTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.RotationTasks", "ForkParentTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.RotationTasks", "AssignedToId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfiles", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.SafetyCriticalControlTeamRelations", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.SafetyCriticalControlTeamRelations", "CriticalControlId", "dbo.SafetyCriticalControls");
            DropForeignKey("dbo.SafetyMessageUserRelations", "UserId", "dbo.UserProfiles");
            DropForeignKey("dbo.SafetyMessageUserRelations", "SafetyMessageId", "dbo.SafetyMessages");
            DropForeignKey("dbo.SafetyMessageTeamRelations", "TeamId", "dbo.Teams");
            DropForeignKey("dbo.SafetyMessageTeamRelations", "SafetyMessageId", "dbo.SafetyMessages");
            DropForeignKey("dbo.SafetyMessages", "CriticalControlId", "dbo.SafetyCriticalControls");
            DropForeignKey("dbo.SafetyCriticalControls", "MajorHazardId", "dbo.SafetyMajorHazards");
            DropForeignKey("dbo.SafetyMajorHazards", "IconId", "dbo.SafetyBankOfIcons");
            DropForeignKey("dbo.SafetyBankOfIcons", "ImageId", "dbo.Files");
            DropForeignKey("dbo.TaskBoards", "Id", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfiles", "RotationPatternId", "dbo.RotationPatterns");
            DropForeignKey("dbo.RotationReportSharingRelations", "ReportOwnerId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationReportSharingRelations", "RecipientId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfiles", "PositionId", "dbo.Positions");
            DropForeignKey("dbo.Notifications", "RecipientId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserProfiles", "CurrentRotationId", "dbo.Rotations");
            DropForeignKey("dbo.RotationTasks", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "ProjectManagerId", "dbo.UserProfiles");
            DropForeignKey("dbo.ProjectFollowers", "UserId", "dbo.UserProfiles");
            DropForeignKey("dbo.ProjectFollowers", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.Projects", "CreatorId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserContributors", "ContributorId", "dbo.UserProfiles");
            DropForeignKey("dbo.UserContributors", "UserId", "dbo.UserProfiles");
            DropForeignKey("dbo.Rotations", "RotationOwnerId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationModules", "TempateModuleId", "dbo.TemplateModules");
            DropForeignKey("dbo.RotationModules", "ShiftId", "dbo.Shifts");
            DropForeignKey("dbo.Shifts", "ShiftRecipientId", "dbo.UserProfiles");
            DropForeignKey("dbo.Shifts", "RotationId", "dbo.Rotations");
            DropForeignKey("dbo.ShiftsRelations", "RelatedShiftId", "dbo.Shifts");
            DropForeignKey("dbo.ShiftsRelations", "ShiftId", "dbo.Shifts");
            DropForeignKey("dbo.RotationTopicGroups", "TempateTopicGroupId", "dbo.TemplateTopicGroups");
            DropForeignKey("dbo.VoiceMessages", "RotationTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.VoiceMessages", "RotationTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.VoiceMessages", "FileId", "dbo.Files");
            DropForeignKey("dbo.RotationTopics", "TempateTopicId", "dbo.TemplateTopics");
            DropForeignKey("dbo.RotationTopicSharingRelations", "TemplateTopicId", "dbo.TemplateTopics");
            DropForeignKey("dbo.RotationTopicSharingRelations", "SourceTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTopicSharingRelations", "RecipientId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationTopicSharingRelations", "DestinationTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.TemplateTopics", "TopicGroupId", "dbo.TemplateTopicGroups");
            DropForeignKey("dbo.TemplateTopicGroups", "ParentTopicGroupId", "dbo.TemplateTopicGroups");
            DropForeignKey("dbo.TemplateTopicGroups", "ModuleId", "dbo.TemplateModules");
            DropForeignKey("dbo.TemplateModules", "TemplateId", "dbo.Templates");
            DropForeignKey("dbo.Positions", "WorkplaceId", "dbo.Workplaces");
            DropForeignKey("dbo.Positions", "TemplateId", "dbo.Templates");
            DropForeignKey("dbo.TemplateModules", "ParentModuleId", "dbo.TemplateModules");
            DropForeignKey("dbo.TemplateTasks", "TopicId", "dbo.TemplateTopics");
            DropForeignKey("dbo.TemplateTasks", "ParentTaskId", "dbo.TemplateTasks");
            DropForeignKey("dbo.TemplateTopics", "ParentTopicId", "dbo.TemplateTopics");
            DropForeignKey("dbo.RotationTopics", "SuccessorTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTopics", "ShareSourceTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTopics", "RotationTopicGroupId", "dbo.RotationTopicGroups");
            DropForeignKey("dbo.RotationTopics", "ParentTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTopics", "ParentRotationTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.ManagerComments", "RotationTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTopicLogs", "UserId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationTopicLogs", "RootTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTopics", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.RotationTopics", "ForkParentTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.AttachmentsLinks", "RotationTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.AttachmentsLinks", "RotationTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.RotationTopics", "AssignedToId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationTopics", "AncestorTopicId", "dbo.RotationTopics");
            DropForeignKey("dbo.RotationTopicGroups", "RotationModuleId", "dbo.RotationModules");
            DropForeignKey("dbo.RotationTopicGroups", "ParentRotationTopicGroupId", "dbo.RotationTopicGroups");
            DropForeignKey("dbo.RotationModules", "RotationId", "dbo.Rotations");
            DropForeignKey("dbo.Rotations", "PrevRotationId", "dbo.Rotations");
            DropForeignKey("dbo.Rotations", "NextRotationId", "dbo.Rotations");
            DropForeignKey("dbo.Rotations", "LineManagerId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationsRelations", "RelatedRotationId", "dbo.Rotations");
            DropForeignKey("dbo.RotationsRelations", "RotationId", "dbo.Rotations");
            DropForeignKey("dbo.Rotations", "DefaultBackToBackId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationContributors", "UserId", "dbo.UserProfiles");
            DropForeignKey("dbo.RotationContributors", "RotationId", "dbo.Rotations");
            DropForeignKey("dbo.UserProfiles", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.SafetyV2Stats", "SafetyMessageId", "dbo.SafetyV2Messages");
            DropForeignKey("dbo.SafetyV2Stats", "RecipientId", "dbo.UserProfiles");
            DropForeignKey("dbo.SafetyV2Messages", "CriticalControlId", "dbo.SafetyV2CriticalControls");
            DropForeignKey("dbo.SafetyV2CriticalControls", "OwnerId", "dbo.UserProfiles");
            DropForeignKey("dbo.SafetyV2CriticalControls", "MajorHazardId", "dbo.SafetyV2MajorHazards");
            DropForeignKey("dbo.SafetyV2MajorHazards", "OwnerId", "dbo.UserProfiles");
            DropForeignKey("dbo.SafetyV2CriticalControlChampions", "UserId", "dbo.UserProfiles");
            DropForeignKey("dbo.SafetyV2CriticalControlChampions", "CriticalControlId", "dbo.SafetyV2CriticalControls");
            DropForeignKey("dbo.UserProfiles", "AvatarId", "dbo.Files");
            DropForeignKey("dbo.RotationTasks", "AncestorTaskId", "dbo.RotationTasks");
            DropForeignKey("dbo.Attachments", "FileId", "dbo.Files");
            DropForeignKey("dbo.AdminSettings", "LogoId", "dbo.Files");
            DropIndex("dbo.SafetyCriticalControlTeamRelations", new[] { "TeamId" });
            DropIndex("dbo.SafetyCriticalControlTeamRelations", new[] { "CriticalControlId" });
            DropIndex("dbo.SafetyMessageUserRelations", new[] { "UserId" });
            DropIndex("dbo.SafetyMessageUserRelations", new[] { "SafetyMessageId" });
            DropIndex("dbo.SafetyMessageTeamRelations", new[] { "TeamId" });
            DropIndex("dbo.SafetyMessageTeamRelations", new[] { "SafetyMessageId" });
            DropIndex("dbo.ProjectFollowers", new[] { "UserId" });
            DropIndex("dbo.ProjectFollowers", new[] { "ProjectId" });
            DropIndex("dbo.UserContributors", new[] { "ContributorId" });
            DropIndex("dbo.UserContributors", new[] { "UserId" });
            DropIndex("dbo.ShiftsRelations", new[] { "RelatedShiftId" });
            DropIndex("dbo.ShiftsRelations", new[] { "ShiftId" });
            DropIndex("dbo.RotationsRelations", new[] { "RelatedRotationId" });
            DropIndex("dbo.RotationsRelations", new[] { "RotationId" });
            DropIndex("dbo.RotationContributors", new[] { "UserId" });
            DropIndex("dbo.RotationContributors", new[] { "RotationId" });
            DropIndex("dbo.SafetyV2CriticalControlChampions", new[] { "UserId" });
            DropIndex("dbo.SafetyV2CriticalControlChampions", new[] { "CriticalControlId" });
            DropIndex("dbo.RotationTaskLogs", new[] { "RootTaskId" });
            DropIndex("dbo.SafetyMessages", new[] { "CriticalControlId" });
            DropIndex("dbo.SafetyBankOfIcons", new[] { "ImageId" });
            DropIndex("dbo.SafetyMajorHazards", new[] { "IconId" });
            DropIndex("dbo.SafetyCriticalControls", new[] { "MajorHazardId" });
            DropIndex("dbo.TaskBoards", new[] { "Id" });
            DropIndex("dbo.RotationReportSharingRelations", new[] { "ReportOwnerId" });
            DropIndex("dbo.RotationReportSharingRelations", new[] { "RecipientId" });
            DropIndex("dbo.Notifications", new[] { "RecipientId" });
            DropIndex("dbo.Projects", new[] { "ProjectManagerId" });
            DropIndex("dbo.Projects", new[] { "CreatorId" });
            DropIndex("dbo.Shifts", new[] { "RotationId" });
            DropIndex("dbo.Shifts", new[] { "ShiftRecipientId" });
            DropIndex("dbo.VoiceMessages", new[] { "RotationTopicId" });
            DropIndex("dbo.VoiceMessages", new[] { "RotationTaskId" });
            DropIndex("dbo.VoiceMessages", new[] { "FileId" });
            DropIndex("dbo.RotationTopicSharingRelations", new[] { "TemplateTopicId" });
            DropIndex("dbo.RotationTopicSharingRelations", new[] { "RecipientId" });
            DropIndex("dbo.RotationTopicSharingRelations", new[] { "DestinationTopicId" });
            DropIndex("dbo.RotationTopicSharingRelations", new[] { "SourceTopicId" });
            DropIndex("dbo.Positions", new[] { "TemplateId" });
            DropIndex("dbo.Positions", new[] { "WorkplaceId" });
            DropIndex("dbo.TemplateModules", new[] { "ParentModuleId" });
            DropIndex("dbo.TemplateModules", new[] { "TemplateId" });
            DropIndex("dbo.TemplateTopicGroups", new[] { "ParentTopicGroupId" });
            DropIndex("dbo.TemplateTopicGroups", new[] { "ModuleId" });
            DropIndex("dbo.TemplateTasks", new[] { "ParentTaskId" });
            DropIndex("dbo.TemplateTasks", new[] { "TopicId" });
            DropIndex("dbo.TemplateTopics", new[] { "ParentTopicId" });
            DropIndex("dbo.TemplateTopics", new[] { "TopicGroupId" });
            DropIndex("dbo.ManagerComments", new[] { "RotationTopicId" });
            DropIndex("dbo.RotationTopicLogs", new[] { "UserId" });
            DropIndex("dbo.RotationTopicLogs", new[] { "RootTopicId" });
            DropIndex("dbo.AttachmentsLinks", new[] { "RotationTopicId" });
            DropIndex("dbo.AttachmentsLinks", new[] { "RotationTaskId" });
            DropIndex("dbo.RotationTopics", new[] { "ParentRotationTopicId" });
            DropIndex("dbo.RotationTopics", new[] { "ShareSourceTopicId" });
            DropIndex("dbo.RotationTopics", new[] { "ForkParentTopicId" });
            DropIndex("dbo.RotationTopics", new[] { "ParentTopicId" });
            DropIndex("dbo.RotationTopics", new[] { "SuccessorTopicId" });
            DropIndex("dbo.RotationTopics", new[] { "AncestorTopicId" });
            DropIndex("dbo.RotationTopics", new[] { "TempateTopicId" });
            DropIndex("dbo.RotationTopics", new[] { "RotationTopicGroupId" });
            DropIndex("dbo.RotationTopics", new[] { "LocationId" });
            DropIndex("dbo.RotationTopics", new[] { "AssignedToId" });
            DropIndex("dbo.RotationTopicGroups", new[] { "ParentRotationTopicGroupId" });
            DropIndex("dbo.RotationTopicGroups", new[] { "TempateTopicGroupId" });
            DropIndex("dbo.RotationTopicGroups", new[] { "RotationModuleId" });
            DropIndex("dbo.RotationModules", new[] { "ShiftId" });
            DropIndex("dbo.RotationModules", new[] { "RotationId" });
            DropIndex("dbo.RotationModules", new[] { "TempateModuleId" });
            DropIndex("dbo.Rotations", new[] { "NextRotationId" });
            DropIndex("dbo.Rotations", new[] { "PrevRotationId" });
            DropIndex("dbo.Rotations", new[] { "DefaultBackToBackId" });
            DropIndex("dbo.Rotations", new[] { "LineManagerId" });
            DropIndex("dbo.Rotations", new[] { "RotationOwnerId" });
            DropIndex("dbo.SafetyV2Stats", new[] { "RecipientId" });
            DropIndex("dbo.SafetyV2Stats", new[] { "SafetyMessageId" });
            DropIndex("dbo.SafetyV2Messages", new[] { "CriticalControlId" });
            DropIndex("dbo.SafetyV2MajorHazards", new[] { "OwnerId" });
            DropIndex("dbo.SafetyV2CriticalControls", new[] { "MajorHazardId" });
            DropIndex("dbo.SafetyV2CriticalControls", new[] { "OwnerId" });
            DropIndex("dbo.UserProfiles", new[] { "CurrentRotationId" });
            DropIndex("dbo.UserProfiles", new[] { "CompanyId" });
            DropIndex("dbo.UserProfiles", new[] { "TeamId" });
            DropIndex("dbo.UserProfiles", new[] { "RotationPatternId" });
            DropIndex("dbo.UserProfiles", new[] { "PositionId" });
            DropIndex("dbo.UserProfiles", new[] { "AvatarId" });
            DropIndex("dbo.RotationTasks", new[] { "ParentRotationTaskId" });
            DropIndex("dbo.RotationTasks", new[] { "ProjectId" });
            DropIndex("dbo.RotationTasks", new[] { "TaskBoardId" });
            DropIndex("dbo.RotationTasks", new[] { "ForkParentTaskId" });
            DropIndex("dbo.RotationTasks", new[] { "ParentTaskId" });
            DropIndex("dbo.RotationTasks", new[] { "SuccessorTaskId" });
            DropIndex("dbo.RotationTasks", new[] { "AncestorTaskId" });
            DropIndex("dbo.RotationTasks", new[] { "TemplateTaskId" });
            DropIndex("dbo.RotationTasks", new[] { "RotationTopicId" });
            DropIndex("dbo.RotationTasks", new[] { "AssignedToId" });
            DropIndex("dbo.Attachments", new[] { "RotationTopicId" });
            DropIndex("dbo.Attachments", new[] { "RotationTaskId" });
            DropIndex("dbo.Attachments", new[] { "FileId" });
            DropIndex("dbo.AdminSettings", new[] { "LogoId" });
            DropTable("dbo.SafetyCriticalControlTeamRelations");
            DropTable("dbo.SafetyMessageUserRelations");
            DropTable("dbo.SafetyMessageTeamRelations");
            DropTable("dbo.ProjectFollowers");
            DropTable("dbo.UserContributors");
            DropTable("dbo.ShiftsRelations");
            DropTable("dbo.RotationsRelations");
            DropTable("dbo.RotationContributors");
            DropTable("dbo.SafetyV2CriticalControlChampions");
            DropTable("dbo.RotationTaskLogs");
            DropTable("dbo.SafetyMessages");
            DropTable("dbo.SafetyBankOfIcons");
            DropTable("dbo.SafetyMajorHazards");
            DropTable("dbo.SafetyCriticalControls");
            DropTable("dbo.Teams");
            DropTable("dbo.TaskBoards");
            DropTable("dbo.RotationPatterns");
            DropTable("dbo.RotationReportSharingRelations");
            DropTable("dbo.Notifications");
            DropTable("dbo.Projects");
            DropTable("dbo.Shifts");
            DropTable("dbo.VoiceMessages");
            DropTable("dbo.RotationTopicSharingRelations");
            DropTable("dbo.Workplaces");
            DropTable("dbo.Positions");
            DropTable("dbo.Templates");
            DropTable("dbo.TemplateModules");
            DropTable("dbo.TemplateTopicGroups");
            DropTable("dbo.TemplateTasks");
            DropTable("dbo.TemplateTopics");
            DropTable("dbo.ManagerComments");
            DropTable("dbo.RotationTopicLogs");
            DropTable("dbo.Locations");
            DropTable("dbo.AttachmentsLinks");
            DropTable("dbo.RotationTopics");
            DropTable("dbo.RotationTopicGroups");
            DropTable("dbo.RotationModules");
            DropTable("dbo.Rotations");
            DropTable("dbo.Companies");
            DropTable("dbo.SafetyV2Stats");
            DropTable("dbo.SafetyV2Messages");
            DropTable("dbo.SafetyV2MajorHazards");
            DropTable("dbo.SafetyV2CriticalControls");
            DropTable("dbo.UserProfiles");
            DropTable("dbo.RotationTasks");
            DropTable("dbo.Attachments");
            DropTable("dbo.Files");
            DropTable("dbo.AdminSettings");
        }
    }
}
