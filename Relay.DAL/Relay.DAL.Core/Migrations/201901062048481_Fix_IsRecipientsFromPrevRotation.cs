namespace MomentumPlus.Core.DataSource.DbInit
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fix_IsRecipientsFromPrevRotation : DbMigration
    {
        public override void Up()
        {
            this.AddColumnIfNotExists("dbo.RotationTopics", "IsRecipientsFromPrevRotation", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.RotationTopics", "IsRecipientsFromPrevRotation");
        }

    }
}
