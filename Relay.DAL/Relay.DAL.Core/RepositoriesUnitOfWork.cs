﻿using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Core.DataSource
{
    public class RepositoriesUnitOfWork : IRepositoriesUnitOfWork
    {
        private readonly MomentumContext _context;

        public RepositoriesUnitOfWork(MomentumContext context)
        {
            _context = context;
        }


        #region User Profile

        private IGenericRepository<UserProfile> _userProfileRepository;

        #endregion

        #region Company, Rotation Pattern, Position, Workplace, Team

        private IGenericRepository<Company> _companyRepository;

        private IGenericRepository<RotationPattern> _rotationPatternRepository;

        private IGenericRepository<Position> _positionRepository;

        private IGenericRepository<Workplace> _workplaceRepository;

        private IGenericRepository<Team> _teamRepository;

        private IGenericRepository<Template> _rTemplateRepository;
        #endregion


        #region Template, TemplateModule, TemplateTopicGroup, TemplateTopic, TemplateTask

        private IGenericRepository<Template> _templateRepository;

        private IGenericRepository<TemplateModule> _templateModuleRepository;

        private IGenericRepository<TemplateTopicGroup> _templateTopicGroupRepository;

        private IGenericRepository<TemplateTopic> _templateTopicRepository;

        private IGenericRepository<TemplateTask> _templateTaskRepository;

        #endregion


        #region Rotation, Shift, RotationModule, RotationTopicGroup, RotationTopic, RotationTask, Location

        private IGenericRepository<Rotation> _rotationRepository;

        private IGenericRepository<ManagerComments> _managerCommentsRepository;

        private IGenericRepository<Shift> _shiftRepository;

        private IGenericRepository<RotationModule> _rotationModuleRepository;

        private IGenericRepository<RotationTopicGroup> _rotationTopicGroupRepository;

        private IGenericRepository<RotationTopic> _rotationTopicRepository;

        private IGenericRepository<RotationTask> _rotationTaskRepository;

        private IGenericRepository<Location> _locationRepository;

        private IGenericRepository<RotationTaskLog> _taskLogRepository;

        private IGenericRepository<RotationTopicLog> _topicLogRepository;

        #endregion


        #region Safety v1

        private IGenericRepository<MajorHazard> _majorHazardRepository;

        private IGenericRepository<CriticalControl> _criticalControlRepository;

        private IGenericRepository<SafetyMessage> _safetyMessageRepository;

        private IGenericRepository<BankOfIconItem> _bankOfIconRepository;

        #endregion


        #region Safety v2

        private IGenericRepository<MajorHazardV2> _majorHazardV2Repository;

        private IGenericRepository<CriticalControlV2> _criticalControlV2Repository;

        private IGenericRepository<SafetyMessageV2> _safetyMessageV2Repository;

        private IGenericRepository<SafetyStatV2> _safetyStatV2Repository;

        #endregion


        #region File, Attachment, VoiceMessage

        private IGenericRepository<Attachment> _attachmentRepository;

        private IGenericRepository<AttachmentsLink> _attachmentsLinkRepository;

        private IGenericRepository<File> _fileRepository;

        private IGenericRepository<VoiceMessage> _voiceMessageRepository;

        #endregion


        #region Project, Admin Settings, Notification

        private IGenericRepository<AdminSettings> _adminSettingsRepository;

        private IGenericRepository<Project> _projectsRepository;

        private IGenericRepository<Notification> _notificationsRepository;

        #endregion


        #region Sharing Topic Relations, Sharing Report Relations

        private IGenericRepository<RotationTopicSharingRelation> _sharingTopicRelationsRepository;

        private IGenericRepository<RotationReportSharingRelation> _sharingReportRelationsRepository;

        #endregion


        // Task Board 
        private IGenericRepository<TaskBoard> _taskBoardRepository;
        //

        //----------------------------------------
        #region IUnitOfWork Members

        #region User Profile

        public IGenericRepository<UserProfile> UserProfileRepository
        {
            get
            {
                return _userProfileRepository ?? (_userProfileRepository = new GenericRepository<UserProfile>(_context));
            }
        }

        #endregion



        public IGenericRepository<RotationReportSharingRelation> ReportSharingRelationRepository
        {
            get
            {
                return _sharingReportRelationsRepository ?? (_sharingReportRelationsRepository = new GenericRepository<RotationReportSharingRelation>(_context));
            }
        }


        public IGenericRepository<Shift> ShiftRepository
        {
            get
            {
                return _shiftRepository ?? (_shiftRepository = new GenericRepository<Shift>(_context));
            }
        }


        public IGenericRepository<RotationTopicSharingRelation> TopicSharingRelationRepository
        {
            get
            {
                return _sharingTopicRelationsRepository ?? (_sharingTopicRelationsRepository = new GenericRepository<RotationTopicSharingRelation>(_context));
            }
        }

        public IGenericRepository<Company> CompanyRepository
        {
            get
            {
                return _companyRepository ?? (_companyRepository = new GenericRepository<Company>(_context));
            }
        }



        public IGenericRepository<Template> TemplateRepository
        {
            get
            {
                return _rTemplateRepository ?? (_rTemplateRepository = new GenericRepository<Template>(_context));
            }
        }

        public IGenericRepository<AdminSettings> AdminSettingsRepository
        {
            get
            {
                return _adminSettingsRepository ?? (_adminSettingsRepository = new GenericRepository<AdminSettings>(_context));
            }
        }

        public IGenericRepository<TemplateModule> TemplateModuleRepository
        {
            get
            {
                return _templateModuleRepository ?? (_templateModuleRepository = new GenericRepository<TemplateModule>(_context));
            }
        }

        public IGenericRepository<TemplateTopicGroup> TemplateTopicGroupRepository
        {
            get
            {
                return _templateTopicGroupRepository ?? (_templateTopicGroupRepository = new GenericRepository<TemplateTopicGroup>(_context));
            }
        }

        public IGenericRepository<TemplateTopic> TemplateTopicRepository
        {
            get
            {
                return _templateTopicRepository ?? (_templateTopicRepository = new GenericRepository<TemplateTopic>(_context));
            }
        }

        public IGenericRepository<TemplateTask> TemplateTaskRepository
        {
            get
            {
                return _templateTaskRepository ?? (_templateTaskRepository = new GenericRepository<TemplateTask>(_context));
            }
        }

        public IGenericRepository<RotationModule> RotationModuleRepository
        {
            get
            {
                return _rotationModuleRepository ?? (_rotationModuleRepository = new GenericRepository<RotationModule>(_context));
            }
        }

        public IGenericRepository<RotationTopicGroup> RotationTopicGroupRepository
        {
            get
            {
                return _rotationTopicGroupRepository ?? (_rotationTopicGroupRepository = new GenericRepository<RotationTopicGroup>(_context));
            }
        }

        public IGenericRepository<RotationTopic> RotationTopicRepository
        {
            get
            {
                return _rotationTopicRepository ?? (_rotationTopicRepository = new GenericRepository<RotationTopic>(_context));
            }
        }

        public IGenericRepository<RotationTask> RotationTaskRepository
        {
            get
            {
                return _rotationTaskRepository ?? (_rotationTaskRepository = new GenericRepository<RotationTask>(_context));
            }
        }

        public IGenericRepository<Attachment> AttachmentRepository
        {
            get
            {
                return _attachmentRepository ?? (_attachmentRepository = new GenericRepository<Attachment>(_context));
            }
        }

        public IGenericRepository<AttachmentsLink> AttachmentsLinkRepository
        {
            get
            {
                return _attachmentsLinkRepository ?? (_attachmentsLinkRepository = new GenericRepository<AttachmentsLink>(_context));
            }
        }

        public IGenericRepository<ManagerComments> ManagerCommentsRepository
        {
            get
            {
                return _managerCommentsRepository ?? (_managerCommentsRepository = new GenericRepository<ManagerComments>(_context));
            }
        }


        public IGenericRepository<File> FileRepository
        {
            get
            {
                return _fileRepository ?? (_fileRepository = new GenericRepository<File>(_context));
            }
        }


        public IGenericRepository<Position> PositionRepository
        {
            get
            {
                return _positionRepository ?? (_positionRepository = new GenericRepository<Position>(_context));
            }
        }


        public IGenericRepository<VoiceMessage> VoiceMessageRepository
        {
            get
            {
                return _voiceMessageRepository ?? (_voiceMessageRepository = new GenericRepository<VoiceMessage>(_context));
            }
        }

        public IGenericRepository<Workplace> WorkplaceRepository
        {
            get
            {
                return _workplaceRepository ?? (_workplaceRepository = new GenericRepository<Workplace>(_context));
            }
        }


        public IGenericRepository<Team> TeamRepository
        {
            get
            {
                return _teamRepository ?? (_teamRepository = new GenericRepository<Team>(_context));
            }
        }

        public IGenericRepository<RotationPattern> RotationPatternRepository
        {
            get
            {
                return _rotationPatternRepository ?? (_rotationPatternRepository = new GenericRepository<RotationPattern>(_context));
            }
        }

        public IGenericRepository<Rotation> RotationRepository
        {
            get
            {
                return _rotationRepository ?? (_rotationRepository = new GenericRepository<Rotation>(_context));
            }
        }

        public IGenericRepository<Location> LocationRepository
        {
            get
            {
                return _locationRepository ?? (_locationRepository = new GenericRepository<Location>(_context));
            }
        }

        public IGenericRepository<MajorHazard> MajorHazardRepository
        {
            get
            {
                return _majorHazardRepository ?? (_majorHazardRepository = new GenericRepository<MajorHazard>(_context));
            }
        }

        public IGenericRepository<CriticalControl> CriticalControlRepository
        {
            get
            {
                return _criticalControlRepository ?? (_criticalControlRepository = new GenericRepository<CriticalControl>(_context));
            }
        }

        public IGenericRepository<SafetyMessage> SafetyMessageRepository
        {
            get
            {
                return _safetyMessageRepository ?? (_safetyMessageRepository = new GenericRepository<SafetyMessage>(_context));
            }
        }

        public IGenericRepository<BankOfIconItem> BankOfIconRepository
        {
            get
            {
                return _bankOfIconRepository ?? (_bankOfIconRepository = new GenericRepository<BankOfIconItem>(_context));
            }
        }

        public IGenericRepository<Project> ProjectRepository
        {
            get
            {
                return _projectsRepository ?? (_projectsRepository = new GenericRepository<Project>(this._context));
            }
        }

        public IGenericRepository<MajorHazardV2> MajorHazardV2Repository
        {
            get
            {
                return _majorHazardV2Repository ?? (_majorHazardV2Repository = new GenericRepository<MajorHazardV2>(this._context));
            }
        }

        public IGenericRepository<CriticalControlV2> CriticalControlV2Repository
        {
            get
            {
                return _criticalControlV2Repository ?? (_criticalControlV2Repository = new GenericRepository<CriticalControlV2>(this._context));
            }
        }

        public IGenericRepository<SafetyMessageV2> SafetyMessageV2Repository
        {
            get
            {
                return _safetyMessageV2Repository ?? (_safetyMessageV2Repository = new GenericRepository<SafetyMessageV2>(this._context));
            }
        }

        public IGenericRepository<SafetyStatV2> SafetyStatV2Repository
        {
            get
            {
                return _safetyStatV2Repository ?? (_safetyStatV2Repository = new GenericRepository<SafetyStatV2>(this._context));
            }
        }

        public IGenericRepository<Notification> NotificationRepository
        {
            get
            {
                return _notificationsRepository ?? (_notificationsRepository = new GenericRepository<Notification>(this._context));
            }
        }

        public IGenericRepository<TaskBoard> TaskBoardRepository
        {
            get
            {
                return _taskBoardRepository ?? (_taskBoardRepository = new GenericRepository<TaskBoard>(this._context));
            }
        }


        public IGenericRepository<RotationTaskLog> TaskLogRepository
        {
            get
            {
                return _taskLogRepository ?? (_taskLogRepository = new GenericRepository<RotationTaskLog>(this._context));
            }
        }

        public IGenericRepository<RotationTopicLog> TopicLogRepository
        {
            get
            {
                return _topicLogRepository ?? (_topicLogRepository = new GenericRepository<RotationTopicLog>(_context));
            }
        }

        //------------------------------------------------
        public int Save()
        {
            return _context.SaveChanges();
        }

        #endregion
        //----------------------------------------
    }
}
