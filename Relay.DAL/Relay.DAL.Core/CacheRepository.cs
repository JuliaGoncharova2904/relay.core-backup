﻿using System;
using System.Runtime.Caching;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Core.DataSource
{
    public class CacheRepository<TEntity> : ICacheRepository<TEntity> where TEntity : BaseModel
    {

        public TEntity Get(Guid id)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            return memoryCache.Get(id.ToString()) as TEntity;
        }

        public void Delete(Guid id)
        {
            MemoryCache memoryCache = MemoryCache.Default;
            if (memoryCache.Contains(id.ToString()))
            {
                memoryCache.Remove(id.ToString());
            }
        }

        public void Update(TEntity entity)
        {
            MemoryCache memoryCache = MemoryCache.Default;

            var cash = Get(entity.Id);
            if (cash != null)
            {
                memoryCache.Set(entity.Id.ToString(), entity, DateTime.Now.AddMinutes(10));
            }
        }

        public bool Add(TEntity entity)
        {
            MemoryCache memoryCache = MemoryCache.Default;

            var cash = Get(entity.Id);
            if (cash != null)
            {
                return memoryCache.Add(entity.Id.ToString(), entity, DateTime.Now.AddMinutes(10));
            }

          return false;
        }
    }
}
