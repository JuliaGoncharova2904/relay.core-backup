﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using MomentumPlus.Core.Interfaces;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Core.DataSource
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : BaseModel
    {
        private MomentumContext context;
        private DbSet<TEntity> dbSet;
        private ICacheRepository<TEntity> _cacheRepository;

        public GenericRepository(MomentumContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
            this._cacheRepository = new CacheRepository<TEntity>();
        }

        public int Count(EntityAccess access = EntityAccess.NoDeleted)
        {
            int result = 0;

            switch (access)
            {
                case EntityAccess.All:
                    result = dbSet.Count();
                    break;
                case EntityAccess.NoDeleted:
                    result = dbSet.Count(e => e.DeletedUtc.HasValue == false);
                    break;
                case EntityAccess.Deleted:
                    result = dbSet.Count(e => e.DeletedUtc.HasValue == true);
                    break;
            }

            return result;
        }

        public TEntity Create()
        {
            return dbSet.Create();
        }

        public TEntity Attach(Guid id)
        {
            TEntity entity = (TEntity)Activator.CreateInstance(typeof(TEntity));

            (entity as BaseModel).Id = id;

            dbSet.Attach(entity);

            return entity;
        }

        public TEntity Get(Guid id)
        {
            var result = _cacheRepository.Get(id) ?? dbSet.Find(id);

            return result;
        }

        public IQueryable<TEntity> GetAll(EntityAccess access = EntityAccess.NoDeleted)
        {
            IQueryable<TEntity> result = null;

            switch (access)
            {
                case EntityAccess.All:
                    result = dbSet;
                    break;
                case EntityAccess.NoDeleted:
                    result = dbSet.Where(e => e.DeletedUtc.HasValue == false);
                    break;
                case EntityAccess.Deleted:
                    result = dbSet.Where(e => e.DeletedUtc.HasValue == true);
                    break;
            }

            return result;
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> expression, EntityAccess access = EntityAccess.NoDeleted)
        {
            IQueryable<TEntity> query = GetAll(access);
                
            if (expression != null)
                query = query.Where(expression);

            return query;
        }

        public TEntity Add(TEntity entity)
        {
            entity = dbSet.Add(entity);

            _cacheRepository.Add(entity);

            return entity;
        }

        public IEnumerable<TEntity> Add(params TEntity[] entities)
        {
            IEnumerable<TEntity> result = dbSet.AddRange(entities);

            foreach (var entity in entities)
            {
                _cacheRepository.Add(entity);

            }

            return result;
        }

        public void Delete(Guid id)
        {
            TEntity entity = dbSet.Find(id);
            _cacheRepository.Delete(id);

            Delete(entity);
        }

        public void Delete(TEntity entity)
        {
            if (entity != null)
            {
                dbSet.Remove(entity);
                _cacheRepository.Delete(entity.Id);

            }
        }

        public void Delete(IEnumerable<Guid> ids)
        {
            IEnumerable<TEntity> entities = dbSet.Where(e => ids.Contains(e.Id));
            Delete(entities);
        }

        public void Delete(IEnumerable<TEntity> entities)
        {
            if (entities != null)
            {
                dbSet.RemoveRange(entities);
            }

            foreach (var entity in entities)
            {
                _cacheRepository.Delete(entity.Id);

            }
        }

        public void Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            _cacheRepository.Update(entity);
        }

    }

}
