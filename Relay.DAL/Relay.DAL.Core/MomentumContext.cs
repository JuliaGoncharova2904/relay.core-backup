﻿using MomentumPlus.Core.DataSource.DbInit;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Interfaces.Auth;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;

namespace MomentumPlus.Core.DataSource
{
    public class MomentumContext : DbContext
    {

        public MomentumContext(IAuthBridge authBridge) : this(authBridge.GetActiveRelayDbConnectionString())
        { }

        public MomentumContext(string connectionString) : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = true;
            Configuration.ProxyCreationEnabled = true;

            this.Database.Connection.ConnectionString = connectionString;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<MomentumContext, DbInitializerConfiguration>(true, new DbInitializerConfiguration()));
        }

        //public MomentumContext()
        //{
        //    Configuration.LazyLoadingEnabled = true;
        //    Configuration.ProxyCreationEnabled = true;
        //    Database.SetInitializer(new MigrateDatabaseToLatestVersion<MomentumContext, DbInitializerConfiguration>(true, new DbInitializerConfiguration()));
        //}

        public DbSet<UserProfile> UsersProfiles { get; set; }

        public DbSet<Position> Positions { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<Workplace> Workplaces { get; set; }

        public DbSet<RotationPattern> RotationPatterns { get; set; }

        public DbSet<Rotation> Rotations { get; set; }

        public DbSet<Template> Templates { get; set; }

        public DbSet<TemplateModule> TemplateModules { get; set; }

        public DbSet<TemplateTopicGroup> TemplateTopicGroups { get; set; }

        public DbSet<TemplateTopic> TemplateTopics { get; set; }

        public DbSet<TemplateTask> TemplateTasks { get; set; }

        public DbSet<RotationModule> RotationModules { get; set; }

        public DbSet<RotationTopicGroup> RotationTopicGroups { get; set; }

        public DbSet<RotationTopic> RotationTopics { get; set; }

        public DbSet<RotationTask> RotationTasks { get; set; }

        public DbSet<RotationTopicSharingRelation> TopicsSharingRelations { get; set; }

        public DbSet<RotationReportSharingRelation> RotationsReportsRelations { get; set; }

        public DbSet<Location> Locations { get; set; }

        public DbSet<AdminSettings> AdminSettings { get; set; }

        public DbSet<MajorHazard> SafetyMajorHazards { get; set; }

        public DbSet<MajorHazardV2> SafetyV2MajorHazards { get; set; }

        public DbSet<CriticalControl> SafetyCriticalControls { get; set; }

        public DbSet<CriticalControlV2> SafetyV2CriticalControls { get; set; }

        public DbSet<SafetyMessage> SafetyMessages { get; set; }

        public DbSet<SafetyMessageV2> SafetyV2Messages { get; set; }

        public DbSet<BankOfIconItem> SafetyBankOfIcons { get; set; }

        public DbSet<SafetyStatV2> SafetyV2Stats { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Attachment> Attachments { get; set; }

        public DbSet<AttachmentsLink> AttachmentsLink { get; set; }

        public DbSet<Models.ManagerComments> ManagerComment { get; set; }

        public DbSet<File> Files { get; set; }

        public DbSet<VoiceMessage> VoiceMessages { get; set; }

        public DbSet<TaskBoard> TaskBoards { get; set; }

        public DbSet<RotationTaskLog> RotationTaskLogs { get; set; }

        public DbSet<RotationTopicLog> RotationTopicLogs { get; set; }

        //-------------------------------------------------
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            UserProfile_EntitiesConfiguration(modelBuilder);

            Position_EntitiesConfiguration(modelBuilder);

            Rotation_EntitiesConfiguration(modelBuilder);

            Shift_EntitiesConfiguration(modelBuilder);

            RotationModule_EntitiesConfiguration(modelBuilder);

            Log_EntitiesConfiguration(modelBuilder);

            TemplateModule_EntitiesConfiguration(modelBuilder);

            SafetyMessage_EntitiesConfiguration(modelBuilder);

            SafetyMessageV2_EntitiesConfiguration(modelBuilder);

            Project_EntitiesConfiguration(modelBuilder);

            Notification_EntitiesConfiguration(modelBuilder);

            Attachments_EntitiesConfiguration(modelBuilder);

            AttachmentsLink_EntitiesConfiguration(modelBuilder);

            ManagerComments_EntitiesConfiguration(modelBuilder);

            VoiceMessages_EntitiesConfiguration(modelBuilder);

            AdminSettings_EntitiesConfiguration(modelBuilder);

            Obsolete_EntitiesConfiguration(modelBuilder);
        }

        private void Obsolete_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.ParentRotationTopic)
                        .WithMany(rotationTopic => rotationTopic.ChildRotationTopics)
                        .HasForeignKey(rotationTopic => rotationTopic.ParentRotationTopicId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTask>()
                         .HasOptional(rotationTask => rotationTask.ParentRotationTask)
                         .WithMany(rotationTask => rotationTask.ChildRotationTasks)
                         .HasForeignKey(rotationTask => rotationTask.ParentRotationTaskId)
                         .WillCascadeOnDelete(false);

        }


        #region Fluent API configuration

        private void UserProfile_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //User Profile
            modelBuilder.Entity<UserProfile>()
                        .HasRequired(userProfile => userProfile.RotationPattern)
                        .WithMany(rotationPattern => rotationPattern.Users)
                        .HasForeignKey(userProfile => userProfile.RotationPatternId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<UserProfile>()
                        .HasRequired(userProfile => userProfile.Team)
                        .WithMany(team => team.Users)
                        .HasForeignKey(userProfile => userProfile.TeamId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<UserProfile>()
                        .HasRequired(userProfile => userProfile.Position)
                        .WithMany(position => position.Users)
                        .HasForeignKey(userProfile => userProfile.PositionId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<UserProfile>()
                        .HasRequired(userProfile => userProfile.Company)
                        .WithMany(company => company.Users)
                        .HasForeignKey(userProfile => userProfile.CompanyId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<UserProfile>()
                        .HasOptional(userProfile => userProfile.Avatar)
                        .WithMany()
                        .HasForeignKey(rotation => rotation.AvatarId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<UserProfile>()
                        .HasOptional(userProfile => userProfile.CurrentRotation)
                        .WithMany()
                        .HasForeignKey(rotation => rotation.CurrentRotationId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<UserProfile>()
                        .HasRequired(userProfile => userProfile.TaskBoard)
                        .WithRequiredPrincipal(taskBoard => taskBoard.User)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<UserProfile>()
                        .HasMany(userProfile => userProfile.Contributors)
                        .WithMany(userProfile => userProfile.ContributedUsers)
                        .Map(model =>
                        {
                            model.MapLeftKey("UserId");
                            model.MapRightKey("ContributorId");
                            model.ToTable("UserContributors");
                        });
        }

        private void Rotation_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Rotation
            modelBuilder.Entity<Rotation>()
                        .HasRequired(rotation => rotation.RotationOwner)
                        .WithMany(userProfile => userProfile.WorkRotations)
                        .HasForeignKey(rotation => rotation.RotationOwnerId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<Rotation>()
                        .HasRequired(rotation => rotation.LineManager)
                        .WithMany(userProfile => userProfile.ManagedRotations)
                        .HasForeignKey(rotation => rotation.LineManagerId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<Rotation>()
                        .HasRequired(rotation => rotation.DefaultBackToBack)
                        .WithMany(userProfile => userProfile.HandoverSourceRotations)
                        .HasForeignKey(rotation => rotation.DefaultBackToBackId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<Rotation>()
                        .HasMany(rotation => rotation.Contributors)
                        .WithMany(userProfile => userProfile.ContributedRotations)
                        .Map(model =>
                        {
                            model.MapLeftKey("RotationId");
                            model.MapRightKey("UserId");
                            model.ToTable("RotationContributors");
                        });


            modelBuilder.Entity<Rotation>()
                        .HasMany(rotation => rotation.HandoverToRotations)
                        .WithMany(rotation => rotation.HandoverFromRotations)
                        .Map(model =>
                        {
                            model.MapLeftKey("RotationId");
                            model.MapRightKey("RelatedRotationId");
                            model.ToTable("RotationsRelations");
                        });


            modelBuilder.Entity<Rotation>()
                        .HasOptional(rotation => rotation.PrevRotation)
                        .WithMany()
                        .HasForeignKey(rotation => rotation.PrevRotationId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<Rotation>()
                        .HasOptional(rotation => rotation.NextRotation)
                        .WithMany()
                        .HasForeignKey(rotation => rotation.NextRotationId)
                        .WillCascadeOnDelete(false);


            //Rotation Sharing Relation
            modelBuilder.Entity<RotationReportSharingRelation>()
                        .HasRequired(s => s.Recipient)
                        .WithMany(u => u.ReceivedReports)
                        .HasForeignKey(s => s.RecipientId).WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationReportSharingRelation>()
                        .HasRequired(s => s.ReportOwner)
                        .WithMany(u => u.SendedReports)
                        .HasForeignKey(s => s.ReportOwnerId).WillCascadeOnDelete(false);


        }

        private void Shift_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Shift>()
                        .HasRequired(shift => shift.Rotation)
                        .WithMany(rotation => rotation.RotationShifts)
                        .HasForeignKey(shift => shift.RotationId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<Shift>()
                        .HasOptional(shift => shift.ShiftRecipient)
                        .WithMany()
                        .HasForeignKey(s => s.ShiftRecipientId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Shift>()
                        .HasMany(shift => shift.HandoverToShifts)
                        .WithMany(shift => shift.HandoverFromShifts).
                        Map(m =>
                        {
                            m.MapLeftKey("ShiftId");
                            m.MapRightKey("RelatedShiftId");
                            m.ToTable("ShiftsRelations");
                        });
        }

        private void Position_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Position
            modelBuilder.Entity<Position>()
                        .HasRequired(position => position.Workplace)
                        .WithMany(workplace => workplace.Positions)
                        .HasForeignKey(position => position.WorkplaceId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<Position>()
                        .HasRequired(position => position.Template)
                        .WithMany(template => template.Positions)
                        .HasForeignKey(position => position.TemplateId)
                        .WillCascadeOnDelete(true);
        }

        private void TemplateModule_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Template Module
            modelBuilder.Entity<TemplateModule>()
                        .HasOptional(templateModule => templateModule.Template)
                        .WithMany(template => template.Modules)
                        .HasForeignKey(templateModule => templateModule.TemplateId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<TemplateModule>()
                        .HasOptional(templateModule => templateModule.ParentModule)
                        .WithMany(templateModule => templateModule.ChildModules)
                        .HasForeignKey(templateModule => templateModule.ParentModuleId)
                        .WillCascadeOnDelete(false);


            //Template Topic Group
            modelBuilder.Entity<TemplateTopicGroup>()
                        .HasRequired(templateTopicGroup => templateTopicGroup.Module)
                        .WithMany(templateModule => templateModule.TopicGroups)
                        .HasForeignKey(templateTopicGroup => templateTopicGroup.ModuleId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<TemplateTopicGroup>()
                        .HasOptional(templateTopicGroup => templateTopicGroup.ParentTopicGroup)
                        .WithMany(templateTopicGroup => templateTopicGroup.ChildTopicGroups)
                        .HasForeignKey(templateTopicGroup => templateTopicGroup.ParentTopicGroupId)
                        .WillCascadeOnDelete(false);


            //Template Topic
            modelBuilder.Entity<TemplateTopic>()
                        .HasRequired(templateTopic => templateTopic.TopicGroup)
                        .WithMany(templateTopicGroup => templateTopicGroup.Topics)
                        .HasForeignKey(templateTopic => templateTopic.TopicGroupId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<TemplateTopic>()
                        .HasOptional(templateTopic => templateTopic.ParentTopic)
                        .WithMany(templateTopic => templateTopic.ChildTopics)
                        .HasForeignKey(templateTopic => templateTopic.ParentTopicId)
                        .WillCascadeOnDelete(false);


            //Template Task
            modelBuilder.Entity<TemplateTask>()
                        .HasRequired(templateTask => templateTask.Topic)
                        .WithMany(templateTopic => templateTopic.Tasks)
                        .HasForeignKey(templateTask => templateTask.TopicId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<TemplateTask>()
                        .HasOptional(templateTask => templateTask.ParentTask)
                        .WithMany(templateTask => templateTask.ChildTasks)
                        .HasForeignKey(templateTask => templateTask.ParentTaskId)
                        .WillCascadeOnDelete(false);

        }

        private void RotationModule_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Rotation Module
            modelBuilder.Entity<RotationModule>()
                        .HasOptional(rotationModule => rotationModule.Rotation)
                        .WithMany(rotation => rotation.RotationModules)
                        .HasForeignKey(rotationModule => rotationModule.RotationId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<RotationModule>()
                        .HasOptional(rotationModule => rotationModule.Shift)
                        .WithMany(shift => shift.RotationModules)
                        .HasForeignKey(rotationModule => rotationModule.ShiftId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationModule>()
                        .HasOptional(rotationModule => rotationModule.TemplateModule)
                        .WithMany(templateModule => templateModule.ChildRotationModules)
                        .HasForeignKey(rotationModule => rotationModule.TempateModuleId)
                        .WillCascadeOnDelete(false);


            //Rotation Topic Group
            modelBuilder.Entity<RotationTopicGroup>()
                        .HasRequired(rotationTopicGroup => rotationTopicGroup.RotationModule)
                        .WithMany(rotationModule => rotationModule.RotationTopicGroups)
                        .HasForeignKey(rotationTopicGroup => rotationTopicGroup.RotationModuleId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<RotationTopicGroup>()
                        .HasOptional(rotationTopicGroup => rotationTopicGroup.TempateTopicGroup)
                        .WithMany(templateTopicGroup => templateTopicGroup.ChildRotationTopicGroups)
                        .HasForeignKey(rotationTopicGroup => rotationTopicGroup.TempateTopicGroupId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTopicGroup>()
                        .HasOptional(rotationTopicGroup => rotationTopicGroup.ParentRotationTopicGroup)
                        .WithMany(rotationTopicGroup => rotationTopicGroup.ChildRotationTopicGroups)
                        .HasForeignKey(rotationTopicGroup => rotationTopicGroup.ParentRotationTopicGroupId)
                        .WillCascadeOnDelete(false);


            //Rotation Topic
            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.AssignedTo)
                        .WithMany()
                        .HasForeignKey(rotationTopic => rotationTopic.AssignedToId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.Location)
                        .WithMany(location => location.RotationTopics)
                        .HasForeignKey(rotationTopic => rotationTopic.LocationId)
                        .WillCascadeOnDelete(false);


            //Topic Relation
            modelBuilder.Entity<RotationTopic>()
                        .HasRequired(rotationTopic => rotationTopic.RotationTopicGroup)
                        .WithMany(rotationTopicGroup => rotationTopicGroup.RotationTopics)
                        .HasForeignKey(rotationTopic => rotationTopic.RotationTopicGroupId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.TempateTopic)
                        .WithMany(tempateTopic => tempateTopic.ChildRotationTopics)
                        .HasForeignKey(rotationTopic => rotationTopic.TempateTopicId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.ParentTopic)
                        .WithMany(rotationTopic => rotationTopic.ChildTopics)
                        .HasForeignKey(rotationTopic => rotationTopic.ParentTopicId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.AncestorTopic)
                        .WithMany()
                        .HasForeignKey(rotationTopic => rotationTopic.AncestorTopicId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.SuccessorTopic)
                        .WithMany()
                        .HasForeignKey(rotationTopic => rotationTopic.SuccessorTopicId)
                        .WillCascadeOnDelete(false);

            //Fork Relation
            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.ForkParentTopic)
                        .WithMany(rotationTopic => rotationTopic.ForkChildTopics)
                        .HasForeignKey(rotationTopic => rotationTopic.ForkParentTopicId)
                        .WillCascadeOnDelete(false);


            //Sharing Relation
            modelBuilder.Entity<RotationTopic>()
                        .HasOptional(rotationTopic => rotationTopic.ShareSourceTopic)
                        .WithMany(rotationTopic => rotationTopic.SharedTopics)
                        .HasForeignKey(rotationTopic => rotationTopic.ShareSourceTopicId)
                        .WillCascadeOnDelete(false);


            //Topic Sharing Relation
            modelBuilder.Entity<RotationTopicSharingRelation>()
                        .HasRequired(topicSharingRelation => topicSharingRelation.SourceTopic)
                        .WithMany(rotationTopic => rotationTopic.TopicSharingRelations)
                        .HasForeignKey(topicSharingRelation => topicSharingRelation.SourceTopicId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTopicSharingRelation>()
                        .HasOptional(topicSharingRelation => topicSharingRelation.DestinationTopic)
                        .WithMany()
                        .HasForeignKey(topicSharingRelation => topicSharingRelation.DestinationTopicId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<RotationTopicSharingRelation>()
                        .HasOptional(topicSharingRelation => topicSharingRelation.Recipient)
                        .WithMany()
                        .HasForeignKey(topicSharingRelation => topicSharingRelation.RecipientId)
                        .WillCascadeOnDelete(false);


            //Rotation Task
            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.AssignedTo)
                        .WithMany()
                        .HasForeignKey(rotationTask => rotationTask.AssignedToId)
                        .WillCascadeOnDelete(false);

            //Task Relation
            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.RotationTopic)
                        .WithMany(rotationTopic => rotationTopic.RotationTasks)
                        .HasForeignKey(rotationTask => rotationTask.RotationTopicId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.TemplateTask)
                        .WithMany(templateTask => templateTask.ChildRotationTasks)
                        .HasForeignKey(rotationTask => rotationTask.TemplateTaskId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.ParentTask)
                        .WithMany(rotationTask => rotationTask.ChildTasks)
                        .HasForeignKey(rotationTask => rotationTask.ParentTaskId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.AncestorTask)
                        .WithMany()
                        .HasForeignKey(rotationTask => rotationTask.AncestorTaskId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.SuccessorTask)
                        .WithMany()
                        .HasForeignKey(rotationTask => rotationTask.SuccessorTaskId)
                        .WillCascadeOnDelete(false);

            //Fork Relation
            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.ForkParentTask)
                        .WithMany(rotationTask => rotationTask.ForkChildTasks)
                        .HasForeignKey(rotationTask => rotationTask.ForkParentTaskId)
                        .WillCascadeOnDelete(false);


            //Task Board Relation
            modelBuilder.Entity<RotationTask>()
                        .HasOptional(rotationTask => rotationTask.TaskBoard)
                        .WithMany(taskBoard => taskBoard.Tasks)
                        .HasForeignKey(rotationTask => rotationTask.TaskBoardId);


        }

        private void Log_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<RotationTaskLog>()
                        .HasRequired(log => log.RootTask)
                        .WithMany(task => task.Logs)
                        .HasForeignKey(log => log.RootTaskId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<RotationTopicLog>()
                .HasRequired(log => log.RootTopic)
                .WithMany(topic => topic.Logs)
                .HasForeignKey(log => log.RootTopicId)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<RotationTopicLog>()
                .HasOptional(log => log.User)
                .WithMany()
                .HasForeignKey(log => log.UserId)
                .WillCascadeOnDelete(false);
        }

        private void SafetyMessage_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Bank Of Icon Item
            modelBuilder.Entity<BankOfIconItem>().ToTable("SafetyBankOfIcons");

            modelBuilder.Entity<BankOfIconItem>()
                        .HasRequired(bankOfIconItem => bankOfIconItem.Image)
                        .WithMany()
                        .HasForeignKey(bankOfIconItem => bankOfIconItem.ImageId)
                        .WillCascadeOnDelete(false);


            //Major Hazard  
            modelBuilder.Entity<MajorHazard>().ToTable("SafetyMajorHazards");

            modelBuilder.Entity<MajorHazard>()
                        .HasRequired(majorHazard => majorHazard.Icon)
                        .WithMany()
                        .HasForeignKey(majorHazard => majorHazard.IconId)
                        .WillCascadeOnDelete(false);


            //Critical Control
            modelBuilder.Entity<CriticalControl>().ToTable("SafetyCriticalControls");

            modelBuilder.Entity<CriticalControl>()
                        .HasRequired(criticalControl => criticalControl.MajorHazard)
                        .WithMany(majorHazard => majorHazard.CriticalControls)
                        .HasForeignKey(criticalControl => criticalControl.MajorHazardId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<CriticalControl>()
                        .HasMany(criticalControl => criticalControl.Teams)
                        .WithMany(team => team.CriticalControls)
                        .Map(model =>
                        {
                            model.MapLeftKey("CriticalControlId");
                            model.MapRightKey("TeamId");
                            model.ToTable("SafetyCriticalControlTeamRelations");
                        });


            //Safety Message
            modelBuilder.Entity<CriticalControl>().ToTable("SafetyCriticalControls");

            modelBuilder.Entity<SafetyMessage>()
                        .HasRequired(safetyMessage => safetyMessage.CriticalControl)
                        .WithMany(criticalControl => criticalControl.SafetyMessages)
                        .HasForeignKey(safetyMessage => safetyMessage.CriticalControlId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<SafetyMessage>()
                        .HasMany(safetyMessage => safetyMessage.Teams)
                        .WithMany(team => team.SafetyMessages)
                        .Map(model =>
                        {
                            model.MapLeftKey("SafetyMessageId");
                            model.MapRightKey("TeamId");
                            model.ToTable("SafetyMessageTeamRelations");
                        });


            modelBuilder.Entity<SafetyMessage>()
                        .HasMany(safetyMessage => safetyMessage.Users)
                        .WithMany()
                        .Map(model =>
                        {
                            model.MapLeftKey("SafetyMessageId");
                            model.MapRightKey("UserId");
                            model.ToTable("SafetyMessageUserRelations");
                        });
        }

        private void SafetyMessageV2_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Major Hazard
            modelBuilder.Entity<MajorHazardV2>().ToTable("SafetyV2MajorHazards");

            modelBuilder.Entity<MajorHazardV2>()
                        .HasOptional(majorHazardV2 => majorHazardV2.Owner)
                        .WithMany(usersProfile => usersProfile.MajorHazardsOwnerV2)
                        .HasForeignKey(majorHazardV2 => majorHazardV2.OwnerId)
                        .WillCascadeOnDelete(false);


            //Critical Control
            modelBuilder.Entity<CriticalControlV2>().ToTable("SafetyV2CriticalControls");

            modelBuilder.Entity<CriticalControlV2>()
                        .HasOptional(criticalControlV2 => criticalControlV2.Owner)
                        .WithMany(usersProfile => usersProfile.CriticalControlsOwnerV2)
                        .HasForeignKey(criticalControlV2 => criticalControlV2.OwnerId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<CriticalControlV2>()
                        .HasRequired(criticalControlV2 => criticalControlV2.MajorHazard)
                        .WithMany(majorHazardV2 => majorHazardV2.CriticalControls)
                        .HasForeignKey(criticalControlV2 => criticalControlV2.MajorHazardId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<CriticalControlV2>()
                        .HasMany(criticalControlV2 => criticalControlV2.Champions)
                        .WithMany(usersProfile => usersProfile.ChampionOfCriticalControlsV2)
                        .Map(model =>
                        {
                            model.MapLeftKey("CriticalControlId");
                            model.MapRightKey("UserId");
                            model.ToTable("SafetyV2CriticalControlChampions");
                        });


            //Safety Message
            modelBuilder.Entity<SafetyMessageV2>().ToTable("SafetyV2Messages");

            modelBuilder.Entity<SafetyMessageV2>()
                        .HasRequired(safetyMessageV2 => safetyMessageV2.CriticalControl)
                        .WithMany(criticalControlV2 => criticalControlV2.SafetyMessages)
                        .HasForeignKey(safetyMessageV2 => safetyMessageV2.CriticalControlId)
                        .WillCascadeOnDelete(true);


            //Safety Stat
            modelBuilder.Entity<SafetyStatV2>().ToTable("SafetyV2Stats");

            modelBuilder.Entity<SafetyStatV2>()
                        .HasRequired(safetyStatV2 => safetyStatV2.SafetyMessage)
                        .WithMany(safetyMessageV2 => safetyMessageV2.SafetyStats)
                        .HasForeignKey(safetyStatV2 => safetyStatV2.SafetyMessageId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<SafetyStatV2>()
                        .HasRequired(safetyStatV2 => safetyStatV2.Recipient)
                        .WithMany(usersProfile => usersProfile.SafetyMessageStatsV2)
                        .HasForeignKey(safetyStatV2 => safetyStatV2.RecipientId)
                        .WillCascadeOnDelete(false);
        }

        private void Project_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Project
            modelBuilder.Entity<Project>()
                        .HasRequired(project => project.Creator)
                        .WithMany(userProfile => userProfile.CreatedProjects)
                        .HasForeignKey(project => project.CreatorId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<Project>()
                        .HasRequired(project => project.ProjectManager)
                        .WithMany(userProfile => userProfile.ManagedProjects)
                        .HasForeignKey(project => project.ProjectManagerId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<Project>()
                     .HasMany(project => project.ProjectFollowers)
                     .WithMany(userProfile => userProfile.FollowingProjects)
                     .Map(model =>
                     {
                         model.MapLeftKey("ProjectId");
                         model.MapRightKey("UserId");
                         model.ToTable("ProjectFollowers");
                     });

            modelBuilder.Entity<Project>()
                .HasMany(project => project.Tasks)
                .WithOptional(task => task.Project)
                .HasForeignKey(project => project.ProjectId)
                .WillCascadeOnDelete(false);

        }

        private void Notification_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Notification
            modelBuilder.Entity<Notification>()
                        .HasRequired(notification => notification.Recipient)
                        .WithMany(userProfile => userProfile.Notifications)
                        .HasForeignKey(notification => notification.RecipientId)
                        .WillCascadeOnDelete(true);
        }

        private void Attachments_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Attachment
            modelBuilder.Entity<Attachment>()
                        .HasRequired(attachment => attachment.File)
                        .WithMany()
                        .HasForeignKey(attachment => attachment.FileId)
                        .WillCascadeOnDelete(false);

            modelBuilder.Entity<Attachment>()
                        .HasOptional(attachment => attachment.RotationTopic)
                        .WithMany(rotationTopic => rotationTopic.Attachments)
                        .HasForeignKey(attachment => attachment.RotationTopicId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<Attachment>()
                        .HasOptional(attachment => attachment.RotationTask)
                        .WithMany(rotationTask => rotationTask.Attachments)
                        .HasForeignKey(attachment => attachment.RotationTaskId)
                        .WillCascadeOnDelete(true);
        }

        private void AttachmentsLink_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Attachment

            modelBuilder.Entity<AttachmentsLink>()
                        .HasOptional(attachment => attachment.RotationTopic)
                        .WithMany(rotationTopic => rotationTopic.AttachmentsLink)
                        .HasForeignKey(attachment => attachment.RotationTopicId)
                        .WillCascadeOnDelete(true);

            modelBuilder.Entity<AttachmentsLink>()
                        .HasOptional(attachment => attachment.RotationTask)
                        .WithMany(rotationTask => rotationTask.AttachmentsLink)
                        .HasForeignKey(attachment => attachment.RotationTaskId)
                        .WillCascadeOnDelete(true);
        }


        private void ManagerComments_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Manager Comments
            modelBuilder.Entity<Models.ManagerComments>()
                        .HasOptional(managerComments => managerComments.RotationTopic)
                        .WithMany(rotationTopic => rotationTopic.ManagerComment)
                        .HasForeignKey(managerComments => managerComments.RotationTopicId)
                        .WillCascadeOnDelete(true);
        }

        private void VoiceMessages_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Voice Message
            modelBuilder.Entity<VoiceMessage>()
                        .HasRequired(voiceMessage => voiceMessage.File)
                        .WithMany()
                        .HasForeignKey(voiceMessage => voiceMessage.FileId)
                        .WillCascadeOnDelete(false);


            modelBuilder.Entity<VoiceMessage>()
                        .HasOptional(voiceMessage => voiceMessage.RotationTopic)
                        .WithMany(rotationTopic => rotationTopic.VoiceMessages)
                        .HasForeignKey(voiceMessage => voiceMessage.RotationTopicId)
                        .WillCascadeOnDelete(true);


            modelBuilder.Entity<VoiceMessage>()
                        .HasOptional(voiceMessage => voiceMessage.RotationTask)
                        .WithMany(rotationTask => rotationTask.VoiceMessages)
                        .HasForeignKey(voiceMessage => voiceMessage.RotationTaskId)
                        .WillCascadeOnDelete(true);
        }

        private void AdminSettings_EntitiesConfiguration(DbModelBuilder modelBuilder)
        {
            //Admin Settings
            modelBuilder.Entity<AdminSettings>()
                .HasOptional(adminSettings => adminSettings.Logo)
                .WithMany()
                .HasForeignKey(adminSettings => adminSettings.LogoId)
                .WillCascadeOnDelete(true);


            modelBuilder.Entity<AdminSettings>()
                .HasOptional(adminSettings => adminSettings.Logo)
                .WithMany()
                .HasForeignKey(adminSettings => adminSettings.LogoId)
                .WillCascadeOnDelete(false);
        }

        #endregion

        public override int SaveChanges()
        {
            try
            {
                foreach (DbEntityEntry entity in ChangeTracker.Entries().Where(e => e.Entity is BaseModel))
                {
                    SetCreatedAndModified(entity);
                }

                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                var fullErrorMessage =
                    string.Join("; ", errorMessages);

                var exceptionMessage =
                    string.Concat(
                    ex.Message, " The validation errors are: ", fullErrorMessage);

                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        private static void SetCreatedAndModified(DbEntityEntry entity)
        {
            if (entity.Entity is BaseModel)
            {
                BaseModel baseModel = (BaseModel)entity.Entity;

                if (entity.State == EntityState.Added)
                {
                    baseModel.CreatedUtc = DateTime.UtcNow;

                    if (baseModel.Id == Guid.Empty)
                        baseModel.Id = Guid.NewGuid();
                }

                if (entity.State == EntityState.Modified)
                {
                    baseModel.ModifiedUtc = DateTime.UtcNow;
                }
            }
        }
    }
}