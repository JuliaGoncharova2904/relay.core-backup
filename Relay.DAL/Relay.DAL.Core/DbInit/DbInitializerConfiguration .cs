﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Configuration;
//using EFCache;

namespace MomentumPlus.Core.DataSource.DbInit
{
    public class DbInitializerConfiguration : DbMigrationsConfiguration<MomentumContext>
    {
        public DbInitializerConfiguration()
        {
         
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;

            SetSqlGenerator("System.Data.SqlClient", new AddColumnIfNotExistsSqlGenerator());

        }

        protected override void Seed(MomentumContext context)
        {
            InitializeAdminSettings(context);

            InitializeSafetyV2(context);

            var seedDatabase = bool.Parse(WebConfigurationManager.AppSettings["seedDatabase"]);

            if (seedDatabase)
            {
                var isMultiTenant = bool.Parse(WebConfigurationManager.AppSettings["isMultiTenant"]);

                InitializeCompanies(context, isMultiTenant);

                InitializeWorkPatterns(context);

                InitializeWorkplaces(context);

                InitializeTemplates(context);

                InitializePositions(context);

                InitializeTeams(context);

                InitializeBaseModules(context);

                InitializeUserProfiles(context);
            }

            InitializeTaskBoard(context);

            context.SaveChanges();
        }

        #region Init methods

        private static void InitializeAdminSettings(MomentumContext context)
        {
            var adminSetting = context.AdminSettings.Where(s => s.Id == DbInitializerConstants.AdminSetting.SettingsId);

            if (!adminSetting.Any())
            {
                context.AdminSettings.AddOrUpdate(s => s.Id, new AdminSettings
                {
                    Id = DbInitializerConstants.AdminSetting.SettingsId,
                    PlanType = TypeOfPlan.Enterprise,
                    SupportType = TypeOfSupport.Premium,
                    HandoverPreviewType = TypeOfHandoverPreview.PenultimateDayOfSwing,
                    HandoverTriggerType = TypeOfHandoverTrigger.FinalDayOfSwing,
                    HostingProvider = "Azure",
                    TimeZoneId = "GMT Standard Time",
                    HandoverLimit = 100,
                    TemplateLimit = 50,
                    TwilioSettings = new TwilioSettings()
                });
            }
        }

        private static void InitializeTaskBoard(MomentumContext context)
        {
            List<Guid> usersIds = context.UsersProfiles.Select(u => u.Id).ToList();

            foreach (Guid id in usersIds)
            {
                if (!context.TaskBoards.Any(tb => tb.Id == id))
                {
                    context.TaskBoards.Add(new TaskBoard { Id = id });
                }
            }

            context.SaveChanges();
        }

        private static void InitializeSafetyV2(MomentumContext context)
        {
            foreach (MajorHazardV2 majorHazard in DbInitializerConstants.SafetyMessageV2.MajorHazards())
            {
                if (!context.SafetyV2MajorHazards.Any(mh => mh.Id == majorHazard.Id))
                {
                    context.SafetyV2MajorHazards.Add(majorHazard);
                }
            }
        }

        private static void InitializeWorkPatterns(MomentumContext context)
        {
            RotationPattern[] workPatterns = {
                new RotationPattern { Id = DbInitializerConstants.WorkPatterns.FiveWorkTwoOff, DayOff = 2 , DayOn = 5},
                new RotationPattern { Id = DbInitializerConstants.WorkPatterns.NineWorkFiveOff,DayOff = 5 , DayOn = 9 }
            };

            context.RotationPatterns.AddOrUpdate(p => p.Id, workPatterns);
        }

        private static void InitializeWorkplaces(MomentumContext context)
        {

            Workplace[] workPlaces = {
                new Workplace { Id = DbInitializerConstants.Workplaces.iHandoverWorkplaceId , Name = "iHandover Workplace" }
            };

            context.Workplaces.AddOrUpdate(w => w.Id, workPlaces);
        }

        private static void InitializeTemplates(MomentumContext context)
        {
            Template[] templates = {
                new Template { Id = DbInitializerConstants.Templates.iHandoverAdminTemplateId,
                    Name = "iHandover Admin",
                    Description = "Template for iHandover Admin"},
            };

            context.Templates.AddOrUpdate(r => r.Id, templates);
        }


        private static void InitializePositions(MomentumContext context)
        {
            Position[] positions = {
                 new Position {
                    Id = DbInitializerConstants.Positions.iHandoverAdminPositionId,
                    Name = "iHandover Admin",
                    WorkplaceId = DbInitializerConstants.Workplaces.iHandoverWorkplaceId,
                    TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId }
            };

            context.Positions.AddOrUpdate(r => r.Id, positions);
        }

        private static void InitializeCompanies(MomentumContext context, bool isMultiTenant)
        {
            Company[] companies = {
                new Company  { Id = DbInitializerConstants.Companies.iHandoverCompanyId, Name = "iHandover", IsDefault = !isMultiTenant}
            };

            //sql 


            context.Companies.AddOrUpdate(c => c.Id, companies);
        }

        private static void InitializeTeams(MomentumContext context)
        {
            Team[] teams = {
                new Team { Id = DbInitializerConstants.Teams.iHandoverTeamId , Name = "iHandover" }
            };

            context.Teams.AddOrUpdate(t => t.Id, teams);
        }

        private static void InitializeBaseModules(MomentumContext context)
        {
            TemplateModule[] baseModules = {
                new TemplateModule { Id = DbInitializerConstants.BaseModules.HSEModuleId, Type = TypeOfModule.HSE, Enabled = true },
                new TemplateModule { Id = DbInitializerConstants.BaseModules.CoreModuleId, Type = TypeOfModule.Core, Enabled = true },
                new TemplateModule { Id = DbInitializerConstants.BaseModules.TeamModuleId, Type = TypeOfModule.Team, Enabled = true },
                new TemplateModule { Id = DbInitializerConstants.BaseModules.ProjectModuleId, Type = TypeOfModule.Project, Enabled = true }
            };

            TemplateModule[] iHandoverAdminTemplateModules = {
                new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.HSEModuleId, Type = TypeOfModule.HSE, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.HSEModuleId},
                new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.CoreModuleId, Type = TypeOfModule.Core, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.CoreModuleId},
                new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.TeamModuleId, Type = TypeOfModule.Team, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.TeamModuleId},
                new TemplateModule { Id = DbInitializerConstants.iHandoverAdminModules.ProjectModuleId, Type = TypeOfModule.Project, Enabled = true, TemplateId = DbInitializerConstants.Templates.iHandoverAdminTemplateId, ParentModuleId = DbInitializerConstants.BaseModules.ProjectModuleId}
            };

            context.TemplateModules.AddOrUpdate(m => m.Id, iHandoverAdminTemplateModules);

            context.TemplateModules.AddOrUpdate(m => m.Id, baseModules);
        }

        private static void InitializeUserProfiles(MomentumContext context)
        {
            UserProfile[] users = {
                new UserProfile
                {
                    Id = DbInitializerConstants.Users.iHandoverAdminUserId,
                    UserName = "admin@ihandover.co",
                    Email = "admin@ihandover.co",
                    PositionId = DbInitializerConstants.Positions.iHandoverAdminPositionId,
                    TeamId = DbInitializerConstants.Teams.iHandoverTeamId,
                    RotationPatternId =   DbInitializerConstants.WorkPatterns.FiveWorkTwoOff,
                    CompanyId =  DbInitializerConstants.Companies.iHandoverCompanyId,
                    FirstName = "Admin",
                    LastName =  "iHandover",
                    CreatedUtc = DateTime.UtcNow
                },
            };

            context.UsersProfiles.AddOrUpdate(c => c.Id, users);

            InitializeTaskBoard(context);
        }

        #endregion
    }
}
