﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Core.DataSource
{
    public static class DbInitializerConstants
    {
        public static class SafetyMessageV2
        {
            public static MajorHazardV2[] MajorHazards()
            {
                return new[] {
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA46D"), Name = "Confine Space Entries", IconPath = "/Content/img/safety-v2/ConfineSpaceEntries.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA47D"), Name = "Electricity", IconPath = "/Content/img/safety-v2/Electricity.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA48D"), Name = "Equipment Safeguarding", IconPath = "/Content/img/safety-v2/EquipmentSafeguarding.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA49D"), Name = "Explosives", IconPath = "/Content/img/safety-v2/Explosives.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA50D"), Name = "Hazardous Materials", IconPath = "/Content/img/safety-v2/HazardousMaterials.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA51D"), Name = "Heavy Mobile Equipment", IconPath = "/Content/img/safety-v2/HeavyMobileEquipment.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA52D"), Name = "Inrush", IconPath = "/Content/img/safety-v2/Inrush.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA53D"), Name = "Isolations", IconPath = "/Content/img/safety-v2/Isolations.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA54D"), Name = "Lifting Operations", IconPath = "/Content/img/safety-v2/LiftingOperations.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA55D"), Name = "Light Vehicles", IconPath = "/Content/img/safety-v2/LightVehicles.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA56D"), Name = "Pit Wall Failure", IconPath = "/Content/img/safety-v2/PitWallFailure.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA57D"), Name = "Pressure Equipment", IconPath = "/Content/img/safety-v2/PressureEquipment.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA58D"), Name = "Rules Of Life", IconPath = "/Content/img/safety-v2/RulesOfLife.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA59D"), Name = "UG Ground Control", IconPath = "/Content/img/safety-v2/UGGroundControl.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA60D"), Name = "Underground Ventilation", IconPath = "/Content/img/safety-v2/UndergroundVentilation.svg" },
                    new MajorHazardV2 { Id = new Guid("22D4F4C1-FB22-4DBC-AD84-0555015DA61D"), Name = "Work At Height", IconPath = "/Content/img/safety-v2/WorkAtHeight.svg" }
                };
            }
        }


        public static class Users
        {
            public static readonly Guid iHandoverAdminUserId = new Guid("D18E8B72-1756-4CC4-BBED-855153269071");

            public static readonly Guid iHandoverLineManagerUserId = new Guid("D18E8B72-1756-4CC4-BBED-855153269074");

        }

        public static class AdminSetting
        {
            public static readonly Guid SettingsId = new Guid("D18E8B72-1756-4CC4-BBED-855153266666");
        }


        public static class Roles
        {
            public static readonly Guid iHandoverAdminRoleId = new Guid("AB66F022-A24B-4780-AE01-284C56C7DC69");

            public static readonly Guid AdminRoleRoleId = new Guid("29D3D119-00B3-4B17-8BDB-C4E64D753465");

            public static readonly Guid HeadLineManagerRoleId = new Guid("4AA14D72-AED5-4A3C-948B-E59964FD858A");

            public static readonly Guid LineManagerRoleId = new Guid("E334244D-F8D9-4A42-86DF-E1D11927182D");

            public static readonly Guid SafetyManagerRoleId = new Guid("AB66F022-A24B-2780-AE01-284C56C7DC19");

            public static readonly Guid ContributorRoleId = new Guid("AB66F022-A24B-4780-AE01-284C56C7DC12");

            public static readonly Guid SimpleUserRoleId = new Guid("AB66F022-A24B-4780-AE01-284C56C7DC19");
        }

        public static class Templates
        {
            public static readonly Guid iHandoverAdminTemplateId = new Guid("D18E8B72-1756-4CC4-BBED-855153269071");

            public static readonly Guid iHandoverLineManagerTemplateId = new Guid("D18E8B72-1756-4CC4-BBED-855153269074");
        }


        public static class UserRoles
        {
            public static readonly Guid iHandoverAdminUserRoleId = new Guid("D18E8B72-1756-7CC7-BBED-855153269071");

            public static readonly Guid iHandoverLineManagerRoleId = new Guid("D18E8B72-1756-2CC9-BBED-855153269074");
        }

        public static class BaseModules
        {
            public static readonly Guid HSEModuleId = new Guid("D18E8B72-1756-4CC4-BBED-855153269071");

            public static readonly Guid CoreModuleId = new Guid("D18E8B72-1756-4CC4-BBED-855153269072");

            public static readonly Guid TeamModuleId = new Guid("D18E8B72-1756-4CC4-BBED-855153269073");

            public static readonly Guid ProjectModuleId = new Guid("D18E8B72-1756-4CC4-BBED-855153269074");
        }

        public static class iHandoverAdminModules
        {
            public static readonly Guid HSEModuleId = new Guid("118E8B72-1756-4CC4-BBED-855153269071");

            public static readonly Guid CoreModuleId = new Guid("218E8B72-1756-4CC4-BBED-855153269072");

            public static readonly Guid TeamModuleId = new Guid("318E8B72-1756-4CC4-BBED-855153269073");

            public static readonly Guid ProjectModuleId = new Guid("418E8B72-1756-4CC4-BBED-855153269074");
        }

        public static class iHandoverLineManagerModules
        {
            public static readonly Guid HSEModuleId = new Guid("128E8B72-1756-4CC4-BBED-855153269071");

            public static readonly Guid CoreModuleId = new Guid("238E8B72-1756-4CC4-BBED-855153269072");

            public static readonly Guid TeamModuleId = new Guid("348E8B72-1756-4CC4-BBED-855153269073");

            public static readonly Guid ProjectModuleId = new Guid("458E8B72-1756-4CC4-BBED-855153269074");
        }


        public static class Positions
        {
            public static readonly Guid iHandoverAdminPositionId = new Guid("7E0F3C35-C0B6-45E0-BD2B-A93372FE069D");

            public static readonly Guid iHandoverLineManagerPositionId = new Guid("14385630-54A6-4396-928A-0D176B0DCDE8");
        }

        public static class Workplaces
        {
            public static readonly Guid iHandoverWorkplaceId = new Guid("D28E8B72-1756-6CC6-BBED-855153269071");

        }

        public static class Companies
        {
            public static readonly Guid iHandoverCompanyId = new Guid("D13E8B72-1756-6CC6-BBED-855153269071");
        }

        public static class Teams
        {
            public static readonly Guid iHandoverTeamId = new Guid("D13E8B72-1756-6CC6-BBED-855153269071");
        }

        public static class WorkPatterns
        {
            public static readonly Guid FiveWorkTwoOff = new Guid("D13E8B72-1756-6CC6-BBED-855153269077");

            public static readonly Guid NineWorkFiveOff = new Guid("D18E1B72-1756-6CC6-BBED-855153269071");
        }
    }
}
