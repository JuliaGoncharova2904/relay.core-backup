﻿using System;

namespace MomentumPlus.Core.Interfaces
{
    public interface ICacheRepository<TEntity> where TEntity : class
    {
        TEntity Get(Guid id);

        void Delete(Guid id);

        void Update(TEntity entity);

        bool Add(TEntity entity);
    }
}
