﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Core.Interfaces
{
    public enum EntityAccess
    {
        All = 0,
        NoDeleted = 1,
        Deleted = 2
    }
}
