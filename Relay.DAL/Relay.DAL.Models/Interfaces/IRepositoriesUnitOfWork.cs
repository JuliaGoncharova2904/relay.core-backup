﻿using MomentumPlus.Core.Models;

namespace MomentumPlus.Core.Interfaces
{
    public interface IRepositoriesUnitOfWork
    {
        IGenericRepository<UserProfile> UserProfileRepository { get; }

        IGenericRepository<Attachment> AttachmentRepository { get; }

        IGenericRepository<AttachmentsLink> AttachmentsLinkRepository { get; }

        IGenericRepository<ManagerComments> ManagerCommentsRepository { get; }

        IGenericRepository<File> FileRepository { get; }

        IGenericRepository<Company> CompanyRepository { get; }

        IGenericRepository<Position> PositionRepository { get;  }
        IGenericRepository<VoiceMessage> VoiceMessageRepository { get; }
        IGenericRepository<Workplace> WorkplaceRepository { get; }

        IGenericRepository<Team> TeamRepository { get; }
        IGenericRepository<RotationPattern> RotationPatternRepository { get; }
        IGenericRepository<Rotation> RotationRepository { get; }

        IGenericRepository<Template> TemplateRepository { get; }

        IGenericRepository<TemplateModule> TemplateModuleRepository { get; }
        IGenericRepository<TemplateTopicGroup> TemplateTopicGroupRepository { get; }
        IGenericRepository<TemplateTopic> TemplateTopicRepository { get; }
        IGenericRepository<TemplateTask> TemplateTaskRepository { get; }

        IGenericRepository<RotationModule> RotationModuleRepository { get; }
        IGenericRepository<RotationTopicGroup> RotationTopicGroupRepository { get; }
        IGenericRepository<RotationTopic> RotationTopicRepository { get; }
        IGenericRepository<RotationTask> RotationTaskRepository { get; }

        IGenericRepository<Location> LocationRepository { get; }

        // Safety
        IGenericRepository<MajorHazard> MajorHazardRepository { get; }
        IGenericRepository<CriticalControl> CriticalControlRepository { get; }
        IGenericRepository<SafetyMessage> SafetyMessageRepository { get; }
        IGenericRepository<BankOfIconItem> BankOfIconRepository { get; }

        IGenericRepository<MajorHazardV2> MajorHazardV2Repository { get; }
        IGenericRepository<CriticalControlV2> CriticalControlV2Repository { get; }
        IGenericRepository<SafetyMessageV2> SafetyMessageV2Repository { get; }
        IGenericRepository<SafetyStatV2> SafetyStatV2Repository { get; }
        //--
        // Task Board
        IGenericRepository<TaskBoard> TaskBoardRepository { get; }
        //--
        IGenericRepository<AdminSettings> AdminSettingsRepository { get; }
        IGenericRepository<Project> ProjectRepository { get; }
        IGenericRepository<Notification> NotificationRepository { get; }
        IGenericRepository<RotationTopicSharingRelation> TopicSharingRelationRepository { get; }

        IGenericRepository<Shift> ShiftRepository { get; }
        IGenericRepository<RotationReportSharingRelation> ReportSharingRelationRepository { get; }


        IGenericRepository<RotationTaskLog> TaskLogRepository { get; }

        IGenericRepository<RotationTopicLog> TopicLogRepository { get; }

        //---------------------
        int Save();
    }
}
