﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MomentumPlus.Core.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        int Count(EntityAccess access = EntityAccess.NoDeleted);

        TEntity Create();

        TEntity Attach(Guid id);

        TEntity Get(Guid id);

        IQueryable<TEntity> GetAll(EntityAccess access = EntityAccess.NoDeleted);

        IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> expression, EntityAccess access = EntityAccess.NoDeleted);

        TEntity Add(TEntity entity);

        IEnumerable<TEntity> Add(params TEntity[] entities);
        
        void Delete(Guid id);

        void Delete(TEntity entity);

        void Delete(IEnumerable<Guid> ids);

        void Delete(IEnumerable<TEntity> entities);
        
        void Update(TEntity entity);
    }
}
