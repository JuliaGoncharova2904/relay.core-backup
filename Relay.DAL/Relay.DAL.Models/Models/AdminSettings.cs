﻿using System;

namespace MomentumPlus.Core.Models
{
    public class AdminSettings : BaseModel
    {
        public DateTime? PurchaseDate { get; set; }

        public DateTime? LicenceEndDate { get; set; }

        public string DomainUrl { get; set; }

        public string TimeZoneId { get; set; }

        public int? TemplateLimit { get; set; }

        public int? HandoverLimit { get; set; }

        public string AccountManager { get; set; }

        public string Version { get; set; }

        public string HostingProvider { get; set; }

        public string FileExtensions { get; set; }

        public string AllowedFileExtensions { get; set; }

        public TypeOfPlan PlanType { get; set; }

        public TypeOfSupport SupportType { get; set; }

        public TypeOfHandoverTrigger HandoverTriggerType { get; set; }

        public string HandoverTriggerTime { get; set; }

        public TypeOfHandoverPreview HandoverPreviewType { get; set; }

        public string HandoverPreviewTime { get; set; }

        public Guid? LogoId { get; set; }

        public virtual File Logo { get; set; }

        public TwilioSettings TwilioSettings { get; set; }


    }
}
