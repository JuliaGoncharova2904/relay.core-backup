﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class Rotation : BaseModel
    {
        public Guid RotationOwnerId { get; set; }

        public virtual UserProfile RotationOwner { get; set; }

        public Guid LineManagerId { get; set; }

        public virtual UserProfile LineManager { get; set; }

        public Guid DefaultBackToBackId { get; set; }

        public virtual UserProfile DefaultBackToBack { get; set; }

        [Obsolete]
        public virtual ICollection<UserProfile> Contributors { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? ConfirmDate { get; set; }

        public int DayOn { get; set; }

        public int DayOff { get; set; }

        public int RepeatTimes { get; set; }

        public RotationState State { get; set; }

        public RotationType RotationType { get; set; }

        public virtual ICollection<RotationModule> RotationModules { get; set; }

        public Guid? PrevRotationId { get; set; }

        public virtual Rotation PrevRotation { get; set; }

        public Guid? NextRotationId { get; set; }

        public virtual Rotation NextRotation { get; set; }

        public virtual ICollection<Rotation> HandoverFromRotations{ get; set; }

        public virtual ICollection<Rotation> HandoverToRotations { get; set; }

        public virtual ICollection<Shift> RotationShifts { get; set; }
    }
}
