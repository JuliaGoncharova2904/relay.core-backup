﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Core.Models
{
    public class PlannedvsActual
    {
        public string Planned { get; set; }
        public string Actual { get; set; }
        public Guid? UnitsTypeId { get; set; }
        public List<SelectListItem> Units { get; set; }
        public string UnitsSelectedType { get; set; }
    }
}
