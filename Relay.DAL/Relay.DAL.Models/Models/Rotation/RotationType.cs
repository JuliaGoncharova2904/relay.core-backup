﻿namespace MomentumPlus.Core.Models
{
    public enum RotationType
    {
        Swing = 0,
        Shift = 1
    }
}
