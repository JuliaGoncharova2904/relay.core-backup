﻿namespace MomentumPlus.Core.Models
{
    public enum RotationState
    {
        Created = 0,
        Confirmed = 1,
        SwingEnded = 2,
        Expired = 3
    }
}
