﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class Workplace : BaseModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        [StringLength(750)]
        public string Address { get; set; }

        [StringLength(2000)]
        public string MapLink { get; set; }

        public string TimeZoneId { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
    }
}
