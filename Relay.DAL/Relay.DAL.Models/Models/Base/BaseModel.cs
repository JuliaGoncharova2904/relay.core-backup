﻿using System;

namespace MomentumPlus.Core.Models
{  
    public abstract class BaseModel
    {
        public Guid Id { get; set; }     

        public DateTime? CreatedUtc { get; set; }

        public DateTime? ModifiedUtc { get; set; }

        public DateTime? DeletedUtc { get; set; }
    }
}
