﻿using System;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace MomentumPlus.Core.Models
{
    public abstract class HandoverItem : BaseModel
    {
        protected BaseModel Clone()
        {
            return Clone<BaseModel>();
        }
        protected T Clone<T>() where T : BaseModel
        {
            Type thisType = this.GetType();
            Type cloneType = thisType;

            //Check if object is Proxy
            if (ObjectContext.GetObjectType(thisType) != thisType)
            {
                cloneType = thisType.BaseType;
            }

            var clone = (BaseModel)Activator.CreateInstance(cloneType);
            if (cloneType != null)
            {
                var cloneProperties = cloneType.GetProperties();

                foreach (var prop in cloneProperties.ToList())
                {
                    if (prop.CanWrite)
                        prop.SetValue(clone, thisType.GetProperty(prop.Name).GetValue(this, null), null);
                }
            }

            // Create new Id
            clone.Id = Guid.NewGuid();

            // Set Dates
            clone.CreatedUtc = DateTime.UtcNow;
            clone.ModifiedUtc = null;
            clone.DeletedUtc = null;

            return (T)clone;
        }
    }
}
