﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    [Serializable]
    public class File : HandoverItem
    {
        [Required]
        public byte[] BinaryData { get; set; }
        public string FileType { get; set; }

        public string ContentType { get; set; }

        public string Title { get; set; }

        public new File Clone()
        {
            var clone = Clone<File>();

            /*if(BinaryData != null)
            {
                clone.BinaryData = new byte[BinaryData.Length];
                Array.Copy(BinaryData, clone.BinaryData, BinaryData.Length);
            }*/

            return clone;
        }
    }
}
