﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class Shift : BaseModel
    {
        public DateTime? StartDateTime { get; set; }

        public int WorkMinutes { get; set; }

        public int BreakMinutes { get; set; }

        public int RepeatTimes { get; set; }

        public Guid? ShiftRecipientId { get; set; }

        public virtual UserProfile ShiftRecipient { get; set; }

        public Guid RotationId { get; set; }

        public virtual Rotation Rotation { get; set; }

        public ShiftState State { get; set; }

        public virtual ICollection<RotationModule> RotationModules { get; set; }

        public virtual ICollection<Shift> HandoverFromShifts { get; set; }

        public virtual ICollection<Shift> HandoverToShifts { get; set; }
    }
}
