﻿using System;
using MomentumPlus.Core.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MomentumPlus.Core.Models
{
    [ComplexType]
    public class TwilioSettings
    {
        [Column("TwilioAccount")]
        public string TwilioAccount { get; set; }

        [Column("TwilioEndpoint")]
        public string TwilioEndpoint { get; set; }

        [Column("TwilioPhoneNumber")]
        public string TwilioPhoneNumber { get; set; }

        [Column("SMSGateway")]
        public string SMSGateway { get; set; }

        [Column("NumberSMSPerUserPerDay")]
        public int NumberSMSPerUserPerDay { get; set; }
    }
}
