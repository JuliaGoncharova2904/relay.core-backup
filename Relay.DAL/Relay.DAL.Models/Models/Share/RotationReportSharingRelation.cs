﻿using System;

namespace MomentumPlus.Core.Models
{
    public class RotationReportSharingRelation : BaseModel
    {
        public Guid ReportId { get; set; }

        public RotationType ReportType { get; set; }

        public Guid RecipientId { get; set; }

        public virtual UserProfile Recipient { get; set; }

        public Guid ReportOwnerId { get; set; }

        public virtual UserProfile ReportOwner { get; set; }
    }
}
