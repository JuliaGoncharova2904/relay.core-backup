﻿using System;

namespace MomentumPlus.Core.Models
{
    public class RotationTopicSharingRelation : HandoverItem
    {
        public Guid SourceTopicId { get; set; }

        public virtual RotationTopic SourceTopic { get; set; }

        public Guid? DestinationTopicId { get; set; }

        public virtual RotationTopic DestinationTopic { get; set; }

        public Guid? RecipientId { get; set; }

        public virtual UserProfile Recipient { get; set; }

        public string RecipientEmail { get; set; }

        public bool? IsRecipientsFromPrevRotation { get; set; }

        public Guid? TemplateTopicId { get; set; }

        public new RotationTopicSharingRelation Clone()
        {
            var clone = base.Clone<RotationTopicSharingRelation>();
            clone.SourceTopic = null;
            clone.DestinationTopic = null;
            clone.TemplateTopicId = this.TemplateTopicId;
            clone.IsRecipientsFromPrevRotation = this.IsRecipientsFromPrevRotation;

            return clone;
        }
    }
}
