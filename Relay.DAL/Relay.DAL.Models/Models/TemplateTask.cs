﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class TemplateTask : BaseModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public PriorityOfTask Priority { get; set; }

        //public DateTime Deadline { get; set; }

        public Guid TopicId { get; set; }

        public virtual TemplateTopic Topic { get; set; }

        public Guid? ParentTaskId { get; set; }

        public virtual TemplateTask ParentTask { get; set; }

        public virtual ICollection<RotationTask> ChildRotationTasks { get; set; }

        public virtual ICollection<TemplateTask> ChildTasks { get; set; }

        public bool Enabled { get; set; }

        public bool IsNullReport { get; set; }
    }
}
