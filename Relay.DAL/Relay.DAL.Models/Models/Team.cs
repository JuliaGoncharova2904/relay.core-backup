﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class Team : BaseModel
    {
        [Required]
        public string Name { get; set; }

        public virtual ICollection<UserProfile> Users { get; set; }

        public virtual ICollection<CriticalControl> CriticalControls { get; set; }

        public virtual ICollection<SafetyMessage> SafetyMessages { get; set; }

    }
}
