﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MomentumPlus.Core.Models
{
    public class RotationTopic : HandoverItem
    {
        [Required]
        public string Name { get; set; }
        public IEnumerable<string> Tags { get; set; }
        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    tagsString = string.Format("{0} \"{1}\",", tagsString, Tags);
                }

                return tagsString;
            }
        }
        public string Variance { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Field must be numeric")]
        public string Planned { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Field must be numeric")]
        public string Actual { get; set; }

        public Guid? UnitsTypeId { get; set; }

        public string UnitsSelectedType { get; set; }

        public bool? IsExpandPlannedActualField { get; set; }

        public bool? IsFirstTopicByTopicGroupName { get; set; }

        public Guid? CreatorManagerCommentsId { get; set; }

        public DateTime? DateTimeCreatedManagerComments { get; set; }

        public bool? IsCanRemoveManagerComments { get; set; }

        public bool? IsLineManager { get; set; }

        public string FullNameLineManager { get; set; }

        public bool? IsEditManagerComments { get; set; }

        public bool? HasTags { get; set; }

        public bool? PlannedActualHas { get; set; }

        public string FirstTagTopic { get; set; }

        public string SearchText { get; set; }

        public bool? MoreTags { get; set; }

        public bool? IsPinned { get; set; }

        public bool IsNullReport { get; set; }

        public bool IsFeedbackRequired { get; set; }

        public string Description { get; set; }

        public string SearchTags { get; set; }

        public string ManagerComments { get; set; }

        public string Feedback { get; set; }

        public bool Enabled { get; set; }

        public StatusOfFinalize FinalizeStatus { get; set; }

        public Guid? AssignedToId { get; set; }

        public Guid? CreatorId { get; set; }

        public virtual UserProfile AssignedTo { get; set; }

        public Guid? LocationId { get; set; }

        public virtual Location Location { get; set; }

        public virtual ICollection<VoiceMessage> VoiceMessages { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }

        public virtual ICollection<AttachmentsLink> AttachmentsLink { get; set; }

        public virtual ICollection<ManagerComments> ManagerComment { get; set; }

        public virtual ICollection<RotationTopic> SharedTopics { get; set; }

        public virtual ICollection<RotationTask> RotationTasks { get; set; }

        public virtual ICollection<RotationTopicLog> Logs { get; set; }

        #region Topic Relation

        public Guid? RelationId { get; set; }

        public Guid RotationTopicGroupId { get; set; }

        public virtual RotationTopicGroup RotationTopicGroup { get; set; }

        public Guid? TempateTopicId { get; set; }

        public virtual TemplateTopic TempateTopic { get; set; }

        // Handover relations
        public Guid? AncestorTopicId { get; set; }

        public virtual RotationTopic AncestorTopic { get; set; }

        public Guid? SuccessorTopicId { get; set; }

        public virtual RotationTopic SuccessorTopic { get; set; }

        public Guid? ParentTopicId { get; set; }

        public virtual RotationTopic ParentTopic { get; set; }

        public virtual ICollection<RotationTopic> ChildTopics { get; set; }

        #endregion


        #region Fork Relation

        public int ForkCounter { get; set; }

        public Guid? ForkParentTopicId { get; set; }

        public virtual RotationTopic ForkParentTopic { get; set; }

        public virtual ICollection<RotationTopic> ForkChildTopics { get; set; }

        #endregion


        #region Sharing Relation

        public Guid? SharedRecipientId { get; set; }

        public Guid? ShareSourceTopicId { get; set; }

        public virtual RotationTopic ShareSourceTopic { get; set; }

        public virtual ICollection<RotationTopicSharingRelation> TopicSharingRelations { get; set; }

        public bool? IsRecipientsFromPrevRotation { get; set; }

        #endregion

        [Obsolete]
        public Guid? ParentRotationTopicId { get; set; }

        [Obsolete]
        public virtual RotationTopic ParentRotationTopic { get; set; }

        [Obsolete]
        public virtual ICollection<RotationTopic> ChildRotationTopics { get; set; }

        [Obsolete]
        public int CopyTimesCounter { get; set; }


        public new RotationTopic Clone()
        {
            var clone = base.Clone<RotationTopic>();

            clone.Location = this.Location?.Clone();

            clone.Attachments = Attachments?.Select(attachment =>
            {
                attachment = attachment.Clone();
                attachment.RotationTopic = clone;
                attachment.RotationTopicId = clone.Id;

                return attachment;

            }).ToList();

            clone.AttachmentsLink = AttachmentsLink?.Select(attachmentLink =>
            {
                attachmentLink = attachmentLink.Clone();
                attachmentLink.RotationTopic = clone;
                attachmentLink.RotationTopicId = clone.Id;

                return attachmentLink;

            }).ToList();

            clone.ManagerComment = ManagerComment?.Select(managerComments =>
            {
                managerComments = managerComments.Clone();
                managerComments.RotationTopic = clone;
                managerComments.RotationTopicId = clone.Id;

                return managerComments;

            }).ToList();


            if (this.TempateTopicId.HasValue)
            {
                this.TempateTopic.TopicSharingRelations = this.TopicSharingRelations;
            }

            clone.TopicSharingRelations = clone.TopicSharingRelations?.Select(topicsSharing =>
            {
                topicsSharing = topicsSharing.Clone();

                return topicsSharing;

            }).Where(r => r.RecipientId.HasValue).GroupBy(t => t.RecipientId).Select(t => t.FirstOrDefault()).ToList();


            clone.IsRecipientsFromPrevRotation = this.IsRecipientsFromPrevRotation;

            clone.AncestorTopic = this;
            clone.AncestorTopicId = Id;
            clone.RelationId = this.RelationId;
            clone.ParentTopic = null;
            clone.ParentTopicId = this.ParentTopicId.HasValue ? this.ParentTopicId : this.Id;
            clone.SuccessorTopic = null;
            clone.SuccessorTopicId = null;


            clone.ForkParentTopic = this.ForkParentTopic;
            clone.ForkParentTopicId = this.ForkParentTopicId;
            clone.ForkCounter = 0;
            clone.ForkChildTopics = null;


            clone.ChildTopics = null;
            clone.Logs = null;
            clone.VoiceMessages = null;
            clone.AssignedToId = this.AssignedToId;
            clone.AssignedTo = this.AssignedTo;
            clone.RotationTasks = null;
            clone.SharedTopics = null;

            //Old Relations
            clone.ChildRotationTopics = null;
            clone.ParentRotationTopicId = null;
            clone.ParentRotationTopic = null;

            clone.RotationTopicGroup = null;

            return clone;
        }
    }
}
