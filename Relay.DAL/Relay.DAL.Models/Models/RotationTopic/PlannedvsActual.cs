﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class PlannedvsActual
    {
        public string Planned { get; set; }
        public string Actual { get; set; }
        public Guid? UnitsTypeId { get; set; }
        public string UnitsSelectedType { get; set; }
    }
}