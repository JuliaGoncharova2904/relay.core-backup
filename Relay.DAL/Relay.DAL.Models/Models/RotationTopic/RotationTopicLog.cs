﻿using System;

namespace MomentumPlus.Core.Models
{
    public class RotationTopicLog : BaseModel
    {
        public Guid? TopicId { get; set; }

        public string LogMessage { get; set; }

        public DateTime LogDate { get; set; }

        public Guid RootTopicId { get; set; }

        public virtual RotationTopic RootTopic { get; set; }

        public Guid? UserId { get; set; }

        public virtual UserProfile User { get; set; }
    }
}
