﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Core.Models
{
    public class ManagerComments : HandoverItem
    {
       // [Required]
      //  public Guid Id { get; set; }

        [Required]
        [Display(Name = "Notes*")]
        [StringLength(300)]
        public string Notes { get; set; }

        public Guid? CreatorManagerCommentsId { get; set; }

        public DateTime? DateTimeCreatedManagerComments { get; set; }

        public bool? IsCanRemoveManagerComments { get; set; }

        public bool? IsEditManagerComments { get; set; }

        public bool? IsLinemanager { get; set; }

        public string FullNameLineManager { get; set; }

        public virtual RotationTopic RotationTopic { get; set; }

        public virtual Guid? RotationTopicId { get; set; }

        public new ManagerComments Clone()
        {
            var clone = Clone<ManagerComments>();

            if (Notes != null)
            {
                clone.Notes = clone.Notes;
            }

            return clone;
        }
    }
}
