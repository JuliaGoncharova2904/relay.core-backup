﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Core.Models
{
    public class VoiceMessage : HandoverItem
    {
        [Required]
        public string Name { get; set; }

        public virtual File File { get; set; }

        public Guid FileId { get; set; }

        public virtual RotationTask RotationTask { get; set; }

        public virtual Guid? RotationTaskId { get; set; }

        public virtual RotationTopic RotationTopic { get; set; }

        public virtual Guid? RotationTopicId { get; set; }


    }
}