﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class Company : BaseModel
    {
        [Required]
        public string Name { get; set; }

        public string Notes { get; set; }

        public bool IsDefault { get; set; }

        public virtual ICollection<UserProfile> Users { get; set; }
    }
}
