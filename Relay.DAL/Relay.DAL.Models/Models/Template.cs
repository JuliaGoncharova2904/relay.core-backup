﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class Template : BaseModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<TemplateModule> Modules { get; set; }

        public virtual ICollection<Position> Positions { get; set; }

        public bool? AddRelayTemplateToTemplates { get; set; }
    }
}
