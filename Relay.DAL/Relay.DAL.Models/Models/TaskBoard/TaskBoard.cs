﻿using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class TaskBoard : BaseModel
    {
        public virtual UserProfile User { get; set; }

        public virtual ICollection<RotationTask> Tasks { get; set; }
    }
}
