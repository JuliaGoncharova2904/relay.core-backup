﻿namespace MomentumPlus.Core.Models
{
    public enum TaskBoardTaskType
    {
        NotSet = 0,
        Draft = 1,
        Received = 2,
        Pending = 3,
        MyTask = 4
    }
}
