﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class TemplateTopicGroup : BaseModel
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsNullReport { get; set; }

        public Guid ModuleId { get; set; }

        public virtual TemplateModule Module { get; set; }

        public Guid? RelationId { get; set; }

        public Guid? ParentTopicGroupId { get; set; }

        public virtual TemplateTopicGroup ParentTopicGroup { get; set; }

        public virtual ICollection<TemplateTopicGroup> ChildTopicGroups { get; set; }

        public virtual ICollection<RotationTopicGroup> ChildRotationTopicGroups { get; set; }

        public virtual ICollection<TemplateTopic> Topics { get; set; }

        public bool Enabled { get; set; }
    }
}
