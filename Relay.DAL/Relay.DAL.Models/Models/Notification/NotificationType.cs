﻿namespace MomentumPlus.Core.Models
{
    public enum NotificationType
    {
        //--------Task CheckList----
        HandbackTasksStillComplete = 2,
        TasksStillComplete = 3,
        AssignedTasks = 5,
        AssignedHandbackTasks = 6,
        ManagerReassignsTask = 9,
        StatuOfTaskIncompleteAfterDueDate = 22,

        //-----------------------------
        ManagerAddedComment = 4,
        TeamMemberHandedOver = 7,
        ManagerReassignsHandover = 8,
        PossibilityToExtendSwing = 1,

        //------- Now tasks --------
        AssignedNowTaskForYou = 10,
        YourNowTaskHasNotCompleted = 11,
        YouHaveNowTaskToComplete = 12,
        CreatorModifiedDueDate = 13,
        CreatorReassignedNowTask = 14,
        CreatorDeletedNowTask = 15,
        UserHasCompletedTask = 18,
        StatuOfTaskIncompleteAfterDueDateFoTaskNow = 23,
        //----- info sharing -------
        OneSharedInformationWithYou = 16,
        SharedReportWithYou = 24,
        OneReadSharedInfoAndComplete = 17,
        //----- Shift notify -------
        PossibilityToExtendShift = 19,

        //----- New Task logic notify -------
        TaskDueDateIsChanged = 20,
        ReceivedTaskWasReassigned = 21,

        AddedAttachmentToIncompleteTask = 25,
        AddedAttachmentToCompleteTask = 26,
        CreateSectorIhandoverAdmin = 27,
        CreateOwnHandover = 28,
        ShiftReceivedReport = 29,
        SwingReceivedReport = 30,
    }
}
