﻿using System;

namespace MomentumPlus.Core.Models
{
    public class Notification : BaseModel
    {
        public NotificationType Type { get; set; }
        public string IconPath { get; set; }
        public string HtmlMessage { get; set; }
        public DateTime Timestamp { get; set; }

        public virtual UserProfile Recipient { get; set; }
        public Guid RecipientId { get; set; }

        public bool IsShown { get; set; }
        public bool IsOpened { get; set; }

        public string ServiceData { get; set; }

        public Guid? RelationId { get; set; }

        public Guid? ShiftId { get; set; }

        public Guid? SwingId { get; set; }

        public bool PermissionsBasic { get; set; }

        public bool PermissionsUltimate { get; set; }
    }
}
