﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class Attachment : HandoverItem
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string FileName { get; set; }

        public string Link { get; set; }

        public virtual File File { get; set; }

        public Guid FileId { get; set; }

        public virtual RotationTask RotationTask { get; set; }

        public virtual Guid? RotationTaskId { get; set; }

        public virtual RotationTopic RotationTopic { get; set; }

        public virtual Guid? RotationTopicId { get; set; }

        public bool? IsReceivedAttachment { get; set; }

        public new Attachment Clone()
        {
            var clone = Clone<Attachment>();

            if (File != null)
            {
                clone.File = File.Clone();
                clone.FileId = clone.File.Id;
            }

            return clone;
        }


    }
}
