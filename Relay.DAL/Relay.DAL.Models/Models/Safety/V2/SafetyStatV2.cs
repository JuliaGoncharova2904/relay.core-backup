﻿using System;

namespace MomentumPlus.Core.Models
{
    public class SafetyStatV2 : BaseModel
    {
        public virtual SafetyMessageV2 SafetyMessage { get; set; }

        public Guid SafetyMessageId { get; set; }

        public virtual UserProfile Recipient { get; set; }

        public Guid RecipientId { get; set; }

        public SafetyRecipientStateV2 State { get; set; }
    }
}
