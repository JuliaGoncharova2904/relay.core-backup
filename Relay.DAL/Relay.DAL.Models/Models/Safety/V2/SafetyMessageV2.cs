﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class SafetyMessageV2 : BaseModel
    {
        public DateTime Date { get; set; }

        public string Message { get; set; }

        public virtual CriticalControlV2 CriticalControl { get; set; }

        public Guid CriticalControlId { get; set; }

        public virtual ICollection<SafetyStatV2> SafetyStats { get; set; }
    }
}
