﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class MajorHazardV2 : BaseModel
    {
        public string Name { get; set; }

        public string IconPath { get; set; }

        public virtual UserProfile Owner { get; set; }

        public Guid? OwnerId { get; set; }

        public virtual ICollection<CriticalControlV2> CriticalControls { get; set; }
    }
}
