﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class CriticalControlV2 : BaseModel
    {
        public string Name { get; set; }

        public Guid? OwnerId { get; set; }

        public virtual UserProfile Owner { get; set; }

        public virtual MajorHazardV2 MajorHazard { get; set; }

        public Guid MajorHazardId { get; set; }

        public virtual ICollection<UserProfile> Champions { get; set; }

        public virtual ICollection<SafetyMessageV2> SafetyMessages { get; set; }
    }
}
