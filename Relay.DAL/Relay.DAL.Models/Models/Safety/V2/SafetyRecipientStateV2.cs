﻿namespace MomentumPlus.Core.Models
{
    public enum SafetyRecipientStateV2
    {
        Viewed = 0,
        ViewedLate = 1,
        NoViewed = 2
    }
}
