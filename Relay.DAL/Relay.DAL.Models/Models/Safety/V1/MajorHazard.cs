﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class MajorHazard : BaseModel
    {
        public string Name { get; set; }

        public Guid IconId { get; set; }
        public virtual BankOfIconItem Icon { get; set; }

        public virtual ICollection<CriticalControl> CriticalControls { get; set; }
    }
}
