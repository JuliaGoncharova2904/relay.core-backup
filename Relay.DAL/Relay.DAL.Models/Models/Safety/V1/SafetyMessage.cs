﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class SafetyMessage : BaseModel
    {
        public DateTime Date { get; set; }

        public string Message { get; set; }

        public int OpenCounter { get; set; }

        public int DayUseCounter { get; set; }

        public virtual ICollection<Team> Teams { get; set; }

        public Guid CriticalControlId { get; set; }

        public virtual CriticalControl CriticalControl { get; set; }

        public virtual ICollection<UserProfile> Users { get; set; }
    }
}
