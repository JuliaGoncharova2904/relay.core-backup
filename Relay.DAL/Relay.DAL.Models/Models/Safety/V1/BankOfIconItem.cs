﻿using System;

namespace MomentumPlus.Core.Models
{
    public class BankOfIconItem : BaseModel
    {
        public string Name { get; set; }

        public Guid ImageId { get; set; }

        public virtual File Image { get; set; }
    }
}
