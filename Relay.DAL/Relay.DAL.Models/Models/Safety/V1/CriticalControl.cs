﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class CriticalControl : BaseModel
    {
        public string Name { get; set; }

        public virtual ICollection<Team> Teams { get; set; }

        public Guid MajorHazardId { get; set; }
        public virtual MajorHazard MajorHazard { get; set; }

        public virtual ICollection<SafetyMessage> SafetyMessages { get; set; }
    }
}
