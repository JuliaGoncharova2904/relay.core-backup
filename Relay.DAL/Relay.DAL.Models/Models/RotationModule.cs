﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class RotationModule : HandoverItem
    {
        public TypeOfModule Type { get; set; }

        public bool Enabled { get; set; }

        public TypeOfModuleSource SourceType { get; set; }

        public Guid? TempateModuleId  { get; set; }

        public virtual TemplateModule TemplateModule { get; set; }

        public Guid? RotationId { get; set; }

        public virtual Rotation Rotation { get; set; }

        public Guid? ShiftId { get; set; }

        public virtual Shift Shift { get; set; }

        public virtual ICollection<RotationTopicGroup> RotationTopicGroups { get; set; }

        public new RotationModule Clone()
        {
            var clone = Clone<RotationModule>();

            return clone;
        }

    }
}
