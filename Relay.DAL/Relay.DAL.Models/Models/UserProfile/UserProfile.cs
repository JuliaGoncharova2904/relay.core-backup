﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MomentumPlus.Core.Models
{
    public class UserProfile : BaseModel
    {
        public string Title { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public Guid? AvatarId { get; set; }

        public File Avatar { get; set; }

        public Guid PositionId { get; set; }

        public virtual Position Position { get; set; }

        public Guid RotationPatternId { get; set; }

        public string ShiftPatternType { get; set; }

        public virtual RotationPattern RotationPattern { get; set; }

        public Guid TeamId { get; set; }

        public virtual Team Team { get; set; }

        public virtual Company Company { get; set; }

        public Guid CompanyId { get; set; }

        public Guid? CurrentRotationId { get; set; }

        public Guid? PasswordResetGuid { get; set; }

        public virtual Rotation CurrentRotation { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [EmailAddress]
        public string SecondaryEmail { get; set; }

        public DateTime? LastLoginDate { get; set; }

        public DateTime? LastPasswordChangedDate { get; set; }

        public DateTime? LastPasswordFailureDate { get; set; }

        public DateTime? LockDate { get; set; }

        public virtual ICollection<Project> CreatedProjects { get; set; }

        public virtual ICollection<Project> ManagedProjects { get; set; }

        public virtual ICollection<Project> FollowingProjects { get; set; }

        public virtual ICollection<Notification> Notifications { get; set; }

        public virtual ICollection<Rotation> WorkRotations { get; set; }

        public virtual ICollection<Rotation> ManagedRotations { get; set; }

        public virtual ICollection<Rotation> HandoverSourceRotations { get; set; }

        [Obsolete]
        public virtual ICollection<Rotation> ContributedRotations { get; set; }

        public virtual ICollection<MajorHazardV2> MajorHazardsOwnerV2 { get; set; }

        public virtual ICollection<CriticalControlV2> CriticalControlsOwnerV2 { get; set; }

        public virtual ICollection<CriticalControlV2> ChampionOfCriticalControlsV2 { get; set; }

        public virtual ICollection<SafetyStatV2> SafetyMessageStatsV2 { get; set; }

        public virtual ICollection<RotationReportSharingRelation> ReceivedReports { get; set; }

        public virtual ICollection<RotationReportSharingRelation> SendedReports { get; set; }


        public virtual ICollection<UserProfile> Contributors { get; set; }
        public virtual ICollection<UserProfile> ContributedUsers { get; set; }


        public virtual TaskBoard TaskBoard { get; set; }


        [NotMapped]
        public string FullName
        {
            get
            {
                return string.Join(" ", FirstName, LastName);
            }
            set
            {
                string[] name = value.Split(new char[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);

                if (name.Length > 0)
                    FirstName = name[0];

                if (name.Length > 1)
                    LastName = name[1];
            }
        }

        [NotMapped]
        public string PositionName
        {
            get { return Position.Name; }
            set { Position.Name = value; }
        }
    }
}
