﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MomentumPlus.Core.Models
{
    public class RotationPattern : BaseModel
    {
        [Required]
        public int DayOn { get; set; }

        [Required]
        public int DayOff { get; set; }

        public virtual ICollection<UserProfile> Users { get; set; }

        [NotMapped]
        public string Name
        {
            get
            {
                return string.Format("{0} days on, {1} days off", DayOn, DayOff);
            }
        }
    }
}