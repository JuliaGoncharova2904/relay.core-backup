﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Core.Models
{
    public class AttachmentsLink : HandoverItem
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string Link { get; set; }

        public virtual RotationTask RotationTask { get; set; }

        public virtual Guid? RotationTaskId { get; set; }

        public virtual RotationTopic RotationTopic { get; set; }

        public virtual Guid? RotationTopicId { get; set; }

        public bool? IsReceivedAttachment { get; set; }

        public new AttachmentsLink Clone()
        {
            var clone = Clone<AttachmentsLink>();

            if (Link != null)
            {
                clone.Link = clone.Link;
            }

            return clone;
        }

    }
}
