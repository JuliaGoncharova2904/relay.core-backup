﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class Project : BaseModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid CreatorId { get; set; }

        public virtual UserProfile Creator { get; set; }

        public Guid ProjectManagerId { get; set; }

        public virtual UserProfile ProjectManager { get; set; }

        public virtual ICollection<UserProfile> ProjectFollowers { get; set; }

        public virtual ICollection<RotationTask> Tasks { get; set; }
    }
}