﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Core.Models
{
    public class TemplateModule : BaseModel
    {
        public TypeOfModule Type { get; set; }

        public bool Enabled { get; set; }

        public Guid? TemplateId { get; set; }

        public virtual Template Template { get; set; }

        public Guid? ParentModuleId { get; set; }

        public virtual TemplateModule ParentModule { get; set; }

        public virtual ICollection<TemplateModule> ChildModules { get; set; }

        public virtual ICollection<RotationModule> ChildRotationModules { get; set; }

        public virtual ICollection<TemplateTopicGroup> TopicGroups { get; set; }
    }
}
