﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MomentumPlus.Core.Models
{
    public class TemplateTopic : HandoverItem
    {
        [Required]
        [StringLength(150, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public Guid TopicGroupId { get; set; }

        public virtual TemplateTopicGroup TopicGroup { get; set; }

        public Guid? ParentTopicId { get; set; }

        public virtual TemplateTopic ParentTopic { get; set; }

        public virtual ICollection<TemplateTopic> ChildTopics { get; set; }

        public virtual ICollection<RotationTopic> ChildRotationTopics { get; set; }

        public virtual ICollection<TemplateTask> Tasks { get; set; }

        public bool Enabled { get; set; }

        public bool? PlannedActualHas { get; set; }

        public string UnitsSelectedType { get; set; }

        public bool? IsExpandPlannedActualField { get; set; }


        //-------------------------

        public virtual ICollection<RotationTopicSharingRelation> TopicSharingRelations { get; set; }

        public new TemplateTopic Clone()
        {
            var clone = base.Clone<TemplateTopic>();
            
            clone.ParentTopicId = this.ParentTopicId;
            clone.ParentTopic = null;
            clone.TopicGroupId = this.TopicGroupId;
            clone.TopicGroup = null;
            clone.ChildTopics = null;
            clone.ChildRotationTopics = null;
            
            clone.TopicSharingRelations = clone.TopicSharingRelations?.Select(topicsSharing =>
            {
                topicsSharing = topicsSharing.Clone();

                return topicsSharing;

            }).Where(r => r.RecipientId.HasValue).GroupBy(t => t.RecipientId).Select(t => t.FirstOrDefault()).ToList();


            return clone;
        }
    }
}
