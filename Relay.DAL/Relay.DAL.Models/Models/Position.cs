﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class Position : BaseModel
    {
        //[Required]
        //[StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public Guid? PositionId { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string CustomPositionName { get; set; }

        public Guid WorkplaceId { get; set; }

        public virtual Workplace Workplace { get; set; }

        public Guid TemplateId { get; set; }

        public virtual Template Template { get; set; }

        public virtual ICollection<UserProfile> Users { get; set; }
    }
}
