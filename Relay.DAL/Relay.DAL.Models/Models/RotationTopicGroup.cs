﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class RotationTopicGroup : HandoverItem
    {
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public string SearchTags { get; set; }

        public Guid? RelationId { get; set; }

        public Guid RotationModuleId { get; set; }

        public virtual RotationModule RotationModule { get; set; }

        public Guid? TempateTopicGroupId { get; set; }

        public virtual TemplateTopicGroup TempateTopicGroup { get; set; }

        public virtual ICollection<RotationTopic> RotationTopics { get; set; }

        public bool Enabled { get; set; }

        public Guid? ParentRotationTopicGroupId { get; set; }

        public virtual RotationTopicGroup ParentRotationTopicGroup { get; set; }

        public virtual ICollection<RotationTopicGroup> ChildRotationTopicGroups { get; set; }

        public string SearchText { get; set; }

        public new RotationTopicGroup Clone()
        {
            var clone = Clone<RotationTopicGroup>();

            clone.ParentRotationTopicGroup = this;
            clone.ParentRotationTopicGroupId = this.Id;

            return clone;
        }
    }
}
