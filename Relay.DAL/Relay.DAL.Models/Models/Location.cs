﻿using System.Collections.Generic;
using MomentumPlus.Core.Models;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Core.Models
{
    public class Location : HandoverItem
    {
        [Required]
        public string Name { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public virtual ICollection<RotationTopic> RotationTopics { get; set; }

        public new Location Clone()
        {
            var clone = Clone<Location>();

            clone.RotationTopics = null;
          
            return clone;
        }
    }
}
