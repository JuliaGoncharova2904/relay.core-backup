﻿using System.ComponentModel;

namespace MomentumPlus.Core.Models
{
    public enum PriorityOfTask
    {
        [Description("Low")]
        Low = 0,
        [Description("Normal")]
        Normal = 1,
        [Description("Important")]
        Important = 2,
        [Description("Critical")]
        Critical = 3,
        [Description("Urgent")]
        Urgent = 4
    }
}
