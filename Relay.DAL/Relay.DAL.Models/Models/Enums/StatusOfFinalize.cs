﻿namespace MomentumPlus.Core.Models
{
    public enum StatusOfFinalize
    {
        NotFinalized = 0,
        Finalized = 1,
        AutoFinalized = 2
    }
}
