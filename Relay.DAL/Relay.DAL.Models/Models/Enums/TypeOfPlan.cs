﻿namespace MomentumPlus.Core.Models
{
    public enum TypeOfPlan
    {
        Basic = 0,
        Professional = 1,
        Enterprise = 2
    }
}
