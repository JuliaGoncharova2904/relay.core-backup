﻿namespace MomentumPlus.Core.Models
{
    public enum TypeOfSupport
    {
        Basic = 0,
        Premium = 1,
        Ultimate = 2
    }
}
