﻿namespace MomentumPlus.Core.Models
{
    public enum TypeOfModuleSource
    {
        Received = 0,
        Draft = 1
    }
}
