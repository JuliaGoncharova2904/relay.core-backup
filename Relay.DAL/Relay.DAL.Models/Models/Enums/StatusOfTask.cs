﻿namespace MomentumPlus.Core.Models
{
    public enum StatusOfTask
    {
        Default = 0,
        Now = 1,
        NewNow = 2,
    }
}
