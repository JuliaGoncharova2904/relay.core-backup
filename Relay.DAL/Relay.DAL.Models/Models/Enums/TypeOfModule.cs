﻿using System.ComponentModel;

namespace MomentumPlus.Core.Models
{
    public enum TypeOfModule
    {
        [Description("Safety")]
        HSE = 0,

        [Description("Core")]
        Core = 1,

        [Description("Project")]
        Project = 2,

        [Description("Team")]
        Team = 3,

        [Description("Daily Note")]
        DailyNote = 4
    }
}
