﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace MomentumPlus.Core.Models
{
    public class RotationTask : HandoverItem
    {
        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public string SearchTags { get; set; }

        public bool? IsReceivedTask { get; set; }

        public PriorityOfTask Priority { get; set; }

        public StatusOfTask Status { get; set; }

        public DateTime Deadline { get; set; }

        public DateTime? CompleteDateUtc { get; set; }

        public bool IsComplete { get; set; }

        public StatusOfFinalize FinalizeStatus { get; set; }

        public bool IsNullReport { get; set; }

        public bool IsFeedbackRequired { get; set; }

        public bool IsHaveNotificationTask { get; set; }

        public bool? IsPinned { get; set; }

        public string ManagerComments { get; set; }

        public string Feedback { get; set; }

        public bool Enabled { get; set; }

        public Guid? AssignedToId { get; set; }

        public Guid? CreatorId { get; set; }

        public virtual UserProfile AssignedTo { get; set; }

        public virtual ICollection<VoiceMessage> VoiceMessages { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }

        public virtual ICollection<AttachmentsLink> AttachmentsLink { get; set; }

        public virtual ICollection<RotationTaskLog> Logs { get; set; }


        #region Task Relation

        public Guid? RelationId { get; set; }

        public Guid? RotationTopicId { get; set; }

        public virtual RotationTopic RotationTopic { get; set; }

        public Guid? TemplateTaskId { get; set; }

        public virtual TemplateTask TemplateTask { get; set; }

        // Handover relations
        public Guid? AncestorTaskId { get; set; }

        public virtual RotationTask AncestorTask { get; set; }

        public Guid? SuccessorTaskId { get; set; }

        public virtual RotationTask SuccessorTask { get; set; }

        public Guid? ParentTaskId { get; set; }

        public virtual RotationTask ParentTask { get; set; }

        public virtual ICollection<RotationTask> ChildTasks { get; set; }

        #endregion


        #region ForkRelation

        public int ForkCounter { get; set; }

        public Guid? ForkParentTaskId { get; set; }

        public virtual RotationTask ForkParentTask { get; set; }

        public virtual ICollection<RotationTask> ForkChildTasks { get; set; }

        #endregion


        #region TaskBoardRelation

        public Guid? TaskBoardId { get; set; }

        public virtual TaskBoard TaskBoard { get; set; }

        public TaskBoardTaskType TaskBoardTaskType { get; set; }

        public DateTime? DeferredHandoverTime { get; set; }

        public bool IsInArchive { get; set; }

        #endregion


        #region ProjectRelation

        public Guid? ProjectId { get; set; }

        public virtual Project Project { get; set; }

        #endregion

        [Obsolete]
        public Guid? ParentRotationTaskId { get; set; }
        [Obsolete]
        public virtual RotationTask ParentRotationTask { get; set; }
        [Obsolete]
        public virtual ICollection<RotationTask> ChildRotationTasks { get; set; }
        [Obsolete]
        public int CopyTimesCounter { get; set; }

        //NEW TASK LOGIC
        public bool AddToReport { get; set; }

        public Guid? HandoverId { get; set; }

        public new RotationTask Clone()
        {
            var clone = base.Clone<RotationTask>();

            clone.Attachments = Attachments?.Select(attachment =>
            {
                attachment = attachment.Clone();
                attachment.RotationTask = clone;
                attachment.RotationTaskId = clone.Id;

                return attachment;

            }).ToList();

            //Task relation
            clone.TemplateTaskId = this.TemplateTaskId;
            clone.TemplateTask = this.TemplateTask;

            //Handover relation
            clone.AncestorTask = this;
            clone.AncestorTaskId = Id;
            clone.RelationId = this.RelationId;
            //clone.ParentTask = this.ParentTaskId.HasValue ? this.ParentTask : this;
            clone.ParentTask = null;
            clone.ParentTaskId = this.ParentTaskId.HasValue ? this.ParentTaskId : this.Id;
            clone.SuccessorTask = null;
            clone.SuccessorTaskId = null;

            //Fork relation
            clone.ForkParentTask = this.ForkParentTask;
            clone.ForkParentTaskId = this.ForkParentTaskId;
            clone.ForkCounter = 0;
            clone.ForkChildTasks = null;


            clone.ChildTasks = null;
            clone.Logs = null;
            clone.VoiceMessages = null;
            clone.AssignedToId = this.AssignedToId;
            clone.AssignedTo = this.AssignedTo;
            clone.TaskBoard = null;


            //Old Relations
            clone.ChildRotationTasks = null;
            clone.ParentRotationTask = null;
            clone.ParentRotationTaskId = null;



            clone.RotationTopic = null;
            clone.RotationTopicId = null;

            //New task logic

            clone.AddToReport = this.AddToReport;
            clone.HandoverId = this.HandoverId;


            return clone;
        }
    }

    public class AttachmentsTask
    {
        public Guid? AttachmentsId { get; set; }

        public bool IsAddCreator { get; set; }
    }
}
