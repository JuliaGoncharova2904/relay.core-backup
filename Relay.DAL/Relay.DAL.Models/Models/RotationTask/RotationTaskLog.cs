﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Core.Models
{
    public class RotationTaskLog : BaseModel
    {
        public Guid? TaskId { get; set; }

        public string LogMessage { get; set; }

        public DateTime LogDate { get; set; }

        public Guid RootTaskId { get; set; }

        public virtual RotationTask RootTask { get; set; }
    }
}
