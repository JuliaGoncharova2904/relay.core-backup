﻿using System;
using System.Web;
using System.Web.Configuration;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Helpers
{
    public class AppSession
    {
        /// <summary>
        ///Is MultiTenant build mode
        /// </summary>
        public bool IsMultiTenant => bool.Parse(WebConfigurationManager.AppSettings["isMultiTenant"]);

        /// <summary>
        /// ID of the currently subscription Id.
        /// </summary>
        public Guid? CurrentSubscriptionId { get; set; }

        public SubscriptionDetailsModel Subscription { get; set; }

        /// <summary>
        /// Get the current session.
        /// </summary>
        public static AppSession Current
        {
            get
            {

                if (HttpContext.Current.Session == null)
                {
                    AppSession s = new AppSession();
                    return s;
                }
                if (HttpContext.Current.Session["Relay"] == null)
                {
                    AppSession s = new AppSession();
                    HttpContext.Current.Session["Relay"] = s;
                    return s;
                }

                return (AppSession)HttpContext.Current.Session["Relay"];

                //AppSession session = (AppSession)HttpContext.Current.Session["Relay"];
                //if (session == null)
                //{
                //    session = new AppSession();
                //    HttpContext.Current.Session["Relay"] = session;
                //}
                //return session;
            }
        }

        public AppSession ClearAppSession()
        {
            AppSession s = new AppSession();
            return s;
        }
    }
}