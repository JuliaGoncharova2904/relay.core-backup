﻿using System;

namespace MomentumPlus.Relay.Exceptions
{
    public class PaymentRequiredException : Exception
    {
        public PaymentRequiredException(string message) : base(message)
        { }
    }
}
