﻿namespace MomentumPlus.Relay.Constants
{
    public static class NumericConstants
    {
        public static class Paginator
        {
            public static int PaginatorPageSize = 10;
            public static int PaginatorPageSizeForTeamRotations = 9;
            public static int PaginatorPageSizeForFilterRotations = 1;
            public static int PaginatorPageSizeForGlobalReport = 5;
            public static int PaginatorPageSizeForTopicSearch = 10;
        }
    }
}
