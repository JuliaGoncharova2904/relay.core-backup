﻿namespace MomentumPlus.Relay.Roles
{
    public static class iHandoverRoles
    {
        public struct Relay
        {
            public const string Administrator = "Administrator";
            public const string iHandoverAdmin = "iHandover Admin";
            public const string ExecutiveUser = "Executive User";
            public const string HeadLineManager = "Head Line Manager";
            public const string LineManager = "Line Manager";
            public const string SafetyManager = "Safety Manager";
            public const string User = "User";
            public const string Contributor = "Contributor";

        }

        public struct RelayGroups
        {
            public const string Admins = Relay.Administrator + "," + Relay.iHandoverAdmin + "," + Relay.ExecutiveUser;

            public const string LineManagers = Relay.HeadLineManager + "," + Relay.LineManager + "," + Relay.SafetyManager;

            public const string SimpleUser = Relay.User + "," + Relay.Contributor;

            public const string All = Relay.Administrator + "," +
                                        Relay.iHandoverAdmin + "," +
                                        Relay.HeadLineManager + "," +
                                        Relay.LineManager + "," +
                                        Relay.SafetyManager + "," +
                                        Relay.User + "," +
                                        Relay.Contributor +
                                        Relay.ExecutiveUser;
        }
    }
}