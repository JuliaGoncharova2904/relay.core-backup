﻿using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces
{
    public interface INotificationAdapter
    {
        void PushNotificationInfo(Guid userId, int msgNumber);
        void PushNotification(Guid userId, NotificationMessageViewModel newNotification);
    }
}
