﻿using MomentumPlus.Relay.Interfaces.ApiServices;

namespace MomentumPlus.Relay.Interfaces
{
    public interface IApiServicesUnitOfWork
    {
        IAccountService AccountService { get; }
        IHomeService HomeService { get; }
        INotificationService NotificationService { get; }

        IProfileService ProfileService { get; }
    }    
}
