﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISiteService
    {
        List<SiteViewModel> GetSites();
        IEnumerable<SiteViewModel> GetAllSites();

        IEnumerable<SelectListItem> GetSitesList();

        SiteViewModel GetSiteForPosition(Guid positionId);

        bool AddSite(SiteViewModel site);

        SiteViewModel GetSite(Guid siteId, Guid userId);

        bool UpdateSite(SiteViewModel site);

        bool CanRemoveSite(Guid siteId);

        void RemoveSite(Guid siteId);

        SiteViewModel PopulateAddSiteModel();
    }
}
