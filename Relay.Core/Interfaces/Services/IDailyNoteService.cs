﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IDailyNoteService
    {
        DailyNoteViewModel DailyNoteSlideForPanel(Guid dailyNoteId, Guid currentUserId, string safetyVesion);
        DailyNotesPanelViewModel DailyNotesForPanel(Guid rotationId, Guid currentUserId, string safetyVesion);

        IEnumerable<DailyNotesTopicViewModel> DailyNoteTopicsForRotation(Guid rotationId, ModuleSourceType sourceType);

        DailyNoteViewModel GetDailyNoteById(Guid Id);
        DailyNotesTopicViewModel GetDailyNoteTopic(Guid topicId);

        void UpdateDailyNote(DailyNoteViewModel dailyNote);
        DailyNotesTopicViewModel UpdateDailyNoteTopic(DailyNotesTopicViewModel dailyNote);

        DailyNoteViewModel UpdateDailyNoteContext(DailyNoteViewModel dailyNote);
    }
}
