﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISharingTopicService
    {
        ShareTopicViewModel PopulateShareTopicViewModel(Guid topicId);

        void ChangeShareTopicRelations(ShareTopicViewModel model);

        IEnumerable<SelectListItem> GetShareTopicRecipients(Guid topicId);

        void AddShareToTopic(ShareTopicViewModel model, Guid topicId, Guid userId);

        IEnumerable<SelectListItem> GetEmailsList();

        void SetEmailsRecepient(ShareTopicViewModel model);

        ShareTopicViewModel SetStringEmailsRecepient(string email);
    }
}
