﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IShiftService
    {
        void ConfirmShift(Guid userId, ConfirmShiftViewModel model);

        void ConfirmShiftV2(Guid userId, DateTime startDate, DateTime endTime, Guid identityUserId);

        ConfirmShiftViewModel PopulateConfirmShiftViewModel(Guid userId);

        ConfirmShiftV2ViewModel PopulateConfirmShiftV2ViewModel(Guid userId);

        ChooseShiftRecipientModel PopulateChooseShiftRecipientViewModel(Guid shiftId);

        void UpdateShiftRecipient(ChooseShiftRecipientModel model);

        Guid? GetCurrentShiftIdForRotation(Guid rotationId);
        Guid? GetCurrentShiftIdForRotation(Rotation rotation);

        Guid? GetCurrentShiftIdByRotationId(Guid rotationId);

        EditShiftDatesViewModel GetShiftDates(Guid shiftId);
        EditShiftDatesV2ViewModel PopulateEditShiftModel(Guid shiftId);

        void UpdateRotationDates(EditShiftDatesViewModel model);

        void UpdateShiftEndTime(Guid shiftId, DateTime shiftEndTime);

        bool RotationHasWorkingShift(Guid rotationId);
        bool ShiftHasRecipient(Guid shiftId);
        Guid GetUserIdByShiftId(Guid shiftId);
        Shift GetShift(Guid shiftId);

        string GetShiftType(Guid shiftId);
        string GetShiftType(Shift shift);

        bool IsShiftWorkTimeEnd(Guid shiftId);

        bool IsShiftEnd(Guid shiftId);

        string GetShiftPeriod(Guid? shiftId);

        string GetShiftHandoverRecipientName(Guid? shiftId);

        Guid ShiftRotationId(Guid shiftId);

        List<ShiftReceivedSlideViewModel> PopulateReceivedShiftsPanel(Guid shiftId, Guid userId);

        void SetFinalizeStatusForDraftItems(Guid shiftId, FinalizeStatus status);

        string GetReceivedShiftPeriod(Guid? shiftId, Guid? sourceShiftId);

        string GetShiftOwnerName(Guid shiftId);

        void ExpireShift(Guid shiftId, byte[] pdfBytes);
    }
}
