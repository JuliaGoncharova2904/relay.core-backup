﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISearchService
    {
        SearchResultsViewModel SearchSharedTopics(Guid currentUserId, SearchViewModel model);

        SearchResultsViewModel SearchTags(Guid currentUserId, SearchViewModel model);

        SearchTasksResultsViewModel SearchTasks(Guid currentUserId, SearchViewModel model);

        TopicReportPdfViewModel PopulateSearchReportViewModel(Guid currentUserId, SearchViewModel model);

        List<SelectListItem> GetUnitsPvA();

        bool IsActiveRotation(UserProfile topicCreator);

    }
}
