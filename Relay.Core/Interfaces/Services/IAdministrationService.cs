﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IAdministrationService
    {
        AccountDetailsViewModel PopulateAccountDetailsViewModel();

        void UpdateAccountDetails(AccountDetailsViewModel model);

        VersionDetailsViewModel PopulateVersionDetailsViewModel();

        void UpdateVersionDetails(VersionDetailsViewModel model);

        ThirdPartySettingsViewModel PopulateThirdPartySettingsViewModel();

        void UpdateThirdPartySettings(ThirdPartySettingsViewModel model);

        PreferencesSettingsViewModel PopulatePreferencesSettingsViewModel(Guid userId);

        void UpdatePreferencesSettings(PreferencesSettingsViewModel model, HttpPostedFileBase logo);


        DateTime GetCurrentServerTime(Guid userId);

        string GetUserPattern(Guid userId);


    }
}
