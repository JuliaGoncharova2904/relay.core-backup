﻿using System;
using System.Collections.Generic;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITopicService
    {
        IEnumerable<rTopicViewModel> GetTopicGroupTopicsViewModels(Guid topicGroupId);

        rTopicViewModel GetTopicViewModel(Guid topicId);

        Guid AddTopic(rTopicViewModel model);

        void RemoveTopic(Guid topicId);

        void UpdateTopic(rTopicViewModel model);

        bool TopicExist(rTopicViewModel model);

        IEnumerable<rTopicViewModel> PopulateTemplateTopicGroupTopicsViewModels(Guid baseTopicGroupId, Guid templateModuleId);

        bool CheckChildTopicStatus(Guid parentTopicId, Guid templateModuleId);

        void UpdateTopicStatus(Guid topicId, bool status);

        //void UpdateChildTopicStatus(Guid baseTopicId, Guid templateId, bool status);
        void UpdateChildTopicStatus(Guid baseTopicId, Guid templateId, bool status, Guid? topicGroupId, TypeOfModule typeOfModule = TypeOfModule.HSE);

        void UpdateTopicGroupTopicsStatus(Guid topicGroupId, bool status);

        void UpdateTopicMetrics(Guid topicId, bool metrics, string unitSelectedType);
        void RemoveTopicsCore(Guid topicId, Guid templateId, bool status, Guid? topicGroupId, TypeOfModule typeOfModule, Guid moduleId);
        void UpdateTopicFromTamplates(rTopicViewModel model);
    }
}
