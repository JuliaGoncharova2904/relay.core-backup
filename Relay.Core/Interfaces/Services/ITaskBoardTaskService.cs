﻿using MomentumPlus.Relay.Models;
using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITaskBoardTaskService
    {
        IEnumerable<TaskBoardTaskForHandover> GetTaskBoardTasksForShift(Guid shiftId, ModuleSourceType sourceType, Guid? receivedShiftId = null);

        IEnumerable<TaskBoardTaskForHandover> GetTaskBoardTasksForRotation(Guid rotationId, ModuleSourceType sourceType, Guid? receivedRotationId = null);

        TaskBoardTaskForHandover GetTask(Guid taskId);
    }
}
