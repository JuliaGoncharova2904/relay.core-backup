﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISafetyMessageV2Service
    {
        EditSafetyMessageV2ViewModel PopulateEditSafetyMessageModel(DateTime? date);
        EditSafetyMessageV2ViewModel PopulateEditSafetyMessageModel(EditSafetyMessageV2ViewModel model);
        SafetyMessageV2ViewModel GetSafetyMessage(Guid safetyMessageId);
        IEnumerable<SafetyMessageV2ViewModel> GetSafetyMessagesForUser(DateTime date, Guid userId);
        void SetSafetyMessageShowForUser(Guid safetyMessageId, Guid userId);

        SafetyMessageRescheduleV2ViewModel GetSafetyMessageForRescheduling(Guid safetyMessageId);
        void RescheduleSafetyMessage(SafetyMessageRescheduleV2ViewModel model);

        EditSafetyMessageV2ViewModel GetSafetyMessageForEdit(Guid safetyMessageId);
        void UpdateSafetyMessage(EditSafetyMessageV2ViewModel model);
        void AddSafetyMessage(EditSafetyMessageV2ViewModel model);

        SafetyCalendarV2ViewModel GetSafetyCalendar(DateTime date);
        SafetyMessagesForDateV2ViewModel GetSafetyMessagesForDate(DateTime date);
    }
}
