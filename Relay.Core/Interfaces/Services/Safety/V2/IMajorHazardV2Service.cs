﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IMajorHazardV2Service
    {
        IEnumerable<MajorHazardV2ViewModel> GetAllMajorHazards();
        MajorHazardV2ViewModel GetMajorHazard(Guid majorHazardId);
        EditMajorHazardV2ViewModel GetMajorHazardForEdit(Guid majorHazardId);
        void UpdateMajorHazard(EditMajorHazardV2ViewModel model);
    }
}
