﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISafetyStatV2Service
    {
         IEnumerable<ArchivedSafetyMessageV2ViewModel> PopulateArchivedSafetyMessagesViewModel();
    }
}
