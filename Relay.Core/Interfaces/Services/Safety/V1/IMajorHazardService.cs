﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IMajorHazardService
    {
        IEnumerable<SelectListItem> GetMajorHazardsList();

        IEnumerable<MajorHazardViewModel> GetAll();

        EditMajorHazardViewModel GetMajorHazard(Guid majorHazardId);

        void AddMajorHazard(EditMajorHazardViewModel viewModel);
    }
}
