﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISafetyMessageService
    {
        EditSafetyMessageViewModel PopulateSafetyMessageModel();
        EditSafetyMessageViewModel PopulateSafetyMessageModel(EditSafetyMessageViewModel model);

        SafetyCalendarViewModel GetSafetyCalendar(DateTime date, Guid? teamId = null);

        IEnumerable<SelectListItem> GetSafetyMessagesList();
        SafetyMessagesViewModel GetSafetyMessagesByDateAndTeam(DateTime date, Guid teamId);

        SafetyMessageViewModel GetSafetyMessageForPreview(Guid safetyMessageId);

        EditSafetyMessageViewModel GetSafetyMessage(Guid safetyMessageId);
        void UpdateSafetyMessage(EditSafetyMessageViewModel model);
        void AddSafetyMessage(EditSafetyMessageViewModel model);
        IEnumerable<SafetyMessageViewModel> GetSafetyMessageForUser(Guid userId, DateTime date);
        void SetSafetyMessageShowForUser(Guid userId, Guid safetyMessageId);

    }
}
