﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationTopicService
    {
        void UpdateRotationTopicGroupTopicsStatus(Guid rotationTopicGroupId, bool status);
        void UpdateRotationTopicStatus(Guid rotationTopicId, bool status);

        void UpdateTemplateTopicChildTopicsStatus(Guid templateTopicId, bool status, bool? metrics, string unitSelectedType = null);

        bool IsTopicExist(Guid topicId);

        bool IsTopicExist(Guid topicGroupId, Guid topicId, string name);

        bool IsTopicAssignedWithSameNameExist(Guid destId, Guid assignedToId, string name, MomentumPlus.Relay.Models.RotationType rotationType = MomentumPlus.Relay.Models.RotationType.Swing);

        bool IsTopicAssignedWithSameNameExistByTopic(Guid rotationTopicId, Guid assignedToId, string name);

        List<SelectListItem> GetContributorUsers(HSETopicViewModel model, Guid userId);
        void UpdateHSETopic(HSETopicDialogViewModel model, Guid userId);
        List<SelectListItem> GetContributorUsersByTopicId(Guid topicId, Guid userId);

        TagsTopic GetTagsTopicByTopicId(Guid topicId);
        TagsTopic GetTagsTopicByTopicId(RotationTopic rotationTopic);
        string GetTagsByTopicId(Guid topicId, Guid userId);
        void SetTagsByTopicId(Guid topicId, Guid userId, List<string> tags);

        PlannedActual ChangePlannedByTopicIdNew(Guid topicId, string planned);
        PlannedActual ChangeActualByTopicIdNew(Guid topicId, string actual);
        PlannedActual ChangeUnitsTypeByTopicIdNew(Guid topicId, string unitsSelected);
        PlannedActual GetPlannedVsActualByTopicId(Guid topicId);
        int? GetShareCounter(Guid topicId);
        PlannedActual GetPlannedVsActualByTempleteTopicId(Guid topicId);
        PlannedActual GetPlannedVsActualTopicId(RotationTopic rotationTopic);
        bool IsExpandTemplateTopic(Guid topicId);
        string GetUnitSelected(RotationTopic topicId);
        List<SelectListItem> GetUnitsPvA();

        List<RotationTopic> GetTopicsByRelationId(Guid relationId, Guid rotationTopicGroupId);
        List<RotationTopic> SortByHaveFirstTopicName(List<RotationTopic> rotationTopics);
        RotationTopicGroup SortByHaveFirstTopicGroupName(RotationTopicGroup rotationTopics, ModuleSourceType moduleSourceType);
        bool HaveFirstTopicByTopicGroupName(RotationTopicGroup rotationTopics);
        RotationTopicGroup GetFirstTopicByTopicGroupName(RotationTopicGroup rotationTopics);
        List<RotationTopic> UpdateTopicGroupsNameFromRelation(IEnumerable<RotationTopic> rotationTopics);

        HSETopicViewModel UpdateHSETopic(HSETopicViewModel model, Guid userId);

        HSETopicViewModel GetHSETopic(Guid topicId);

        void UpdateCoreTopic(CoreTopicDialogViewModel model, Guid userId);

        List<SelectListItem> ShowIconContributorUsers(Guid userId, RotationTopic rotationTopic);

        CoreTopicViewModel UpdateCoreTopic(CoreTopicViewModel model, Guid userId);

        CoreTopicViewModel GetCoreTopic(Guid topicId);

        void UpdateProjectTopic(ProjectTopicDialogViewModel model, Guid userId);

        ProjectsTopicViewModel GetProjectTopic(Guid topicId);

        ProjectsTopicViewModel UpdateProjectTopic(ProjectsTopicViewModel model, Guid userId);

        void UpdateTeamTopic(TeamTopicDialogViewModel model, Guid userId);

        TeamTopicViewModel UpdateTeamTopic(TeamTopicViewModel model, Guid userId);

        TeamTopicViewModel GetTeamTopic(Guid topicId);

        HSETopicDialogViewModel PopulateHSETopicDialogModel(Guid topicId, Guid userId);

        CoreTopicDialogViewModel PopulateCoreTopicDialogModel(Guid topicId, Guid userId);

        ProjectTopicDialogViewModel PopulateProjectTopicDialogModel(Guid topicId, Guid userId);

        TeamTopicDialogViewModel PopulateTeamTopicDialogModel(Guid topicId, Guid userId);

        HSEAddInlineTopicViewModel PopulateHSETopicInlineModel(Guid rotationId, Guid moduleId);

        RotationCoreAddInlineTopicViewModel PopulateCoreTopicInlineModel(Guid rotationId, Guid moduleId);

        ProjectAddInlineTopicViewModel PopulateProjectTopicInlineModel(Guid rotationId, Guid moduleId);

        TeamAddInlineTopicViewModel PopulateTeamTopicInlineModel(Guid rotationId, Guid teamId);

        void AddHSETopic(HSEAddInlineTopicViewModel model);

        void AddRotationCoreTopic(RotationCoreAddInlineTopicViewModel model);

        void AddProjectTopic(ProjectAddInlineTopicViewModel model);

        void AddTeamTopic(TeamAddInlineTopicViewModel model);

        IEnumerable<ManagerCommentsViewModel> PopulateManagerCommentsViewModel(Guid topicId, Guid? userId);
        ManagerCommentsViewModel PopulateManagerCommentViewModel(Guid topicId);

        void UpdateTopicManagerComment(ManagerCommentsViewModel model, Guid identityUserId);

        ManagerCommentsViewModel GetManagerCommentsViewModel(Guid topicId, Guid managerCommentId);

        void CarryforwardTopic(Guid topicId);

        bool IsMyTopic(Guid userId, Guid topicId);

        bool IsContributersTopicUser(Guid userId, Guid topicId);

        bool IsCanChangeTopic(Guid userId, Guid topicId);

        bool RemoveTopic(Guid topicId);

        ModuleType GetTopicModuleType(Guid topicid);

        ShiftCoreAddInlineTopicViewModel PopulateShiftCoreTopicInlineModel(Guid shiftId, Guid moduleId);

        void AddShiftCoreTopic(ShiftCoreAddInlineTopicViewModel model);

        ShiftHSEAddInlineTopicViewModel PopulateShiftHSETopicInlineModel(Guid shiftId, Guid moduleId);

        void AddShiftHSETopic(ShiftHSEAddInlineTopicViewModel model);

        ShiftProjectAddInlineTopicViewModel PopulateShiftProjectTopicInlineModel(Guid shiftId, Guid moduleId);

        void AddShiftProjectTopic(ShiftProjectAddInlineTopicViewModel model);

        ShiftTeamAddInlineTopicViewModel PopulateShiftTeamTopicInlineModel(Guid shiftId, Guid teamId);

        void AddShiftTeamTopic(ShiftTeamAddInlineTopicViewModel model);

        bool IsMyShiftTopic(Guid userId, Guid topicId);

        bool IsShiftTopic(Guid topicId);

        TeamTopicDialogViewModel PopulateShiftTeamTopicDialogModel(Guid topicId, Guid userId);


        TopicDetailsDialogViewModel GetModelForTopicDetailsModal(Guid topicId);

        void RemoveTopicManagerComment(Guid Id, Guid managerCommentId);

        int CommentCounterTopic(Guid topicId);

        int AttachCounterTopic(Guid topicId);
    }
}
