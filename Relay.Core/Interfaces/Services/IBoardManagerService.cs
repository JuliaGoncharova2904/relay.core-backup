﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IBoardManagerService
    {

        BoardManagerStepOneViewModel PopulateStepOneViewModel(BoardManagerStepOneViewModel model);

        IEnumerable<SelectListItem> GetPositions(Guid? siteId);

        IEnumerable<SelectListItem> GetTemplates();

        Guid CreateSitePositionAndTemplate(BoardManagerStepOneViewModel model);

        BoardManagerStepTwoViewModel PopulateStepTwoViewModel(Guid positionId);

        BoardManagerStepTwoViewModel PopulateStepTwoViewModel(BoardManagerStepTwoViewModel model);

        Guid CreateUser(BoardManagerStepTwoViewModel model, out Guid userTeamId);

        void UpdateUserPattern(Guid userId, string shiftPatternType);

        BoardManagerStepThreeViewModel PopulateStepThreeViewModel(Guid userId);

        BoardManagerStepThreeViewModel PopulateStepThreeViewModel(BoardManagerStepThreeViewModel model);

        bool CreateRotation(BoardManagerStepThreeViewModel model);


        SelectListItem GetPositionTemplate(Guid positionId);

        SelectListItem GetSelectedTeam(Guid teamId);
    }
}
