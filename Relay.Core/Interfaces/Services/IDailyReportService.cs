﻿using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IDailyReportService
    {
        DailyReportsPanelViewModel PopulateDailyReportsPanelViewModel(Guid userId, bool showSafetyMessages, string safetyVesion);

        DailyReportViewModel PopulateDailyReportViewModel(Guid shiftId, Guid currentUserId, string safetyVesion);

        int GetUnreadSafetyMessages(Guid userId, string safetyVesion);
    }
}
