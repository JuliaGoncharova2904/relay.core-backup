﻿using System;
using System.Collections.Generic;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITopicSearchService
    {
        List<SearchBarItem> GetBaseTopicGroups();
        List<SearchBarItem> GetTopicPeriod(int selectedPeriod);
        SearchResultsViewModel SearchMetricsTopics(Guid currentUserId, SearchViewModel model);
        SearchResultsViewModel SearchTopics(Guid currentUserId, SearchViewModel model);
        List<RotationTopic> GetTopicsBySearctText(Guid currentUserId, SearchViewModel model);
        TopicSearchResultViewModel SearchTopics(Guid currentUserId, TopicSearchViewModel model);
        List<RotationTopic> GetTopicMetricsBySearchText(Guid currentUserId, SearchViewModel model);
        RotationTopic SetTopicPVA(RotationTopic topic);
        TopicReportPdfViewModel PopulateTopicSearchReportViewModel(IEnumerable<Guid?> selectedTopicGroupsIds, IEnumerable<Guid?> selectedUserIds, TopicSearchDateRangeType? dateRangeType, int selectedPeriod = 0);
    }
}
