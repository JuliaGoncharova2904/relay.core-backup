﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IAttachmentService
    {
        List<AttachmentViewModel> GetAttachmentForTopic(Guid topicId);
        List<AttachmentViewModel>  GetAttachmentLinkForTopic(Guid topicId);

        List<AttachmentViewModel> GetAttachmentLinkForTask(Guid taskId);
        List<AttachmentViewModel> GetAttachmentForTask(Guid taskId);

        void AddAttachmentToTopic(AttachmentViewModel attachment, Guid creatorId);
        void AddAttachmentToTask(AttachmentViewModel attachment, Guid creatorId);

        void RemoveAttachmentFromTopic(Guid topicId, Guid attachmentId, Guid userId);
        void RemoveAttachmentFromTask(Guid taskId, Guid attachmentId);

        FileViewModel GetAttachment(Guid id);
    }
}
