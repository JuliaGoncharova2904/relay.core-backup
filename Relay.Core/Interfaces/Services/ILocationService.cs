﻿using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ILocationService
    {
        LocationViewModel GetLocationForTopic(Guid topicId);
        bool CreateLocationForTopic(Guid topicId, LocationViewModel model, Guid userId);
        bool UpdateLocation(LocationViewModel model);
    }
}
