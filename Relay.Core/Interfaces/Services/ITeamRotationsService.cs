﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITeamRotationsService
    {
        MyTeamRotationViewModel GetMyTeamRotations(Guid userId, bool isViewMode, int page, int pageSize, bool IdentityAdmin = false);
        bool IsActiveShiftOrRotation(Rotation rotation, Guid userId);
    }
}
