﻿using System;
using MomentumPlus.Relay.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITeamService
    {
        IEnumerable<TeamViewModel> GetAllTeams();

        IEnumerable<SelectListItem> GetTeamList();

        IEnumerable<SelectListItem> GetTeamListByCriticalControl(Guid criticalControlId);

        IEnumerable<SelectListItem> GetTeamMembersList(Guid teamId);

        IEnumerable<SelectListItem> GetTeamMembersListWidthOther(Guid teamId);

        TeammatesDialogViewModel PopulateTeammatesDialogViewModel(Guid userId, bool showMe, bool showOnSite, bool showOffSite, bool showWithoutRotation);

        string GetTeamName(Guid teamId);

        bool AddTeam(TeamViewModel team);

        TeamViewModel GetTeam(Guid teamId);

        bool UpdateTeam(TeamViewModel team);
    }
}
