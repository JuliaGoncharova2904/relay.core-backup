﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITaskLogService
    {
        IEnumerable<string> GetTaskLogs(Guid taskId);
    }
}
