﻿using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationService
    {

        EditRotationViewModel PopulateEditRotationModel(EditRotationViewModel model, Guid userId);
        CreateRotationViewModel PopulateEditRotationModel(CreateRotationViewModel model, Guid userId);
        IEnumerable<ReceivedHistorySlideViewModel> GetRotationsReceivedHistorySlidesByUser(Guid userId);
        IEnumerable<RotationHistorySlideViewModel> GetRotationsHistorySlidesByUser(Guid userId);

        Guid? GetCurrentRotationIdForUser(Guid userId);
        List<SearchBarItem> GetTagsOrTopicsForRotation(Guid userId);
        SummaryViewModel GetSummaryForRotation(Guid rotationId, Guid currentUserId, bool viewMode);
        SummaryViewModel GetSummaryForRotation(Rotation rotation, Guid currentUserId, bool viewMode);

        EditRotationViewModel GetRotation(Guid Id, Guid userId);
        Guid GetRotationOwnerId(Guid rotationId);
        MomentumPlus.Core.Models.RotationType GetRotationType(Guid rotationId);
        bool UpdateRotation(EditRotationViewModel model);
        void UpdateSwingRecipient(ChooseSwingRecipientModel model);
        void UpdateSwingRecipient(Guid rotationId, Guid rotationRecipientId, bool save = true);
        FromToBlockViewModel GetFromToBlock(Guid rotationId);
        FromToBlockViewModel GetFromToBlockShift(Guid shiftId);

        bool CreateFirstRotation(CreateRotationViewModel model);
        bool CheckRotationPatternCompatibility(Guid employeeId, ConfirmRotationViewModel model);
        bool ConfirmRotation(Guid employeeId, ConfirmRotationViewModel model, Guid identityUserId);
        bool RotationIsConfirmed(Guid rotationid);
        bool RotationIsConfirmed(Rotation rotation);

        ConfirmRotationViewModel PopulateConfirmModelWithCurrentRotation(Guid employeeId);

        string GetRotationOwnerFullName(Guid rotationId);

        void ChangeFinalizeStatusForDraftTopicsAndTasks(Guid rotationId, FinalizeStatus status);

        IEnumerable<ReceivedRotationSlideViewModel> GetRotationsWhoPopulateMyReceivedSection(Guid rotationId);

        bool ExpireRotation(Guid rotationId);

        bool IsRotationExpiredOrNotStarted(Guid rotationId);

        string GetRotationPeriod(Guid rotationId);
        string GetReceivedRotationPeriod(Guid? rotationId, Guid? sourceRotationId);
        Rotation GetRotation(Guid rotationId);

        Guid GetCurrentRotationId(Guid userAnyoneRotationId);

        Guid? GetRotationOwnerId(Guid? rotationId);

        Guid? GetRotationBackToBackId(Guid? rotationId);

        EditRotationDatesViewModel GetRotationDates(Guid rotationId);

        bool UpdateRotationDates(EditRotationDatesViewModel model);

        bool EndSwingOfRotation(Guid rotationId);

        bool IsRotationSwingEnd(Guid rotationId);
        bool IsRotationSwingEnd(Rotation rotation);

        bool IsShiftRotation(Guid rotationId);
        bool IsShiftRotation(Rotation rotation);

        bool IsRotationFirstShiftNotConfirmed(Guid rotationId);
        bool IsRotationFirstShiftNotConfirmed(Rotation rotation);

        IEnumerable<ShiftReceivedSlideViewModel> GetShiftsReceivedHistorySlidesByUser(Guid userId);

        bool IsRotationHavePrevRotation(Guid rotationId);
        bool IsRotationHavePrevRotation(Rotation rotationId);

        IEnumerable<RotationItemViewModel> GetRotationItemsForPosition(Guid positionId);

        ConfirmButtonViewModel ConfirmButton(Guid userId);

        ChooseSwingRecipientModel PopulateChooseSwingRecipientViewModel(Guid rotationId);

        bool IsExistTemplateTopicsPrevRotationOrShift(UserProfile user, Guid topicId);
        bool IsNotExistAliveRotationOrShift(Guid userId);

        bool IsShowConfirmButton(Guid userId);

        bool IsShowRotationEditDatesButton(Guid userId);
        bool IsShowShiftEditDatesButton(Guid userId);

        Rotation GetCurrentRotationForUser(Guid userId);

        bool EndSwing(Guid rotationId, byte[] pdfBytes);

        bool SwingHasRecipient(Guid rotationId);
    }
}
