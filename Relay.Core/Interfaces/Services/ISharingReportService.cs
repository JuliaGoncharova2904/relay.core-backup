﻿using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ISharingReportService
    {
        ShareReportUsersViewModel CountRecipientsBySourceId(Guid sourceId);
        ShareReportViewModel PopulateShareReportViewModel(Guid sourceId, RotationType rotationType);
        void ChangeShareReportRelations(ShareReportViewModel model, Guid ownerId);
    }
}
