﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IVoiceMessageService
    {
        IEnumerable<VoiceMessageViewModel> GetVoiceMessagesForTopic(Guid topicId);
        IEnumerable<VoiceMessageViewModel> GetVoiceMessagesForTask(Guid taskId);

        bool AddVoiceMessageToTopic(VoiceMessageViewModel voiceMessage);
        bool AddVoiceMessageToTask(VoiceMessageViewModel voiceMessage);

        bool RemoveVoiceMessageFromTopic(Guid topicId, Guid voiceMessageId);
        bool RemoveVoiceMessageFromTask(Guid taskId, Guid voiceMessageId);
    }
}
