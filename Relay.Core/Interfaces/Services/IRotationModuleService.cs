﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationModuleService
    {
        void ChangeTemplateModuleChildRotationModulesStatus(Guid templateModuleId, bool status);

        void ChangeRotationModuleStatus(Guid moduleId, bool status);

        IEnumerable<HSETopicViewModel> GetHseModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        IEnumerable<CoreTopicViewModel> GetCoreModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        IEnumerable<TeamTopicViewModel> GetTeamModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        IEnumerable<ProjectsTopicViewModel> GetProjectModuleTopics(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType,
            RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

        bool IsModuleEnabled(ModuleType moduleType, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing);

        bool IsTemplateModuleEnabled(ModuleType moduleType, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing);

        bool IsTemplateModuleReceivedEnabled(Guid receivedId, ModuleType moduleType, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing);

        IEnumerable<HSETopicViewModel> GetHseModuleTopicsForReceivedType(Guid identityUserId, Guid moduleOwnerId, ModuleSourceType sourceType, RotationType rotationType = RotationType.Swing, HandoverReportFilterType filter = HandoverReportFilterType.HandoverItems);

    }
}
