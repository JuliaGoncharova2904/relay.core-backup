﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationTaskService
    {
        RotationTaskViewModel PopulateTaskModel(RotationTaskViewModel model);

        RotationTaskDetailsViewModel GetTaskDetails(Guid taskId);

        IEnumerable<RotationTaskViewModel> GetTopicTasks(Guid topicId);
        void AddTopicTask(Guid topicId, RotationTaskViewModel task);
        bool IsFirstTopicTasks(Guid topicId);
        RotationTaskViewModel GetTask(Guid taskId);
        void UpdateTopicTask(RotationTaskViewModel task);
        bool RemoveTask(Guid taskId);

        bool IsTopicTaskExist(Guid topicId, string name);
        bool IsTaskNameExist(Guid taskId, string name);
        bool IsChildTaskComplited(Guid taskId);

        void UpdateRotationTopicTasksStatus(Guid rotationTopicId, bool status);

        void UpdateRotationTaskStatus(Guid rotationTaskId, bool status);

        void UpdateTemplateTaskChildTaskStatus(Guid templateTaskId, bool status);

        TasksTopicViewModel UpdateTaskTopic(TasksTopicViewModel taskTopic, Guid changedByUserId);

        TasksTopicViewModel GetTaskTopic(Guid taskId, Guid creatorTaskId);

        IEnumerable<TasksTopicViewModel> GetReceivedTasks(Guid rotationId);

        IEnumerable<TasksTopicViewModel> GetReceivedTasks(Guid rotationId, Guid userId);

        RotationTaskCompleteViewModel PopulateCompleteTaskViewModel(Guid taskId);

        ManagerCommentsViewModel PopulateManagerCommentViewModel(Guid taskId);

        void UpdateTaskManagerComment(ManagerCommentsViewModel model);

        void CarryforwardTask(Guid taskId);

        bool IsMyTask(Guid userId, Guid taskId);

        Guid GetTaskRecipientId(Guid taskId);

        IEnumerable<TasksTopicViewModel> GetAllRotationTasks(Guid identityUserId, Guid rotationId, ModuleSourceType sourceType);


        IEnumerable<TasksTopicViewModel> GetAllShiftTasks(Guid identityUserId, Guid shiftId, ModuleSourceType sourceType);

        RotationTaskViewModel GetShiftTask(Guid taskId);

        bool IsShiftTask(Guid taskId);

        IEnumerable<TasksTopicViewModel> GetShiftReceivedTasks(Guid rotationId);

        IEnumerable<TasksTopicViewModel> GetShiftReceivedTasks(Guid rotationId, Guid userId);

        void SaveCompleteTask(RotationTaskCompleteViewModel model);

        void EditReceivedTask(Guid currentUserId, RotationTaskViewModel model);
    }
}
