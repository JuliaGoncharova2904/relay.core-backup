﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRotationPatternService
    {

        IEnumerable<SelectListItem> GetRotationPatternsList();
        List<SelectListItem> GetShiftPatternsList();
        RotationPatternViewModel GetRotationPaternById(Guid Id);
        IEnumerable<RotationPatternViewModel> GetAllRotationPatterns();

        bool RotationPatternExist(RotationPatternViewModel model);
        void AddRotationpattern(RotationPatternViewModel model);
        void UpdateRotationPattern(RotationPatternViewModel model);
        void RemoveRotationpattern(Guid rotationPatternId);
        bool CanRemoveRotationPattern(Guid rotationPatternId);
    }
}
