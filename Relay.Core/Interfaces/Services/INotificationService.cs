﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;
using MvcPaging;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface INotificationService
    {
        IPagedList<NotificationMessageItemViewModel> GetNotificationsByUser(Guid userId, int page, int pageSize);

        IEnumerable<NotificationMessageItemViewModel> GetNotificationsByUserForPopover(Guid userId, int page, int pageSize);

        int CountNotShownNotificationsForUser(Guid userId);
        void MakeLastNotificationsShownForUser(Guid userId);
        void MakeNotificationOpenedForUser(Guid userId, Guid notificationId);
    }
}
