﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITemplateService
    {
        TemplateViewModel GetTemplateViewModel(Guid templateId);
        IEnumerable<SelectListItem> GetTemplateList();
        IEnumerable<SelectListItem> GetAdminTemplateList(Guid? sectorId);
        GlobalTemplatesViewModel GetGlobalTemplates();
        IEnumerable<SelectListItem> GetSectorTemplateList();

        TemplatesViewModel PopulateTemplatesViewModel(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null);

        TemplatesViewModel PopulateAdminTemplatesViewModel(
            string selectedTemplate,
            ModuleType? selectedModule,
            int? topicGroupsPageNo,
            int? topicsPageNo,
            int? tasksPageNo,
            Guid? topic = null,
            Guid? topicGroup = null, bool? isViewGlobalTemplates = null);

        void AddTemplate(TemplateViewModel model);
        void UpdateTemplate(TemplateViewModel model);
        bool TemplateExist(TemplateViewModel model);
        bool SectorExist(SectorViewModule model);
        List<SelectListItem> GetSectionsType();
        CopyTemplateViewModel GetCopyTemplateViewModel(Guid templateId);
        CopyTemplateViewModel CopyTemplateViewModel(CopyTemplateViewModel model);
        void AddExistTemplate();
        void CopyTopicsRelayTemplate(Guid templateFromId, Guid templateToId, List<TemplateModule> existTemplateModules, bool isExistTemplate = false);
        void AddRelayTemplate(Guid relayTemplateId);
    }
}
