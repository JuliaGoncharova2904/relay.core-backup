﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ICompanyService
    {
        IEnumerable<SelectListItem> GetCompaniesList();

        CompanyViewModel GetCompany(Guid companyId);

        IEnumerable<CompanyViewModel> GetCompanies();

        bool CompanyExist(CompanyViewModel model);

        void AddCompany(CompanyViewModel model);

        void UpdateCompany(CompanyViewModel model);

        void RemoveCompany(Guid companyId);

        bool IsDefaultCompanyExist(CompanyViewModel model);

        Guid? GetDefaultCompanyId();

        bool CanRemoveCompany(Guid companyId);

        bool IsExistDefaultCompany();
    }
}