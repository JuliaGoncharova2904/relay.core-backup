﻿using MomentumPlus.Relay.Models;
using System;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IContributorsService
    {
        ContributedUsersViewModel GetContributedUsers(Guid userId, int page, int pageSize);

        bool IsHaveContributedUser(Guid userId);
    }
}
