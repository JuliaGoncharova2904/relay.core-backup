﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IUserService
    {
        EmployeeViewModel PopulateEmployeeModel(Guid userId);
        EmployeeViewModel PopulateEmployeeModel(EmployeeViewModel model, Guid userId);

        IEnumerable<SelectListItem> GetEmployeesList();
        IEnumerable<EmployeeViewModel> GetAllEmployees();
        IEnumerable<EmployeeViewModel> GetEmployeesForPosition(Guid positionId);

        EmployeeViewModel GetEmployeeById(Guid Id, Guid userId);
    
        IEnumerable<SelectListItem> GetEmployeesForTeam(Guid teamId);

        void CreateEmployee(CreateEmployeeViewModel employeeViewModel, HttpPostedFileBase profileImage);

        IEnumerable<UserDetailsViewModel> GetUsersDetails();
        void RemoveUser(Guid userId);

        SecurityViewModel GetSecurityDetails(Guid personId);
        void UpdateSecurityDetails(Guid personId, SecurityViewModel model);

        PersonalDetailsViewModel GetPersonDetails(Guid personId);
        void UpdatePersonDetails(Guid personId, PersonalDetailsViewModel personDetails, HttpPostedFileBase foto);

        void UpdateEmployee(EmployeeViewModel employeeViewModel, HttpPostedFileBase profileImage);

        List<TeamForWidgetViewModel> GetTeamForWidjet(Guid? rotationId, Guid? userId);

        void UpdateLastLoginDate(Guid userId);

        void InitializeSubscriptionManagerProfile(SubscriptionDetailsModel model);

        void ReactivateUser(Guid userId, bool status);

        InlineManualPeopleTrackingViewModel GetUserInfoForInlineManual(Guid userId);

    }
}
