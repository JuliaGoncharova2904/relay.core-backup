﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models;
using MvcPaging;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IGlobalReportsService
    {
        IPagedList<HistoryGlobalReportItemViewModel> GetHistoryReportsItemsForUser(Guid userId, int? page, int? pageSize);
        IPagedList<ReceivedGlobalReportItemViewModel> GetReceivedReportsItemsForUser(Guid userId, int? page, int? pageSize);
        IPagedList<SharedGlobalReportItemViewModel> GetSharedReportsItemsForUser(Guid userId, int? page, int? pageSize);
        GlobalReportsListViewModel GetTeamReportsItemsForUser(Guid userId, Guid? filterUserId, RotationType? filterRotationType, int? page, int? pageSize);
        IPagedList<SharedTopicGlobalReportItemViewModel> GetSharedTopicItemsForUser(Guid userId, int? page, int? pageSize);
        GlobalReportsListViewModel GetGlobalReportsItemsForUser(Guid userId, Guid? filterUserId, RotationType? filterRotationType, int? page, int? pageSize);
    }
}
