﻿using MomentumPlus.Core.Models;
using System;

namespace MomentumPlus.Relay.Interfaces
{
    public interface IMailerService
    {
        void SendHandoverPreviewEmail(Guid rotationId);

        void SendShiftHandoverPreviewEmail(Guid shiftId);

        void SendRotationShareReportEmail(Guid rotationId);

        void SendShiftRecepintEmail(Shift shift, byte[] pdfBytes);

        void SendRecepientRotationReportEmail(Rotation rotation, byte[] pdfBytes);

    }
}