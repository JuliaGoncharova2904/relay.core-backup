﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IRestrictionService
    {
        void UsersLimitExceeded(Guid userId, string role);

        void UsersLimitExceededForDeactivateUsers(Guid userId, string role, int userCount);

        void HandoverItemsLimitExceeded(Guid destId, Models.RotationType rotationType);
        void HandoverTasksLimitExceeded(Guid topicId);
        //bool EmailSharesLimitExceeded(int emailSharesNumber);
        //bool IsAllowedBranding();
        void TopicsCarryForwardsLimitExceeded(Guid topicId);
        void TasksCarryForwardsLimitExceeded(Guid taskId);

        void HandoverItemsLimitExceededNew(Guid destId, Models.RotationType rotationType, int itemsNumber);
        List<RotationModule> GetModulesByRotationId(Guid destId, Models.RotationType rotationType);
    }
}
