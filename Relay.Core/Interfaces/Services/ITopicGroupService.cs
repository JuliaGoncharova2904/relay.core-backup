﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITopicGroupService
    {
        IEnumerable<rTopicGroupViewModel> GetModuleTopicGroupsViewModels(Guid moduleId);

        IEnumerable<rTopicGroupViewModel> PopulateTemplateModuleTopicGroupsViewModels(Guid baseModuleId, Guid templateModuleId);

        rTopicGroupViewModel GetTopicGroupViewModel(Guid topicGroupId);

        ProjectTopicGroupViewModel PopulateProjectTopicGroupViewModel(ProjectTopicGroupViewModel topicModel);

        TeamTopicGroupViewModel PopulateTeamTopicGroupViewModel(TeamTopicGroupViewModel topicModel);

        Guid AddTopicGroup(rTopicGroupViewModel model);

        void UpdateTopicGroup(rTopicGroupViewModel model);

        void UpdateTopicGroupStatus(Guid topicGroupId, bool status);
        void UpdateChildTopicGroupStatusForRemove(Guid baseTopicGroupId, Guid templateId, bool status);

        void UpdateChildTopicGroupStatus(Guid baseTopicGroupId, Guid templateId, bool status, TypeOfModule typeOfModule = TypeOfModule.HSE);

        bool TopicGroupExist(rTopicGroupViewModel model);

        bool CheckTopicGroupStatus(Guid topicGroupId);

        bool TopicGroupRelationExist(rTopicGroupViewModel model);

        void UpdateModuleTopicGroupsStatus(Guid moduleId, bool status);

        void RemoveTopicGroup(Guid topicGroupId);

        IEnumerable<SelectListItem> GetTemplateTopicGroups(ModuleType moduleType, Guid templateId);
        IEnumerable<SelectListItem> GetTemplateReferences(ModuleType moduleType);
        IEnumerable<SelectListItem> GetTemplateReferencesByTopicGroupId(ModuleType moduleType, Guid TopicGroupId);
        string GetNotesReferences(Guid? topicId);
        bool? GetMetricsReferences(Guid? topicId);
        SelectListItem GetUnitsReferences(Guid? topicId);
        TypeOfModule GetTypeOfModule(Guid topicGroupId);
        List<TemplateTopicGroup> GetTemplateTopicGroupsByTemplateId(ModuleType moduleType, Guid templateId);
    }
}
