﻿using MomentumPlus.Relay.Models;
using System;
using System.Web;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IMediaService
    {
        Guid AddFile(HttpPostedFileBase imageFile);

        FileViewModel GetImage(Guid imageId, int? width = null, int? height = null);

        FileViewModel GetProjectAvatarImage(Guid projectId, int? width, int? height);

        FileViewModel GetEmployeeAvatarImage(Guid employeeId, int? width, int? height);

        FileViewModel GetFile(Guid fileId);
    }
}
