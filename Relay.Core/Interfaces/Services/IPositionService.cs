﻿using MomentumPlus.Relay.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface IPositionService
    {
        PositionViewModel PopulatePositionModel();
        PositionViewModel PopulatePositionModel(PositionViewModel model);

        PositionViewModel GetPosition(Guid positionId);
        bool UpdatePosition(PositionViewModel model);
        IEnumerable<PositionViewModel> GetPositionsForSite(Guid siteId);
        PositionViewModel GetPositionForEmployee(Guid employeeId);

        bool AddPosition(PositionViewModel position);

        IEnumerable<SelectListItem> GetPositionsList();

        bool CanRemovePosition(Guid positionId);

        void RemovePosition(Guid positionId);

    }
}
