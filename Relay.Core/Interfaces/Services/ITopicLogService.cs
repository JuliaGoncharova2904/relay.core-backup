﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Services
{
    public interface ITopicLogService
    {
        IEnumerable<string> GetTopicLogs(Guid topicId);
    }
}
