﻿using Owin;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Auth
{
    public interface IAuthBridge
    {
        string GetActiveRelayDbConnectionString();
        IEnumerable<string> GetAllRelayDbConnectionStrings();
        void RegisterAuthModule(IAppBuilder app);

        IAuthService AuthService { get; }
        IRestrictionProvider RestrictionProvider { get; }
    }
}
