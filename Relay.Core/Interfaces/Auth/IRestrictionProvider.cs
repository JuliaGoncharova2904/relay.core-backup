﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Interfaces.Auth
{
    public interface IRestrictionProvider
    {
        bool UsersLimitExceeded(Guid userId);
        bool UsersLimitExceededForActivate(Guid userId);
        bool UsersLimit(Guid userId, int userCount);
        bool IsLockUser(Guid userId);
        bool ManagersLimitExceeded(Guid userId);
        bool HandoverItemsLimitExceeded(int handoverItemsNumber);
        bool HandoverTasksLimitExceeded(int handoverTasksNumber);
        //bool EmailSharesLimitExceeded(int emailSharesNumber);
        bool CarryForwardsLimitExceeded(int carryForwardsNumber);
        //bool IsAllowedBranding();

        int MonthArchivesLimit();
        bool IsMonthArchivesLimited();
    }
}
