﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using MomentumPlus.Core.Models;
using MomentumPlus.Relay.Authorization.Domain.Entities;
using MomentumPlus.Relay.Models;

namespace MomentumPlus.Relay.Interfaces.Auth
{
    public interface IAuthService
    {
        bool AddUserToRole(Guid userId, string role);
        bool RemoveUserFromRole(Guid userId, string role);
        IEnumerable<string> GetUserRoles(Guid userId);
        IEnumerable<string> GetAllRolesAccessedByUser(Guid userId);
        DateTime? GetLastLoginDateForUser(Guid userId);
        Guid GetUserIdByLogin(string login);
        Guid GetUserIdBySubscriptionId(Guid? subscriptionId);
        bool GetAccessToPageUpgrade(Guid userId);
        IEnumerable<Guid> GetUsersIdsByRole(string role, Guid requestOwnerId);
        IEnumerable<Guid> GetUsersIdsByRoles(IEnumerable<string> roles, Guid requestOwnerId);

        bool CreateUser(string login, string password, bool needEmailConfirm = false);

        bool RemoveUser(Guid userId, Guid initiatorId);
        bool ChangeUserLogin(Guid userId, string newLogin);
        bool ChangeUserPassword(Guid userId, string password, string newPassword);
        bool ResetUserPassword(Guid userId, string newPassword);

        void UpdateLastLoginDateForUser(Guid userId);

        SubscriptionDetailsModel GetSubscriptionDetailsModel();

        UpgradeSubscriptionViewModel GetUpgradeSubscriptionViewModel();

        bool SubscriptionMaxUserCanChange(int newUserNumber);

        bool UpgradeSubscription(UpgradeSubscriptionViewModel model);

        void ReactivateUser(Guid userId, bool status);

        bool IsLockUser(Guid userId);
        Guid? GetUserSubscriptionId(Guid userId);
        void SendMail(Guid userId, string userName, string urlUdtatePassword);
        void SendAutoRenewSubscriptionMail(Guid userId, string emailOfSubscription);
        string GetSecurityStampForUpdatePassword(Guid userId);
        int? NumberOfRemainingTrialDays(Guid userId);
        bool IsTrialSubscription(Guid userId);
        bool ContributorEnable(Guid userId);
        RestrictionType? GetSubscriptionType(Guid userId);
        void CancelSubscriptionAtPeriodEnd(bool autoRenewSubscription);

        string GetTimeZoneFromSubscription(Guid userId);

        int? GetSubscriptionHandoverItemsLimitExceeded();

        Guid? GetSubscriptionOwnerId();

        void AddSector(SectorViewModule model);
        void AddTemplate(TemplateViewModel model);

        IEnumerable<SelectListItem> GetAdminSectors();
        IEnumerable<SelectListItem> GetAdminTemplates(Guid? sectorId);

        AdminTemplates GetAdminTemplateById(Guid templateId);

        List<RelayUser> GetAdminsOfSunscription();
        void AddExistTemplate(Template template);

          Guid AddTopicGroup(rTopicGroupViewModel model, Authorization.Domain.Entities.TypeOfModule type);
          Guid AddTopic(rTopicViewModel model, Authorization.Domain.Entities.TypeOfModule type);
          IEnumerable<SelectListItem> GetAdminTopicGroupsByType(Authorization.Domain.Entities.TypeOfModule moduleType);
          IEnumerable<SelectListItem> GetAdminTopicsByType(Authorization.Domain.Entities.TypeOfModule moduleType);
          Guid? GetSectorByTemplateId(Guid? templateId);
          List<AdminTemplateTopicGroups> GetAdminTopicGroups(Authorization.Domain.Entities.TypeOfModule moduleType, Guid templateId);
         void CopyRelayRotations(Rotation rotation);

        Rotation GetRotation(Guid rotationId);
        RotationModule GetRelayRotationModule(Core.Models.TypeOfModule type, Guid rotationId, Core.Models.TypeOfModuleSource sourceType);
        List<RotationTopicGroup> GetRelayRotationTopicGroups(Guid rotationId, Guid moduleId, bool IsShift = false);
        List<RotationTopic> GetRelayRotationTopics(Guid topicGroupId);
        string GetNameRelayRotationTopicGroup(Guid topicGroupId);

        string GetDefaultBackToBackName(Guid rotationId);
        string GetOwnerRotationName(Guid rotationId);
        List<RotationTask> GetRotationTask(Guid rotationId);
        void CopyRelayRotationTasks(RotationTask rotationTask, Guid rotationId);
        void CopyRelayShifts(Shift shift);
        Shift GetShift(Guid shiftId);
        string GetShiftDefaultBackToBackName(Guid shiftId);
        string GetShiftGetOwnerRotationName(Guid shiftId);
        RotationModule GetShiftRelayRotationModule(Core.Models.TypeOfModule type, Guid shiftId, Core.Models.TypeOfModuleSource sourceType);
        void CopyRelayAdminSettings(AdminSettings adminSettings, Guid rotationId);
        File GetRelayFileLogo(Guid rotationId);
        Guid? GetIdAdminOfSubscription();
    }
}
