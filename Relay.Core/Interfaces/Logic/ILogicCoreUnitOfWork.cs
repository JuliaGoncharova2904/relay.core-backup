﻿using MomentumPlus.Core.Interfaces;

namespace MomentumPlus.Relay.Interfaces.Logic
{
    public interface ILogicCoreUnitOfWork
    {
        IRepositoriesUnitOfWork Repositories { get; }

        INotificationAdapter NotificationAdapter { get; }
        IMailerService MailerService { get; }
    }
}
