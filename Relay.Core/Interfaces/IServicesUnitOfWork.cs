﻿using MomentumPlus.Relay.Interfaces.Auth;
using MomentumPlus.Relay.Interfaces.Services;

namespace MomentumPlus.Relay.Interfaces
{
    public interface IServicesUnitOfWork
    {
        IUserService UserService { get; }
        ITeamService TeamService { get; }
        IRotationService RotationService { get; }
        ISiteService SiteService { get; }
        IPositionService PositionService { get; }
        IMediaService MediaService { get; }
        IAttachmentService AttachmentService { get; }
        IVoiceMessageService VoiceMessageService { get; }
        IRotationPatternService RotationPatternService { get; }
        IDailyNoteService DailyNoteService { get; }
        IModuleService ModuleService { get; }
        ITopicGroupService TopicGroupService { get; }
        ITopicService TopicService { get; }
        ITaskService TaskService { get; }

        IProjectService ProjectService { get; }

        ITemplateService TemplateService { get; }

        IRotationModuleService RotationModuleService { get; }
        IRotationTopicGroupService RotationTopicGroupService { get; }
        IRotationTopicService RotationTopicService { get; }
        IRotationTaskService RotationTaskService { get; }

        ILocationService LocationService { get; }

        IReportService ReportService { get; }
        // safety
        IMajorHazardService MajorHazardService { get; }
        ICriticalControlService CriticalControlService { get; }
        ISafetyMessageService SafetyMessageService { get; }
        // safety v2
        IMajorHazardV2Service MajorHazardV2Service { get; }
        ICriticalControlV2Service CriticalControlV2Service { get; }
        ISafetyMessageV2Service SafetyMessageV2Service { get; }
        ISafetyStatV2Service SafetyStatV2Service { get; }
        //
        IAdministrationService AdministrationService { get; }

        IAccessService AccessService { get; }
        ITimelineService TimelineService { get; }
        INotificationService NotificationService { get; }
        ISharingTopicService SharingTopicService { get; }
        IShiftService ShiftService { get; }
        IDailyReportService DailyReportService { get; }

        ISchedulerService SchedulerService { get; }
        IGlobalReportsService GlobalReportService { get; }

        ISharingReportService SharingReportService { get; }

        ICompanyService CompanyService { get; }

        ITaskBoardService TaskBoardService { get; }

        ITeamRotationsService TeamRotationsService { get; }

        ITaskLogService TaskLogService { get; }

        IAuthService AuthService { get; }

        IRestrictionService RestrictionService { get; }
        ITopicSearchService TopicSearchService { get; }

        ISearchService SearchService { get; }

        IBoardManagerService BoardManagerService { get; }

        IContributorsService ContributorsService { get; }

        ITopicLogService TopicLogService { get; }


        ITaskBoardTaskService TaskBoardTaskService { get; }
    }
}
