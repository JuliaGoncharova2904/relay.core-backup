﻿using System;
using MomentumPlus.Relay.Models.Api;

namespace MomentumPlus.Relay.Interfaces.ApiServices
{
    public interface IHomeService
    {
        HomePageViewModel PopulateHomePage(Guid userId);
    }
}
