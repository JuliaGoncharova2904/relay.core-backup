﻿using System;
using MomentumPlus.Relay.Helpers;
using MomentumPlus.Relay.Models.Api;

namespace MomentumPlus.Relay.Interfaces.ApiServices
{
    public interface INotificationService
    {
        PagedResult<NotificationViewModel> GetNotificationsByUser(Guid userId, int page, int pageSize);

        void MakeLastNotificationsShownForUser(Guid userId);
    }
}
