﻿using System;
using MomentumPlus.Relay.Models.Api;

namespace MomentumPlus.Relay.Interfaces.ApiServices
{
    public interface IProfileService
    {
        ProfilePageViewModel PopulateProfileInfo(Guid userId);

        string UpdateAvatar(string base64ImageData, Guid userId);
    }
}
