﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SubscriptionDetailsModel
    {
        public Guid ManagerId { get; set; }

        public string ManagerEmail { get; set; }

        public string ManagerName { get; set; }

        public string ManagerSurname { get; set; }

        public string ManagerCompany { get; set; }

        public string ManagerOfficeLocation { get; set; }

        public string ManagerPosition { get; set; }

        public string PlanType { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int MaxUsers { get; set; }

        public int MaxManagers { get; set; }

        public bool? CancelAtPeriodEnd { get; set; }

        public string TimeZone { get; set; }
    }
}
