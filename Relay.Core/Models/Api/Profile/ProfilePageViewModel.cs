﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models.Api
{
    public class ProfilePageViewModel
    {
        public string Avatar { get; set; }

        public string Title { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public string Company { get; set; }

        public string Position { get; set; }

        public string Team { get; set; }
    }
}
