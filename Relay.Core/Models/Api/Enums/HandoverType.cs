﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models.Api.Enums
{
    public enum HandoverType
    {
        [Description("None")]
        None = 0,
        [Description("Swing")]
        Swing = 1,
        [Description("Shift")]
        Shift = 2
    }
}
