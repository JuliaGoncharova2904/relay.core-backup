﻿using System;

namespace MomentumPlus.Relay.Models.Api
{
    public class NotificationViewModel
    {
        public Guid Id { get; set; }

        public string Message { get; set; }

        public string ImageUrl { get; set; }

        public DateTime DateTime { get; set; }

        public bool IsOpened { get; set; }

        public string TypeClass { get; set; }

        public Guid? RelationId { get; set; }
    }
}
