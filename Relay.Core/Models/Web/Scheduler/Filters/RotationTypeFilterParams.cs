﻿namespace MomentumPlus.Relay.Models.Scheduler.Filters
{
    public enum RotationTypeFilterParams
    {
        Off = 0,
        Swing = 1,
        Shift = 2
    }
}
