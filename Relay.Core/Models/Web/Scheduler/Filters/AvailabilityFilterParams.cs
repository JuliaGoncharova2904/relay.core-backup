﻿namespace MomentumPlus.Relay.Models.Scheduler.Filters
{
    public enum AvailabilityFilterParams
    {
        Off = 0,
        Available = 1,
        Unavailable = 2
    }
}
