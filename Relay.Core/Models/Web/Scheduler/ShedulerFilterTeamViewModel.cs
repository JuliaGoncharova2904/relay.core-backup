﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ShedulerFilterTeamViewModel
    {
        public Guid TeamId { get; set; }
        public string TeamName { get; set; }
    }
}
