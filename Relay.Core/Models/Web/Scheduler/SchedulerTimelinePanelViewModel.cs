﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SchedulerTimelinePanelViewModel
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string LastUpdate { get; set; }
        public TimelineViewModel Timeline { get; set; }
    }
}
