﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class SchedulerPanelViewModel
    {
        public Guid UserId;
        public bool IsSimpleUserMpode;
        public List<SchedulerTimelinePanelViewModel> Timelines;
    }
}
