﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class SchedulerFilterPanelViewModel
    {
        public Guid UserId { get; set; }
        public bool AllTeamsBtn { get; set; }
        public IEnumerable<ShedulerFilterTeamViewModel> Teams { get; set; }
    }
}
