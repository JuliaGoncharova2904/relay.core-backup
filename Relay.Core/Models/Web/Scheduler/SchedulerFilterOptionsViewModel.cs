﻿using System;
using System.Collections.Generic;
using MomentumPlus.Relay.Models.Scheduler.Filters;

namespace MomentumPlus.Relay.Models
{
    public class SchedulerFilterOptionsViewModel
    {
        public AvailabilityFilterParams availability { get; set; }
        public RotationTypeFilterParams rotationType { get; set; }

        public bool allTeams { get; set; }
        public IEnumerable<Guid> teamsIds { get; set; }
    }
}
