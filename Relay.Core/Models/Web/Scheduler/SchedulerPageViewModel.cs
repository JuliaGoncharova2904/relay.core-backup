﻿namespace MomentumPlus.Relay.Models
{
    public class SchedulerPageViewModel
    {
        public SchedulerFilterPanelViewModel FilterPanel { get; set; }
        public SchedulerPanelViewModel SchedulerPanel { get; set; }
    }
}
