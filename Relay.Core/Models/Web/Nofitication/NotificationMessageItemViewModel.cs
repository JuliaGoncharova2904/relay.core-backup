﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class NotificationMessageItemViewModel
    {
        public Guid Id { get; set; }
        public string Message { get; set; }
        public string UrlImage { get; set; }
        public DateTime Timestapm { get; set; }
        public bool IsOpened { get; set; }
        public string TypeClass { get; set; }
        public Guid RelationId { get; set; }
        public Guid? ShiftId { get; set; }
        public Guid? SwingId { get; set; }
    }
}
