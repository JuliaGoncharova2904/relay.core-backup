﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class DropDownNotificationListModel
    {
        public IEnumerable<NotificationMessageItemViewModel> Notifications { get; set; }
    }
}
