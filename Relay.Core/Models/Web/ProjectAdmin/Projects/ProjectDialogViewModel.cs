﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ProjectDialogViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Project Name*")]
        [StringLength(35, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        [StringLength(250)]
        public string Description { get; set; }

        [Required]
        [Display(Name = "Start Date*")]
        public DateTime? StartDate { get; set; }

        [Required]
        [Display(Name = "End Date*")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Status")]
        public string ProjectStatus { get; set; }

        [Required]
        [Display(Name = "Project Manager*")]
        public Guid ProjectManagerId { get; set; }
        public IEnumerable<SelectListItem> Employees { get; set; }
    }
}
