﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ProjectPanelViewModel
    {
        public bool CanCreateProject { get; set; }
        public IEnumerable<ProjectSlideViewModel> Projects { get; set; }
    }
}
