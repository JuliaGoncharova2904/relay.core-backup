﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ProjectFollowerViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
        public string Team { get; set; }
        public bool OnSite { get; set; }
        public bool Select { get; set; }
    }
}
