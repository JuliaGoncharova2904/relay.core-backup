﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ProjectTeamViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}
