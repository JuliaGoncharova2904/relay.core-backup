﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ProjectFollowersTabViewModel
    {
        public Guid ProjectId { get; set; }
        public IEnumerable<ProjectFollowerViewModel> Followers { get; set; }
        public IEnumerable<Guid> SelectedFollowers { get; set; }
        public IEnumerable<Guid> UnselectedFollowers { get; set; }
    }
}
