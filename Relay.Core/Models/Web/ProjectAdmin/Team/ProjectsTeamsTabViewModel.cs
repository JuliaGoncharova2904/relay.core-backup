﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ProjectsTeamsTabViewModel
    {
        public Guid ProjectId { get; set; }
        public IEnumerable<ProjectTeamViewModel> Teams { get; set; }
        public IEnumerable<Guid> SelectedTeams { get; set; }
        public IEnumerable<Guid> UnselectedTeams { get; set; }
    }
}
