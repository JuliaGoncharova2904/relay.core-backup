﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ProjectTeamPanelViewModel
    {
        public int TotalFolowersCounter { get; set; }

        public IEnumerable<ProjectFollowerViewModel> Followers { get; set; }
    }
}
