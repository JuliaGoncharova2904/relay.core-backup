﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum ProjectTasksFilterOptions
    {
        [Description("All Tasks")]
        AllTasks = 0,

        [Description("All Active Tasks")]
        AllActiveTasks = 1,

        [Description("Incomplete Tasks")]
        IncompleteTasks = 2,

        [Description("Completed Tasks")]
        CompletedTasks = 3,

        [Description("Pending Tasks")]
        PendingTasks = 4,

        [Description("Archived Tasks")]
        ArchivedTasks = 5
    }
}
