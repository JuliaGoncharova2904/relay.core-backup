﻿namespace MomentumPlus.Relay.Models
{
    public class ProjectAdminCompleteTaskDialogViewModel
    {
        public ProjectAdminProjectDetailsTabViewModel ProjectDetailsTab { get; set; }

        public ProjectAdminCompleteTaskDetailTabViewModel TaskDetailTab { get; set; }

        public ProjectAdminOutcomesTabViewModel OutcomesTab { get; set; }

        public int SelectedTabNumber { get; set; }
        public CompleteDialogOption CompleteOption { get; set; }
    }
}
