﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ProjectAdminEditTaskDetailTabViewModel
    {
        public Guid TaskId { get; set; }

        public Guid OwnerId { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        [StringLength(500)]
        public string Notes { get; set; }

        [Required(ErrorMessage = "Assignee is required")]
        [Display(Name = "Assignee")]
        public Guid? AssignedTo { get; set; }

        [Required]
        [Display(Name = "Due Date")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Send Date")]
        public DateTime? SendDate { get; set; }

        public DateTime StartLimitDate { get; set; }

        [Required]
        [Display(Name = "Priority*")]
        public TaskPriority Priority { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        [Display(Name = "Sending Option")]
        public TaskSendingOptions SendingOption { get; set; }


        [Display(Name = "Add to handover")]
        public bool AddToReport { get; set; }

        public bool CanAddToReport { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach (string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
    }
}
