﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ProjectAdminCompleteTaskDetailTabViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        public string TaskNotes { get; set; }

        public Guid? AssignedFrom { get; set; }

        [Display(Name = "Due Date")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Priority")]
        public TaskPriority Priority { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach(string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
    }
}
