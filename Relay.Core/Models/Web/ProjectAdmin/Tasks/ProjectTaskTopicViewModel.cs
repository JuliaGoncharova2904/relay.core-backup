﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ProjectTaskTopicViewModel
    {
        public Guid Id { get; set; }
        public bool ReadOnly { get; set; }
        public bool IsFeedbackRequired { get; set; }
        public string Name { get; set; }
        public string CompleteStatus { get; set; }
        public string Priority { get; set; }
        public string Deadline { get; set; }
        public Guid? TeammateId { get; set; }
        public bool IsInArchive { get; set; }
        public bool HasAttachments { get; set; }
        public bool HasVoiceMessages { get; set; } 
    }
}
