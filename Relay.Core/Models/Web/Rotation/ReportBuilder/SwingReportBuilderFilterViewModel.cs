﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SwingReportBuilderFilterViewModel
    {
        public Guid Id { get; set; }

        public HandoverReportFilterType Filter { get; set; }
    }
}
