﻿using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class RotationReportPanelViewModel
    {
        public Guid UserId { get; set; }

        public HandoverReportFilterType FilterType { get; set; }

        public bool ContributorEnabled { get; set; }

        public bool IconsPDFEnabled { get; set; }

        public bool ShareEnabled { get; set; }

        public List<DropDownListItemModel> UsersList { get; set; }

        public bool? IsEndRotation { get; set; }

        public Guid? RotationId { get; set; }
    }
}
