﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class HistoryPanelViewModel
    {
        public Guid? SelectedRotationId { get; set; }
        public string RotationPeriod { get; set; }

        public IEnumerable<RotationHistorySlideViewModel> Rotations { get; set; }
    }
}
