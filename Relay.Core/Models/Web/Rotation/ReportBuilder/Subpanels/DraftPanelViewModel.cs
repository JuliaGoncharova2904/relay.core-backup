﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class DraftPanelViewModel
    {
        public Guid RotationId { get; set; }

        public string RotationPeriod { get; set; }

        public string EmptyPanelMessage { get; set; }

        public HandoverReportFilterType Filter { get; set; }
    }
}
