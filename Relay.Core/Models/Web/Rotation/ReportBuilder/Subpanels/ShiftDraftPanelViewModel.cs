﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ShiftDraftPanelViewModel
    {
        public Guid ShiftId { get; set; }

        public Guid RotationId { get; set; }

        public string ShiftPeriod { get; set; }

        public bool IsCurrentShift { get; set; }

        public string HandoverRecipientName { get; set; }

        public HandoverReportFilterType Filter { get; set; }

        public bool EnableHSESection { get; set; }
    }
}
