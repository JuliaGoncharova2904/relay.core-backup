﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ReceivedPanelViewModel
    {
        public Guid? SelectedRotationId { get; set; }
        public Guid? ReceivedRotationId { get; set; }
        public string RotationPeriod { get; set; }

        public IEnumerable<ReceivedHistorySlideViewModel> Rotations { get; set; }
        public IEnumerable<ReceivedRotationSlideViewModel> ReceivedRotations { get; set; }
    }
}
