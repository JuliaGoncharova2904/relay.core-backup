﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class RotationHistorySlideViewModel
    {
        public Guid Id { get; set; }
        public string Date { get; set; }
        public string Contributors { get; set; }
        public string HandoverTo { get; set; }
        public int DraftTopicCounter { get; set; }
        public int DraftTaskCounter { get; set; }
        public List<DropDownListItemModel> ContributorsList { get; set; }
    }
}
