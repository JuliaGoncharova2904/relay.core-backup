﻿using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ShiftReportPanelViewModel
    {
        public Guid? UserId { get; set; }

        public Guid? ShiftId { get; set; }

        public HandoverReportFilterType FilterType { get; set; }

        public bool ContributorEnabled { get; set; }

        public bool IconsPDFEnabled { get; set; }

        public bool ShareEnabled { get; set; }

        public string HandoverCreatorOrRecipientName { get; set; }

        public string ShiftType { get; set; }

        public bool? IsActiveShift { get; set; }
    }
}
