﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class ShiftReportBuilderFilterViewModel
    {
        public Guid Id { get; set; }

        public HandoverReportFilterType Filter { get; set; }
    }
}
