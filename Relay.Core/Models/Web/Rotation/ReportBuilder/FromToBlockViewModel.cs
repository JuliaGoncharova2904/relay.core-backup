﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class FromToBlockViewModel
    {
        public Guid? RotationOwnerId { get; set; }
        public string RotationOwnerFullName { get; set; }
        public Guid? RotationBackToBackId { get; set; }
        public string RotationBackToBackFullName { get; set; }
        public Guid? ShiftRecipientId { get; set; }
        public string ShiftRecipientFullName { get; set; }
        public IEnumerable<DropDownListItemModel> ContributorsUsersList { get; set; }
        public List<DropDownListItemModel> UsersList { get; set; }
    }
}
