﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ReportTopicViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool? IsPinned { get; set; }
        public bool IsNullReport { get; set; }
        public string TopicGroupName { get; set; }
        public bool IsFinalized { get; set; }
        public bool IsFeedbackRequired { get; set; }
        public Guid? FromTeammateId { get; set; }
        public Guid? FromRotationId { get; set; }
        public bool HasComments { get; set; }
        public bool HasTasks { get; set; }
        public bool HasAttachments { get; set; }
        public bool HasVoiceMessages { get; set; }
        public bool HasLocation { get; set; }
        public Guid? RelationId { get; set; }
        public string Notes { get; set; }
        public IEnumerable<ReportTaskViewModel> Tasks { get; set; } 
        public string UserName { get; set; }
    }
}
