﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class ReportShiftModuleViewModel
    {
        public string Name { get; set; }

        public IEnumerable<ReportShiftTopicViewModel> Topics { get; set; }
    }
}
