﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class EditDatesButtonViewModel
    {
        public bool IsShowShiftEditDatesButton { get; set; }
        public bool IsShowRotationEditDatesButton { get; set; }
        public Guid? UserId { get; set; }
    }
}
