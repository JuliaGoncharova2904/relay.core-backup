﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ChooseSwingRecipientModel
    {
        [Required]
        public Guid RotationId { get; set; }

        [Required(ErrorMessage = "Please choose swing recipient")]
        public Guid? RotationRecipientId { get; set; }

        public IEnumerable<SelectListItem> TeamMembers { get; set; }
    }
}
