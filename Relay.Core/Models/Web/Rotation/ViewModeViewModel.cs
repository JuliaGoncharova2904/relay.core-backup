﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ViewModeViewModel
    {
        public string RotationOwnerFullName { get; set; }
        public string RotationOwnerName { get; set; }
        public bool IsViewMode { get; set; }
        public Guid? UserId { get; set; }
        public bool HaveRotation { get; set; }
    }
}
