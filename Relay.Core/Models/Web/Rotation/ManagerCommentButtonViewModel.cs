﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class ManagerCommentButtonViewModel
    {
        public Guid? ItemId { get; set; }

        public bool IsActive { get; set; }

        public bool HasComments { get; set; }
    }
}
