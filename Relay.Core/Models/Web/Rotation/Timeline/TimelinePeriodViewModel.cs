﻿namespace MomentumPlus.Relay.Models
{
    public class TimelinePeriodViewModel
    {
        public int DaysCount;
        public int FirstDayNumber;
        public int LastDayNumber;
        public int CurrentDayOffset;
        public TimeLinePeriodType Type;
        public int[] Holidays;

        public bool prevPeriodIsDayOn;
        public bool nextPeriodIsDayOn;
    }
}
