﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TimelineMonthViewModel
    {
        public int Month;

        public string MonthLabel;
        public bool IsCurrentMonth;
        public List<TimelinePeriodViewModel> Periods;
    }
}
