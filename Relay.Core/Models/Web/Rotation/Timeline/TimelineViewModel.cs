﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TimelineViewModel
    {
        public List<TimelineMonthViewModel> Months { get; set; }
    }
}
