﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class RotationCoreAddInlineTopicViewModel
    {
        public Guid? RotationId { get; set; }

        public Guid CreatorId { get; set; }

        [Required]
        public Guid? ProcessGroup { get; set; }
        public IEnumerable<SelectListItem> ProcessGroups { get; set; }

        [Required]
        [Display(Name = "Reference")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string ProcessLocation { get; set; }

        [StringLength(1000, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Notes { get; set; }
    }
}
