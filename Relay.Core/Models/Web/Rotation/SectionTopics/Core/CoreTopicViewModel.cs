﻿namespace MomentumPlus.Relay.Models
{
    public class CoreTopicViewModel : BaseTopicViewModel
    {
        public string Category { get; set; }

        public string Reference { get; set; }
    }
}
