﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TasksTopicViewModel : BaseTopicViewModel
    {
        public string Location { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public TaskPriority Priority { get; set; }

        public string Deadline { get; set; }

        public string CompleteStatus { get; set; }

        public string ChildCompleteStatus { get; set; }

        public string SectionType { get; set; }

        public bool IsComplete { get; set; }

        public bool IsRotationMode { get; set; }
    }
}
