﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TaskLogsViewModel
    {
        public IEnumerable<string> LogsRow { get; set; }
    }
}
