﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ShiftProjectAddInlineTopicViewModel
    {
        public Guid? ShiftId { get; set; }

        public Guid CreatorId { get; set; }

        [Required]
        public Guid? Project { get; set; }
        public IEnumerable<SelectListItem> Projects { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Reference { get; set; }

        [StringLength(1000, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Notes { get; set; }
    }
}
