﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardTaskForHandover
    {
        public Guid Id { get; set; }

        public Guid OwnerId { get; set; }

        public string Name { get; set; }

        public string Notes { get; set; }

        public string Status { get; set; }

        public TaskPriority Priority { get; set; }

        public string Deadline { get; set; }

        public string CompleteStatus { get; set; }

        public bool IsComplete { get; set; }

        public bool HasAttachments { get; set; }

        public bool HasVoiceMessages { get; set; }

        public bool HasComments { get; set; }

        public bool IsFeedbackRequired { get; set; }

        public Guid? AssignedToId { get; set; }

        public Guid? RelationId { get; set; }

        public Guid? FromTeammateId { get; set; }

        public string FromTeammateFullName { get; set; }
        public Guid? TeammateId { get; set; }

        public bool IsFinalized { get; set; }

    }
}
