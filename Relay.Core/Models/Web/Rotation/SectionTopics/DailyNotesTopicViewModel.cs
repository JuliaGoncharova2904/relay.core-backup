﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class DailyNotesTopicViewModel : BaseTopicViewModel
    {
        public string Date { get; set; }
        public DateTime OriginalDate { get; set; }
    }
}
