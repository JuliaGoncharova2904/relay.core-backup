﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class BaseTopicViewModel
    {
        public Guid? Id { get; set; }

        public Guid OwnerId { get; set; }

        public bool IsOwnerOrCreator { get; set; }

        public Guid? CreatorId { get; set; }

        public bool IsContributor { get; set; }

        public bool IsNullReport { get; set; }

        public bool IsCustom { get; set; }

        public int CarryforwardCounter { get; set; } 

        public bool IsFeedbackRequired { get; set; }

        public bool? IsPinned { get; set; }

        public bool HasComments { get; set; }

        public string Notes { get; set; }

        public Guid? RelationId { get; set; }

        public Guid? TeammateId { get; set; }

        public Guid? FromTeammateId { get; set; }

        public string FromTeammateFullName { get; set; }

        public Guid? FromRotationId { get; set; }

        public Guid? FromShiftId { get; set; }

        public bool HasLocation { get; set; }

        public bool HasTasks { get; set; }

        public bool HasAttachments { get; set; }

        public bool HasVoiceMessages { get; set; }

        public bool IsFinalized { get; set; }

        public bool IsSharingTopic { get; set; }

        public int SharedCounter { get; set; }

        public List<SelectListItem> EditorUsers { get; set; }
       
        public string TagsString { get; set; }

        public string FirstTagTopic { get; set; }

        public bool? MoreTags { get; set; }

        public bool? HasTags { get; set; }

        public bool? PlannedActualHas { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Field must be numeric")]
        public string Planned { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Field must be numeric")]
        public string Actual { get; set; }

        public string Variance { get; set; }

        public Guid? UnitsTypeId { get; set; }

        public string UnitsSelectedType { get; set; }

        public PlannedActual PlannedActualField { get; set; }

        public bool? IsExpandPlannedActualField { get; set; }

        public bool? IsFirstTopicByTopicGroupName { get; set; }

        public TagsTopic TagsTopicField { get; set; }

        public bool? IsExecutiveUser { get; set; }
    }

    public class PlannedActual
    {
        [RegularExpression("^[0-9]*$", ErrorMessage = "Field must be numeric")]
        public string Planned { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Field must be numeric")]
        public string Actual { get; set; }

        public bool? IsExpandPlannedActualField { get; set; }
        //public Guid? UnitsTypeId { get; set; }
        public string Variance { get; set; }
        public List<SelectListItem> Units { get; set; }
        public string UnitsSelectedType { get; set; }
    }

    public class TagsTopic
    {
        public string FirstTagTopic { get; set; }
        public bool? MoreTags { get; set; }
        public bool? HasTags { get; set; }
    }
}
