﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class PlannedActualModel
    {
        public string Planned { get; set; }

        public string Actual { get; set; }

        public List<SelectListItem> Units { get; set; }
    }
}
