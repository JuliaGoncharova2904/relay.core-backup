﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class AddShiftTopicViewModel
    {
        public Guid ShiftId { get; set; }
        public Guid CreatorId { get; set; }
        public Guid RotationTopicGroupId { get; set; }
        public Guid? AssignedToId { get; set; }
        public string Name { get; set; }
        public string Notes { get; set; }
        public Guid? RelationId { get; set; }
    }
}
