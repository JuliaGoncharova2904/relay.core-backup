﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class TagsTopicViewModel
    {
        public string TagsString { get; set; }
        public Guid? TopicId { get; set; }
    }
}
