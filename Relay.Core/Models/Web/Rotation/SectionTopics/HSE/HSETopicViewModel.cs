﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class HSETopicViewModel : BaseTopicViewModel
    {
        public string Type { get; set; }

        public string Reference { get; set; }

        public DateTime? CreatedUtc { get; set; }
    }
}
