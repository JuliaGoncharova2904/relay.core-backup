﻿namespace MomentumPlus.Relay.Models
{
    public class TeamTopicViewModel : BaseTopicViewModel
    {
        public string TeamMember { get; set; }

        public string Reference { get; set; }
    }
}
