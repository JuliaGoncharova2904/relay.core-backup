﻿namespace MomentumPlus.Relay.Models
{
    public class TaskBoardPdfReportItemViewModel
    {
        public string TopicName { get; set; }
        public string TaskName { get; set; }
        public string TaskNotes { get; set; }
        public string TaskPriority { get; set; }
        public string TaskDate { get; set; }
    }
}
