﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardTaskTopicViewModel
    {
        public Guid Id { get; set; }
        public bool ReadOnly { get; set; }
        public bool IsCustom { get; set; }
        public bool IsTaskBoardTask { get; set; }
        public bool IsFeedbackRequired { get; set; }
        public int CarryforwardCounter { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public string CompleteStatus { get; set; }
        public TaskPriority Priority { get; set; }
        public string DueDate { get; set; }
        public Guid AssigneeId { get; set; }
        public string AssigneeName { get; set; }
        public string TaskOwnerName { get; set; }
        public Guid TaskOwnerId { get; set; }
        public bool IsInArchive { get; set; }
        public bool HasAttachments { get; set; }
        public bool HasVoiceMessages { get; set; }
        public Guid CreatorId { get; set; }
        public bool? IsReceivedAttachment { get; set; }
    }
}
