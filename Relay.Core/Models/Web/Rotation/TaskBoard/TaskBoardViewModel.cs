﻿using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardViewModel
    {
        public Guid UserId { get; set; }
        public bool IsViewMode { get; set; }
        public string OwnerName { get; set; }
        public TaskBoardSortOptions? SortStrategy { get; set; }
        public TaskBoardFilterOptions? Filter { get; set; }
        public IPagedList<TaskBoardTaskTopicViewModel> TasksTopics { get; set; }
    }
}
