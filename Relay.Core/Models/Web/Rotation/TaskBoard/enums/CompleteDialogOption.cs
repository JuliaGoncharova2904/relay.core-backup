﻿namespace MomentumPlus.Relay.Models
{
    public enum CompleteDialogOption
    {
        Complete = 0,
        Save = 1,
    }
}
