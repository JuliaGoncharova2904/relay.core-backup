﻿namespace MomentumPlus.Relay.Models
{
    public enum TaskBoardSection
    {
        Received = 1,
        Assigned = 2,
        MyTasks = 3
    }
}
