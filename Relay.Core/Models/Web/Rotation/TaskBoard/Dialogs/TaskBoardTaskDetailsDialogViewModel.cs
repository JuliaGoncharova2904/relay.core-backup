﻿namespace MomentumPlus.Relay.Models
{
    public class TaskBoardTaskDetailsDialogViewModel
    {
        public TaskBoardCompleteTaskDetailTabViewModel TaskDetailTab { get; set; }
        public TaskBoardCompleteOutcomesTabViewModel TaskOutcomesTab { get; set; }
        public ProjectAdminProjectDetailsTabViewModel ProjectDetailsTab { get; set; }
    }
}
