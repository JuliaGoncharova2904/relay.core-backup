﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardEditTaskDialogViewModel
    {
        public Guid ID { get; set; }

        public Guid OwnerId { get; set; }

        [Required]
        [Display(Name = "Task Name*")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        [StringLength(500)]
        public string Notes { get; set; }

        //[Required(ErrorMessage = "Assignee is required")]
        [Display(Name = "Assignee")]
        public Guid? AssignedTo { get; set; }

        [Required]
        [Display(Name = "Due Date")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Send Date")]
        public DateTime? SendDate { get; set; }

        public DateTime StartLimitDate { get; set; }

        public bool IsProjectTask { get; set; }

        [Required]
        [Display(Name = "Priority*")]
        public TaskPriority Priority { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        [Display(Name = "Sending Option")]
        public TaskSendingOptions SendingOption { get; set; }

        [Display(Name = "Assign to Handover Item")]
        public Guid? SelectedTopicId { get; set; }
        public IEnumerable<SelectListItem> TopicsForAssignee { get; set; }

        public bool CanChangeAssignee { get; set; }

        [Display(Name = "Add to handover")]
        public bool AddToReport { get; set; }

        public bool CanAddToReport { get; set; }

        public bool CanChange { get; set; }

        public bool IsExecutiveUser { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach (string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }

        public Guid ItemId { get; set; }
        public int AttacmentsCounter { get; set; }
        public int VoiceMessagesCounter { get; set; }
        public bool IsFeedbackRequired { get; set; }
        public bool? CanEdit { get; set; }
        public TaskBoardCompleteItemTabViewModel ItemDetailsTab { get; set; }

        [Required]
        public Guid? UserSelectedId { get; set; }
        public IEnumerable<SelectListItem> Users { get; set; }
        public IEnumerable<DropDownListItemModel> UsersList { get; set; }
    }
}
