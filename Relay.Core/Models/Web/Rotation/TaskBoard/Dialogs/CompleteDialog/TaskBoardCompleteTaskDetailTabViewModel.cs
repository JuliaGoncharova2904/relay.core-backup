﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardCompleteTaskDetailTabViewModel
    {
        [Display(Name = "Task Name")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        public string TaskNotes { get; set; }

        public Guid? AssignedFrom { get; set; }

        [Display(Name = "Due Date")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Priority")]
        public TaskPriority Priority { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        public bool CanChangeAssignee { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach(string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
        public Guid TaskId { get; set; }
        public Guid ItemId { get; set; }
        public int AttacmentsCounter { get; set; }
        public int VoiceMessagesCounter { get; set; }
        public bool IsFeedbackRequired { get; set; }
        public Guid CreatorTaskId { get; set; }
        public bool IsCanEdit { get; set; }
    }
}
