﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TaskBoardCompleteTaskDialogViewModel
    {
        public TaskBoardCompleteTaskDetailTabViewModel TaskDetailTab { get; set; }
        public TaskBoardCompleteOutcomesTabViewModel TaskOutcomesTab { get; set; }
        public ProjectAdminProjectDetailsTabViewModel ProjectDetailsTab { get; set; }

        public TaskBoardEditTaskDialogViewModel TaskBoardEditTaskDialogViewModel { get; set; }

        public bool HasTopic { get; set; }
        public int SelectedTabNumber { get; set; }
        public CompleteDialogOption CompleteOption { get; set; }

        public bool IsExecutiveUser { get; set; }

        public bool CanEdit { get; set; }

        public DateTime? Deadline { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach (string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
    }
}
