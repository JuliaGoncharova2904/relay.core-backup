﻿namespace MomentumPlus.Relay.Models
{
    public class TaskBoardCompleteHandbackTaskDialogViewModel
    {
        public int SelectedTabNumber { get; set; }
        public CompleteDialogOption CompleteOption { get; set; }
        public TaskBoardCompleteTaskDetailTabViewModel TaskDetailTab { get; set; }
        public TaskBoardCompleteOutcomesTabViewModel TaskOutcomesTab { get; set; }
        public TaskBoardCompleteItemTabViewModel TopicDetailTab { get; set; }
        public bool CanEdit { get; set; }
    }
}
