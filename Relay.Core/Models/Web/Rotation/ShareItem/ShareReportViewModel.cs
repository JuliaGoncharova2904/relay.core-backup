﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ShareReportViewModel
    {
        public Guid ReportId { get; set; }
        public RotationType ReportType { get; set; }

        [Display(Name = "Recipients")]
        public IEnumerable<Guid> RecipientsIds { get; set; }

        public IEnumerable<Guid> DeleteRelationsIds { get; set; }

        public IEnumerable<SelectListItem> Recipients { get; set; }

        public IEnumerable<ShareReportRelationViewModel> ReportRelations { get; set; }
    }
}
