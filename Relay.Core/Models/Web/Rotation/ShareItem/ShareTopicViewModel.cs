﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ShareTopicViewModel
    {
        public Guid TopicId { get; set; }

        [Display(Name = "Recipients")]
        public IEnumerable<Guid> RecipientsIds { get; set; }

        public IEnumerable<Guid> DeleteTopicRelationsIds { get; set; }

        public IEnumerable<SelectListItem> Recipients { get; set; }

        public IEnumerable<TopicRelationViewModel> TopicRelations { get; set; }

        public bool? IsRecipientsFromPrevRotation { get; set; }

        // public IEnumerable<SelectListItem> RecipientsEmail { get; set; }

        [EmailAddress]
        public Guid? RecipientEmail { get; set; }

        public string CustomRecipientsEmailName { get; set; }

        public IEnumerable<string> Emails { get; set; }

        public string EmailsString
        {
            get
            {
                string emailsString = string.Empty;

                if (Emails != null)
                {
                    foreach (string email in Emails)
                    {
                        emailsString = string.Format("{0} \"{1}\",", emailsString, email);
                    }
                }

                return emailsString;
            }
        }
    }
}
