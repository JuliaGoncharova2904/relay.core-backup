﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class ShareReportUsersViewModel
    {
        public int ShareReportCount { get; set; }
        public List<DropDownListItemModel> UsersList { get; set; }
    }
}
