﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ShareReportRelationViewModel
    {
        public Guid RelationId { get; set; }

        public Guid? RecipientId { get; set; }
        public string RecipientTitle { get; set; }
        public string RecipientPosition { get; set; }
    }
}
