﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ConfirmButtonViewModel
    {
        public bool IsShowConfirmRotationButton { get; set; }
        public bool IsShowShiftRotationButton { get; set; }
        public Guid? UserId { get; set; }
    }
}
