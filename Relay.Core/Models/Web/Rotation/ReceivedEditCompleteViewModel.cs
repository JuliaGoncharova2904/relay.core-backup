﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ReceivedEditCompleteViewModel
    {
        public Guid UserId { get; set; }
        public Guid TaskId { get; set; }
    }
}
