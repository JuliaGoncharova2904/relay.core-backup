﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class DailyNotesPanelViewModel
    {
        public bool ShowEndSlide { get; set; }
        public List<DailyNoteViewModel> DailyNotes { get; set; }
        public Guid? UserID { get; set; }

        public DateTime EndDayRotation { get; set; }
    }
}
