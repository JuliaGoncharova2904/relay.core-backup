﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MomentumPlus.Relay.Models
{
    public class AttachmentViewModel
    {
        public Guid? Id { get; set; }

        public Guid UserId { get; set; }

        public Guid? TargetId { get; set; }

        public Guid? FileId { get; set; }

        public string FileName { get; set; }

        public bool AbleRemove { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        public string AddedTime { get; set; }

        [Display(Name = "Upload from Computer")]
        //[FileExtensions(Extensions = "txt,doc,docx,pdf,png", ErrorMessage = "Specify a CSV file. (Comma-separated values)")]
        public HttpPostedFileBase File { get; set; }

        [RegularExpression(@"(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})", ErrorMessage = "Invalid link format.")]
        public string Link { get; set; }

        public bool? IsCanEdit { get; set; }
    }
}
