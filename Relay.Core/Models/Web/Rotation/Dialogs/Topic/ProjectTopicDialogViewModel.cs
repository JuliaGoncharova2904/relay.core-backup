﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ProjectTopicDialogViewModel : BaseTopicDialogViewModel
    {
        [Required]
        [Display(Name = "Project Name*")]
        public string RotationTopicGroupId { get; set; }
        public IEnumerable<SelectListItem> RotationTopicGroups { get; set; }
        public bool IsExecutiveUser { get; set; }
    }
}
