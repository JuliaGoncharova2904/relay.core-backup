﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class TopicDetailsDialogViewModel
    {
        public Guid? Id { get; set; }

        public int SelectedTabNumber { get; set; }

        public TopicDetailsViewModel TopicDetails { get; set; }

    }
}
