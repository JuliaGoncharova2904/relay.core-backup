﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class HSETopicDialogViewModel : BaseTopicDialogViewModel
    {
        [Required]
        [Display(Name = "Type*")]
        public string RotationTopicGroupId { get; set; }
        public IEnumerable<SelectListItem> RotationTopicGroups { get; set; }

        public bool IsExecutiveUser { get; set; }
    }
}
