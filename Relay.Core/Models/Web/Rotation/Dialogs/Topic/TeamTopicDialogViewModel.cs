﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class TeamTopicDialogViewModel : BaseTopicDialogViewModel
    {
        [Required]
        public string RotationTopicGroupId { get; set; }

        public IEnumerable<SelectListItem> TeamMembers { get; set; }

        public bool IsExecutiveUser { get; set; }

    }
}
