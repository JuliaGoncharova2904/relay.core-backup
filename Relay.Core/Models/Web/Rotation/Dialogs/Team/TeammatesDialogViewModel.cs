﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TeammatesDialogViewModel
    {
        public TeammateViewModel RequestOwner { get; set; }
        public IEnumerable<TeammateViewModel> OnSiteTeammates { get; set; }
        public IEnumerable<TeammateViewModel> OffSiteTeammates { get; set; }
        public IEnumerable<TeammateViewModel> WithoutRotation { get; set; }
    }
}
