﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class LocationViewModel
    {
        public Guid? Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Address")]
        [StringLength(500)]
        public string Address { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        public bool? IsLink { get; set; }
    }
}
