﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class RotationCompleteTaskTopicTabViewModel
    {
        public Guid ItemId { get; set; }

        [Display(Name = "Section")]
        public string Section { get; set; }

        [Display(Name = "Reference")]
        public string ItemName { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "Comments")]
        public string ManagerComments { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        public int AttacmentsCounter { get; set; }

        public int VoiceMessagesCounter { get; set; }

        public int TasksCounter { get; set; }

        public bool IsFeedbackRequired { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach (string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
    }
}
