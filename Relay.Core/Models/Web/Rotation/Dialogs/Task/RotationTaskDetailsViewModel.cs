﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class RotationTaskDetailsViewModel
    {
        public Guid? Id { get; set; }

        public Guid? TopicId { get; set; }

        public bool IsSwingMode { get; set; }

        [Display(Name = "Section")]
        public string Section { get; set; }

        [Display(Name = "Name")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        [StringLength(500)]
        public string TaskNotes { get; set; }

        [Display(Name = "Handback Notes")]
        [StringLength(500)]
        public string Feedback { get; set; }

        public Guid? AssignedFrom { get; set; }

        public bool IsComplete { get; set; }

        [Display(Name = "Deadline")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Priority")]
        public TaskPriority Priority { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }


        [Display(Name = "Attacments")]
        public int AttacmentsCounter { get; set; }

        public bool HasNewAttacments { get; set; }

        [Display(Name = "Voice Messages")]

        public int VoiceMessagesCounter { get; set; }

        public bool HasNewVoiceMessages { get; set; }

        [Display(Name = "Handback")]
        public bool IsFeedbackRequired { get; set; }

        public TopicDetailsViewModel TopicDetails { get; set; }

        public TaskSendingOptions SendingOption { get; set; }

        public int SelectedTabNumber { get; set; }

        public bool CanChangeAssign { get; set; }

        public bool IsRotationMode { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach (string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }
    }
}
