﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class RotationCompleteTaskTaskDetailsTabViewModel
    {
        public bool IsSwingMode { get; set; }

        public bool IsFeedbackRequired { get; set; }

        [Display(Name = "Task Name*")]
        [StringLength(50, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        [StringLength(500)]
        public string TaskNotes { get; set; }

        public Guid? AssignedFrom { get; set; }

        [Display(Name = "Due Date")]
        public DateTime? Deadline { get; set; }

        [Display(Name = "Priority")]
        public TaskPriority Priority { get; set; }

        [Display(Name = "Tags")]
        public IEnumerable<string> Tags { get; set; }

        [Display(Name = "Attacments")]
        public int AttacmentsCounter { get; set; }

        [Display(Name = "Voice Messages")]
        public int VoiceMessagesCounter { get; set; }

        public TaskSendingOptions SendingOption { get; set; }

        public bool CanChangeAssignee { get; set; }

        public bool IsRotationMode { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    foreach (string tag in Tags)
                    {
                        tagsString = string.Format("{0} \"{1}\",", tagsString, tag);
                    }
                }

                return tagsString;
            }
        }

    }
}
