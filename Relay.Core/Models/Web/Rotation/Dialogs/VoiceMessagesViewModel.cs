﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MomentumPlus.Relay.Models
{
    public class VoiceMessageViewModel
    {
        public Guid? Id { get; set; }

        public Guid? TargetId { get; set; }

        public Guid? FileId { get; set; }
        public string FileName { get; set; }

        public string AddedTime { get; set; }

        public string Pin { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Upload from Computer")]
        //[Required, FileExtensions(Extensions = "csv", ErrorMessage = "Specify a audio file.")]
        public HttpPostedFileBase File { get; set; }
    }
}
