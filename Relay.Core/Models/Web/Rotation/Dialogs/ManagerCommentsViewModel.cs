﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ManagerCommentsViewModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Notes*")]
        [StringLength(300)]
        public string Notes { get; set; }

        public Guid? CreatorManagerCommentsId { get; set; }

        public string FullNameLineManager { get; set; }

        public DateTime? DateTimeCreatedManagerComments { get; set; }

        public bool? IsCanRemoveManagerComments { get; set; }

        public bool? IsEditManagerComments { get; set; }

        public virtual Guid? RotationTopicId { get; set; }

        public bool? IsLineManager { get; set; }

    }
}
