﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class DailyReportViewModel
    {
        public Guid ShiftId { get; set; }

        public Guid RotationId { get; set; }
        public bool IsCurrentShift { get; set; }

        public bool HasItems { get; set; }

        public string Date { get; set; }
        public string ShiftRecipientFullName { get; set; }
        public DateTime StartDateTime { get; set; }

        public SafetyMessages SafetyMessages { get; set; }
    }
}
