﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class MajorHazardViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public Guid IconId { get; set; }
        public IEnumerable<CriticalControlViewModel> CriticalControls { get; set; }
    }
}
