﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class SchedulerViewModel
    {
        public IEnumerable<SelectListItem> Teams { get; set; }

        public string TeamsString
        {
            get
            {
                string testsString = "[";
                string separator = "";

                if (Teams != null)
                {
                    foreach (var team in Teams)
                    {
                        testsString = string.Format("{0}{1}{{\"Id\": \"{2}\", \"Name\": \"{3}\"}}", testsString, separator, team.Value, team.Text);
                        separator = ",";
                    }
                }

                return testsString + "]";
            }
        }
    }
}
