﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class EditSafetyMessageViewModel
    {
        public Guid Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Major Hazard")]
        public Guid MajorHazard { get; set; }
        public IEnumerable<SelectListItem> MajorHazards { get; set; }

        [Required]
        [Display(Name = "Critical Control")]
        public Guid CriticalControl { get; set; }
        public IEnumerable<SelectListItem> CriticalControls { get; set; }

        [Display(Name = "Team Permissions")]
        public IList<Guid> Teams { get; set; }
        public IEnumerable<SelectListItem> AllTeams { get; set; }

        [Required]
        [Display(Name = "Message*")]
        [StringLength(450, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Message { get; set; }

        public string AllTeamsString
        {
            get
            {
                string testsString = "[";
                string separator = "";

                if (AllTeams != null)
                {
                    foreach (var team in AllTeams)
                    {
                        testsString = string.Format("{0}{1}{{\"Id\": \"{2}\", \"Name\": \"{3}\"}}", testsString, separator, team.Value, team.Text);
                        separator = ",";
                    }
                }

                return testsString + "]";
            }
        }

        public string TeamsString
        {
            get
            {
                string testsString = "[";
                string separator = "";

                if (Teams != null)
                {
                    IEnumerable<SelectListItem> result = AllTeams.Where(t => Teams.Contains(Guid.Parse(t.Value)));
                    foreach (var team in result)
                    {
                        testsString = string.Format("{0}{1}{{\"Id\": \"{2}\", \"Name\": \"{3}\"}}", testsString, separator, team.Value, team.Text);
                        separator = ",";
                    }
                }

                return testsString + "]";
            }
        }
    }
}
