﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class SafetyMessagesViewModel
    {
        public string Date { get; set; }
        public IEnumerable<SelectListItem> SafetyMessages { get; set; }
    }
}
