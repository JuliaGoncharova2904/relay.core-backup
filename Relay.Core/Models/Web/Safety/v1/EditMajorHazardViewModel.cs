﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace MomentumPlus.Relay.Models
{
    public class EditMajorHazardViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Title")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Image")]
        public HttpPostedFileBase Icon { get; set; }
    }
}
