﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class CriticalControlViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<SelectListItem> Teams { get; set; }
    }
}
