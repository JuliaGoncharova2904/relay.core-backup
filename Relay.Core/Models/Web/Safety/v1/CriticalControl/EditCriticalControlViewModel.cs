﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class EditCriticalControlViewModel
    {
        public Guid Id { get; set; }
        public Guid MajorHazardId { get; set; }

        [Required]
        [Display(Name = "Critical Control*")]
        [StringLength(70, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Teams Permissions")]
        public IList<Guid> Teams { get; set; }
        public IEnumerable<SelectListItem> AllTeams { get; set; }

        public string AllTeamsString
        {
            get
            {
                string testsString = "[";
                string separator = "";

                if (AllTeams != null)
                {
                    foreach (var team in AllTeams)
                    {
                        testsString = string.Format("{0}{1}{{\"Id\": \"{2}\", \"Name\": \"{3}\"}}", testsString, separator, team.Value, team.Text);
                        separator = ",";
                    }
                }

                return testsString + "]";
            }
        }

        public string TeamsString
        {
            get
            {
                string testsString = "[";
                string separator = "";

                if(Teams != null)
                {
                    IEnumerable<SelectListItem> result = AllTeams.Where(t => Teams.Contains(Guid.Parse(t.Value)));
                    foreach (var team in result)
                    {
                        testsString = string.Format("{0}{1}{{\"Id\": \"{2}\", \"Name\": \"{3}\"}}", testsString, separator, team.Value, team.Text);
                        separator = ",";
                    }
                }

                return testsString + "]";
            }
        }
    }
}
