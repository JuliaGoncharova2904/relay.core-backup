﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SafetyCalendarDateViewModel
    {
        public string DayNumber { get; set; }
        public DateTime Date { get; set; }
        public Guid MessageId { get; set; }
        public Guid MessaageIconId { get; set; }
        public int MessagesNumber { get; set; }
    }
}
