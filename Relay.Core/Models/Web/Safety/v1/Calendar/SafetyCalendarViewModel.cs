﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class SafetyCalendarViewModel
    {
        public string Month { get; set; }
        public string MonthYear { get; set; }
        public DateTime Date { get; set; }
        public SafetyCalendarDateViewModel[] Dates { get; set; }
    }
}
