﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class EditSafetyMessageV2ViewModel
    {
        public Guid Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        [Display(Name = "Major Hazard")]
        public Guid MajorHazardId { get; set; }
        public IEnumerable<SelectListItem> MajorHazards { get; set; }

        [Required]
        [Display(Name = "Critical Control")]
        public Guid CriticalControlId { get; set; }
        public IEnumerable<SelectListItem> CriticalControls { get; set; }

        [Display(Name = "CCO")]
        public string CriticalControlOwnerName { get; set; }

        [Display(Name = "CCC(s)")]
        public IEnumerable<string> CriticalControlChampions { get; set; }

        [Required]
        [Display(Name = "Message*")]
        [StringLength(450, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Message { get; set; }

    }
}
