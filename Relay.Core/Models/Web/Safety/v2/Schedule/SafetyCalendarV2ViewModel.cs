﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SafetyCalendarV2ViewModel
    {
        public string MonthYear { get; set; }
        public DateTime Date { get; set; }
        public DateTime CurrentDate { get; set; }
        public SafetyCalendarDateV2ViewModel[] Dates { get; set; }
    }
}
