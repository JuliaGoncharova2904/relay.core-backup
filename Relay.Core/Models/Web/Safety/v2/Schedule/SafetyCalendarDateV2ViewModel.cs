﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SafetyCalendarDateV2ViewModel
    {
        public DateTime Date { get; set; }
        public string IconUrl { get; set; }
        public int MessagesNumber { get; set; }
    }
}
