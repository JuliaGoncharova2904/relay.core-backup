﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class MajorHazardV2ViewModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "MHO")]
        public string OwnerName { get; set; }

        public string IconUrl { get; set; }
    }
}
