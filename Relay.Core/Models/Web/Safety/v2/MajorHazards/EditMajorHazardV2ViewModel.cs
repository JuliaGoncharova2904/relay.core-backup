﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class EditMajorHazardV2ViewModel
    {
        public Guid Id { get; set; }

        [Display(Name = "MHO")]
        public Guid OwnerId { get; set; }
        public IEnumerable<SelectListItem> Owners { get; set; }

        public IEnumerable<CriticalControlV2ViewModel> CriticalControls { get; set; }
    }
}
