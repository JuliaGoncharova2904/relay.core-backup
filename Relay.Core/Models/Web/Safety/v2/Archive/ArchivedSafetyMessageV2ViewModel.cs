﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class ArchivedSafetyMessageV2ViewModel
    {
        public Guid MessageId { get; set; }

        public string MajorHazardName { get; set; }

        public string MessageText{ get; set; }

        public string ScheduledDate { get; set; }

        public IEnumerable<DropDownListItemModel> OpenUsers { get; set; }

        public IEnumerable<DropDownListItemModel> LateOpenUsers { get; set; }

        public IEnumerable<DropDownListItemModel> UnopenedUsers { get; set; }


    }
}
