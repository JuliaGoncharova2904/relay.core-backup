﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class CriticalControlV2ViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string OwnerName { get; set; }
        public IEnumerable<string> Champions { get; set; }
    }
}
