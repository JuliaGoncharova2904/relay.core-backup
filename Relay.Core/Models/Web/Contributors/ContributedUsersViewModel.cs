﻿using MvcPaging;

namespace MomentumPlus.Relay.Models
{
    public class ContributedUsersViewModel
    {
        public bool IsViewMode { get; set; }
        public string OwnerName { get; set; }
        public IPagedList<ContributedUserViewModel> ContributedUsers { get; set; }
    }
}
