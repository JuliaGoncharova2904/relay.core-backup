﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class MenuViewModel
    { 
        public bool IsViewMode { get; set; }
        public IEnumerable<Guid?> SelectedTopicGroupsTagsIds { get; set; }
    }
}
