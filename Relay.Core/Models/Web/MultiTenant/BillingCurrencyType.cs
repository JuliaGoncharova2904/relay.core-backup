﻿namespace MomentumPlus.Relay.Models
{
    public enum BillingCurrencyType
    {
        Usd = 0,
        Aud = 1,
        Gbp = 2,
        Eur = 3
    }
}
