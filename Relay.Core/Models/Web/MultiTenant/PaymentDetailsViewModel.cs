﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class PaymentDetailsViewModel
    {
        [Display(Name = "Card Number*")]
        [Required]
        [Range(100000000000, 9999999999999999999, ErrorMessage = "must be between 12 and 19 digits")]
        public long CardNumber { get; set; }

        [Required]
        public string CardHolder { get; set; }

        [Required]
        public string ExpiryDate { get; set; }

        [Required]
        public string Cvv { get; set; }
    }
}