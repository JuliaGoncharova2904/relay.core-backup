﻿namespace MomentumPlus.Relay.Models
{
    public enum UserSubscriptionType
    {
        Single = 0,
        BackToBack = 1,
        Team =2
    }
}
