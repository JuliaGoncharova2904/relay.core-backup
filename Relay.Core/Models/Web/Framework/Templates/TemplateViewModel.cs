﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class TemplateViewModel
    {
        public Guid? Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual IEnumerable<rModuleViewModel> Modules { get; set; }

        public Guid? CopyFromTemplateId { get; set; }

        public Guid? CopyToTemplateId { get; set; }

        public IEnumerable<SelectListItem> Templates { get; set; }

        public Guid? SelectedSectorsId { get; set; }

        public IEnumerable<SelectListItem> Sectors { get; set; }
    }
}
