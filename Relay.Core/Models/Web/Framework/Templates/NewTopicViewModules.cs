﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class NewTopicViewModules
    {
        public Guid? Id { get; set; }

        [Display(Name = "Name")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        public string Description { get; set; }
        public string DescriptionExist { get; set; }

        public Guid? TopicGroupId { get; set; }

        public Guid? SelectedTemplate { get; set; }

        public Guid? ParentTopicId { get; set; }

        public bool Enabled { get; set; }

        [Display(Name = "Metrics")]
        public bool? Metrics { get; set; }

        public bool? PlannedActualHas { get; set; }

        public string UnitsSelectedType { get; set; }

        public List<SelectListItem> Units { get; set; }

        public List<SelectListItem> Sections { get; set; }

        public string SelectSection { get; set; }

        [Display(Name = "Topic")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string CustomTemplateTopicGroupName { get; set; }

        [Display(Name = "Reference")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string CustomTemplateReferenceName { get; set; }

        public Guid? TemplateId { get; set; }

        public Guid? ModuleId { get; set; }

        public Guid? TopicId { get; set; }

        public bool? SaveAddAnother { get; set; }

        public bool? MetricsFalse { get; set; }
    }
}
