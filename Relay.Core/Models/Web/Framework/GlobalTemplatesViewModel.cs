﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class GlobalTemplatesViewModel
    {
        public Guid? Id { get; set; }

        public IEnumerable<SelectListItem> GlobalTemplates { get; set; }

        public IEnumerable<SelectListItem> Sectors { get; set; }

        public string ActiveSector { get; set; }

        public string ActiveTemplate { get; set; }
    }
}
