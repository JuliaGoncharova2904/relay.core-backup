﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class RotationPatternViewModel
    {
        public Guid? Id { get; set; }

        [Required]
        [Display(Name = "Days On")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "DaysOn - Cannot be equal '0' or less.")]
        public int? DayOn { get; set; }

        [Required]
        [Display(Name = "Days Off")]
        [RegularExpression("([0-9][0-9]*)", ErrorMessage = "DaysOff - Cannot be less '0'.")]
        public int? DayOff { get; set; }

        public string Name { get; set; }
    }
}
