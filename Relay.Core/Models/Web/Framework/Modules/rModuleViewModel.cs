﻿using System;
using System.Collections.Generic;
using MvcPaging;

namespace MomentumPlus.Relay.Models
{
    public class rModuleViewModel
    {
        public Guid Id { get; set; }

        public ModuleType Type { get; set; }

        public Guid? ParentModuleId { get; set; }

        public Guid? TemplateId { get; set; }

        public bool Enabled { get; set; }

        public IPagedList<rTopicGroupViewModel> TopicGroupsList { get; set; }

        public IPagedList<rTopicViewModel> TopicsList { get; set; }

        public IPagedList<rTaskViewModel> TasksList { get; set; }

        public Guid? ActiveTopicGroupId { get; set; }

        public Guid? ActiveTopicId { get; set; }

        public int EnabledTopicGroupsCounter { get; set; }

        public virtual ICollection<rTopicGroupViewModel> TopicGroups { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage2 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage3 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage4 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage5 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage6 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage7 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage8 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage9 { get; set; }

        public virtual List<rTopicGroupViewModel> TopicGroupsPage10 { get; set; }


        public virtual List<rTopicViewModel> TopicPage2 { get; set; }

        public virtual List<rTopicViewModel> TopicPage3 { get; set; }

        public virtual List<rTopicViewModel> TopicPage4 { get; set; }

        public virtual List<rTopicViewModel> TopicPage5 { get; set; }

        public virtual List<rTopicViewModel> TopicPage6 { get; set; }

        public virtual List<rTopicViewModel> TopicPage7 { get; set; }

        public virtual List<rTopicViewModel> TopicPage8 { get; set; }

        public virtual List<rTopicViewModel> TopicPage9 { get; set; }

        public virtual List<rTopicViewModel> TopicPage10 { get; set; }
    }
}
