﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class rTopicViewModel
    {
        public Guid? Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        public string Description { get; set; }

        public string TopicGroupName { get; set; }

        public Guid? TopicGroupId { get; set; }

        public Guid? ParentTopicId { get; set; }

        public ICollection<rTaskViewModel> Tasks { get; set; }

        public bool Enabled { get; set; }

        public int? PageNumber { get; set; }

        [Display(Name = "Metrics")]
        public bool? Metrics { get; set; }

        public bool? PlannedActualHas { get; set; }

        public string UnitsSelectedType { get; set; }

        public List<SelectListItem> Units { get; set; }

        public List<SelectListItem> Sections { get; set; }

        public string SelectSection { get; set; }

        public string CustomTemplateTopicGroupName { get; set; }

        public string CustomTemplateReferenceName { get; set; }

        public Guid? TemplateId { get; set; }

        public List<SelectListItem> TemplatesAdded { get; set; }

        public List<SelectListItem> AllTemplates { get; set; }

        public List<Guid> TemplatesIds { get; set; }
    }
}
