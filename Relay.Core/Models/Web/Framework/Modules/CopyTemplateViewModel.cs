﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class CopyTemplateViewModel
    {
        [Required]
        public Guid? CopyFromTemplateId { get; set; }

        [Required]
        public Guid? CopyToTemplateId { get; set; }

        public IEnumerable<SelectListItem> Templates { get; set; }

    }

}
