﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class ProjectTopicGroupViewModel : rTopicGroupViewModel
    {

        public IEnumerable<SelectListItem> Projects { get; set; }

        [Required]
        [Display(Name = "Project*")]
        public string SelectedProject { get; set; }


    }
}
