﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class rTopicGroupViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Notes")]
        public string Description { get; set; }

        [Required]
        public Guid ModuleId { get; set; }

        public Guid? RelationId { get; set; }

        public Guid? ParentTopicGroupId { get; set; }

        public ICollection<rTopicViewModel> Topics { get; set; }

        public bool Enabled { get; set; }

        public int? PageNumber { get; set; }

        public bool IsNullReport { get; set; }

        public Guid? TemplateId { get; set; }
    }
}
