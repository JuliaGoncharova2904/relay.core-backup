﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class TeamTopicGroupViewModel : rTopicGroupViewModel
    {
        public IEnumerable<SelectListItem> Teams { get; set; }

        [Required]
        [Display(Name = "Team*")]
        public string SelectedTeam { get; set; }
    }
}
