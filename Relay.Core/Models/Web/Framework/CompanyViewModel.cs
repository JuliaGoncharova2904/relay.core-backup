﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class CompanyViewModel
    {
        public Guid? Id { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }


        [Display(Name = "Notes")]
        public string Notes { get; set; }

        [Display(Name = "Use as Default Company?")]
        public bool IsDefaultCompany { get; set; }
    }
}
