﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class SecurityViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "RelayWorks email")]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string RelayEmail { get; set; }

        [EmailAddress]
        [Display(Name = "Secondary Email")]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string SecondaryEmail { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Current Password*")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "New Password*")]
        public string NewPassword { get; set; }

        [Compare("NewPassword", ErrorMessage = "The passwords do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm New Password*")]
        public string ConfirmNewPassword { get; set; }
    }
}
