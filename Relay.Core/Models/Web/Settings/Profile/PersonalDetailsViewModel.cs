﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class PersonalDetailsViewModel
    {
        public Guid Id { get; set; }

        [StringLength(10)]
        [Display(Name = "Title")]
        public string Title { get; set; }
        public IEnumerable<SelectListItem> Titles { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Display(Name = "Surname")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Surname { get; set; }

        [Display(Name = "Job Position")]
        public string Position { get; set; }

        public PersonalDetailsViewModel FillTitles()
        {
            this.Titles = new SelectList(new[] { "Mr", "Mrs", "Ms", "Miss", "Dr" });
            return this;
        }
    }
}
