﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class UserDetailsViewModel
    {
        public Guid Id { get; set; }

        public string Role { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string LastLogin { get; set; }

        public bool CanDelete { get; set; }

        public bool IsLock { get; set; }
    }
}
