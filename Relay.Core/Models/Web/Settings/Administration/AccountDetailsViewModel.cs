﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class AccountDetailsViewModel
    {
        [Display(Name = "Version Details")]
        public string VersionDetails { get; set; }

        [Display(Name = "Purchase Date")]
        public string PurchaseDate { get; set; }

        [Display(Name = "Licence End Date")]
        public string LicenceEndDate { get; set; }

        [Display(Name = "Plan Type")]
        public string PlanType { get; set; }

        [Display(Name = "Technical Support")]
        public string TechnicalSupport { get; set; }

        [Display(Name = "Templates")]
        public string Templates { get; set; }

        [Display(Name = "Users")]
        public string Handovers { get; set; }

        public bool isMultiTenant { get; set; }

        [Display(Name = "Auto-Renew Subscription")]
        public bool? AutoRenewSubscription { get; set; }
    }
}
