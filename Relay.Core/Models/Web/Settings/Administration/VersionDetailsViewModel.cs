﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class VersionDetailsViewModel
    {
        [Display(Name = "Licence End Date")]
        public DateTime? LicenceEndDate { get; set; }

        [Display(Name = "Domain Url")]
        public string DomainUrl { get; set; }

        [Display(Name = "iHandover Account Manager")]
        public string iHandoverAccountManager { get; set; }

        [Display(Name = "Hosting Provider")]
        public string HostingProvider { get; set; }

        [Display(Name = "Maximum Templates")]
        [Required]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Maximum Templates - Cannot be equal '0' or less.")]
        public int MaximumTemplates { get; set; }

        [Display(Name = "Maximum Handovers")]
        [Required]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Maximum Handovers - Cannot be equal '0' or less.")]
        public int MaximumHandovers { get; set; }
    }
}
