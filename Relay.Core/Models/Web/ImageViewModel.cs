﻿namespace MomentumPlus.Relay.Models
{
    public class FileViewModel
    {
        public byte[] BinaryData { get; set; }
        public string ContentType { get; set; }

        public string Title { get; set; }

        public string FileName { get; set; }

        public string Link { get; set; }
    }
}
