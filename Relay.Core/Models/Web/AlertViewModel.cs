﻿namespace MomentumPlus.Relay.Models
{
    public class AlertViewModel
    {
        public string Message { get; set; }

        public AlertType Type { get; set; }
    }
}
