﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class InlineManualPeopleTrackingViewModel
    {
        public Guid? Uid { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public string Name { get; set; }

        public string Created { get; set; }

        public string Updated { get; set; }

        public string Role { get; set; }

        public string Group { get; set; }

        public string Plan { get; set; }

    }
}
