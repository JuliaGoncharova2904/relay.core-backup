﻿using MvcPaging;
using System;

namespace MomentumPlus.Relay.Models
{
    public class MyTeamRotationViewModel
    {
        public bool IsViewMode { get; set; }
        public string OwnerName { get; set; }
        public IPagedList<TeamRotationViewModel> TeamRotations { get; set; }
        public bool IsViewTeamBtn { get; set; }
        public int XUser { get; set; }
        public int YUser { get; set; }
    }
}
