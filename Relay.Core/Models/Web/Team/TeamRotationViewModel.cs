﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MomentumPlus.Relay.Models
{
    public class TeamRotationViewModel
    {
        public Guid ID { get; set; }
        public Guid OwnerId { get; set; }
        public string OwnerFullName { get; set; }
        public string OwnerEmail { get; set; }
        public string OwnerPosition { get; set; }
        public Guid? BackToBackId { get; set; }
        public string BackToBackName { get; set; }
        public bool IsActive { get; set; }
        public string StateMessage { get; set; }
        public bool IsLineManager { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsEnable { get; set; }
        public bool IsLock { get; set; }
        public bool IsHaveContributerUsers { get; set; }
        public List<UserProfile> ContributorsUsers { get; set; }
        public List<string> ContributorsUsersFullName { get; set; }
        public IEnumerable<DropDownListItemModel> ContributorsUsersList { get; set; }
    }
}
