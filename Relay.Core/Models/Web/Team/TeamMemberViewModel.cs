﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class TeamMemberViewModel
    {
        public Guid UserId { get; set; }

        public string UserName { get; set; }

        public int CountReceivedTask { get; set; }
    }
}
