﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class TeamCheckListViewModel
    {
        public LineManagerForTeamViewModel LineManager { get; set; }
        public BackToBackForTeamViewModel BackToBack { get; set; }       
        public IEnumerable<TeamMemberViewModel> TeamMember { get; set; }
        public Guid RotationId { get; set; }
    }
}
