﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class TeamForWidgetViewModel
    {
        public string  TeamName { get; set; }
        public Guid RotationId { get; set; }
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string RotarionPatternName { get; set; }
        public string PositionName { get; set; }
        public string CompanyName { get; set; }
        public string TeamStatus { get; set; }
    }
}
