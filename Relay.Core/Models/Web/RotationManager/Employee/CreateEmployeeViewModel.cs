﻿using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class CreateEmployeeViewModel : EmployeeViewModel
    {
        [Required]
        [Display(Name = "Password*")]
        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
            ErrorMessage = @"Your password must be 8 characters and contain a mixture of upper and lower-case letters, numbers and special characters such as !, #, @ or ?"
        )]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "The passwords do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password*")]
        public string PasswordConfirm { get; set; }
    }
}
