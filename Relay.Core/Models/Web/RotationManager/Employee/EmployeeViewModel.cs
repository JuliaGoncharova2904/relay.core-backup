﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class EmployeeViewModel
    {
        public Guid? Id { get; set; }

        [StringLength(10)]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        public string FullName { get; set; }

        [Required]
        [Display(Name = "Surname*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Surname { get; set; }

        [Display(Name = "Profile Image")]
        public Guid? ImageId { get; set; }

        [Required]
        [Display(Name = "Company*")]
        public string CompanyId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email/Login*")]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "User Type*")]
        public string UserTypeId { get; set; }
        public IEnumerable<SelectListItem> UserTypes { get; set; }

        [Required]
        [Display(Name = "Job Position*")]
        public string PositionId { get; set; }
        public IEnumerable<SelectListItem> Positions { get; set; }

        [Required]
        [Display(Name = "Default Working Pattern*")]
        public string RotationPatternId { get; set; }
        public IEnumerable<SelectListItem> RotationPatterns { get; set; }

        [Required]
        [Display(Name = "Team*")]
        public string TeamId { get; set; }
        public IEnumerable<SelectListItem> Teams { get; set; }

        [Display(Name = "Job Position")]
        public string Position { get; set; }

        public bool EditRoleEnabled { get; set; }
    }
}
