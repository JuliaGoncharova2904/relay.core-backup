﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class CreateRotationViewModel : BaseRotationViewModel
    {
        [Required]
        [Display(Name = "Handover Owner*")]
        public string RotationOwnerId { get; set; }
        public IEnumerable<SelectListItem> RotationOwnerEmployees { get; set; }
        public bool ContributorEnable { get; set; }

        [Display(Name = "Shift Pattern*")]
        public string ShiftPatternType { get; set; }
        public List<SelectListItem> ShiftPatterns { get; set; }
    }
}
