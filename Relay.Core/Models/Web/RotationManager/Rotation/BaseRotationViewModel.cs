﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class BaseRotationViewModel
    {
        public Guid Id { get; set; }

        [Display(Name = "Job Position")]
        public string PositionName { get; set; }
        public Guid? PositionId { get; set; }

        [Display(Name = "Location")]
        public string SiteName { get; set; }
        public Guid? SiteId { get; set; }

        [Required]
        [Display(Name = "Line Manager*")]
        public string LineManagerId { get; set; }
        public IEnumerable<SelectListItem> LineManagers { get; set; }

        //[Required]
        [Display(Name = "Default Back-to-Back")]
        public string DefaultBackToBackId { get; set; }
        public IEnumerable<SelectListItem> BackToBackEmployees { get; set; }

        [Display(Name = "Contributors")]
        public IList<Guid> ContributorsIds { get; set; }
        public IEnumerable<SelectListItem> Contributors { get; set; }

        [Display(Name = "Projects")]
        public IList<Guid> ProjectsIds { get; set; }
        public IEnumerable<SelectListItem> Projects { get; set; }

        [Display(Name = "Shift Handovers")]
        public RotationType RotationType { get; set; }

        //[Required]
        //[Display(Name = "Start Date*")]
        //public DateTime? StartDate { get; set; }
    }
}
