﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class EditRotationViewModel : BaseRotationViewModel
    {
        [Display(Name = "Handover Owner")]
        public string RotationOwner { get; set; }
        public string RotationOwnerId { get; set; }

        public bool CanChangeLineManager { get; set; }

        public bool ContributorEnable { get; set; }

        [Display(Name = "Shift Pattern*")]
        public string ShiftPatternType { get; set; }
        public List<SelectListItem> ShiftPatterns { get; set; }
    }
}
