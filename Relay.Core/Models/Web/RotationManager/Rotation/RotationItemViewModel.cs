﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class RotationItemViewModel
    {
        public Guid RotationOwnerId { get; set; }

        public string RotationOwnerName { get; set; }

        public Guid RotationId { get; set; }
    }
}
