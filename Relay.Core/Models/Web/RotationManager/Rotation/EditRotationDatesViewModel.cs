﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{

    public class EditRotationDatesViewModel
    {
        public Guid RotationId { get; set; }

        [Required]
        [Display(Name = "First Day*")]
        public DateTime? StartSwingDate { get; set; }

        [Required]
        [Display(Name = "Last Day*")]
        public DateTime? EndSwingDate { get; set; }
        public bool EndSwingDateReadOnly { get; set; }

        [Required]
        [Display(Name = "Returning*")]
        public DateTime? BackOnSiteDate { get; set; }
        public bool BackOnSiteDateReadOnly { get; set; }

        [Required]
        [Display(Name = "Repeat Times")]
        public int RepeatTimes { get; set; }

        public DateTime CurrentDate { get; set; }

        public IEnumerable<SelectListItem> RepeatRotationRange { get; set; }

        public EditRotationDatesViewModel FillRepeatRotationRange()
        {
            this.RepeatRotationRange = new[] {
                new SelectListItem() { Text = "No", Value = "0" },
                new SelectListItem() { Text = "Once", Value = "1" },
                new SelectListItem() { Text = "Twice", Value = "2" },
                new SelectListItem() { Text = "3 times", Value = "3" },
                new SelectListItem() { Text = "4 times", Value = "4" },
                new SelectListItem() { Text = "5 times", Value = "5" }
            };

            return this;
        }

    }
}
