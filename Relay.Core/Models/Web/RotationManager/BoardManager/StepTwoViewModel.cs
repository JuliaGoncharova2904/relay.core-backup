﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class BoardManagerStepTwoViewModel
    {
        [Required]
        public Guid UserPositionId { get; set; }

        [Required]
        [Display(Name = "Name*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Surname*")]
        [StringLength(35, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email/Login*")]
        [StringLength(256, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} characters long.")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password*")]
        [RegularExpression(
        @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,}$",
        ErrorMessage = @"Your password must be 8 characters and contain a mixture of upper and lower-case letters, numbers and special characters such as !, #, @ or ?"
        )]
        public string Password { get; set; }

        [Required]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The passwords do not match.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password*")]
        public string PasswordConfirm { get; set; }

        [Required]
        [Display(Name = "Company*")]
        public Guid? CompanyId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }

        [Required]
        [Display(Name = "User Type*")]
        public string UserTypeId { get; set; }
        public IEnumerable<SelectListItem> UserTypes { get; set; }

        [Required]
        [Display(Name = "Default Working Pattern*")]
        public Guid? RotationPatternId { get; set; }
        public IEnumerable<SelectListItem> RotationPatterns { get; set; }

        //[Required]
        [Display(Name = "Team*")]
        public Guid? TeamId { get; set; }
        public IEnumerable<SelectListItem> Teams { get; set; }


        [Required(ErrorMessage = "The Team* field is required.")]
        public string CustomTeamName { get; set; }

        public bool Continue { get; set; }

    }
}
