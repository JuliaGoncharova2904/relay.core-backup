﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class BoardManagerStepOneViewModel
    {
        [Display(Name = "Location*")]
        public Guid? SiteId { get; set; }

        //[Required(ErrorMessage = "The Site* field is required.")]
        public string CustomSiteName { get; set; }

        [Display(Name = "Job Position*")]
        public Guid? PositionId { get; set; }

        //[Required(ErrorMessage = "The Position* field is required.")]
        public string CustomPositionName { get; set; }

        [Display(Name = "Template*")]
        public Guid? TemplateId { get; set; }


        //[Required(ErrorMessage = "The Template* field is required.")]
        public string CustomTemplateName { get; set; }

    }
}
