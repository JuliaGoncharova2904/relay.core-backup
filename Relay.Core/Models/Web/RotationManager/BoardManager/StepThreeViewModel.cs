﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class BoardManagerStepThreeViewModel
    {
        public Guid UserId { get; set; }

        public Guid? TemplateId { get; set; }

        [Required]
        [Display(Name = "Line Manager*")]
        public Guid? LineManagerId { get; set; }
        public IEnumerable<SelectListItem> LineManagers { get; set; }

        //[Required]
        [Display(Name = "Default Handover Recipient*")]
        public Guid? HandoverRecipientId { get; set; }
        public IEnumerable<SelectListItem> HandoverRecipients { get; set; }

        [Display(Name = "Contributors")]
        public IList<Guid> ContributorsIds { get; set; }
        public IEnumerable<SelectListItem> Contributors { get; set; }

        [Display(Name = "Shift Handovers")]
        public RotationType RotationType { get; set; }

        public Guid TeamUserId { get; set; }

        public bool ContributorEnable { get; set; }

        [Display(Name = "Shift Pattern*")]
        public string ShiftPatternType { get; set; }
        public List<SelectListItem> ShiftPatterns { get; set; }
    }
}
