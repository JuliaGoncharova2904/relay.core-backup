﻿using System;
using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum SubscriptionRestrictionType
    {
        [Description("Basic")]
        Basic = 0,

        [Description("Intermediate")]
        Premium = 1,

        [Description("Advanced")]
        Ultimate = 2
    }
}
