﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class UpgradeSubscriptionViewModel
    {
        [Required]
        public int NumberOfusers { get; set; }

        public decimal CurrentPrice { get; set; }

        public decimal NewPrice { get; set; }

        public string CardNumberLast4 { get; set; }

        public string CardBrand { get; set; }

        public string StripePublicKey { get; set; }

        [Required]
        public SubscriptionRestrictionType SubscriptionLevelType { get; set; }

        public decimal SubscriptionLevelPriceBasic { get; set; }

        public decimal SubscriptionLevelPricePremium { get; set; }

        public decimal SubscriptionLevelPriceUltimate { get; set; }

        public decimal UserPriceForBasicType { get; set; }

        public decimal UserPriceForPremiumType { get; set; }

        public decimal UserPriceForUltimateType { get; set; }

        public bool IsTrial { get; set; }



        [Display(Name = "Card Number*")]
        [StringLength(50, MinimumLength = 3)]
        public string CardNumber { get; set; }


        [Display(Name = "Exp Date*")]
        public string ExpDate { get; set; }



        [Display(Name = "CVC*")]
        [StringLength(3, MinimumLength = 3)]
        public string CVC { get; set; }


        [Display(Name = "VAT*")]
        [StringLength(16, MinimumLength = 2)]
        public string VAT { get; set; }

        public string Email { get; set; }

        //[Required(ErrorMessage = "Please select country")]
        [Display(Name = "Country*")]
        public string UserCountry { get; set; }

        public Guid? SubscriptionId { get; set; }

        public string[] Countries { get; set; }

    }
}
