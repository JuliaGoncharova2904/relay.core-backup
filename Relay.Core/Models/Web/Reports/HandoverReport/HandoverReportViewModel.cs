﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class HandoverReportViewModel
    {
        public bool IsReceived { get; set; }

        public ModuleSourceType ReporType { get; set; }

        public Guid? CompanyLogoImageId { get; set; }

        public Guid SourceId { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Dates { get; set; }

        public string SharedUserInitials { get; set; }

        public string ContributorsInitials { get; set; }

        public Guid? SelectedSourceId { get; set; }

        public HandoverReportModuleViewModel HSEModule { get; set; }

        public HandoverReportModuleViewModel CoreModule { get; set; }

        public HandoverReportModuleViewModel TeamModule { get; set; }

        public HandoverReportModuleViewModel ProjectModule { get; set; }

        //public QMEReportModuleViewModel DailyNotesModule { get; set; }

        public HandoverReportTaskModuleViewModel TaskModule { get; set; }

        public File FileLogo { get; set; }

        public IEnumerable<TaskBoardTaskForHandover> TaskBoardTasks { get; set; }

        public string ShiftType { get; set; }

        public bool? IsPreviewMail { get; set; }
    }
}
