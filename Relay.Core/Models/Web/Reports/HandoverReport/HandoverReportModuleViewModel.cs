﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class HandoverReportModuleViewModel
    {
        public string Name { get; set; }

        public IEnumerable<HandoverReportTopicViewModel> Topics { get; set; }
    }
}
