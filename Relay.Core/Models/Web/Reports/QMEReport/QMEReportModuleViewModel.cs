﻿using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class QMEReportModuleViewModel
    {
        public string Name { get; set; }

        public IEnumerable<QMEReportTopicViewModel> Topics { get; set; }

        public bool IsTwoColInRow { get; set; }
    }
}
