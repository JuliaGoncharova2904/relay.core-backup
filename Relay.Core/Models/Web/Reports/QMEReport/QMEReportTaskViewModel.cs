﻿namespace MomentumPlus.Relay.Models
{
    public class QMEReportTaskViewModel
    {
        public string Name { get; set; }
        public string Reference { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public bool IsNullReport { get; set; }
        public string Date { get; set; }
    }
}
