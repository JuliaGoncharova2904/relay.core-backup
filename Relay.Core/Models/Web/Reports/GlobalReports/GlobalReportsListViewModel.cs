﻿using MvcPaging;
using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class GlobalReportsListViewModel
    {
        public IPagedList<BaseGlobalReportItemViewModel> ReportsItems { get; set; }
        public Guid? FilterUserId { get; set; }
        public RotationType? FilterRotationType { get; set; }
        public IEnumerable<DropDownListItemModel> Users { get; set; }

        public bool EnabledIcons { get; set; }
        public bool? ShowReports { get; set; }
    }
}
