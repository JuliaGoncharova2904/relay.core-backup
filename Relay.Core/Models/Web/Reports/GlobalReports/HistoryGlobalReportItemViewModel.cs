﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class HistoryGlobalReportItemViewModel : BaseGlobalReportItemViewModel
    {
        public Guid HandoverToId { get; set; }
        public string HandoverToName { get; set; }

        public bool EnabledIcons { get; set; }
    }
}
