﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SharedGlobalReportItemViewModel : BaseGlobalReportItemViewModel
    {
        public Guid HandoverCreatorId { get; set; }
        public string HandoverCreatorName { get; set; }
        public Guid HandoverRecipientId { get; set; }
        public string HandoverRecipientName { get; set; }
    }
}
