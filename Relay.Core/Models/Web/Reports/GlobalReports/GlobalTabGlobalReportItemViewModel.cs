﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models.Web.Reports.GlobalReports
{
    public class GlobalTabGlobalReportItemViewModel : BaseGlobalReportItemViewModel
    {
        public Guid HandoverCreatorId { get; set; }
        public string HandoverCreatorName { get; set; }
        public Guid HandoverRecipientId { get; set; }
        public string HandoverRecipientName { get; set; }
    }
}
