﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class ReceivedGlobalReportItemViewModel : BaseGlobalReportItemViewModel
    {
        public Guid ReceivedFromId { get; set; }
        public string ReceivedFromName { get; set; }
        public Guid FromSourceId { get; set; }

        public bool EnabledIcons { get; set; }
    }
}
