﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SharedTopicGlobalReportItemViewModel 
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public string Reference { get; set; }

        public string Notes { get; set; }

        public bool HasAttachments { get; set; }

        public string Owner { get; set; }

        public Guid OwnerId { get; set; }

        public int TaskCounter { get; set; }

        public int AttachmentsCounter { get; set; }

        public int ManagerCommentsCounter { get; set; }

        public bool HasComments { get; set; }

        public string DateString { get; set; }
    }
}
