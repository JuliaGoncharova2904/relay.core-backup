﻿using System;
using System.Collections.Generic;

namespace MomentumPlus.Relay.Models
{
    public class TopicSearchResultItem
    {
        public Guid Id { get; set; }

        public string CreatedDate { get; set; }

        public string CreatedTime { get; set; }

        public  string CreatorName { get; set; }

        public Guid CreatorId { get; set; }

        public string Name { get; set; }

        public string Reference { get; set; }

        public string Notes { get; set; }

        public bool HasLocation { get; set; }

        public bool HasTasks { get; set; }

        public bool HasAttachments { get; set; }

        public bool IsFinalized { get; set; }

        public string SearchTags { get; set; }

        public string FirstTagTopic { get; set; }

        public bool MoreTags { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    tagsString = string.Format("{0} \"{1}\",", tagsString, Tags);
                }

                return tagsString;
            }
        }
        public int AttachmentsCounter { get; set; }

        public int ManagerCommentsCounter { get; set; }

        public string Actual { get; set; }

        public string Planned { get; set; }

        public string Units { get; set; }

        public string Variance { get; set; }
    }
}
