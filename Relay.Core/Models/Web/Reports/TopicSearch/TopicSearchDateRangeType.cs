﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public enum TopicSearchDateRangeType
    {
        [Display(Name = "All Time")]
        [Description("All Time")]
        AllTime = 0,
        //[Description("Last Day")]
        //LastDay = 1,
        //[Description("Last 7 Days")]
        //Last7Days = 2,
        [Display(Name = "Last Month")]
        [Description("Last Month")]
        LastMonth = 3,
        //[Description("Last Year")]
        //LastYear = 4,
        [Display(Name = "Last 24hr")]
        [Description("Last 24hr")]
        Last24hr = 5,
        [Display(Name="Last 48hr")]
        //[Description("Last 48hr")]
        Last48hr = 6,
        [Display(Name = "Last Week")]
        [Description("Last Week")]
        LastWeek = 7,
        [Display(Name = "Last 6 Months")]
        [Description("Last 6 Months")]
        Last6Month = 8
    }
}
