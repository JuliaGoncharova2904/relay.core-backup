﻿using System;

namespace MomentumPlus.Relay.Models
{
    public class SearchBarItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }

    public class PeriodTopic
    {
        public int NumberPeriod { get; set; }

        public string Name { get; set; }
    }


    public enum PeriodTopicEnum
    {
        Last24Hours = 1,
        Last28Hours = 2,
        LastWeek = 3,
        LastMonth = 4,
        Last6Months = 5
    }
}

   
