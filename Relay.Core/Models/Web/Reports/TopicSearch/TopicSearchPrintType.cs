﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public enum TopicSearchPrintType
    {
        //[Description("Portrait")]
        [Display(Name = "Portrait")]
        portrait = 0,
        [Display(Name = "Landscape")]
        //[Description("Landscape")]
        landscape = 1,
    }
}
