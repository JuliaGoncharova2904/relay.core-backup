﻿using System;
using System.Collections.Generic;
using MomentumPlus.Core.Models;

namespace MomentumPlus.Relay.Models
{
    public class TopicReportPdfViewModel
    {
        public Guid? CompanyLogoImageId { get; set; }

        public List<TopicSearchResultItem> Topics { get; set; }

        public List<SearchResultTaskItem> Tasks { get; set; }

        public string Employees { get; set; }

        public string DateRange { get; set; }

        public string TopicsInSearch { get; set; }

        public string ReportDate { get; set; }

        public File FileLogo { get; set; }

        public TypeFilter typeFilter { get; set; }

        public int AttachmentsCounter { get; set; }
    }
}
