﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ChooseShiftRecipientModel
    {
        [Required]
        public Guid ShiftId { get; set; }

        [Required(ErrorMessage = "Please choose shift recipient")]
        public Guid? ShiftRecipientId { get; set; }

        public IEnumerable<DropDownListItemModel> TeamMembers { get; set; }
    }
}
