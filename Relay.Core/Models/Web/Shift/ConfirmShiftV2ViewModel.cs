﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ConfirmShiftV2ViewModel
    {
        public Guid ShiftOwnerId { get; set; }

        [Required]
        public ShiftEndType ShiftEndType { get; set; }

        [Required]
        public int? ShiftEndHour { get; set; }

        [Required]
        public int? ShiftEndMinutes { get; set; }

        public DateTime TodayShiftEndLimitDateTime { get; set; }

        public string ShiftPatternType { get; set; }
    }
}
