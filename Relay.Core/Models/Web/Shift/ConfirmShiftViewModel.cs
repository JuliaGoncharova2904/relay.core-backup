﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class ConfirmShiftViewModel
    {
        [Required]
        [Display(Name = "Start of your shift*")]
        public DateTime? StartShiftDate { get; set; }

        [Required]
        [Display(Name = "End of your shift*")]
        public DateTime? EndShiftDate { get; set; }

        [Required]
        [Display(Name = "Beginning of next shift*")]
        public DateTime? NextShiftDate { get; set; }

        public DateTime StartLimitDate { get; set; }
        public DateTime EndLimitDate { get; set; }

        public DateTime BeginningOfNextShiftLimitDate { get; set; }

        [Required]
        [Display(Name = "Repeat shift")]
        public int RepeatTimes { get; set; }
    }
}
