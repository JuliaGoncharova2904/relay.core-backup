﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class EditShiftDatesViewModel
    {
        [Required]
        public Guid ShiftId { get; set; }

        [Required]
        [Display(Name = "Start of your shift*")]
        public DateTime? StartShiftDate { get; set; }

        [Required]
        [Display(Name = "End of your shift*")]
        public DateTime? EndShiftDate { get; set; }
        public bool EndShiftDateReadOnly { get; set; }

        [Required]
        [Display(Name = "Beginning of next shift*")]
        public DateTime? NextShiftDate { get; set; }
        public bool NextShiftDateReadOnly { get; set; }

        public DateTime CurrentDate { get; set; }
        public DateTime EndLimitDate { get; set; }

        public DateTime BeginningOfNextShiftLimitDate { get; set; }

        [Required]
        [Display(Name = "Repeat shift")]
        public int RepeatTimes { get; set; }
    }
}
