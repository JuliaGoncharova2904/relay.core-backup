﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public class EditShiftDatesV2ViewModel
    {
        [Required]
        public Guid ShiftId { get; set; }

        [Required]
        public DateTime EndShiftDate { get; set; }

        [Required]
        public int ShiftEndHour { get; set; }

        [Required]
        public int ShiftEndMinutes { get; set; }

        public DateTime LimitEndShiftDate { get; set; }

        public int LimitShiftEndHour { get; set; }

        public int LimitShiftEndMinutes { get; set; }


    }
}
