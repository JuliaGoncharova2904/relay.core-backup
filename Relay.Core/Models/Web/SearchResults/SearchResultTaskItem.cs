﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class SearchResultTaskItem
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string CompleteStatus { get; set; }

        public Guid? AssignedToId { get; set; }

        public Guid? CreatorId { get; set; }

        public string CreatorName { get; set; }

        public virtual UserProfile AssignedTo { get; set; }

        public DateTime? CreateDate { get; set; }

        public int TaskCounter { get; set; }

    }
}