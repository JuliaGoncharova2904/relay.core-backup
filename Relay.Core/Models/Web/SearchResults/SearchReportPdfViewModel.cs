﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class SearchReportPdfViewModel
    {
        public Guid? CompanyLogoImageId { get; set; }

        public List<SearchResultsItem> Topics { get; set; }

        public string Employees { get; set; }

        public string DateRange { get; set; }

        public string TopicsInSearch { get; set; }

        public string ReportDate { get; set; }
    }
}