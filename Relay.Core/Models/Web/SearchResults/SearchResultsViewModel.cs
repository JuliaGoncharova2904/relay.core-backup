﻿using MomentumPlus.Core.Models;
using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MomentumPlus.Relay.Models
{
    public class SearchResultsViewModel
    {
        public IPagedList<SearchResultsItem> ResultItems { get; set; }
        public SearchViewModel Params { get; set; }
        public Guid? FilterUserId { get; set; }
        public List<UserProfile> Users { get; set; }
        public IEnumerable<DropDownListItemModel> UsersList { get; set; }
    }

    public class SearchTasksResultsViewModel
    {
        public IPagedList<SearchResultTaskItem> ResultItems { get; set; }
        public SearchViewModel Params { get; set; }
        public Guid? FilterUserId { get; set; }
        public List<UserProfile> Users { get; set; }
    }
}
