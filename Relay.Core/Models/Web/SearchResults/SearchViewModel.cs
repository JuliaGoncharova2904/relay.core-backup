﻿using MomentumPlus.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MomentumPlus.Relay.Models
{
    public class SearchViewModel
    {
        public IEnumerable<Guid?> SelectedTopicGroupsIds { get; set; }

        public IEnumerable<Guid?> SelectedUserIds { get; set; }

        public TopicSearchDateRangeType DateRangeType { get; set; }

        public List<SelectListItem> DateRangeTypes { get; set; }

        public TopicSearchPrintType PrintType { get; set; }

        public int? PageNumber { get; set; }

        public int? PageSize { get; set; }

        public int SelectedPeriod { get; set; }

        public string SearchText { get; set; }

        public IEnumerable<string> Tags { get; set; }

        public string TagsString
        {
            get
            {
                string tagsString = string.Empty;

                if (Tags != null)
                {
                    tagsString = string.Format("{0} \"{1}\",", tagsString, Tags);
                }

                return tagsString;
            }
        }

        public TypeFilter TypeFilter { get; set; }

        public IEnumerable<Guid?> SelectedTasksIds { get; set; }

        public List<SelectListItem> PrintTypes { get; set; }

        public int AttachmentsCounter { get; set; }

        public Guid? FilterUserId { get; set; }
        public IEnumerable<DropDownListItemModel> UsersList { get; set; }


        public string SelectedTopicName { get; set; }
        public IEnumerable<DropDownListItemModel> NameTopics { get; set; }
    }

    public enum TypeFilter
    {
        Topics = 1,
        Tags = 2,
        Tasks = 3,
        Shared = 4,
        Metrics = 5
    }
}
