﻿namespace MomentumPlus.Relay.Models
{
    public class DropDownListItemModel
    {
        public bool Disabled { get; set; }

        public bool Selected { get; set; }

        public string Text { get; set; }

        public string Value { get; set; }
    }
}
