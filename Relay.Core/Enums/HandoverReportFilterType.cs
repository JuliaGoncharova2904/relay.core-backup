﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum HandoverReportFilterType
    {
        [Description("Handover Report")]
        HandoverItems = 0,

        [Description("Checked")]
        NrItems = 1,

       [Description("All Topics")]
        All = 2
    }
}
