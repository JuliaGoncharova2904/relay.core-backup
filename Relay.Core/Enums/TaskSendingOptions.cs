﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum TaskSendingOptions
    {
        EndOfSwing = 0,
        Now = 1,
        Pending = 2,
    }
}