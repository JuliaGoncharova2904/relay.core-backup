﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum ReportType
    {
        [Description("Swing")]
        Draft = 0,

        [Description("Received")]
        Received = 1
    }
}
