﻿namespace MomentumPlus.Relay.Models
{
    public enum ConfirmRotationState
    {
        NoOperation = 0,
        RequestCustomPatern = 1,
        AcceptCustomPatern = 2,
        DenieCustomPatern = 3
    }
}
