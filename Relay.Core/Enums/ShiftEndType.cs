﻿using System.ComponentModel;

namespace MomentumPlus.Relay.Models
{
    public enum ShiftEndType
    {
        [Description("Today")]
        Today = 0,
        [Description("Tomorrow")]
        Tomorrow = 1
    }
}
