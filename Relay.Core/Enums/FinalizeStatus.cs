﻿namespace MomentumPlus.Relay.Models
{
    public enum FinalizeStatus
    {
        NotFinalized = 0,
        Finalized = 1,
        AutoFinalized = 2
    }
}
