﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace MomentumPlus.Relay.Models
{
    public enum HandoverTriggerType
    {
        [Description("Final Day Of Swing")]
        [Display(Name = "Final Day Of Swing")]
        FinalDayOfSwing = 0
    }
}
