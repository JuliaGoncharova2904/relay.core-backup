﻿namespace MomentumPlus.Relay.Models
{
    public enum AlertType
    {
        Success,
        Info,
        Warning,
        Danger
    }
}
